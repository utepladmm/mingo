RESULTADOS ESCENARIO = 1, , , , , ,
========================, , , , , ,
PARQUE OPTIMO,2018,2019,2020,2021,2022,
EOLO100 CON	 IT=-1	 DA=0	 IA=1.0,0,0,0,0,0,
EOLO100 OPE	 IT=0	 DA=0	 IA=1.0,0,0,0,0,0,
GTG120 CON	 IT=-1	 DA=0	 IA=1.0,0,0,0,0,0,
GTG120 OPE	 IT=0	 DA=0	 IA=1.0,0,0,0,0,0,
SCC180 CON	 IT=-1	 DA=0	 IA=1.0,0,0,0,0,0,
SCC180 OPE	 IT=0	 DA=0	 IA=1.0,0,0,0,0,0,
SCC_180 CON	 IT=-1	 DA=0	 IA=1.0,0,0,0,0,0,
SCC_180 OPE	 IT=0	 DA=0	 IA=1.0,0,0,0,0,0,
SOL200 CON	 IT=-1	 DA=0	 IA=1.0,0,0,0,0,0,
SOL200 OPE	 IT=0	 DA=0	 IA=1.0,0,0,0,1,1,
DECISIÓN OPTIMA, , , , , ,
EOLO100,0,0,0,0, ,
GTG120,0,0,0,0, ,
SCC180,0,0,0,0, ,
SCC_180,0,0,0,0, ,
SOL200,0,0,1,0, ,
VALOR DE BELLMAN DEL CD,840.90,838.25,831.87,793.34, ,
COSTO ESPERADO GENERALIZADO DEL CD,82.70,86.00,116.04,136.09,729.95,
ANUALIDADES DE INVERSION DEL CD,0.00,0.00,0.00,0.00,159.80,
COSTOS FIJOS DEL CD,0.00,0.00,0.00,23.08,0.00,
NOTA: los valores del Bellman se calculan al inicio del paso de tiempo, el resto de valores en el instante medio, , , , ,23.08,
 , , , , , ,
 , , , , , ,
 , , , , , ,
 , , , , , ,
 , , , , , ,
 , , , , , ,
********** ENERGIAS ( GWh )************, , , , , ,
PALIER(fuente en DB),2018,2019,2020,2021,2022,
TERRA,588.38,591.65,601.01,618.43,628.98,
BAYGORRIA,429.77,432.57,439.30,442.06,449.56,
PALMAR,1435.88,1440.71,1453.89,1457.98,1470.76,
SALTO,3892.84,3892.84,3892.84,3892.84,3892.84,
TOTAL HID,6346.89,6357.79,6387.07,6411.32,6442.16,
CTR,22.06,10.08,12.72,10.44,12.02,
GAPR_C,11.31,4.83,6.44,5.06,6.29,
GMO80_P,141.28,48.91,41.51,31.62,39.06,
GMO80_S,0.00,0.00,0.00,0.00,0.00,
GPTI,293.25,48.83,62.82,49.37,60.21,
GTG170,173.80,0.00,0.00,0.00,0.00,
SCC500,0.00,702.84,835.58,750.00,863.99,
TOTAL TERM,641.73,815.52,959.10,846.51,981.58,
EOLOdeci,4883.41,4880.13,4883.16,4911.26,4887.88,
SOLdeci,450.50,455.21,453.81,452.81,454.22,
SOL200,0.00,0.00,0.00,378.92,380.10,
TOTAL EOLO,5333.92,5335.34,5336.97,5743.00,5722.22,
BmDesp,1.73,1.26,1.49,1.39,1.57,
BmNoDesp,264.70,315.40,315.40,315.40,315.40,
MDP,549.22,549.22,549.22,547.03,547.03,
UPM1,114.93,114.93,114.93,114.78,114.78,
TOTAL BIO,930.59,980.81,981.05,978.60,978.78,
IARGO,0.93,0.27,0.34,0.40,0.65,
IBMELO,3.86,1.38,2.09,3.07,4.25,
IBRIV,0.00,0.00,0.00,0.00,0.00,
TOTAL IMP,4.79,1.66,2.44,3.48,4.92,
EXCED,-2029.51,-1996.02,-1848.83,-1901.88,-1731.64,
TOTAL EXP,-2029.51,-1996.02,-1848.83,-1901.88,-1731.64,
FALLA 1,0.55,0.30,0.50,0.46,0.68,
FALLA 2,0.32,0.22,0.26,0.39,0.63,
FALLA 3,0.18,0.13,0.13,0.30,0.51,
FALLA 4,0.04,0.00,0.04,0.16,0.24,
TOTAL FALLA,1.10,0.67,0.94,1.32,2.09,
DEMANDA,11229.50,11495.69,11818.69,12082.30,12400.10,
TOTAL DEMANDA,11229.50,11495.69,11818.69,12082.30,12400.10,
********** COSTOS  ( MUSD )************, , , , , ,
PALIER (fuente en DB),2018,2019,2020,2021,2022,
TERRA,0.00,0.00,0.00,0.00,0.00,
BAYGORRIA,0.00,0.00,0.00,0.00,0.00,
PALMAR,0.00,0.00,0.00,0.00,0.00,
SALTO,0.00,0.00,0.00,0.00,0.00,
TOTALHID,0.00,0.00,0.00,0.00,0.00,
CTR,3505.86,1695.49,2447.43,2120.57,2510.61,
GAPR_C,1615.19,740.38,1125.15,959.92,1253.47,
GMO80_P,13153.94,4287.97,4790.39,3993.23,5083.28,
GMO80_S,0.00,0.00,0.00,0.00,0.00,
GPTI,38357.51,6903.09,10109.26,8658.85,10938.30,
GTG170,23254.27,0.00,0.00,0.00,0.00,
SCC500,0.00,71170.12,95817.29,94182.59,112257.17,
TOTALTERM,79886.79,84797.07,114289.55,109915.18,132042.85,
EOLOdeci,0.00,0.00,0.00,0.00,0.00,
SOLdeci,0.00,0.00,0.00,0.00,0.00,
SOL200,0.00,0.00,0.00,0.00,0.00,
TOTALEOLO,0.00,0.00,0.00,0.00,0.00,
BmDesp,121.74,88.27,104.60,97.43,110.10,
BmNoDesp,2.59,3.19,3.19,3.19,3.19,
MDP,5.49,5.49,5.49,5.47,5.47,
UPM1,1.14,1.14,1.14,1.15,1.15,
TOTALBIO,130.99,98.12,114.44,107.25,119.92,
IARGO,336.12,87.89,109.36,135.48,222.14,
IBMELO,1394.48,499.12,756.65,1111.27,1537.02,
IBRIV,0.00,0.00,0.00,0.00,2.78,
TOTALIMP,1730.60,587.02,866.01,1246.75,1761.94,
EXCED,-60.88,-59.88,-55.46,-57.05,-51.94,
TOTALEXP,-60.88,-59.88,-55.46,-57.05,-51.94,
FALLA 1,205.65,97.70,164.36,150.88,228.70,
FALLA 2,192.88,138.05,160.64,235.60,383.76,
FALLA 3,439.95,319.72,317.92,737.46,1240.65,
FALLA 4,179.28,29.57,188.11,674.49,996.51,
TOTAL FALLA,1017.78,585.06,831.04,1798.44,2849.63,
DEMANDA, , , , , ,
TOTAL COSTOS DE MAQUINAS (SIN SC_gas),82705.29,86007.39,116045.59,113010.58,136722.41,
