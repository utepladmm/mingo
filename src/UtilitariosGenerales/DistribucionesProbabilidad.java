/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UtilitariosGenerales;

import java.util.ArrayList;
import java.util.Collections;

import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class DistribucionesProbabilidad {

    /**
     * Devuelve los valores de la distribución contenida en datos, que toman
     * los percentiles de vPercentiles.
     * Se suponen los datos equiprobables.
     * Si exacto = false interpola en los valores de datos para devolver los percentiles.
     * Si exacto = true devuelve el valor de dato exacto más cercano al vPercentil interpolado
     * @param datos son los datos de la muestra en cualquier orden
     * @param vPercentiles son los valores de percentiles a calcular, entre 0 y 1.
     * @param exacto es true si se busca el más cercano valor de dato y false si se interpola
     *
     */
    public static ArrayList<Double> percentiles(ArrayList<Double> datos, ArrayList<Double> vPercentiles,
            boolean exacto) throws XcargaDatos {

        ArrayList<Double> dPercentiles = new ArrayList<Double>();

        ArrayList<Double> probs = new ArrayList<Double>();
        for(int ip = 0; ip<datos.size(); ip++){
            double unaProb = (double)(1/(double)datos.size());
            probs.add(unaProb);
        }
        dPercentiles = percentiles(datos, probs, vPercentiles, exacto);


        return dPercentiles;
    }

    /**
     * Devuelve los valores de la distribución contenida en datos, que toman
     * los percentiles de vPercentiles.
     * Las probabilidades de los datos están en probs, en el mismo orden en que aparecen
     * en datos.
     * La lista datos no se altera.
     * Si exacto = false interpola en los valores de datos para devolver los percentiles.
     * Si exacto = true devuelve el valor de dato exacto más cercano al vPercentil interpolado
     * @param datos son los datos de la muestra en cualquier orden
     * @param vPercentiles son los valores de percentiles a calcular, entre 0 y 1.
     * @param exacto es true si se busca el más cercano valor de dato y false si se interpola
     *
     */
    public static ArrayList<Double> percentiles(ArrayList<Double> datos, ArrayList<Double> probs,
            ArrayList<Double> vPercentiles, boolean exacto) throws XcargaDatos {


        if (probs.size() != datos.size()) {
            throw new XcargaDatos("Se invocó percentiles con " +
                    "una cantidad de datos distinta de la cantidad de probabilidades asociadas");
        }

        ArrayList<ParDatos> datosYProbs = new ArrayList<ParDatos>();
        for (int id = 0; id < datos.size(); id++) {
            ParDatos ppd = new ParDatos(datos.get(id), probs.get(id));
            datosYProbs.add(ppd);
        }
        Collections.sort(datosYProbs);

        ArrayList<Double> dPercentiles = new ArrayList<Double>();
        ArrayList<Double> probAcum = new ArrayList<Double>();
        /**
         * probAcum tiene la probabilidad acumulada hasta el respectivo valor de datos
         * es decir continua por izquierda prob(va <= x)
         */
        int cantDatos = datos.size();

        /**
         * Si hay un sólo dato todos los percentiles son ese dato.
         */
        if (cantDatos == 1) {
            for (int i = 0; i < vPercentiles.size(); i++) {
                dPercentiles.add(datos.get(0));
            }
            return dPercentiles;
        }

        /**
         * Calcula en probAcum la distribución de probabilidad acumulada de los datos
         * ordenados en datosYProbs.
         */
        for (ParDatos pDP : datosYProbs) {
            probAcum.add((Double)pDP.getDato2());
        }
        for (int ipa = 1; ipa<probAcum.size(); ipa++ ){
            probAcum.set(ipa, probAcum.get(ipa-1)+probAcum.get(ipa));
        }
        probAcum.set(cantDatos - 1, 1.0); // remacha 1 por las dudas en el mayor valor de datos

        for (Double vP : vPercentiles) {
            if (vP > 1 || vP < 0) {
                throw new XcargaDatos("Se pidió un percentil mayor que 1 o menor que 0");
            }
            if (vP < probAcum.get(0)) {
                dPercentiles.add(datosYProbs.get(0).getDato());              
            }else{
                int i = 1;
                while (vP > probAcum.get(i)) {
                    i++;
                }
                // i está indicando la posición en datosYProbs del primer dato mayor o igual a vP
                int ialto = i;
                int ibajo = i - 1;
                double datoAlto = datosYProbs.get(ialto).getDato();
                double datoBajo = datosYProbs.get(ibajo).getDato();
                if (vP == probAcum.get(ialto)) {
                    dPercentiles.add(datoAlto);
                } else if (vP == probAcum.get(ibajo)) {
                    dPercentiles.add(datoBajo);
                } else {
                    double paso = probAcum.get(ialto)-probAcum.get(ibajo);
                    double palto = (vP - probAcum.get(ibajo)) / paso;
                    double pbajo = (probAcum.get(ialto) - vP) / paso;
                    if (exacto) {
                        if (palto >= pbajo) {
                            dPercentiles.add(datoAlto);
                        } else {
                            dPercentiles.add(datoBajo);
                        }
                    } else {
                        dPercentiles.add(datoAlto * palto + datoBajo * pbajo);
                    }
                }

            }
        }
        return dPercentiles;
    }

    public static void main(String[] args) throws XcargaDatos {

        ArrayList<Double> datos = new ArrayList<Double>();
        datos.add(5.0);
        datos.add(3.9);
        datos.add(2.9);
        datos.add(2.2);
        datos.add(1.0);
        datos.add(1.0);
        datos.add(1.9);
        datos.add(2.3);
        double unaprob;
        unaprob = (double)1/(double)8;
        ArrayList<Double> probs = new ArrayList<Double>();
        for(int i=1;i<=8;i++){
           probs.add(unaprob);
        }
        ArrayList<Double> vPercentiles = new ArrayList<Double>();
        vPercentiles.add(0.33);
        vPercentiles.add(1.0);
        vPercentiles.add(0.0);

        ArrayList<Double> dPercentiles = percentiles(datos, probs, vPercentiles, false);

        System.out.println("no exactos");
        for (int i = 0; i < dPercentiles.size(); i++) {
            System.out.println(dPercentiles.get(i));
        }

        dPercentiles = percentiles(datos, probs, vPercentiles, true);

        System.out.println("exactos");
        for (int i = 0; i < dPercentiles.size(); i++) {
            System.out.println(dPercentiles.get(i));
        }

        dPercentiles = percentiles(datos, vPercentiles, true);

    }
}
