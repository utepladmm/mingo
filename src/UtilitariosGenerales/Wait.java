/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilitariosGenerales;

/**
 *
 * @author ut469262
 */
public class Wait {

    /**
     * Espera un segundo
     */
    public static void oneSec() {
     try {
            Thread.sleep(1000);
       }
     catch (InterruptedException e) {
       e.printStackTrace();
       }
     }

    /**
     * Espera un s segundos
     */
    public static void manySec(long s) {
        try {
                Thread.sleep(s * 1000);
        }
        catch (InterruptedException e) {
           e.printStackTrace();
        }
    }



  public static void main(String args[]) {
     System.out.println("Wait one second");
     Wait.oneSec();
     System.out.println("Done\nWait five seconds");
     Wait.manySec(5);
     System.out.println("Done");
     }

}
