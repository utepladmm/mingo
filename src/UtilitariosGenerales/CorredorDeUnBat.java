/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UtilitariosGenerales;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author ut469262
 */
public class CorredorDeUnBat {

    /**
     * Ejecuta el programa dirBat
     * @param dirBat
     */
    public static void correBat(String dirBat) {
        dirBat = DirectoriosYArchivos.barraAscendente(dirBat);
//        Runtime run = Runtime.getRuntime();
//        try {
//            run.exec(dirBat);
//        } catch (Exception e) {
//        e.printStackTrace();
//        }

        try {
            /* directorio/ejecutable es el path del ejecutable y un nombre */
            Process p = Runtime.getRuntime().exec(dirBat);
            // Se obtiene el stream de salida del programa
            InputStream is = p.getInputStream();

            /* Se prepara un bufferedReader para poder leer la salida más comodamente. */
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            // Se lee la primera linea
            String aux = br.readLine();

            // Mientras se haya leido alguna linea
            while (aux != null) {
                // Se escribe la linea en pantalla
                System.out.println(aux);

                // y se lee la siguiente.
                aux = br.readLine();
            }

        } catch (Exception e) {
            /* Se lanza una excepción si no se encuentra en ejecutable o el fichero no es ejecutable. */
            e.printStackTrace();
        }


    }

    public static void main(String[] args) throws Exception {
        String dirBat = "Q:/EDF/2011/PruebasMingo/PruebaMingo6/corridas/c_1_1";
        dirBat += "/corridas_PC127537_22297.bat";
        correBat(dirBat);
    }
}
