/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UtilitariosGenerales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import persistenciaMingo.XcargaDatos;

public class DirectoriosYArchivos {

    /**
     * Contiene métodos para crear y manejar archivos y directorios.
     */
    /**
     * Crea un archivo con líneas de texto a partir de un String[][]
     * Hay una línea para cada valor de i [i][.]
     * Cada línea tiene Strings separados por un separador sep, dado por el usuario 
     * El texto es grabado en el archivo dirArchivo.
     */
    public static void creaTexto(String[][] texto, String dirArchivo, String sep) throws XcargaDatos {
        try {
            dirArchivo = barraAscendente(dirArchivo);
            File archGraba = new File(dirArchivo);
            // Crear el archivo si no existe
            boolean success = archGraba.createNewFile();
            if (success) {
                // El archivo no existía y fue creado
            } else {
                // El archivo ya existía
            }
            String linea;
            PrintStream print = new PrintStream(archGraba);
            for (int ilin = 0; ilin < texto.length; ilin++) {
                linea = "";
                for (int icol = 0; icol < texto[ilin].length; icol++) {
                    if (texto[ilin][icol] != null) {
                        linea = linea + texto[ilin][icol] + sep;

                    }else{
                        linea = linea + " " + sep;                        
                    }
                }
                print.println(linea);                
            }
            print.close();
        }  catch (IOException e) {
            throw new XcargaDatos("Error al grabar el archivo " + dirArchivo);
        }
    }

    /**
     * @author ut469262
     * Graba un texto en un archivo. Si el archivo no existe lo crea.
     * @param dirArchivo es el nombre del archivo incluso el path.
     * @param texto es el texto a grabar.
     */
    public static void grabaTexto(String dirArchivo, String texto) throws XcargaDatos {
        try {
            dirArchivo = barraAscendente(dirArchivo);
            File archGraba = new File(dirArchivo);
            // Crear el archivo si no existe
            boolean success = archGraba.createNewFile();
            if (success) {
                // El archivo no existía y fue creado
            } else {
                // El archivo ya existía
            }
            PrintStream print = new PrintStream(archGraba);
            print.println(texto);
            print.close();
        } catch (IOException e) {
            throw new XcargaDatos("Error al grabar el archivo " + dirArchivo);
        }
    }
    /**
     * Graba un texto en un archivo a partir de una lista de l�neas de texto.
     * Las l�neas se graban saltando de l�nea en el archivo grabado.
     * @param dirArchivo
     * @param texto
     */
    public static void grabaTextoDeLineas(String dirArchivo, ArrayList<String> texto) throws XcargaDatos{
        try {
            dirArchivo = barraAscendente(dirArchivo);
            File archGraba = new File(dirArchivo);
            // Crear el archivo si no existe
            boolean success = archGraba.createNewFile();
            if (success) {
                // El archivo no existía y fue creado
            } else {
                // El archivo ya existía
            }
            PrintStream print = new PrintStream(archGraba);
        	for(String linea: texto){     	        		
        		print.println(linea);
        	}
            print.close();
        } catch (IOException e) {
            throw new XcargaDatos("Error al grabar el archivo " + dirArchivo);
        }    	

    	
    	
    	
    }
    /**
     * Graba un texto en un archivo. Si el archivo no existe lo crea.
     * @param dirArchivo es el nombre del archivo incluso el path.
     * @param texto es el texto a grabar.
     */
    public static void agregaTexto(String dirArchivo, String texto) throws XcargaDatos {
        dirArchivo = barraAscendente(dirArchivo);
        try {
            File archGraba = new File(dirArchivo);
            if (!archGraba.exists()) {
                archGraba.createNewFile();
            }
            FileOutputStream fileStream = new FileOutputStream(archGraba, true);
            PrintStream print = new PrintStream(fileStream);
            print.println(texto);
            print.close();
        } catch (IOException e) {
            throw new XcargaDatos("Error al grabar el archivo " + dirArchivo);
        }
    }

    /**
     * Graba un texto en un archivo. Si el archivo no existe lo crea.
     * @param dirArchivo es el nombre del archivo incluso el path.
     * @param arrayst es el texto a grabar por filas y columnas
     * @parm sep es un separador de columnas a grabar
     */
    public static void agregaTexto(String dirArchivo, String[][] arrayst, String sep) throws XcargaDatos {
        dirArchivo = barraAscendente(dirArchivo);
        try {
            File archGraba = new File(dirArchivo);
            if (!archGraba.exists()) {
                archGraba.createNewFile();
            }
            FileOutputStream fileStream = new FileOutputStream(archGraba, true);
            PrintStream print = new PrintStream(fileStream);
            String texto = "";
            for(int ifil =0;ifil <arrayst.length;ifil++){
                for(int icol=0;icol<arrayst[ifil].length;icol++){
                    if(arrayst[ifil][icol]!= null){
                        texto += arrayst[ifil][icol] + sep;
                    }else{
                        texto += " " + sep;
                    }
                }                
                if(ifil!=arrayst.length-1) texto += "\r\n";                
            }
                
            print.println(texto);
            print.close();
        } catch (IOException e) {
            throw new XcargaDatos("Error al grabar el archivo " + dirArchivo);
        }
    }    
    
    
    /**
     * Crea un directorio de nombre dirNuevo en el directorio dirRaiz
     * Si no existe dirRaiz hay un error.
     * @param dirRaiz es el path del directorio raíz.
     * @param dirNuevo es el nombre del subdirectorio a agregar.
     */
    public static void creaDirectorio(String dirRaiz, String dirNuevo) throws XcargaDatos {
        dirRaiz = barraAscendente(dirRaiz);
        dirNuevo = barraAscendente(dirNuevo);
        File pathDirRaiz = new File(dirRaiz);
        if (!pathDirRaiz.exists()) {
            throw new XcargaDatos("El directorio raíz " + dirRaiz + " no existe");
        }
        File pathDirectorio = new File(dirRaiz + "/" + dirNuevo);
        System.out.println(pathDirectorio);

        if (!pathDirectorio.mkdir()) {
            System.out.println("El directorio " + pathDirectorio.toString() + " ya existía");
        }
    }

    /**
     * @param dirArch es el archivo incluso path cuya existencia quiere verificarse
     * @return result es true si el archivo existe y false de lo contrario
     */
    public static boolean existeArchivo(String dirArch) {
        dirArch = barraAscendente(dirArch);
        File fin = new File(dirArch);
        boolean result = fin.exists();
        return result;
    }

    /**
     * Elimina el archivo o directorio dirArch
     * Si es un directorio y no está vacío lanza una excepción
     * @param dirArch
     * @return
     */
    public static boolean eliminaArchivo(String dirArch) {
        dirArch = barraAscendente(dirArch);
        File f = new File(dirArch);
        // Make sure the file or directory exists and isn't write protected
        if (!f.exists()) {
            throw new IllegalArgumentException(
                    "Delete: no such file or directory: " + dirArch);
        }
        if (!f.canWrite()) {
            throw new IllegalArgumentException("Delete: write protected: " + dirArch);
        }
        // If it is a directory, make sure it is empty
        if (f.isDirectory()) {
            String[] files = f.list();
            if (files.length > 0) {
                throw new IllegalArgumentException(
                        "Delete: directory not empty: " + dirArch);
            }
        }
        // Attempt to delete it
        boolean success = f.delete();
        if (!success) {
            throw new IllegalArgumentException("Delete: deletion failed");
        }
        return success;
    }

    /**
     * Sustituye la barra descendente \ por la ascendente en el string input
     * @param input
     * @return 
     */
    public static String barraAscendente(String input) {
        StringBuffer buffer = new StringBuffer(input);
        StringBuffer output = new StringBuffer();
        for (int i = 0; i < buffer.length(); i++) {
            if (buffer.charAt(i) == '\\') {
                output.append('/');
            } else {
                output.append(buffer.charAt(i));
            }
        }
        return output.toString();
    }

    @SuppressWarnings("empty-statement")
    public static void copy(String fromFileName, String toFileName)
            throws IOException {
        File fromFile = new File(fromFileName);
        File toFile = new File(toFileName);

        if (!fromFile.exists()) {
            throw new IOException("FileCopy: " + "no such source file: " + fromFileName);
        }
        if (!fromFile.isFile()) {
            throw new IOException("FileCopy: " + "can't copy directory: " + fromFileName);
        }
        if (!fromFile.canRead()) {
            throw new IOException("FileCopy: " + "source file is unreadable: " + fromFileName);
        }

        if (toFile.isDirectory()) {
            toFile = new File(toFile, fromFile.getName());
        }

        if (toFile.exists()) {
            if (!toFile.canWrite()) {
                throw new IOException("FileCopy: " + "destination file is unwriteable: " + toFileName);
            }
            System.out.print("Overwrite existing file " + toFile.getName() + "? (Y/N): ");
            System.out.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    System.in));
            String response = in.readLine();
            if (!response.equals("Y") && !response.equals("y")) {
                throw new IOException("FileCopy: " + "existing file was not overwritten.");
            }
        } else {
            String parent = toFile.getParent();
            if (parent == null) {
                parent = System.getProperty("user.dir");
            }
            File dir = new File(parent);
            if (!dir.exists()) {
                throw new IOException("FileCopy: " + "destination directory doesn't exist: " + parent);
            }
            if (dir.isFile()) {
                throw new IOException("FileCopy: " + "destination is not a directory: " + parent);
            }
            if (!dir.canWrite()) {
                throw new IOException("FileCopy: " + "destination directory is unwriteable: " + parent);
            }
        }

        FileInputStream from = null;
        FileOutputStream to = null;
        try {
            from = new FileInputStream(fromFile);
            to = new FileOutputStream(toFile);
            byte[] buffer = new byte[4096];
            int bytesRead;

            while ((bytesRead = from.read(buffer)) != -1) {
                to.write(buffer, 0, bytesRead); // write
            }
        } finally {
            if (from != null) {
                try {
                    from.close();
                } catch (IOException e) {
                    ;
                }
            }
            if (to != null) {
                try {
                    to.close();
                } catch (IOException e) {
                    ;
                }
            }
        }
    }

    /**
     * Copies src file to dst file.
     * If the dst file does not exist, it is created
     */
    public static void copy2(String stSrc, String stDst) throws IOException {
        File src = new File(stSrc);
        File dst = new File(stDst);
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static void main(String[] args) throws IOException, XcargaDatos {
 
  
        String[] s1 = new String[] {"1", "2", "3"};
        String[] s2 = new String[] {"1", "2", "3"};        
        String[][] st = new String[2][];
        st[0]=s1;
        st[1]=s2;
        String dirArchivo = "Q:/EDF/2012/PlanOct2012/Estudio/CASOsinCAR/Result_Caso/PruebaGrabaArchTexto.txt";
        agregaTexto(dirArchivo, st, ",");
        agregaTexto(dirArchivo, st, ",");        
    }

    
    
    
}
