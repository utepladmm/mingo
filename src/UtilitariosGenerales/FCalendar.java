/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilitariosGenerales;

import dominio.DatosGeneralesEstudio;
import dominio.TiempoAbsoluto;
import java.util.Calendar;

/**
 *
 * @author ut469262
 */
public class FCalendar {

    /**
     * Devuelve la semana del año a partir de una fecha en Calendar
     */
    public static int semDelAnio(Calendar fecha){
        int sem = 0;
        int dia = fecha.get(Calendar.DAY_OF_YEAR);
        sem = Math.min((Integer)(dia/7) + 1, 52);
        return sem;
    }

    /**
     * Devuelve la cantidad de días del año anio
     */
    public static int cantDiasAnio(int anio){
        int cantDias = 0;
        Calendar ultDiaAnio = Calendar.getInstance();
        ultDiaAnio.set(anio, Calendar.DECEMBER, 31);
        cantDias = ultDiaAnio.get(Calendar.DAY_OF_YEAR);
        return cantDias;
    }


    /**
     * Devuelve la fecha inicial en Calendar de un TiempoAbsoluto
     *
     */
    public static Calendar fechaFinDeTa(int anio, int parteDelAnio, DatosGeneralesEstudio datGen){
        int pasosPorAnio = datGen.getPasosPorAnio();
        Calendar fecha = Calendar.getInstance();
        int cantDiasAnio = FCalendar.cantDiasAnio(anio);
        int diaDelAnioFinal;
        fecha.set(Calendar.YEAR, anio);
        if (pasosPorAnio==1){
            diaDelAnioFinal = cantDiasAnio;
        }else{
            diaDelAnioFinal = (cantDiasAnio*parteDelAnio)/pasosPorAnio;
        }
        fecha.set(Calendar.DAY_OF_YEAR, diaDelAnioFinal);
        return fecha;
    }

    public static Calendar fechaFinDeTa(TiempoAbsoluto ta, DatosGeneralesEstudio datGen){
        int anio = ta.getAnio();
        int parteDelAnio = ta.getParteDelAnio();
        int pasosPorAnio = datGen.getPasosPorAnio();
        Calendar fecha = Calendar.getInstance();
        int cantDiasAnio = FCalendar.cantDiasAnio(anio);
        int diaDelAnioFinal;
        fecha.set(Calendar.YEAR, anio);
        if (pasosPorAnio==1){
            diaDelAnioFinal = cantDiasAnio;
        }else{
            diaDelAnioFinal = (cantDiasAnio*parteDelAnio)/pasosPorAnio;
        }
        fecha.set(Calendar.DAY_OF_YEAR, diaDelAnioFinal);
        return fecha;
    }


    public static Calendar fechaIniDeTa(int anio, int parteDelAnio, DatosGeneralesEstudio datGen){
        int pasosPorAnio = datGen.getPasosPorAnio();
        Calendar fecha = Calendar.getInstance();
        int cantDiasAnio = FCalendar.cantDiasAnio(anio);
        int diaDelAnioInicial;
        fecha.set(Calendar.YEAR, anio);
        diaDelAnioInicial = Math.max(1, (cantDiasAnio*(parteDelAnio-1)) /pasosPorAnio);
        fecha.set(Calendar.DAY_OF_YEAR, diaDelAnioInicial);
        return fecha;
    }

    public static Calendar fechaIniDeTa(TiempoAbsoluto ta, DatosGeneralesEstudio datGen){
        int anio = ta.getAnio();
        int parteDelAnio = ta.getParteDelAnio();
        int pasosPorAnio = datGen.getPasosPorAnio();
        Calendar fecha = Calendar.getInstance();
        int cantDiasAnio = FCalendar.cantDiasAnio(anio);
        int diaDelAnioInicial;
        fecha.set(Calendar.YEAR, anio);
        diaDelAnioInicial = Math.max(1, (cantDiasAnio*(parteDelAnio-1)) /pasosPorAnio);
        fecha.set(Calendar.DAY_OF_YEAR, diaDelAnioInicial);
        return fecha;
    }



    public static void main(String[] args){
        Calendar fecha = Calendar.getInstance();
        fecha.set(2014, 0, 1);
        int semana = FCalendar.semDelAnio(fecha);
        System.out.println("La semana del año es: " + semana);

        TiempoAbsoluto ta = new TiempoAbsoluto("2014.1");
        DatosGeneralesEstudio datGen = new DatosGeneralesEstudio();
        datGen.setPasosPorAnio(3);
        System.out.println(FCalendar.fechaIniDeTa(ta, datGen).toString());
        System.out.println(FCalendar.fechaFinDeTa(ta, datGen).toString());

        ta = new TiempoAbsoluto("2014.3");
        datGen.setPasosPorAnio(3);
        System.out.println(FCalendar.fechaIniDeTa(ta, datGen));
        System.out.println(FCalendar.fechaFinDeTa(ta, datGen));
    }
}
