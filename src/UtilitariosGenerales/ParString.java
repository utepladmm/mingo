package UtilitariosGenerales;

public class ParString {
	
	private String st1;
	private String st2;
	
	
	
	public ParString(String st1, String st2) {
		super();
		this.st1 = st1;
		this.st2 = st2;
	}
	public String getSt1() {
		return st1;
	}
	public void setSt1(String st1) {
		this.st1 = st1;
	}
	public String getSt2() {
		return st2;
	}
	public void setSt2(String st2) {
		this.st2 = st2;
	}
	
	
	

}
