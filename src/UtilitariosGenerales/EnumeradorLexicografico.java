/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilitariosGenerales;

import java.util.ArrayList;

import persistenciaMingo.XcargaDatos;

import java.util.ArrayList;

/**
 *
 * @author ut469262
 *
 * Genera vectores de cantDigitos enteros ordenados, en forma sucesiva
 * donde cada vector aparece en orden lexicográfico.
 *
 */

public class EnumeradorLexicografico {


	private String nombre;
	private int cantDigitos;
	private int[] cotasInferiores;
	private int[] cotasSuperiores;
	private int cursor;
	private int[] proximoADevolver;
	private boolean terminado;
	/**
	 * proximoADevolver es el próximo vector que devolverá el enumerador.
	 *
	 * cursor indica la posición del último digito que cambió cuando se creó el vector
	 * proximoADevolver.
     *
     * Las posiciones del cursor varían desde cantDigitos-1 hasta 0
	 * es decir que el orden es por ejemplo:
	 * 1111, 1112, 1113, etc.
	 */


	public EnumeradorLexicografico(int cantDigitos, 
			int[] cotasInferiores, int[] cotasSuperiores) {
		this.cantDigitos = cantDigitos;
		this.cotasInferiores = cotasInferiores;
		this.cotasSuperiores = cotasSuperiores;

		assert (cotasInferiores.length ==cantDigitos) :
			"Error en cantidad de cotas inferiores en enumerador" ;
		assert (cotasSuperiores.length ==cantDigitos) :
			"Error en cantidad de cotas superiores" ;
		proximoADevolver = cotasInferiores.clone();
		cursor = cantDigitos -1;
		terminado = false;

	}


    /**
     * Devuelve el próximo vector y si se terminó la enumeración
     * devuelve null
     *
     * @return vectorDevuelto array de int de dimensión cantDígitos con
     * los valores del enumerador, o null si terminó la enumeración.
     */
	public int[] devuelveVector(){

		if(terminado != true){
			int[] vectorDevuelto = new int[cantDigitos];
			vectorDevuelto = proximoADevolver.clone();
				cursor = cantDigitos - 1;

				while(proximoADevolver[cursor]==cotasSuperiores[cursor]){
					proximoADevolver[cursor]=cotasInferiores[cursor];
					if(cursor == 0){
						terminado = true;
						return vectorDevuelto;
					}
					cursor=cursor-1;
				}
				proximoADevolver[cursor]++;


			return vectorDevuelto;
		}else {
			return null;
		}

	}

	public static void main(String[] args){

		int[] vector ;
		int[] cotasInf = {0, 0, 0, 0};
		int[] cotasSup = {9, 0, 9, 9};
		EnumeradorLexicografico enumerador =
				new EnumeradorLexicografico(4, cotasInf, cotasSup);
		do {
			vector = enumerador.devuelveVector();
			if(vector!= null){
				System.out.print(vector[0]);
				System.out.print("\t");
				System.out.print(vector[1]);
				System.out.print("\t");
				System.out.print(vector[2]);
				System.out.print("\t");
				System.out.print(vector[3]);
				System.out.print("\n");}
		}while(vector != null);		
	}




}
