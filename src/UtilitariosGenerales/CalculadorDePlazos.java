/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilitariosGenerales;

import dominio.DatosGeneralesEstudio;
import dominio.TiempoAbsoluto;
import persistenciaMingo.CargaDatosGenerales;
import persistenciaMingo.XcargaDatos;

import java.io.Serializable;
import java.util.Calendar;

/**
 *
 * @author ut469262
 */
public class CalculadorDePlazos implements Serializable {

	private DatosGeneralesEstudio datGen;

	public CalculadorDePlazos(DatosGeneralesEstudio datGen) {
		this.datGen = datGen;
	}

	

	/**
	 * Calcula la cantidad de pasos de tiempo para pasar
	 * de un TiempoAbsoluto ta1 a otro ta2
	 * cuando los datos generales del estudio son datGEn
	 *
	 * Ejemplo: ta1 = 2008.1, ta2 =2014.3
	 * hay 3 pasos por año, el avance es (2014-2008)*3 + (3-1) = 18 + 2 = 20
	 *
	 * El avance puede ser negativo, si el ta2 es anterior a ta2 o nulo
	 * si coinciden
	 *
	 */
	public int hallaAvanceTiempo(
				TiempoAbsoluto ta1,
				TiempoAbsoluto ta2){
		int avance = 0;
		int ani1 = ta1.getAnio();
		int ani2 = ta2.getAnio();
		if(datGen.getPasosPorAnio()==1){
			avance = ani2 - ani1;
		}else{
			int pda1 = ta1.getParteDelAnio();
			int pda2 = ta2.getParteDelAnio();
			avance = (ani2 - ani1)*datGen.getPasosPorAnio() +
					(pda2 - pda1);
		}
		return avance;
	}


	/**
	 * Encuentra el tiempo absoluto ta que resulta de avanzar n pasos
	 * de tiempo a partir del tiempo absoluto to
     *
     * @param to es el TiempoAbsoluto inicial
     * @param npasos es el avance en pasos de tiempo
     *
	 */
    public TiempoAbsoluto hallaTiempoAbsoluto (
			TiempoAbsoluto to, int npasos){
		TiempoAbsoluto ta = new TiempoAbsoluto();
		int pasosPorAnio = datGen.getPasosPorAnio();
		int aniosAvance = npasos/pasosPorAnio;
		if (pasosPorAnio>1){
			int pasosResto = npasos%pasosPorAnio;
			if(to.getParteDelAnio() + pasosResto > pasosPorAnio){
				ta.setAnio(to.getAnio()+ aniosAvance +1);
				ta.setParteDelAnio(to.getParteDelAnio() + pasosResto
						- pasosPorAnio);
			}else{
				ta.setAnio(to.getAnio()+ aniosAvance);
				ta.setParteDelAnio(to.getParteDelAnio() + pasosResto );
			}
            ta.setInicio(FCalendar.fechaIniDeTa(ta, datGen));
            ta.setFin(FCalendar.fechaFinDeTa(ta, datGen));
			return ta;
		}else{
			ta.setAnio(to.getAnio()+npasos);
			ta.setParteDelAnio(1);
            ta.setInicio(FCalendar.fechaIniDeTa(ta, datGen));
            ta.setFin(FCalendar.fechaFinDeTa(ta, datGen));
			return ta;
		}
	}

    
    /**
     * modifica un paso de tiempo haciéndolo avanzar una cantidad de pasos
     * igual a npasos.
     * @param ta es el TiempoAbsoluto a avanzar.
     * @param npasos en la cantidad de pasos a avanzar.
     */
    public void avanzaTiempoAbsoluto(TiempoAbsoluto ta, int npasos){
        ta = (this).hallaTiempoAbsoluto(ta, npasos);
    }


    /**
     * Devuelve el TiempoAbsoluto al que pertenece el Calendar fecha.
     * Si fecha es el instante inicial de un TiempoAbsoluto pertence al mismo
     * @param fecha
     * @return
     */
    public TiempoAbsoluto tAbsDeCalendar(Calendar fecha){
        TiempoAbsoluto tabs = new TiempoAbsoluto();
        int anio = fecha.get(Calendar.YEAR);
        tabs.setAnio(anio);
        tabs.setParteDelAnio(null);
        int semDelAnio = fecha.get(Calendar.WEEK_OF_YEAR) + 1;
        int pasosPorAnio = datGen.getPasosPorAnio();
        if(pasosPorAnio == 1){
            return tabs;
        }else{
            int parteDelAnio = (Integer)(semDelAnio*pasosPorAnio/52) ;
            tabs.setParteDelAnio(parteDelAnio);
            return tabs;
        }
    }
	

	public static void main(String[] args){
		String texto = "";
        String dirDatGen = "D:/Java/PruebaJava3/V5-DatosGenerales.txt";
		DatosGeneralesEstudio datGen1 = new DatosGeneralesEstudio();

        CargaDatosGenerales cargador = new CargaDatosGenerales() {};
        try{
			TiempoAbsoluto ta1;
			TiempoAbsoluto ta2;
            cargador.cargarDatosGen(dirDatGen, datGen1);
			texto = datGen1.toString();
			System.out.println(texto + "\n" );
			CalculadorDePlazos calc = new CalculadorDePlazos(datGen1);


			int avance = calc.hallaAvanceTiempo(
					ta1 = new TiempoAbsoluto("2010"),
					ta2 = new TiempoAbsoluto("2014")    );
			System.out.print(ta1.toString()+ ta2.toString()
					+ " avance=" + avance+ "\n");
			avance = calc.hallaAvanceTiempo(
					ta1 = new TiempoAbsoluto("2014"),
					ta2 = new TiempoAbsoluto("2010")    );
			System.out.print(ta1.toString()+ ta2.toString()
					+ " avance=" + avance+ "\n");

			datGen1.setPasosPorAnio(3);
			System.out.print("Pasos por anio =" + datGen1.getPasosPorAnio()+ "\n");


			avance = calc.hallaAvanceTiempo(
					ta1 = new TiempoAbsoluto("2010.2"),
					ta2 = new TiempoAbsoluto("2014.3")    );
			System.out.print(ta1.toString()+ ta2.toString()
					+ " avance=" + avance + "\n");
			avance = calc.hallaAvanceTiempo(
					ta1 = new TiempoAbsoluto("2014.3"),
					ta2 = new TiempoAbsoluto("2010.1")    );
			System.out.print(ta1.toString()+ ta2.toString()
					+ " avance=" + avance+ "\n" + "\n");

			avance = 4;
			TiempoAbsoluto ta3 = calc.hallaTiempoAbsoluto(ta1, avance);
			System.out.print("Llamadas a HallaTiempoAbsoluto" + "\n");
			System.out.print("Cantidad de pasos por anio" 
					+ datGen1.getPasosPorAnio()+ "\n");
			System.out.print(ta1.toString() + "avance " + avance + " " + ta3.toString());

			avance = 6;
			TiempoAbsoluto ta4 = calc.hallaTiempoAbsoluto(ta1, avance);
			System.out.print(ta1.toString() + "avance " + avance + " " + ta4.toString());


            Calendar fecha = Calendar.getInstance();
            fecha.set(2012, Calendar.DECEMBER, 31);
            TiempoAbsoluto tabs = calc.tAbsDeCalendar(fecha);
            System.out.print("Resultado de función tAbsDeCalendar " + tabs.toStringCorto());

            System.out.println();

            fecha.set(2012, Calendar.JANUARY, 1);
            tabs = calc.tAbsDeCalendar(fecha);
            System.out.print("Resultado de función tAbsDeCalendar " + tabs.toStringCorto());
			
        }catch(XcargaDatos ex){
            System.out.println("------ERROR-------");
            System.out.println(ex.getDescripcion());
            System.out.println("------------------\n");
		}
	}
}
