/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UtilitariosGenerales;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 * 
 * Objeto que almacena un dato que varía en el tiempo y puede ser aleatorio
 * 
 */
public class DatoT {

    private TipoDato tipo;
    
    /**
     * Las formas posibles son
     * - en fechas hay Calendar con inicio de validez de los datos (FECHA)
     * - fechas está vacío y en datos hay una lista de datos ordenados (LISTA)
     * - fechas está vacío y en datos no hay nada, en cambio se carga el nombreVa de la 
     * variable aleatoria que sigue el dato (ALEAT).
     * - fechas está vacío y en datos hay un único valor fijo (FIJO)
     */
    private FormaDatoT forma;
    private String nombreVA; // nombre de la variable aleatoria si el dato es aleatorio
//    private ArrayList<String> codigo; // almacena los códigos del cambio aleatorio
//    private String pattern = "yyyy/MM/dd";
    /**
     * Es el patrón en el que se leen las fechas y tiempos
     * puede cambiarse si se quiere según las reglas de SimpleDateFormat
     */
    private ArrayList<Calendar> fechas;  // los inicios de validez de cada dato
    private ArrayList<Object> datos;    // los datos

    /**
     * ATENCION: 
     * El Java recomienda usar la clase Calendar para hacer los cálculos internos con tiempo
     * Si embargo para leer Strings y transformarlos a Calendar hay que usar las clases Date y 
     * SimpleDateFormat (a pesar de que los métodos de Date para hacer los cálculos están deprecated).
     * 
     * ATENCIÓN: Los números de los meses en Calendar van de 0 a 11 !!!!
     * 
     */
    public DatoT(TipoDato tipo) {
        this.tipo = tipo;
        fechas = new ArrayList<Calendar>();
        datos = new ArrayList<Object>();
    }

    public DatoT(TipoDato tipo, String nombreVA) {
        this.tipo = tipo;
        this.nombreVA = nombreVA;
    }

    /**
     * La forma del DatoT
     */
    public enum FormaDatoT{
        FIJO,  // fechas está vacío y hay un único dato en datos que vale siempre
        FECHA, // fechas tiene Calendar que se interpretan como el inicio de un dato
        ALEAT, // fechas y datos están vacíos y en cambio nombreVA es el nombre de una variable aleatoria 
        LISTA  // hay una lista ordenada en datos y no importan las fechas
    }
    
    
    /**
     * Los tipos de dato que puede almacenar un objeto DatoT con valores
     */
    public enum TipoDato {
        STRING,  // los datos son objetos Strings
        INT,     // los datos son objetos Integer
        DOUBLE   // los datos on objetos Double
    }
    /**
     * Los tipos de funciones a aplicar a los datos de un DatoT en un período.
     */
    public enum TipoFuncDatoT{
        MIN, PROM
    }
    
    public enum TipoOperDatoT{
        PROD
    }

    public ArrayList<Object> getDatos() {
        return datos;
    }

    public void setDatos(ArrayList<Object> datos) {
        this.datos = datos;
    }

    public ArrayList<Calendar> getFechas() {
        return fechas;
    }

    public void setFechas(ArrayList<Calendar> fechas) {
        this.fechas = fechas;
    }

    public String getNombreVA() {
        return nombreVA;
    }

    public void setNombreVA(String nombreVA) {
        this.nombreVA = nombreVA;
    }

    public TipoDato getTipo() {
        return tipo;
    }

    public void setTipo(TipoDato tipo) {
        this.tipo = tipo;
    }
    
    

    /**
     * Recibe una lista de Strings con el código para cargar los datos y carga los 
     * atributos fechas y datos y lo interpreta de acuerdo al valor del atributo tipo,
     * que debe estar cargado previamente con un TipoDato.
     * 
     * @param texto es una lista de Strings que puede tener cuatro formas posibles correspondientes
     * a los valores del atributo forma.
     * - forma FECHA: lista de Strings donde cada String tiene la forma "fecha:dato"
     * todo sin blancos separadores
     * y la fecha tiene la forma "yyyy/mm/dd";
     * - forma ALEAT: un único String con "VALEAT:nombre" deonde nombre es el nombre de una variable aleatoria que no puede tener sólo dígitos
     * - forma LISTA: una lista de Strings con más de un String, donde cada String tiene un valor numérico y los valores se interpretan 
     * como los correspondientes a cada tiempo absoluto a partir del tiempoAbsolutoBase del RecursoBase.
     * - forma FIJO: un único String que no comience con "VALEAT:" y no tiene formato "fecha:dato", con el valor fijo que permanece constante
     */
    public void carga(ArrayList<String> texto) throws XcargaDatos {
        //            111........
        //  0123456789012........
        //  yyyy/mm/dd: 
        //  VALEAT:
        String st1 = texto.get(0);
        if(esDatoTFecha(st1)){
            // forma FECHA
            forma = FormaDatoT.FECHA;            
            for (String st : texto) {
                if (st.substring(10, 11).equalsIgnoreCase(":")) {
                    String fecha = st.substring(0, 10);
                    String dato = st.substring(11);
                    Calendar cal = Fechas.calDeTextoF(fecha);
                    fechas.add(cal);
                    if (tipo.equals(TipoDato.STRING)) {
                        datos.add(dato);
                    } else if (tipo.equals(TipoDato.DOUBLE)) {
                        datos.add(Double.parseDouble(dato));
                    } else if (tipo.equals(TipoDato.INT)) {
                        datos.add(Integer.parseInt(dato));
                    } else {
                        throw new XcargaDatos("Error en la lectura del dato " + st);
                    }
                } else {
                    throw new XcargaDatos("Error en la lectura del dato " + st);
                }
            }
        }else if(texto.size()>1){
            // forma LISTA
            forma = FormaDatoT.LISTA; 
            if(tipo.equals(TipoDato.STRING)){
                datos.addAll(texto);
            }else if(tipo.equals(TipoDato.INT)){
                for(int i = 0; i<texto.size(); i++){
                    datos.add(Integer.parseInt(texto.get(i)));
                }
            }else{
                // es TipoDato igual a DOUBLE
                for(int i = 0; i<texto.size(); i++){
                    datos.add(Double.parseDouble(texto.get(i)));
                }                
            }    
        }else if(texto.get(0).length()>7 && texto.get(0).substring(0,7).equalsIgnoreCase("VALEAT:")){
            // forma ALEAT
            forma = FormaDatoT.ALEAT;
            nombreVA = texto.get(0).substring(7);
        }else{
            // forma FIJO
            forma = FormaDatoT.FIJO;
            datos.add(texto.get(0));
        }        
    }

    /**
     * Devuelve el objeto de la fecha representada por el Calendar cal
     * Si cal es anterior a la fecha del primer Calendar del DatoT devuelve
     * cero si es tipo DOUBLE o INT, y el string "NULL" si es tipo STRING.
     * @param cal
     * @return 
     */
    public Object datoDeCal(Calendar cal) throws XcargaDatos {
        if(forma!=FormaDatoT.FECHA && forma!=FormaDatoT.FECHA) throw new XcargaDatos("Se pidio datoDeCal a un DatoT que es de forma " + forma);
        if (cal.before(fechas.get(0))) {
            if (tipo.equals(TipoDato.INT)) {
                return 0;
            }
            if (tipo.equals(TipoDato.DOUBLE)) {
                return 0.0;
            }
            if (tipo.equals(TipoDato.STRING)) {
                return "NULL";
            }
        } else {
            int ind = 0;
            while (ind < fechas.size() && !cal.before(fechas.get(ind))) {
                ind++;
            }
            return datos.get(ind - 1);
        }
        return null;    // nunca pasa por aca
    }
    

    /**
     * Devuelve el objeto de la fecha representada por el String fecha
     * de formato "yyyy/mm/dd" (meses de 1 a 12)
     * Si cal es anterior a la fecha del primer Calendar del DatoT devuelve
     * cero si es tipo DOUBLE o INT, y el string "NULL" si es tipo STRING.
     * @param cal
     * @return 
     */
    public Object datoDeFechaF(String fecha) throws XcargaDatos {
        Calendar cal = Calendar.getInstance();
        cal = Fechas.calDeTextoF(fecha);
        return datoDeCal(cal);
    }
    

    /**
     * Devuelve una función de la variable en el período que empieza en calini
     * y termina inmediatemente antes de calfin.
     * Se supone nulo el valor del dato antes del primer Calendar del DatoT
     * @param calini
     * @param calfin
     * @param funcion es el tipo de función a considerar en el período:
     * - MIN mínimo de la función en el período
     * - PROM promedio en el tiempo de la función en el período
     * @return 
     */
    public double funcion(Calendar calini, Calendar calfin, TipoFuncDatoT func) throws XcargaDatos {
        if (tipo.equals(TipoDato.STRING)) {
            throw new XcargaDatos("Se pidió a un DatoT el promedio de un tipo de dato STRING");
        }
        if (calfin.before(calini)) throw new XcargaDatos("se pidio promedio con calini posterior al calfin");
        /*
         * Se van acumulando en prom los promedios ponderados por duraciones de tiempo en horas
         * para eso los tiempos en milisegundos se dividen por 1000*3600
         */
        double prom = 0.0;
        double dato = 0.0;
        double min = Double.MAX_VALUE;
        long lapsoEnHoras = (calfin.getTimeInMillis()-calini.getTimeInMillis())/3600000;
        int ind = 0;
        Calendar ci = Calendar.getInstance();   // ci instante inicial corriente
        ci = calini;
        // avanza ind en fechas tratando de superar calini
        while (ind < fechas.size() && !calini.before(fechas.get(ind))) {
            ind++;
        }
        if(ind==fechas.size()){
            // calini excede la fecha última de fechas
            prom = min = (Double)datos.get(datos.size()-1);
        }else{
            // hay alguna fecha en fechas posterior a calini
            while (ind < fechas.size() ){
                if(calfin.before(fechas.get(ind))){
                    dato = (Double)datos.get(ind-1);
                    if(func == TipoFuncDatoT.PROM){
                        prom += dato*(calfin.getTimeInMillis()-ci.getTimeInMillis())/3600000;
                        return prom/lapsoEnHoras;
                    }
                    if(func == TipoFuncDatoT.MIN){
                        if(dato<min) min = dato;
                        return min;
                    }
                }else{                    
                    if(ind==0){
                        // la primera fecha ya es superior a calini
                        dato = 0;
                    }else{
                        dato = (Double)datos.get(ind-1);
                    }
                    prom += dato*(fechas.get(ind).getTimeInMillis()-ci.getTimeInMillis())/3600000;
                    if(dato<min) min = dato;                    
                    ci = fechas.get(ind);
                    if(ind == fechas.size()-1 ){
                        // se procesó la última fecha, hay que agregar el lapso hasta cfin
                        prom += (Double)datos.get(ind)*(calfin.getTimeInMillis()-fechas.get(ind).getTimeInMillis())/3600000; 
                        if((Double)datos.get(ind)<min) min = (Double)datos.get(ind);                        
                    }                    
                    ind ++;                    
                }               
            }    
        }
        if(func == TipoFuncDatoT.PROM) return prom/lapsoEnHoras;     
        if(func == TipoFuncDatoT.MIN) return min;  
        return 0.0; // Esta sentencia no debe alcanzarse nunca
    }
    
    
    
    /**
     * Encuentra un DatoT que tiene la unión de las fechas de d1 y d2
     * y cuyos datos en cada período resultante son una operación entre los 
     * datos de d1 y d2.
     * d1 y d2 deben ser de TipoDato numérico 
     * No altera d1 y d2
     * @param d1
     * @param d2 
     */
    public static DatoT opera2DatoT(DatoT d1, DatoT d2, TipoOperDatoT oper) throws XcargaDatos{
        
        ArrayList<Calendar> unionC = new ArrayList<Calendar>();
        ArrayList<Object> datosUnion = new ArrayList<Object>();
        DatoT unionDT = new DatoT(TipoDato.DOUBLE);
        unionDT.setDatos(datosUnion);
        unionDT.setFechas(unionC);
        unionC.addAll(d1.fechas);
        unionC.addAll(d2.fechas);        
        Collections.sort(unionC);
        for(Calendar cu: unionC){
            double num1 = (Double)d1.datoDeCal(cu);
            double num2 = (Double)d2.datoDeCal(cu);
            if(oper == TipoOperDatoT.PROD){
                double datocu = num1*num2;
                datosUnion.add(datocu);
            }else{
                throw new XcargaDatos("Se pidió un tipo de dato inexistente en opera2DatoT");
            }
            
        }
        return unionDT;             
    }
    
    
    /**
     * Devuelve la primera fecha en el atributo fechas estrictamente posterior a fechaIni o null si no hay ninguna
     */
    public Calendar primeraPosterior(Calendar fechaIni){
        int ind = 0;
        while(ind<fechas.size() && !fechas.get(ind).after(fechaIni)){
            ind ++;
        }
        if(ind == fechas.size()) return null;
        return fechas.get(ind);        
    }
    
    
    /**
     * Reconoce si un String corresponde en principio a un DatoT del tipo FECHA, porque tiene "/" y ":" en las
     * posiciones adecuadas
     *           1
     * 01234567890
     * yyyy/mm/dd:
     */
    public static boolean esDatoTFecha(String st){
        if(st.length() < 11) return false;
        if(!st.substring(4,5).equalsIgnoreCase("/") || !st.substring(7,8).equalsIgnoreCase("/") 
                || !st.substring(10,11).equalsIgnoreCase(":")) return false;
        return true;        
    }
    
    /**
     * Crea un ArrayList<String> uno por cada tiempo absoluto que representa con la mayor aproximación
     * el DatoT.
     * El primer T
     * 
     * @param 
     * @return 
     */
//    public ArrayList<String> creaListaPorTA(){
//        ArrayList<String> listaPorTa = new ArrayList<String>();
//      OJO HAY UN LIO CON TIEMPO ABSOLUTO BASE  
//    
//    }
    

    @Override
    public String toString() {
        String texto = "";
        texto += "DatoT FORMA: " + forma + "- ";
        if(forma==FormaDatoT.ALEAT){
            texto += "Variable " + nombreVA;          
        }else if (forma==FormaDatoT.FIJO){
            texto += "Dato fijo " + datos.get(0);
        }else{
            for (int i = 0; i < datos.size(); i++) {
                if(forma == FormaDatoT.FECHA){
                    texto += Fechas.textoDeCalF(fechas.get(i));
                    texto += ":";
                }
                texto += datos.get(i).toString() + "   ";            
            }
        }
        return texto;
    }

    public static void main(String[] args) throws XcargaDatos {

        ArrayList<String> texto = new ArrayList<String>();
        String texto1 = "2013/11/23:125.5";
        System.out.println(esDatoTFecha(texto1));
        String texto2 = "2014/07/01:200.5";
        String texto3 = "2014/10/01:90.5";
        texto.add(texto1);
        texto.add(texto2);
        texto.add(texto3);
        DatoT cosas = new DatoT(TipoDato.DOUBLE);
        cosas.carga(texto);
        System.out.println(cosas);

        String fecha1 = "2012/10/01";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));
        double suma = (Double) cosas.datoDeFechaF(fecha1) + 1;
        System.out.println(suma);
        fecha1 = "2013/11/23";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));
        fecha1 = "2014/07/01";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));
        fecha1 = "2014/08/01";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));
        fecha1 = "2014/10/01";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));
        fecha1 = "2016/10/01";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));

        texto = new ArrayList<String>();
        texto1 = "2013/11/23:perro";
        texto2 = "2014/07/01:OJO";
        texto3 = "2014/10/01:123";
        texto.add(texto1);
        texto.add(texto2);
        texto.add(texto3);
        cosas = new DatoT(TipoDato.STRING);
        cosas.carga(texto);
        System.out.println(cosas);

        texto = new ArrayList<String>();
        texto1 = "2013/11/23:50";
        texto2 = "2014/07/01:100";
        texto3 = "2014/10/01:200";
        texto.add(texto1);
        texto.add(texto2);
        texto.add(texto3);
        cosas = new DatoT(TipoDato.INT);
        cosas.carga(texto);
        System.out.println(cosas);

        fecha1 = "2012/10/01";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));
        int sumaInt = (Integer) cosas.datoDeFechaF(fecha1) + 1;
        System.out.println(sumaInt);
        fecha1 = "2013/11/23";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));
        fecha1 = "2014/07/01";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));
        fecha1 = "2014/08/01";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));
        fecha1 = "2014/10/01";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));
        fecha1 = "2016/10/01";
        System.out.println(fecha1 + "-" + cosas.datoDeFechaF(fecha1));
        
        texto = new ArrayList<String>();        
        texto1 = "2013/01/01:1";
        texto2 = "2014/01/01:2";
        texto3 = "2015/01/01:3";
        texto.add(texto1);
        texto.add(texto2);
        texto.add(texto3);
        cosas = new DatoT(TipoDato.DOUBLE);
        cosas.carga(texto);
        System.out.println(cosas);        
        Calendar cini = Fechas.calDeTextoF("2012/12/01");
        Calendar cfin = Fechas.calDeTextoF("2014/01/04");
        System.out.println("cini " + Fechas.textoDeCalF(cini) + " - cfin " + Fechas.textoDeCalF(cfin));
        double promedio = cosas.funcion(cini, cfin, TipoFuncDatoT.PROM);
        double minimo = cosas.funcion(cini, cfin, TipoFuncDatoT.MIN);        
        System.out.println("promedio = " + promedio + " - mínimo = " + minimo);        
        Calendar cals = cosas.primeraPosterior(Fechas.calDeTextoF("2016/01/01"));
        if (cals!=null) System.out.println("Primera posterior " + Fechas.textoDeCalF(cals));
        if (cals==null) System.out.println("No hay fecha posterior a la dada ");

        
        ArrayList<String> textoB = new ArrayList<String>();        
        String texto1B = "2013/06/01:5";
        String texto2B = "2014/06/01:3";

        textoB.add(texto1B);
        textoB.add(texto2B);
        DatoT cosasB = new DatoT(TipoDato.DOUBLE);
        cosasB.carga(textoB);  
        
        DatoT prodDatosT = DatoT.opera2DatoT(cosas, cosasB, TipoOperDatoT.PROD);
        System.out.println("Primer DatoT " + cosas.toString());
        System.out.println("Segundo DatoT " + cosasB.toString());        
        System.out.println("Producto de dos DatoT " + prodDatosT.toString());
        
        ArrayList<String> textoC = new ArrayList<String>();        
        String texto1C = "150.3";
        String texto2C = "13";

        textoC.add(texto1C);
        textoC.add(texto2C);
        DatoT cosasC = new DatoT(TipoDato.DOUBLE);
        cosasC.carga(textoC);  
        System.out.println(cosasC.toString()); 

        ArrayList<String> textoD = new ArrayList<String>();  
        textoD.add("VALEAT:VARALEAT");
        DatoT cosasD = new DatoT(TipoDato.DOUBLE);        
        cosasD.carga(textoD);
        System.out.println(cosasD.toString());  
        
        ArrayList<String> textoE = new ArrayList<String>();          
        textoE.add("153.1");
        DatoT cosasE = new DatoT(TipoDato.DOUBLE);
        cosasE.carga(textoE);
        System.out.println(cosasE.toString());          

    }
}
