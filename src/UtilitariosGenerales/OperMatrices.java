/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilitariosGenerales;

import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class OperMatrices {

    public static double[][] productoMatRealAB(double[][] a, double[][] b) throws XcargaDatos{
        // el primer get son filas y el segundo columnas
        int nfilA = a.length;
        int ncolA = a[0].length;
        int nfilB = b.length;
        int ncolB = b[0].length;
        if (ncolA != ncolB) throw new XcargaDatos("en el producto matricial las" +
                "dimensiones no cuadran");
        double[][] prod = new double [nfilA][ncolB];
        for (int i=1; i<= nfilA; i++){

            for(int j=1; j<ncolB; j++){
                double suma = 0.0;
                for(int k = 1; k<= ncolA; k++){
                    suma += a[i-1][k-1]*b[k-1][j-1];
                }
                prod[i-1][j-1] = suma;
            }
        }
        return prod;
    }

    public static int cantFilasMatReal(double[][] mat){
        return mat.length;
    }

    public static int cantColMatReal(double[][] mat){
        return mat[0].length;
    }

    public static double[][] copiaMatReal(double[][] mat){
        int nfil = cantFilasMatReal(mat);
        int ncol = cantColMatReal(mat);
        double[][] copia = new double[nfil][ncol];
        for(int i=1; i<= nfil; i++){
            for(int j=1; j<= ncol; i++){
                copia[i-1][j-1] = mat[i-1][j-1];
            }
        }
        return copia;

    }

    public static boolean[][] productoMatBoolAB(boolean[][] a, boolean[][] b) throws XcargaDatos{
        // el primer get son filas y el segundo columnas
        int nfilA = a.length;
        int ncolA = a[0].length;
        int nfilB = b.length;
        int ncolB = b[0].length;
        if (ncolA != ncolB) throw new XcargaDatos("en el producto matricial las" +
                "dimensiones no cuadran");
        boolean[][] prod = new boolean [nfilA][ncolB];
        for (int i=1; i<= nfilA; i++){

            for(int j=1; j<ncolB; j++){
                boolean suma = false;
                for(int k = 1; k<= ncolA; k++){
                    if(a[i-1][k-1]== true & b[k-1][j-1]==true){
                        suma = true;
                        break;
                    }
                }
                prod[i-1][j-1] = suma;
            }
        }
        return prod;
    }

    public static int cantFilasMatBool(boolean[][] mat){
        return mat.length;
    }

    public static int cantColMatBool(boolean[][] mat){
        return mat[0].length;
    }

    public static boolean[][] copiaMatBool(boolean[][] mat){
        int nfil = cantFilasMatBool(mat);
        int ncol = cantColMatBool(mat);
        boolean[][] copia = new boolean[nfil][ncol];
        for(int i=1; i<= nfil; i++){
            for(int j=1; j<= ncol; i++){
                copia[i-1][j-1] = mat[i-1][j-1];
            }
        }
        return copia;

    }

}
