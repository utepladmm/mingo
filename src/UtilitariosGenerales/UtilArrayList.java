/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilitariosGenerales;

import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class UtilArrayList {

    /**
     * Crea una copia de un ArrayList<ArrayList<String>> que puede modificarse
     * sin alterar el originial
     * @param original
     * @return copia
     */
    public static ArrayList<ArrayList<String>> copiaALString2D(ArrayList<ArrayList<String>> original){
        ArrayList<ArrayList<String>> copia = new ArrayList<ArrayList<String>>();
        for (int i=0; i<original.size(); i++){
            ArrayList<String> aux = new ArrayList<String>();
            for(int j=0; j<original.get(i).size(); j++){
                aux.add(original.get(i).get(j));
            }
            copia.add(aux);
        }
        return copia;
    }

}
