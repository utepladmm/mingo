/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UtilitariosGenerales;

import java.util.Calendar;

import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 * 
 * Hace operaciones con fechas
 */
public abstract class Fechas {

    /**
     * Devuelve un texto con el formato yyyy/mm/dd a partir de un Calendar
     * El mes del texto producido va entre 1 y 12
     * Los campos de mes y día se completan con ceros: por ejemplo el mes 1 queda 01
     */
    public static String textoDeCalF(Calendar cal) {
        String fecha = "";
        String stAnio = "";
        String stMes = "";
        String stDia = "";
        int anio = cal.get(Calendar.YEAR);
        stAnio = String.valueOf(anio);

        int mes = cal.get(Calendar.MONTH) + 1;
        stMes = String.valueOf(mes);
        if(mes<10) stMes = "0" + stMes;        

        int dia = cal.get(Calendar.DAY_OF_MONTH);
        stDia = String.valueOf(dia);
        if(dia<10) stDia = "0" + stDia;                
        
        fecha = stAnio + "/" + stMes + "/" + stDia;
        return fecha;
    }

    /**
     * Devuelve un Calendar generado a partir del String texto que tiene una fecha
     * con el patrón "yyyy/mm/dd"
     * El mes va entre 1 y 12
     * @param texto
     * @return 
     */
    public static Calendar calDeTextoF(String texto) throws XcargaDatos {
        
        Calendar cal = Calendar.getInstance();        
        //  0123456789
        //  yyyy/mm/dd
        int anio, mes, dia;
        if(texto.length()!= 10) throw new XcargaDatos("ERROR EN FECHA " + texto);
        if (texto.substring(4, 5).equalsIgnoreCase("/") && texto.substring(7,8).equalsIgnoreCase("/")) {
            anio = Integer.parseInt(texto.substring(0, 4));
            mes = Integer.parseInt(texto.substring(5, 7));
            dia = Integer.parseInt(texto.substring(8, 10));
        } else {
            throw new XcargaDatos("ERROR EN FECHA " + texto);
        }
        cal.setLenient(false);
        if(mes<0 || mes>12 || dia > 31) throw new XcargaDatos("ERROR EN FECHA " + texto);
        if( (mes==4 || mes==6 || mes==9 || mes==11) & dia>30) throw new XcargaDatos("ERROR EN FECHA " + texto);
        if( mes==2 & dia>29) throw new XcargaDatos("error en fecha " + texto);
        if( mes==2 & dia>28 & !(Math.IEEEremainder(anio, 4)==0)) throw new XcargaDatos("ERROR EN FECHA " + texto);
        cal.set(anio, mes-1, dia);
        return cal;
    }

    
    
    public static void main(String[] args) throws XcargaDatos {
        String textoFecha = "2012/02/33";
        Calendar cal = calDeTextoF(textoFecha);
        cal.add(Calendar.DAY_OF_MONTH, 30);
        String salida = textoDeCalF(cal);
        System.out.println(salida);
    }
}
