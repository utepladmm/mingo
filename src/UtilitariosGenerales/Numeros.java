/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilitariosGenerales;

/**
 *
 * @author ut469262
 */
public class Numeros {

    /**
     * Devuelve un String con el real representado por el String numero
     * con decimales cifras después del punto decimal.
     * @param numero es String con un número con o sin punto decimal
     * @param decimales es la cantidad de decimales que se quiere mostrar
     * @return
     */
    public static String redondeaString(String numero, int decimales){

        int largo = numero.length();
        int posicionDec = numero.indexOf(".");
        if(posicionDec<0){
            // no hay punto decimal
            numero += ".";
            for(int id = 1; id<= decimales; id++){
                numero += "0";
            }
        }else{
            int decimalesEnNum = largo - posicionDec - 1;
            if(decimalesEnNum < decimales){
                for(int id = 1; id<= decimales-decimalesEnNum; id++){
                    numero += "0";
                }
            }else{
                numero = numero.substring(0, posicionDec + decimales + 1);
            }
        }

        return numero;


    }



    /**
     * Devuelve un String con el real representado por el String numero
     * con decimales cifras después del punto decimal.
     * @param numero es String con un número con o sin punto decimal
     * @param decimales es la cantidad de decimales que se quiere mostrar
     * @return
     */
    public static String redondeaDouble(double numero, int decimales){
        String texto = Double.toString(numero);
        texto = redondeaString(texto, decimales);
        return texto;
    }


    public static void main(String[] args){
        String num = "234.459";
        String numRed = redondeaString(num, 2);
        System.out.println(numRed);

        num = "234";
        numRed = redondeaString(num, 2);
        System.out.println(numRed);

        num = "234.";
        numRed = redondeaString(num, 2);
        System.out.println(numRed);

        double numero = 345.12345678;
        numRed = redondeaDouble(numero, 2);
        System.out.println(numRed);

        numero = 345.0;
        numRed = redondeaDouble(numero, 2);
        System.out.println(numRed);

    }

}
