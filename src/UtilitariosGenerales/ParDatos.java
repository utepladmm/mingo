/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilitariosGenerales;

/**
 *
 * @author ut469262
 */
public class ParDatos implements Comparable{
    double dato;
    Object dato2;

    public double getDato() {
        return dato;
    }

    public void setDato(double dato) {
        this.dato = dato;
    }

    public Object getDato2() {
        return dato2;
    }

    public void setDato2(Object dato2) {
        this.dato2 = dato2;
    }

    public ParDatos(double dato, Object dato2) {
        this.dato = dato;
        this.dato2 = dato2;
    }

    public int compareTo(Object o) {
        ParDatos op = (ParDatos)o;
        if(dato>op.getDato())return 1;
        if(dato<op.getDato())return -1;
        return 0;
    }



}
