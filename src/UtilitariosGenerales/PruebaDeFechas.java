/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilitariosGenerales;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author ut469262
 */
public class PruebaDeFechas {
    public static void main(String[] args){
    
        Date today;
        String dateOut;
        DateFormat dateFormatter;

        dateFormatter = DateFormat.getDateInstance();
        today = new Date();
        dateOut = dateFormatter.format(today);

        System.out.println(dateOut + " " );
        int anio = today.getYear();
        int mes = today.getMonth();
        int dia = today.getDay();
        int ordinalDia = today.getDate();

        System.out.println("Año es" + 1900 + anio);
        System.out.println("Mes es" + mes);
        System.out.println("Día es" + dia);
        System.out.println("Ordinal del Día es" + ordinalDia);

        Calendar ahora = Calendar.getInstance();
        int anioAhora = ahora.get(Calendar.YEAR);
        int diaDelAnio = ahora.get(Calendar.DAY_OF_YEAR);
        int semanaDelAnio = ahora.get(Calendar.WEEK_OF_YEAR);
        System.out.println("Año es " + anioAhora);
        System.out.println("Dia del año es " + diaDelAnio);
        System.out.println("Semana del año es " + semanaDelAnio);


        Calendar otraFecha = Calendar.getInstance();
        otraFecha.set(2011, 2, 28);
        int anioOtraFecha = ahora.get(Calendar.YEAR);
        diaDelAnio = otraFecha.get(Calendar.DAY_OF_YEAR);
        semanaDelAnio = otraFecha.get(Calendar.WEEK_OF_YEAR);
        System.out.println("Año es " + anioOtraFecha);
        System.out.println("Dia del año es " + diaDelAnio);
        System.out.println("Semana del año es " + semanaDelAnio);



    }



}
