/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilitariosGenerales;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class OpcionDetener {



    public void opcionDet(String s) throws IOException, XcargaDatos{
    /**
     * Imprime el mensaje del string s y requiere la entrada de un carácter
     * por consola. Si el carácter es "C" continúa la ejecución. De lo contrario
     * lanza una excepción para detener la ejecución.
     */

    InputStreamReader ir = new InputStreamReader(System.in);
    BufferedReader in = new BufferedReader(ir);
    System.out.print(s);
    System.out.println("Para continuar pulse C y luego ENTER");
    System.out.print("Para detener la ejecución pulse cualquier otra tecla y luego ENTER" + "\n");
    try{
        s = in.readLine();
        if (! (s.equalsIgnoreCase("C"))) throw new XcargaDatos("Hay un error en el archivo de restricciones heurísticas.");
    }catch(IOException e){
        e.printStackTrace();
    }
    }
}
