package UtilitariosGenerales;

public class DevuelveString {
	
	public static String stringDeObjeto(Object obj, String etiIni, String etiFin){
		String result;
		if(obj instanceof Integer){
			int entero = (Integer)obj;
			result = etiIni + String.valueOf(entero) + etiFin;
		}else if(obj instanceof Double){
			double doble = (Double)obj;
			result = etiIni + String.valueOf(doble) + etiFin;
		}else{
			result = (String)obj;
		}
		return result;
	}

}