/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilitariosGenerales;

import AlmacenSimulaciones.ImplementadorGeneral;
import GrafoEstados.PeriodoDeTiempo;
import ImplementaEDF.ImplementadorEDF;
import dominio.Estudio;
import dominio.TiempoAbsoluto;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import naturaleza3.VariableNat;
import persistenciaMingo.XcargaDatos;

/**
 * CLASE DatosAleatoriosT
 * 
 * Almacena objetos de evolucionan en el tiempo y según el valor de una variable
 * aleatoria
 *
 * @author ut469262
 */
public class DatosEnT {
    private TipoDatoAleatT tipoDato;  // por ejemplo entero, real, objeto, string
    private TipoTiempo tipoT;
    private boolean esAleatorio;
    /**
     * El dato puede ser determinístico, en ese caso esAleatorio es false
     * la variable varNat es null
     */
    private VariableNat varNat;
    private ArrayList<ArrayList<Object>> valores;
    /**
     * El primer índice se corresponde con cada uno de los valores posibles de
     * la variable de la naturaleza varNat
     * El segundo índice se corresponde con instantes o períodos de tiempo según
     * sea tipoT.
     */
    private ArrayList<ArrayList<Object>> tiempos;    
    /**
     * El primer índice se corresponde con cada uno de los valores posibles de
     * la variable de la naturaleza varNat
     * El segundo índice se corresponde con un período de tiempo o un Date de Java, según
     * el valor de tipoT. Corresponde a períodos o instantes que son distintos
     * para cada uno de los valores de las variables de la naturaleza
     *
     * Si TipoT es Date se interpreta que el tiempos.get(i) es el último Date
     * en el que vale el objeto de valores.get(i).
     * En particular el objeto valores.get(0) vale hasta el Date tiempos.get(0) inclusive
     *
     * valores y tiempos tienen dimensiones idénticas en los dos niveles
     */



    public DatosEnT(TipoDatoAleatT tipoDato, TipoTiempo tipoT,
            VariableNat varNat, boolean esAleatorio) {
        this.tipoDato = tipoDato;
        this.tipoT = tipoT;
        this.varNat = varNat;
        this.esAleatorio = esAleatorio;
        int cantValoresVN = varNat.getValores().size();

        if(!esAleatorio){
            cantValoresVN = 1;
            varNat = null;
        }

        valores = new ArrayList<ArrayList<Object>>();
        tiempos = new ArrayList<ArrayList<Object>>();
        for(int j=0; j < varNat.getValores().size(); j++){
            valores.add(new ArrayList<Object>());
            tiempos.add(new ArrayList<Object>());
        }
    }

    
    

    
    
    
    

    public enum TipoTiempo{
        PERIODO_DE_TIEMPO,    // La clase de GrafoEstados
        EDAD,                 // Edad en pasos de tiempo
        INSTANTE;             // La clase de Java que representa instantes
    }

    public enum TipoDatoAleatT{
        ENTERO,
        REAL,
        STRING,
        OBJETO;
    }



    public ArrayList<ArrayList<Object>> getTiempo() {
        return tiempos;
    }

    public void setTiempo(ArrayList<ArrayList<Object>> tiempo) {
        this.tiempos = tiempo;
    }

    public boolean isEsAleatorio() {
        return esAleatorio;
    }

    public void setEsAleatorio(boolean esAleatorio) {
        this.esAleatorio = esAleatorio;
    }

    public ArrayList<ArrayList<Object>> getTiempos() {
        return tiempos;
    }

    public void setTiempos(ArrayList<ArrayList<Object>> tiempos) {
        this.tiempos = tiempos;
    }


    public TipoDatoAleatT getTipoDato() {
        return tipoDato;
    }

    public void setTipoDato(TipoDatoAleatT tipoDato) {
        this.tipoDato = tipoDato;
    }

    public TipoTiempo getTipoT() {
        return tipoT;
    }

    public void setTipoT(TipoTiempo tipoT) {
        this.tipoT = tipoT;
    }

    public ArrayList<ArrayList<Object>> getValores() {
        return valores;
    }

    public void setValores(ArrayList<ArrayList<Object>> valores) {
        this.valores = valores;
    }

    public VariableNat getVarNat() {
        return varNat;
    }

    public void setVarNat(VariableNat varNat) {
        this.varNat = varNat;
    }


    /**
     * METODO cargaDatosUnValorVN
     *
     * carga las listas de valores y tiempos asociadas a un valor de la
     * variable aleatoria
     *
     * @param valorVN es el valor de la variable aleatoria
     * @param objetos es la lista de objetos a cargar
     * @param tiemp es la lista de períodos de tiempos o Dates
     */
     public void cargaDatosUnValorVN(String valorVN, ArrayList<Object> val,
             ArrayList<Object> tiemp) throws XcargaDatos{

         int indiceValorVN;
         if(esAleatorio){
             indiceValorVN = varNat.getValores().indexOf(valorVN);
             if(indiceValorVN == -1) throw new XcargaDatos("Se trató de cargar" +
                     " un valor en un DatosAleatoriosT con valorVN " + valorVN +
                     " que no existe para la variable aleatoria " + varNat.getNombre());
         }else{
             indiceValorVN = 1;
         }
         if(val.size()!= tiemp.size())throw new XcargaDatos("Se trató de cargar" +
                     " objetos y tiempos con dimensiones diferentes para " + valorVN +
                     " de la variable aleatoria " + varNat.getNombre());

         valores.get(indiceValorVN).addAll(val);
         tiempos.get(indiceValorVN).addAll(tiemp);
     }

    /**
     * METODO cargaDatosUnValorVN
     *
     * carga las listas de valores y tiempos asociadas a un valor de la
     * variable aleatoria
     *
     * @param est es el Estudio del que se trata
     * @param valorVN es el valor de la variable aleatoria
     * @param objetos es la lista de objetos a cargar
     * @param tiemp1 es el primer TiempoAbsoluto a cargar
     * @param tiemp2 es el ultimo TiempoAbsoluto a cargar
     * Los pasos de tiempo entre tiemp1 y tiemp2 inclusive deben ser tantos
     * como la cantidad de objetos
     */
    public void cargaDatosUnValorVN(Estudio est, String valorVN, ArrayList<Object> val,
             TiempoAbsoluto tiemp1, TiempoAbsoluto tiemp2) throws XcargaDatos{

        CalculadorDePlazos calcP = new CalculadorDePlazos(est.getDatosGenerales());
        int cantidad = calcP.hallaAvanceTiempo(tiemp1, tiemp2) + 1;
        if(cantidad!=val.size())throw new XcargaDatos("Error al cargar un DatosEnT");
        ArrayList<Object> auxTiemp = new ArrayList<Object>();
        for(int j=0; j<cantidad; j++){
            TiempoAbsoluto ta = calcP.hallaTiempoAbsoluto(tiemp1, j);
            auxTiemp.add(ta);
        }
        cargaDatosUnValorVN(valorVN, val, auxTiemp);
    }





    /**
     * METODO valorDato
     * Devuelve un Object con el valor del dato según el valor de la
     * variable aleatoria y el valor de pt
     *
     * @param valorVN valor de la variable aleatoria.Es irrelevante si el
     * dato no es aleatorio.
     *
     * @param pt períodoDeTiempo para el que se quiere la variable aleatoria
     * obsérvese que la cantidad de ellos puede ser distinta para cada valorVN
     *
     * @return result, el objeto almacenado en valores correspondiente a los parámetros
     * de entrada. Es responsabilidad del usuario hacer el cast a la clase adecuada
     * al hacer el llamado a esta función
     */
    public Object valorDato(String valorVN, PeriodoDeTiempo pt) throws XcargaDatos{

        if(tipoT!=TipoTiempo.PERIODO_DE_TIEMPO) throw new XcargaDatos("Se pidió un dato " +
                "por PeriodoDeTiempo y el DatosEnT no es por PeriodoDeTiempo");

        Object result = null;
        int indVN;

        if(esAleatorio){
            indVN = varNat.getValores().indexOf(valorVN);
            assert(indVN != -1): "Error en el valor de la variable de la " +
                    "naturaleza " + varNat.getNombre() + " valor " + valorVN;
        }else{
            indVN = 0;
        }
        int indTiempo = tiempos.get(indVN).indexOf(pt);
        if(indTiempo == -1) throw new XcargaDatos( "Error en el valor de la variable de la " +
                "naturaleza " + varNat.getNombre() + " valor " + valorVN + 
                " Tiempo " + pt.toString() + " no existe" );
        result = valores.get(indVN).get(indTiempo);
        return result;
    }

    /**
     * METODO valorDato
     * Devuelve un Object con el valor del dato según el valor de la
     * variable aleatoria y el valor de edad
     *
     * @param valorVN valor de la variable aleatoria.Es irrelevante si el
     * dato no es aleatorio.
     *
     * @param edad en años para el que se quiere la variable aleatoria
     * obsérvese que la cantidad de ellos puede ser distinta para cada valorVN
     *
     * @return result, el objeto almacenado en valores correspondiente a los parámetros
     * de entrada. Es responsabilidad del usuario hacer el cast a la clase adecuada
     * al hacer el llamado a esta función
     */
    public Object valorDato(String valorVN, double edad) throws XcargaDatos{

        // Pasa la edad a pasos de tiempo
        
        if(tipoT!=TipoTiempo.EDAD) throw new XcargaDatos("Se pidió un dato " +
        "por EDAD y el DatosEnT no es por EDAD");
        int indVN;
        Object result = null;

        if(esAleatorio){
            indVN = varNat.getValores().indexOf(valorVN);
            assert(indVN != -1): "Error en el valor de la variable de la " +
                    "naturaleza " + varNat.getNombre() + " valor " + valorVN;
        }else{
            indVN = 0;
        }
        int indTiempo = -1;
        for(int jt = 0; jt < tiempos.size(); jt++){
            int et = (Integer) tiempos.get(indVN).get(jt);
            if(edad <= et){
                indTiempo = jt;
                break;
            }

        }
        if(indTiempo == -1) throw new XcargaDatos( "Error en el valor de la variable de la " +
                "naturaleza " + varNat.getNombre() + " valor " + valorVN +
                " Edad en pasos de tiempo " + edad + " es mayor que cualquiera");
        result = valores.get(indVN).get(indTiempo);
        return result;
    }



    /**
     * METODO valorDato
     * Devuelve un Object con el valor del dato según el valor de la
     * variable aleatoria y el valor de pt (Date). Se elige el valor
     * que queda entre los Date adecuados.
     *
     * @param valorVN valor de la variable aleatoria. Es irrelevante si el
     * dato no es aleatorio.
     *
     * @param pt fecha Date para el que se quiere la variable aleatoria
     * obsérvese que la cantidad de ellos puede ser distinta para cada valorVN.
     * Se toma el objeto asociado a la primera Date que excede a pt en la lista
     *
     * @return result, el objeto almacenado en valores correspondiente a los parámetros
     * de entrada. Es responsabilidad del usuario hacer el cast a la clase adecuada
     * al hacer el llamado a esta función
     */
    public Object valorDato(String valorVN, Date pt) throws XcargaDatos{

        if(tipoT!=TipoTiempo.INSTANTE) throw new XcargaDatos("Se pidió un dato " +
        "por INSTANTE (Date) y el DatosEnT no es por INSTANTE");
        int indVN;
        Object result = null;

        if(esAleatorio){
            indVN = varNat.getValores().indexOf(valorVN);
            assert(indVN != -1): "Error en el valor de la variable de la " +
                    "naturaleza " + varNat.getNombre() + " valor " + valorVN;
        }else{
            indVN = 0;
        }        
        int indTiempo = -1;
        for(int jt = 0; jt < tiempos.size(); jt++){
            Date dt = (Date)tiempos.get(indVN).get(jt);
            if(pt.compareTo(dt) <= 0){
                indTiempo = jt;
                break;
            }
       
        }
        if(indTiempo == -1) throw new XcargaDatos( "Error en el valor de la variable de la " +
                "naturaleza " + varNat.getNombre() + " valor " + valorVN +
                " Tiempo " + pt.toString() + " es mayor que cualquiera");
        result = valores.get(indVN).get(indTiempo);
        return result;
    }




    @Override
    public String toString() {
        String texto = "-----------------------------------------" + "\n";
        texto += "COMIENZA un DatosAleatoriosT" + "\n";
        texto += "-----------------------------------------" + "\n";
        texto += "Tipo de dato: " + tipoDato.toString() + "\n";
        texto += "Tipo de tiempo (período/date) :" + tipoT.toString() + "\n";
        texto += "Variable de la naturaleza: " + varNat.toString()+ "\n";
        int i = 0;
        for(String st: varNat.getValores()){
            texto += "Valor variable nat: " + st + "\n";
            for(Object ob: tiempos.get(i) ){
                texto += ob.toString() + "\t";
            }
            texto += "\n";
            for(Object ob: valores.get(i) ){
                texto += ob.toString() + "\t";
            }
            i++;
        }
    return texto;
    }



    public static void main(String[] args) throws IOException, XcargaDatos, ParseException{

        String dirBase = "D:/Java/PruebaJava4";
        String dirEstudio = dirBase + "/" + "Datos_Estudio";
        String dirImplementacion = dirBase + "/" + "Datos_Implementacion";
        String dirCorridas = dirBase + "/" + "corridas";
        ImplementadorGeneral impG = new ImplementadorEDF();
        impG.inicializaImplementador(dirImplementacion, dirEstudio, dirCorridas);

        Estudio est = new Estudio("Estudio de prueba", 4, impG);
        String directorio = "D:/Java/PruebaJava3";
        est.leerEstudio(directorio, false);
        VariableNat rotura = est.getGrafoN().devuelveVN("muereSALAB_CAL");

        // Prueba con períodos de tiempo

        DatosEnT dAl = new DatosEnT(TipoDatoAleatT.STRING, TipoTiempo.PERIODO_DE_TIEMPO,
                rotura, true);
        ArrayList<Object> valoresD = new ArrayList<Object>();
        valoresD.add("primero");
        valoresD.add("segundo");
        ArrayList<Object> tiemposD = new ArrayList<Object>();
        tiemposD.add(new TiempoAbsoluto(2010));
        tiemposD.add(new TiempoAbsoluto(2011));
        dAl.cargaDatosUnValorVN("si", valoresD, tiemposD);
        String texto = dAl.toString();
        System.out.print(texto + "\n");
        String resultado = (String)dAl.valorDato("si", new TiempoAbsoluto(2011));
        System.out.print("El valorDato es " + resultado + "\n");


        // Prueba con instantes (fechas)

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date d1 = new Date();
        d1 = formatter.parse("2010-12-31");
        Date d2 = new Date();
        int anio = d2.getYear();
        System.out.print("Leyó bien el año" + anio);
        Date fecha = DateFormat.getDateInstance().parse("2011-12-31");





        d2 = formatter.parse("2011-12-31");
        Date d3 = new Date();
        d3 = formatter.parse("2011-06-01");

        DatosEnT dInst = new DatosEnT(TipoDatoAleatT.STRING, TipoTiempo.INSTANTE,
                rotura, true);
        ArrayList<Object> tiemposI = new ArrayList<Object>();
        tiemposI.add(d1);
        tiemposI.add(d2);
        dInst.cargaDatosUnValorVN("si", valoresD, tiemposI);
        texto = dInst.toString();
        System.out.print(texto+ "\n");
        resultado = (String)dInst.valorDato("si", d3);
        System.out.print("El valorDato es " + resultado + "\n");

        // Prueba con edades


        Integer e1 = 1;
        Integer e2 = 2;
        double d = 1.5;

        DatosEnT dEdad = new DatosEnT(TipoDatoAleatT.STRING, TipoTiempo.EDAD,
                rotura, true);
        ArrayList<Object> tiemposE = new ArrayList<Object>();
        tiemposE.add(e1);
        tiemposE.add(e2);
        dEdad.cargaDatosUnValorVN("si", valoresD, tiemposE);
        texto = dEdad.toString();
        System.out.print(texto+ "\n");
        resultado = (String)dEdad.valorDato("si", d);
        System.out.print("El valorDato es " + resultado + "\n");


    }
}
