package utilitariosMingo;

public class ContadorTiempo {
	private long milisegundosAcumulados;
	private long tiempoInicio;
	
	private boolean prendido;


	public ContadorTiempo() {
		super();
		milisegundosAcumulados = 0;
		tiempoInicio = 0;		
		prendido = false;
	}


//	public void iniciarContador() {
//		if (!prendido)	{
//			milisegundosAcumulados = 0;
//			tiempoInicio = 	System.currentTimeMillis();
//			prendido = true;
//		}
//	}

	public void pausarContador() {
		if (prendido) {
			milisegundosAcumulados += System.currentTimeMillis() - tiempoInicio;			
			prendido = false;
			
		}
		
	}
	
	public void continuarContador() {
		if (!prendido) {
			tiempoInicio = 	System.currentTimeMillis();
			prendido = true;
		}
		
	}
	
	public void terminarContador(){
		if (prendido) {				
			prendido = false;			
		}
		
	}
	
	public long finalizarContador(){
		if (!prendido) {
			return milisegundosAcumulados;
		}
		return milisegundosAcumulados;
	}
	

	public long getMilisegundosAcumulados() {
		return milisegundosAcumulados;
	}


	public void setMilisegundosAcumulados(long milisegundosAcumulados) {
		this.milisegundosAcumulados = milisegundosAcumulados;
	}


	public long getTiempoInicio() {
		return tiempoInicio;
	}


	public void setTiempoInicio(long tiempoInicio) {
		this.tiempoInicio = tiempoInicio;
	}




	public boolean isPrendido() {
		return prendido;
	}


	public void setPrendido(boolean prendido) {
		this.prendido = prendido;
	}


}
