package utilitariosMingo;

import java.io.Serializable;

public class ObjetoDePrueba implements Serializable{
	
	/**
	 *
	 * @author ut469262
	 */
	String numero;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public ObjetoDePrueba(String numero) {
        this.numero = numero;
    }


}
