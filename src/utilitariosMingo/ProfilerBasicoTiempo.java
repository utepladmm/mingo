package utilitariosMingo;

import java.util.Hashtable;

public class ProfilerBasicoTiempo {
	private static ProfilerBasicoTiempo instance;
	private Hashtable<String, ContadorTiempo> contadores;
	
	
	private ProfilerBasicoTiempo() {
		setContadores(new Hashtable<String, ContadorTiempo>());
	}
	
	
	/**Funci�n del singleton que devuelve siempre la misma instancia*/	
	public static ProfilerBasicoTiempo getInstance()
	{
		if (instance  == null)
			instance = new ProfilerBasicoTiempo();
	
		return instance;
	}
	
	
	public void crearContador(String nombre) {
		ContadorTiempo nuevo = new ContadorTiempo();
		contadores.put(nombre, nuevo);
	}


	public void pausarContador(String nombre) {
		contadores.get(nombre).pausarContador();	
	}
	
	public void continuarContador(String nombre) {		
		contadores.get(nombre).continuarContador();
		
	}
	
	public void terminarContador(String nombre){
		contadores.get(nombre).terminarContador();
	}

	public long getMilisegundosAcumulados(String nombre){
		return contadores.get(nombre).getMilisegundosAcumulados();
	}
	
	public void imprimirTiempo(String nombre){
		System.out.println("El tiempo acumulado por el contador "+ nombre + " es de: " + contadores.get(nombre).getMilisegundosAcumulados());
	}

	public Hashtable<String, ContadorTiempo> getContadores() {
		return contadores;
	}


	public void setContadores(Hashtable<String, ContadorTiempo> contadores) {
		this.contadores = contadores;
	}
}
