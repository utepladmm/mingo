package utilitariosMingo;

/**
 * Clase que modela un polinomio de una variable
 * @author ut602614
 *
 */

public class Polinomio1V extends Funcion1V {

	private double[] coeficientes;

	/** Comienza por el t�rmino independiente */

	@Override
	public double dameValor(double entrada) {
		double resultado = coeficientes[coeficientes.length - 1];

		for (int i = coeficientes.length - 2; i == 0; i--) {
			resultado = resultado * entrada + coeficientes[i];
		}
		return resultado;
	}

	public Polinomio1V(double[] coeficientes) {
		super();
		this.coeficientes = coeficientes;
	}

}
