package utilitariosMingo;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class LeerDatosArchivo {

    /**
     * Lee un archivo de texto por l�neas y separa dentro de las l�neas por blancos,
     * eliminando comentarios //
     * @param dirArchivo
     * @return resultado es el ArrayList<ArrayList<String>> con el texto separado en l�neas
     */
    public static ArrayList<ArrayList<String>> getDatos(String dirArchivo){
        BufferedReader entrada = null;
        ArrayList<ArrayList<String>> resultado =
                                            new ArrayList<ArrayList<String>>();
        ArrayList<String> lineaResul;
        String[] datosLinea;
        String linea;
        String sep = "[\\s]+"; // Separador de datos.
        String coment = "//"; // Indicador de comentario.
        int posCom; // Posici�n del inicio del comentario.
        int j;

        try{
            File archivo = new File(dirArchivo);
            entrada = new BufferedReader(new FileReader(archivo));
            while( (linea = entrada.readLine()) != null){
                // Eliminaci�n de los comentarios de las l�neas.
                if( (posCom = linea.indexOf(coment)) > -1 ){
                    linea = (posCom == 0)? "" : linea.substring(0, posCom);
                }
                // Si la l�nea no es una l�nea de blancos, entonces se agregan
                // los datos y se agrega una nueva "l�nea" a resultado.
                if( !linea.matches("[\\s]*" ) ){
                    datosLinea = linea.split(sep);
                    lineaResul = new ArrayList<String>();
                    for(j = 0; j < datosLinea.length; j++){
                        /* Si el dato NO es un string de longitud cero "",
                         * lo cual podr�a ocurrir si
                         * la l�nea comienza por ej. con tabulador, se agrega
                         * el dato. */
                        if( !datosLinea[j].equalsIgnoreCase("") ){
                            lineaResul.add(datosLinea[j]);
                        }
                    }
                    resultado.add(lineaResul);
                }
            }
            entrada.close();
        }catch(FileNotFoundException ex){
                System.out.println("------ERROR en LeerDatosArchivo-------");
            System.out.println("No se encontr� el archivo: " + dirArchivo);
            System.out.println("------------------\n");
            System.exit(1);
        }catch(IOException ex){
            System.out.println("------ERROR en LeerDatosArchivo-------");
            System.out.println("Al leer el archivo: " + dirArchivo +
                    ", se produjo la excepci�n: " + ex.toString() );
            System.out.println("------------------\n");
            System.exit(1);            
        }


        return resultado;
    }


}
