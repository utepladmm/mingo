package utilitariosMingo;
/**
 * Clase utilizada para almacenar todas las constantes
 * @author ut602614
 *
 */
public class Constantes {
	

	/**
	 * Constantes asociadas a procesos generales como simulaci�n, optimizaci�n cl�sica, etc.
	 */
	public static final String FASE_SIM = "simulaci�n";
	public static final String FASE_OPT = "optimizaci�n";
	
	/**
	 * Constantes generales de la corrida
	 */
	public static final String XML_CORRIDA = "corrida";
	public static final String XML_IDCORRIDA = "id";
	public static final String XML_NOMBRECORRIDA = "nombre";
	public static final String XML_DESCRIPCIONCORRIDA = "descripcion";
	public static final String XML_PARAMETROSCORRIDA= "parametrosCorrida";
	public static final String XML_CANTPOSTESCORRIDA= "cantidadPostes";
	public static final String XML_DURACIONPOSTESCORRIDA= "duracionPostes";
	public static final String XML_TIPORESOLUCIONCORRIDA= "tipoResolucionValoresBellman";
	public static final String XML_CONDICIONESITERACIONES = "condicionesIteraciones";
	public static final String XML_MAXITERACIONES = "maximoIteraciones";
		
	/**COSTANTES ASOCIADAS A DIRECTORIOS DE ENTRADA Y SALIDA	
	 */
	//public static final String DIR_SALIDAS = "D:/SalidasModeloOp";
	
	/**Constantes asociadas a la corrida*/
	public static final String COMPVALORESBELLMAN = "tipoResolucionValoresBellman";
	public static final String COMPDEMANDA = "tipoDemanda";
	public static final String PROBHIPERPLANOS = "hiperplanos";
	public static final String PROBINCREMENTOS = "incrementos";
	public static final String DEMRESIDUAL = "residual";
	public static final String DEMTOTAL = "total";
	
	
	/**Constantes de los T�rmicos*/
	public static final String COMPMINTEC = "compMinimosTecnicos";
	public static final String TERSINMINTEC = "SinMinTec";
	public static final String TERMINTECFORZADO = "MinTecForzado";
	public static final String TERVARENTERAS = "VarEnteras";
	public static final String TERVARENTERASYVARESTADO = "VarEnterasYVarEstado";
	public static final String TERDOSPASOS="dosPasos"; //primera iteracion sinMinTec Segunda iteracion MinTecForzado
	public static final String COMPFLEXIBILIDAD = "flexibilidadMin";
	public static final String TERFLEXHORARIO = "horaria";
	public static final String TERFLEXSEMANAL = "semanal";
	
	
		
	/**Constantes de las iteraciones*/
	public static final String PORNUMEROITERACIONES = "porNumeroIteraciones";
	public static final String PORUNANIMIDADPARTICIPANTES = "porUnanimidadParticipantes";
	
	
	/**Constantes del acumulador*/
	public static final String COMPPASO = "compPaso";
	public static final String ACUCIERRAPASO = "cierraPaso";
	public static final String ACUMULTIPASO ="multiPaso";
	public static final String ACUBALANCECRONOLOGICO ="balanceCronologico";
	
	/**Constantes de las hidr�ulicas */
	public static final String COMPLAGO = "compLago";
	public static final String COMPCOEFENERGETICO = "compCoefEnergetico";
	public static final String HIDROCONLAGO = "ConLago";
	public static final String HIDROSINLAGO = "SinLago";
	public static final String HIDROSINLAGOENOPTIM = "SinLagoEnOptim";  // con lago en simulaci�n y sin lago en optimizaci�n
	public static final String HIDROCOEFENERGCONSTANTES = "CoefEnergConstantes";
	public static final String HIDROPOTENCIACAUDAL = "PotenciaCaudal";
	public static final double EPSILONVOLAGUA = 100.0;    // m3
	public static final double EPSILONVALAGUA = 0.01; // En USD/hm3, se toma como m�nimo valor del agua de centrales con lago en el comportamiento simulaci�n
	// public static final double EPSILONCAUDALAGUA = 0.01;    // m3/s
	public static final double EPSILONCAUDALAGUA = 0.000001;    // m3/s
	public static final double AUMENTO_ERO_ABAJO = 1.2; // Se aplica en la limitaci�n de erogados
//	public static final double PENALIZACION_AGUA_DESTRUIDA = 0.01;  // En USD/m3 - Penaliza la destrucci�n de agua en centrales sin lago para poder usar restricci�n de desigualdad
	public static final double PENALIZACION_AGUA_DESTRUIDA = 0.01; // 0.000001
	public static final double TOLERANCIA_DESTRUCCION_AGUA = 6000;    // En m3 - Avisa si se destruye m�s que esta cantidad en centrales sin lago
	public static final int TOLERANCIA_CREACION_AGUA = 6000;   // En m3 - Avisa si se fabrica m�s que esta cantidad en centrales sin lago
	public static final double VARIA_EROGADO_ENTRE_POSTES = 1; // Porcentraje de variaci�n del erogado para centrales sin lago 
	// por el turbinado m�ximo de centrales aguas abajo
	
	
	/**Constantes de contratos de combustible top*/
	public static final String COMPCOSTOSTOP = "compCostos";
	public static final String COSTOSCONVEXOS = "CostosConvexos";  // la funci�n de costos del gas en el despacho es convexa
	public static final String COSTOSNOCONVEXOS ="CostosNoConvexos";
	
	
	/**Constantes de red de combustible y sus elementos*/
	public static final String COMPREDCOMB = "compRedComb";
	public static final String VARSPORPASO = "varsPorPaso";   // los elementos de la red tienen una variable por paso
	public static final String VARSPORSUBCONJPOSTES = "varsPorSubConjPostes";  // id. una variable por cada subconjunto de postes definidos
	
	public static final String COMPTANQUECOMP = "compTanqueComp";
	public static final String TANQUECIERRAPASO = "tanqueCierraPaso"; // un �nico cierre por paso
	public static final String TANQUECIERRASUBCONJPOSTES = "tanqueCierraSubConjPostes"; // un cierre en cada subconjunto de pasos
	
	
	/**Red*/
	public static final String COMPRED = "compUsoRed";
	public static final String CONRED = "conRed";
	public static final String UNINODAL = "uninodal";
	
	/**Comportamientos Rama*/
	public static final String COMPRAMA = "compRama";
	public static final String RAMASIMPLE = "simple";
	public static final String RAMADC = "dc";
	
	/**Comportamientos Falla*/
	public static final String COMPFALLA= "compFalla";
	public static final String FALLA_CONESTADO_SINDUR= "conEstadoSinDur";
	public static final String FALLA_CONESTADO_CONDUR= "conEstadoConDur";
	public static final String FALLASINESTADO= "sinEstado";
	

	
	/**BINARIA, ENTERA, CONTINUA*/
	
	/**Tipos de variable de control para resolver el problema lineal*/
	public static final int VCESTANDAR = 0;
	public static final int VCENTERA = 1;
	public static final int VCCONTINUA = 2;
	
	public static final int VCLIBRE = 3;
	public static final int VCPOSITIVA = 4;
	public static final int VCSEMICONTINUA = 5;
	public static final int VCBINARIA = 6;
	
	/**Tipos de restriccion*/
	public static final int RESTMENOROIGUAL = -1;
	public static final int RESTIGUAL = 0;
	public static final int RESTMAYOROIGUAL = 1;
	
	
	/**Tipos de valPostizacion*/
	public static final int VALPPROMEDIO = 0;
	public static final int VALPALEAT = 1;
	public static final int VALPMIN = 2;
	public static final int VALPMAX = 3;
	
	
	/**Tipos de palier de importaci�n o exportaci�n*/
	public static final String IEEVOL = "IEEVOL";
	public static final String IEALEATFORMUL = "IEALEATFORMUL";
	public static final String IEALEATPRPOT = "IEALEATPRPOT";
	

	/** Factores de conversi�n*/
	public static final int SEGUNDOSXHORA = 60*60;
	public static final int SEGUNDOSXDIA = 24*60*60;
	public static final int SEGUNDOSXSEMANA = 24*7*60*60;
	public static final int SEGUNDOSXANIO = 24*365*60*60;
	public static final int MWHXGWH = 1000;
	public static final int USDXMUSD = 1000000;
	public static final int USDXkUSD = 1000;
	public static final int KWXMW = 1000;
	
	
	
	public static final String NOMBREBARRAUNICA = "barraUnica";
	public static final double EPSILONCOEF = 1E-8;
	public static final double DESLIZAMIENTOMUESTREO = 0.5;
	public static final String PROVVENTA = "venta";
	public static final String PROVCOMPRA = "compra";
	public static final int EPSILONSALTOTIEMPO = 1;

	
	/** Duraci�n de pasos de procesos estoc�sticos*/
	public static final String PASOSEMANA = "SEMANA";
	public static final String PASODIA = "DIA";
	public static final String PASOHORA = "HORA";	
	
	/**F�SICAS*/
	public static final double G = 9.80655;
	public static final double DENSIDADDELAGUA = 1000;
	public static final double PESOESPDELAGUA = G*DENSIDADDELAGUA;
	public static final double CONHM3AM3 = 1000000;
	public static final double INFNUESTRO = 1e12;
	
	
	/**
	 * Constantes de par�metros de la salida provisoria
	 *
	 * ATENCION MANOLO: ACA INVOCA AL M�TODO QUE CREA LOS RESULTADOS RESUMIDOS
	 * 
	 * param es int[]; 0 indica que no se produce la salida, 1 indica que s�.
	 * 
	 *          nombre archivo
	 * param[0]	ener_resumen  	energ�a anual promedio en los escenarios; filas recurso; columnas a�o
	 * param[1]	ener_cron  		energ�a por a�o y escenario para todos los recursos: filas a�o,escenario; columnas recurso   
	 * param[2]	pot				para recursos en particular, un archivo por poste, filas paso, columnas poste
	 * param[3]					lista de enteros int[] con los indicadores de los recursos para los que se va a sacar el archivo de pot
	 * 
	 * param[4]	costo_resumen 	costo anual promedio en los escenarios; filas recurso; columnas a�o
	 * param[5]	costo_cron 		costo por a�o y escenario para todos los recursos: filas (a�o,escenario); columnas recurso   
	 * param[6]	costo_poste		para recursos en particular, un archivo por poste, filas paso, columnas poste
	 * param[7]					lista de enteros int[] con los indicadores de los recursos para los que se va a sacar el archivo de costo_poste		 
	 *
	 * param[8]	cosmar_resumen filas paso; columnas poste; (los promedios seg�n cantidad de horas = curva plana)
	 * param[9]	cosmar_cron    un archivo por poste, filas paso, columnas cr�nicas
	 * 
	 * param[10]		       lista de enteros int[] con los indices de las barras para los que se va a sacar los costos marginales detallados	 
	 * 
	 * param[11]	Si es =1 genera un directorio cantMod, con un archivo de disponibilidades para cada recurso
	 * 				En esos archivos las filas son pasos y las columnas son escenarios (cr�nicas)
	 * param[12]    lista de enteros int[] con los �ndices de los recursos para los que se sacan atributos detallados
	 * 
	 * param[13]    Si es =1 genera el archivo de salidas detalladas por cada paso SalidaDetalladaSP
	 * param[14]    Si es =1 genera el archivo de costo por paso y por escenario
	 * param[15] 	Si es =1 genera el archivo con numpos por paso y escenario si la postizaci�n es interna
	 * param[16] 	Si es =1 genera el archivo con costos marginales por paso, escenario e intervalo de muestreo para la barra 0 (�nica)
	 */
	
	
	public static final int PARAMSAL_RESUMEN = 0;
	public static final int PARAMSAL_ENERCRON = 1;
	public static final int PARAMSAL_POT = 2;	
	public static final int PARAMSAL_IND_POT = 3;		
	public static final int PARAMSAL_COSTO_RESUMEN = 4;
	public static final int PARAMSAL_COSTO_CRON = 5;
	public static final int PARAMSAL_COSTO_POSTE = 6;	
	public static final int PARAMSAL_IND_COSTO_POSTE = 7;	
	public static final int PARAMSAL_COSMAR_RESUMEN = 8;
	public static final int PARAMSAL_COSMAR_CRON = 9;
	public static final int PARAMSAL_IND_COSMAR_CRON = 10;	
	public static final int PARAMSAL_CANTMOD = 11;
	public static final int PARAMSAL_IND_ATR_DET = 12;
	public static final int PARAMSAL_SALIDA_DET_PASO = 13;
	public static final int PARAMSAL_COSTO_PASO_CRON = 14;
	public static final int PARAMSAL_NUMPOS = 15;
	public static final int PARAMSAL_COSMAR_INT_MUESTREO = 16;
	public static final int PARAMSAL_CANT_PARAM = 50;   // Constante para dimensionar el array de par�metros
	
	/**
	 * NIVEL DE DETALLE EN LO QUE SE IMPRIME EN CONSOLA
	 * 
	 * 0: NADA
	 * 1: SOLO LO INDISPENSABLE
	 * 2: TODO, INCLUSO LO DE PRUEBA
	 */
	
	public static final int NIVEL_CONSOLA = 0;  
	
	
	/**
	 * Prefijos para indicar tipos de recursos (o afines ya que est� la falla tambi�n)
	 */
	public static final String TER = "TER";
	public static final String HID = "HID";
	public static final String PROV = "PROV";
	public static final String DEM = "DEM";
	public static final String FALLA = "FALLA";
	public static final String EOLO = "EOLO";
	public static final String FOTOV = "FOTOV";
	public static final double FACTOR_CARGA_NUCLEOS = 1;
	public static final String ACUM = "ACUM";
	public static final String IMPOEXPO = "IMPOEXPO";


	/***
	 * Constantes de operaciones Paralelismo
	 */
	
	
	public static final int CARGAR_CORRIDA = 0;
	public static final int OPTIMIZAR = 1;
	public static final int OPTIMIZAR_ESTADOS = 2;
	public static final int RETROCEDER_PASO = 3;
	public static final int SIMULAR = 4;
	public static final int SIMULAR_ESCENARIOS = 5;
	public static final int ESPERANDO_OPERACION = 10;
	public static final int CANT_ESTADOS_PAQUETE = 1; //POR AHORA ESTO NO SE PUEDE TOCAR
	public static final int CANT_ESTADOS_PAQUETE_ESCENARIO = 1; //POR AHORA ESTO NO SE PUEDE TOCAR
	public static final int ENESPERA = 0;
	public static final int ENRESOLUCION = 1;
	public static final int TERMINADO = 2;
	
	public static final int CP = 20; //Cantidad de paquetes cargada inicialmente en la lista paquetesAResolver
	public static final int CPESCENARIOS = 20; //Cantidad de paquetes cargada inicialmente en la lista paquetesAResolver
	public static final int TD = 10000; //Demora m�xima admisible antes de volver a enviar un paquete a la lista paquetesAResolver
	public static final int TDESCENARIOS = 10000; //Demora m�xima admisible antes de volver a enviar un paquete a la lista paquetesAResolver
	public static final int TRESU = 100; //Tiempo para volver a chequear la lista paquetes resueltos
	public static final int TRESUESCENARIOS = 1000; //Tiempo para volver a chequear la lista paquetes resueltos
	public static final int CE = 5; //Cantidad de paquetes a enviar en exceso de la cantidad de resueltos CR
	public static final int CEESCENARIOS = 5; //Cantidad de paquetes a enviar en exceso de la cantidad de resueltos CR
	
	public static final String ruta_log_paralelismo = "d:\\salidasmodeloop\\logparalelismo\\";
	
	/***
	 * Constantes de solvers
	 */

	/**
	 * Determinacion de solver
	 */
	public static final int RES_LP_SOLVE = 0;
	public static final int RES_GLPK = 1;
//	public static final int RESOLVEDOR_PRINCIPAL = RES_LP_SOLVE;
	public static final int RESOLVEDOR_PRINCIPAL = RES_GLPK;
	
	/**
	 * Constantes de LPSOLVE
	 */
	public static final int NUM_MAX_RELAX_PARAM_LP_SOLVE = 5;
	public static final int MULTIP_AUMENTO_EPS_PARAM_LP_SOLVE = 10;
	
	/**
	 * Constantes de glpk
	 */
	// constantes para glpk como solver principal
	public static final boolean GLP_RP_primario = false; // reutilizacion de problema
	public static final boolean GLP_RB_primario = false; // reutilizacion de base
	
	// constantes para glpk como solver secundario
	public static final boolean GLP_RP_secundario = false; // reutilizacion de problema
	public static final boolean GLP_RB_secundario = false; // reutilizacion de base

	public static final boolean RES_GLP_RP = RESOLVEDOR_PRINCIPAL == RES_LP_SOLVE ? GLP_RP_secundario : GLP_RP_primario; 
	public static final boolean RES_GLP_RB = RESOLVEDOR_PRINCIPAL == RES_LP_SOLVE ? GLP_RB_secundario : GLP_RB_primario; 
	public static final boolean RES_GLP_RESET_EXACTO = false; // utilizar solver exacto para problema lineal en caso de falla
	public static final boolean RES_GLP_IMPRIMIR_LP_SOL = false;  // imprimir los problemas y soluciones
	public static final double RES_GLP_TOL = 1e-11; // tolerancia numerica
	public static final int RES_GLP_TIEMPO_MAX = 1*1000; // tiempo maximo inicial de ejecucion del glpk
	public static final String RES_GLP_IMPRIMIR_LP_RUTA = "d:\\salidasModeloOp\\lp\\";
	public static final String RUTA_SALIDA_SIM_PARALELA = "S:\\UTE\\MOP\\SalidaParalela";
	public static final String VERSION_NUM = "0.1.0";
	public static final String VERSION_ET = " - " + VERSION_NUM;

	
	
}
