package utilitariosMingo;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import datatypes.DatosPolinomio;
import datatypes.Pair;

public class Polinomio {
	private String tipo;
	private double[] coefs;
	private Double xmin;
	private Double xmax;
	private Double valmin;
	private Double valmax;
	private Hashtable<String, Polinomio> colpols;
	private ArrayList<Polinomio> polsrango;
	private Polinomio fueraRango;
	private ArrayList<Pair<Double, Double>> rangos;

	public Polinomio() {
		tipo = "poli";
		coefs = new double[3];

	}

	public Polinomio(String tipo, double[] coefs, Double xmin, Double xmax, Double valmin, Double valmax) {
		super();
		this.tipo = tipo;
		this.coefs = coefs;
		this.xmin = xmin;
		this.xmax = xmax;
		this.valmin = valmin;
		this.valmax = valmax;
	}

	public Polinomio(DatosPolinomio datosPolinomio) {
		this.tipo = datosPolinomio.getTipo();
		this.coefs = datosPolinomio.getCoefs();
		this.xmin = datosPolinomio.getXmin();
		this.xmax = datosPolinomio.getXmax();
		this.valmin = datosPolinomio.getValmin();
		this.valmax = datosPolinomio.getValmax();
		this.colpols = new Hashtable<String, Polinomio>();
		this.polsrango = new ArrayList<Polinomio>();

		if (tipo.equalsIgnoreCase("poliMulti")) {
			Set<String> claves = datosPolinomio.getPols().keySet();
			Iterator<String> it = claves.iterator();
			while (it.hasNext()) {
				String clave = it.next();
				Polinomio nuevo = new Polinomio(datosPolinomio.getPols().get(clave));
				colpols.put(clave, nuevo);
			}
		}
		if (tipo.equalsIgnoreCase("porRangos")) {
			this.setFueraRango(new Polinomio(datosPolinomio.getFueraRango()));
			this.setRangos(datosPolinomio.getRangos());
			for (DatosPolinomio dp : datosPolinomio.getPolsrangos())
				this.polsrango.add(new Polinomio(dp));
		}
	}

	public double dameValor(double entrada) {

		if (tipo.equalsIgnoreCase("policoncotas")) {
			if (entrada < xmin) {
				return valmin;
			}
			if (entrada > xmax) {
				return valmax;
			}
		} else if (tipo.equalsIgnoreCase("porRangos")) {
			int i = 0;
			for (Pair<Double, Double> rango : this.rangos) {
				if (entrada >= rango.first && entrada < rango.second)
					return this.polsrango.get(i).dameValor(entrada);
				i++;
			}
			return this.fueraRango.dameValor(entrada);
		}

		double resultado = coefs[coefs.length - 1];

		for (int i = coefs.length - 2; i >= 0; i--) {
			resultado = resultado * entrada + coefs[i];
		}

		return resultado;
	}

	public double dameValor(Hashtable<String, Double> parametros) {
		double resultado = 0;
		Set<String> claves = parametros.keySet();
		Iterator<String> it = claves.iterator();
		String clave;
		while (it.hasNext()) {
			clave = it.next();
			resultado += colpols.get(clave).dameValor(parametros.get(clave));
		}

		return resultado;
	}

	public double dameValor(double a, double b) {
		if (tipo.equalsIgnoreCase("poliMulti")) {
			double resultado = 0;
			Set<String> claves = colpols.keySet();
			Iterator<String> it = claves.iterator();

			resultado += colpols.get(it.next()).dameValor(a);
			resultado += colpols.get(it.next()).dameValor(b);

			return resultado;
		} 
		if (tipo.equalsIgnoreCase("porRangos")) {
			int i = 0;
			for (Pair<Double, Double> rango : this.rangos) {
				if (a >= rango.first && a < rango.second)
					return this.polsrango.get(i).dameValor(a,b);
				i++;
			}
			return this.fueraRango.dameValor(a,b);
		}
		return 0;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public double[] getCoefs() {
		return coefs;
	}

	public void setCoefs(double[] coefs) {
		this.coefs = coefs;
	}

	public Double getXmin() {
		return xmin;
	}

	public void setXmin(Double xmin) {
		this.xmin = xmin;
	}

	public Double getXmax() {
		return xmax;
	}

	public void setXmax(Double xmax) {
		this.xmax = xmax;
	}

	public Double getValmin() {
		return valmin;
	}

	public void setValmin(Double valmin) {
		this.valmin = valmin;
	}

	public Double getValmax() {
		return valmax;
	}

	public void setValmax(Double valmax) {
		this.valmax = valmax;
	}

	public Hashtable<String, Polinomio> getColpols() {
		return colpols;
	}

	public void setColpols(Hashtable<String, Polinomio> colpols) {
		this.colpols = colpols;
	}

	public ArrayList<Polinomio> getPolsrango() {
		return polsrango;
	}

	public void setPolsrango(ArrayList<Polinomio> polsrango) {
		this.polsrango = polsrango;
	}

	public Polinomio getFueraRango() {
		return fueraRango;
	}

	public void setFueraRango(Polinomio fueraRango) {
		this.fueraRango = fueraRango;
	}

	public ArrayList<Pair<Double, Double>> getRangos() {
		return rangos;
	}

	public void setRangos(ArrayList<Pair<Double, Double>> rangos) {
		this.rangos = rangos;
	}

}

