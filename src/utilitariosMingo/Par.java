package utilitariosMingo;

public class Par {
 
	int int1;
	int int2;
	public Par(int int1, int int2) {
		super();
		this.int1 = int1;
		this.int2 = int2;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + int1;
		result = prime * result + int2;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Par other = (Par) obj;
		if (int1 != other.int1)
			return false;
		if (int2 != other.int2)
			return false;
		return true;
	}
	
	
    	
    
}
