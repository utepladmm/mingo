package utilitariosMingo;

public class ComparadorObjetos implements Comparable{
	
	private double valorParaComparar;
	private Object objeto;
	
	
	public ComparadorObjetos(double valorParaComparar, Object objeto) {
		super();
		this.valorParaComparar = valorParaComparar;
		this.objeto = objeto;
	}



	public int compareTo(Object obj){
		ComparadorObjetos co = (ComparadorObjetos)obj;
		if(this.valorParaComparar < co.getValorParaComparar()) return -1;
		if(this.valorParaComparar > co.getValorParaComparar()) return 1;
		return 0;
		
	}
	
	

	public double getValorParaComparar() {
		return valorParaComparar;
	}

	public void setValorParaComparar(double valorParaComparar) {
		this.valorParaComparar = valorParaComparar;
	}

	public Object getObjeto() {
		return objeto;
	}

	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}
	
	

}
