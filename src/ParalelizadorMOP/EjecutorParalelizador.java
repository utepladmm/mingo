package ParalelizadorMOP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EjecutorParalelizador {
	

	
	
	public static int ejecutaParalelizador(String jarParalelizador, String xmlInvocaParalelizador) {
	
		String argumento = "java -jar " + jarParalelizador + " " + xmlInvocaParalelizador;
		
		Process process;
		try {
			process = Runtime.getRuntime().exec(argumento);
			 // Read the output from the process
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String outputLine;
			while ((outputLine = stdInput.readLine()) != null) {
			      System.out.println(outputLine);
			}
	
			// Read the error output from the process
			BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			String errorLine;
			while ((errorLine = stdError.readLine()) != null) {
		      		System.err.println(errorLine);
			}
	
			int exitCode = process.waitFor();	//0 representa que el proceso se ejecuto correctamente
			
			return exitCode;
			
			    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
			return 1;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
			return 1;
		}

	}
}
