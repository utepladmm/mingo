package ImplementaMOP;

import java.util.ArrayList;
import java.util.Hashtable;

import UTE_CalculosEmpresa.DatosCorridaUTE;
import UTE_contratos.DatosContratosInterrumpibleCorrida;
import datatypes.*;
import datatypesProcEstocasticos.*;
import datatypesSalida.*;
import datatypesTiempo.*;
import tiempo.Evolucion;

public class DatosCorridaMingo {
	private String nombre;											/**Nombre de la corrida*/
	private String descripcion;										/**Descripci�n de la corrida*/	
	private String postizacion; 									/**En el caso de externa es la ruta y en caso de ser interna el tipo (mon�tona o cronol�gica)*/
	private String tipoPostizacion;									/**Puede ser externa o interna*/
	private String tipoSimulacion;
	private String valPostizacion;									/**En el caso de externa es la ruta*/
	private String tipoValpostizacion;								/**Puede ser externa o interna*/
	private String ruta;											/**Ruta XML*/
	private String inicioCorrida;
	private String finCorrida;
	private Double semilla;
	private Double tasa;
	private double topeSpot; // en USD/MWh
	private boolean despSinExp;  // si es true una de las iteraciones de la simulación se usará para obtener un despacho sin exportación
	private int iteracionSinExp; // si despSinExp = true, es la iteración, empezando de 1, que se usa para estimar el despacho sin exportación
	private ArrayList<String> paisesACortar; // Los destinos a los que se anulan las exportaciones en la corrida sin exportación 
	private String rutaSals;
	private Integer cantEscenarios;
	private Evolucion<Integer> cantSorteosMont;									/**Cantidad de sorteos para la optimizaci�n (montecarlitos)**/
	private Hashtable<String,Evolucion<String>> valoresComportamientoGlobal;			/**Lista de valores de comportamiento para cada variable de comportamiento*/	
	private DatosIteracionesCorrida datosIteraciones;				/**Datos de las iteraciones asociadas a cada paso de la corrida*/
	private Hashtable<String,DatosTermicosCorrida> casosTermicos;							/**Datos de los t�rmicos*/
	private Hashtable<String,DatosCiclosCombinadosCorrida> casosCiclosComb;	
	private Hashtable<String,DatosHidraulicosCorrida> casosHidraulicos;					/**Datos de los hidr�ulicos*/
	private Hashtable<String,DatosEolicosCorrida> casosEolicos;							/**Datos de los e�licos*/
	private Hashtable<String,DatosFotovoltaicosCorrida> casosFotovoltaicos;			    /**Datos de los fotovoltaicos*/
	private Hashtable<String,DatosImpoExposCorrida> casosImpoExpos;
	private Hashtable<String,DatosDemandasCorrida> casosDemandas;							/**Datos de las demandas*/
	private Hashtable<String,DatosFallasEscalonadasCorrida> casosFallas;					/**Datos de las fallas*/
	private Hashtable<String,DatosImpactosCorrida> casosImpactos;
	private Hashtable<String,DatosContratosEnergiaCorrida> casosContratosEner;
	private Hashtable<String,DatosContratosInterrumpibleCorrida> casosContratosInt;
	private DatosRedElectricaCorrida red;							/**Datos de la red el�ctrica*/
	private Hashtable<String,DatosCombustiblesCorrida> casosCombustibles;					/**Datos de las redes de combustibles*/
	private Hashtable<String,DatosAcumuladoresCorrida> casosAcumuladores;
	private Hashtable<String,DatosConvertidoresCorrida> casosConvertidores;
	private DatosLineaTiempo lineaTiempo;
//	private Hashtable<String,Hashtable<String,DatosProcesoEstocastico>> casosProcesosEstocasticos;
	private Hashtable<String,DatosProcesoEstocastico> procesosEstocasticos;
	private DatosParamSalida datosParamSalida;
	private DatosParamSalidaOpt datosParamSalidaOpt;
	private DatosParamSalidaSim datosParamSalidaSim;
	private DatosCorridaUTE datosCorridaUTE;
	private Boolean costosVariables = true;
	
	public DatosCorridaMingo() {								
		this.valoresComportamientoGlobal = new Hashtable<String,Evolucion<String>>();		
		casosDemandas = new Hashtable<String,DatosDemandasCorrida>();
		datosIteraciones = new DatosIteracionesCorrida();
		casosTermicos = new Hashtable<String,DatosTermicosCorrida>();
		casosCiclosComb = new Hashtable<String, DatosCiclosCombinadosCorrida>();
		casosHidraulicos = new Hashtable<String,DatosHidraulicosCorrida>();
		casosEolicos = new Hashtable<String,DatosEolicosCorrida>();
		casosFotovoltaicos = new Hashtable<String,DatosFotovoltaicosCorrida>();
		casosImpoExpos = new Hashtable<String,DatosImpoExposCorrida>();
		casosFallas = new Hashtable<String,DatosFallasEscalonadasCorrida>();
		casosAcumuladores = new Hashtable<String,DatosAcumuladoresCorrida>();
		red =  new DatosRedElectricaCorrida();
		casosCombustibles = new Hashtable<String,DatosCombustiblesCorrida>();
		casosConvertidores = new Hashtable<String,DatosConvertidoresCorrida>();
		lineaTiempo = new DatosLineaTiempo();
//		casosProcesosEstocasticos = new Hashtable<String,Hashtable<String,DatosProcesoEstocastico>>();
		procesosEstocasticos = new Hashtable<String,DatosProcesoEstocastico>();
		datosParamSalida = new DatosParamSalida();
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getPostizacion() {
		return postizacion;
	}

	public void setPostizacion(String postizacion) {
		this.postizacion = postizacion;
	}

	public String getTipoPostizacion() {
		return tipoPostizacion;
	}

	public void setTipoPostizacion(String tipoPostizacion) {
		this.tipoPostizacion = tipoPostizacion;
	}

	public String getTipoSimulacion() {
		return tipoSimulacion;
	}

	public void setTipoSimulacion(String tipoSimulacion) {
		this.tipoSimulacion = tipoSimulacion;
	}

	public String getValPostizacion() {
		return valPostizacion;
	}

	public void setValPostizacion(String valPostizacion) {
		this.valPostizacion = valPostizacion;
	}

	public String getTipoValpostizacion() {
		return tipoValpostizacion;
	}

	public void setTipoValpostizacion(String tipoValpostizacion) {
		this.tipoValpostizacion = tipoValpostizacion;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String getInicioCorrida() {
		return inicioCorrida;
	}

	public void setInicioCorrida(String inicioCorrida) {
		this.inicioCorrida = inicioCorrida;
	}

	public String getFinCorrida() {
		return finCorrida;
	}

	public void setFinCorrida(String finCorrida) {
		this.finCorrida = finCorrida;
	}

	public Double getSemilla() {
		return semilla;
	}

	public void setSemilla(Double semilla) {
		this.semilla = semilla;
	}

	public Double getTasa() {
		return tasa;
	}

	public void setTasa(Double tasa) {
		this.tasa = tasa;
	}

	public String getRutaSals() {
		return rutaSals;
	}

	public void setRutaSals(String rutaSals) {
		this.rutaSals = rutaSals;
	}

	public Integer getCantEscenarios() {
		return cantEscenarios;
	}

	public void setCantEscenarios(Integer cantEscenarios) {
		this.cantEscenarios = cantEscenarios;
	}

	public Evolucion<Integer> getCantSorteosMont() {
		return cantSorteosMont;
	}

	public void setCantSorteosMont(Evolucion<Integer> cantSorteosMont) {
		this.cantSorteosMont = cantSorteosMont;
	}

	public Hashtable<String, Evolucion<String>> getValoresComportamientoGlobal() {
		return valoresComportamientoGlobal;
	}

	public void setValoresComportamientoGlobal(Hashtable<String, Evolucion<String>> valoresComportamientoGlobal) {
		this.valoresComportamientoGlobal = valoresComportamientoGlobal;
	}

	public DatosIteracionesCorrida getDatosIteraciones() {
		return datosIteraciones;
	}

	public void setDatosIteraciones(DatosIteracionesCorrida datosIteraciones) {
		this.datosIteraciones = datosIteraciones;
	}

	public Hashtable<String, DatosTermicosCorrida> getCasosTermicos() {
		return casosTermicos;
	}

	public void setCasosTermicos(Hashtable<String, DatosTermicosCorrida> casosTermicos) {
		this.casosTermicos = casosTermicos;
	}

	public Hashtable<String, DatosHidraulicosCorrida> getCasosHidraulicos() {
		return casosHidraulicos;
	}

	public void setCasosHidraulicos(Hashtable<String, DatosHidraulicosCorrida> casosHidraulicos) {
		this.casosHidraulicos = casosHidraulicos;
	}

	public Hashtable<String, DatosEolicosCorrida> getCasosEolicos() {
		return casosEolicos;
	}

	public void setCasosEolicos(Hashtable<String, DatosEolicosCorrida> casosEolicos) {
		this.casosEolicos = casosEolicos;
	}

	public Hashtable<String, DatosFotovoltaicosCorrida> getCasosFotovoltaicos() {
		return casosFotovoltaicos;
	}

	public void setCasosFotovoltaicos(Hashtable<String, DatosFotovoltaicosCorrida> casosFotovoltaicos) {
		this.casosFotovoltaicos = casosFotovoltaicos;
	}

	public Hashtable<String, DatosImpoExposCorrida> getCasosImpoExpos() {
		return casosImpoExpos;
	}

	public void setCasosImpoExpos(Hashtable<String, DatosImpoExposCorrida> casosImpoExpos) {
		this.casosImpoExpos = casosImpoExpos;
	}

	public Hashtable<String, DatosDemandasCorrida> getCasosDemandas() {
		return casosDemandas;
	}

	public void setCasosDemandas(Hashtable<String, DatosDemandasCorrida> casosDemandas) {
		this.casosDemandas = casosDemandas;
	}

	public Hashtable<String, DatosFallasEscalonadasCorrida> getCasosFallas() {
		return casosFallas;
	}

	public void setCasosFallas(Hashtable<String, DatosFallasEscalonadasCorrida> casosFallas) {
		this.casosFallas = casosFallas;
	}

	public DatosRedElectricaCorrida getRed() {
		return red;
	}

	public void setRed(DatosRedElectricaCorrida red) {
		this.red = red;
	}

	public Hashtable<String, DatosCombustiblesCorrida> getCasosCombustibles() {
		return casosCombustibles;
	}

	public void setCasosCombustibles(Hashtable<String, DatosCombustiblesCorrida> casosCombustibles) {
		this.casosCombustibles = casosCombustibles;
	}

	public Hashtable<String, DatosAcumuladoresCorrida> getCasosAcumuladores() {
		return casosAcumuladores;
	}

	public void setCasosAcumuladores(Hashtable<String, DatosAcumuladoresCorrida> casosAcumuladores) {
		this.casosAcumuladores = casosAcumuladores;
	}

	public Hashtable<String, DatosConvertidoresCorrida> getCasosConvertidores() {
		return casosConvertidores;
	}

	public void setCasosConvertidores(Hashtable<String, DatosConvertidoresCorrida> casosConvertidores) {
		this.casosConvertidores = casosConvertidores;
	}

	public DatosLineaTiempo getLineaTiempo() {
		return lineaTiempo;
	}

	public void setLineaTiempo(DatosLineaTiempo lineaTiempo) {
		this.lineaTiempo = lineaTiempo;
	}

//	public Hashtable<String, Hashtable<String, DatosProcesoEstocastico>> getCasosProcesosEstocasticos() {
//		return casosProcesosEstocasticos;
//	}
//
//	public void setCasosProcesosEstocasticos(
//			Hashtable<String, Hashtable<String, DatosProcesoEstocastico>> casosProcesosEstocasticos) {
//		this.casosProcesosEstocasticos = casosProcesosEstocasticos;
//	}
	
	

	public DatosParamSalida getDatosParamSalida() {
		return datosParamSalida;
	}

	public Hashtable<String, DatosProcesoEstocastico> getProcesosEstocasticos() {
		return procesosEstocasticos;
	}

	public void setProcesosEstocasticos(Hashtable<String, DatosProcesoEstocastico> procesosEstocasticos) {
		this.procesosEstocasticos = procesosEstocasticos;
	}

	public void setDatosParamSalida(DatosParamSalida datosParamSalida) {
		this.datosParamSalida = datosParamSalida;
	}

	public DatosParamSalidaOpt getDatosParamSalidaOpt() {
		return datosParamSalidaOpt;
	}

	public void setDatosParamSalidaOpt(DatosParamSalidaOpt datosParamSalidaOpt) {
		this.datosParamSalidaOpt = datosParamSalidaOpt;
	}

	public DatosParamSalidaSim getDatosParamSalidaSim() {
		return datosParamSalidaSim;
	}

	public void setDatosParamSalidaSim(DatosParamSalidaSim datosParamSalidaSim) {
		this.datosParamSalidaSim = datosParamSalidaSim;
	}

	public double getTopeSpot() {
		return topeSpot;
	}

	public void setTopeSpot(double topeSpot) {
		this.topeSpot = topeSpot;
	}

	public boolean isDespSinExp() {
		return despSinExp;
	}

	public void setDespSinExp(boolean despSinExp) {
		this.despSinExp = despSinExp;
	}

	public int getIteracionSinExp() {
		return iteracionSinExp;
	}

	public void setIteracionSinExp(int iteracionSinExp) {
		this.iteracionSinExp = iteracionSinExp;
	}

	public ArrayList<String> getPaisesACortar() {
		return paisesACortar;
	}

	public void setPaisesACortar(ArrayList<String> paisesACortar) {
		this.paisesACortar = paisesACortar;
	}

	public Hashtable<String, DatosCiclosCombinadosCorrida> getCasosCiclosComb() {
		return casosCiclosComb;
	}

	public void setCasosCiclosComb(Hashtable<String, DatosCiclosCombinadosCorrida> casosCiclosComb) {
		this.casosCiclosComb = casosCiclosComb;
	}

	public Hashtable<String, DatosImpactosCorrida> getCasosImpactos() {
		return casosImpactos;
	}

	public void setCasosImpactos(Hashtable<String, DatosImpactosCorrida> casosImpactos) {
		this.casosImpactos = casosImpactos;
	}

	public Hashtable<String, DatosContratosEnergiaCorrida> getCasosContratosEner() {
		return casosContratosEner;
	}

	public void setCasosContratosEner(Hashtable<String, DatosContratosEnergiaCorrida> casosContratosEner) {
		this.casosContratosEner = casosContratosEner;
	}

	public Hashtable<String, DatosContratosInterrumpibleCorrida> getCasosContratosInt() {
		return casosContratosInt;
	}

	public void setCasosContratosInt(Hashtable<String, DatosContratosInterrumpibleCorrida> casosContratosInt) {
		this.casosContratosInt = casosContratosInt;
	}

	public Boolean getCostosVariables() {
		return costosVariables;
	}

	public void setCostosVariables(Boolean costosVariables) {
		this.costosVariables = costosVariables;
	}

	public DatosCorridaUTE getDatosCorridaUTE() {
		return datosCorridaUTE;
	}

	public void setDatosCorridaUTE(DatosCorridaUTE datosCorridaUTE) {
		this.datosCorridaUTE = datosCorridaUTE;
	}
	
	
	
}
