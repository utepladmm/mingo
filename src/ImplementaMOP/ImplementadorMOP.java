package ImplementaMOP;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import AlmacenSimulaciones.CreadorComandos;
import AlmacenSimulaciones.IdentSimulPaso;
import AlmacenSimulaciones.ImplementadorGeneral;
import GrafoEstados.PeriodoDeTiempo;
import GrafoEstados.ResSimul;
import ManejadorTextos.CreadorTex;
import ManejadorTextos.PlantillaEnTexto;
import TiposEnum.EstadosRec;
import UTE_CalculosEmpresa.UTE_escritorXML;
import UTE_contratos.DatosContratoInterrumpibleCorrida;
import UTE_contratos.DatosContratosInterrumpibleCorrida;
import UtilitariosGenerales.DevuelveString;
import UtilitariosGenerales.DirectoriosYArchivos;
import UtilitariosGenerales.ManejaObjetosEnDisco;
import UtilitariosGenerales.ParString;

import ImplementaMOP.DatosCorridaMingo;
import datatypes.*;
import datatypesProblema.*;
import datatypesProcEstocasticos.*;
import datatypesSalida.*;
import datatypesTiempo.*;
import tiempo.*;
import utilitarios.LectorPropiedades;
import utilitarios.UtilArrays;
import utilitarios.Utilitarios;
import utilitariosMingo.*;
import dominio.Acumulador;
import dominio.Biomasa;
import dominio.Bloque;
import dominio.ConjTiposDeDato;
import dominio.DatosGeneralesEstudio;
import dominio.Demanda;
import dominio.Estudio;
import dominio.Exportacion;
import dominio.Falla;
import dominio.GeneradorCicloCombinado;
import dominio.GeneradorEolico;
import dominio.GeneradorHidro;
import dominio.GeneradorFotovoltaico;
import dominio.GeneradorTermico;
import dominio.Importacion;
import dominio.ParTipoDatoValor;
import dominio.Parque;
import dominio.Recurso;
import dominio.RecursoBase;
import dominio.ResSimulDB;
import dominio.TiempoAbsoluto;
import dominio.TipoDeDatos;
import persistencia.EscritorXML;
import auxiliarMOP.CargadorXML;
import persistenciaMingo.LeerDatosArchivo;
import persistenciaMingo.XcargaDatos;

public class ImplementadorMOP extends ImplementadorGeneral implements Serializable{
	
	
	/**
	 * CONSTANTES DE LOS NOMBRES DE TIPOS DE DATOS
	 */
	private final static String TERMICOS = "termicos";
	private final static String CICLOS_COMBINADOS = "ciclosCombinados";
	private final static String HIDRAULICOS = "hidraulicos";
	private final static String EOLICOS = "eolicos";
	private final static String FOTOVOLTAICOS = "fotovoltaicos";
	private final static String IMPOEXPOS = "impoexpos";
	private final static String DEMANDAS = "demandas";
	private final static String FALLAS = "fallas";	
	private final static String COMBUSTIBLES = "combustibles";
	private final static String ACUMULADORES = "acumuladores";
//	private final static String PROCESOS_ESTOCASTICOS = "procesos_estocasticos";
	private final static String CARACTER_SEPARADOR = "\\$";
	
	
	/**
	 * Constantes de construcción de la línea de tiempo
	 */
	private final static Integer CANT_PASOS_B1 = 51;
	private final static Integer CANT_PASOS_B2 = 1;
	private final static Integer DUR_PASO_B1 = 604800;
	private final static Integer DUR_PASO_B2_NOBIS = 691200;
	private final static Integer DUR_PASO_B2_BIS = 777600;
	private final static Integer CANT_POSTES = 9;
	private final static Integer INT_MUESTREO = 3600;
	private final static Integer[] DURPOS_B1 = {3600,3600,3600,18000,36000,126000,126000,126000,162000};
	private final static Integer[] DURPOS_B2_NOBIS = {3600,3600,3600,25200,43200,144000,144000,144000,180000};
	private final static Integer[] DURPOS_B2_BIS = {3600,3600,3600,28800,50400,162000,162000,162000,201600};
	private final static Integer PERIODO_BLOQUE = 0;
	private final static boolean CRONOLOGICO = false;
	
	
	
	private String jarEjecutableMOP; // archivo ejecutable del MOP, se lee en config.txt
	

	private static DatosCorridaMingo datosMingo;
	private static DatosCorrida corridaOrigen;
	
	private static String instIniXmlMingo;  // instante de inicio del xmlCorridaMingo en formato fecha
	private static GregorianCalendar instIniXmlMingoGC;   // lo mismo GregorianCalendar
	
	private static GregorianCalendar instIniAnioCorr; // instante inicial de la corrida que se crea
	
	
	/**
	 * Diferencia en milisegundos 
	 * tiempo inicial de la corrida a crear menos tiempo inicial del xmlCorridaMingo 
	 */
	private static long deltaTiempo; 

	/**
	 * 
	 * Como todos los datos se leen de un único xml que carga datosMingo
	 * la dirección que se ponga a cada tipo de datos en defTipoDeDatos.txt es irrelevante.
	 * Solo se consideran de ese archivo los nombres de los tipos de datos.
	 * 
	 */


	/**
	 * Para cada conjunto de centrales hidráulicas, indica el nombre de la configuración asociada
	 * clave String con los nombres sucesivos de las hidráulicas en orden alfabético, concatenados y separados
	 * por coma.
	 * valor String con el nombre de la configuración asociada que es el nombre del caso de la configuración 
	 * 
	 */
	private Hashtable<String, String> nombreConfiguracionesHidro;
	
	/**
	 * Directorio donde deben crearse los subdirectorios, uno para cada paquete, que guardan los xml de las corridas a ejecutar en el paquete,
	 * Los subdirectorios se forman con /Pn, donde n ese el número de paquete.
	 */
	private String rutaEntradaParalelizador; 

	/**
	 * Directorio donde deben crearse los subdirectorios, uno para cada paquete, que guardan los archivos de salida de las corridas a ejecutar en el paquete,
	 * Los subdirectorios se forman con /Pn, donde n ese el número de paquete.
	 */	
	private String rutaSalidaParalelizador; // directorio donde deben crearse los archivos de salida de las corridas a ejecutar, se lee en config.txt
	
	private LineaTiempo lt;
	
	private Document dom;


	public ImplementadorMOP (){
		nombreConfiguracionesHidro = new Hashtable<String, String>();
		datosMingo = new DatosCorridaMingo();
		LectorPropiedades lp = new LectorPropiedades(".\\resources\\MINGO.conf");
		try {
			jarEjecutableMOP = lp.getProp("jarEjecutableMOP");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	/**
	 **************************************************
	 *
	 * COMIENZAN LOS MÉTODOS QUE DEBEN SOBREESCRIBIRSE
	 * IMPLEMENTADOR GENERAL
	 *
	 **************************************************
	 */

	public void inicializaImplementador(String dir_Datos_Implementacion,
			String dir_Expansion, String dir_Corridas) throws XcargaDatos{

		super.inicializaImplementador(dir_Datos_Implementacion, dir_Expansion, dir_Corridas);

		// Carga directorio raíz del que se cuelgan los subdirectorios con los .xml que leerá el paralelizador
		rutaEntradaParalelizador = dir_Corridas;
		
		// Carga directorio raíz del que cuelgan los directorios de salidas de los paquetes de corridas
		rutaSalidaParalelizador = dir_Expansion + "/resultados";
		
		// Lee las configuraciones de los hidráulicos posibles
		String dirArchivo = dir_Datos_Implementacion + "/" + "configuracionesHidro.txt";
		ArrayList<ArrayList<String>> configH = LeerDatosArchivo.leerTextoSep(dirArchivo, " ");
		for(int i=0; i<configH.size(); i++){
			String valor = configH.get(i).get(0);
			ArrayList<String> nombresH = new ArrayList<String>(); 
			for(int j=1; j<configH.get(i).size(); j++){
				nombresH.add(configH.get(i).get(j));
			}
			String clave = claveDeNombres(nombresH);
			nombreConfiguracionesHidro.put(clave, valor);    		
		}
		String dirXmlOrigen = dir_Expansion + "/Estudio/" + "xmlCorridaMingo.xml";
		CargadorXML loader = new CargadorXML();
		loader.setCopiadoPortapapeles(true);
		DatosCorrida datosOrigen = loader.cargarCorrida(dirXmlOrigen);
		
		/**
		 * Construye el DatosCorridaMingo datosMingo a partir del DatosCorrida datosOrigen
		 * ATENCIÓN: ALTERA EL DATATYPE DE ORIGEN !!!!!!
		 */
		
		instIniXmlMingo = datosOrigen.getInicioCorrida();
		instIniXmlMingoGC = LineaTiempo.dameGregorianCDeFechaXml(instIniXmlMingo);
		
		datosMingo.setNombre(datosOrigen.getNombre());
		datosMingo.setDescripcion(datosOrigen.getDescripcion());
		datosMingo.setPostizacion(datosOrigen.getPostizacion());
		datosMingo.setTipoPostizacion(datosOrigen.getTipoPostizacion());
		datosMingo.setTipoSimulacion(datosOrigen.getTipoSimulacion());
		datosMingo.setValPostizacion(datosOrigen.getValPostizacion());
		datosMingo.setTipoValpostizacion(datosOrigen.getTipoValpostizacion());
		datosMingo.setRuta(datosOrigen.getRuta());
		datosMingo.setTasa(datosOrigen.getTasa());
		datosMingo.setSemilla(datosOrigen.getSemilla());
		
		datosMingo.setTopeSpot(datosOrigen.getTopeSpot());
		datosMingo.setDespSinExp(datosOrigen.isDespSinExp());
		datosMingo.setIteracionSinExp(datosOrigen.getIteracionSinExp());
		datosMingo.setPaisesACortar(datosOrigen.getPaisesACortar());
		datosMingo.setSemilla(datosOrigen.getSemilla());
		datosMingo.setRutaSals(datosOrigen.getRutaSals());
		datosMingo.setCantEscenarios(datosOrigen.getCantEscenarios());
		datosMingo.setCantSorteosMont(datosOrigen.getCantSorteosMont());
		datosMingo.setValoresComportamientoGlobal(datosOrigen.getValoresComportamientoGlobal());
		datosMingo.setDatosIteraciones(datosOrigen.getDatosIteraciones());
		datosMingo.setLineaTiempo(datosOrigen.getLineaTiempo());
		datosMingo.setDatosParamSalida(datosOrigen.getDatosParamSalida());
		datosMingo.setDatosParamSalidaOpt(datosOrigen.getDatosParamSalidaOpt());
		datosMingo.setDatosParamSalidaSim(datosOrigen.getDatosParamSalidaSim());
		datosMingo.setValoresComportamientoGlobal(datosOrigen.getValoresComportamientoGlobal());
		datosMingo.setLineaTiempo(datosOrigen.getLineaTiempo());
	
		
		/**
		 * Construye los casos de datos térmicos, hidráulicos, etc. parseando los nombres compuestos (caso, nombre) de datosOrigen		
		 */
		Hashtable<String, DatosTermicosCorrida> casosTermicos = construyeCasosDatosTermicos(datosOrigen);
		datosMingo.setCasosTermicos(casosTermicos);
				
		Hashtable<String, DatosCiclosCombinadosCorrida> casosCiclos = construyeCasosDatosCiclosComb(datosOrigen);
		datosMingo.setCasosCiclosComb(casosCiclos);

		Hashtable<String, DatosHidraulicosCorrida> casosHidraulicos = construyeCasosDatosHidraulicos(datosOrigen);
		datosMingo.setCasosHidraulicos(casosHidraulicos);

		Hashtable<String, DatosEolicosCorrida> casosEolicos = construyeCasosDatosEolicos(datosOrigen);
		datosMingo.setCasosEolicos(casosEolicos);
		
		Hashtable<String, DatosFotovoltaicosCorrida> casosFotovoltaicos = construyeCasosDatosFotovoltaicos(datosOrigen);
		datosMingo.setCasosFotovoltaicos(casosFotovoltaicos);		
		
		Hashtable<String, DatosImpoExposCorrida> casosImpoexpos = construyeCasosDatosImpoExpos(datosOrigen);
		datosMingo.setCasosImpoExpos(casosImpoexpos);	
		
		Hashtable<String, DatosDemandasCorrida> casosDemandas = construyeCasosDatosDemandas(datosOrigen);
		datosMingo.setCasosDemandas(casosDemandas);	

		Hashtable<String, DatosFallasEscalonadasCorrida> casosFallas = construyeCasosDatosFallas(datosOrigen);
		datosMingo.setCasosFallas(casosFallas);			

		Hashtable<String, DatosImpactosCorrida> casosImpactos = construyeCasosDatosImpactos(datosOrigen);
		datosMingo.setCasosImpactos(casosImpactos);				
		
		Hashtable<String, DatosContratosEnergiaCorrida> casosContratosEnergia = construyeCasosDatosContratosEnergia(datosOrigen);
		datosMingo.setCasosContratosEner(casosContratosEnergia);			
		
		Hashtable<String, DatosContratosInterrumpibleCorrida> casosContratosInt = construyeCasosDatosContratosInt(datosOrigen);
		datosMingo.setCasosContratosInt(casosContratosInt);				
		
		Hashtable<String, DatosCombustiblesCorrida> casosContratosComb = construyeCasosDatosCombustibles(datosOrigen);
		datosMingo.setCasosCombustibles(casosContratosComb);	

		Hashtable<String, DatosAcumuladoresCorrida> casosAcumuladores = construyeCasosDatosAcumuladores(datosOrigen);
		datosMingo.setCasosAcumuladores(casosAcumuladores);	
		
		// Construye datos de red eléctrica		
		datosMingo.setRed(datosOrigen.getRed());
		
				
		datosMingo.setProcesosEstocasticos(datosOrigen.getProcesosEstocasticos());					
		System.out.println("Termina la construcción de casos de datosMingo");
		
		datosMingo.setDatosCorridaUTE(datosOrigen.getDatosUTE());
		
	}
	
	
	
	private Hashtable<String, DatosTermicosCorrida> construyeCasosDatosTermicos(DatosCorrida datosOrigen) {		
		DatosTermicosCorrida dtOrigen = datosOrigen.getTermicos();
		Hashtable<String, DatosTermicoCorrida> termicosOrigen = dtOrigen.getTermicos();
		Hashtable<String, DatosTermicosCorrida> casosTermicos = new Hashtable<String,DatosTermicosCorrida>();
		ArrayList<String> nombresDeCasos = new ArrayList<String>();
		Set<String> nombresTer = termicosOrigen.keySet();
		Iterator it = nombresTer.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1termico = par.second;
			DatosTermicosCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosTermicosCorrida();
				caso.setAtribtosDetallados(dtOrigen.getAtribtosDetallados());
				caso.setValoresComportamiento(dtOrigen.getValoresComportamiento());
				casosTermicos.put(n1caso, caso);
			}else{
				caso = casosTermicos.get(n1caso);
			}
			termicosOrigen.get(nombreCompuesto).setNombre(n1termico);
			caso.getTermicos().put(n1termico, termicosOrigen.get(nombreCompuesto) );
			
			// kk OJO QUE EN EL datatype de un termico queda mal el nombre, le sobra el $nombre del caso$
		}
		return casosTermicos;		
	}
	
	
	private Hashtable<String, DatosCiclosCombinadosCorrida> construyeCasosDatosCiclosComb(DatosCorrida datosOrigen){
		DatosCiclosCombinadosCorrida dccOrigen = datosOrigen.getCcombinados();
		Hashtable<String, DatosCicloCombinadoCorrida> ciclosOrigen = dccOrigen.getCcombinados();
		Hashtable<String, DatosCiclosCombinadosCorrida> casosCiclos = new Hashtable<String,DatosCiclosCombinadosCorrida>();
		ArrayList<String>nombresDeCasos = new ArrayList<String>();
		Set<String> nombresCC = ciclosOrigen.keySet();
		Iterator it = nombresCC.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1CC = par.second;
			DatosCiclosCombinadosCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosCiclosCombinadosCorrida();
				caso.setAtribtosDetallados(dccOrigen.getAtribtosDetallados());
				caso.setValoresComportamiento(dccOrigen.getValoresComportamiento());
				casosCiclos.put(n1caso, caso);
			}else{
				caso = casosCiclos.get(n1caso);
			}
			ciclosOrigen.get(nombreCompuesto).setNombre(n1CC);
			caso.getCcombinados().put(n1CC, ciclosOrigen.get(nombreCompuesto));
			
		}	
//		dccOrigen.setAtribtosDetallados(datosOrigen.getCcombinados().getAtribtosDetallados());
//		dccOrigen.setOrdenCargaXML(datosOrigen.getCcombinados().getOrdenCargaXML());
//		dccOrigen.setValoresComportamiento(datosOrigen.getCcombinados().getValoresComportamiento());
		
		return casosCiclos;
		
	}
	
	
	private Hashtable<String, DatosHidraulicosCorrida> construyeCasosDatosHidraulicos(DatosCorrida datosOrigen) {		
	
		DatosHidraulicosCorrida dhOrigen = datosOrigen.getHidraulicos();
		Hashtable<String, DatosHidraulicoCorrida> hidraulicosOrigen = dhOrigen.getHidraulicos();
		Hashtable<String, DatosHidraulicosCorrida> casosHidraulicos = new Hashtable<String,DatosHidraulicosCorrida>();
		ArrayList<String> nombresDeCasos = new ArrayList<String>();
		Set<String> nombresHid = hidraulicosOrigen.keySet();
		Iterator it = nombresHid.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1hidraulico = par.second;
			DatosHidraulicosCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosHidraulicosCorrida();
				caso.setAtribtosDetallados(dhOrigen.getAtribtosDetallados());
				caso.setValoresComportamiento(dhOrigen.getValoresComportamiento());
				casosHidraulicos.put(n1caso, caso);
			}else{
				caso = casosHidraulicos.get(n1caso);
			}
			hidraulicosOrigen.get(nombreCompuesto).setNombre(n1hidraulico);
			caso.getHidraulicos().put(n1hidraulico, hidraulicosOrigen.get(nombreCompuesto) );
		}
//		dhOrigen.setAtribtosDetallados(datosOrigen.getHidraulicos().getAtribtosDetallados());
//		dhOrigen.setOrdenCargaXML(datosOrigen.getCcombinados().getOrdenCargaXML());
//		dhOrigen.setValoresComportamiento(datosOrigen.getValoresComportamientoGlobal());
		return casosHidraulicos;
	
	}
	
	
	private Hashtable<String, DatosEolicosCorrida> construyeCasosDatosEolicos(DatosCorrida datosOrigen) {	
		
		DatosEolicosCorrida deOrigen = datosOrigen.getEolicos();
		Hashtable<String, DatosEolicoCorrida> eolicosOrigen = deOrigen.getEolicos();
		Hashtable<String, DatosEolicosCorrida> casosEolicos = new Hashtable<String,DatosEolicosCorrida>();
		ArrayList<String> nombresDeCasos = new ArrayList<String>();
		Set<String> nombresEol = eolicosOrigen.keySet();
		Iterator it = nombresEol.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1eolico = par.second;
			DatosEolicosCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosEolicosCorrida();
				caso.setAtributosDetallados(deOrigen.getAtributosDetallados());
				caso.setValoresComportamiento(deOrigen.getValoresComportamiento());
				casosEolicos.put(n1caso, caso);
			}else{
				caso = casosEolicos.get(n1caso);
			}
			eolicosOrigen.get(nombreCompuesto).setNombre(n1eolico);
			caso.getEolicos().put(n1eolico, eolicosOrigen.get(nombreCompuesto) );
		}	
//		deOrigen.setAtributosDetallados(datosOrigen.getEolicos().getAtributosDetallados());
//		deOrigen.setOrdenCargaXML(datosOrigen.getEolicos().getOrdenCargaXML());
//		deOrigen.setValoresComportamiento(datosOrigen.getEolicos().getValoresComportamiento());
		
		return casosEolicos;
	}
	
	
	private Hashtable<String, DatosFotovoltaicosCorrida> construyeCasosDatosFotovoltaicos(DatosCorrida datosOrigen) {	
		DatosFotovoltaicosCorrida dfOrigen = datosOrigen.getFotovoltaicos();
		Hashtable<String, DatosFotovoltaicoCorrida> fotovoltaicosOrigen = dfOrigen.getFotovoltaicos();
		Hashtable<String, DatosFotovoltaicosCorrida> casosFotovoltaicos = new Hashtable<String,DatosFotovoltaicosCorrida>();
		ArrayList<String> nombresDeCasos = new ArrayList<String>();
		Set<String> nombresFoto = fotovoltaicosOrigen.keySet();
		Iterator it = nombresFoto.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1foto = par.second;
			DatosFotovoltaicosCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosFotovoltaicosCorrida();
				caso.setAtributosDetallados(dfOrigen.getAtributosDetallados());
				caso.setValoresComportamiento(dfOrigen.getValoresComportamiento());
				casosFotovoltaicos.put(n1caso, caso);
			}else{
				caso = casosFotovoltaicos.get(n1caso);
			}
			fotovoltaicosOrigen.get(nombreCompuesto).setNombre(n1foto);
			caso.getFotovoltaicos().put(n1foto, fotovoltaicosOrigen.get(nombreCompuesto) );
		}
//		dfOrigen.setAtributosDetallados(datosOrigen.getFotovoltaicos().getAtributosDetallados());
//		dfOrigen.setOrdenCargaXML(datosOrigen.getFotovoltaicos().getOrdenCargaXML());
//		dfOrigen.setValoresComportamiento(datosOrigen.getFotovoltaicos().getValoresComportamiento());
		return casosFotovoltaicos;
	}
	
	
	
	private Hashtable<String, DatosImpoExposCorrida> construyeCasosDatosImpoExpos(DatosCorrida datosOrigen) {	
		DatosImpoExposCorrida diOrigen = datosOrigen.getImpoExpos();
		Hashtable<String, DatosImpoExpoCorrida> impoexposOrigen = diOrigen.getImpoExpos();
		Hashtable<String, DatosImpoExposCorrida> casosImpoexpos = new Hashtable<String,DatosImpoExposCorrida>();
		ArrayList<String> nombresDeCasos = new ArrayList<String>();
		Set<String> nombresIE = impoexposOrigen.keySet();
		Iterator it = nombresIE.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1ie = par.second;
			DatosImpoExposCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosImpoExposCorrida();
				caso.setAtributosDetallados(diOrigen.getAtributosDetallados());
				caso.setValoresComportamiento(diOrigen.getValoresComportamiento());
				casosImpoexpos.put(n1caso, caso);
			}else{
				caso = casosImpoexpos.get(n1caso);
			}
			impoexposOrigen.get(nombreCompuesto).setNombre(n1ie);
			caso.getImpoExpos().put(n1ie, impoexposOrigen.get(nombreCompuesto) );
		}
//		diOrigen.setAtributosDetallados(datosOrigen.getImpoExpos().getAtributosDetallados());
//		diOrigen.setOrdenCargaXML(datosOrigen.getImpoExpos().getOrdenCargaXML());
//		diOrigen.setValoresComportamiento(datosOrigen.getImpoExpos().getValoresComportamiento());
		return casosImpoexpos;
	}
	
	
	
	private Hashtable<String, DatosDemandasCorrida> construyeCasosDatosDemandas(DatosCorrida datosOrigen) {	
		DatosDemandasCorrida ddOrigen = datosOrigen.getDemandas();
		Hashtable<String, DatosDemandaCorrida> demandasOrigen = ddOrigen.getDemandas();
		Hashtable<String, DatosDemandasCorrida> casosDemandas = new Hashtable<String,DatosDemandasCorrida>();
		ArrayList<String> nombresDeCasos = new ArrayList<String>();
		Set<String> nombresD = demandasOrigen.keySet();
		Iterator it = nombresD.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1dem = par.second;
			DatosDemandasCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosDemandasCorrida();
				caso.setAtributosDetallados(ddOrigen.getAtributosDetallados());
				caso.setValoresComportamiento(ddOrigen.getValoresComportamiento());
				casosDemandas.put(n1caso, caso);
			}else{
				caso = casosDemandas.get(n1caso);
			}
			demandasOrigen.get(nombreCompuesto).setNombre(n1dem);
			caso.getDemandas().put(n1dem, demandasOrigen.get(nombreCompuesto) );
		}
//		ddOrigen.setAtributosDetallados(datosOrigen.getDemandas().getAtributosDetallados());
//		ddOrigen.setOrdenCargaXML(datosOrigen.getDemandas().getOrdenCargaXML());
//		ddOrigen.setValoresComportamiento(datosOrigen.getDemandas().getValoresComportamiento());
		return casosDemandas;
	}
	
	
	private Hashtable<String, DatosFallasEscalonadasCorrida> construyeCasosDatosFallas(DatosCorrida datosOrigen) {	
		
		DatosFallasEscalonadasCorrida dfaOrigen = datosOrigen.getFallas();
		Hashtable<String, DatosFallaEscalonadaCorrida> fallasOrigen = dfaOrigen.getFallas();
		Hashtable<String, DatosFallasEscalonadasCorrida> casosFallas = new Hashtable<String,DatosFallasEscalonadasCorrida>();
		ArrayList<String> nombresDeCasos = new ArrayList<String>();
		Set<String> nombresFa = fallasOrigen.keySet();
		Iterator it = nombresFa.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1falla = par.second;
			DatosFallasEscalonadasCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosFallasEscalonadasCorrida();
				caso.setAtributosDetallados(dfaOrigen.getAtributosDetallados());
				caso.setValoresComportamiento(dfaOrigen.getValoresComportamiento());
				casosFallas.put(n1caso, caso);
			}else{
				caso = casosFallas.get(n1caso);
			}
			fallasOrigen.get(nombreCompuesto).setNombre(n1falla);
			caso.getFallas().put(n1falla, fallasOrigen.get(nombreCompuesto) );
		}
//		dfaOrigen.setAtributosDetallados(datosOrigen.getFallas().getAtributosDetallados());
//		dfaOrigen.setOrdenCargaXML(datosOrigen.getFallas().getOrdenCargaXML());
//		dfaOrigen.setValoresComportamiento(datosOrigen.getFallas().getValoresComportamiento());
		return casosFallas;
	}
	
	
	private  Hashtable<String, DatosContratosEnergiaCorrida> construyeCasosDatosContratosEnergia(DatosCorrida datosOrigen){

		DatosContratosEnergiaCorrida dceOrigen = datosOrigen.getContratosEnergia();
		Hashtable<String, DatosContratoEnergiaCorrida> ContratosEnergiaOrigen = dceOrigen.getContratosEnergia();
		Hashtable<String, DatosContratosEnergiaCorrida> casosContratosEnergia = new Hashtable<String,DatosContratosEnergiaCorrida>();
		ArrayList<String> nombresDeCasos = new ArrayList<String>();
		Set<String> nombresCE = ContratosEnergiaOrigen.keySet();
		Iterator it = nombresCE.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1ContratoEnergia = par.second;
			DatosContratosEnergiaCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosContratosEnergiaCorrida();
				caso.setAtributosDetallados(dceOrigen.getAtributosDetallados());
				casosContratosEnergia.put(n1caso, caso);
			}else{
				caso = casosContratosEnergia.get(n1caso);
			}
			ContratosEnergiaOrigen.get(nombreCompuesto).setNombre(n1ContratoEnergia);
			caso.getContratosEnergia().put(n1ContratoEnergia, ContratosEnergiaOrigen.get(nombreCompuesto) );		
			// kk OJO QUE EN EL datatype de un ContratoEnergia queda mal el nombre, le sobra el $nombre del caso$
		}
//		dceOrigen.setAtributosDetallados(datosOrigen.getContratosEnergia().getAtributosDetallados());
//		dceOrigen.setOrdenCargaXML(datosOrigen.getContratosEnergia().getOrdenCargaXML());
		return casosContratosEnergia;
	}
	
	
	
	
	private Hashtable<String, DatosImpactosCorrida> construyeCasosDatosImpactos(DatosCorrida datosOrigen) {		
		DatosImpactosCorrida dimpOrigen = datosOrigen.getImpactos();
		Hashtable<String, DatosImpactoCorrida> impactosOrigen = dimpOrigen.getImpactos();
		Hashtable<String, DatosImpactosCorrida> casosImpactos = new Hashtable<String,DatosImpactosCorrida>();
		ArrayList<String> nombresDeCasos = new ArrayList<String>();
		Set<String> nombresImp = impactosOrigen.keySet();
		Iterator it = nombresImp.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1Imp = par.second;
			DatosImpactosCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosImpactosCorrida();
				caso.setAtributosDetallados(dimpOrigen.getAtributosDetallados());
				casosImpactos.put(n1caso, caso);
			}else{
				caso = casosImpactos.get(n1caso);
			}
			impactosOrigen.get(nombreCompuesto).setNombre(n1Imp);
			caso.getImpactos().put(n1Imp, impactosOrigen.get(nombreCompuesto));
			
			// kk OJO QUE EN EL datatype de un termico queda mal el nombre, le sobra el $nombre del caso$
		}
//		dimpOrigen.setAtributosDetallados(datosOrigen.getImpactos().getAtributosDetallados());
//		dimpOrigen.setOrdenCargaXML(datosOrigen.getImpactos().getOrdenCargaXML());
		return casosImpactos;
	}
	
	
	
	private Hashtable<String, DatosContratosInterrumpibleCorrida> construyeCasosDatosContratosInt(DatosCorrida datosOrigen){		
		DatosContratosInterrumpibleCorrida dciOrigen = datosOrigen.getContratosInterrumpibles();
		Hashtable<String, DatosContratoInterrumpibleCorrida> contratosInterrumpibleOrigen = dciOrigen.getContratosInterrumpibles();
		Hashtable<String, DatosContratosInterrumpibleCorrida> casosContratosInterrumpibles = new Hashtable<String,DatosContratosInterrumpibleCorrida>();
		ArrayList<String> nombresDeCasos = new ArrayList<String>();
		Set<String> nombresCI = contratosInterrumpibleOrigen.keySet();
		Iterator it = nombresCI.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1ContratoInterrumpible = par.second;
			DatosContratosInterrumpibleCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new 	DatosContratosInterrumpibleCorrida();
				caso.setAtributosDetallados(dciOrigen.getAtributosDetallados());
				casosContratosInterrumpibles.put(n1caso, caso);
			}else{
				caso = casosContratosInterrumpibles.get(n1caso);
			}
			contratosInterrumpibleOrigen.get(nombreCompuesto).setNombre(n1ContratoInterrumpible);
			caso.getContratosInterrumpibles().put(n1ContratoInterrumpible, contratosInterrumpibleOrigen.get(nombreCompuesto) );
			
			// kk OJO QUE EN EL datatype de un ContratoEnergia queda mal el nombre, le sobra el $nombre del caso$
		}	
//		dciOrigen.setAtributosDetallados(datosOrigen.getContratosInterrumpibles().getAtributosDetallados());
//		dciOrigen.setOrdenCargaXML(datosOrigen.getContratosInterrumpibles().getAtributosDetallados());
		return casosContratosInterrumpibles;
	}
	
	private Hashtable<String, DatosCombustiblesCorrida> construyeCasosDatosCombustibles(DatosCorrida datosOrigen){
		
		DatosCombustiblesCorrida dcOrigen = datosOrigen.getCombustibles();
		Hashtable<String, DatosCombustibleCorrida> combustiblesOrigen = dcOrigen.getCombustibles();
		Hashtable<String, DatosCombustiblesCorrida> casosCombustibles = new Hashtable<String,DatosCombustiblesCorrida>();
		 ArrayList<String>nombresDeCasos = new ArrayList<String>();
		Set<String> nombresComb = combustiblesOrigen.keySet();
		Iterator it = nombresComb.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1comb = par.second;
			DatosCombustiblesCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosCombustiblesCorrida();
				caso.setValoresComportamiento(dcOrigen.getValoresComportamiento());
				casosCombustibles.put(n1caso, caso);
			}else{
				caso = casosCombustibles.get(n1caso);
			}
			combustiblesOrigen.get(nombreCompuesto).setNombre(n1comb);
			caso.getCombustibles().put(n1comb, combustiblesOrigen.get(nombreCompuesto) );
		}
//		dcOrigen.setOrdenCargaXML(datosOrigen.getCombustibles().getOrdenCargaXML());
//		dcOrigen.setValoresComportamiento(datosOrigen.getCombustibles().getValoresComportamiento());
		return casosCombustibles;
	}
	
	
	private Hashtable<String, DatosAcumuladoresCorrida>construyeCasosDatosAcumuladores(DatosCorrida datosOrigen){
		DatosAcumuladoresCorrida daOrigen = datosOrigen.getAcumuladores();
		Hashtable<String, DatosAcumuladorCorrida> acumuladoresOrigen = daOrigen.getAcumuladores();
		Hashtable<String, DatosAcumuladoresCorrida> casosAcumuladores = new Hashtable<String,DatosAcumuladoresCorrida>();
		ArrayList<String> nombresDeCasos = new ArrayList<String>();
		Set<String> nombresAcum = acumuladoresOrigen.keySet();
		Iterator it = nombresAcum.iterator();		
		while (it.hasNext()){
			String nombreCompuesto = (String)it.next();
			Pair<String,String> par = devuelveCasoYNombre(nombreCompuesto);
			String n1caso = par.first;
			String n1acum = par.second;
			DatosAcumuladoresCorrida caso;
			if(!nombresDeCasos.contains(n1caso)){ 
				// aparece un nuevo caso
				nombresDeCasos.add(par.first);
				caso = new DatosAcumuladoresCorrida();
				caso.setAtributosDetallados(daOrigen.getAtributosDetallados());
				caso.setValoresComportamiento(daOrigen.getValoresComportamiento());
				casosAcumuladores.put(n1caso, caso);
			}else{
				caso = casosAcumuladores.get(n1caso);
			}
			acumuladoresOrigen.get(nombreCompuesto).setNombre(n1acum);
			caso.getAcumuladores().put(n1acum, acumuladoresOrigen.get(nombreCompuesto) );
		}
//		daOrigen.setAtributosDetallados(datosOrigen.getAcumuladores().getAtributosDetallados());
//		daOrigen.setOrdenCargaXML(datosOrigen.getAcumuladores().getOrdenCargaXML());
//		daOrigen.setValoresComportamiento(datosOrigen.getAcumuladores().getValoresComportamiento());
		return casosAcumuladores;
	}
	

	
	
	/**
	 * A partir de un String con el nombre de un participante precedido de 
	 * la etiqueta $nombreCaso$ devuelve el Pair (nombreCaso, nombre del Participante)
	 */
	public Pair<String, String> devuelveCasoYNombre(String nombreCompuesto){
		String[] partes = nombreCompuesto.split(CARACTER_SEPARADOR);
		// EL PESOS INICIAL QUEDA EN EL PRIMER STRING
		if (partes[0].equalsIgnoreCase("CARACTER_SEPARADOR") || partes.length!=3 ){
			System.out.println("Error en el nombre de caso en lectura del xmlMingo: " + nombreCompuesto);
			System.exit(1);
		}
		Pair<String, String> par = new Pair(partes[1], partes[2]);
		return par;
	}

	/**
	 * Construye una clave para identificar un caso hidráulico con los nombres
	 * de las hidráulicas existentes en el parque ordenados alfabéticamente y separados por blanco
	 * @param nombres
	 * @param separador
	 * @return
	 */
	public String claveDeNombres(ArrayList<String> nombres){
		String clave = "";
		Collections.sort(nombres);
		for(int j=0; j<nombres.size(); j++){
			clave = clave + nombres.get(j);
			if(j<nombres.size()-1) clave = clave + " ";
		}
		return clave;
	}


	/**
	 * El método genera los resultados de una corrida correspondientes a un
	 * PeriodoDeTiempo
	 *
	 * ESTE METODO DEBE SER SOBREESCRITO PARA LAS CLASES QUE HEREDAN DE ESTA
	 * PARA CADA IMPLEMENTACION CONCRETA PARA UN MODELO DE OPERACION
	 *
	 * @param dirCorrida es el path del directorio donde están los resultados
	 * de la corrida
	 * @param per es el PeriodoDeTiempo para el que se creará el resultado.
	 * @param informacion es la información empaquetada que puedan precisar
	 * los métodos de las clases que heredan de esta.
	 * @return es el ResSimulPaso del paso respectivo.
	 */

	//    kk ojo debe leer resultados MOP
	//    ojo hay que agregar una columna inicial en costocron con el costo total por crónica
	public ResSimul creaRes(String dirCorrida, PeriodoDeTiempo per, Estudio est) throws XcargaDatos{
		String dirCostocron = dirCorrida + "/costocron.xlt";


		String sep = "\t";
		TiempoAbsoluto ta = (TiempoAbsoluto) per;
		int cantCronicas = est.getDatosGenerales().getCantCronicas();
		boolean encontroAnio = false;
		ArrayList<ArrayList<String>> costoCron = null;
		ArrayList<ArrayList<String>> sobreCostos = null;        
		boolean sigo = true;
		while (sigo) {
			try {
				costoCron = CreadorTex.leeTextoBase(dirCostocron, sep);
				sigo = false;
			} catch (XcargaDatos ex) {
				sigo = false;
				System.out.println("Error en la lectura de costocron en directorio " + dirCorrida);
				int levantar = JOptionPane.showConfirmDialog(null, "Error en costocron, cuando quiera siga");
				if (levantar == JOptionPane.OK_OPTION) {
					sigo = true;
				} else {
					throw new XcargaDatos("Error en la lectura de costocron en directorio " + dirCorrida);
				}
			}
		}

		int anio = ta.getAnio();
		String anioStr = String.valueOf(anio);
		String anioSigStr = String.valueOf(anio + 1);

		/**        
		 * Lee costocron.xlt 
		 */ 
		int i = 0;
		do {
			if (costoCron.get(i).get(0).equalsIgnoreCase("Año") && costoCron.get(i).get(1).equalsIgnoreCase(anioStr)) {
				encontroAnio = true;
			}
			i++;
		} while (i < costoCron.size() && !encontroAnio);
		if (!encontroAnio) {
			throw new XcargaDatos("No se encontró el año " + anio
					+ " en archivo costocron del directorio " + dirCostocron);
		}
		// el i queda posicionado en la primera línea posterior al año y se suma 1 para evitar títulos
		i++;
		/**
		 * Suma la totalidad de los costos de cada cr�nica y los agrega a un array
		 * previa divisi�n por 1000 para pasar de kUSD a MUSD.
		 */

		double sumacron;
		int ultCron = 0;
		ArrayList<Double> result = new ArrayList<Double>();
		for (int icron = 0; icron < cantCronicas; icron++) {
			sumacron = 0.0;
			if (costoCron.get(i).get(0).equalsIgnoreCase(anioSigStr)) {
				throw new XcargaDatos("El archivo costocron tiene menos cr�nicas que las especificadas");
			}
			for (int j = 1; j < costoCron.get(i).size(); j++) {
				sumacron = sumacron + Double.parseDouble(costoCron.get(i).get(j));
			}
			result.add(sumacron / 1000);

			ultCron = Integer.parseInt(costoCron.get(i).get(0));
			i++;  // avanza el índice en las líneas del archivo leído
		}
		/**
		 *  i quedo posicionado luego de la �ltima línea de costrocron con la cr�nica
		 *  del año
		 */
		if (i < costoCron.size()) {
			// hay más datos despu�s del �ltimo leído
			int loDeDespues = (Integer.parseInt(costoCron.get(i).get(0)));
			if (loDeDespues == ultCron + 1) {
				throw new XcargaDatos("El archivo costocron tiene más cr�nicas que las especificadas");
			}
		}


		/**        
		 * Lee sobrecostos_gnl.xlt 
		 * ATENCION LOS COMENTARIOS ABAJO NO DEBEN SER BORRADOS
		 */   
		//        sigo = true;
		//        while (sigo) {
		//            try {
		//                sobreCostos = CreadorTex.leeTextoBase(dirSobrecostos, sep);
		//                sigo = false;
		//            } catch (XcargaDatos ex) {
		//                sigo = false;
		//                System.out.println("Error en la lectura de sobrecostos_gnl en directorio " + dirCorrida);
		//                int levantar = JOptionPane.showConfirmDialog(null, "Error en sobrecostos_gnl, cuando quiera siga");
		//                if (levantar == JOptionPane.OK_OPTION) {
		//                    sigo = true;
		//                } else {
		//                    throw new XcargaDatos("Error en la lectura de sobrecostos_gnl en directorio " + dirCorrida);
		//                }
		//            }
		//        }
		//       try {
		//           sobreCostos = CreadorTex.leeTextoBase(dirSobrecostos, sep);
		//       } catch (XcargaDatos ex) {
		//           throw new XcargaDatos("Error en la lectura de sobrecostos de GNL en directorio " + dirCorrida);
		//       }        

		//        i = 0;
		//        do {
		//            if (sobreCostos.get(i).get(0).equalsIgnoreCase(anioStr)) {
		//                encontroAnio = true;
		//            }
		//            i++;
		//        } while (i < sobreCostos.size() && !encontroAnio);
		//        if (!encontroAnio) {
		//            throw new XcargaDatos("No se encontr� el año " + anio
		//                    + " en archivo sobrecostos_gnl del directorio " + dirCostocron);
		//        }
		//        // el i queda posicionado en la primera línea posterior al año que es el primer dato
		//        for (int icron = 0; icron < cantCronicas; icron++) {
		//            // divide por 1000 para pasar de kUSD a MUSD
		//            result.set(icron, result.get(icron)+ Double.parseDouble(sobreCostos.get(i).get(1))/1000);
		//            i++;  // avanza el índice en las líneas del archivo leído
		//        }        

		/**
		 * Se carga el resultado
		 */
		ResSimul res = new ResSimul();
		res.setPer(ta);
		double[][] array2Doble = new double[1][cantCronicas];
		double[] aprobs = new double[cantCronicas];
		for (int j = 0; j < cantCronicas; j++) {
			array2Doble[0][j] = result.get(j);
			aprobs[j] = (double) 1 / cantCronicas;
		}
		res.setResult(array2Doble);
		res.setProbs(aprobs);
		res.setPer(ta);
		return res;
	}

	public ResSimulDB creaResDB(String dirCorrida, PeriodoDeTiempo per, Estudio est) throws XcargaDatos{ 
		
		String dirCostocron = dirCorrida + "/costocron.xlt";
		String dirEnergcron = dirCorrida + "/energcron.xlt";
		String sep = "\t";
		TiempoAbsoluto ta = (TiempoAbsoluto) per;
		int cantCronicas = est.getDatosGenerales().getCantCronicas();
		boolean encontroAnio = false;
		ArrayList<ArrayList<String>> costoCron = null;
		ArrayList<ArrayList<String>> energCron = null;
		ArrayList<ArrayList<String>> nomImpExp = null;   
		ArrayList<String> nombresImp = new ArrayList<String>();
		ArrayList<String> nombresExp = new ArrayList<String>();
		boolean sigo = true;
		// Lee los archivos energcron y costocron
		// Tambi�n lee el archivo de nombres de importaciones y exportaciones
		while (sigo) {
			try {
				costoCron = CreadorTex.leeTextoBase(dirCostocron, sep);
				energCron = CreadorTex.leeTextoBase(dirEnergcron, sep);              
				sigo = false;
			} catch (XcargaDatos ex) {
				sigo = false;
				System.out.println("Error en la lectura de costocron o energcron en directorio " + dirCorrida);
				int levantar = JOptionPane.showConfirmDialog(null, "Error en costocron o energcron, cuando quiera siga");
				if (levantar == JOptionPane.OK_OPTION) {
					sigo = true;
				} else {
					throw new XcargaDatos("Error en la lectura de costocron o energcron en directorio " + dirCorrida);
				}
			}
		}

		/**
		 * Encuentra el inicio del año buscado en energCron
		 */
		int anio = ta.getAnio();
		String anioStr = String.valueOf(anio);

		int i = 0;
		do {
			if (costoCron.get(i).get(0).equalsIgnoreCase("A�o") && costoCron.get(i).get(1).equalsIgnoreCase(anioStr)) {
				encontroAnio = true;
			}
			i++;
		} while (i < costoCron.size() && !encontroAnio);
		if (!encontroAnio) {
			throw new XcargaDatos("No se encontr� el año " + anio
					+ " en archivo costocron del directorio " + dirCostocron);
		}
		/**
		 * el i queda posicionado en la primera línea posterior al año que es la de
		 * nombres de las fuentes
		 * 
		 * ATENCIÓN: NO SON EL OBJETO Fuente del Mingo sino el campo fuente de la base de datos
		 * que corresponde a un participante del MOP.
		 * 
		 */

		int cantFuentes = energCron.get(i).size() - 1;
		String[] nomF = new String[cantFuentes];
		String[] nomTipo = new String[cantFuentes];
		String[] nomSubTipo1 = new String[cantFuentes];
		String[] nomSubTipo2 = new String[cantFuentes];
		double[][] ener = new double[cantCronicas][cantFuentes];
		double[][] cost = new double[cantCronicas][cantFuentes];
		int fIni = i + 1;  // fIni es la fila del primer dato

		/**
		 * fIni es el ordinal de la primera fila de energCron y costoCron
		 * donde hay datos numéricos
		 */
		// kk  hay que resolver el tema de combustibles principal y alternativos
		// carga el nombre de las fuentes y el tipo y subtipos
		for (int ifu = 1; ifu <= cantFuentes; ifu++) {

			nomF[ifu - 1] = energCron.get(i).get(ifu).trim();

			for (int icron = 1; icron <= cantCronicas; icron++) {
				ener[icron - 1][ifu - 1] = Double.valueOf(energCron.get(fIni + icron - 1).get(ifu));
				if (ifu <= 5) {   
					cost[icron - 1][ifu - 1] = 0.0;
				} else {
					cost[icron - 1][ifu - 1] = Double.valueOf(costoCron.get(fIni + icron - 1).get(ifu - 5));
				}
			}
			// identifica el RecursoBase asociado a la fuente

			RecursoBase rb;
			if(nomF[ifu-1].equalsIgnoreCase("DEMANDA")){
				nomTipo[ifu-1]="DEMANDA";
				nomSubTipo1[ifu-1]=" ";
				nomSubTipo2[ifu-1]=" ";
			}else if (nomF[ifu-1].length()>5 && nomF[ifu-1].substring(0,5).equalsIgnoreCase("FALLA")){
				nomTipo[ifu-1]="FALLA";
				nomSubTipo1[ifu-1]=" ";
				nomSubTipo2[ifu-1]=" ";                
			}
			else{
				if(nomF[ifu - 1].endsWith("_P") || nomF[ifu - 1].endsWith("_S")){
					String nomTrun = nomF[ifu-1].substring(0, nomF[ifu-1].length()-2);
					// hay que comparar con igual n�mero de caracteres del nombre de los RecursoBase t�rmicos
					rb = est.getConjRecB().getUnRecBTr(nomTrun);                
				}else{
					// hay que comparar con el nombre de los RecursoBase
					rb = est.getConjRecB().getUnRecB(nomF[ifu-1]);      
				}
				nomTipo[ifu-1]=rb.getTipo();
				nomSubTipo1[ifu-1]=rb.getSubtipo1();
				nomSubTipo2[ifu-1]=rb.getSubtipo2();
			}            
		}
		int[] numC= new int[cantCronicas];       
		for (int k=0; k<cantCronicas; k++){
			numC[k]= Integer.parseInt(energCron.get(fIni+k).get(0));
		}

		ResSimulDB rsDB = new ResSimulDB(cantCronicas, cantFuentes);
		rsDB.setCronicas(numC);
		rsDB.setNombFuente(nomF);
		rsDB.setMatEner(ener);
		rsDB.setMatCost(cost);         
		rsDB.setAnio(anio);
		rsDB.setTipo(nomTipo);
		rsDB.setSubtipo1(nomSubTipo1);
		rsDB.setSubtipo2(nomSubTipo2);
		return rsDB;

	}   

	/**
	 * Realiza las inicializaciones que requiera cada implementación particular
	 * Los métodos que sobreescriban a este le dan contenido.
	 */
	public void inicializaParticular(){

	}


	/**
	 * Crea una línea de tiempo para una corrida de un año entre los instantes ini y fin, instantes inicial 
	 * y final de un año
	 * @param ini String con el instante de inicio de la corrida, primer día del año
	 * @return
	 */
	public DatosLineaTiempo creaDatosLT(String ini) {
		
		DatosLineaTiempo dlt = new DatosLineaTiempo();
		
		int anio = LineaTiempo.anioDeFechaMOP(ini);
		boolean bisiesto = LineaTiempo.bisiesto(anio);
		ArrayList<Integer> durpos = UtilArrays.dameAListInteger(DURPOS_B1);
		dlt.agregarBloqueDatosInt(CANT_PASOS_B1, DUR_PASO_B1, INT_MUESTREO, CANT_POSTES, durpos, PERIODO_BLOQUE, CRONOLOGICO);
		if(!bisiesto) {
			durpos = UtilArrays.dameAListInteger(DURPOS_B2_NOBIS);
			dlt.agregarBloqueDatosInt(CANT_PASOS_B2, DUR_PASO_B2_NOBIS, INT_MUESTREO, CANT_POSTES, durpos, PERIODO_BLOQUE, CRONOLOGICO);		
		}else {
			durpos = UtilArrays.dameAListInteger(DURPOS_B2_BIS);
			dlt.agregarBloqueDatosInt(CANT_PASOS_B2, DUR_PASO_B2_BIS, INT_MUESTREO, CANT_POSTES, durpos, PERIODO_BLOQUE, CRONOLOGICO);					
		}
		
		anio++ ;
		bisiesto = LineaTiempo.bisiesto(anio);		
		durpos = UtilArrays.dameAListInteger(DURPOS_B1);
		dlt.agregarBloqueDatosInt(CANT_PASOS_B1, DUR_PASO_B1, INT_MUESTREO, CANT_POSTES, durpos, PERIODO_BLOQUE, CRONOLOGICO);
		if(!bisiesto) {
			durpos = UtilArrays.dameAListInteger(DURPOS_B2_NOBIS);
			dlt.agregarBloqueDatosInt(CANT_PASOS_B2, DUR_PASO_B2_NOBIS, INT_MUESTREO, CANT_POSTES, durpos, PERIODO_BLOQUE, CRONOLOGICO);		
		}else {
			durpos = UtilArrays.dameAListInteger(DURPOS_B2_BIS);
			dlt.agregarBloqueDatosInt(CANT_PASOS_B2, DUR_PASO_B2_BIS, INT_MUESTREO, CANT_POSTES, durpos, PERIODO_BLOQUE, CRONOLOGICO);					
		} 		
		dlt.setTiempoInicial(ini);
		return dlt;
	}


	public ParString creaDatosYComandosMOP(String dirPaquete, String dirSalPaquete, int ordinalPaquete, int ordinalCorr, IdentSimulPaso identS, ArrayList<Object> informacion) throws XcargaDatos{
		/**
		 * Crea archivo y carga parametros generales
		 * 
		 * ATENCION: HAY QUE ARREGLAR LOS PARAMETROS GENERALES PARA TENER AÑO DE GUARDIA
		 * Y SIMULACIÓN ENCADENADA
		 * 
		 */
	
		TiempoAbsoluto ta = identS.getTa();
		Estudio est = (Estudio) informacion.get(0);
		DatosGeneralesEstudio datGen = est.getDatosGenerales();
		Integer anio = ta.getAnio();

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			dom = dBuilder.newDocument();;
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Carga DatosGenerales
		DatosCorrida corr = new DatosCorrida();
		corr.setNombre(datosMingo.getNombre());
		corr.setDatosUTE(datosMingo.getDatosCorridaUTE());
		corr.setDescripcion(datosMingo.getDescripcion());
		corr.setPostizacion(datosMingo.getPostizacion());
		corr.setTipoPostizacion(datosMingo.getTipoPostizacion());
		corr.setTipoSimulacion(datosMingo.getTipoSimulacion());
		corr.setValPostizacion(datosMingo.getValPostizacion());
		corr.setTipoValpostizacion(datosMingo.getTipoValpostizacion());
		corr.setTasa(datosMingo.getTasa());
		corr.setTopeSpot(ordinalCorr);
		corr.setPaisesACortar(datosMingo.getPaisesACortar());
		corr.setDespSinExp(datosMingo.isDespSinExp());
		corr.setIteracionSinExp(datosMingo.getIteracionSinExp());
		corr.setSemilla(datosMingo.getSemilla());
		corr.setRuta(datosMingo.getRuta());    
		
		String inicioC = "01 01 " + anio + " 00:00:00";
		String finC = "31 12 " + (anio+1) + " 23:59:59";
		
		corr.setInicioCorrida(inicioC);
		
		
		DatosLineaTiempo dlt = creaDatosLT(inicioC);
		dlt.setTiempoInicialEvoluciones(inicioC);
		corr.setLineaTiempo(dlt);
		
		GregorianCalendar inicio = Utilitarios.stringToGregorianCalendar(corr.getInicioCorrida(), "dd MM yyyy");
		GregorianCalendar fin = (GregorianCalendar)inicio.clone(); 

//		System.out.println("inicio de corrida " + inicio.getTimeInMillis());
//		System.out.println("fin de corrida " + fin.getTimeInMillis());
	
		
		LineaTiempo.agregalargoDeDatosLT(dlt, fin);
//		System.out.println("fin de corrida " + fin.getTimeInMillis());
		
		lt = new LineaTiempo(corr.getLineaTiempo(), inicio, fin);
		
		
		deltaTiempo =  lt.getTiempoInicial().getTimeInMillis()  -  instIniXmlMingoGC.getTimeInMillis();
		
		corr.setFinCorrida(finC);
		
		corr.setDatosParamSalida(datosMingo.getDatosParamSalida());
		corr.setDatosParamSalidaOpt(datosMingo.getDatosParamSalidaOpt());
		corr.setDatosParamSalidaSim(datosMingo.getDatosParamSalidaSim());		
		corr.setCantEscenarios(datosMingo.getCantEscenarios());
		corr.setCantSorteosMont(datosMingo.getCantSorteosMont());
		corr.setValoresComportamientoGlobal(datosMingo.getValoresComportamientoGlobal());
		corr.setDatosIteraciones(datosMingo.getDatosIteraciones());
		corr.setTipoSimulacion("encadenada");
		
		
		
		String caso;
		
		
		/**
		 * Carga procesos estocásticos
		 */
//		caso = idCorr.valorSimbolicoDeTipoDatos(PROCESOS_ESTOCASTICOS);          
//		Hashtable<String, DatosProcesoEstocastico> dp = datosMingo.getCasosProcesosEstocasticos().get(caso);		
		corr.setProcesosEstocasticos(datosMingo.getProcesosEstocasticos());
				
		/**
		 * COMIENZA A CREAR TIPOS DE DATOS DE PARTICIPANTES
		 */
		// TODO		
		ArrayList<Bloque> bloques;
		Parque par = identS.getParque();
		bloques = par.bloquesDelParque(est);
		// AGREGA LOS BLOQUES COMUNES A TODOS LOS PARQUES A LA COLECCIÓN DE BLOQUES
		bloques.addAll(Parque.bloquesComunesDeTA(ta, est));


		/**
		 * Carga en el DatosCorrida corr de los datos de cada tipo de participantes     
		 */
		
		creaDatosTermicos(identS, datGen, bloques, corr);
		
		creaDatosCiclosCombinados(identS, datGen, bloques, corr);

		 // Se elige el caso hidráulico según la configuración de hidraulicos operativos
		creaDatosHidraulicos(identS, datGen, bloques, corr);
		
		creaDatosEolicos(identS, datGen, bloques, corr);

		creaDatosFotovoltaicos(identS, datGen, bloques, corr);

		creaDatosImpoExpos(identS, datGen, bloques, corr);
			
		creaDatosAcumuladores(identS, datGen, bloques, corr);

		creaDatosDemandasYFallas(identS, datGen, bloques, corr);
		
		// Carga red eléctrica
		corr.setRed(datosMingo.getRed());
				
		creaDatosCombustibles(identS, datGen, bloques, corr);
		
		EscritorXML esc = new EscritorXML(lt);
		String archXml = dirPaquete + "/P" + ordinalPaquete + "_C" + ordinalCorr + ".xml";
		String rutaSals = dirSalPaquete; 
		corr.setRutaSals(rutaSals);
		esc.guardarCorrida(corr, archXml);
		
		return new ParString(" ", " ");
		         
	}
	

	
	/**
	 * Crea datos para xml de generadores térmicos     
	 */
	public void creaDatosTermicos(IdentSimulPaso identS, DatosGeneralesEstudio datGen, ArrayList<Bloque> bloques, DatosCorrida corr) throws XcargaDatos{

		TiempoAbsoluto ta = identS.getTa();
		
		String caso = identS.valorSimbolicoDeTipoDatos(TERMICOS);          
		DatosTermicosCorrida datosTermicosMingo = datosMingo.getCasosTermicos().get(caso);
		Integer cantModInst = 0;
		ArrayList<String> listaUtilizados = new ArrayList<String>();		 
		
		for (Bloque b : bloques) {
			Recurso rec = b.getRecurso();
			RecursoBase rb = b.getRecurso().getRecBase();
			if (rb instanceof GeneradorTermico && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
				listaUtilizados.add( rb.getNombre() );       		 
				int multiplicador = b.getMultiplicador();
				int cantMod1RB = rec.cantMod(datGen, ta);
				double potNominal = rec.potNom1Mod(datGen, ta);
				if (cantMod1RB * potNominal == 0) {
					int selec;
					selec = JOptionPane.showOptionDialog(null, "Entrada antes de tiempoAbsolutoBase "
							+ rb.getNombre(), "¿Desea seguir?", JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE, null,
							new Object[]{"Detener la ejecución", "Seguir"}, null);
					// Devuelve 0, 1 o 2 según la opción
					if (selec == 0) {
						throw new XcargaDatos("Se detuvo ejecución porque un RecursoBase entraría en servicio "
								+ "antes de su tiempoAbsolutoBase");
					}
				} else {
					cantModInst = cantMod1RB * multiplicador;

				}
				DatosTermicoCorrida dt1 = datosTermicosMingo.getTermicos().get(rb.getNombre());
				if(rb.esComun()){
					if(dt1.getCantModInst() instanceof EvolucionConstante) {
						// no se afecta la evolución del xmlCorridaMingo
					}else if(dt1.getCantModInst() instanceof EvolucionPorInstantes) {
//						// Desplaza los instantes iniciales usando el instante inicial de xmlCorridaMingo
//						for(int i=0; i<eclonada.getInstantesOrdenados().size(); i++) {
//							eclonada.getInstantesOrdenados().set(i, (long)eclonada.getInstantesOrdenados().get(i) - deltaTiempo);
//						}
//						dt1.setCantModInst(eclonada);
					}else {
						System.out.println("La cantidad de módulos no es una EvolucionConstante ni por Instantes");
						System.exit(1);
						
					}
					
//					EvolucionPorInstantes ev = (EvolucionPorInstantes)dt1.getCantModInst();
//					System.out.println(UtilArrays.dameTexto(ev.devuelveFechaInstantes(lt)));
					
				}else{	        		 
					// Se carga la cantidad de módulos de un bloque no común
					SentidoTiempo st = new SentidoTiempo(-1);  
					EvolucionConstante<Integer> ev = new EvolucionConstante<Integer>(cantModInst, st);
					dt1.setCantModInst(ev);
				}
				corr.getTermicos().getTermicos().put(rb.getNombre(),dt1);
			}	
		}         
		corr.getTermicos().setAtribtosDetallados(datosTermicosMingo.getAtribtosDetallados());
		corr.getTermicos().setListaUtilizados(listaUtilizados);
		corr.getTermicos().setValoresComportamiento(datosTermicosMingo.getValoresComportamiento());
		corr.getTermicos().setOrdenCargaXML(listaUtilizados);
		
	}

	
	
	/**
	 * Crea datos para xml de ciclos combinados
	 * Si el bloque tiene n>1 módulos de ciclo combinado
	 * en el xml se crean n CicloCombinado con nombres
	 * construidos con el nombre del RecursoBase + "_" + i, con i=1,...n  
	 */
	public void creaDatosCiclosCombinados(IdentSimulPaso identS, DatosGeneralesEstudio datGen, ArrayList<Bloque> bloques, DatosCorrida corr) throws XcargaDatos{

		TiempoAbsoluto ta = identS.getTa();
		
		String caso = identS.valorSimbolicoDeTipoDatos(CICLOS_COMBINADOS);          
		DatosCiclosCombinadosCorrida datosCiclosCombMingo = datosMingo.getCasosCiclosComb().get(caso);
		Integer cantModInst = 0;
		ArrayList<String> listaUtilizados = new ArrayList<String>();		 
		
		for (Bloque b : bloques) {
			Recurso rec = b.getRecurso();
			RecursoBase rb = b.getRecurso().getRecBase();
			if (rb instanceof GeneradorCicloCombinado && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
				listaUtilizados.add( rb.getNombre() );       		 
				int multiplicador = b.getMultiplicador();
				int cantMod1RB = rec.cantMod(datGen, ta);
				double potNominal = rec.potNom1Mod(datGen, ta);
				if (cantMod1RB * potNominal == 0) {
					int selec;
					selec = JOptionPane.showOptionDialog(null, "Entrada antes de tiempoAbsolutoBase "
							+ rb.getNombre(), "¿Desea seguir?", JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE, null,
							new Object[]{"Detener la ejecución", "Seguir"}, null);
					// Devuelve 0, 1 o 2 según la opción
					if (selec == 0) {
						throw new XcargaDatos("Se detuvo ejecución porque un RecursoBase entraría en servicio "
								+ "antes de su tiempoAbsolutoBase");
					}
				} else {
					cantModInst = cantMod1RB * multiplicador;

				}
				DatosCicloCombinadoCorrida dcc = datosCiclosCombMingo.getCcombinados().get(rb.getNombre());
				if(rb.esComun()){
					// Si el recurso es común se carga su datatype sin modificación tal cual se lee  
					SentidoTiempo st = new SentidoTiempo(-1);  
					EvolucionConstante<Integer> ev = new EvolucionConstante<Integer>(cantModInst, st);
					corr.getCcombinados().getCcombinados().put(rb.getNombre(), dcc);
				}else{	        		 
					// Si hay más de un módulo de CC se carga la cantidad n de ciclos combinados cantModInst con nombres
					// creados con sufijos "_" + i, con i=1,...,n
					for(int imod = 1; imod<cantModInst; imod++) {
						SentidoTiempo st = new SentidoTiempo(-1);  
						EvolucionConstante<Integer> ev = new EvolucionConstante<Integer>(cantModInst, st);
						corr.getCcombinados().getCcombinados().put(rb.getNombre()+ "_" + imod, dcc);
					}
				}

			}	
		}         
		corr.getCcombinados().setAtribtosDetallados(datosCiclosCombMingo.getAtribtosDetallados());
		corr.getCcombinados().setListaUtilizados(listaUtilizados);
		corr.getCcombinados().setValoresComportamiento(datosCiclosCombMingo.getValoresComportamiento());
		corr.getCcombinados().setOrdenCargaXML(listaUtilizados);

	}
	
	public void creaDatosHidraulicos(IdentSimulPaso identS, DatosGeneralesEstudio datGen, ArrayList<Bloque> bloques, DatosCorrida corr) throws XcargaDatos{
		
		ArrayList<String> nombresH = new ArrayList<String>();
		for (Bloque b : bloques) {
			RecursoBase rb = b.getRecurso().getRecBase();
			if (rb instanceof GeneradorHidro && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
				nombresH.add(rb.getNombre());                  
			}
		}
		String claveH = claveDeNombres(nombresH);
		String casoH = nombreConfiguracionesHidro.get(claveH);
		DatosHidraulicosCorrida dh = datosMingo.getCasosHidraulicos().get(casoH);
		dh.setListaUtilizados(nombresH);
		corr.setHidraulicos(dh);
		corr.getHidraulicos().setOrdenCargaXML(nombresH);
	
	}
	
	
	public void creaDatosEolicos(IdentSimulPaso identS, DatosGeneralesEstudio datGen, ArrayList<Bloque> bloques, DatosCorrida corr) throws XcargaDatos{
		TiempoAbsoluto ta = identS.getTa();
		String caso = identS.valorSimbolicoDeTipoDatos(EOLICOS);          
		DatosEolicosCorrida datosEolicosMingo = datosMingo.getCasosEolicos().get(caso);		 
		ArrayList<String> listaUtilizados = new ArrayList<String>();		 
		int cantModInst = 0;

		for (Bloque b : bloques) {
			Recurso rec = b.getRecurso();
			RecursoBase rb = b.getRecurso().getRecBase();
			if (rb instanceof GeneradorEolico && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {

				listaUtilizados.add(rb.getNombre());
				int multiplicador = b.getMultiplicador();
				int cantMod1RB = rec.cantMod(datGen, ta);

				double potNominal = rec.potNom1Mod(datGen, ta);
				if (cantMod1RB * potNominal == 0) {
					int selec;
					selec = JOptionPane.showOptionDialog(null, "Entrada antes de tiempoAbsolutoBase "
							+ rb.getNombre(), "¿Desea seguir?", JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE, null,
							new Object[]{"Detener la ejecución", "Seguir"}, null);
					// Devuelve 0, 1 o 2 según la opción
					if (selec == 0) {
						throw new XcargaDatos("Se detuvo ejecución porque un RecursoBase entraría en servicio "
								+ "antes de su tiempoAbsolutoBase");
					}
				} else {
					cantModInst = cantMod1RB * multiplicador;
//					potTotal = cantModInst * potNominal;
				}				
				DatosEolicoCorrida de1 = datosEolicosMingo.getEolicos().get(rb.getNombre());
				if(rb.esComun()){
					// Si el recurso es común se carga su datatype sin modificación tal cual se lee     
				}else{
					// Se carga la cantidad de módulos de un bloque no común
					SentidoTiempo st = new SentidoTiempo(-1);  
					EvolucionConstante<Integer> ev = new EvolucionConstante<Integer>(cantModInst, st);
					de1.setCantModInst(ev);
				}
				corr.getEolicos().getEolicos().put(rb.getNombre(),de1);

			}	
		}         
		corr.getEolicos().setAtributosDetallados(datosEolicosMingo.getAtributosDetallados());
		corr.getEolicos().setListaUtilizados(listaUtilizados);	
		corr.getEolicos().setOrdenCargaXML(listaUtilizados);
		
		
	}
	
	
	public void creaDatosFotovoltaicos(IdentSimulPaso identS, DatosGeneralesEstudio datGen, ArrayList<Bloque> bloques, DatosCorrida corr) throws XcargaDatos{
		TiempoAbsoluto ta = identS.getTa();
		
		String caso = identS.valorSimbolicoDeTipoDatos(FOTOVOLTAICOS);          
		DatosFotovoltaicosCorrida datosFotovoltaicosMingo = datosMingo.getCasosFotovoltaicos().get(caso);		 
		ArrayList<String> listaUtilizados = new ArrayList<String>();		 
		int cantModInst = 0;

		for (Bloque b : bloques) {
			Recurso rec = b.getRecurso();
			RecursoBase rb = b.getRecurso().getRecBase();
			if (rb instanceof GeneradorFotovoltaico && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {

				listaUtilizados.add(rb.getNombre());
				int multiplicador = b.getMultiplicador();
				int cantMod1RB = rec.cantMod(datGen, ta);

				double potNominal = rec.potNom1Mod(datGen, ta);
				if (cantMod1RB * potNominal == 0) {
					int selec;
					selec = JOptionPane.showOptionDialog(null, "Entrada antes de tiempoAbsolutoBase "
							+ rb.getNombre(), "¿Desea seguir?", JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE, null,
							new Object[]{"Detener la ejecuci�n", "Seguir"}, null);
					// Devuelve 0, 1 o 2 según la opción
					if (selec == 0) {
						throw new XcargaDatos("Se detuvo ejecución porque un RecursoBase entraría en servicio "
								+ "antes de su tiempoAbsolutoBase");
					}
				} else {
					cantModInst = cantMod1RB * multiplicador;
//					potTotal = cantModInst * potNominal;
				}				
				DatosFotovoltaicoCorrida df1 = datosFotovoltaicosMingo.getFotovoltaicos().get(rb.getNombre());
				if(rb.esComun()){
					// Si el recurso es común se carga su datatype sin modificación tal cual se lee     					
				}else{
					// Se carga la cantidad de módulos de un bloque no común
					SentidoTiempo st = new SentidoTiempo(-1);  
					EvolucionConstante<Integer> ev = new EvolucionConstante<Integer>(cantModInst, st);
					df1.setCantModInst(ev);
				}
				corr.getFotovoltaicos().getFotovoltaicos().put(rb.getNombre(),df1);

			}	
		}         		 
		corr.getFotovoltaicos().setAtributosDetallados(datosFotovoltaicosMingo.getAtributosDetallados());
		corr.getFotovoltaicos().setListaUtilizados(listaUtilizados);
		corr.getFotovoltaicos().setOrdenCargaXML(listaUtilizados);
	
	}
	
	public void creaDatosImpoExpos(IdentSimulPaso identS, DatosGeneralesEstudio datGen, ArrayList<Bloque> bloques, DatosCorrida corr) throws XcargaDatos{
		TiempoAbsoluto ta = identS.getTa();
		String caso = identS.valorSimbolicoDeTipoDatos(IMPOEXPOS);          
		DatosImpoExposCorrida datosImpoExposMingo = datosMingo.getCasosImpoExpos().get(caso);		 
		ArrayList<String> listaUtilizados = new ArrayList<String>();		 
		int cantModInst = 0;

		for (Bloque b : bloques) {
			Recurso rec = b.getRecurso();
			RecursoBase rb = b.getRecurso().getRecBase();
			if ((rb instanceof Importacion || rb instanceof Exportacion) && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
				if(b.getMultiplicador()>1){
					System.out.println("EN MOP SE PIDIO UN IMPOEXPO CON MÁS DE UN MÓDULO");
					System.exit(1);
				}
				listaUtilizados.add(rb.getNombre());

				int cantMod1RB = rec.cantMod(datGen, ta);
				double potNominal = rec.potNom1Mod(datGen, ta);
				if (cantMod1RB * potNominal == 0) {
					int selec;
					selec = JOptionPane.showOptionDialog(null, "Entrada antes de tiempoAbsolutoBase "
							+ rb.getNombre(), "¿Desea seguir?", JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE, null,
							new Object[]{"Detener la ejecuci�n", "Seguir"}, null);
					// Devuelve 0, 1 o 2 según la opción
					if (selec == 0) {
						throw new XcargaDatos("Se detuvo ejecución porque un RecursoBase entraría en servicio "
								+ "antes de su tiempoAbsolutoBase");
					}
				} 			
				DatosImpoExpoCorrida die1 = datosImpoExposMingo.getImpoExpos().get(rb.getNombre());
				// Los recursos impoexpo entran o no entran enteros. No
				corr.getImpoExpos().getImpoExpos().put(rb.getNombre(),die1);
			}	
		}         		 
		corr.getImpoExpos().setAtributosDetallados(datosImpoExposMingo.getAtributosDetallados());
		corr.getImpoExpos().setListaUtilizados(listaUtilizados);
		corr.getImpoExpos().setOrdenCargaXML(listaUtilizados);
		
		
	}
	
	
	public void creaDatosAcumuladores(IdentSimulPaso identS, DatosGeneralesEstudio datGen, ArrayList<Bloque> bloques, DatosCorrida corr) throws XcargaDatos{
		TiempoAbsoluto ta = identS.getTa();
	
		String caso = identS.valorSimbolicoDeTipoDatos(ACUMULADORES);          
		DatosAcumuladoresCorrida datosAcumuladoresMingo = datosMingo.getCasosAcumuladores().get(caso);		 
		ArrayList<String> listaUtilizados = new ArrayList<String>();		 
		int cantModInst = 0;

		for (Bloque b : bloques) {
			Recurso rec = b.getRecurso();
			RecursoBase rb = b.getRecurso().getRecBase();
			if (rb instanceof Acumulador && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
				listaUtilizados.add(rb.getNombre());
				int multiplicador = b.getMultiplicador();
				int cantMod1RB = rec.cantMod(datGen, ta);

				double potNominal = rec.potNom1Mod(datGen, ta);
				if (cantMod1RB * potNominal == 0) {
					int selec;
					selec = JOptionPane.showOptionDialog(null, "Entrada antes de tiempoAbsolutoBase "
							+ rb.getNombre(), "¿Desea seguir?", JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE, null,
							new Object[]{"Detener la ejecución", "Seguir"}, null);
					// Devuelve 0, 1 o 2 según la opción
					if (selec == 0) {
						throw new XcargaDatos("Se detuvo ejecución porque un RecursoBase entraría en servicio "
								+ "antes de su tiempoAbsolutoBase");
					}
				} else {
					cantModInst = cantMod1RB * multiplicador;
//					potTotal = cantModInst * potNominal;
				}				
				DatosAcumuladorCorrida da1 = datosAcumuladoresMingo.getAcumuladores().get(rb.getNombre());
				if(rb.esComun()){
					// Si el recurso es común se carga su datatype sin modificación tal cual se lee     
				}else{
					// Se carga la cantidad de módulos de un bloque no común
					SentidoTiempo st = new SentidoTiempo(-1);  
					EvolucionConstante<Integer> ev = new EvolucionConstante<Integer>(cantModInst, st);
					da1.setCantModInst(ev);
				}
				corr.getAcumuladores().getAcumuladores().put(rb.getNombre(),da1);
			}	
		}         			 
		corr.getAcumuladores().setAtributosDetallados(datosAcumuladoresMingo.getAtributosDetallados());
		corr.getAcumuladores().setListaUtilizados(listaUtilizados);
		corr.getAcumuladores().setValoresComportamiento(datosAcumuladoresMingo.getValoresComportamiento());
		corr.getAcumuladores().setOrdenCargaXML(listaUtilizados);
	
		
	}
	
	
	public void creaDatosDemandasYFallas(IdentSimulPaso identS, DatosGeneralesEstudio datGen, ArrayList<Bloque> bloques, DatosCorrida corr) throws XcargaDatos{

		// demandas
		String caso = identS.valorSimbolicoDeTipoDatos(DEMANDAS);          
		DatosDemandasCorrida demandas = datosMingo.getCasosDemandas().get(caso);
		Set<String> setDem = demandas.getDemandas().keySet();
		ArrayList<String> listaUtilizadosDem = new ArrayList<String>();
		for(String s: setDem){
			listaUtilizadosDem.add(s);
		}
		demandas.setListaUtilizados(listaUtilizadosDem);
		corr.setDemandas(demandas);
		corr.getDemandas().setOrdenCargaXML(listaUtilizadosDem);
		
		// fallas
		ArrayList<String> listaUtilizadosFallas = new ArrayList<String>();
		caso = identS.valorSimbolicoDeTipoDatos(FALLAS);          
		DatosFallasEscalonadasCorrida fallas = datosMingo.getCasosFallas().get(caso);
		Set<String> setf = fallas.getFallas().keySet();
		for(String nombreFalla: setf){
			String nombreDemanda = fallas.getFallas().get(nombreFalla).getDemanda();			
			if(listaUtilizadosDem.contains(nombreDemanda)) listaUtilizadosFallas.add(nombreFalla);
		}
		fallas.setListaUtilizados(listaUtilizadosFallas);	
		corr.setFallas(fallas);				
		
	}
	
	public void creaDatosCombustibles(IdentSimulPaso identS, DatosGeneralesEstudio datGen, ArrayList<Bloque> bloques, DatosCorrida corr) throws XcargaDatos{
		TiempoAbsoluto ta = identS.getTa();
		String caso = identS.valorSimbolicoDeTipoDatos(COMBUSTIBLES);          
		DatosCombustiblesCorrida combustibles = datosMingo.getCasosCombustibles().get(caso);	
		Set<String> nombresComb = combustibles.getCombustibles().keySet();
		ArrayList<String> listaUtilizados = new ArrayList<String>();
		for(String s: nombresComb){
			listaUtilizados.add(s);
		}
		combustibles.setListaUtilizados(listaUtilizados);
		corr.setCombustibles(combustibles);	
		corr.getCombustibles().setOrdenCargaXML(listaUtilizados);
	
	}
		
	public void creaXmlParalelizador(int ordinalPaquete, String jarParalelizador, String xmlInvocaParalelizador, int cantMaq) {
		
		utilitarios.DirectoriosYArchivos.siExisteElimina(xmlInvocaParalelizador);
		File file = new File(xmlInvocaParalelizador);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			dom = dBuilder.newDocument();
			TransformerFactory transFactory = TransformerFactory.newInstance();
			Transformer trans = transFactory.newTransformer();

			StreamResult sr = new StreamResult(file);
			DOMSource source = new DOMSource(dom);
			trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
//			trans.setOutputProperty(OutputKeys.INDENT, "yes");
			trans.setOutputProperty(OutputKeys.INDENT, "no");
			trans.transform(source, sr);
		} catch (ParserConfigurationException | TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Element nodoPrincipal = dom.createElement("problema");
		
		Element nombre = dom.createElement("NOMBRE");
		nombre.appendChild(dom.createTextNode("corrida MOP"));
		nodoPrincipal.appendChild(nombre);
		
		Element rutaEntrada = dom.createElement("RUTA_ENTRADA");
		rutaEntrada.appendChild(dom.createTextNode(rutaEntradaParalelizador + "/P_" + ordinalPaquete));
		nodoPrincipal.appendChild(rutaEntrada);
		
		Element rutaEjec = dom.createElement("RUTA_EJECUTABLE");
		rutaEjec.appendChild(dom.createTextNode(jarEjecutableMOP));
		nodoPrincipal.appendChild(rutaEjec);
		
		Element rutaSal = dom.createElement("RUTA_SALIDA");
		rutaSal.appendChild(dom.createTextNode(rutaSalidaParalelizador + "/RP_" + ordinalPaquete));
		nodoPrincipal.appendChild(rutaSal);
		
		Element cantM = dom.createElement("CANT_MAQUINAS");
		cantM.appendChild(dom.createTextNode(String.valueOf(cantMaq)));
		nodoPrincipal.appendChild(cantM);
		
		Element ejec = dom.createElement("EJECUTOR");
		ejec.appendChild(dom.createTextNode("MOP"));
		nodoPrincipal.appendChild(ejec);

		
		System.out.println("Inicio escritura XML del paralelizador");
		
		try {
			dom.appendChild(nodoPrincipal);
			
			
			File fileRuta = new File("D:\\PruebaXML");
			
			TransformerFactory transFactory = TransformerFactory.newInstance();
			Transformer trans = transFactory.newTransformer();
			
			StreamResult sr = new StreamResult(fileRuta);
			DOMSource source = new DOMSource(dom);
			trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");
//			trans.setOutputProperty(OutputKeys.INDENT, "no");
			trans.transform(source, sr);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Escribió el xml de entrada del paralelizador");


		
	}
	
	
	
	
	public static void main(String[] args) throws XcargaDatos {
		ImplementadorMOP impMOP = new ImplementadorMOP();
		String dir_Datos_Implementacion = "S:/UTE/MINGO/_PruebaImplementacionMOP/Datos_ImplementacionMOP"; 
		String dir_Estudios = "S:/UTE/MINGO/_PruebaImplementacionMOP";
		String dir_Corridas = "S:/UTE/MINGO/_PruebaImplementacionMOP/corridas";
		impMOP.inicializaImplementador(dir_Datos_Implementacion, dir_Estudios, dir_Corridas);
   

	}
	
	


}
