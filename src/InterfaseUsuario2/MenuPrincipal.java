/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * MenuPrincipal.java
 *
 * Created on 02/06/2011, 12:14:54 PM
 */

package InterfaseUsuario2;

import AlmacenSimulaciones.BolsaDeSimulaciones3;
import UtilitariosGenerales.ManejaObjetosEnDisco;
import dominio.Estudio;
import persistenciaMingo.XcargaDatos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 *
 * @author ut469262
 */
public class MenuPrincipal extends javax.swing.JFrame {
    private Estudio estCorr;
    private Texto texto;
    private BolsaDeSimulaciones3 bolsaCorr;
    /** Creates new form MenuPrincipal */
    public MenuPrincipal() {
        texto = new Texto();
        estCorr = new Estudio();
//        bolsaCorr = new BolsaDeSimulaciones();
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
     // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
     private void initComponents() {

          jPanel3 = new javax.swing.JPanel();
          jTabbedPane1 = new javax.swing.JTabbedPane();
          jPanelEstudio = new javax.swing.JPanel();
          jTabbedPane2 = new javax.swing.JTabbedPane();
          jPanelArchEstudio = new javax.swing.JPanel();
          jButtonCreaDesdeTexto = new javax.swing.JButton();
          jButtonCargaDeDisco = new javax.swing.JButton();
          jButtonGuardaEnDisco = new javax.swing.JButton();
          jPanelBolsaSim = new javax.swing.JPanel();
          jButtonCreaNuevaBolsa = new javax.swing.JButton();
          jButtonTraeBolsaDeDisco = new javax.swing.JButton();
          jPanelCaso = new javax.swing.JPanel();

          javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
          jPanel3.setLayout(jPanel3Layout);
          jPanel3Layout.setHorizontalGroup(
               jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGap(0, 100, Short.MAX_VALUE)
          );
          jPanel3Layout.setVerticalGroup(
               jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGap(0, 100, Short.MAX_VALUE)
          );

          setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

          jButtonCreaDesdeTexto.setText("Crear Estudio desde archivos de texto");
          jButtonCreaDesdeTexto.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonCreaDesdeTextoActionPerformed(evt);
               }
          });

          jButtonCargaDeDisco.setText("Cargar Estudio preexistente en disco");
          jButtonCargaDeDisco.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonCargaDeDiscoActionPerformed(evt);
               }
          });

          jButtonGuardaEnDisco.setText("Guardar Estudio corriente en disco");
          jButtonGuardaEnDisco.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonGuardaEnDiscoActionPerformed(evt);
               }
          });

          javax.swing.GroupLayout jPanelArchEstudioLayout = new javax.swing.GroupLayout(jPanelArchEstudio);
          jPanelArchEstudio.setLayout(jPanelArchEstudioLayout);
          jPanelArchEstudioLayout.setHorizontalGroup(
               jPanelArchEstudioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(jPanelArchEstudioLayout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addGroup(jPanelArchEstudioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                         .addComponent(jButtonGuardaEnDisco, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                         .addComponent(jButtonCargaDeDisco, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                         .addComponent(jButtonCreaDesdeTexto, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE))
                    .addContainerGap(590, Short.MAX_VALUE))
          );
          jPanelArchEstudioLayout.setVerticalGroup(
               jPanelArchEstudioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(jPanelArchEstudioLayout.createSequentialGroup()
                    .addGap(41, 41, 41)
                    .addComponent(jButtonCreaDesdeTexto)
                    .addGap(36, 36, 36)
                    .addComponent(jButtonCargaDeDisco)
                    .addGap(37, 37, 37)
                    .addComponent(jButtonGuardaEnDisco)
                    .addContainerGap(230, Short.MAX_VALUE))
          );

          jTabbedPane2.addTab("ARCHIVO", jPanelArchEstudio);

          javax.swing.GroupLayout jPanelEstudioLayout = new javax.swing.GroupLayout(jPanelEstudio);
          jPanelEstudio.setLayout(jPanelEstudioLayout);
          jPanelEstudioLayout.setHorizontalGroup(
               jPanelEstudioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGap(0, 934, Short.MAX_VALUE)
               .addGroup(jPanelEstudioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelEstudioLayout.createSequentialGroup()
                         .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 906, javax.swing.GroupLayout.PREFERRED_SIZE)
                         .addContainerGap(28, Short.MAX_VALUE)))
          );
          jPanelEstudioLayout.setVerticalGroup(
               jPanelEstudioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGap(0, 492, Short.MAX_VALUE)
               .addGroup(jPanelEstudioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelEstudioLayout.createSequentialGroup()
                         .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 441, javax.swing.GroupLayout.PREFERRED_SIZE)
                         .addContainerGap(51, Short.MAX_VALUE)))
          );

          jTabbedPane1.addTab("ESTUDIO", jPanelEstudio);

          jButtonCreaNuevaBolsa.setText("Crea nueva BolsaDeSimulaciones");
          jButtonCreaNuevaBolsa.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButtonCreaNuevaBolsaActionPerformed(evt);
               }
          });

          jButtonTraeBolsaDeDisco.setText("Trae BolsaDeSimulaciones de disco");

          javax.swing.GroupLayout jPanelBolsaSimLayout = new javax.swing.GroupLayout(jPanelBolsaSim);
          jPanelBolsaSim.setLayout(jPanelBolsaSimLayout);
          jPanelBolsaSimLayout.setHorizontalGroup(
               jPanelBolsaSimLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(jPanelBolsaSimLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanelBolsaSimLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                         .addComponent(jButtonTraeBolsaDeDisco, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                         .addComponent(jButtonCreaNuevaBolsa, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE))
                    .addContainerGap(699, Short.MAX_VALUE))
          );
          jPanelBolsaSimLayout.setVerticalGroup(
               jPanelBolsaSimLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(jPanelBolsaSimLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(jButtonCreaNuevaBolsa)
                    .addGap(54, 54, 54)
                    .addComponent(jButtonTraeBolsaDeDisco)
                    .addContainerGap(372, Short.MAX_VALUE))
          );

          jTabbedPane1.addTab("BOLSA DE SIMULACIONES", jPanelBolsaSim);

          javax.swing.GroupLayout jPanelCasoLayout = new javax.swing.GroupLayout(jPanelCaso);
          jPanelCaso.setLayout(jPanelCasoLayout);
          jPanelCasoLayout.setHorizontalGroup(
               jPanelCasoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGap(0, 934, Short.MAX_VALUE)
          );
          jPanelCasoLayout.setVerticalGroup(
               jPanelCasoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGap(0, 492, Short.MAX_VALUE)
          );

          jTabbedPane1.addTab("CASO", jPanelCaso);

          javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
          getContentPane().setLayout(layout);
          layout.setHorizontalGroup(
               layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGap(0, 975, Short.MAX_VALUE)
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                         .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 939, javax.swing.GroupLayout.PREFERRED_SIZE)
                         .addContainerGap(36, Short.MAX_VALUE)))
          );
          layout.setVerticalGroup(
               layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGap(0, 567, Short.MAX_VALUE)
               .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                         .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE)
                         .addContainerGap(47, Short.MAX_VALUE)))
          );

          pack();
     }// </editor-fold>//GEN-END:initComponents

    private void jButtonCreaDesdeTextoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCreaDesdeTextoActionPerformed
        JFileChooser constEstDeTexto = new JFileChooser();

        constEstDeTexto.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int result = constEstDeTexto.showOpenDialog(this);
        if(result == JFileChooser.APPROVE_OPTION){
            File archEstExist = constEstDeTexto.getSelectedFile();
//            File nuevo = new File();
            String stringArchEstExist = archEstExist.getPath();
            stringArchEstExist=UtilitariosGenerales.DirectoriosYArchivos.barraAscendente(stringArchEstExist);
            try {
                System.out.print("EL DIRECTORIO DONDE SE BUSCA ES:" + archEstExist.getPath());
                estCorr.leerEstudio(stringArchEstExist, false);
            } catch (IOException ex) {
                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            } catch (XcargaDatos ex) {
                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.print(estCorr.toString());
        }
    }//GEN-LAST:event_jButtonCreaDesdeTextoActionPerformed

    private void jButtonCargaDeDiscoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCargaDeDiscoActionPerformed
        JFileChooser constEstDeTexto = new JFileChooser();
        constEstDeTexto.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int result = constEstDeTexto.showOpenDialog(this);
        if(result == JFileChooser.APPROVE_OPTION){
            File archEstExist = constEstDeTexto.getSelectedFile();
            try {
                estCorr = (Estudio) ManejaObjetosEnDisco.traerDeDisco(archEstExist);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.print(estCorr.toString());
        }// TODO add your handling code here:
    }//GEN-LAST:event_jButtonCargaDeDiscoActionPerformed

    private void jButtonGuardaEnDiscoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGuardaEnDiscoActionPerformed
        JFileChooser constEstDeTexto = new JFileChooser();
        constEstDeTexto.setDialogType(JFileChooser.SAVE_DIALOG);
        int result = constEstDeTexto.showOpenDialog(this);
        if(result == JFileChooser.SAVE_DIALOG){
            File archAGrabar = constEstDeTexto.getSelectedFile();
//            try {
//                ManejaObjetosEnDisco.guardarEnDisco(archAGrabar, estCorr);
//            } catch (FileNotFoundException ex) {
//                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (IOException ex) {
//                Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }// TODO add your handling code here:// TODO add your handling code here:
    }//GEN-LAST:event_jButtonGuardaEnDiscoActionPerformed

    private void jButtonCreaNuevaBolsaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCreaNuevaBolsaActionPerformed
         // TODO add your handling code here:
    }//GEN-LAST:event_jButtonCreaNuevaBolsaActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuPrincipal().setVisible(true);
            }
        });
    }

     // Variables declaration - do not modify//GEN-BEGIN:variables
     private javax.swing.JButton jButtonCargaDeDisco;
     private javax.swing.JButton jButtonCreaDesdeTexto;
     private javax.swing.JButton jButtonCreaNuevaBolsa;
     private javax.swing.JButton jButtonGuardaEnDisco;
     private javax.swing.JButton jButtonTraeBolsaDeDisco;
     private javax.swing.JPanel jPanel3;
     private javax.swing.JPanel jPanelArchEstudio;
     private javax.swing.JPanel jPanelBolsaSim;
     private javax.swing.JPanel jPanelCaso;
     private javax.swing.JPanel jPanelEstudio;
     private javax.swing.JTabbedPane jTabbedPane1;
     private javax.swing.JTabbedPane jTabbedPane2;
     // End of variables declaration//GEN-END:variables

}
