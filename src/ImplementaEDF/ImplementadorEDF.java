/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ImplementaEDF;

import AlmacenSimulaciones.CreadorComandos;
import AlmacenSimulaciones.IdentSimulPaso;
import AlmacenSimulaciones.ImplementadorGeneral;
import GrafoEstados.PeriodoDeTiempo;
import GrafoEstados.ResSimul;
import dominio.ResSimulDB;
import ManejadorTextos.CreadorTex;
import ManejadorTextos.PlantillaEnTexto;
import TiposEnum.EstadosRec;
import TiposEnum.SubTipo1;
import TiposEnum.TipoBloque;
import UtilitariosGenerales.DirectoriosYArchivos;
import UtilitariosGenerales.FCalendar;
import UtilitariosGenerales.ParString;
import dominio.Biomasa;
import dominio.BiomasaEstac;
import dominio.BiomasaFormul;
import dominio.Bloque;
//import dominio.Bloque.TipoBloque;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import dominio.ExportEstac;
import dominio.ExportFijo;
import dominio.ExportFormul;
import dominio.Exportacion;
import dominio.GeneradorEolico;
import dominio.GeneradorTermico;
import dominio.ImportEstac;
import dominio.ImportFijo;
import dominio.ImportFormul;
import dominio.Importacion;
import dominio.ParTipoDatoValor;
import dominio.Parque;
import dominio.Recurso;
import dominio.RecursoBase;
import dominio.TiempoAbsoluto;
import persistenciaMingo.XcargaDatos;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.*;
import javax.swing.JOptionPane;

/**
 *
 * @author ut469262
 */
public class ImplementadorEDF extends ImplementadorGeneral implements Serializable{

    /**
     **************************************************
     *
     * COMIENZAN LOS MÉTODOS QUE SOBREESCRIBEN LOS DE
     * IMPLEMENTADOR GENERAL
     *
     **************************************************
     */
    public ImplementadorEDF() {
        super();
    }

    @Override
    public void inicializaParticular() {
        /**
         * Verifica que el valor fijo asociado al tipo de dato "demanda"
         * coincida con el valor leído por la demamda
         */
    }

    /**
     * El método genera los resultados de una corrida del EDF correspondientes a un
     * TiempoAbsoluto. Los resultados son los costos en un numerario único.
     * El método espera cantCronicas crónicas y si hay una cantidad distinta detecta el error.
     * 
     * @param dirCorrida es el path del directorio donde están los resultados
     * de la corrida:
     * - en un archivo costocron.xlt que incluye el año del TiempoAbsoluto ta.
     * - en otro archivo sobrecostos_gnl.xlt que incluye el año del TiempoAbsoluto ta.
     * 
     * Los resultados se pasan de kUSD a MUSD y en esta última unidad se almacenan.
     * @param ta es el tiempo absoluto para el que se crerá el resultado.
     * @return es el ResSimulPaso del paso respectivo.
     */
    @Override
    public ResSimul creaRes(String dirCorrida, PeriodoDeTiempo per, Estudio est) throws XcargaDatos {

        String dirCostocron = dirCorrida + "/costocron.xlt";
        String dirSobrecostos = dirCorrida + "/sobrecostos_gnl.xlt";
        String sep = ",";
        TiempoAbsoluto ta = (TiempoAbsoluto) per;
        int cantCronicas = est.getDatosGenerales().getCantCronicas();
        boolean encontroAnio = false;
        ArrayList<ArrayList<String>> costoCron = null;
        ArrayList<ArrayList<String>> sobreCostos = null;        
        boolean sigo = true;
        while (sigo) {
            try {
                costoCron = CreadorTex.leeTextoBase(dirCostocron, sep);
                sigo = false;
            } catch (XcargaDatos ex) {
                sigo = false;
                System.out.println("Error en la lectura de costocron en directorio " + dirCorrida);
                int levantar = JOptionPane.showConfirmDialog(null, "Error en costocron, cuando quiera siga");
                if (levantar == JOptionPane.OK_OPTION) {
                    sigo = true;
                } else {
                    throw new XcargaDatos("Error en la lectura de costocron en directorio " + dirCorrida);
                }
            }
        }

        int anio = ta.getAnio();
        String anioStr = String.valueOf(anio);
        String anioSigStr = String.valueOf(anio + 1);
        
        /**        
         * Lee costocron.xlt 
         */ 
        int i = 0;
        do {
            if (costoCron.get(i).get(0).equalsIgnoreCase(anioStr)) {
                encontroAnio = true;
            }
            i++;
        } while (i < costoCron.size() && !encontroAnio);
        if (!encontroAnio) {
            throw new XcargaDatos("No se encontró el año " + anio
                    + " en archivo costocron del directorio " + dirCostocron);
        }
        // el i queda posicionado en la primera línea posterior al año y se suma 1 para evitar títulos
        i++;
        /**
         * Suma la totalidad de los costos de cada crónica y los agrega a un array
         * previa división por 1000 para pasar de kUSD a MUSD.
         */

        double sumacron;
        int ultCron = 0;
        ArrayList<Double> result = new ArrayList<Double>();
        for (int icron = 0; icron < cantCronicas; icron++) {
            sumacron = 0.0;
            if (costoCron.get(i).get(0).equalsIgnoreCase(anioSigStr)) {
                throw new XcargaDatos("El archivo costocron tiene menos crónicas que las especificadas");
            }
            for (int j = 1; j < costoCron.get(i).size(); j++) {
                sumacron = sumacron + Double.parseDouble(costoCron.get(i).get(j));
            }
            result.add(sumacron / 1000);

            ultCron = Integer.parseInt(costoCron.get(i).get(0));
            i++;  // avanza el índice en las líneas del archivo leído
        }
        /**
         *  i quedo posicionado luego de la última línea de costrocron con la crónica
         *  del año
         */
        if (i < costoCron.size()) {
            // hay más datos después del último leído
            int loDeDespues = (Integer.parseInt(costoCron.get(i).get(0)));
            if (loDeDespues == ultCron + 1) {
                throw new XcargaDatos("El archivo costocron tiene más crónicas que las especificadas");
            }
        }
        
        
        /**        
         * Lee sobrecostos_gnl.xlt 
         */   
        sigo = true;
        while (sigo) {
            try {
                sobreCostos = CreadorTex.leeTextoBase(dirSobrecostos, sep);
                sigo = false;
            } catch (XcargaDatos ex) {
                sigo = false;
                System.out.println("Error en la lectura de sobrecostos_gnl en directorio " + dirCorrida);
                int levantar = JOptionPane.showConfirmDialog(null, "Error en sobrecostos_gnl, cuando quiera siga");
                if (levantar == JOptionPane.OK_OPTION) {
                    sigo = true;
                } else {
                    throw new XcargaDatos("Error en la lectura de sobrecostos_gnl en directorio " + dirCorrida);
                }
            }
        }
 //       try {
 //           sobreCostos = CreadorTex.leeTextoBase(dirSobrecostos, sep);
 //       } catch (XcargaDatos ex) {
 //           throw new XcargaDatos("Error en la lectura de sobrecostos de GNL en directorio " + dirCorrida);
 //       }        
        
        i = 0;
        do {
            if (sobreCostos.get(i).get(0).equalsIgnoreCase(anioStr)) {
                encontroAnio = true;
            }
            i++;
        } while (i < sobreCostos.size() && !encontroAnio);
        if (!encontroAnio) {
            throw new XcargaDatos("No se encontró el año " + anio
                    + " en archivo sobrecostos_gnl del directorio " + dirCostocron);
        }
        // el i queda posicionado en la primera línea posterior al año que es el primer dato
        for (int icron = 0; icron < cantCronicas; icron++) {
            // divide por 1000 para pasar de kUSD a MUSD
            result.set(icron, result.get(icron)+ Double.parseDouble(sobreCostos.get(i).get(1))/1000);
            i++;  // avanza el índice en las líneas del archivo leído
        }        
                     
        /**
         * Se carga el resultado
         */
        ResSimul res = new ResSimul();
        res.setPer(ta);
        double[][] array2Doble = new double[1][cantCronicas];
        double[] aprobs = new double[cantCronicas];
        for (int j = 0; j < cantCronicas; j++) {
            array2Doble[0][j] = result.get(j);
            aprobs[j] = (double) 1 / cantCronicas;
        }
        res.setResult(array2Doble);
        res.setProbs(aprobs);
        res.setPer(ta);
        return res;
    }

    /**
     * Crea un objeto con los resultados detallados que deben cargarse en la base de datos detallados
     * Para eso lee enercron y costocron del EDF
     * @param dirCorrida nombre de la corrida (el directorio)
     * @param per PeriodoDeTiempo año
     * @param est el Estudio
     * @return
     * @throws XcargaDatos 
     */
    @Override
    public ResSimulDB creaResDB(String dirCorrida, PeriodoDeTiempo per, Estudio est) throws XcargaDatos {

        String dirCostocron = dirCorrida + "/costocron.xlt";
        String dirEnergcron = dirCorrida + "/energcron.xlt";
        String dirNomImpExp = dirCorrida + "/nomgenint.xlt";
        String sep = ",";
        TiempoAbsoluto ta = (TiempoAbsoluto) per;
        int cantCronicas = est.getDatosGenerales().getCantCronicas();
        boolean encontroAnio = false;
        ArrayList<ArrayList<String>> costoCron = null;
        ArrayList<ArrayList<String>> energCron = null;
        ArrayList<ArrayList<String>> nomImpExp = null;   
        ArrayList<String> nombresImp = new ArrayList<String>();
        ArrayList<String> nombresExp = new ArrayList<String>();
        boolean sigo = true;
        // Lee los archivos energcron y costocron
        // También lee el archivo de nombres de importaciones y exportaciones
        while (sigo) {
            try {
                costoCron = CreadorTex.leeTextoBase(dirCostocron, sep);
                energCron = CreadorTex.leeTextoBase(dirEnergcron, sep);
                nomImpExp = CreadorTex.leeTextoBase(dirNomImpExp, sep);                
                sigo = false;
            } catch (XcargaDatos ex) {
                sigo = false;
                System.out.println("Error en la lectura de costocron o energcron en directorio " + dirCorrida);
                int levantar = JOptionPane.showConfirmDialog(null, "Error en costocron o energcron, cuando quiera siga");
                if (levantar == JOptionPane.OK_OPTION) {
                    sigo = true;
                } else {
                    throw new XcargaDatos("Error en la lectura de costocron o energcron en directorio " + dirCorrida);
                }
            }
        }
        /**
         * Carga los nombres de importaciones y exportaciones quitandole los blancos de adelante y atrás
         */
        for (int iimp=1; iimp<nomImpExp.get(0).size(); iimp++){
            nombresImp.add(nomImpExp.get(0).get(iimp).trim());
        }
        for (int iexp=1; iexp<nomImpExp.get(1).size(); iexp++){
            nombresExp.add(nomImpExp.get(1).get(iexp).trim());
        }        
        
        /**
         * Encuentra el inicio del año buscado en energCron
         */
        int anio = ta.getAnio();
        String anioStr = String.valueOf(anio);

        int i = 0;
        do {
            if (energCron.get(i).get(0).equalsIgnoreCase(anioStr)) {
                encontroAnio = true;
            }
            i++;
        } while (i < costoCron.size() && !encontroAnio);
        if (!encontroAnio) {
            throw new XcargaDatos("No se encontró el año " + anio
                    + " en archivo costocron del directorio " + dirCostocron);
        }
        /**
         * el i queda posicionado en la primera línea posterior al año que es la de
         * nombres de las fuentes
         * 
         * ATENCIÓN: NO SON EL OBJETO Fuente del Mingo sino el campo fuente de la base de datos
         * que corresponde a un palier del EDF.
         * 
         */
        
        int cantFuentes = energCron.get(i).size() - 1;
        String[] nomF = new String[cantFuentes];
        String[] nomTipo = new String[cantFuentes];
        String[] nomSubTipo1 = new String[cantFuentes];
        String[] nomSubTipo2 = new String[cantFuentes];
        double[][] ener = new double[cantCronicas][cantFuentes];
        double[][] cost = new double[cantCronicas][cantFuentes];
        int fIni = i + 1;  // fIni es la fila del primer dato
        Pattern pimp=Pattern.compile("^IMPORT");
        Pattern pexp=Pattern.compile("^EXPORT");
        Matcher mimp, mexp;
        /**
         * fIni es el ordinal de la primera fila de energCron y costoCron
         * donde hay datos numéricos
         */
        
        // carga el nombre de las fuentes y el tipo y subtipos
        for (int ifu = 1; ifu <= cantFuentes; ifu++) {
            // sustituye nombre de importnn por nombres reales
            mimp = pimp.matcher(energCron.get(i).get(ifu));
            mexp = pexp.matcher(energCron.get(i).get(ifu));
            if(mimp.find()){
                int lugar = Integer.parseInt(energCron.get(i).get(ifu).substring(6).trim());
                nomF[ifu - 1] = nombresImp.get(lugar-1);
            }else if(mexp.find()){
                int lugar = Integer.parseInt(energCron.get(i).get(ifu).substring(6).trim());
                nomF[ifu - 1] = nombresExp.get(lugar-1);
            }        
            else{
                nomF[ifu - 1] = energCron.get(i).get(ifu).trim();
            }           
            for (int icron = 1; icron <= cantCronicas; icron++) {
                ener[icron - 1][ifu - 1] = Double.valueOf(energCron.get(fIni + icron - 1).get(ifu));
                if (ifu <= 5) {
                    cost[icron - 1][ifu - 1] = 0.0;
                } else {
                    cost[icron - 1][ifu - 1] = Double.valueOf(costoCron.get(fIni + icron - 1).get(ifu - 5));
                }
            }
            // identifica el RecursoBase asociado a la fuente
            RecursoBase rb;
            if(nomF[ifu-1].equalsIgnoreCase("DEMANDA")){
                nomTipo[ifu-1]="DEMANDA";
                nomSubTipo1[ifu-1]=" ";
                nomSubTipo2[ifu-1]=" ";
            }else if (nomF[ifu-1].length()>5 && nomF[ifu-1].substring(0,5).equalsIgnoreCase("FALLA")){
                nomTipo[ifu-1]="FALLA";
                nomSubTipo1[ifu-1]=" ";
                nomSubTipo2[ifu-1]=" ";                
            }
            else{
                if(nomF[ifu - 1].endsWith("_P") || nomF[ifu - 1].endsWith("_S")){
                    String nomTrun = nomF[ifu-1].substring(0, nomF[ifu-1].length()-2);
                    // hay que comparar con igual número de caracteres del nombre de los RecursoBase térmicos
                    rb = est.getConjRecB().getUnRecBTr(nomTrun);                
                }else{
                    // hay que comparar con el nombre de los RecursoBase
                    rb = est.getConjRecB().getUnRecB(nomF[ifu-1]);      
                }
                nomTipo[ifu-1]=rb.getTipo();
                nomSubTipo1[ifu-1]=rb.getSubtipo1();
                nomSubTipo2[ifu-1]=rb.getSubtipo2();
            }            
        }
        int[] numC= new int[cantCronicas];       
        for (int k=0; k<cantCronicas; k++){
            numC[k]= Integer.parseInt(energCron.get(fIni+k).get(0));
        }
           
        ResSimulDB rsDB = new ResSimulDB(cantCronicas, cantFuentes);
        rsDB.setCronicas(numC);
        rsDB.setNombFuente(nomF);
        rsDB.setMatEner(ener);
        rsDB.setMatCost(cost);         
        rsDB.setAnio(anio);
        rsDB.setTipo(nomTipo);
        rsDB.setSubtipo1(nomSubTipo1);
        rsDB.setSubtipo2(nomSubTipo2);
        return rsDB;

    }

    /**
     * El método crea los datos y archivo de comandos de una corrida corespondientes
     * a un PeríodoDeTiempo.
     *
     * ESTE METODO DEBE SER SOBREESCRITO PARA LAS CLASES QUE HEREDAN DE ESTA
     * PARA CADA IMPLEMENTACION CONCRETA PARA UN MODELO DE OPERACION
     *
     * @param idCorr es el identificador que describe la corrida.
     * @param dirCorrida es el path del directorio donde se generan
     * los datos y el archivo de comandos de la corrida.
     * @param per es el PeriodoDeTiempo que se simulará.
     * @param informacion es la información empaquetada que puedan precisar
     * los métodos de las clases que heredan de esta.
     */
    @Override
    public ParString creaDatosYComandos(String nombreCorr, IdentSimulPaso idCorr, String dirCorrida,
            ArrayList<Object> informacion)
            throws XcargaDatos {

        TiempoAbsoluto ta = idCorr.getTa();
        Estudio est = (Estudio) informacion.get(0);
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int anio = idCorr.getTa().getAnio();
        Calendar en1 = Calendar.getInstance();
        en1.set(anio, Calendar.JANUARY, 1);
        Calendar sa1 = Calendar.getInstance();
        /**
         *  Se da un año más de vida a los recursos para tener en cuenta el año
         *  de guardia.
         */
        sa1.set(anio + 1, Calendar.DECEMBER, 31);
        Parque par = idCorr.getParque();

        /**
         * *********************************************************************
         * Crea y graba el archivo genter.prm en el directorio dirCorrida
         * *********************************************************************
         *
         * La cantidad de tranches es el producto del multiplicador del Recurso
         * por la cantidad de módulos del RecursoBase
         *
         * La potencia de cada tranche es la potencia de cada modulo del RecursoBase
         *
         */
        String texto = textoListaGenter(idCorr, est);
        texto += "\r\n";
        String textoManter = "*MANTENIMIENTOS\r\n";
        textoManter += "*SEMANA 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52"+ "\r\n";
        
        ArrayList<Bloque> bloques;
        bloques = par.bloquesDelParque(est);
        bloques.addAll(Parque.bloquesComunesDeTA(ta, est));

        for (Bloque b : bloques) {      	
            Recurso rec = b.getRecurso();
            RecursoBase rb = b.getRecurso().getRecBase();
            if (rb instanceof GeneradorTermico && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
                ArrayList<Calendar> enSe = new ArrayList<Calendar>();
                ArrayList<Calendar> fuSe = new ArrayList<Calendar>();
                int multiplicador = b.getMultiplicador();
                int cantMod = rec.cantMod(datGen, ta);
                double potNominal = rec.potNom1Mod(datGen, ta);
                if (cantMod * potNominal == 0) {
                    int selec;
                    selec = JOptionPane.showOptionDialog(null, "Entrada antes de tiempoAbsolutoBase "
                            + rb.getNombre(), "¿Desea seguir?", JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE, null,
                            new Object[]{"Detener la ejecución", "Seguir"}, null);
                    // Devuelve 0, 1 o 2 según la opción
                    if (selec == 0) {
                        throw new XcargaDatos("Se detuvo ejecución porque un RecursoBase entraría en servicio "
                                + "antes de su tiempoAbsolutoBase");
                    }
                    Calendar enUltimo = Calendar.getInstance();
                    enUltimo.set(anio + 100, Calendar.JANUARY, 1);
                    enSe.add(enUltimo);
                    fuSe.add(enUltimo);
                } else {
                    int cantTranches = cantMod * multiplicador;
                    for (int it = 1; it <= cantTranches; it++) {
                        enSe.add(en1);
                        fuSe.add(sa1);
                    }
                }
                texto += textoDeGenTer(rb, potNominal, enSe, fuSe);
                StringBuilder mant = new StringBuilder();
                GeneradorTermico ter = (GeneradorTermico)rb; 
                if(ter.getManten().size()!= 0){
	            	for(int i=0; i<ter.getManten().size(); i++){
	            		mant.append(ter.getNombre() + ":");
		                for(int j=0; j<ter.getManten().get(i).size(); j++){   	
		                	mant.append(ter.getManten().get(i).get(j));
		                	mant.append(" "); 	
		                }
		                mant.append("\r\n");
	            	} 
	            	textoManter +=  mant.toString() + "\r\n"; 
                }
            }
                      
        }
        DirectoriosYArchivos.grabaTexto(dirCorrida + "/genter.prm", texto);


        DirectoriosYArchivos.grabaTexto(dirCorrida + "/manten.prm", textoManter);


        /**
         * *********************************************************************
         * Crea y graba el archivo prmgenint.d
         * *********************************************************************
         *
         */
        int ncron = est.getDatosGenerales().getCantCronicas();
        int nposte = est.getDatosGenerales().getCantPostes();
        String texto2 = textoListaImpoExpo(idCorr, ncron, nposte, est) + "\r\n";

        texto2 += "****************************************" + "\r\n";
        texto2 += "               IMPORTACIONES " + "\r\n";
        texto2 += "****************************************" + "\r\n";
        for (Bloque b : bloques) {
            RecursoBase rb = b.getRecurso().getRecBase();
            if (rb instanceof Importacion && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
                texto2 += texto1ImpoExpo(b, ta, datGen);
            }
        }
        // Se sigue cargando el mismo String texto2
        texto2 += "****************************************" + "\r\n";
        texto2 += "               EXPORTACIONES " + "\r\n";
        texto2 += "****************************************" + "\r\n";
        for (Bloque b : bloques) {
            RecursoBase rb = b.getRecurso().getRecBase();
            if (rb instanceof Exportacion && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
                texto2 += texto1ImpoExpo(b, ta, datGen);
            }
        }

        // Se sigue cargando el mismo String texto2
        texto2 += "****************************************" + "\r\n";
        texto2 += "               EOLICOS " + "\r\n";
        texto2 += "****************************************" + "\r\n";
        for (Bloque b : bloques) {
            Recurso rec = b.getRecurso();
            RecursoBase rb = rec.getRecBase();
            if (rb instanceof GeneradorEolico && rec.getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
                int multiplicador = b.getMultiplicador();
                int cantMod = rec.cantMod(datGen, ta);
                double potNominal = rec.potNom1Mod(datGen, ta);
                ArrayList<Double> potencias = new ArrayList<Double>();
                double potencia = multiplicador * cantMod * potNominal;
                potencias.add(potencia);
                ArrayList<Calendar> iniPot = new ArrayList<Calendar>();
                iniPot.add(en1);
                texto2 += textoDeEolico(rb, iniPot, potencias, datGen);
            }
        }

        // Se sigue cargando el mismo String texto2
        texto2 += "****************************************" + "\r\n";
        texto2 += "               BIOMASAS " + "\r\n";
        texto2 += "****************************************" + "\r\n";
        for (Bloque b : bloques) {
            Recurso rec = b.getRecurso();
            RecursoBase rb = rec.getRecBase();
            if (rb instanceof Biomasa && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
                int multiplicador = b.getMultiplicador();
                int cantMod = rec.cantMod(datGen, ta);
                double potNominal = rec.potNom1Mod(datGen, ta);
                ArrayList<Double> potencias = new ArrayList<Double>();
                double potencia = multiplicador * cantMod * potNominal;
                potencias.add(potencia);
                ArrayList<Calendar> iniPot = new ArrayList<Calendar>();
                iniPot.add(en1);         
                texto2 += textoDeBiomasa(rb, iniPot, potencias, datGen);
            }
        }        
        DirectoriosYArchivos.grabaTexto(dirCorrida + "/prmgenint.d", texto2);

        /**
         * *********************************************************************
         * Crea y graba el archivo de comandos corrida.bat
         * *********************************************************************
         */
        String textoBat = "";
        ArrayList<ParTipoDatoValor> pTDP = idCorr.getValoresTiposDatos();

//          // Carga el indicador de corrida donde encuentre en textoBaseComando el String "CORRIDA".
//          for (int i = 0; i < textoBaseComando.size(); i++) {
//               for (int j = 0; j < textoBaseComando.get(i).size(); j++) {
//                    if (textoBaseComando.get(i).get(j).equalsIgnoreCase("CORRIDA")) {
//                         textoBaseComando.get(i).set(j, nombreCorr);
//                    }
//               }
//          }
        textoBat = CreadorComandos.creaTextoDeUnComando(textoBaseComando,
                pTDP, plantillas, nombreCorr);
        DirectoriosYArchivos.grabaTexto(dirCorrida + "/corridas.bat", textoBat);
        /**
         * *********************************************************************
         * Crea y graba los archivos de parámetros de la corrida.bat
         * *********************************************************************
         *
         * Se copian sin modificar los archivos:
         * penpalma.d, prmecran.d, prmcosting.d
         * del directorio Datos_Implementacion/parametros
         * Se carga el año de la corrida en:
         * prmdoc.d y prmvagua.d
         *
         */
        // Files.copy(source, target, REPLACE_EXISTING);
        // hay que usar el comando de arriba cuando actualice Java
        // copia penpalma
        String archOrigen = dirDatosImplementador + "/parametros/penpalma.d";
        String archDestino = dirCorrida + "/" + "penpalma.d";
        try {
            UtilitariosGenerales.DirectoriosYArchivos.copy2(archOrigen, archDestino);
        } catch (Exception e) {
            throw new XcargaDatos(e.toString());
        }
        // copia cantsemeshy 
        archOrigen = dirDatosImplementador + "/parametros/cantsemeshy.d";
        archDestino = dirCorrida + "/" + "cantsemeshy.d";
        try {
            UtilitariosGenerales.DirectoriosYArchivos.copy2(archOrigen, archDestino);
        } catch (Exception e) {
            throw new XcargaDatos(e.toString());
        }        

        // copia permcran.d
        archOrigen = dirDatosImplementador + "/parametros/prmecran.d";
        archDestino = dirCorrida + "/" + "prmecran.d";
        try {
            UtilitariosGenerales.DirectoriosYArchivos.copy2(archOrigen, archDestino);
        } catch (Exception e) {
            throw new XcargaDatos(e.toString());
        }

        // copia prmcosting.d
        archOrigen = dirDatosImplementador + "/parametros/prmcosting.d";
        archDestino = dirCorrida + "/" + "prmcosting.d";
        try {
            UtilitariosGenerales.DirectoriosYArchivos.copy2(archOrigen, archDestino);
        } catch (Exception e) {
            throw new XcargaDatos(e.toString());
        }

        /*
         *************************
         * Crea el prmdoc.d
         * ***********************
         */
        // texto base
        String sep = "\\<\\?\\>"; // Separador de datos es "<?>"
        ArrayList<ArrayList<String>> textoBasePrmDoc;
        String archPrmDocBase = dirDatosImplementador + "/parametros/prmdoc.d";
        textoBasePrmDoc = ManejadorTextos.CreadorTex.leeTextoBase(archPrmDocBase, sep);

        // plantillas
        PlantillaEnTexto plantAnDoc = new PlantillaEnTexto();
        int[] posAnDoc = new int[2];
        posAnDoc[0] = 2;
        posAnDoc[1] = 2;
        ArrayList<int[]> aPos = new ArrayList<int[]>();
        aPos.add(posAnDoc);
        plantAnDoc.setPosiciones(aPos);
        plantAnDoc.setNombre("anio");
        ArrayList<PlantillaEnTexto> plants = new ArrayList<PlantillaEnTexto>();
        plants.add(plantAnDoc);

        // valores
        ArrayList<String> valores = new ArrayList<String>();
        valores.add(String.valueOf(anio));

        //
        String archPrmDoc = dirCorrida + "/prmdoc.d";
        String textoPrmDoc = ManejadorTextos.CreadorTex.creaTexto(textoBasePrmDoc, valores, plants);
        DirectoriosYArchivos.grabaTexto(archPrmDoc, textoPrmDoc);

        /**
         ******************************
         * Crea el prmvagua.d
         * ****************************
         */
        // texto base
        ArrayList<ArrayList<String>> textoBasePrmVagua;
        String archPrmVaguaBase = dirDatosImplementador + "/parametros/prmvagua.d";
        textoBasePrmVagua = ManejadorTextos.CreadorTex.leeTextoBase(archPrmVaguaBase, sep);

        // plantillas
        plantAnDoc = new PlantillaEnTexto();
        posAnDoc = new int[2];
        posAnDoc[0] = 2;
        posAnDoc[1] = 2;
        aPos = new ArrayList<int[]>();
        aPos.add(posAnDoc);
        plantAnDoc.setPosiciones(aPos);
        plantAnDoc.setNombre("anio");
        plants = new ArrayList<PlantillaEnTexto>();
        plants.add(plantAnDoc);

        //
        String archPrmVagua = dirCorrida + "/prmvagua.d";
        String textoPrmVagua = ManejadorTextos.CreadorTex.creaTexto(textoBasePrmVagua, valores, plants);
        DirectoriosYArchivos.grabaTexto(archPrmVagua, textoPrmVagua);

        /**
         * **************************
         * Crea el prmuring.d
         * **************************
         */
        // texto base
        ArrayList<ArrayList<String>> textoBasePrMuring;
        String archPrMuringBase = dirDatosImplementador + "/parametros/prmuring.d";
        textoBasePrMuring = ManejadorTextos.CreadorTex.leeTextoBase(archPrMuringBase, sep);

        // plantillas
        plantAnDoc = new PlantillaEnTexto();
        posAnDoc = new int[2];
        posAnDoc[0] = 2;
        posAnDoc[1] = 2;
        aPos = new ArrayList<int[]>();
        aPos.add(posAnDoc);
        posAnDoc = new int[2];
        posAnDoc[0] = 3;
        posAnDoc[1] = 2;
        aPos.add(posAnDoc);
        plantAnDoc.setPosiciones(aPos);
        plantAnDoc.setNombre("anio");
        plants = new ArrayList<PlantillaEnTexto>();
        plants.add(plantAnDoc);

        //
        String archPrMuring = dirCorrida + "/prmuring.d";
        String textoPrMuring = ManejadorTextos.CreadorTex.creaTexto(textoBasePrMuring, valores, plants);
        DirectoriosYArchivos.grabaTexto(archPrMuring, textoPrMuring);


        /**
         * **************************
         * Crea el prmecran.d
         * **************************
         */
        // texto base
        ArrayList<ArrayList<String>> textoBasePrEcran;
        String archPrEcran = dirDatosImplementador + "/parametros/prmecran.d";
        textoBasePrEcran = ManejadorTextos.CreadorTex.leeTextoBase(archPrEcran, sep);

        // plantillas
        plantAnDoc = new PlantillaEnTexto();
        posAnDoc = new int[2];
        posAnDoc[0] = 2;
        posAnDoc[1] = 2;
        aPos = new ArrayList<int[]>();
        aPos.add(posAnDoc);
        posAnDoc = new int[2];
        posAnDoc[0] = 3;
        posAnDoc[1] = 2;
        aPos.add(posAnDoc);
        plantAnDoc.setPosiciones(aPos);
        plantAnDoc.setNombre("anio");
        plants = new ArrayList<PlantillaEnTexto>();
        plants.add(plantAnDoc);

        //
        archPrEcran = dirCorrida + "/prmecran.d";
        String textoPrEcran = ManejadorTextos.CreadorTex.creaTexto(textoBasePrEcran, valores, plants);
        DirectoriosYArchivos.grabaTexto(archPrEcran, textoPrEcran);

        return new ParString("","");   // no se usa lo que retorna
    }

    /**
     **************************************************
     * FINALIZAN LOS MÉTODOS QUE SOBREESCRIBEN LOS DE
     * IMPLEMENTADOR GENERAL
     **************************************************
     */
    /**
     * Invoca al método que crea un texto de importación o exportación
     * @param b
     * @param ta
     * @param datGen
     * @return
     * @throws persistenciaMingo.XcargaDatos
     */
    public String texto1ImpoExpo(Bloque b, TiempoAbsoluto ta,
            DatosGeneralesEstudio datGen) throws XcargaDatos {
        /**
         *
         * La potencia de cada palier es el producto del multiplicador del Recurso
         * por la cantidad de módulos del RecursoBase por la potencia de
         * cada módulo del RecursoBase
         *
         */
//        kk OJO QUE SE SUMEN TODOS LOS EÓLICOS DEL MISMO FIJO
        String texto = "\r\n";
        int anio = ta.getAnio();
        Recurso rec = b.getRecurso();
        RecursoBase rb = rec.getRecBase();
        int multiplicador = b.getMultiplicador();
        double pot1Mod = rec.potNom1Mod(datGen, ta);
        int cantMod = rec.cantMod(datGen, ta);
        double potencia = pot1Mod * cantMod * multiplicador;
        ArrayList<Calendar> iniPot = new ArrayList<Calendar>();
        Calendar en1 = Calendar.getInstance();
        en1.set(anio, Calendar.JANUARY, 1);
        iniPot.add(en1);
        ArrayList<Double> potencias = new ArrayList<Double>();
        potencias.add(potencia);
        texto = textoDeImpoExpo(rb, iniPot, potencias, datGen);
        return texto;
    }

    /**
     * Crea un string con controles de salto de línea con la lista de centrales térmicas
     * @param idCorr es el identificador de la corrida
     * @return es el String con saltos de línea con
     */
    public static String textoListaGenter(IdentSimulPaso idCorr, Estudio est) throws XcargaDatos {

        TiempoAbsoluto ta = idCorr.getTa();
        int anio = ta.getAnio();
        Parque par = idCorr.getParque();
        String lista = "* ARCHIVO GENERADO POR MINGO. " + "\r\n";
        lista += " $PRMTER" + "\r\n";
        lista += "  ANINI=" + anio + "\r\n";
        lista += "  NAN=1" + "\r\n";
        lista += "  TASANI=0.1" + "\r\n";
        lista += "  VIDAUTIL=20" + "\r\n";
        lista += "  PARQUE=";       
        ArrayList<Bloque> bloques = par.bloquesDelParque(est);
        bloques.addAll(Parque.bloquesComunesDeTA(ta, est));
        for (Bloque b : bloques) {
            if (b.getRecurso().getRecBase().getSubtipo1().equalsIgnoreCase(SubTipo1.TERM)) {            
                if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC) &&
                        b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
                    lista += "'" + b.getRecurso().getRecBase().getNombre() + "',";                   
                }
            }
        }
        lista += "\r\n";
        lista += " $END" + "\r\n";
        return lista;
    }

    /**
     * Genera el texto de un palier térmico para EDF incluso los saltos de línea
     *
     * @param rb es el RecursoBase que es un GeneradorTermico
     * @param potNominal es la potencia nominal de un tranche del rb
     * se incrementa la potencia de mínimo técnico en del año base en la misma proporcion
     * que potNominal/gt.getPotNominal(la del año base)
     * @param enSe es una lista de fechas de entrada en servicio de paliers
     * @param fuSe es una lista de fechas de salida de servicio de paliers
     * @return texto es el texto con saltos de línea del palier.
     * @throws persistenciaMingo.XcargaDatos
     */
    public static String textoDeGenTer(RecursoBase rb, double potNominal,
            ArrayList<Calendar> enSe, ArrayList<Calendar> fuSe) throws XcargaDatos {

        if (enSe.size() != fuSe.size()) {
            throw new XcargaDatos("El recursoBase " + rb.getNombre()
                    + "tiene distinto número de entradas y de salidas de tranches");
        }
        GeneradorTermico gt = (GeneradorTermico) rb;
        String texto = "\r\n";
        texto += "* Generador térmico : " + rb.getNombre() + "\r\n";
        texto += "$DEFMAQ" + "\r\n";
        texto += "NOMBRE=" + "'" + gt.getNombre() + "'" + "\r\n";
        texto += "TIPO=" + "'" + gt.getMintec() + "'" + "\r\n";
        texto += "POTMAX=" + potNominal + "\r\n";
        double potmin = gt.getPotenciaMintec() * potNominal / gt.getPotNominalTABase();       
        texto += "POTMIN=" + UtilitariosGenerales.Numeros.redondeaDouble(potmin, 1) + "\r\n";
        texto += "DPOTAN=" + gt.getDpotan() + "\r\n";
        texto += "RENNOM=" + gt.getRendPotenciaNom() + "\r\n";
        texto += "RENMIN=" + gt.getRendPotenciaMin() + "\r\n";
        texto += "DRENAN=" + gt.getDrenan() + "\r\n";
        texto += "DISP=" + gt.getDisponibilidad() + "\r\n";
        texto += "COMPRI=" + "'" + gt.getCombustibles().get(0).getNombre() + "'" + "\r\n";
        if (gt.getCombustibles().size() > 1) {
            texto += "COMSEC=" + "'" + gt.getCombustibles().get(1).getNombre() + "'" + "\r\n";
        } else {
            texto += "COMSEC=" + "'NULO'" + "\r\n";
        }
        texto += "DPOTCS=" + gt.getDPotCS() + "\r\n";
        texto += "DRENCS=" + gt.getDPotCS() + "\r\n";

        texto += "ENSE=";
        int anio = 0;
        int sem = 0;
        String stSem = "";
        for (int itran = 0; itran < enSe.size(); itran++) {
            anio = enSe.get(itran).get(Calendar.YEAR);
            sem = FCalendar.semDelAnio(enSe.get(itran));
            if (sem < 10) {
                stSem = "0" + sem;
            } else {
                stSem = Integer.toString(sem);
            }
            if (itran < enSe.size() - 1) {
                texto += "'" + anio + ":" + stSem + "',";
            } else {
                texto += "'" + anio + ":" + stSem + "'";
            }
        }
        texto += "\r\n";
        texto += "FUSE=";
        for (int itran = 0; itran < fuSe.size(); itran++) {
            anio = fuSe.get(itran).get(Calendar.YEAR);
            sem = FCalendar.semDelAnio(fuSe.get(itran));
            if (sem < 10) {
                stSem = "0" + sem;
            } else {
                stSem = Integer.toString(sem);
            }
            if (itran < fuSe.size() - 1) {
                texto += "'" + anio + ":" + stSem + "',";
            } else {
                texto += "'" + anio + ":" + stSem + "'";
            }
        }
        texto += "\r\n";
        texto += "COYMV=" + gt.getCOYMV() + "\r\n";
        texto += "COYMF=" + gt.getCOYMF() + "\r\n";
        texto += "INVESP=0.0" + "\r\n";
        texto += "CONSTR=0" + "\r\n";
        texto += "$END" + "\r\n";
        return texto;
    }

    public static String textoListaImpoExpo(IdentSimulPaso idCorr, int ncron, int nposte, Estudio est) throws XcargaDatos {
        TiempoAbsoluto ta = idCorr.getTa();
        int anio = ta.getAnio();
        Parque par = idCorr.getParque();
        String lista = "* ARCHIVO GENERADO POR MINGO. " + "\r\n";
        lista += " $PRMGINT" + "\r\n";
        lista += "  ANINI=" + anio + "\r\n";
        // se crean dos años de archivos
        lista += "  NAN=1" + "\r\n";
        lista += "  NUMCRO=" + ncron + "\r\n";
        lista += "  NPOSTE=" + nposte + "\r\n";
        lista += "  ESCAL='NUL'" + "\r\n";
        lista += "  PALIMP=";
        ArrayList<Bloque> bloques = par.bloquesDelParque(est);
        bloques.addAll(Parque.bloquesComunesDeTA(ta, est));
        for (Bloque b : bloques) {
            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC)) {
                RecursoBase rb = b.getRecurso().getRecBase();
                if ((rb instanceof Importacion || rb instanceof GeneradorEolico ||
                        rb instanceof Biomasa) && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
                    lista += "'" + b.getRecurso().getRecBase().getNombre() + "',";          
                }
            }
        }
        lista += "\r\n";
        lista += "  SORTECRON=";        
        int ib = 0;             
        for (Bloque b : bloques) {
            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC)) {
                RecursoBase rb = b.getRecurso().getRecBase();
                if ((rb instanceof Importacion || rb instanceof GeneradorEolico ||
                        rb instanceof Biomasa) && b.getRecurso().getEstado().equalsIgnoreCase(EstadosRec.OPE)) {
                    if (ib > 0) {
                        lista += ",";
                    }
                    // Para EDF la variable aleatOper puede valer: "SORTECRON=0" o "SORTECRON=1"                    
                    if ((b.getRecurso().getRecBase().getAleatOper().length()==11) ){
                        String cod = b.getRecurso().getRecBase().getAleatOper().substring(0, 10);
                        if (cod.equalsIgnoreCase("SORTECRON=")){
                            lista +=  b.getRecurso().getRecBase().getAleatOper().substring(10);
                            ib++;                            
                        }else{
                            throw new XcargaDatos("error en aleatOper, RecursoBase "+rb.getNombre());
                        }    
                    }else{                                           
                        throw new XcargaDatos("error en aleatOper, RecursoBase " +rb.getNombre());
                    }
                }
            }
        }        
        lista += "\r\n";        
        lista += "  PALEXP=";
        ib = 0;
        for (Bloque b : bloques) {
            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC)) {
                RecursoBase rb = b.getRecurso().getRecBase();
                if (rb instanceof Exportacion) {
                    if (ib > 0) {
                        lista += ",";
                    }
                    lista += "'" + b.getRecurso().getRecBase().getNombre() + "'";
                    ib++;
                }
            }
        }
        lista += "\r\n";
        lista += " $END" + "\r\n";
        return lista;
    }

    /**
     * Crea el texto de entrada para un RecursoBase importación o exportacion
     * a partir de una lista de potencias nominales y la lista de fechas
     * en las que esa potencia nominal comienza a valer.
     *
     *
     * @param rb es el RecursoBase importación.
     * @param iniPot es la lista de fechas de inicio de validez de las potencias nominales.
     * @param potencias es la lista de potencias.
     * @return texto.
     */
    public static String textoDeImpoExpo(RecursoBase rb, ArrayList<Calendar> iniPot,
            ArrayList<Double> potencias, DatosGeneralesEstudio datGen) throws XcargaDatos {

        if (iniPot.size() != potencias.size()) {
            throw new XcargaDatos("El recursoBase " + rb.getNombre()
                    + "tiene distinto número de fechas de potencia y de datos de potencia");
        }
        String texto = null;
        Calendar iniPer, finPer;
        TiempoAbsoluto finEstudio = datGen.getFinEstudio();
        Calendar calFinEst = FCalendar.fechaFinDeTa(finEstudio, datGen);
        String tipo;
        String nombre;
        String pais;
        int nest;
        int ntram;
        ArrayList<Integer> semfinest;
        ArrayList<Double> preest;
        ArrayList<Double> pretram;
        ArrayList<Double> facest;
        ArrayList<Double> limtram;
        ArrayList<Double> factram;
        ArrayList<Double> disptram;
        ArrayList<Double> dispest;
        ArrayList<Double> escest;
        Double potnominal;
        ArrayList<ArrayList<Double>> potest;
        ArrayList<ArrayList<Double>> pottram;
        String ref;

        if (rb instanceof ImportEstac || rb instanceof ExportEstac) {
            for (int ic = 0; ic < iniPot.size(); ic++) {

                if (rb instanceof ImportEstac) {
                    ImportEstac rbi = (ImportEstac) rb;
                    tipo = "'IMP'";
                    nombre = rbi.getNombre();
                    pais = rbi.getPais();
                    nest = rbi.getNest();
                    semfinest = rbi.getSemFinEst();
                    preest = rbi.getPreEst();
                    facest = rbi.getFacEst();
                    dispest = rbi.getDispEst();
                    potest = rbi.getPotEst();
                    potnominal = rbi.getPotNominalTABase();
                } else {
                    ExportEstac rbe = (ExportEstac) rb;
                    tipo = "'EXP'";
                    nombre = rbe.getNombre();
                    pais = rbe.getPais();
                    nest = rbe.getNest();
                    semfinest = rbe.getSemFinEst();
                    preest = rbe.getPreEst();
                    facest = rbe.getFacEst();
                    dispest = rbe.getDispEst();
                    potest = rbe.getPotEst();
                    potnominal = rbe.getPotNominalTABase();

                }
                nombre = "'" + nombre + "'";

                texto = "$ESTAC" + "\r\n";
                texto += "NOMPAL=" + nombre + "\r\n";
                texto += "TIPO=" + tipo + "\r\n";
                texto += "PAIS=" + "'" + pais + "'" + "\r\n";
                iniPer = iniPot.get(ic);
                if (ic < iniPot.size() - 1) {
                    finPer = iniPot.get(ic + 1);
                } else {
                    finPer = calFinEst;
                }
                texto += textoIniFin(iniPer, finPer);
                texto += "NEST=" + nest + "\r\n";
                texto += "SEMFINEST= ";
                for (int j = 0; j < semfinest.size(); j++) {
                    texto += semfinest.get(j) + ",";
                }
                texto += "\r\n";
                texto += "PREEST= ";
                for (int j = 0; j < preest.size(); j++) {
                    texto += preest.get(j) + ",";
                }
                texto += "\r\n";
                texto += "FACEST= ";
                for (int j = 0; j < facest.size(); j++) {
                    texto += facest.get(j) + ",";
                }
                texto += "\r\n";
                texto += "POTEST= ";
                for (int i = 0; i < potest.size(); i++) {
                    for (int j = 0; j < potest.get(i).size(); j++) {
                        double pot = potest.get(i).get(j)
                                * (potencias.get(ic) / potnominal);                        
                        texto += UtilitariosGenerales.Numeros.redondeaDouble(pot, 1) + ", ";
                        /**
                         * Las potencias son proporcionales a la potencia nominal
                         */
                    }
                    texto += "\r\n";
                }
                texto += "DISPEST= ";
                for (int j = 0; j < dispest.size(); j++) {
                    texto += dispest.get(j) + ",";
                }
                texto += "\r\n";
                texto += "$END";
                texto += "\r\n";
                texto += "\r\n";
            }
        } else if (rb instanceof ImportFormul || rb instanceof ExportFormul) {
            if (rb instanceof ImportFormul) {
                ImportFormul rbf = (ImportFormul) rb;
                tipo = "'IMP'";
                nombre = rbf.getNombre();
                pais = rbf.getPais();
                ntram = rbf.getNtram();
                limtram = rbf.getLimTram();
                pretram = rbf.getPreTram();
                factram = rbf.getFacTram();
                disptram = rbf.getDispTram();
                pottram = rbf.getPotTram();
                potnominal = rbf.getPotNominalTABase();
            } else {
                ExportFormul rbf = (ExportFormul) rb;
                tipo = "'EXP'";
                nombre = rbf.getNombre();
                pais = rbf.getPais();
                ntram = rbf.getNtram();
                limtram = rbf.getLimTram();
                pretram = rbf.getPreTram();
                factram = rbf.getFacTram();
                disptram = rbf.getDispTram();
                pottram = rbf.getPotTram();
                potnominal = rbf.getPotNominalTABase();
            }
            nombre = "'" + nombre + "'";

            for (int ic = 0; ic < iniPot.size(); ic++) {
                if(rb instanceof ImportFormul){             
                    ImportFormul rbf = (ImportFormul) rb;
                    texto = "* Importacion : " + nombre + "\r\n";
                    texto += "$FORMUL" + "\r\n";
                    texto += "NOMPAL=" + nombre + "\r\n";
                    texto += "TIPO=" + tipo + "\r\n";
                    texto += "PAIS=" + "'" + pais + "'" + "\r\n";
                    iniPer = iniPot.get(ic);
                    if (ic < iniPot.size() - 1) {
                        finPer = iniPot.get(ic + 1);
                    } else {
                        finPer = calFinEst;
                    }
                    texto += textoIniFin(iniPer, finPer);
                    texto += "NTRAM=" + ntram + "\r\n";
                    texto += "LIMTRA=";
                    for (int j = 0; j < limtram.size(); j++) {
                        texto += limtram.get(j) + ",";
                    }
                    texto += "\r\n";
                    texto += "PRETRAM= ";
                    for (int j = 0; j < pretram.size(); j++) {
                        texto += pretram.get(j) + ",";
                    }
                    texto += "\r\n";
                    texto += "FACTRAM= ";
                    for (int j = 0; j < factram.size(); j++) {
                        texto += factram.get(j) + ",";
                    }
                    texto += "\r\n";
                    texto += "POTRAM= ";
                    for (int i = 0; i < pottram.size(); i++) {
                        for (int j = 0; j < pottram.get(i).size(); j++) {
                            double pot = pottram.get(i).get(j)
                                    * (potencias.get(ic) / potnominal);
                            texto += UtilitariosGenerales.Numeros.redondeaDouble(pot, 1) + ", ";
                            /**
                             * Las potencias son proporcionales a la potencia nominal
                             */
                        }
                        texto += "\r\n";
                    }
                    texto += "DISPTRAM= ";
                    for (int j = 0; j < disptram.size(); j++) {
                        texto += disptram.get(j) + ",";
                    }
                    texto += "\r\n";
                    texto += "$END";
                    texto += "\r\n";
                    texto += "\r\n";
                } else if (rb instanceof ExportFormul) {
                    ExportFormul rbf = (ExportFormul) rb;
                    texto = "* Exportacion : " + nombre + "\r\n";
                    texto += "$FORMUL" + "\r\n";
                    texto += "NOMPAL=" + nombre + "\r\n";
                    texto += "TIPO=" + tipo + "\r\n";
                    texto += "PAIS=" + "'" + pais + "'" + "\r\n";
                    iniPer = iniPot.get(ic);
                    if (ic < iniPot.size() - 1) {
                        finPer = iniPot.get(ic + 1);
                    } else {
                        finPer = calFinEst;
                    }
                    texto += textoIniFin(iniPer, finPer);
                    texto += "NTRAM=" + ntram + "\r\n";
                    texto += "LIMTRA=";
                    for (int j = 0; j < limtram.size(); j++) {
                        texto += limtram.get(j) + ",";
                    }
                    texto += "\r\n";
                    texto += "PRETRAM= ";
                    for (int j = 0; j < pretram.size(); j++) {
                        texto += pretram.get(j) + ",";
                    }
                    texto += "\r\n";
                    texto += "FACTRAM= ";
                    for (int j = 0; j < factram.size(); j++) {
                        texto += factram.get(j) + ",";
                    }
                    texto += "\r\n";
                    texto += "POTRAM= ";
                    for (int i = 0; i < pottram.size(); i++) {
                        for (int j = 0; j < pottram.get(i).size(); j++) {
                            double pot = pottram.get(i).get(j)
                                    * (potencias.get(ic) / potnominal);
                            texto += UtilitariosGenerales.Numeros.redondeaDouble(pot, 1) + ", ";
                            /**
                             * Las potencias son proporcionales a la potencia nominal
                             */
                        }
                        texto += "\r\n";
                    }
                    texto += "DISPTRAM= ";
                    for (int j = 0; j < disptram.size(); j++) {
                        texto += disptram.get(j) + ",";
                    }
                    texto += "\r\n";
                    texto += "$END";
                    texto += "\r\n";
                    texto += "\r\n";
                }
            }

        } else if (rb instanceof ImportFijo || rb instanceof ExportFijo) {

            if (rb instanceof ImportFijo) {
                ImportFijo rbfi = (ImportFijo) rb;
                nombre = rbfi.getNombre();
                tipo = "'IMP'";
                ref = rbfi.getRef();
                semfinest = rbfi.getSemFinEst();
                escest = rbfi.getEscEst();
                potnominal = rbfi.getPotNominalTABase();
                dispest = rbfi.getDispEst();
            } else {
                ExportFijo rbfi = (ExportFijo) rb;
                nombre = rbfi.getNombre();
                tipo = "'EXP'";
                ref = rbfi.getRef();
                semfinest = rbfi.getSemFinEst();
                escest = rbfi.getEscEst();
                potnominal = rbfi.getPotNominalTABase();
                dispest = rbfi.getDispEst();
            }



            for (int ic = 0; ic < iniPot.size(); ic++) {
                ImportFijo rbfi = (ImportFijo) rb;
                texto = "* Importacion : " + nombre + "\r\n";
                texto += "$FIJO" + "\r\n";
                texto += "NOMPAL=" + nombre + "\r\n";
                texto += "TIPO=" + tipo + "\r\n";
                texto += "REF=" + "'" + ref + "'" + "\r\n";
                iniPer = iniPot.get(ic);
                if (ic < iniPot.size() - 1) {
                    finPer = iniPot.get(ic + 1);
                } else {
                    finPer = calFinEst;
                }
                texto += textoIniFin(iniPer, finPer);
                texto += "NEST=" + rbfi.getNest();
                texto += "SEMFINEST= ";
                for (int j = 0; j < semfinest.size(); j++) {
                    texto += semfinest.get(j) + ",";
                }
                texto += "\r\n";
                texto += "ESCEST= ";
                double val = 0;
                for (int j = 0; j < rbfi.getEscEst().size(); j++) {
                    
                    if (rbfi.isRefUnitario()) {
                        val = potencias.get(ic);
                    } else {
                        val = potencias.get(ic) / rbfi.getPotNominalTABase();
                    }
                    texto += rbfi.getEscEst().get(j) + ",";
                }
                /**
                 * Las potencias son proporcionales a la potencia nominal con base 1
                 * si el recurso esReUnitario y con base potNominal del RecursoBase de
                 * lo contrario.
                 */
                texto += "\r\n";
                texto += "DISPEST= ";
                for (int j = 0; j < rbfi.getDispEst().size(); j++) {
                    texto += rbfi.getDispEst().get(j) + ",";
                }
                texto += "\r\n";
                texto += "$END";
                texto += "\r\n";
                texto += "\r\n";
            }
        } else {
            throw new XcargaDatos("Un RecursoBase no es de ningún tipo de importación existente");
        }
        return texto;
    }
    
    /**
     * Crea el texto de entrada para un RecursoBase importación o exportacion
     * a partir de una lista de potencias nominales y la lista de fechas
     * en las que esa potencia nominal comienza a valer.
     *
     *
     * @param rb es el RecursoBase importación.
     * @param iniPot es la lista de fechas de inicio de validez de las potencias nominales.
     * @param potencias es la lista de potencias.
     * @return texto.
     */
    public static String textoDeBiomasa(RecursoBase rb, ArrayList<Calendar> iniPot,
            ArrayList<Double> potencias, DatosGeneralesEstudio datGen) throws XcargaDatos {

        if (iniPot.size() != potencias.size()) {
            throw new XcargaDatos("El recursoBase " + rb.getNombre()
                    + "tiene distinto número de fechas de potencia y de datos de potencia");
        }
        String texto = null;
        Calendar iniPer, finPer;
        TiempoAbsoluto finEstudio = datGen.getFinEstudio();
        Calendar calFinEst = FCalendar.fechaFinDeTa(finEstudio, datGen);
        String tipo;
        String nombre;
        String pais;
        int nest;
        int ntram;
        ArrayList<Integer> semfinest;
        ArrayList<Double> preest;
        ArrayList<Double> pretram;
        ArrayList<Double> facest;
        ArrayList<Double> limtram;
        ArrayList<Double> factram;
        ArrayList<Double> disptram;
        ArrayList<Double> dispest;
        ArrayList<Double> escest;
        Double potnominal;
        ArrayList<ArrayList<Double>> potest;
        ArrayList<ArrayList<Double>> pottram;
        String ref;

        if (rb instanceof BiomasaEstac ) {
            for (int ic = 0; ic < iniPot.size(); ic++) {

                BiomasaEstac rbi = (BiomasaEstac) rb;
                tipo = "'IMP'";
                nombre = rbi.getNombre();
//                pais = rbi.getPais();
                nest = rbi.getNest();
                semfinest = rbi.getSemFinEst();
                preest = rbi.getPreEst();
                facest = rbi.getFacEst();
                dispest = rbi.getDispEst();
                potest = rbi.getPotEst();
                potnominal = rbi.getPotNominalTABase();
   
                nombre = "'" + nombre + "'";

                texto = "$ESTAC" + "\r\n";
                texto += "NOMPAL=" + nombre + "\r\n";
                texto += "TIPO=" + tipo + "\r\n";
                texto += "PAIS=" + "'" + "BIOLANDIA" + "'" + "\r\n";
                iniPer = iniPot.get(ic);
                if (ic < iniPot.size() - 1) {
                    finPer = iniPot.get(ic + 1);
                } else {
                    finPer = calFinEst;
                }
                texto += textoIniFin(iniPer, finPer);
                texto += "NEST=" + nest + "\r\n";
                texto += "SEMFINEST= ";
                for (int j = 0; j < semfinest.size(); j++) {
                    texto += semfinest.get(j) + ",";
                }
                texto += "\r\n";
                texto += "PREEST= ";
                for (int j = 0; j < preest.size(); j++) {
                    texto += preest.get(j) + ",";
                }
                texto += "\r\n";
                texto += "FACEST= ";
                for (int j = 0; j < facest.size(); j++) {
                    texto += facest.get(j) + ",";
                }
                texto += "\r\n";
                texto += "POTEST= ";
                for (int i = 0; i < potest.size(); i++) {
                    for (int j = 0; j < potest.get(i).size(); j++) {
                        double pot = potest.get(i).get(j)
                                * (potencias.get(ic) / potnominal);                        
                        texto += UtilitariosGenerales.Numeros.redondeaDouble(pot , 1) + ", ";
                        /**
                         * Las potencias son proporcionales a la potencia nominal
                         */
                    }
                    texto += "\r\n";
                }
                texto += "DISPEST= ";
                for (int j = 0; j < dispest.size(); j++) {
                    texto += dispest.get(j) + ",";
                }
                texto += "\r\n";
                texto += "$END";
                texto += "\r\n";
                texto += "\r\n";
            }
        } else if (rb instanceof BiomasaFormul ) {

            BiomasaFormul rbf = (BiomasaFormul) rb;
            tipo = "'IMP'";
            nombre = rbf.getNombre();
            pais = rbf.getPais();
            ntram = rbf.getNtram();
            limtram = rbf.getLimTram();
            pretram = rbf.getPreTram();
            factram = rbf.getFacTram();
            disptram = rbf.getDispTram();
            pottram = rbf.getPotTram();
            potnominal = rbf.getPotNominalTABase();
 
            nombre = "'" + nombre + "'";

            for (int ic = 0; ic < iniPot.size(); ic++) {
                texto = "* Biomasa : " + nombre + "\r\n";
                texto += "$FORMUL" + "\r\n";
                texto += "NOMPAL=" + nombre + "\r\n";
                texto += "TIPO=" + tipo + "\r\n";
                texto += "PAIS=" + "'" + pais + "'" + "\r\n";
                iniPer = iniPot.get(ic);
                if (ic < iniPot.size() - 1) {
                    finPer = iniPot.get(ic + 1);
                } else {
                    finPer = calFinEst;
                }
                texto += textoIniFin(iniPer, finPer);
                texto += "NTRAM=" + ntram + "\r\n";
                texto += "LIMTRA=";
                for (int j = 0; j < limtram.size(); j++) {
                    texto += limtram.get(j) + ",";
                }
                texto += "\r\n";
                texto += "PRETRAM= ";
                for (int j = 0; j < pretram.size(); j++) {
                    texto += pretram.get(j) + ",";
                }
                texto += "\r\n";
                texto += "FACTRAM= ";
                for (int j = 0; j < factram.size(); j++) {
                    texto += factram.get(j) + ",";
                }
                texto += "\r\n";
                texto += "POTRAM= ";
                for (int i = 0; i < pottram.size(); i++) {
                    for (int j = 0; j < pottram.get(i).size(); j++) {
                        texto += pottram.get(i).get(j)
                                * (potencias.get(ic) / potnominal) + ", ";
                        /**
                         * Las potencias son proporcionales a la potencia nominal
                         */
                    }
                    texto += "\r\n";
                }
                texto += "DISPTRAM= ";
                for (int j = 0; j < disptram.size(); j++) {
                    texto += disptram.get(j) + ",";
                }
                texto += "\r\n";
                texto += "$END";
                texto += "\r\n";
                texto += "\r\n";
            }

//        } else if (rb instanceof ImportFijo || rb instanceof ExportFijo) {
//
//            if (rb instanceof ImportFijo) {
//                ImportFijo rbfi = (ImportFijo) rb;
//                nombre = rbfi.getNombre();
//                tipo = "'IMP'";
//                ref = rbfi.getRef();
//                semfinest = rbfi.getSemFinEst();
//                escest = rbfi.getEscEst();
//                potnominal = rbfi.getPotNominalTABase();
//                dispest = rbfi.getDispEst();
//            } else {
//                ExportFijo rbfi = (ExportFijo) rb;
//                nombre = rbfi.getNombre();
//                tipo = "'EXP'";
//                ref = rbfi.getRef();
//                semfinest = rbfi.getSemFinEst();
//                escest = rbfi.getEscEst();
//                potnominal = rbfi.getPotNominalTABase();
//                dispest = rbfi.getDispEst();
//            }
//
//
//
//            for (int ic = 0; ic < iniPot.size(); ic++) {
//                ImportFijo rbfi = (ImportFijo) rb;
//                texto = "* Importacion : " + nombre + "\r\n";
//                texto += "$FIJO" + "\r\n";
//                texto += "NOMPAL=" + nombre + "\r\n";
//                texto += "TIPO=" + tipo + "\r\n";
//                texto += "REF=" + "'" + ref + "'" + "\r\n";
//                iniPer = iniPot.get(ic);
//                if (ic < iniPot.size() - 1) {
//                    finPer = iniPot.get(ic + 1);
//                } else {
//                    finPer = calFinEst;
//                }
//                texto += textoIniFin(iniPer, finPer);
//                texto += "NEST=" + rbfi.getNest();
//                texto += "SEMFINEST= ";
//                for (int j = 0; j < semfinest.size(); j++) {
//                    texto += semfinest.get(j) + ",";
//                }
//                texto += "\r\n";
//                texto += "ESCEST= ";
//                double val = 0;
//                for (int j = 0; j < rbfi.getEscEst().size(); j++) {
//                    if (rbfi.isRefUnitario()) {
//                        val = potencias.get(ic);
//                    } else {
//                        val = potencias.get(ic) / rbfi.getPotNominalTABase();
//                    }
//                    texto += rbfi.getEscEst().get(j) + ",";
//                }
//                /**
//                 * Las potencias son proporcionales a la potencia nominal con base 1
//                 * si el recurso esReUnitario y con base potNominal del RecursoBase de
//                 * lo contrario.
//                 */
//                texto += "\r\n";
//                texto += "DISPEST= ";
//                for (int j = 0; j < rbfi.getDispEst().size(); j++) {
//                    texto += rbfi.getDispEst().get(j) + ",";
//                }
//                texto += "\r\n";
//                texto += "$END";
//                texto += "\r\n";
//                texto += "\r\n";
//            }
        } else {
            throw new XcargaDatos("Un RecursoBase no es de ningún tipo de biomasa existente");
        }
        return texto;
    }    

    /**
     * Devuelve un texto para imprimir el conjunto anini, anfin, semini, semfin
     * @param fechaIni fecha inicial
     * @param fechaFin fecha final
     * @return texto incluso un salto de línea final.
     */
    public static String textoIniFin(Calendar fechaIni, Calendar fechaFin) {
        String texto;
        texto = "ANINI=" + fechaIni.get(Calendar.YEAR) + "\r\n";
        texto += "ANFIN=" + fechaFin.get(Calendar.YEAR) + "\r\n";
        texto += "SEMINI=" + FCalendar.semDelAnio(fechaIni) + "\r\n";
        texto += "SEMFIN=" + FCalendar.semDelAnio(fechaFin) + "\r\n";
        return texto;
    }

    public enum TipoPrimitivo {

        ENT, // entero
        REAL; // real
    }

    /**
     * Genera un texto con el título, signo de igual y luego los objetos impresos
     * separados por comas.
     */
    public static String textoVec1DString(String titulo, ArrayList<String> objetos) {
        String texto;
        texto = titulo + "=";
        for (Object o : objetos) {
            texto += o + ", ";
        }
        texto += "\r\n";
        return texto;
    }

    /**
     * Genera un texto con el título, signo de igual y luego los objetos impresos
     * separados por comas.
     */
    public static String textoVec1DEntero(String titulo, ArrayList<Integer> objetos) {
        String texto;
        texto = titulo + "=";
        for (Object o : objetos) {
            texto += o + ", ";
        }
        texto += "\r\n";
        return texto;
    }

    /**
     * Genera un texto con el título, signo de igual y luego los objetos impresos
     * separados por comas.
     */
    public static String textoVec1DReal(String titulo, ArrayList<Double> objetos) {
        String texto;
        texto = titulo + "=";
        for (Object o : objetos) {
            texto += o + ", ";
        }
        texto += "\r\n";
        return texto;
    }

    /**
     * Crea el texto de entrada para un RecursoBase eólico a partir de una lista de potencias
     * nominales y la lista de fechas en las que esa potencia nominal comienza a valer.
     *
     * @param rb
     * @param iniPot es la lista de fechas en las que comienzan a valer las potencias nominales
     * de potencias
     * @param potencias es la lista de esas potencias
     * @return texto es el texto con saltos de línea del eólico.
     */
    public static String textoDeEolico(RecursoBase rb, ArrayList<Calendar> iniPot,
            ArrayList<Double> potencias, DatosGeneralesEstudio datGen) throws XcargaDatos {
        TiempoAbsoluto finEstudio = datGen.getFinEstudio();
        Calendar calFinEst = FCalendar.fechaFinDeTa(finEstudio, datGen);
        String texto = "";
        Calendar iniPer, finPer;
        GeneradorEolico rbeo = (GeneradorEolico) rb;
        for (int ic = 0; ic < iniPot.size(); ic++) {

            texto = "* Eólico : " + rbeo.getNombre() + "\r\n";
            texto += "$FIJO" + "\r\n";
            texto += "NOMPAL=" + "'" + rbeo.getNombre() + "'" + "\r\n";
            texto += "TIPO='IMP'" + "\r\n";
            texto += "REF=" + "'" + rbeo.getRef() + "'" + "\r\n";
            iniPer = iniPot.get(ic);
            if (ic < iniPot.size() - 1) {
                finPer = iniPot.get(ic + 1);
            } else {
                finPer = calFinEst;
            }
            texto += textoIniFin(iniPer, finPer);
            texto += "NEST= 1";
            texto += "\r\n";
            texto += "SEMFINEST= 52";
            texto += "\r\n";
            double pot = potencias.get(ic);  
            texto += "ESCEST= " + UtilitariosGenerales.Numeros.redondeaDouble(pot , 1) + ", ";            
            texto += "\r\n";
            texto += "DISPEST= 1.0";
            texto += "\r\n";
            texto += "$END";
            texto += "\r\n";
            texto += "\r\n";

        }
        return texto;
    }

    public static void main(String[] args) throws XcargaDatos, IOException, IOException {


        ImplementadorEDF creadorRes = new ImplementadorEDF();
        Estudio est = new Estudio("Estudio de prueba", 4, creadorRes);
        String directorio = "D:/Java/PruebaJava3";
        String dirCorrida = directorio + "/corridas/corr1";
        est.leerEstudio(directorio, false);


        TiempoAbsoluto ta = new TiempoAbsoluto("2011");
        ArrayList<Object> informacion = new ArrayList<Object>();
        informacion.add(est);
        ResSimul res1 = new ResSimul(ta, 1, 100);
        res1 = creadorRes.creaRes(dirCorrida, ta, est);
        String texto = res1.toString();
        System.out.print(texto);
        String dirResult = directorio + "/corridas/corr1/" + "resCorrida.xlt";
        DirectoriosYArchivos.grabaTexto(dirResult, texto);


        DatosGeneralesEstudio datGen = est.getDatosGenerales();

        ArrayList<Calendar> enSe = new ArrayList<Calendar>();
        ArrayList<Calendar> fuSe = new ArrayList<Calendar>();
        Calendar en1 = Calendar.getInstance();
        Calendar en2 = Calendar.getInstance();
        en1.set(2010, Calendar.JANUARY, 1);
        en2.set(2011, Calendar.JANUARY, 1);
        Calendar sa1 = Calendar.getInstance();
        Calendar sa2 = Calendar.getInstance();
        sa1.set(2015, Calendar.DECEMBER, 1);
        sa2.set(2015, Calendar.DECEMBER, 31);
        enSe.add(en1);
        enSe.add(en2);
        fuSe.add(sa1);
        fuSe.add(sa2);

        RecursoBase rbTG120 = est.getConjRecB().getUnRecB("TG120");
        double potNominal = 130.0;
        String texto1 = textoDeGenTer(rbTG120, potNominal, enSe, fuSe);
        System.out.print(texto1);

        ArrayList<Double> potencias = new ArrayList<Double>();
        potencias.add(150.0);
        potencias.add(200.0);
        RecursoBase rbIARSPFO = est.getConjRecB().getUnRecB("IARSPFO");
        String texto2 = textoDeImpoExpo(rbIARSPFO, enSe, potencias, datGen);
        System.out.print(texto2);

        potencias.clear();
        potencias.add(300.0);
        potencias.add(500.0);
        RecursoBase rbEolo250 = est.getConjRecB().getUnRecB("Eolo250");
        String texto3 = textoDeEolico(rbEolo250, enSe, potencias, datGen);
        System.out.print(texto3);


        String archGenter = "D:/Java/PruebaJava3" + "/salidasCreaTexto.prm";
        DirectoriosYArchivos.grabaTexto(archGenter, texto1 + texto2 + texto3);




    }
}
