/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package AlmacenSimulaciones;

import GrafoEstados.PeriodoDeTiempo;
import GrafoEstados.ResSimul;
import dominio.ResSimulDB;
import ManejadorTextos.CreadorTex;
import ManejadorTextos.ParTipoDatoPlantilla;
import ManejadorTextos.PlantillaEnTexto;
import UtilitariosGenerales.DirectoriosYArchivos;
import UtilitariosGenerales.ParString;
import dominio.ConjTiposDeDato;
import dominio.Estudio;
import dominio.TipoDeDatos;
import persistenciaMingo.CargaDefTiposDeDato;
import persistenciaMingo.XcargaDatos;

import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ut469262
 *
 * La clase almacena los datos para la implementación del modelo de operación (MOP) asociado 
 * y tiene los métodos que permiten generar las corridas y los resultados del MOP
 * que deben ser sobreescritos
 *
 *
 */
public class ImplementadorGeneral implements Serializable{

    protected ArrayList<ArrayList<String>> textoBaseComando;
    protected ArrayList<ParTipoDatoPlantilla> plantillas;
    protected ConjTiposDeDato conjTD;
    
    /**
     * Directorio donde se encuentran los datos propios del implementador
     */
    protected String dirDatosImplementador;
    
    /**
     * Directorio donde se almacenarán los datos de simulaciones
     * que ejecute el implementador
     */
    protected String dirCorridas;
    
    /**
     * Directorio donde se almacenan los resultados
     */
    protected String dirResultados;
    
    
    /**
     * Es el directorio raíz debajo del cual se crean los estudios
     */
    protected String dirExpansion;
    

    public ImplementadorGeneral(){
        textoBaseComando = new ArrayList<ArrayList<String>>();
        plantillas = new ArrayList<ParTipoDatoPlantilla>();
        conjTD = new ConjTiposDeDato();
    }

    public ConjTiposDeDato getConjTD() {
        return conjTD;
    }

    public void setConjTD(ConjTiposDeDato conjTD) {
        this.conjTD = conjTD;
    }

    public ArrayList<ParTipoDatoPlantilla> getPlantillas() {
        return plantillas;
    }

    public void setPlantillas(ArrayList<ParTipoDatoPlantilla> plantillas) {
        this.plantillas = plantillas;
    }

    public ArrayList<ArrayList<String>> getTextoBaseComando() {
        return textoBaseComando;
    }

    public void setTextoBaseComando(ArrayList<ArrayList<String>> textoBaseComando) {
        this.textoBaseComando = textoBaseComando;
    }

 

    public String getDirExpansion() {
		return dirExpansion;
	}

	public void setDirExpansion(String dirExpansion) {
		this.dirExpansion = dirExpansion;
	}

	public String getDirDatosImplementador() {
        return dirDatosImplementador;
    }

    public void setDirDatosImplementador(String dirDatosImplementador) {
        this.dirDatosImplementador = dirDatosImplementador;
    }

    public String getDirCorridas() {
        return dirCorridas;
    }

    public void setDirCorridas(String dirCorridas) {
        this.dirCorridas = dirCorridas;
    }

/**
 **************************************************
 *
 * COMIENZAN LOS MÉTODOS QUE DEBEN SOBREESCRIBIRSE
 * IMPLEMENTADOR GENERAL
 *
 **************************************************
 */

    /**
     * El método genera los resultados de una corrida correspondientes a un
     * PeriodoDeTiempo
     *
     * ESTE METODO DEBE SER SOBREESCRITO PARA LAS CLASES QUE HEREDAN DE ESTA
     * PARA CADA IMPLEMENTACION CONCRETA PARA UN MODELO DE OPERACION
     *
     * @param dirCorrida es el path del directorio donde están los resultados
     * de la corrida
     * @param per es el PeriodoDeTiempo para el que se creará el resultado.
     * @param informacion es la información empaquetada que puedan precisar
     * los métodos de las clases que heredan de esta.
     * @return es el ResSimulPaso del paso respectivo.
     */
    public ResSimul creaRes(String dirCorrida, PeriodoDeTiempo per, Estudio est) throws XcargaDatos{
        ResSimul res = new ResSimul();
        return res;
    }
    
    public ResSimulDB creaResDB(String dirCorrida, PeriodoDeTiempo per, Estudio est) throws XcargaDatos{ 
        ResSimulDB rsDB = new ResSimulDB(1,1);
        return rsDB;
    }   

    /**
     * Realiza las inicializaciones que requiera cada implementación particular
     * Los métodos que sobreescriban a este le dan contenido.
     */
    public void inicializaParticular(){

    }



    /**
     * El método crea los datos y archivo de comandos de una corrida corespondientes
     * a un PeríodoDeTiempo.
     *
     * ESTE METODO DEBE SER SOBREESCRITO PARA LAS CLASES QUE HEREDAN DE ESTA
     * PARA CADA IMPLEMENTACION CONCRETA PARA UN MODELO DE OPERACION
     *
     * @param dirDatosPadre es el directorio donde se encuentran los datos
     * @param nombreCorr es el nombre de la corrida que aparecerá en el archivo de comando .bat
     * @param dirCorrida es el path del directorio donde se generan
     * los datos y el archivo de comandos de la corrida
     * @param per es el PeriodoDeTiempo que se simulará.
     * @param informacion es la información empaquetada que puedan precisar
     * los métodos de las clases que heredan de esta.
     * 
     * @return en la implementaci�n MOP devuelve ruta de entrada y salida de la corrida
     * en la implmentaci�n EDF es irrelevante
     */
     public ParString creaDatosYComandos(String nombreCorr, IdentSimulPaso idCorr,
             String dirCorrida, ArrayList<Object> informacion) throws XcargaDatos{
    	 return new ParString("","");

     }
     
     
     
 
     /**
      * ESTE METODO DEBE SER SOBREESCRITO PARA LAS CLASES QUE HEREDAN DE ESTA
      * PARA CADA IMPLEMENTACION CONCRETA PARA UN MODELO DE OPERACION
      * 
      * ESTE MÉTODO TIENE LA FIRMA ADECUADA PARA EL PARALELIZADOR DEL MOP
      *
      * @param ordinalPaquete  número de paquete de corridas empezando en cero
      * @param ordinalCorrida  número de corrida dentro del paquete empezando en cero
      * @param identS   identificador de la corrida (parque y valores de los tipos de datos)
      * 
      */
      public ParString creaDatosYComandosMOP(String dirPaquete, String dirSalPaquete, int ordinalPaquete, int ordinalCorr, IdentSimulPaso identS, ArrayList<Object> informacion) throws XcargaDatos{
     	 return new ParString("","");
      }  
      
      /**
       * ESTE METODO DEBE SER SOBREESCRITO PARA LAS CLASES QUE HEREDAN DE ESTA
       * PARA CADA IMPLEMENTACION CONCRETA PARA UN MODELO DE OPERACION
       */
      public void creaDatosDeCorridas() {
    	  
      }
      
     
      /**
       * ESTE METODO DEBE SER SOBREESCRITO PARA LAS CLASES QUE HEREDAN DE ESTA
       * PARA CADA IMPLEMENTACION CONCRETA PARA UN MODELO DE OPERACION
       */
      public void lanzaEjecucionDePaquete() {
    	  
      }
      
      public void creaXmlParalelizador(int ordinalPaquete, String jarParalelizador, String xmlInvocaParalelizador, int cantMaq) {
    	  
      }

/**
 **************************************************
 * FINALIZAN LOS MÉTODOS QUE DEBEN SER SOBREESCRITOS
 * IMPLEMENTADOR GENERAL
 **************************************************
 */

     
     
     /**
      * Inicializa el implementador leyendo los TipoDeDatos, el archivo de texto base
      * de comandos .bat, y las plantillas para sustituir en el texto base.
      *
      * @param dir_Datos_Implementación es el directorio donde se leen los archivos
      * de datos para el implementador
      * @param dir_Expansion es el directorio más arriba, que tiene debajo Estudio, resultados, corridas...
      * @param dir_Corridas es el directorio "corridas"
      */
     public void inicializaImplementador(String dir_Datos_Implementacion,
             String dir_Expansion, String dir_Corridas) throws XcargaDatos{

        dirDatosImplementador = dir_Datos_Implementacion;
        dirCorridas = dir_Corridas;
        dirExpansion = dir_Expansion;
        dirResultados = dir_Expansion + "/resultados";
        
        // Verifica si existen los directorios debajo de dir_Expansion
        if(!DirectoriosYArchivos.existeArchivo(dirCorridas)){
            JOptionPane.showMessageDialog(null, "ATENCION: El directorio de corridas no existe, debe crearlo" +
                    " y luego reiniciar la ejecución cerrando este diálodo");
        }
        
        if(!DirectoriosYArchivos.existeArchivo(dirDatosImplementador)){
            JOptionPane.showMessageDialog(null, "ATENCION: El directorio de datos de implementación no existe, debe crearlo" +
                    " y luego reiniciar la ejecución cerrando este diálodo");
        }
        if(!DirectoriosYArchivos.existeArchivo(dirResultados)){
            JOptionPane.showMessageDialog(null, "ATENCION: El directorio de resultados no existe, debe crearlo" +
                    " y luego reiniciar la ejecución cerrando este diálodo");
        }        
        
        /**
         * ------------------------------------------------------
         * Lee los tipos de dato de la implementación
         * ------------------------------------------------------
         */
        String dirTiposDeDatos = dir_Datos_Implementacion + "/" + "defTiposDeDatos.txt";
        CargaDefTiposDeDato.cargarTiposDato(dirTiposDeDatos, conjTD);

        
        /**
         * ---------------------------------------------------
         * Lee el archivo de configuración
         * ---------------------------------------------------
         */
        
        
        /**
         * ----------------------------------------------------
         * Elimina si existe el archivo resumen de simulaciones
         * ----------------------------------------------------
         */
        String archResumenCorr = dir_Corridas + "/" +  "resumenCorridas.txt";        
        try{
            DirectoriosYArchivos.eliminaArchivo(archResumenCorr);

        }catch (IllegalArgumentException e){
            // no se hace nada porque es posible que el archivo no exista cuando se empieza el estudio.
        }
     }


    /**
     * Carga las plantillas en texto que se emplean para fabricar archivos de comandos,
     * asociadas a cada uno de los tipos en su orden respectivo.
     * @param dirArchivo es el path del archivo de plantillas.
     */
    public ArrayList<ParTipoDatoPlantilla> leePlantillasDeTipos(String dirArchivo) throws XcargaDatos{
        ArrayList<PlantillaEnTexto> plantAux = CreadorTex.leePlantillas(dirArchivo);
        ArrayList<ParTipoDatoPlantilla> listaPTDP = new ArrayList<ParTipoDatoPlantilla>();
        ArrayList<TipoDeDatos> tipos = conjTD.getTipos();
        for(TipoDeDatos tip: tipos){
            int iplan = 0;
            boolean encontro = false;
            do{
               if(tip.getNombre().equalsIgnoreCase(plantAux.get(iplan).getNombre()) ){
                   encontro = true;
                   ParTipoDatoPlantilla parTP = new ParTipoDatoPlantilla(tip, plantAux.get(iplan) );
                   listaPTDP.add(parTP);
               }
               iplan++;
            }while(iplan < plantAux.size() && !encontro);
        }
        return listaPTDP;
    }
}
