/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AlmacenSimulaciones;

import dominio.Estudio;
import dominio.ParTipoDatoValor;
import dominio.Parque;
import dominio.TiempoAbsoluto;
import persistenciaMingo.XcargaDatos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 *
 * Es el mínimo conjunto de datos que define una simulación en el Estudio
 * corriente
 */
public class IdentSimulPaso implements Serializable {

     private TiempoAbsoluto ta;
     private Parque parque;
     /**
      * Es un parque que debe contener sólo recursos y todos de tipo OPE (operativos)
      */
     private ArrayList<ParTipoDatoValor> valoresTiposDatos;
     /**
      * Es la lista de valores simbólicos asociados uno a uno a cada TipoDeDatos
      * y en el mismo orden en que aparecen los TipoDeDatos en el ConjTiposDeDato
      * del estudio.
      */
     private String nombreSimul;

     /**
      * Es el String que determina el directorio de la corrida empleada para cargar los
      * resultados de esta simulación
      */
     public IdentSimulPaso(Parque parque, TiempoAbsoluto ta, ArrayList<ParTipoDatoValor> valoresTiposDatos) {
          this.ta = ta;
          this.parque = parque;
          this.valoresTiposDatos = valoresTiposDatos;
     }
     
 	/**
 	 * Devuelve el valor simb�lico de un tipo de datos o null si no existe un tipo de 
 	 * datos con ese nombre
 	 * @param nombreTD es el nombre de un tipo de datos
 	 */
 	public String valorSimbolicoDeTipoDatos(String nombreTD){
 		for (ParTipoDatoValor pt: valoresTiposDatos){
 			if(pt.getTipoDatos().getNombre().equalsIgnoreCase(nombreTD)) return pt.getValor();
 		}
 		return null;
 	}



     public ArrayList<ParTipoDatoValor> getValoresTiposDatos() {
          return valoresTiposDatos;
     }

     public void setValoresTiposDatos(ArrayList<ParTipoDatoValor> valoresTiposDatos) {
          this.valoresTiposDatos = valoresTiposDatos;
     }

     public String getNombreSimul() {
          return nombreSimul;
     }

     public void setNombreSimul(String nombreSimul) {
          this.nombreSimul = nombreSimul;
     }

     public Parque getParque() {
          return parque;
     }

     public void setParque(Parque parque) {
          this.parque = parque;
     }

     public TiempoAbsoluto getTa() {
          return ta;
     }

     public void setTa(TiempoAbsoluto ta) {
          this.ta = ta;
     }

     @Override
     public boolean equals(Object obj) {
          if (obj == null) {
               return false;
          }
          if (getClass() != obj.getClass()) {
               return false;
          }
          final IdentSimulPaso other = (IdentSimulPaso) obj;
          if (this.ta != other.ta && (this.ta == null || !this.ta.equals(other.ta))) {
               return false;
          }
          if (this.parque != other.parque && (this.parque == null || !this.parque.equals(other.parque))) {
               return false;
          }
          if (this.valoresTiposDatos != other.valoresTiposDatos && (this.valoresTiposDatos == null || !this.valoresTiposDatos.equals(other.valoresTiposDatos))) {
               return false;
          }
          return true;
     }

     @Override
     public int hashCode() {
          int hash = 5;
          hash = 67 * hash + (this.ta != null ? this.ta.hashCode() : 0);
          hash = 67 * hash + (this.parque != null ? this.parque.hashCode() : 0);
          hash = 67 * hash + (this.valoresTiposDatos != null ? this.valoresTiposDatos.hashCode() : 0);
          return hash;
     }

     public String toStringCorto(ArrayList<Object> informacion, ArrayList<Boolean> informacionImp) throws XcargaDatos {
          Estudio est = (Estudio) informacion.get(0);
          String texto = "\r\n" + "Identificador de corridas" + "\r\n";
          texto += "Tiempo Absoluto :" + ta.toStringCorto();
          texto += "Parque :" + parque.toStringCorto(informacion, informacionImp);
          texto += "Valores de TipoDeDatos :" + "\r\t";
          for (ParTipoDatoValor ptd : valoresTiposDatos) {
               texto += ptd.getValor() + "\r\t";
          }
          return texto;
     }
}
