/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TiposEnum;

import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class ExistEleg {

    /**
     * Se aplica a los RecursoBase o TransforBase
     * 
     * EXIST: RecursoBase o TransforBase que existe en la expansión por defecto
     * y no puede ser elegido como decisión de inversión.
     *
     * ELEG: RecursoBase o TransforBase que no existe al comienzo del paso t=1 y existirá sólo si
     * aparece como resultado de una decisión de inversión.
     *
     * RESULT RecursoBase que sólo existe como resultado de un CambioAleatorio o Tranformacion.
     * Los RecursoBase resultantes de una Transformacion pueden también ser ELEG.
     *
     * TRANS: sólo puede aparecer como recurso transitorio de una transformación
     * Las transformaciones pueden tener recursos destino final ELEG o RESULT.
     * 
     */
    public static final String EXIST = "EXIST"; // RecursoBase o TransforBase existente no elegible en la optimización
    public static final String ELEG = "ELEG";   // Recurso elegible en la optimización
    public static final String TRANS = "TRANS"; // Recurso transitorio de una transformacion
    public static final String RESULT = "RESULT"; // Recurso resultante de una transformacion o cambio aleatorio.

    public static String valueOf(String s) throws XcargaDatos {
        if (s.equalsIgnoreCase(EXIST)) {
            return "EXIST";
        }
        if (s.equalsIgnoreCase(ELEG)) {
            return "ELEG";
        }
        if (s.equalsIgnoreCase(TRANS)) {
            return "TRANS";
        }
        if (s.equalsIgnoreCase(RESULT)) {
            return "RESULT";
        }        
        throw new XcargaDatos("Error en la lectura de un valor de EstadosRec");
    }
}
