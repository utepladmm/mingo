/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package TiposEnum;

import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public abstract class EstadosRec {

    public static final String OPE = "OPE";
    public static final String CON = "CON";
    public static final String DEM = "DEM";
    public static final String FALL = "FALL";
    public static final String TERMIN = "TERMIN";
    public static final String TODOS = "TODOS";
    
    /**
     * OPE operativo
     * CON en construcción
     * DEM construcción demorada
     * FALL construcción fallida: una transformación se completó y no existían
     * en ese momento los RecursoBase origen
     * TERMIN terminada, una transormación se terminó y se incorporó como bloque
     * para representar los costos.
     * 
     */

    public static String valueOf(String s) throws XcargaDatos {
      if(s.equalsIgnoreCase(OPE)) return "OPE";
      if(s.equalsIgnoreCase(CON)) return "CON";
      if(s.equalsIgnoreCase(FALL)) return "FALL";
      if(s.equalsIgnoreCase(TERMIN)) return "TERMIN";
      if(s.equalsIgnoreCase(TODOS)) return "TODOS";
      throw new XcargaDatos("Error en la lectura de un valor de EstadosRec");
    }
}
