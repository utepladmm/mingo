/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package TiposEnum;

import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class SubTipo1 {

// Subtipo1 asociados a GEN
//        TERM, // Generador térmico
//        HID, // Hidráulico
//        EOLO, // Eólico
//        IMP, // Importacion
// Subtipo1 asociados a CARG//        EXP;

    public static final String TERM = "TERM";
    public static final String CCOMB = "CCOMB";
    public static final String HID = "HID";
    public static final String EOLO = "EOLO";
    public static final String IMP = "IMP";   
    public static final String EXP = "EXP";
    public static final String BIO = "BIO";
    public static final String FOTO = "FOTO";
    public static final String ACUM = "ACUM";
    public static final String[] lista; 
//    public static String valueOf(String s) throws XcargaDatos {
//      if(s.equalsIgnoreCase(TERM)) return "TERM";
//      if(s.equalsIgnoreCase(TERM)) return "TERM";
//      if(s.equalsIgnoreCase(HID)) return "HID";
//      if(s.equalsIgnoreCase(EOLO)) return "EOLO";
//      if(s.equalsIgnoreCase(IMP)) return "IMP";
//      if(s.equalsIgnoreCase(EXP)) return "EXP";
//      throw new XcargaDatos("Error en la lectura de un valor de EstadosRec");
//    }
    
    static{
        lista = new String[9];
        lista[0]=HID;
        lista[1]=TERM;
        lista[2]=CCOMB;
        lista[3]=EOLO;
        lista[4]=BIO;
        lista[5]=IMP;
        lista[6]=EXP;  
        lista[7]=FOTO;
        lista[8]=ACUM;
    }

    public static String valueOf(String subt1) throws XcargaDatos {
        if (subt1.equalsIgnoreCase("termico") || subt1.equalsIgnoreCase("térmico")  ||
                subt1.equalsIgnoreCase("TERM")) {
            return SubTipo1.TERM;
        } else if (subt1.equalsIgnoreCase("cicloCombinado") || subt1.equalsIgnoreCase("CCOMB")  ||
                subt1.equalsIgnoreCase("cicloComb") ) {
             return SubTipo1.CCOMB;            
        } else if (subt1.equalsIgnoreCase("hidraulico") || subt1.equalsIgnoreCase("hidr�ulico")  ||
               subt1.equalsIgnoreCase("HID") ) {
            return SubTipo1.HID;
        } else if (subt1.equalsIgnoreCase("eolico") || subt1.equalsIgnoreCase("e�lico") ||
                subt1.equalsIgnoreCase("EOLO"))  {
            return SubTipo1.EOLO;
        } else if (subt1.equalsIgnoreCase("importacion") || subt1.equalsIgnoreCase("importaci�n") ||
                subt1.equalsIgnoreCase("IMP")) {
            return SubTipo1.IMP;
        } else if (subt1.equalsIgnoreCase("exportacion") || subt1.equalsIgnoreCase("exportaci�n") ||
                subt1.equalsIgnoreCase("EXP")) {
            return SubTipo1.EXP;
        } else if (subt1.equalsIgnoreCase("biomasa") || subt1.equalsIgnoreCase("bio") ||
                subt1.equalsIgnoreCase("BIO")) {
            return SubTipo1.BIO; 
        } else if (subt1.equalsIgnoreCase("fotovoltaico") || subt1.equalsIgnoreCase("solar") ||
        		subt1.equalsIgnoreCase("FOTO")) {
            return SubTipo1.FOTO;
        } else if (subt1.equalsIgnoreCase("acumulador") || subt1.equalsIgnoreCase("acum") ||
        		subt1.equalsIgnoreCase("ACUM")) {
            return SubTipo1.ACUM;                         
        } else {
            throw new XcargaDatos("Aparece en un dato un subtipo1 no conocido: " + subt1);
        }
    }
    


}
