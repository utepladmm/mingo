/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GrafoEstados;

import java.io.Serializable;

/**
 *
 * @author ut469262
 */
public class ObjDecision implements Serializable{
	/**
	 * Crea pares objetivo - decisión óptima con ese objetivo
	 */
	private Objetivo obj;
	private Portafolio dec;

	public ObjDecision(Objetivo obj, Portafolio dec) {
		this.obj = obj;
		this.dec = dec;
	}


	public Portafolio getDec() {
		return dec;
	}

	public void setDec(Portafolio dec) {
		this.dec = dec;
	}

	public Objetivo getObj() {
		return obj;
	}

	public void setObj(Objetivo obj) {
		this.obj = obj;
	}

}
