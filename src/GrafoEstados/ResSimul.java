/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import dominio.TiempoAbsoluto;
import persistenciaMingo.XcargaDatos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 * Contiene los resultados de una simulación para un conjunto de numerarios
 * y de crónicas aleatorias en un período de tiempo.
 */
public class ResSimul implements Serializable {

    private PeriodoDeTiempo per;
    /**
     * Es un texto que identifica de donde se obtuvieron los resultados. En la implementación inicial
     * es un directorio donde están las simulaciones.
     */
    private String nombreSimul;
    /**
     * El primer índice es el de numerario y el segundo es el de crónica en el modelo
     * de operación usado.
     */
    private double[][] result;
    /**
     * probs son las probabilidades de las crónicas.
     */
    private double[] probs; 
    public ResSimul() {
    }

    public ResSimul(PeriodoDeTiempo per, int cantNumerarios, int cantCronicas) {
        this.per = per;
        result = new double[cantNumerarios][cantCronicas];
        probs = new double[cantCronicas];
    }

    public PeriodoDeTiempo getPer() {
        return per;
    }

    public void setPer(PeriodoDeTiempo per) {
        this.per = per;
    }

    public String getNombreSimul() {
        return nombreSimul;
    }

    public void setNombreSimul(String nombreSimul) {
        this.nombreSimul = nombreSimul;
    }

    public double[][] getResult() {
        return result;
    }

    public void setResult(double[][] result) {
        this.result = result;
    }

    public double[] getProbs() {
        return probs;
    }

    public void setProbs(double[] probs) {
        this.probs = probs;
    }

    /**
     * Calcula el valor esperado de un ResSimul dadas unas ponderaciones de
     * los numerarios y empleando las probabilidades del propio ResSimul
     */
    public double valorEsperado(ArrayList<Double> pond) throws XcargaDatos{
        double valEsp = 0.0;
        if(! (pond.size()==result.length) ) throw new XcargaDatos("Se pidió el valor esperado" +
                "de un resultado con un número equivocado de ponderadores de numerarios");
        for(int inum =0; inum <pond.size(); inum ++ ){
            for(int icron = 0; icron < result[inum].length; icron ++){
                valEsp += result[inum][icron]*pond.get(inum)*probs[icron];
            }
        }
        return valEsp;
    }



    /**
     * Devuelve un nuevo resultado que es la suma de this más un vector, con un valor por numerario,
     * se suma el vector a los valores de cada crónica.
     * Al hallar la suma no se afecta this.
     * @param vecConstante
     * @return
     */
    public ResSimul sumaVecConstante(ArrayList<Double> vecConstante) {
        ResSimul suma = this.copiaResSimul();
        for (int icron = 0; icron < result[0].length; icron++) {
            for (int inum = 0; inum < result.length; inum++) {
                suma.getResult()[inum][icron] += vecConstante.get(inum);
            }
        }
        return suma;
    }

    /**
     * Crea una copia modificable sin alterar el original
     * @return
     */
    public ResSimul copiaResSimul() {
        PeriodoDeTiempo perCopia;
        if (per instanceof TiempoAbsoluto) {
            perCopia = ((TiempoAbsoluto) per).copiaTiempoAbsoluto();
        } else {
            perCopia = per.copiaPerDeTiempo();
        }
        int cantNum = result.length;
        int cantCron = result[0].length;
        ResSimul copia = new ResSimul(perCopia, cantNum, cantCron);
        copia.setPer(perCopia);
        copia.setNombreSimul(nombreSimul);
        for (int icron = 0; icron < cantCron; icron++) {
            for (int inum = 0; inum < cantNum; inum++) {
                copia.getResult()[inum][icron] = result[inum][icron];
            }
            copia.getProbs()[icron] = probs[icron];
        }
        return copia;
    }
    
    @Override
    public String toString() {
        String texto = "Resultado de simulación ";
        texto += "paso " + per.toStringCorto() + "\n";
        texto += "ATENCION: Cada columna es un numerario y cada fila crónica" + "\n";
        /**
         * En result el primer indice es numerario y el segundo es crónica
         */
        for (int icron = 0; icron < result[0].length; icron++) {
            texto += "prob =" + probs[icron] + "\t";
            for (int inum = 0; inum < result.length; inum++) {
                texto += result[inum][icron] + "\t";
            }
            texto += "\r\n";
        }
        return texto;
    }
}
