/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import java.io.Serializable;
import java.util.ArrayList;

import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import naturaleza3.NodoN;
import persistenciaMingo.XcargaDatos;

import static java.lang.Math.*;

/**
 *
 * @author ut469262
 */
public class AplicadorProgramacionDinamica implements Serializable {

    /**
     * Supone que los costos de cada paso de tiempo se cargan en el instante medio
     * del paso de tiempo.
     * Se recurre a los pasos de cada etapa y el paso representativo de cada etapa.
     * LA ULTIMA ETAPA DEBE TENER SÃ“LO UN PASO
     *
     */
    private GrafoE grafoE;
    private Object baseDeDatosResultados;

    /**
     * Es un objeto que contiene la base de datos que permite cargar los costos
     * de los nodos del grafoE.
     * En la implementaciÃ³n inicial el objeto es una BolsaDeSimulaciones
     */
    public AplicadorProgramacionDinamica(GrafoE grafoE, Object baseDeDatosResultados) {
        this.grafoE = grafoE;
    }

    public GrafoE getGrafoE() {
        return grafoE;
    }

    public void setGrafoE(GrafoE grafoE) {
        this.grafoE = grafoE;
    }

    public Object getBaseDeDatosResultados() {
        return baseDeDatosResultados;
    }

    public void setBaseDeDatosResultados(Object baseDeDatosResultados) {
        this.baseDeDatosResultados = baseDeDatosResultados;
    }

    public enum TipoNoObs {

        /**
         * Tipo Indep quiere decir que las variables no observadas en t
         * son independientes de las de t+k para todo k, lo que simplifica
         * el tratamiento de la optimizaciÃ³n
         */
        Indep,
        NoIndep
    }

    /**
     * Ejecuta la programaciÃ³n dinÃ¡mica sobre grafoE con el objetivo obj,
     * Si boolean = true imprime resultados detallados por nodo de las iteraciones
     *
     * @param obj el objetivo de la programaciÃ³n dinÃ¡mica
     * @param tipoNO ooo
     * @param impDet lll
     */
    public void aplicaPD(Objetivo obj, Object baseDeDatosResult, String archImp, TipoNoObs tipoNO, boolean impDet) throws XcargaDatos {
        double cosmin;
        double costod;
        double probcond;
        /**
         * probcond: probabilidad condicional de un nodoE en el cd
         */
        double cosJUnNodo;
        ArrayList<Double> cosJNodos;
        ArrayList<Double> cosJMinDeUnaDec = null;
        ArrayList<Object> informacion = grafoE.getInformacion();
        Estudio est = (Estudio)informacion.get(0);
        boolean anualidades = est.getDatosGenerales().getTipoCargosFijos().equalsIgnoreCase(DatosGeneralesEstudio.ANUALIDADES);
        /**
         * cosJMinDeUnaDec: costo actualizado al inicio del paso en un nodo
         * para la decisiÃ³n que en la iteraciÃ³n estÃ¡ dando el mÃ­nimo costo
         */
        grafoE.cargaCostosPaso(obj, baseDeDatosResult);
        grafoE.cargaOtrosResultados(obj, baseDeDatosResult);
        double valorFin;
        double costoip;
        double costoJ;
        double factor = grafoE.getFactorDescPaso();
        int cantEtapas = grafoE.getCantEtapas();
        String texto = "";
        if (impDet) {
            texto += "************************************************" + "\r\n";
            texto += "COMIENZAN RESULTADOS DE LA PROGRAMACIÃ“N DINÃ�MICA  " + "\r\n";
            texto += "************************************************" + "\r\n";
            texto += "INICIO ETAPA " + String.valueOf(cantEtapas) + "\r\n";
        }
        /**
         * Procesa inicialmente los nodos de la Ãºltima etapa que no tienen
         * sucesores ni decisiÃ³n Ã³ptima asociada
         */
        for (Object cdOb : grafoE.getBolsaCD().cDsUnaEtapa(cantEtapas)) {
            ConjDeDecision cd = (ConjDeDecision) cdOb;
            double vBellCD = 0.0;
            double costoPasoCDEsp = 0.0;
            for (NodoE nodoE : cd.getNodos()) {
                probcond = nodoE.getNodoN().getProbCondDadoCI();
                valorFin = 0.0;
                if(anualidades) valorFin = nodoE.getValorFinJuego();
                /**
                 * El valor de fin de juego valorFin es el valor de Bellmann al final de la Ãºltima etapa
                 */
                costoip = nodoE.getCostoPaso();
                costoJ = costoip * sqrt(factor) + valorFin * factor;
                nodoE.setValorVBIniPaso(costoJ);
                vBellCD += costoJ * probcond;
                costoPasoCDEsp += costoip * probcond;
                if (impDet) {
                    texto += nodoE.resultPDDelNodo2(null, informacion) + "\r\n";
                }
            }
            cd.agregaDecOptima(null, obj, vBellCD, costoPasoCDEsp);
        }


        /**
         * Procesa ahora los nodos de todas las etapas restantes,
         * que son nodos que tienen sucesores
         * comenzando desde la penÃºltima etapa hasta la primera
         */
        for (int e = grafoE.getCantEtapas() - 1; e >= 1; e--) {
            if (impDet) {
                texto += "INICIO ETAPA " + String.valueOf(e) + "\r\n";
            }
            System.out.println("Inicia cÃ¡lculo de VB etapa " + e);
            for (Object cdOb : grafoE.getBolsaCD().cDsUnaEtapa(e)) {
                ConjDeDecision cd = (ConjDeDecision) cdOb;
                if (!cd.isSinSuc()) {
                    if (impDet) {
                        texto += "\r\n" + "Inicio CD ordinalCD " + cd.getOrdinalCD() + "\r\n";
                    }
                    System.out.println("Inicio CD ordinalCD " + cd.getOrdinalCD());

                    Portafolio decmin = new Portafolio();
                    decmin = null;
                    cosmin = 1.0E20;
                    double cosPasoMin = 0.0;
                    cosJMinDeUnaDec = new ArrayList<Double>();
                    /**
                     * Si alguna decisiÃ³n conduce siempre a CDs con sucesores el Cd no es sin sucesores
                     */
                    boolean hayDecConFuturo = false;
                    for (Portafolio d : cd.getDecisiones()) {
                        boolean dSirve = true;
                        double costoPasoCDEsp = 0.0;
                        costod = 0.0;
                        cosJNodos = new ArrayList<Double>();
                        //					if (tipoNO == TipoNoObs.NoIndep){
                        /**
                         * Cuando las variables no observadas de cada perÃ­odo no son
                         * independientes de las de los restantes perÃ­odos. Hay que
                         * considerar las probabilidades condicionales de cada cd
                         */
                        for (NodoE nodoE : cd.getNodos()) {
                            /**
                             * La probabilidad condicional de nodoE dado que se estÃ¡
                             * en cd es la probabilidad del nodoN de nodoE dado se
                             * se estÃ¡ en el conjunto de informaciÃ³n de cd
                             */
                            probcond = nodoE.getNodoN().getProbCondDadoCI();
                            cosJUnNodo = 0.0;
                            try{
                                cosJUnNodo = costoJInicioPaso(grafoE,
                                        nodoE, d, obj, informacion);
                            }catch (XCdSinSuc xc){
                                /**
                                 *  la decisiÃ³n d a partir de nodoE conduce a un CD sin sucesores
                                 *  para algÃºn nodoN 
                                 */ 
                                dSirve = false;
                                break;                                
                            }
                            costod += probcond * cosJUnNodo;
                            costoip = nodoE.getCostoPaso();
                            costoPasoCDEsp += costoip * probcond;
                            cosJNodos.add(cosJUnNodo);
                        }

                        //					}else{
                        /**
                         * Cuando las variables no observadas de cada perÃ­odo son
                         * independientes de las de los restantes perÃ­odos, la decisiÃ³n
                         * Ã³ptima es la misma para todos los nodos del CD
                         */
                        //					}
                        /**
                         * Si la decisiÃ³n reduce el costo respecto a la mejor ya
                         * procesada se pasa a considerar como la mejor
                         */
                        if(dSirve){
                            if (costod < cosmin) {
                                cosmin = costod;
                                cosPasoMin = costoPasoCDEsp;
                                decmin = d;
                                cosJMinDeUnaDec.clear();
                                cosJMinDeUnaDec.addAll(cosJNodos);
                                cosJNodos.clear();
                            }
                            hayDecConFuturo = true;                            
                        }
                    }

                    //                    ObjDecision decOpt = new ObjDecision(obj, decmin);
                    if(hayDecConFuturo){
                        cd.agregaDecOptima(decmin, obj, cosmin, cosPasoMin);
                        /**
                         * Carga los valores valorVBIniPaso de todos los nodos del cd
                         * y del propio cd
                         */
                        //                  aca tengo ya el cosmin del cd para cargarlo en el cd
                        int ind = 0;
                        
                        for (NodoE nodoE : cd.getNodos()) {
                        	if(!anualidades){
                        		ArrayList<Double> invYCfij = decmin.totalInvYCFijosActualizados(obj, informacion, nodoE);
                        		nodoE.agregaInvYCFijosActualizados(invYCfij);
                        	}
                            nodoE.setValorVBIniPaso(cosJMinDeUnaDec.get(ind));
                            if (impDet) {
                                texto += nodoE.resultPDDelNodo2(obj, informacion) + "\r\n";
                            }
                            ind++;
                        }
                    }else{
                        cd.setSinSuc(true);
                    }
                }
            }
            if (impDet) {
                texto += "\r\n";
            }
        }
        UtilitariosGenerales.DirectoriosYArchivos.grabaTexto(archImp, texto);
    }

    /**
     * METODO costoJInicioPaso
     * Para los nodos que no estÃ¡n en la Ãºltima etapa,
     * calcula el costo total esperado J(nodoE),
     * (actualizado al inicio del paso de nodoE),
     * suma de costos corrientes de la etapa y costos futuros, si la
     * decisiÃ³n tomada en nodoE es d.
     * Es un valor esperado en los nodos sucesores del NodoN asociado a NodoE
     *
     * Se suman costos de pasos entre el paso representativo de la etapa e
     * inclusive y el paso anterior al representativo de la etapa e+1, a
     * menos que e sea la Ãºltima etapa y allÃ­ se incluye solo el paso
     * representativo (que es el Ãºnico de esa etapa).
     * 
     * Si con la decisiÃ³n d algÃºn nodo sucesor pertenece a un cd sin sucesores se
     * lanza la excepciÃ³n XCdSinSuc
     */
    public double costoJInicioPaso(GrafoE grafoE, NodoE nodoE,
            Portafolio d, Objetivo obj, ArrayList<Object> informacion) throws XcargaDatos, XCdSinSuc {
    	Estudio est = (Estudio)informacion.get(0);
    	boolean anualidades = est.getDatosGenerales().getTipoCargosFijos().equalsIgnoreCase("anualidades");
        int e = nodoE.getEtapa();
        int pasoIni, pasoFin;
        int cantEtapas = grafoE.getCantEtapas();
        double costoJ = 0.0;
        double costoip = 0.0;
        NodoN nN = nodoE.getNodoN();  // nodoN de nodoE

        pasoIni = grafoE.getEtapasPasos().get(e)[2];
        // pasoIni es el paso representativo de la etapa e

        double valorFin; // valor al fin del pasoFin
        double factor = grafoE.getFactorDescPaso();
        double probTrans;
        ArrayList<NodoE> nodosESuc = new ArrayList<NodoE>();
        nodosESuc = nodoE.sucesoresConUnaDec(d);
        /**
         * No estamos en la Ãºltima etapa
         * Los costos por paso a sumar van entre pasoIni y pasoFin
         * Como no estamos en la etapa final el nodo tiene sucesores
         */
        pasoFin = grafoE.getEtapasPasos().get(e + 1)[2] - 1;
        for (NodoE nESuc : nodosESuc) {
            if(nESuc.getCD().isSinSuc()){
                throw new XCdSinSuc();
            }
            NodoN nNSuc = nESuc.getNodoN();
            double probUnsuc = nN.probDeUnSucesor(nNSuc);
            double acumUnsuc = 0.0;
            /**
             * ff es el factor aplicado en cada paso que varÃ­a a lo largo
             * de la iteraciÃ³n por pasos
             */
            double ff = sqrt(factor);
            for (int ip = pasoIni; ip <= pasoFin; ip++) {
                if (ip <= grafoE.getEtapasPasos().get(e)[1]) {
                    // el paso es menor o igual al paso final de la etapa e
                    costoip = nodoE.getCostoPaso();
                } else {
                    // el paso es mayor o igual al paso inicial de la etapa e+1
                    costoip = nESuc.getCostoPaso();
                }
                acumUnsuc += costoip * ff;
                ff = factor * ff;
            }
            valorFin = nESuc.getValorVBIniPaso();
            ff = ff / sqrt(factor);
            acumUnsuc += valorFin * ff;
            costoJ += acumUnsuc * probUnsuc;
        }
        if(!anualidades){
        	costoJ += d.calculaCostoTotalDecision(obj, informacion, nodoE);
        }
        return costoJ;
    }
}
