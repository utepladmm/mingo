/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GrafoEstados;

import AlmacenSimulaciones.BolsaDeSimulaciones3;
import java.io.Serializable;
import naturaleza3.NodoN;
import persistenciaMingo.XcargaDatos;

import java.util.ArrayList;
import naturaleza3.ConjNodosN;

/**
 *
 * @author ut469262
 */
public class Portafolio implements Serializable{

    protected int etapa;  // Etapa del estudio en la que vive el portafolio
//    protected PeriodoDeTiempo perTiempo;
//    protected String resumenP;   // Texto resumido que describe el portafolio;
//    protected String nombreSimul; // Simulación que se empleó para calcular el costo del nodo


    public Portafolio(int etapa) {
        this.etapa = etapa;
        
	}

    public Portafolio() {
    }

    public int getEtapa() {
        return etapa;
    }

    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }


//    public String getNombreSimul() {
//        return nombreSimul;
//    }
//
//    public void setNombreSimul(String nombreSimul) {
//        this.nombreSimul = nombreSimul;
//    }



    /**
     * Crea una copia que puede modificarse sin alterar el original
     * @return
     */
    public Portafolio copiaPortafolio(){
        Portafolio copia = new Portafolio(etapa);
//        copia.setNombreSimul(nombreSimul);
        return copia;
    }
        


/**
 * *******************************************************************
 * A CONTINUACIÓN APARECEN LOS MÉTODOS A SOBREESCRIBIR POR LAS CLASES
 * QUE HEREDEN DE PORTAFOLIO
 * *******************************************************************
 */

    /**
     * Genera el portafolio sucesor de uno dado pe que vive en la etapa e
     * en función de
     * - los nodos de la naturaleza n (de la etapa e) y nemas1 (de e+1)
     * - la decisión de tomada en e
     * - informacion es un paquete de cualquier información relevante
     */
	public Portafolio sucesorDeUnPort(Portafolio de, NodoN ne, NodoN nemas1, ArrayList<Object> informacion)
			throws XcargaDatos{
  
		Portafolio pemas1 = new Portafolio();
		return pemas1;
	}



    /**
     * Genera el portafolio sucesor de uno dado pe que vive en la etapa e
     * en función de
     * - los conjuntos determinantes de parque en GrafoN cDPNe (de la etapa e) y cDPNtmas1 (de e+1)
     * - la decisión de tomada en e
     * - informacion es un paquete de cualquier información relevante.
     * El sucesor vive en la etapa e+1.
     */
	public Portafolio sucesorDeUnPort(Portafolio de, ConjNodosN cDPNe, ConjNodosN cDPNemas1, ArrayList<Object> informacion)
			throws XcargaDatos{
 
		Portafolio pemas1 = new Portafolio();
		return pemas1;
	}

    /**
     * Devuelve la lista de decisiones factibles de un portafolio cualquiera
     * del conjunto de decision cDe
     *
     * Decisiones factibles son las que:
     * 1) Cumplen las restricciones de inicio de construcción, es decir
     * las que pueden evaluarse conociendo solo el portafolio inicial y los atributos
     * de la Decision que se toma en la etapa e, sin considerar lo que pasa en la etapa e+1.
     * 2) No son descartadas de antemano por conducir a violar restricciones de parque en
     * e+1. Este descarte es propio de la clase hija y se elabora en el método
     * que sobreescribe este.
     */
    public ArrayList<Portafolio> decisionesDeUnPort(ConjDeDecision cDe,
            ConjRestGrafoE conjRInicio, ConjRestGrafoE conjRparque, 
            ArrayList<Object> informacion) throws XcargaDatos {

		ArrayList<Portafolio> listaDec = new ArrayList<Portafolio>();
		return listaDec;
	}



    /**
     * @param conjRestSigParque es el conjunto de restricciones significativas de Portafolio
     * @parm simple si es true limita la cantidad de portafolios contiguos 
     * 
     * Devuelve los portafolios contiguos de uno dado para
     * determinar si es interior a las restricciones heurísticas
     *
     * @param informacion
     *
     * @param conjRestSigPort el el conjunto de restricciones significativas
     * de portafolio
     *
     * @return contiguos es el conjunto de portafolios contiguos
     *
     * @throws persistenciaMingo.XcargaDatos
     */
    public ArrayList<Portafolio> entregaContiguos (
            ArrayList<Object> informacion, ConjRestGrafoE conjRestSigParque, boolean simple)throws XcargaDatos{
        ArrayList<Portafolio> contiguos = null;
        return contiguos;
    }






    /**
     * Carga el costo del paso si el portafolio es this, el objetivo obj
     * y el NodoE es nE.
     * EL COSTO DEL PASO QUE SE SUPONE CARGADO EN EL INSTANTE MEDIO DEL PASO DE TIEMPO
     * @param obj
     * @param nt
     * @param informacion
     * @return
     */
    public double calculaCostoPaso(Objetivo obj, Object bolsaDeSimulResult, NodoE nE, ArrayList<Object> informacion)
                        throws XcargaDatos{
        return 0.0;
    }

    
    /**
     * Carga en el nodoE nE (en otrosResultados) el costo operativo del paso y si se est� en tipoCargosFijos = ANUALIDADES
     * carga tambi�n la anualidad y el costo fijo.
     * @param obj
     * @param obBolsaSim
     * @param nE
     * @param informacion
     * @throws XcargaDatos
     */
    public void cargaOtrosResultados(Objetivo obj, Object obBolsaSim, NodoE nE, ArrayList<Object> informacion)
            throws XcargaDatos {
    	
    }
    
    
    /**
     * Devuelve un ResSimul con los resultados del caso INCLUSO OTROS COSTOS ADICIONALES A LA SIMULACION
     * que para un Parque son los costos fijos y de inversión, si el portafolio es this, el objetivo obj
     * y el NodoE es nE.
     * @param obj
     * @param bolsaDeSimulResult 
     * @param nE
     * @param informacion
     * @return
     */
    public ResSimul resSimulCostoPaso(Objetivo obj, Object bolsaDeSimulResult, NodoE nE, ArrayList<Object> informacion)
                        throws XcargaDatos{
        ResSimul rs = new ResSimul();                     
        return rs;
    }    
    
    
    /**
     * Calcula el costo de la decisi�n valor actual de inversi�n y costos fijos. Se usa solamente en la opci�n TotalPasoDecisi�n
     * @param obj
     * @param informacion
     * @param nodoE
     * @return
     * @throws XcargaDatos
     */
    public double calculaCostoTotalDecision(Objetivo obj, ArrayList<Object> informacion, NodoE nodoE)throws XcargaDatos{
    	return 0.0;
    }
    
    public void cargaBaseDeDatos(Object impGen, Object bolsaDeSimulResult, String dirResultCaso, String dirCorrida, ArrayList<Object> informacion)
    throws XcargaDatos {
        return;
    }    



    /**
     * Carga el valor de fin de juego si el portafolio final es this, el objetivo obj
     * y el nodoE es nE, suponiendo que este portafolio está en la etapa final.
     * @param obj
     * @param nE
     * @param informacion
     * @return
     */
    public double calculaCostoFinJuego(Objetivo obj, Object baseDeDatosResult, NodoE nE, ArrayList<Object> informacion)
            throws XcargaDatos{
        return 0.0;
    }

    
    /**
     * Devuelve inversi�n y costos fijos actualizados de una decisi�n, que es un Portafolio
     * @param obj
     * @param informacion
     * @param nE
     * @return
     * @throws XcargaDatos
     */
    public ArrayList<Double> totalInvYCFijosActualizados(Objetivo obj, ArrayList<Object> informacion, NodoE nE) throws XcargaDatos{
    	ArrayList<Double> al = new ArrayList<Double>();
    	return al;
    }

    public String toStringCorto(ArrayList<Object> informacion, ArrayList<Boolean> informacionImp) throws XcargaDatos{
        String texto = "";
        return texto;
    }

//    public String toStringCortoLinea(ArrayList<Object> informacion, ArrayList<Boolean> informacionImp) throws XcargaDatos{
//        String texto = "";
//        return texto;
//    }

//    public String toStringMuyCorto(){
//        String texto = "";
//        return texto;
//    }
//
//    public String toStringOrdenado(){
//        String texto = "";
//        return texto;
//    }

//    /**
//     * Carga el texto resumido que describe el portafolio en resumenP.
//     * @param usaEquiv si es true los elementos del portafolio se sustituyen en el resumen
//     * por otros que son equivalentes en la simulación de costos.
//     */
//    public void cargaResumenP(ArrayList<Object> informacion, boolean usaEquiv)throws XcargaDatos {
//
//    }


    /**
     * Crea un texto resumido que describe el portafolio.
     * @param usaEquiv si es true los elementos del portafolio se sustituyen en el resumen
     * por otros que son equivalentes en la simulación de costos.
     */
    public String creaResumenP(ArrayList<Object> informacion, boolean usaEquiv)throws XcargaDatos {
        return "";
    }    
    

    /**
     * Crea textos adicionales para la impresión de los NodoE
     */
    public String creaTextoAdicNodoE(ArrayList<Object> informacion, NodoN nN) throws XcargaDatos {
        return "";
    }

    /**
     * Carga en los portafolios información necesaria para procesos siguientes
     */
    public void cargaResumenFinal(ArrayList<Object> informacion) throws XcargaDatos{

    }

    /**
     * Devuelve el ResSimul cuando el portafolio es this y el NodoN es nN
     */
    public ResSimul devuelveResSimul(NodoN nN, Object baseDeDatosResult,
            ArrayList<Object> informacion) throws XcargaDatos{
        ResSimul rS = new ResSimul();
        return rS;
    }





// *******************************************************************
// ACA TERMINAN LOS MÉTODOS A SOBREESCRIBIR
// *******************************************************************


    /**
     * METODO esNodoEFactible
     * Determina si el NodoE (this, nt) es factible dadas las restricciones
     * a la configuración de portafolios que existen, tanto
     * significativas como heurísticas
     *
     * @param nt es el nodo de la naturaleza.
     *
     * @param informacion
     *
     * @conjRParque es el conjunto de restricciones de portafolio que debe
     * cumplir el nodoE.
     *
     * @return result true o false según el nodoE sea factible o no.
     *
     */
    public boolean esNodoEFactible(NodoN nt, ArrayList<Object> informacion,
			ConjRestGrafoE conjRPort ) throws XcargaDatos{
        boolean result = true;
        Portafolio vacio = new Portafolio();
            for(RestGrafoE rgE: conjRPort.getRestricciones()){
                if(! rgE.evaluar(informacion, this, vacio, nt)){
                    result = false;
                    break;
                }
            }
		return result;
	}

//	public boolean esNodoEFactible(ArrayList<Object> informacion,
//			ConjRestGrafoE conjRPort ) throws XcargaDatos{
//            boolean result = true;
//            for(RestGrafoE rgE: conjRPort.getRestricciones()){
//                if(! rgE.evaluar(informacion, this)){
//                    result = false;
//                    break;
//                }
//            }
//		return result;
//	}


    /**
     * METODO esPortafolioInterior
     *
     * Si el portafolio es interior devuelve un ArrayList<RestGrafoE> vacio.
     * Si el portafolio no es interior devuelve la lista de restricciones heurísticas
     * que causan que el portafolio no sea interior.
     *
     * Sean:
     * S es el conjunto de restricciones significativas de parque
     * H es el conjunto de restricciones heurísticas de parque
     *
     * Un parque p es interior a las restricciones heurísticas H si y solo sí 
     * el nodoE (p, nt) es factible y todos sus parques contiguos
     * verifican también las restricciones heurísticas H
     *
     * En la implementación actual nt el estado de la naturaleza no se
     * emplea ya que las restricciones de parque no dependen del estado
     * de la naturaleza
     *
     * @param nt es el nodoN que no se aplica en esta implementación
     *
     * @param informacion
     *
     * @param conjRHP es el conjunto de restricciones heurísticas de portafolio corrientes
     *
     * @param conjRSP es el conjunto de restricciones significativas de portafolio del caso
     *
     * @param contiguos es la lista de portafolios contiguos que deben verificarse
     *
     * @return restHNoCumple es el conjunto de restricciones heurísticas que impiden
     * que el portafolio sea interior; si el conjunto es vacío el parque es interior
     *
     */
    public ArrayList<RestGrafoE> esPortafolioInterior(NodoN nt, ArrayList<Object> informacion,
            ConjRestGrafoE conjRPHeu, ConjRestGrafoE ConjRPSig, 
            ArrayList<Portafolio> contiguos) throws XcargaDatos{

        boolean interior = true;
        Portafolio vacio = new Portafolio();
        ArrayList<RestGrafoE> causanNoInt = new ArrayList<RestGrafoE>();
        // si el nodoE no es factible el portafolio no es interior
        if(! this.esNodoEFactible(nt, informacion, conjRPHeu)) return causanNoInt;
        // si alguno de los portafolios contiguos no cumple la restricción el portafolio no es interior
        for (Portafolio p: contiguos){
            for (RestGrafoE rH: conjRPHeu.getRestricciones()){
                if(rH.evaluar(informacion, p, vacio, nt)== false){
                    causanNoInt.add(rH);
                }
            }
        }
        return causanNoInt;
    }


    /**
     *
     * Si el portafolio es interior devuelve un ArrayList<ParRestPort> vacio.
     * Si el portafolio no es interior devuelve la lista de <ParRestPort>
     * que tienen cada uno:
     * - una restricción heurística que causa que el portafolio (this)no sea interior.
     * - el portafolio contiguo asociado que no cumple la restricción heurística
     *
     * Sean:
     * S es el conjunto de restricciones significativas de parque
     * H es el conjunto de restricciones heurísticas de parque
     *
     * Un parque p es interior a las restricciones heurísticas H si y solo sí
     * el nodoE (p, nt) es factible y todos sus parques contiguos
     * verifican también las restricciones heurísticas H
     *
     * En la implementación actual nt el estado de la naturaleza no se
     * emplea ya que las restricciones de parque no dependen del estado
     * de la naturaleza
     *
     * @param nt es el nodoN que no se aplica en esta implementación
     *
     * @param informacion
     *
     * @param conjRHP es el conjunto de restricciones heurísticas de portafolio corrientes
     *
     * @param conjRSP es el conjunto de restricciones significativas de portafolio del caso
     *
     * @param contiguos es la lista de portafolios contiguos que deben verificarse
     *
     * @return restHNoCumple es el conjunto de ParRestPort que impiden
     * que el portafolio sea interior; si el conjunto es vacío el parque es interior
     *
     */
    public ArrayList<ParRestPort> esPortafolioInterior2(NodoN nt, ArrayList<Object> informacion,
            ConjRestGrafoE conjRPHeu, ConjRestGrafoE ConjRPSig,
            ArrayList<Portafolio> contiguos) throws XcargaDatos{

        boolean interior = true;
        Portafolio vacio = new Portafolio();
        ArrayList<ParRestPort> causanNoInt = new ArrayList<ParRestPort>();
        // si el nodoE no es factible el portafolio no es interior
//        if(! this.esNodoEFactible(nt, informacion, conjRPHeu)) return causanNoInt;
//        // si alguno de los portafolios contiguos no cumple la restricción el portafolio no es interior
        for (Portafolio p: contiguos){
            for (RestGrafoE rH: conjRPHeu.getRestricciones()){
                if(rH.evaluar(informacion, p, vacio, nt)== false){
//                    p.cargaResumenP(informacion, false);
                    ParRestPort prp = new ParRestPort(rH, p);
                    causanNoInt.add(prp);
                }
            }
        }
        return causanNoInt;
    }



}
