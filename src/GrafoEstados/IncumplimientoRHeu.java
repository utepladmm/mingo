/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GrafoEstados;

import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.EscenarioCN;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class IncumplimientoRHeu implements Serializable{


    /**
     * Sirve para almacenar n-uplas
     * (cd , periodo de tiempo, escenario, portafolio contiguo que causa que no sea interno el
     *  portafolio óptimo del cd)
     * 
     * que indican que la restricción heurísta no se cumple para un cd
     *
     */
    private ConjDeDecision cd;
    private PeriodoDeTiempo pt;
    private EscenarioCN esc;
    private Portafolio portC;
    /**
     * portC es el portafolio, contiguo al del cd, que causa que dicho portafolio no sea interno
     */

    public IncumplimientoRHeu(ConjDeDecision cd, PeriodoDeTiempo pt, EscenarioCN esc,
            Portafolio portC) {
        this.cd = cd;
        this.pt = pt;
        this.esc = esc;
        this.portC = portC;
    }

    public ConjDeDecision getCd() {
        return cd;
    }

    public void setCd(ConjDeDecision cd) {
        this.cd = cd;
    }

    public EscenarioCN getEsc() {
        return esc;
    }

    public void setEsc(EscenarioCN esc) {
        this.esc = esc;
    }

    public PeriodoDeTiempo getPt() {
        return pt;
    }

    public void setPt(PeriodoDeTiempo pt) {
        this.pt = pt;
    }

    public Portafolio getPortC() {
        return portC;
    }

    public void setPortC(Portafolio portC) {
        this.portC = portC;
    }



    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IncumplimientoRHeu other = (IncumplimientoRHeu) obj;
        if (this.cd != other.cd && (this.cd == null || !this.cd.equals(other.cd))) {
            return false;
        }
        if (this.pt != other.pt && (this.pt == null || !this.pt.equals(other.pt))) {
            return false;
        }
        if (this.esc != other.esc && (this.esc == null || !this.esc.equals(other.esc))) {
            return false;
        }
        return true;
    }



    
    public String toStringCorto(ArrayList<Object> informacion) throws XcargaDatos {
        String texto ="//  ";
        texto += pt.toStringCorto() + " , ";
        texto += "ord. CD " + cd.getOrdinalCD() + " , ";
        boolean usaequiv = false;
        texto += "port. del CD " + cd.getPort().creaResumenP(informacion, usaequiv) + " , ";        
        texto += "escenario " + esc.getNumesc() + " , ";
        texto += "causante " + portC.creaResumenP(informacion, usaequiv) + "\r\n";
        return texto;
    }
}
