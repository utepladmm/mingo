/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import dominio.Estudio;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import naturaleza3.ConjNodosN;
import naturaleza3.NodoN;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class BolsaCD implements Serializable {
    // Estructura que almacena los conjuntos de decisión

    private GrafoE grafoE;
    private ArrayList<HashMap<ClaveCD, ConjDeDecision>> conjuntosCD;
    /**
     *  Primer índice etapa incluso la etapa 0.
     *  Segundo índice ordinal del cd de cada etapa
     *
     */
    private ArrayList<HashSet<ConjDeDecision>> cDSinSuc;
    /**
     * cDSinSuc es la lista de conjuntos de decisión sin sucesores que deben eliminarse
     * Primer índice etapa
     * Es un set porque los conjuntos de decisión a eliminar no se repiten.
     * No se puede eliminar dos veces el mismo cd
     */
    private ArrayList<ConjDeDecision> cDsAVerificar;
    /**
     * lista de cDs usada en la eliminacion de CDs sin sucesor
     * es la misma para todas las etapas del proceso
     */
    private ArrayList<ArrayList<ClaveCD>> clavesOrdenadas;
    /**
     * Preserva el orden natural de generación de los CDs, mediante sus claves.
     */    
    
    private class ClaveCD{

        private Portafolio port;
        private ConjNodosN conjInfo;

        public ClaveCD(Portafolio port, ConjNodosN conjInfo) {
            this.port = port;
            this.conjInfo = conjInfo;
        }
        
        

        public ConjNodosN getConjInfo() {
            return conjInfo;
        }

        public void setConjInfo(ConjNodosN conjInfo) {
            this.conjInfo = conjInfo;
        }

        public Portafolio getPort() {
            return port;
        }

        public void setPort(Portafolio port) {
            this.port = port;
        }
        
        @Override
        public String toString(){
            String texto = port.toString() + conjInfo.toString();
            return texto;
        }
        

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ClaveCD other = (ClaveCD) obj;
            if (this.port != other.port && (this.port == null || !this.port.equals(other.port))) {
                return false;
            }
            if (this.conjInfo != other.conjInfo && (this.conjInfo == null || !this.conjInfo.equals(other.conjInfo))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 59 * hash + (this.port != null ? this.port.hashCode() : 0);
            hash = 59 * hash + (this.conjInfo != null ? this.conjInfo.hashCode() : 0);
            return hash;
        }
        
        
        
        
    }
    



    public BolsaCD(GrafoE grafoE) {
        this.grafoE = grafoE;
        conjuntosCD = new ArrayList<HashMap<ClaveCD, ConjDeDecision>>();
        cDSinSuc = new ArrayList<HashSet<ConjDeDecision>>();
        clavesOrdenadas = new ArrayList<ArrayList<ClaveCD>>();
        for (int i = 0; i <= grafoE.getCantEtapas(); i++) {
            HashMap<ClaveCD, ConjDeDecision> auxLCD1 = new HashMap<ClaveCD, ConjDeDecision>();
            conjuntosCD.add(auxLCD1);
            HashSet<ConjDeDecision> auxLCD2 = new HashSet<ConjDeDecision>();
            cDSinSuc.add(auxLCD2);
            ArrayList<ClaveCD> auxClave = new ArrayList<ClaveCD>();
            clavesOrdenadas.add(auxClave);
        }
    }


    
    

//    public ArrayList<HashMap<ClaveCD, ConjDeDecision>> getConjuntosCD() {
//        return conjuntosCD;
//    }
//
//    public void setConjuntosD(ArrayList<HashMap<ClaveCD, ConjDeDecision>> conjuntosCD) {
//        this.conjuntosCD = conjuntosCD;
//    }

    /**
     * Devuelve la lista de cDs de la etapa t EN EL MISMO ORDEN EN QUE
     * SE CREARON . 
     * NO DEBE OPERARSE SOBRE ELLOS SINO SOLO RECORRER LA COLECCIÓN.
     * @param t
     * @return
     */
    public ArrayList<ConjDeDecision> cDsUnaEtapa(int t) throws XcargaDatos {
        ArrayList<ConjDeDecision> listaCDs = new ArrayList<ConjDeDecision>();
        HashMap<ClaveCD, ConjDeDecision> mapa;
        mapa = conjuntosCD.get(t);
        for(ClaveCD clave: clavesOrdenadas.get(t) ){
//            if(! mapa.containsKey(clave)) throw new XcargaDatos("En etapa " + t + " no existe cd para clave " + clave.toString());
            listaCDs.add(mapa.get(clave));
        }        
       return listaCDs;        
    }
    
    /**
     * Devuelve el CD de la etapa t que tiene portafolio port y conjunto de información
     * cI o null si no existe dicho CD.
     * @param port Portafolio del CD
     * @param cI conjunto de información del CD
     * @param t etapa del CD.
     */
    public ConjDeDecision devuelveCD(int t, Portafolio port, ConjNodosN cI){
        HashMap<ClaveCD, ConjDeDecision> mapa;
        mapa = conjuntosCD.get(t);
        ClaveCD claveCD = new ClaveCD(port, cI);
        return mapa.get(claveCD);                      
    }
    
    /**
     * Devuelve el CD único que existe en la etapa inicial uno.
     * @return 
     */
    public ConjDeDecision devuelveCDInicial(){
        Collection cDsUno = conjuntosCD.get(1).values();
        Iterator it = cDsUno.iterator();
        return (ConjDeDecision)it.next();        
    }


    
    /** 
     * Recibe un nuevo CD de la etapa t con su Portafolio y ConjNodosN (conjunto de informacion)
     * y lo almacena en la bolsaCD
     *
     */
    public void recibeUnCD(int t, ConjDeDecision cD){        
        ClaveCD claveCD = new ClaveCD(cD.getPort(), cD.getConjInfo());
        assert (!conjuntosCD.get(t).containsKey(claveCD)) : "Error: se intentó guardar un CD que ya existía en la BolsaCD";
        cD.setOrdinalCD(ConjDeDecision.getContadorConjDeDecision());
        ConjDeDecision.setContadorConjDeDecision(ConjDeDecision.getContadorConjDeDecision() + 1);
        conjuntosCD.get(t).put(claveCD, cD); 
        clavesOrdenadas.get(t).add(claveCD);
    }

    public ArrayList<HashSet<ConjDeDecision>> getCDSinSuc() {
        return cDSinSuc;
    }

    public void setCDSinSuc(ArrayList<HashSet<ConjDeDecision>> cDSinSuc) {
        this.cDSinSuc = cDSinSuc;
    }

    public GrafoE getGrafoE() {
        return grafoE;
    }

    public void setGrafoE(GrafoE grafoE) {
        this.grafoE = grafoE;
    }

    public ArrayList<ConjDeDecision> getCDsAVerificar() {
        return cDsAVerificar;
    }

    public void setCDsAVerificar(ArrayList<ConjDeDecision> cDsAVerificar) {
        this.cDsAVerificar = cDsAVerificar;
    }

    public void agregaCDSinSuc(ConjDeDecision cd, int etapa) {
        /**
         * Agrega un CD sin sucesores al cDSinSuc que guarda los conjuntos de
         * decisión sin sucesores
         */
        cDSinSuc.get(etapa).add(cd);
    }

    public boolean estaVaciaCDSinSucEtapa(int etapa) {
        /**
         * Devuelve true si el conjunto de cd sin sucesores de la
         * etapa etapa está vacío y false si tiene algún cd
         */
        boolean result = false;
        if (cDSinSuc.get(etapa).isEmpty()) {
            result = true;
        }
        return result;
    }

    public ConjDeDecision devuelveUnCDSinSucEtapa(int etapa) throws XcargaDatos {
        /**
         * Devuelve un cd sin sucesor de la etapa etapa y lo elimina de
         * la lista de cd sin sucesores
         * Si no hay ninguno lanza una excepción
         */
        ConjDeDecision cd = new ConjDeDecision();
        if (estaVaciaCDSinSucEtapa(etapa)) {
            throw new XcargaDatos("Error: se le " +
                    "acabaron los cd sin sucesores de la etapa " + etapa);
        }
        int indUlt = cDSinSuc.get(etapa).size();
        Iterator<ConjDeDecision> iter = cDSinSuc.get(etapa).iterator();
        
        cd = iter.next();
        return cd;
    }

    public void eliminaUnCDSinSucEtapa(int etapa, ConjDeDecision cd)
            throws XcargaDatos {
        /**
         * Elimina el conjunto de decision cd de la lista de CDs sin sucesores
         * Si no hay ninguno lanza una excepción
         * Elimina los nodosE en el GrafoE del cd eliminado.
         */
        if (estaVaciaCDSinSucEtapa(etapa)) {
            throw new XcargaDatos("Error: se le " +
                    "acabaron los cd sin sucesores de la etapa " + etapa);
        }
        cDSinSuc.get(etapa).remove(cd);
    }

    public void eliminaUnCDDelGrafo(int etapa, ConjDeDecision cd) throws XcargaDatos {
        /**
         * Elimina un CD de conjuntosCD
         * Si el portafolios port no tiene otros CDs en la etapa lo
         * elimina de ConjuntoPortafolios en etapa
         */
        System.out.println("=================================================================");        
        System.out.println("ELIMINA CD SIN SUCESORES ORDINAL " + cd.getOrdinalCD());
        System.out.println("=================================================================");                
        ClaveCD clave = new ClaveCD(cd.getPort(), cd.getConjInfo());
        conjuntosCD.get(etapa).remove(clave);
        clavesOrdenadas.get(etapa).remove(clave);

        
//        Portafolio port = cd.getPort();
//        ArrayList<ConjDeDecision> cdsDePort;
//        cdsDePort = grafoE.getConjPort().cDsDeUnPortafolio(port, etapa);
//        cdsDePort.remove(cd);
//        if (cdsDePort.isEmpty()) {
//            grafoE.getConjPort().eliminaPortafolio(port, etapa);
//        }
    }


    public void creaSucesores(ArrayList<Object> informacion, Objetivo obj) throws XcargaDatos{
        Estudio est = (Estudio)informacion.get(0);
        int cantEtapas = est.getDatosGenerales().getCantEtapas();
        for(int e = 1; e<cantEtapas; e++){
            /**
             * Para encontrar los cd sucesores de un cd toma uno cualquiera
             * de los NodoE del cd, en este caso el primero el get(0) de los NodoE sucesores.
             */
            for(Object cdOb: cDsUnaEtapa(e)){
                ConjDeDecision cd = (ConjDeDecision)cdOb;
                NodoE nE = cd.getNodos().get(0);
                NodoN nN = nE.getNodoN();
                // Se recorren todas las decisiones del cd
                for(DecConjNodos dcn: nE.getSucesores()){
                    DecConjCD dcCD = new DecConjCD();
                    dcCD.setDecision(dcn.getDecision());
                    ArrayList<ConjDeDecision> auxCD = new ArrayList<ConjDeDecision>();
                    ArrayList<Double> auxProb = new ArrayList<Double>();
                    dcCD.setCdSucs(auxCD);
                    dcCD.setProbTrans(auxProb);
                    for(NodoE nEsuc: dcn.getNodosE()){
                        NodoN nNsuc = nEsuc.getNodoN();
                        ConjDeDecision cDsuc = nEsuc.getCD();
                        if(! auxCD.contains(cDsuc) ) {
                            auxCD.add(cDsuc);
                            auxProb.add(0.0);
                        }
                        double prob1 = nN.probDeUnSucesor(nNsuc);
                        int indice = auxCD.indexOf(cDsuc);
                        auxProb.set(indice, auxProb.get(indice)+prob1);
                    }
                    cd.agregaSucesor(dcCD);
                }
            }
        }
    }


//    public String toStringCortoLinea(ArrayList<Object> informacion, Objetivo obj) throws XcargaDatos {
//        String texto = "";
//        Estudio est = (Estudio)informacion.get(0);
//        int cantEtapas = est.getDatosGenerales().getCantEtapas();
//        boolean cabezal = true;
//        texto = conjuntosCD.get(1).get(0).toStringCortoLinea(informacion, obj, cabezal) + "\r\n";
//        cabezal = false;
//        for(int e=1; e<=cantEtapas; e++){
//            for(ConjDeDecision cd: conjuntosCD.get(e)){
//                texto += cd.toStringCortoLinea(informacion, obj, cabezal) + "\r\n";
//            }
//        }
//        return texto;
//    }

//    public void escribeBolsaCDLinea(ArrayList<Object> informacion,
//            String archivo, Objetivo obj) throws XcargaDatos {
//        String texto = "";
//        Estudio est = (Estudio)informacion.get(0);
//        int cantEtapas = est.getDatosGenerales().getCantEtapas();
//        boolean cabezal = true;
//        texto = conjuntosCD.get(1).get(0).toStringCortoLinea(informacion, obj, cabezal) + "\r\n";
//        UtilitariosGenerales.DirectoriosYArchivos.agregaTexto(archivo, texto);
//        cabezal = false;
//        for(int e=1; e<=cantEtapas; e++){
//            for(ConjDeDecision cd: conjuntosCD.get(e)){
//                texto = cd.toStringCortoLinea(informacion, obj, cabezal) + "\r\n";
//                UtilitariosGenerales.DirectoriosYArchivos.agregaTexto(archivo, texto);
//            }
//        }
//    }


}
