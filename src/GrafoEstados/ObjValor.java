/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GrafoEstados;

import java.io.Serializable;

/**
 *
 * @author ut469262
 */
public class ObjValor implements Serializable{
	/**
	 * Es una pareja objetivo - valor numérico real
	 */
	
	private Objetivo obj;
	private double valor;

	public ObjValor(Objetivo obj, double valor) {
		this.obj = obj;
		this.valor = valor;
	}

	public Objetivo getObj() {
		return obj;
	}

	public void setObj(Objetivo obj) {
		this.obj = obj;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		String texto = "";
		texto += "Objetivo: " + obj + "\t";
		texto += "Valor: " + valor + "\n";
		return texto;
	}


	







}
