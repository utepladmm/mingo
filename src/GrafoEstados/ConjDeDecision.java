/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import AlmacenSimulaciones.BolsaDeSimulaciones3;
import AlmacenSimulaciones.ImplementadorGeneral;
import UtilitariosGenerales.ParDatos;
import dominio.Caso;
import dominio.Estudio;
import dominio.Parque;
import dominio.Recurso;
import dominio.Transformacion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import naturaleza3.ConjNodosN;
import naturaleza3.NodoN;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class ConjDeDecision implements Serializable {

    /**
     * Conjunto de decisión es el conjunto de los nodosE de un GrafoE que por tener
     * asociados nodosN del mismo conjunto de información, no son distinguibles
     * - al momento de determinar el conjunto de las decisiones posibles
     * - al momento de elegir la decisión óptima dentro del conjunto de deciciones
     *   posibles.
     *
     */
    private int etapa;
    private static int contadorConjDeDecision = 0;
    private Portafolio port;
    private int ordinalCD;
    private ConjNodosN conjInfo;
    /**
     * Conjunto de información en grafoN asociado al conjunto de decisión this.
     */
    private ArrayList<ConjDetPortE> conjsDetP;
    /**
     * Lista de los conjuntos determinantes de parque de nodosE (CDPE) asociados
     * a este conjunto de decisión.
     */
    private ArrayList<NodoE> nodos;
    /**
     * Los nodosE del conjunto de decisión this
     */
    private ArrayList<Portafolio> decisiones;
    /**
     * La lista de decisiones posibles
     */
    private ArrayList<ObjDecision> decOptimas;
    /**
     * Conjunto de decisiones óptimas en el conjunto de decisión this
     * una para cada objetivo
     */
    private ArrayList<ObjValor> valorVBIniPaso;
    /**
     * Valor de Bellman esperado al inicio del paso con las probabilidades
     * condicionales de los nodos del CI asociado al ConjDeDecision.
     */
    private ArrayList<ObjValor> costoEspPaso;
    /**
     * Costo esperado del paso con las probabilidades condicionales de los nodos del CI
     * asociado al ConjDeDecision.
     */
    
    private ArrayList<DecConjCD> sucesores;
    private boolean sinSuc; // True si el cd no tiene sucesores al crear el grafoE

  
    
    
    public ConjDeDecision(int etapa, Portafolio port) {
        this.etapa = etapa;
        this.port = port;
//          port.cargaResumenP();
        nodos = new ArrayList<NodoE>();
        decisiones = new ArrayList<Portafolio>();
        decOptimas = new ArrayList<ObjDecision>();
        conjsDetP = new ArrayList<ConjDetPortE>();
        valorVBIniPaso = new ArrayList<ObjValor>() ;
        costoEspPaso = new ArrayList<ObjValor>() ;
        sucesores = new ArrayList<DecConjCD>() ;
        sinSuc = false;
    }

    public ConjDeDecision(int etapa, Portafolio port, ConjNodosN conjInfo) {
        this.etapa = etapa;
        this.port = port;
        this.conjInfo = conjInfo;
//          port.cargaResumenP();
        nodos = new ArrayList<NodoE>();
        decisiones = new ArrayList<Portafolio>();
        decOptimas = new ArrayList<ObjDecision>();
        conjsDetP = new ArrayList<ConjDetPortE>();
        valorVBIniPaso = new ArrayList<ObjValor>() ;
        costoEspPaso = new ArrayList<ObjValor>() ;
        sucesores = new ArrayList<DecConjCD>() ;
        sinSuc = false;
    }

    
    public ConjDeDecision() {
        sinSuc = false;
    }

    public Portafolio getPort() {
        return port;
    }

    public void setPort(Portafolio port) {
        this.port = port;
    }

    public int getOrdinalCD() {
        return ordinalCD;
    }

    public void setOrdinalCD(int ordinalCD) {
        this.ordinalCD = ordinalCD;
    }

    public static int getContadorConjDeDecision() {
        return contadorConjDeDecision;
    }

    public static void setContadorConjDeDecision(int contadorConjDeDecision) {
        ConjDeDecision.contadorConjDeDecision = contadorConjDeDecision;
    }

    public ConjNodosN getConjInfo() {
        return conjInfo;
    }

    public void setConjInfo(ConjNodosN conjInfo) {
        this.conjInfo = conjInfo;
    }

    public ArrayList<ConjDetPortE> getConjsDetP() {
        return conjsDetP;
    }

    public void setConjsDetP(ArrayList<ConjDetPortE> conjsDetP) {
        this.conjsDetP = conjsDetP;
    }

    public ArrayList<Portafolio> getDecisiones() {
        return decisiones;
    }

    public void setDecisiones(ArrayList<Portafolio> decisiones) {
        this.decisiones = decisiones;
    }

    public ArrayList<DecConjCD> getSucesores() {
        return sucesores;
    }

    public void setSucesores(ArrayList<DecConjCD> sucesores) {
        this.sucesores = sucesores;
    }

    public void agregaSucesor(DecConjCD suc){
        sucesores.add(suc);
    }


    public int getEtapa() {
        return etapa;
    }

    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }

    public ArrayList<NodoE> getNodos() {
        return nodos;
    }

    public void setNodos(ArrayList<NodoE> nodos) {
        this.nodos = nodos;
    }

    public ArrayList<ObjValor> getValorVBIniPaso() {
        return valorVBIniPaso;
    }

    public void setValorVBIniPaso(ArrayList<ObjValor> valorVBIniPaso) {
        this.valorVBIniPaso = valorVBIniPaso;
    }

    public ArrayList<ObjValor> getCostoEspPaso() {
        return costoEspPaso;
    }

    public void setCostoEspPaso(ArrayList<ObjValor> costoEspPaso) {
        this.costoEspPaso = costoEspPaso;
    }



    public ArrayList<ObjDecision> getDecOptimas() {
        return decOptimas;
    }

    public void setDecOptimas(ArrayList<ObjDecision> decOptimas) {
        this.decOptimas = decOptimas;
    }

    public boolean isSinSuc() {
        return sinSuc;
    }

    public void setSinSuc(boolean sinSuc) {
        this.sinSuc = sinSuc;
    }
    
    

    public boolean agregaDecOptima(Portafolio d, Objetivo obj) {
        /**
         * Agrega la decisión óptima d para el objetivo obj
         * Si preexistía una decisión óptima para ese objetivo la elimina
         * y devuelve true, de lo contrario devuelve false
         */
        ObjDecision aBorrar = null;
        boolean borro = false;
        for (ObjDecision od : decOptimas) {
            if (od.getObj() == obj) {
                aBorrar = od;
                borro = true;
                break;
            }
        }
        if (borro) {
            decOptimas.remove(aBorrar);
        }
        ObjDecision decOpt = new ObjDecision(obj, d);
        decOptimas.add(decOpt);
        return borro;
    }

    /**
     * Carga en el cd la decisión óptima para el objetivo obj, que es la
     * decisión d, y carga el Valor de Bellman esperado del cd vBellCD
     * @param d
     * @param obj
     * @param vBellCD
     * @return
     */
    public boolean agregaDecOptima(Portafolio d, Objetivo obj, double vBellCD,
            double costoPasoCDEsp) {
        /**
         * Agrega la decisión óptima d para el objetivo obj
         * Si preexistía una decisión óptima para ese objetivo la elimina
         * y devuelve true, de lo contrario devuelve false
         */
        ObjDecision aBorrar = null;
        ObjValor oVABorrar = null;
        boolean borro = false;
        for (ObjDecision od : decOptimas) {
            if (od.getObj() == obj) {
                aBorrar = od;
                borro = true;
                break;
            }
        }
        if (borro) {
            decOptimas.remove(aBorrar);
        }
        ObjDecision decOpt = new ObjDecision(obj, d);
        decOptimas.add(decOpt);
        /**
         * Agrega el valor de Bellman esperado
         */
        boolean borro2 = false;
        for (ObjValor ov : valorVBIniPaso) {
            if (ov.getObj() == obj) {
                oVABorrar = ov;
                borro2 = true;
                break;
            }
        }
        if (borro2) {
            valorVBIniPaso.remove(oVABorrar);
        }
        ObjValor vBOpt = new ObjValor(obj, vBellCD);
        valorVBIniPaso.add(vBOpt);
        /**
         * Agrega el costo del paso esperado
         */
        borro2 = false;
        for (ObjValor ov : costoEspPaso) {
            if (ov.getObj() == obj) {
                oVABorrar = ov;
                borro2 = true;
                break;
            }
        }
        if (borro2) {
            costoEspPaso.remove(oVABorrar);
        }
        ObjValor cPOpt = new ObjValor(obj, costoPasoCDEsp);
        costoEspPaso.add(cPOpt);

        return borro;
    }

    public Portafolio decOptima(Objetivo obj) throws XcargaDatos {
        /**
         * Devuelve la decisión optima del CD this con el objetivo obj
         */
        Portafolio decOp = null;
        boolean encontro = false;
        for (ObjDecision od : decOptimas) {
            if (od.getObj() == obj) {
                encontro = true;
                decOp = od.getDec();
                break;
            }
        }
        if (!encontro) {
            throw new XcargaDatos("No encontró la decisión" +
                    "optima asociada al objetivo " + obj.toString() +
                    " en el CD " + this.toString());
        }
        return decOp;
    }

    /**
     * Devuelve el valor de Bellman del CD this con el objetivo obj
     */
    public double valBellmanDeObj(Objetivo obj) throws XcargaDatos {

        boolean encontro = false;
        double valor = 0.0;
        for (ObjValor ov : valorVBIniPaso) {
            if (ov.getObj() == obj) {
                encontro = true;
                valor = ov.getValor();
                break;
            }
        }
        if (!encontro) {
            throw new XcargaDatos("No encontró el valor de Bellman" +
                    "asociado al objetivo " + obj.toString() +
                    " en el CD " + this.toString());
        }
        return valor;
    }

    /**
     * Devuelve el costo esperado del paso del CD this con el objetivo obj
     */
    public double costoEspPasoDeObj(Objetivo obj) throws XcargaDatos {

        boolean encontro = false;
        double valor = 0.0;
        for (ObjValor ov : costoEspPaso) {
            if (ov.getObj() == obj) {
                encontro = true;
                valor = ov.getValor();
                break;
            }
        }
        if (!encontro) {

            throw new XcargaDatos("No encontró el costo esperado del paso" +
                    "asociado al objetivo " + obj.toString() +
                    " en el CD " + this.toString());
        }
        return valor;
    }

//    /**
//     * Devuelve los valores del objetivo obj asociados a los percentiles del Caso,
//     * cuando se considera la distribución de valores del objetivo de todos los NodoE del
//     * ConjDeDecision.
//     */
//    public ArrayList<Double> percentiles(Objetivo obj, Object bolsaDeSimulResult,
//            ArrayList<Object> informacion) throws XcargaDatos{
//        Caso caso = (Caso)informacion.get(1);
//        ArrayList<Double> vPercentiles = caso.getPercentiles();
//        ArrayList<Double> probabilidades = new ArrayList<Double>();
//        ArrayList<Object> listaResS = new ArrayList<Object>();
//        for(NodoE nE: nodos){
//            ResSimul rS = nE.devuelveResSimulDelNodo(obj, bolsaDeSimulResult, informacion);
//            listaResS.add(rS);
//            probabilidades.add(nE.getNodoN().getProbCondDadoCI());
//        }
//        ArrayList<Double> dPercentiles = obj.percentiles(listaResS, probabilidades,
//                            vPercentiles, informacion);
//        return dPercentiles;
//    }


    /**
     * Devuelve los valores del objetivo obj asociados a los percentiles del Caso,
     * cuando se considera la distribución de valores del objetivo de todos los NodoE del
     * ConjDeDecision.
     * 
     * A partir de los ResSimul del los NodoE del CD this, crea una distribución
     * de resultados del Objetivo obj, suponiendo que es crónico ObjNumCron.
     * Para cada crónica y cada NodoN hay un resultado double del obj.
     * Los costos están cargados en la mitad del período
     */    
    public ArrayList<Double> percentiles(Objetivo obj, Object bolsaDeSimulResult,
            ArrayList<Object> informacion) throws XcargaDatos{
        Caso caso = (Caso)informacion.get(1);
        ArrayList<Double> vPercentiles = caso.getPercentiles();
        ArrayList<Double> probabilidades = new ArrayList<Double>();
        ArrayList<Object> listaResS = new ArrayList<Object>();
        for(NodoE nE: nodos){            
            ResSimul rS = port.resSimulCostoPaso(obj, bolsaDeSimulResult, nE, informacion);
            listaResS.add(rS);
            probabilidades.add(nE.getNodoN().getProbCondDadoCI());
        }
        ArrayList<Double> dPercentiles = obj.percentiles(listaResS, probabilidades,
                            vPercentiles, informacion);
        return dPercentiles;
    }
    
    
    
    public void agregaNodoE(NodoE nE) {
        nodos.add(nE);
    }

    public void agregaDecision(Portafolio d, ArrayList<Object> informacion) throws XcargaDatos {
//        d.cargaResumenP(informacion, false);
        decisiones.add(d);
    }

    public boolean contieneDecision(Portafolio d) {
        boolean result = false;
        for (Portafolio da : decisiones) {
            if (da.equals(d)) {
                result = true;
                return result;
            }
        }
        return result;
    }

    public boolean tieneDecisiones() {
        boolean result = true;
        if (decisiones.isEmpty()) {
            result = false;
        }
        return result;
    }

    public boolean contieneElNodoE(NodoE nE) {
        boolean result = false;
        for (NodoE n : nodos) {
            if (n.equals(nE)) {
                /**
                 * El equals emplea el equals de portafolio
                 * que debe ser sobreescrito
                 */
                result = true;
                return result;
            }
        }
        return result;
    }

    public NodoE devuelveElNodoEI(NodoE nB) {
        /**
         * Devuelve el NodoE del cd que es igual a nE en su contenido
         */
        boolean encontro = false;
        for (NodoE nE : nodos) {
            if (nE.equals(nB)) {
                encontro = true;
                return nE;
            }
        }
        assert (encontro == true) : "No encontro un nodoE en un cD";
        return null;
    }

    public NodoE devuelveElNodoE(NodoN nN) {
        /**
         * Devuelve el nodoE del conjunto de decisión que tiene como nodo
         * de la naturaleza el nodoN, o null si el nodoE no existe en el cD
         */
        boolean encontro = false;
        for (NodoE nE : nodos) {
            if (nE.getNodoN() == nN) {
                /**
                 * El nodo de la naturaleza es el mismo, no uno con iguales
                 * datos
                 */
                encontro = true;
                return nE;
            }
        }
        assert (encontro == true) : "No encontro un nodoE en un cD";
        return null;
    }

    public Set<NodoE> eliminaUnaDec(Portafolio d) throws XcargaDatos {
        /**
         * Elimina una decisión del CD y de todos los nodos del CD
         * Devuelve una lista de todos los nodos de la etapa
         * siguiente a los que se accedía desde el CD mediante la decisión
         * eliminada, cuyos predecesores deben ser actualizados
         */
        Set<NodoE> nodosAc = new HashSet<NodoE>();
        ArrayList<DecConjNodos> aElim;
        if (decisiones.remove(d) == false) {
            throw new XcargaDatos("se intentó" +
                    "eliminar una decisión inexistente");
        }
        for (NodoE ne : nodos) {
            aElim = new ArrayList<DecConjNodos>();
            for (DecConjNodos dcn : ne.getSucesores()) {
                if (dcn.getDecision() == d) {
                    nodosAc.addAll(dcn.getNodosE());
                    aElim.add(dcn);
                }
            }
            for (DecConjNodos ae : aElim) {
                ne.getSucesores().remove(ae);
            }
        }
        return nodosAc;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConjDeDecision other = (ConjDeDecision) obj;
        if (this.etapa != other.etapa) {
            return false;
        }
        if (this.port != other.port && (this.port == null || !this.port.equals(other.port))) {
            return false;
        }
        if (this.conjInfo != other.conjInfo && (this.conjInfo == null || !this.conjInfo.equals(other.conjInfo))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.etapa;
        hash = 83 * hash + (this.port != null ? this.port.hashCode() : 0);
        hash = 83 * hash + (this.conjInfo != null ? this.conjInfo.hashCode() : 0);
        return hash;
    }


    /**
     * Genera la descripción del CD en la expansión óptima en un texto
     * 
     * @param grafoE es el GrafoE de simulaciones
     * @param obj es el Objetivo
     * @param bolsaDeSimulResult es el objeto que almacena los resultados de las simulaciones
     */
    public String generaReporteCDOptimo(GrafoE grafoE, Objetivo obj,Estudio est,
            Object bolsaDeSimulResult, String dirResultCaso) throws XcargaDatos{
        String result = "";
        int e = etapa;
        PeriodoDeTiempo pt = grafoE.perTiempoDeUnaEtapa(e);

        ArrayList<Object> informacion = grafoE.getInformacion();
        Caso caso = (Caso)informacion.get(1);
        double entornoSensib = caso.getValorSensib();
        String tipoSensib = caso.getTipoSensib();
        ArrayList<Double> percentiles = caso.getPercentiles();
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        boolean soloEleg = true;
        boolean prod = false;
        boolean usaEquiv = false;
        boolean soloNoNulos = true;
        informacionImp.add(soloEleg);
        informacionImp.add(prod);
        informacionImp.add(usaEquiv);
        informacionImp.add(soloNoNulos);        
        String textoCD = "";
        textoCD += "===================================================" + "\r\n";
        textoCD += "Etapa = " + e + " -  Tiempo = " + pt.toStringCorto() + "\r\n";
        textoCD += "DATOS DEL CD" + "\r\n";
        /**
         *  Datos generales del ConjDeDecision
         */
        textoCD   += "Portafolio= " + port.creaResumenP(informacion, usaEquiv) + "\r\n";
        if(etapa <grafoE.getCantEtapas()){
            textoCD += "Decision= " + decOptima(obj).toStringCorto(informacion, informacionImp)
               + "\r\n";
        }
        textoCD += "V.Bellman esperado al inicio del paso = " +
                UtilitariosGenerales.Numeros.redondeaDouble(valBellmanDeObj(obj),2) + "\r\n";
        textoCD += "Costo esperado del paso para el cd = " + 
                UtilitariosGenerales.Numeros.redondeaDouble(costoEspPasoDeObj(obj),2)+ "\r\n";
        textoCD += "Costo del paso para el cd en percentiles" + " , ";
        ArrayList<Double> valoresPercentiles
                    = percentiles(obj, bolsaDeSimulResult, informacion);
        int id = 0;
        for(Double d: valoresPercentiles){
            textoCD += "P" + UtilitariosGenerales.Numeros.redondeaDouble(percentiles.get(id),4)
                    + " , " + UtilitariosGenerales.Numeros.redondeaDouble(d,2) + " , ";
            id++;
        }
        // Datos de cada uno de los NodoE del ConjDeDecision y carga de la base de datos de resultados
        textoCD += "Datos de los nodos del CD" + "\r\n";
        for (NodoE nE: nodos){
            textoCD += "Ordinal del NodoE" + nE.getOrdinalNE()+ "\r\n";
            NodoN nN = nE.getNodoN();
            textoCD += port.creaTextoAdicNodoE(informacion, nN);
            ImplementadorGeneral impGen =((BolsaDeSimulaciones3)bolsaDeSimulResult).getImpGen();
            String dirCorrida = nE.getNombreSimulacion();
            port.cargaBaseDeDatos(impGen,bolsaDeSimulResult,dirResultCaso,dirCorrida,informacion);
            
        }
        textoCD += "\r\n";

        /**
         * Genera la descripción de portafolios cercanos al óptimo
         * entre los cDs que tienen el mismo conjunto de información asociado
         * y la almacena en textoCercanos
         */
        String textoCercanos = "---------------------------------------" + "\r\n";
        textoCercanos += "CDs cercanos al óptimo" + "\r\n";
        double diferencia = 0.0;
        if(tipoSensib.equalsIgnoreCase("A")){
            diferencia = entornoSensib;
        }else{
            diferencia = costoEspPasoDeObj(obj)*entornoSensib;
        }

        ArrayList<ParDatos> paresCostoEspCD = new ArrayList<ParDatos>();
        ArrayList<ConjDeDecision> conjsCD = new ArrayList<ConjDeDecision>();

        Collection cDsEtapa = grafoE.getBolsaCD().cDsUnaEtapa(e);
        for(Object cDOb: cDsEtapa){            
            ConjDeDecision cDOtro = (ConjDeDecision)cDOb;
            if(!cDOtro.isSinSuc()){
                if(cDOtro.getConjInfo()==getConjInfo() &&
                    (cDOtro.costoEspPasoDeObj(obj) - costoEspPasoDeObj(obj)) < diferencia){
                    ParDatos p1 = new ParDatos(cDOtro.costoEspPasoDeObj(obj), (Object)cDOtro);
                    paresCostoEspCD.add(p1);
                }
            }
        }
        Collections.sort(paresCostoEspCD);           
        String cabezalRecEleg=""; 
        String cabezalRecEleg1="";
        for (int ir = 1; ir <= est.getCantRecNoComunes(); ir++) {
            Recurso rec = (Recurso) est.recNoComunYTransPosible(ir - 1);
            cabezalRecEleg += rec.getRecBase().getNombre()+" "+ rec.getEstado() +",";  
            cabezalRecEleg1+= "IT="+ rec.getIndicadorTiempo()+ " DA="+ rec.getValorDemAl()+ " IA="+ rec.getValorInvAl()+",";
        }
        int cantPosibles = est.getCantRecNoComunesYTransPosibles();
        int cantRNC =  est.getCantRecNoComunes();
        for (int ir = cantRNC +1; ir <= cantPosibles; ir++) {
            Transformacion tra = (Transformacion) est.recNoComunYTransPosible(ir - 1);
            cabezalRecEleg += tra.getTransBase().getNombre() + " " +  tra.getEstado()+",";
            cabezalRecEleg1+= "IT="+ tra.getIndicadorTiempo()+ " DA="+ tra.getValorDemAl()+ " IA="+ tra.getValorInvAl()+",";
        }   
        cabezalRecEleg+=" Costo E de paso, Diferencia de CEP, VB, Diferencia VB,";
        cabezalRecEleg1+="MUSD, MUSD, MUSD, MUSD,";
        //Se escribieron los cabezales de los recusos 
        textoCercanos +=cabezalRecEleg+ "\r\n"; 
        textoCercanos +=cabezalRecEleg1+ "\r\n";
   
        //Comienza el bucle en los cd cercanos al óptimo
        for(ParDatos pD: paresCostoEspCD){        
            ConjDeDecision cDOtro = (ConjDeDecision)(pD.getDato2());
            Portafolio portCer = cDOtro.getPort();
            Parque parqueCer = (Parque) portCer;
            String strCdCercano="";

            for (int ir = 0; ir < cantPosibles; ir++) {
                strCdCercano += String.valueOf(parqueCer.codigoDeInd(ir))+",";
            }
            // Cargo los costos y VB del cd cercano
            strCdCercano += UtilitariosGenerales.Numeros.redondeaDouble(cDOtro.costoEspPasoDeObj(obj), 2)+  ",";
            strCdCercano += UtilitariosGenerales.Numeros.redondeaDouble(cDOtro.costoEspPasoDeObj(obj)
                                           - costoEspPasoDeObj(obj), 2)+  ",";
            strCdCercano += UtilitariosGenerales.Numeros.redondeaDouble(cDOtro.valBellmanDeObj(obj), 2) + " , ";
            strCdCercano += UtilitariosGenerales.Numeros.redondeaDouble(cDOtro.valBellmanDeObj(obj)
                                           - valBellmanDeObj(obj), 2)+ " , ";
            textoCercanos +=strCdCercano;
            // Fin de descripción de un CD cercano      
            textoCercanos += "\r\n";
        }
        result = textoCD + textoCercanos;
        return result;
    }
}
