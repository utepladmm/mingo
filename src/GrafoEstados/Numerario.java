/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GrafoEstados;

import java.io.Serializable;

/**
 *
 * @author ut469262
 */
public class Numerario implements Serializable {
    private String nombre;
    private String descripcion;
    private Double tasaDescuento;
	/**
	 * tasa de descuento de ese numerario: hoy se aplica ahora
	 * hay una misma tasa para todos los numerarios
	 *
	 */

	public Numerario(String nombre, String descripcion, Double tasaDescuento) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.tasaDescuento = tasaDescuento;
	}

	public Numerario(String nombre) {
		this.nombre = nombre;
	}


	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getTasaDescuento() {
		return tasaDescuento;
	}

	public void setTasaDescuento(Double tasaDescuento) {
		this.tasaDescuento = tasaDescuento;
	}


    @Override
    public String toString() {
        String texto = "";
        texto += "Nombre del numerario: " + nombre + "\n";
        texto += "Descripción del numerario: " + descripcion + "\n";
		texto += "Tasa de descuento del numerario: " + tasaDescuento + "\n";
        return texto;
    }


}
