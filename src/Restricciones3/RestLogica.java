/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Restricciones3;

import GrafoEstados.Portafolio;
import dominio.RecOTrans;
import dominio.RecursoBase;
import dominio.TiempoAbsoluto;
import java.util.ArrayList;
import naturaleza3.NodoN;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 *
 * La forma es
 * - cuantificados: TODOS ALGUNO NINGUNO de un
 * - conjunto de RecursosBase en el parque que....
 * - operador: DEBEN_EXISTIR (en algún Recurso del parque) para el inicio de
 * - estado: los estados OPE y CON de Recursos.
 * - RecursosBase o TransforBase (objeto) en la decisión de inicio de construcción
 *   que está condicionado.
 *
 *
 *
 */
public class RestLogica {
    public TiempoAbsoluto tIni;
    public TiempoAbsoluto tFin;
    /**
     * Primer y último tiempo absoluto en que valen las restricciones
     * si se omiten las restricciones valen siempre
     */
    public String cuantificador;
    public ArrayList<RecursoBase> recEnParque;
    public String estado;
    public RecOTrans objetoCondicionado;

    /**
     * Los cuantificadores pueden ser los siguientes
     */
    public static final String todos = "TODOS";
    public static final String alguno = "ALGUNO";
    public static final String ninguno = "NINGUNO";
    /**
     * Los estados pueden ser de la clase EstadosRec
     */



    /**
     * Devuelve false si la restriccion no se cumple
     * Devuelve true si se cumple con o sin holgura
     */
     public boolean evaluar(ArrayList<Object> informacion,
             Portafolio p, Portafolio d, NodoN n) throws XcargaDatos {
          boolean result = true;





          return result;
     }




}
