/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Restricciones3;

import GrafoEstados.Portafolio;
import GrafoEstados.RestGrafoE;
import UtilitariosGenerales.CalculadorDePlazos;
import dominio.CantProductoSimple;
import dominio.Caso;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import dominio.Parque;
import dominio.Producto;
//import dominio.Bloque.EstadosRec;
import dominio.TiempoAbsoluto;
import java.io.Serializable;
import naturaleza3.NodoN;
import persistenciaMingo.XcargaDatos;

import java.util.ArrayList;

/**
 *
 * @author ut469262
 *
 * Las restricciones de producto se definen así:
 *
 * oferta del producto de generadores(t)
 *			mayor que
 * (proporcionInf(t)) x (requerimiento de producto de las demandas +
 *                           requerimiento de producto de las cargas)
 *
 * oferta del producto de generadores(t)
 *			menor que
 * (proporcionSup(t)) x (requerimiento de producto de las demandas +
 *                           requerimiento de producto de las cargas)
 *
 * tiempoAbsolutoBase es el tiempo absoluto que aparece en el get(0)
 * en los ArrayList de proporcions superior e inferior.
 *
 * La proporcion se define para cada paso de tiempo.
 *
 * ATENCIÓN: ES OBLIGATORIO QUE PARA CADA PRODUCTO EL USARIO ESTABLEZCA
 * RESTRICCIONES DE PRODUCTO INFERIOR Y SUPERIOR.
 *
 */
public class RestDeProducto3 extends RestGrafoE implements Serializable {

     private Producto producto;
     private Estudio estudio;
     private InfSup infSup;
     private ArrayList<Double> proporcion;     
     /**
      * La proporcion se define para cada paso de tiempo
      * Se conviene que el get(0) corresponde al paso 1.
      */
     
     // Los dos siguientes son objetos de trabajo de la clase
     private static CantProductoSimple oferta;
     private static CantProductoSimple requerimiento;     
    
     
     static {
         oferta = new CantProductoSimple();
         requerimiento = new CantProductoSimple();    
     }
     
     public enum InfSup {

          INF, // la restricción establece una cota inferior a la cantidad de producto ofertada de un parque
          SUP   // la restricción establece una cota superior a la cantidad de producto ofertada de un parque
     }

     public RestDeProducto3(Producto producto, Estudio estudio, boolean esHeu) {
          this.producto = producto;
          this.estudio = estudio;
          esHeuristica = esHeu;
     }

     public void setProporcion(ArrayList<Double> proporcion) {
          this.proporcion = proporcion;
     }

     public ArrayList<Double> getProporcion() {
          return proporcion;
     }

     public InfSup getInfSup() {
          return infSup;
     }

     public void setInfSup(InfSup infSup) {
          this.infSup = infSup;
     }

     public Producto getProducto() {
          return producto;
     }

     public void setProducto(Producto producto) {
          this.producto = producto;
     }

     /**
      * Devuelve el factor proporcion para el tiempo absoluto ta
      * @param ta
      * @return
      */
     public double proporcionDeTa(TiempoAbsoluto ta) throws XcargaDatos {
          int paso = estudio.getDatosGenerales().pasoDeTiempoAbsoluto(ta);
          double prop = proporcion.get(paso - 1);
          return prop;
     }

     /**
      * Devuelve la menor cantidad de producto simple ofertado admisible
      * @param cantPDem cantidad de producto de las demandas.
      * @param ta tiempoAbsoluto a considerar.
      * @return
      */
     public CantProductoSimple ofertaMinDeDem(CantProductoSimple cantPDem,
             TiempoAbsoluto ta) throws XcargaDatos {
          if (infSup != InfSup.INF) {
               throw new XcargaDatos("Se pidió oferta mínima" +
                       "admisible para una restricción de cota superior de producto");
          }

          double prop = proporcionDeTa(ta);
          CantProductoSimple minCantOferta = cantPDem.clona();
          minCantOferta.escalarCant(prop);
          return minCantOferta;
     }

//**********************************************************************
// INICIO DE METODOS SOBREESCRITOS DE LA CLASE RestGrafoE
//**********************************************************************

//     /**
//      * ATENCION USA BLOQUES
//      * Compara la oferta de cantidad de producto que hacen
//      * los generadores operativos, con el requerimiento de demandas y otras
//      * cargas operativas y devuelve resultado:
//      * true si la restricción se cumple
//      * false si la restricción no se cumple
//      *
//      * No se emplea el NodoN porque las restricciones de producto
//      * se plantearon provisoriamente independientes del estado
//      * de la naturaleza
//      * Igualmente no se emplea el portadolio d
//      */
//     @Override
//     public boolean evaluar(ArrayList<Object> informacion,
//             Portafolio p, Portafolio d, NodoN n) throws XcargaDatos {
//
//          Estudio est = (Estudio) informacion.get(0);
//          DatosGeneralesEstudio datGen = est.getDatosGenerales();
//          Caso cas = (Caso) informacion.get(1);
//
//          boolean resultado = false;
//          double maxCantGen = 0.0;
//          double maxCantCar = 0.0;
//          Parque par = (Parque) p;
//
//
//          TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(p.getEtapa());
//
//          int etapa = datGen.etapaDeUnPaso(datGen.pasoDeTiempoAbsoluto(ta));
//
//          /**
//           * Las cantidades máximas de un generador y de una carga
//           * se determinan para usarlas en la prueba para ver si se
//           * está en la frontera.
//           */
//          CantProductoSimple oferta = new CantProductoSimple(producto);
//          CantProductoSimple requerimiento = new CantProductoSimple(producto);
//
//          datGen = est.getDatosGenerales();
//
//          oferta = par.cantProdGenParque(producto, "OPE", informacion);
//          requerimiento = par.reqProdCargasParque(producto, "OPE", informacion);
//          int iprod = datGen.getProductos().indexOf(producto);
//
//          CantProductoSimple cantDem = est.getConjDemandas().cantProdsDemanda(n, etapa, est).get(iprod);
//          requerimiento.sumaCant(cantDem);
//
////		for (Demanda d: est.getConjDemandas().getDemandas()){
////			String valorVADem = est.getGrafoN().valorVariableNat(n, d.getVAleatoria());
////			CantProductoSimple cantd;
////			cantd = d.cantProd(producto, datGen, ta, valorVADem).clona();
////			requerimiento.sumaCant(cantd);
////		}
//
//
//          double prop = proporcion.get(datGen.getCalculadorDePlazos().hallaAvanceTiempo((TiempoAbsoluto) tiempoAbsolutoBase, ta));
//          /**
//           *  OJO UNA ALTERNATIVA A LO DE ABAJO ES USAR OPERADORES DE AGREGAR A PARQUE
//           *  Y QUITAR A PARQUE RECURSOS
//           */
//          CantProductoSimple reqProporcion;
//          reqProporcion = requerimiento.clona();
//          reqProporcion.escalarCant(prop);
//          /**
//           * reqProporcion es la cantidad de producto del requermiento incrementada
//           * en la proporción de la holgura
//           */
//          if (infSup == InfSup.INF) {
//               /**
//                *  Es una restricción que impone oferta mayor que una proporción
//                *  del requerimiento
//                */
//               if (oferta.comparaCant(reqProporcion) > 0) {
//                    resultado = true;
//               } else {
//                    resultado = false;
//               }
//
//          } else {
//               /**
//                * es una restricción que impone oferta menor que una proporción del requerimiento
//                */
//               if (oferta.comparaCant(reqProporcion) < 0) {
//                    resultado = true;
//               } else {
//                    resultado = false;
//               }
//          }
//          return resultado;
//     }
     
     
     /**
      * Compara la oferta de cantidad de producto que hacen
      * los generadores operativos, con el requerimiento de demandas y otras
      * cargas operativas y devuelve resultado:
      * true si la restricción se cumple
      * false si la restricción no se cumple.
      * 
      * ATENCIÓN: NO USA BLOQUES.
      *
      * No se emplea el NodoN porque las restricciones de producto
      * se plantearon provisoriamente independientes del estado
      * de la naturaleza
      * 
      * No se emplea el portafolio d
      */
     @Override
     public boolean evaluar(ArrayList<Object> informacion,
             Portafolio p, Portafolio d, NodoN n) throws XcargaDatos {

          Estudio est = (Estudio) informacion.get(0);
          DatosGeneralesEstudio datGen = est.getDatosGenerales();
          Caso cas = (Caso) informacion.get(1);

          boolean resultado = false;
          Parque par = (Parque) p;
          TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(p.getEtapa());
          int etapa = datGen.etapaDeUnPaso(datGen.pasoDeTiempoAbsoluto(ta));

          /**
           * Las cantidades máximas de un generador y de una carga
           * se determinan para usarlas en la prueba para ver si se
           * está en la frontera.
           */
          oferta.setProducto(producto);
          oferta.setCant(0.0);          
          requerimiento.setProducto(producto);
          requerimiento.setCant(0.0);                    
          datGen = est.getDatosGenerales();
          oferta = par.cantProdGenParqueCE(producto, "OPE", informacion);
          requerimiento = par.reqProdCargasParqueCE(producto, "OPE", informacion);
          CantProductoSimple cantDem = est.getConjDemandas().cantDeUnProd(producto, n, etapa, est);
          requerimiento.sumaCant(cantDem);

//		for (Demanda d: est.getConjDemandas().getDemandas()){
//			String valorVADem = est.getGrafoN().valorVariableNat(n, d.getVAleatoria());
//			CantProductoSimple cantd;
//			cantd = d.cantProd(producto, datGen, ta, valorVADem).clona();
//			requerimiento.sumaCant(cantd);
//		}


          double prop = proporcion.get(datGen.getCalculadorDePlazos().hallaAvanceTiempo((TiempoAbsoluto) tiempoAbsolutoBase, ta));
          /**
           *  OJO UNA ALTERNATIVA A LO DE ABAJO ES USAR OPERADORES DE AGREGAR A PARQUE
           *  Y QUITAR A PARQUE RECURSOS
           */
          CantProductoSimple reqProporcion;
          reqProporcion = requerimiento.clona();
          reqProporcion.escalarCant(prop);
          /**
           * reqProporcion es la cantidad de producto del requermiento incrementada
           * en la proporción de la holgura
           */
          if (infSup == InfSup.INF) {
               /**
                *  Es una restricción que impone oferta mayor que una proporción
                *  del requerimiento
                */
               if (oferta.comparaCant(reqProporcion) > 0) {
                    resultado = true;
               } else {
                    resultado = false;
               }

          } else {
               /**
                * es una restricción que impone oferta menor que una proporción del requerimiento
                */
               if (oferta.comparaCant(reqProporcion) < 0) {
                    resultado = true;
               } else {
                    resultado = false;
               }
          }
          return resultado;
     }
          
     
     
     
     
     
     
     
     
     

    @Override
     public RestGrafoE pasaAEtapas() {
          return (this);
     }

//**********************************************************************
// FIN DE METODOS A SOBREESCRIBIR POR LAS RESTRICCIONES DE CADA TIPO
//**********************************************************************
     @Override
     public String toString() {
          String texto = "\r\n" + "// RESTRICCION DE PRODUCTO" + "\r\n" +
                  "&PRODUCTO ";
          texto += "\r\n";


          texto += "NOMBRE " + producto.getNombre().toString();
          texto += "\r\n";
          texto += "ES_HEURISTICA ";
          if (esHeuristica) {
               texto += "SI";
          } else {
               texto += "NO";
          }
          texto += "\r\n";
          texto += "COTA" + "\t";
          if (infSup == InfSup.INF) {
               texto += "inferior";
          } else {
               texto += "superior";
          }
          texto += "\r\n";
          texto += "TIEMPO_ABSOLUTO" + "\t";

          CalculadorDePlazos calc = estudio.getDatosGenerales().getCalculadorDePlazos();
          for (int i = 0; i < proporcion.size(); i++) {
               texto += calc.hallaTiempoAbsoluto((TiempoAbsoluto) tiempoAbsolutoBase, i).toStringCorto();
               texto += "\t";
          }
          texto += "\r\n";

          texto += "PROPORCION" + "\t";

          for (int i = 0; i < proporcion.size(); i++) {
               texto += proporcion.get(i);
               texto += "\t";
          }
          texto += "\r\n";
          texto += "&FIN";
          texto += "\r\n";
          return texto;
     }
}



