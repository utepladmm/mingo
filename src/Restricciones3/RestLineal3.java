/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Restricciones3;

import GrafoEstados.RestGrafoE;
import GrafoEstados.Portafolio;
import TiposEnum.EstadosRec;
import UtilitariosGenerales.CalculadorDePlazos;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import dominio.Parque;
//import dominio.Bloque.EstadosRec;
import dominio.RecOTrans;
import dominio.Recurso;
import dominio.RecursoBase;
import dominio.TiempoAbsoluto;
import dominio.TransforBase;
import dominio.Transformacion;
import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.NodoN;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class RestLineal3 extends RestGrafoE implements Serializable{
    /**
     * Restricción lineal en las cantidades de RecursosBase o TransforBase
     * de un parque.
     *
     *
     */
    private Estudio estudio;
    private boolean esEntera;
    /**
     * si esEntera es true los coeficientes y el término independiente son enteros
     */
    private boolean esAditiva;
    /**
     * si esAditiva es true los términos independientes se suman al pasar a etapas
     * si no los términos independientes no se suman
     */
    private ArrayList<Object> objetos;
    // Cada elemento de objetos es un RecursoBase o TransforBase.
    private String estadosRec;
    /**
     * Estados a los que se aplica una restricción. Se consideran los objetos
     * del portafolio (parque) que pertenezcan a los estados establecidos por esta variable.
     * Si estadosRec es TODOS, se consideran todos los estados posibles.
     */

    private ArrayList<Integer> coefEnteros;
    private ArrayList<Double> coefReales;
    /**
     * Coeficientes de la restricción lineal según sea entera o real asociados
     * a los objetos, en el orden en que aparecen en el ArrayList<Object> objetos.
     * Los coeficientes son constantes en el tiempo
     * Las variables de la restricción son las cantidades (multiplicadores enteros)
     * de los objetos en los bloques del parque
     */
    private ArrayList<Integer> tindEntero;
    private ArrayList<Double> tindReal;
    /**
     * Términos independientes de la restricción lineal según sea entera o real
     * que varian por paso de tiempo a partir del tiempoAbsolutoBase de la restricción
     * que es un atributo de la clase padre RestGrafoE3
     * el get(0) corresponde al paso de tiempo o etapa 1.
     */
    private Operador operador;




    public enum Operador{
        MAYOR,
        MAYOR_O_IGUAL,
        MENOR,
        MENOR_O_IGUAL,
        IGUAL;
    }

    public RestLineal3(Estudio est) {
        estudio = est;
        objetos = new ArrayList<Object>();
        coefEnteros = new ArrayList<Integer>();
        coefReales = new ArrayList<Double>() ;
        tindEntero = new ArrayList<Integer>();
        tindReal = new ArrayList<Double>();
        pasoEtapa = "PASO";
    }



    public ArrayList<Integer> getCoefEnteros() {
        return coefEnteros;
    }

    public void setCoefEnteros(ArrayList<Integer> coefEnteros) {
        this.coefEnteros = coefEnteros;
    }

    public ArrayList<Double> getCoefReales() {
        return coefReales;
    }

    public void setCoefReales(ArrayList<Double> coefReales) {
        this.coefReales = coefReales;
    }

    public boolean isEsEntera() {
        return esEntera;
    }

    public void setEsEntera(boolean esEntera) {
        this.esEntera = esEntera;
    }

    public String getEstadosRec() {
        return estadosRec;
    }

    public void setEstadosRec(String estadosRec) {
        this.estadosRec = estadosRec;
    }


    public ArrayList<Object> getObjetos() {
        return objetos;
    }

    public void setObjetos(ArrayList<Object> objetos) {
        this.objetos = objetos;
    }

    public Operador getOperador() {
        return operador;
    }

    public void setOperador(Operador operador) {
        this.operador = operador;
    }


    public boolean isEsAditiva() {
        return esAditiva;
    }

    public void setEsAditiva(boolean esAditiva) {
        this.esAditiva = esAditiva;
    }

    public ArrayList<Integer> getTindEntero() {
        return tindEntero;
    }

    public void setTindEntero(ArrayList<Integer> tindEntero) {
        this.tindEntero = tindEntero;
    }

    public ArrayList<Double> getTindReal() {
        return tindReal;
    }

    public void setTindReal(ArrayList<Double> tindReal) {
        this.tindReal = tindReal;
    }

    public Estudio getEstudio() {
        return estudio;
    }

    public void setEstudio(Estudio estudio) {
        this.estudio = estudio;
    }

    public ArrayList<Object> getInformacion() {
        return informacion;
    }





    /**
     * Devuelve el término independiente del paso p
     * si la restricción es entera
     * @param p es el paso del que se busca el término independiente
     * @return result es el término independiente
     */
    public int tindDePasoEnt(int p){
        int result = 0;
        result = tindEntero.get(p-1);
        return result;
    }

    /**
     * Devuelve el término independiente del paso p
     * si la restricción es real
     * @param p es el paso del que se busca el término independiente
     * @return result es el término independiente
     */        
    public double tindDePasoReal(int p){
        double result = 0;
        result = tindReal.get(p-1);
        return result;
    }


    /**
     * Crea una copia de la restricción corriente (this) que puede alterarse
     * sin modificar el original. NO COPIA LOS CAMPOS violaciones y cota.
     * @return copia
     */
    public RestLineal3 copiaRestLineal(){
       // campos de RestGrafoE
       RestLineal3 copia = new RestLineal3(estudio);
       copia.setNombre(this.getNombre());
       copia.setInformacion(informacion);
       copia.setPasoEtapa(pasoEtapa);
       copia.setEsHeuristica(esHeuristica);
       copia.setTiempoAbsolutoBase(tiempoAbsolutoBase);
       // campos de RestCota3 adicionales
       copia.setEsEntera(esEntera);
       copia.setEstadosRec(estadosRec);
       copia.getCoefEnteros().addAll(coefEnteros);
       copia.getCoefReales().addAll(coefReales);
       copia.setEsAditiva(esAditiva);
       copia.setObjetos(objetos);
       copia.setOperador(operador);
       return copia;
    }



//**********************************************************************
// INICIO DE METODOS SOBREESCRITOS DE LA CLASE RestGrafoE
//**********************************************************************


//    /**
//     * Devuelve true si la restriccion se cumple
//     * Devuelve false si la restricción no se cumple
//     * El Portafolio d y el NodoN n provisoriamente no se emplean.
//     * 
//     * ATENCION ESTA USA BLOQUES
//     */
//    @Override
//   public boolean evaluar(ArrayList<Object> informacion,
//			Portafolio p, Portafolio d, NodoN n)throws XcargaDatos {      
//        Estudio est = (Estudio)informacion.get(0);
//        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(p.getEtapa());        
//        boolean result = false;
//        int indice;
//        int sumaAportesEnt = 0;
//        double sumaAportesReal = 0.0;
//        int coefEnt = 0;
//        double coefReal = 0.0;
//        int tIndEnt = 0;
//        double tIndReal = 0.0;
//        DatosGeneralesEstudio datGen = est.getDatosGenerales();
//        int paso =  datGen.pasoDeTiempoAbsoluto(ta);
//        if(pasoEtapa.equalsIgnoreCase(PASO)){
//            indice = paso-1;
//        }else{
//            indice = datGen.etapaDeUnPaso(paso)-1;
//        }
//        if(esEntera){
//            tIndEnt = tindEntero.get(indice);
//        }else{
//            tIndReal = tindReal.get(indice);
//        }
//        Parque par = (Parque)p;
//        for(int io = 0; io<objetos.size(); io++){
//            Object rBoTBRest = objetos.get(io);
//            if(esEntera){
//                coefEnt = coefEnteros.get(io);
//            } else{
//                coefReal = coefReales.get(io);
//            }
//            for(Bloque b: par.bloquesDelParque(est)){
//                int multip = b.getMultiplicador();
//                boolean igualRBoTB = false;
//                if(b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC) && rBoTBRest instanceof RecursoBase){
//                    Recurso recb = (Recurso) b.getObjeto();
//                    RecursoBase rBb = recb.getRecBase();
//                    if(rBb == rBoTBRest) igualRBoTB = true;
//                }else if(b.getTipoBloque().equalsIgnoreCase(TipoBloque.TRANS) && rBoTBRest instanceof TransforBase){
//                    Transformacion transb = (Transformacion) b.getObjeto();
//                    TransforBase tBb = transb.getTransBase();
//                    if(tBb == rBoTBRest) igualRBoTB = true;
//                }
//                if (igualRBoTB && ((b.estadoDeRecOTrans().equalsIgnoreCase(estadosRec)) ||
//                        (estadosRec.equalsIgnoreCase(EstadosRec.TODOS)))) {
//                    if(esEntera){
//                        sumaAportesEnt += multip*coefEnt;
//                    }else{
//                        sumaAportesReal += multip*coefReal;
//                    }
//                }
//            }
//        }
//        result = false;
//        if(esEntera){
//            if(sumaAportesEnt == tIndEnt
//                && (operador == Operador.MAYOR_O_IGUAL ||
//                    operador == Operador.MENOR_O_IGUAL ||
//                    operador == Operador.IGUAL)           ){
//                result = true;
//                return result;
//            }else if(sumaAportesEnt > tIndEnt
//                && (operador == Operador.MAYOR || operador == Operador.MAYOR_O_IGUAL)  ){
//                result = true;
//                return result;
//            }else if(sumaAportesEnt < tIndEnt
//                && (operador == Operador.MENOR || operador == Operador.MENOR_O_IGUAL)  ){
//                result = true;
//                return result;
//            }
//        }else{
//            // La restricción no es entera
//            if(sumaAportesReal == tIndReal
//                && (operador == Operador.MAYOR_O_IGUAL ||
//                    operador == Operador.MENOR_O_IGUAL ||
//                    operador == Operador.IGUAL)           ){
//                result = true;
//                return result;
//            }else if(sumaAportesReal > tIndReal
//                && (operador == Operador.MAYOR || operador == Operador.MAYOR_O_IGUAL)  ){
//                result = true;
//                return result;
//            }else if(sumaAportesReal < tIndReal
//                && (operador == Operador.MENOR || operador == Operador.MENOR_O_IGUAL)  ){
//                result = true;
//                return result;
//            }
//        }
//        return result;
//    }
// 
    
    /**
     * Devuelve true si la restriccion se cumple
     * Devuelve false si la restricción no se cumple
     * El Portafolio d y el NodoN n provisoriamente no se emplean.
     * 
     * NO USA BLOQUES
     */
    @Override
   public boolean evaluar(ArrayList<Object> informacion,
			Portafolio p, Portafolio d, NodoN n)throws XcargaDatos {      
        Estudio est = (Estudio)informacion.get(0);
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(p.getEtapa());        
        boolean result = false;
        int indice;
        int sumaAportesEnt = 0;
        double sumaAportesReal = 0.0;
        int coefEnt = 0;
        double coefReal = 0.0;
        int tIndEnt = 0;
        double tIndReal = 0.0;
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int paso =  datGen.pasoDeTiempoAbsoluto(ta);
        if(pasoEtapa.equalsIgnoreCase(PASO)){
            indice = paso-1;
        }else{
            indice = datGen.etapaDeUnPaso(paso)-1;
        }
        if(esEntera){
            tIndEnt = tindEntero.get(indice);
        }else{
            tIndReal = tindReal.get(indice);
        }
        Parque par = (Parque)p;
        for(int io = 0; io<objetos.size(); io++){
            Object rBoTBRest = objetos.get(io);
            if(esEntera){
                coefEnt = coefEnteros.get(io);
            } else{
                coefReal = coefReales.get(io);
            }
            for(int ind=0; ind< est.getCantRecNoComunesYTransPosibles();ind++){
                boolean debeSumar = false;
                RecOTrans rot = est.recNoComunYTransPosible(ind);
                int multip = par.codigoDeInd(ind);
                if(multip>0){
                    if (rot instanceof Recurso){
                        RecursoBase rbInd = ((Recurso)rot).getRecBase();
                        if( rbInd==rBoTBRest  && 
                             ( rot.getEstado().equalsIgnoreCase(estadosRec)   || 
                               estadosRec.equalsIgnoreCase(EstadosRec.TODOS) )   ){
                            debeSumar = true;
                        }
                    }
                    if(rot instanceof Transformacion){
                        TransforBase tbInd = ((Transformacion)rot).getTransBase();
                        if( tbInd==rBoTBRest  && 
                             ( rot.getEstado().equalsIgnoreCase(estadosRec)   || 
                               estadosRec.equalsIgnoreCase(EstadosRec.TODOS) )   ){
                            debeSumar = true;
                        }
                    }
                }    
                if(debeSumar){
                    if(esEntera){
                        sumaAportesEnt += multip*coefEnt;
                    }else{
                        sumaAportesReal += multip*coefReal;
                    }
                }
                
            }
        }                
        result = false;
        if(esEntera){
            if(sumaAportesEnt == tIndEnt
                && (operador == Operador.MAYOR_O_IGUAL ||
                    operador == Operador.MENOR_O_IGUAL ||
                    operador == Operador.IGUAL)           ){
                result = true;
                return result;
            }else if(sumaAportesEnt > tIndEnt
                && (operador == Operador.MAYOR || operador == Operador.MAYOR_O_IGUAL)  ){
                result = true;
                return result;
            }else if(sumaAportesEnt < tIndEnt
                && (operador == Operador.MENOR || operador == Operador.MENOR_O_IGUAL)  ){
                result = true;
                return result;
            }
        }else{
            // La restricción no es entera
            if(sumaAportesReal == tIndReal
                && (operador == Operador.MAYOR_O_IGUAL ||
                    operador == Operador.MENOR_O_IGUAL ||
                    operador == Operador.IGUAL)           ){
                result = true;
                return result;
            }else if(sumaAportesReal > tIndReal
                && (operador == Operador.MAYOR || operador == Operador.MAYOR_O_IGUAL)  ){
                result = true;
                return result;
            }else if(sumaAportesReal < tIndReal
                && (operador == Operador.MENOR || operador == Operador.MENOR_O_IGUAL)  ){
                result = true;
                return result;
            }
        }
        return result;
    }   
        
        

    /**
     * Pasa a una restricción lineal definida en los pasos a etapas
     * Son posible dos modalidades:
     *
     * aditiva: el término independiente de la etapa e es la suma de los términos
     * independientes de cada uno de los pasos entre el paso representativo de la
     * etapa e-1 inclusive y el paso anterior al representativo de la etapa e.

     * no aditiva: los términos independientes no se suman, la restricción aplicada
     * a una etapa es la restricción del paso de tiempo representativo de la etapa.
     */
    @Override
    public RestGrafoE pasaAEtapas(){

        RestLineal3 resNue = (this).copiaRestLineal();
        resNue.getTindEntero().clear();
        resNue.getTindReal().clear();
        
        DatosGeneralesEstudio datGen = estudio.getDatosGenerales();
        int cantEtapas = datGen.getCantEtapas();

        double sumaReal = 0.0;
        int sumaEnt = 0;

        for(int e=1; e<= cantEtapas; e++){
            if(esAditiva){
                sumaReal = 0.0;
                sumaEnt = 0;
                int limSupIter;
                if(e==cantEtapas) {
                    limSupIter = datGen.pasoRepDeEtapa(e);
                }else{
                    limSupIter = datGen.pasoRepDeEtapa(e+1);
                }
                for (int p= datGen.pasoRepDeEtapa(e); p < datGen.pasoRepDeEtapa(e+1); p++){
                    if(esEntera){
                        sumaEnt += tindDePasoEnt(p);
                    }else{
                        sumaReal += tindDePasoReal(p);
                    }
                    if(esEntera){
                        resNue.getTindEntero().add(sumaEnt);
                    }else{
                        resNue.getTindReal().add(sumaReal);
                    }
                }
            }else{
                // no es aditiva
                if(esEntera){
                    resNue.getTindEntero().add(tindDePasoEnt(datGen.pasoRepDeEtapa(e)));
                }else{
                    resNue.getTindReal().add(tindReal.get(datGen.pasoRepDeEtapa(e)));
                }
            }
        }
        return resNue;
    }



//**********************************************************************
// FIN DE METODOS A SOBREESCRIBIR POR LAS RESTRICCIONES DE CADA TIPO
//**********************************************************************



    @Override
	public String toString() {
		String texto = "\r\n" + "// RESTRICCION LINEAL" + "\r\n";
        texto += "&LINEAL";
        texto += "\r\n";
        texto += "NOMBRE" + "\t" + nombre;
		texto += "\r\n";
        texto += "ES_HEURISTICA" + "\t";
        if(esHeuristica){
            texto += "SI";
        }else{
            texto += "NO";
        }
 		texto += "\r\n";
        if(esEntera){
            texto += "SI";
        }else{
            texto += "NO";
        }
 		texto += "\r\n";
        texto += "OBJETOS" + "\t";
        for(Object ob: objetos){
            if(ob instanceof RecursoBase){
                RecursoBase rb = (RecursoBase)ob;
                texto += rb.getNombre() + "\t";
            }
            if(ob instanceof TransforBase){
                TransforBase tb = (TransforBase)ob;
                texto += tb.getNombre() + "\t";
            }
        }
 		texto += "\r\n";
        texto += "COEF" + "\t";
        if(esEntera){
            for (int e: coefEnteros){
                texto += e + "\t";
            }
        }else{
            for (double e: coefReales){
                texto += e + "\t";
            }
        }
 		texto += "\r\n";
        texto += "OPERADOR"+ "\t";
        if(operador==Operador.IGUAL){
            texto += "IGUAL";
        }else if(operador==Operador.MENOR){
            texto += "MENOR";
        }else if(operador==Operador.MENOR_O_IGUAL){
            texto += "MENOR_O_IGUAL";
        }else if(operador==Operador.MENOR){
            texto += "MAYOR";
        }else if(operador==Operador.MENOR_O_IGUAL){
            texto += "MAYOR_O_IGUAL";
        }
        texto += "\r\n" + "TIEMPO_ABSOLUTO"+ "\t";

        CalculadorDePlazos calc = estudio.getDatosGenerales().getCalculadorDePlazos();
        for (int i = 0; i<estudio.getDatosGenerales().getCantPasos();i++){
            texto += calc.hallaTiempoAbsoluto((TiempoAbsoluto)tiempoAbsolutoBase,i).toStringCorto();
            texto += "\t";
        }
        
        texto += "\r\n";
        texto += "TIND" + "\t";
        if(esEntera){
            for(int j=0; j<tindEntero.size(); j++){
                texto += tindEntero.get(j)+ "\t";
            }
        }else{
            for(int j=0; j<tindReal.size(); j++){
                texto += tindReal.get(j)+ "\t";
            }
        }
        texto += "\r\n";
		texto += "&FIN";
		texto += "\r\n";
		return texto;
	}

}
