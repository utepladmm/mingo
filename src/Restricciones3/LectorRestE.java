/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Restricciones3;

import AlmacenSimulaciones.ImplementadorGeneral;
import GrafoEstados.ConjRestGrafoE;
import GrafoEstados.RestGrafoE;
import ImplementaEDF.ImplementadorEDF;
import UtilitariosGenerales.DirectoriosYArchivos;
import dominio.Caso;
import dominio.ConjRecB;
import dominio.ConjTransB;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import persistenciaMingo.CargaRestCota3;
import persistenciaMingo.CargaRestDeProducto3;
import persistenciaMingo.CargaRestLineal3;
import persistenciaMingo.XcargaDatos;

import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ut469262
 */
public class LectorRestE {

    /**
     * - Lee las restricciones usando los métodos que están en el
     *   paquete persistenciaMingo y las carga en los conjuntos de restricciones
     *   que reciben como parámetros los métodos de esta misma clase.
     *
     * - Publica (inicialmente en texto) el estado corriente de las
     *   restricciones heurísticas de parque.
     *
     * - Recibe los cambios (inicialmente desde el mismo texto
     *   en que se publica el estado corriente) de las restricciones heurísticas
     *   de parque.
     *
     * - ESTA CLASE NO ALMACENA LAS RESTRICCIONES EN ESTRUCTURAS PROPIAS
     *
     */
// ***********************************************************************************
//
//  METODOS PARA LEER Y GRABAR CONJUNTOS DE RESTRICCIONES    
//
// ***********************************************************************************
    /**
     * METODO cargaRestH
     *
     * Carga el conjunto de restricciones heurísticas iniciales o
     * lee el archivo donde se grabaron las restricciones
     * heurísticas corrientes, que el usuario ha corregido
     * para la siguiente iteración
     *
     * El archivo inicial se conserva.
     *
     * Guarda el conjunto de restricciones leídas al final de sucConjsRH
     *
     * @param sucConjsRH es la sucesión de restricciones heurísticas
     * las nuevas restricciones leídas se almacenan al final de la lista
     *
     * @param dirRest es el directorio del caso, donde se encuentran los archivos
     * de restricciones
     *
     * @param conjRB es el conjunto de RecursosBase del estudio sobre el que
     * se establecerán las restricciones
     *
     * @param conjTB es el conjunto de RecursosBase del estudio sobre el que
     * se establecerán las restricciones
     *
     * @param estudio es el Estudio asociado
     */
    public static void cargaRestH(ArrayList<ConjRestGrafoE> sucConjsRH,
            String dirRest, String tipo, ConjRecB conjRB, ConjTransB conjTB,
            Estudio estudio) throws XcargaDatos, IOException {

        System.out.print("INICIA LECTURA DE RESTRICCIONES HEURISTICAS" + "\r\n");

        int iter = sucConjsRH.size();
        String pathArchivo;
        ConjRestGrafoE cRGE = new ConjRestGrafoE();
        sucConjsRH.add(cRGE);
        if(tipo.equalsIgnoreCase(Caso.PD)){
            if (iter == 0) {
                pathArchivo = dirRest + "/V3-RestHeuIniciales.txt";
            } else {
                pathArchivo = dirRest + "/V3-RestHeuCorr.txt";
            }
        }else{
            if (iter == 0) {
                pathArchivo = dirRest + "/V3-RestAlGenCotaIniciales.txt";
            } else {
                pathArchivo = dirRest + "/V3-RestAlGenCotaCorr.txt";
            }            
        }
        try {
            cargaConjuntoR(sucConjsRH.get(iter), pathArchivo,
                    conjRB, conjTB,
                    estudio);
            for (RestGrafoE rest : sucConjsRH.get(iter).getRestricciones()) {
                rest.setEsHeuristica(true);
            }
        } catch (XcargaDatos ex) {
            int selec = JOptionPane.showOptionDialog(null, "OCURRIÓ UN ERROR EN LA LECTURA DE RESTRICCIONES HEURÍSTICAS" +
                    "Revise el archivo de restricciones heurísticas",
                    "título de la ventana", JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null,
                    new Object[]{"Detener", "Seguir"}, null);
            // Devuelve 0 o 1 según la opción
        }
    }

    /**
     * Carga el conjunto de restricciones conjRest desde el archivo dirArchivo
     * Las restricciones se cargan ya pasadas a etapas.
     *
     * @param conjRest es el ConjRestGrafoE donde se cargan las restricciones leídas
     * y pasadas a etapas.
     *
     * @param pathArchivo es el path del archivo de texto desde donde se leen.
     *
     * @param conjRB es el conjunto de RecursosBase del estudio sobre el que
     * se establecerán las restricciones.
     *
     * @param conjTB es el conjunto de RecursosBase del estudio sobre el que
     * se establecerán las restricciones.
     */
    public static void cargaConjuntoR(ConjRestGrafoE conjRest, String pathArchivo,
            ConjRecB conjRB, ConjTransB conjTB, Estudio estudio) throws XcargaDatos {

        ConjRestGrafoE conjRestAux = new ConjRestGrafoE();
        DatosGeneralesEstudio datgen = estudio.getDatosGenerales();

        CargaRestCota3 cargaCota = new CargaRestCota3() {
        };
        cargaCota.cargaRestCota(pathArchivo, conjRB, conjTB, estudio, conjRestAux);

        CargaRestDeProducto3 cargaRProd = new CargaRestDeProducto3();
        cargaRProd.cargarRestDeProducto(pathArchivo, estudio, conjRestAux);

        CargaRestLineal3 cargaRLin = new CargaRestLineal3();
        cargaRLin.cargarRestLineal(pathArchivo, conjRB, conjTB, estudio, conjRestAux);

        for (RestGrafoE restPaso : conjRestAux.getRestricciones()) {
            RestGrafoE rest = new RestGrafoE();
            rest = restPaso.pasaAEtapas();
            rest.setPasoEtapa("ETAPA");
            rest.setEsHeuristica(false); // para las restricciones heurísticas se carga posteriormente true
            conjRest.getRestricciones().add(rest);
        }
    }

    /**
     * Crea un archivo con las restricciones de un ConjRestGrafoE
     *
     * @param pathArchivo es el camino completo con nombre del archivo
     *
     * @param encabezado es el texto con marca de comentario // que aparece en
     * el encabezado
     *
     * @param conjR es el conjunto de restricciones a guardar en el archivo
     */
    public static void publicaConjRest(String pathArchivo, String encabezado,
            ConjRestGrafoE conjR) throws IOException, XcargaDatos {
        String texto = "// " + encabezado + "\r\n";
        texto += conjR.toString();
        DirectoriosYArchivos grabaA = new DirectoriosYArchivos();
        DirectoriosYArchivos.grabaTexto(pathArchivo, texto);
    }

    /**
     * Crea un archivo con las restricciones heurísticas
     * de parque corrientes para que sea sobreescrito
     *
     * @param conjRH es el conjunto de restricciones heurísticas a imprimir
     *
     * @param dirRest es el directorio del caso, donde se grabará el archivo
     *
     * @param iter es el número de iteración empezando en cero de la sucesión
     * de restricciones heurísticas
     */
    public static void publicaRestHProgDin(ConjRestGrafoE conjRH, String dirRest, int iter) throws IOException, XcargaDatos {
        String nomarch = "/V3-RestHeuCorr.txt";
        String texto = "// RESTRICCIONES HEURÍSTICAS DE PARQUE - ITERACION " + iter;
        texto += "\r\n";
        texto += conjRH.toString();

        String dirArchivo = dirRest + nomarch;
        DirectoriosYArchivos grabaA = new DirectoriosYArchivos();
        DirectoriosYArchivos.grabaTexto(dirArchivo, texto);
    }

//************************************************************************************
//************************************************************************************
    public static void main(String[] args) throws XcargaDatos, IOException {

        String dirBase = "D:/Java/PruebaJava4";
        String dirEstudio = dirBase + "/" + "Datos_Estudio";
        String dirImplementacion = dirBase + "/" + "Datos_Implementacion";
        String dirCorridas = dirBase + "/" + "corridas";
        ImplementadorGeneral impG = new ImplementadorEDF();
        impG.inicializaImplementador(dirImplementacion, dirEstudio, dirCorridas);

        Estudio est = new Estudio("Estudio de prueba", 5, impG);
        String dirEst = "D:/Java/PruebaJava3";
        est.leerEstudio(dirEst, true);

        cargaConjuntoR(est.getConjRestSigParque(),
                dirEst + "/V3-RestSigParque.txt",
                est.getConjRecB(), est.getConjTransB(), est);
        cargaConjuntoR(est.getConjRestSigInicio(),
                dirEst + "/V3-RestSigInicio.txt",
                est.getConjRecB(), est.getConjTransB(), est);

        String dirCaso = dirEst + "/CASO1";

        publicaConjRest(dirEst + "/volcadoRestSigInicio.txt", "Rest sig inicio", est.getConjRestSigInicio());
        publicaConjRest(dirEst + "/volcadoRestSigParque.txt", "Rest sig parque", est.getConjRestSigParque());


        Caso caso = new Caso(est, "Caso 1");
        cargaRestH(caso.getSucRestHParque(),
                dirEst + "/CASO1", Caso.PD,
                est.getConjRecB(), est.getConjTransB(),
                est);
        int selec = JOptionPane.showOptionDialog(null, "PUEDE CAMBIAR RESTRICCIONES HEURISTICAS " +
                "- PULSE SEGUIR CUANDO TERMINE EL CAMBIO",
                "Opcion para cambiar restricciones heurísticas", JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null,
                new Object[]{"SEGUIR"}, null);
        publicaRestHProgDin(caso.getSucRestHParque().get(0), dirEst + "/CASO1", 0);

    }
}
