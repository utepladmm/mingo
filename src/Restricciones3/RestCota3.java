/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Restricciones3;

import GrafoEstados.PeriodoDeTiempo;
import GrafoEstados.RestGrafoE;
import GrafoEstados.Portafolio;

import Restricciones3.RestDeProducto3.InfSup;
import TiposEnum.EstadosRec;
import TiposEnum.TipoBloque;
import UtilitariosGenerales.CalculadorDePlazos;
import dominio.Bloque;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import dominio.Parque;
//import dominio.Bloque.EstadosRec;
import dominio.RecOTrans;
import dominio.Recurso;
import dominio.RecursoBase;
import dominio.TiempoAbsoluto;
import dominio.TransforBase;
import dominio.Transformacion;
import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.NodoN;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 *
 * Restricciones de mayor o igual (cota INF) o de menor o igual (cota SUP)
 * a la cantidad de RecursosBase o TranforBase de un cierto tipo y que
 * se encuentran en cierto estado de la variable enumerada EstadosRec
 *
 * Como las cantidades de esos objetos son enteras, las restricciones son de
 * mayor o igual o menor o igual, es decir el conjunto factible es cerrado
 */
public class RestCota3 extends RestGrafoE implements Serializable {

    private Estudio estudio;
    private String tipoBloque; // REC o TRANS
    private Object objeto;
    /**
     * objeto es un RecursoBase o una TransforBase
     */
    private String infSup;
    /**
     * Cota superior o inferior
     */
    public static final String INF = "INF";    
    public static final String SUP = "SUP";        
    
    private String estadosRec;
    /**
     * Estados a los que se aplica una restricción. Se consideran los objetos
     * del portafolio (parque) que pertenezcan a los estados establecidos por esta variable
     */
    private ArrayList<Integer> cota;

    /**
     * Cota superior o inferior por paso de tiempo o etapa
     * El get(0) corresponde al paso 1 o etapa 1.
     */
    public RestCota3() {
    }

    public RestCota3(Estudio estudio, String tipoBloque, Object objeto) {
        this.estudio = estudio;
        this.tipoBloque = tipoBloque;
        this.objeto = objeto;
        cota = new ArrayList<Integer>();
    }

    public RestCota3(boolean esHeuristica, String pasoEtapa,
            ArrayList<Object> informacion,
            PeriodoDeTiempo tiempoAbsolutoBase,
            Object objeto, Estudio est, InfSup infSup, String estado) {
        super(esHeuristica, pasoEtapa, informacion, tiempoAbsolutoBase);
        this.objeto = objeto;
        if (objeto instanceof RecursoBase) {
            tipoBloque.equalsIgnoreCase(TipoBloque.REC);
        } else {
            tipoBloque.equalsIgnoreCase(TipoBloque.TRANS);
        }
        estudio = est;
        cota = new ArrayList<Integer>();
        pasoEtapa = PASO;
    }

    public ArrayList<Integer> getCota() {
        return cota;
    }

    public void setCota(ArrayList<Integer> cota) {
        this.cota = cota;
    }

    public String getEstadosRec() {
        return estadosRec;
    }

    public void setEstadosRec(String estadosRec) {
        this.estadosRec = estadosRec;
    }

    public String getTipoBloque() {
        return tipoBloque;
    }

    public void setTipoBloque(String tipoBloque) {
        this.tipoBloque = tipoBloque;
    }

    public String getInfSup() {
        return infSup;
    }

    public void setInfSup(String infSup) {
        this.infSup = infSup;
    }

    public Object getObjeto() {
        return objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }

    public Estudio getEstudio() {
        return estudio;
    }

    public void setEstudio(Estudio estudio) {
        this.estudio = estudio;
    }

    /**
     * Devuelve la cota del paso p
     * @param p es el paso del que se desea la cota
     * @return cotaPaso es la cota del paso
     */
    public int cotaDePaso(int p) {
        int cotaPaso = 0;
        cotaPaso = cota.get(p - 1);
        return cotaPaso;
    }

    /**
     * Crea una copia de la restricción corriente (this) que puede alterarse
     * sin modificar el original. NO COPIA LOS CAMPOS violaciones y cota.
     * @return copia
     */
    public RestCota3 copiaRestCota3() {
        // campos de RestGrafoE
        RestCota3 copia = new RestCota3(estudio, tipoBloque, objeto);
        copia.setNombre(nombre);
        copia.setInformacion(informacion);
        copia.setPasoEtapa(pasoEtapa);
        copia.setEsHeuristica(esHeuristica);
        copia.setTiempoAbsolutoBase(tiempoAbsolutoBase);
        // campos de RestCota3 adicionales
        copia.setInfSup(infSup);
        copia.getCota().addAll(cota);
        copia.setEstadosRec(estadosRec);
        return copia;
    }

    /**
     * METODO cotaDeTiempoAbsoluto
     * @param ta TiempoAbsoluto para el que se quiere la cota
     * @return result es la cota de las restricción
     */
    public int cotaDeTiempoAbsoluto(TiempoAbsoluto ta) throws XcargaDatos {
        int result = 0;
        DatosGeneralesEstudio datGen = estudio.getDatosGenerales();
        int paso = datGen.pasoDeTiempoAbsoluto(ta);
        if (pasoEtapa.equalsIgnoreCase(PASO)) {
            result = cotaDePaso(paso);
            return result;
        } else {
            // las restricción ya está pasada a etapas
            int etapa = datGen.etapaDeUnPaso(paso);
            result = cotaDePaso(etapa);
        }
        return result;
    }

//**********************************************************************
// INICIO DE METODOS SOBREESCRITOS DE LA CLASE RestGrafoE
//**********************************************************************
//    @Override
//    /**
//     * ATENCION ESTA ES LA VIEJA EVALUAR QUE USABA BLOQUES
//     * El portafolios d no se emplea, se considera la cota sobre el p
//     */
//    public boolean evaluar(ArrayList<Object> informacion,
//            Portafolio p, Portafolio d, NodoN n) throws XcargaDatos {
//        /**
//         * Devuelve false si la restriccion no se cumple
//         * Devuelve true si se cumple con o sin holgura
//         */
//        Estudio est = (Estudio)informacion.get(0);
//
//        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(p.getEtapa());
//        boolean result = false;
//        int indice;
//        int sumaAportes = 0;
//
//        DatosGeneralesEstudio datGen = est.getDatosGenerales();
//        int paso = datGen.pasoDeTiempoAbsoluto(ta);
//        if (pasoEtapa.equalsIgnoreCase("PASO")) {
//            indice = paso - 1;
//        } else {
//            indice = datGen.etapaDeUnPaso(paso) - 1;
//        }
//        Parque par = (Parque) p;
//
//        String estadoOb;
//        for (Bloque b : par.bloquesDelParque(est)) {
//            int multip = b.getMultiplicador();
//            String tipoOb = b.getTipoBloque();
//            if (tipoOb.equalsIgnoreCase("REC") && tipoBloque.equalsIgnoreCase("REC")) {
//                estadoOb = b.getRecurso().getEstado();
//                Recurso recb = (Recurso) b.getObjeto();
//                RecursoBase rBb = recb.getRecBase();
//                if (    rBb == objeto && (  estadoOb.equalsIgnoreCase(estadosRec) ||
//                        (estadosRec.equalsIgnoreCase("TODOS"))  )  ) {
//                    sumaAportes += multip;
//                }
//            } else if (tipoOb.equalsIgnoreCase("TRANS") && tipoBloque.equalsIgnoreCase("TRANS")) {
//                estadoOb = b.getTransformacion().getEstado();
//                Transformacion transb = (Transformacion) b.getObjeto();
//                TransforBase tBb = transb.getTransBase();
//                if (tBb == objeto && ((estadoOb.equalsIgnoreCase(estadosRec)) || (estadosRec.equalsIgnoreCase("TODOS")))) {
//                    sumaAportes += multip;
//                }
//            }
//        }
//        if (infSup.equalsIgnoreCase("INF")) {
//            if (sumaAportes >= cota.get(indice)) {
//                result = true;
//            } else {
//                result = false;
//            }
//        } else {
//            // la acotación es superior
//            if (sumaAportes <= cota.get(indice)) {
//                result = true;
//            } else {
//                result = false;
//            }
//        }
//        return result;
//    }

    
    @Override
    /**
     * Evalúa la restricción con el portafolio p. 
     * El portafolios d no se emplea, se considera la cota sobre el p.
     * 
     * NO USA BLOQUES !!!!
     */
    public boolean evaluar(ArrayList<Object> informacion,
            Portafolio p, Portafolio d, NodoN n) throws XcargaDatos {
        /**
         * Devuelve false si la restriccion no se cumple
         * Devuelve true si se cumple con o sin holgura
         */
        Estudio est = (Estudio)informacion.get(0);        
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(p.getEtapa());
        boolean result = false;
        int indice;
        int sumaAportes = 0;
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int paso = datGen.pasoDeTiempoAbsoluto(ta);
        if (pasoEtapa.equalsIgnoreCase(PASO)) {
            indice = paso - 1;
        } else {
            indice = datGen.etapaDeUnPaso(paso) - 1;
        }
        Parque par = (Parque) p;       
        for(int ind = 0; ind<est.getCantRecNoComunesYTransPosibles(); ind++){            
            if(par.codigoDeInd(ind)>0){
                RecOTrans rot = est.recNoComunYTransPosible(ind);
                String estadoInd = rot.getEstado();
                // ind<est.getCantRecNoComunes() quiere decir que es tipo REC
                if(ind<est.getCantRecNoComunes() && tipoBloque.equalsIgnoreCase(TipoBloque.REC)){
                    // En el lugar de ind hay un Recurso y la restricción es sobre un RecursoBase
                    Recurso rec = (Recurso)rot;
                    RecursoBase rb = rec.getRecBase();
                    if( rb==objeto  && 
                        (estadoInd.equalsIgnoreCase(estadosRec) ||
                         estadosRec.equalsIgnoreCase(EstadosRec.TODOS))  ) 
                         sumaAportes = sumaAportes + par.codigoDeInd(ind);                    
                }
                if(ind>=est.getCantRecNoComunes() && tipoBloque.equalsIgnoreCase(TipoBloque.TRANS)){
                    // En el lugar de ind hay una Transformacion y la restricción es sobre una TransforBase
                    Transformacion trans = (Transformacion)rot;
                    TransforBase tb = trans.getTransBase();
                    if( tb==objeto  && 
                        (estadoInd.equalsIgnoreCase(estadosRec) ||
                         estadosRec.equalsIgnoreCase(EstadosRec.TODOS))  ) 
                         sumaAportes = sumaAportes + par.codigoDeInd(ind);                    
                }                
            }            
        }
        if (infSup.equalsIgnoreCase(INF)) {
            if (sumaAportes >= cota.get(indice)) {
                result = true;
            } else {
                result = false;
            }
        } else {
            // la acotación es superior
            if (sumaAportes <= cota.get(indice)) {
                result = true;
            } else {
                result = false;
            }
        }
        return result;
    }
    
    
    
    
    @Override
    /**
     * La cota asociada al pasaje del paso representativo de la etapa e (pre)
     * al paso representativo de la etapa siguiente e+1 (premas1) es la suma de las cotas de
     * cada uno de los pasos de la etapa e.
     */
    public RestGrafoE pasaAEtapas() {
        RestCota3 resNue = (this).copiaRestCota3();
        resNue.getCota().clear(); // Se borran las cotas anteriores por paso.
        resNue.setPasoEtapa("ETAPA");
        DatosGeneralesEstudio datGen = estudio.getDatosGenerales();
        int cantEtapas = datGen.getCantEtapas();
        int suma = 0;
        for (int e = 1; e < cantEtapas; e++) {
            // la última etapa tiene un sólo paso y va aparte
            suma = 0;
            for (int p = datGen.pasoRepDeEtapa(e); p < datGen.pasoRepDeEtapa(e + 1); p++) {
                suma += cotaDePaso(p);
            }
            resNue.getCota().add(suma);
        }
        // se carga la última etapa
        resNue.getCota().add(cotaDePaso(datGen.pasoRepDeEtapa(cantEtapas)));
        return resNue;
    }

//**********************************************************************
// FIN DE METODOS A SOBREESCRIBIR POR LAS RESTRICCIONES DE CADA TIPO
//**********************************************************************
    @Override
    public String toString() {
        String texto = "\r\n" + "// RESTRICCION DE INICIO DE CONSTRUCCION DE COTA" + "\r\n";
        if (tipoBloque.equalsIgnoreCase("REC")) {
            texto += "&RECURSO_BASE ";
        } else {
            texto += "&TRANSFORMACION_BASE";
        }
        texto += "\r\n";
        if (tipoBloque.equalsIgnoreCase("REC")) {
            texto += "NOMBRE" + "\t";
            RecursoBase rb = (RecursoBase) objeto;
            texto += rb.getNombre();
        } else {
            TransforBase tb = (TransforBase) objeto;
            texto += tb.getNombre();
        }
        texto += "\r\n";
        texto += "ES_HEURISTICA ";
        if (esHeuristica) {
            texto += "SI";
        } else {
            texto += "NO";
        }
        texto += "\r\n";
        texto += "ESTADOS_REC ";
        texto += estadosRec.toString();
        texto += "\r\n";
        texto += "TIEMPO_ABSOLUTO ";
        CalculadorDePlazos calc = estudio.getDatosGenerales().getCalculadorDePlazos();
        for (int i = 0; i < estudio.getDatosGenerales().getCantPasos(); i++) {
            texto += calc.hallaTiempoAbsoluto((TiempoAbsoluto) tiempoAbsolutoBase, i).toStringCorto();
            texto += "\t";
        }
        texto += "\r\n";
        if (!(cota.isEmpty())) {
            if (infSup.equalsIgnoreCase("SUP")) {
                texto += "COTA_SUPERIOR" + "\t";
            } else {
                texto += "COTA_INFERIOR" + "\t";
            }
            for (int i = 0; i < cota.size(); i++) {
                texto += cota.get(i);
                texto += "\t";
            }
            texto += "\r\n";
        }
        texto += "&FIN";
        texto += "\r\n";
        return texto;
    }
}
