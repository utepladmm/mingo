/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package AlgoritmoGenetico;

import java.util.ArrayList;

/**
 *
 * @author Usuario
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        int cant = UtilitariosProgGen.cantBitsDeEntero(63);
        System.out.println("64  " + cant);
        cant = UtilitariosProgGen.cantBitsDeEntero(200);
        System.out.println("200  " + cant);
        cant = UtilitariosProgGen.cantBitsDeEntero(4);
        System.out.println("4  " + cant);


        ArrayList<Integer> auxInt = new ArrayList<Integer>();
        auxInt.add(6);
        auxInt.add(12);
        IndividuoEntero.setCotasEnteras(auxInt);
        System.out.print(IndividuoEntero.toStringClaseIndEnt());

        IndividuoEntero indEnt = new IndividuoEntero();
        indEnt.agregaCodigoEnt(0);
        indEnt.agregaCodigoEnt(9);

        System.out.print("Individuo entero originel " + "\r\n"+ indEnt.toString() + "\r\n");

        IndividuoBin indBin = IndividuoEntero.indBinDeEntero(indEnt);
        indEnt.cargaIndividuoBin();
        System.out.print(indBin.toString() + "\r\n");

        IndividuoEntero indEnt2 = IndividuoEntero.indEntDeBinario(indBin);
        System.out.print("Individuo entero 2" + "\r\n" + indEnt2.toString()+ "\r\n");

        IndividuoEntero hijoEnt = IndividuoEntero.cruzaSimple(indEnt, indEnt2);
        System.out.print("Hijo " + "\r\n" + hijoEnt.toString()+ "\r\n");

        IndividuoEntero indEnt1Mut = indEnt.mutacionDeIndEnt();
        System.out.print("IndEnt1Mut = IndEnt1 mutado " + "\r\n" + indEnt1Mut.toString()+ "\r\n");

        IndividuoEntero indEnt1Mut2 = indEnt1Mut.mutacionDeIndEnt();
        System.out.print("IndEnt1Mut2 = IndEnt1 mutado 2 veces" + "\r\n" + indEnt1Mut2.toString()+ "\r\n");

        IndividuoEntero indEnt1Mut3 = indEnt1Mut2.mutacionDeIndEnt();
        System.out.print("IndEnt1Mut3 = IndEnt1 mutado 3 veces" + "\r\n" + indEnt1Mut3.toString()+ "\r\n");


    }
}
