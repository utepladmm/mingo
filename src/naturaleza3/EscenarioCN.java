/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaleza3;

import java.util.ArrayList;

/**
 *
 * @author ut469262
 *
 * EscenarioCN es una sucesión de conjsNodosN que constituye un escenario
 * de evolución de variables de la naturaleza.
 *
 */
public class EscenarioCN {

    private BolsaDeConjsNodosN bolsa;
    private int numesc; // número índice del escenario empezando en 1
    private double prob; //probabilidad del escenario
    private int cantEtapas; // cantidad de etapas adicionales a la etapa 0
    private ArrayList<ConjNodosN> conjsNodosN;
    /**
     * El primer ConjNodosN del escenario corresponde a la etapa y paso 0.
     */

    public EscenarioCN(BolsaDeConjsNodosN bolsa) {
        this.bolsa = bolsa;
        conjsNodosN = new ArrayList<ConjNodosN>();
    }

    public BolsaDeConjsNodosN getBolsa() {
        return bolsa;
    }

    public void setBolsa(BolsaDeConjsNodosN bolsa) {
        this.bolsa = bolsa;
    }

    public double getProb() {
        return prob;
    }

    public void setProb(double prob) {
        this.prob = prob;
    }

    public int getCantEtapas() {
        return cantEtapas;
    }

    public void setCantEtapas(int cantEtapas) {
        this.cantEtapas = cantEtapas;
    }

    public ArrayList<ConjNodosN> getConjsNodosN() {
        return conjsNodosN;
    }

    public void setConjsNodosN(ArrayList<ConjNodosN> conjsNodosN) {
        this.conjsNodosN = conjsNodosN;
    }

    public void setConjInfo(ArrayList<ConjNodosN> conjsNodosN) {
        this.conjsNodosN = conjsNodosN;
    }

    public int getNumesc() {
        return numesc;
    }

    public void setNumesc(int numesc) {
        this.numesc = numesc;
    }


    /**
     * Devuelve el ConjNodosN de la etapa etapa del EscenarioCN this.
     * @param etapa es la etapa elegida
     * @return
     */
    public ConjNodosN conjNodosNdeEtapa(int etapa){
        return conjsNodosN.get(etapa);

    }


    public ArrayList<EscenarioCN> generarHijos(BolsaDeConjsNodosN bolsa,
            ArrayList<ConjNodosN> conjs,
            ArrayList<Double> probSucs) {
        /**
         * conjs es una lista de ConjInfo sucesores del último Conjunto
         * del EscenarioCN corriente (this)
         *
         * proba es la lista de probabilidades de cada sucesor en el mismo orden
         */
        ArrayList<EscenarioCN> hijos = new ArrayList<EscenarioCN>();
        int ih = 0;
        for (ConjNodosN csuc : conjs) {
            double prob1S = probSucs.get(ih);
            EscenarioCN escSuc = new EscenarioCN(bolsa);
            escSuc.setCantEtapas(cantEtapas + 1);
            escSuc.getConjsNodosN().addAll(conjsNodosN);
            escSuc.getConjsNodosN().add(csuc);
            escSuc.setProb(prob * prob1S);
            hijos.add(escSuc);
        }
        return hijos;
    }

    @Override
    public String toString() {
        String texto = this.toStringCorto();
        texto += "Valores de las variables observadas";
        texto += "\n";
        int iv = 0;
        for (VariableNat vn : bolsa.getVariablesObservadas()) {
            for (int ie = 0; ie <= cantEtapas; ie++) {
                if (ie == 0) {
                    texto += "-" + "\t";
                } else {
                    texto += conjsNodosN.get(ie).getValoresObservadas().get(iv);
                    texto += "\t";
                }
            }
            iv++;
            texto += "\n";
        }
        texto += "Valores de las variables que determinan parque";
        texto += "\n";
        iv = 0;
        for (VariableNat vn : bolsa.getVariablesDetP()) {
            for (int ie = 0; ie <= cantEtapas; ie++) {
                texto += conjsNodosN.get(ie).getValoresDetP().get(iv);
                texto += "\t";
            }
            iv++;
            texto += "\n";
        }
        return texto;
    }

    public String toStringCorto() {
        GrafoN grafo = bolsa.getGrafoN();
        String texto = "COMIENZA ESCENARIO " + numesc + "\n";
        texto += "Ordinales de Conjs " + "\n";
        texto += "etapas    " + "\t";
        int ie;
        for (ie = 0; ie <= cantEtapas; ie++) {
            texto += ie + "\t";
        }
        texto += "\n";
        texto += "pasos     " + "\t";
        for (ie = 0; ie <= cantEtapas; ie++) {
            texto += grafo.pasoDeUnaEtapa(ie) + "\t";
        }
        texto += "\n";
        texto += "ordinales " + "\t";
        for (ie = 0; ie <= cantEtapas; ie++) {
            texto += conjsNodosN.get(ie).getOrdinal() + "\t";
        }
        texto += "\n";
        return texto;
    }
}
