/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaleza3;

import dominio.DatosGeneralesEstudio;
import persistenciaMingo.CargaDatosGenerales;
import persistenciaMingo.XcargaDatos;

import java.io.Serializable;

/**
 *
 * @author ut469262
 */
public class MatrizTransicion implements Serializable {

    private int etapaIni;
    private int etapaFin;
    private int cantFilas;
    private int cantColumnas;
    private GrafoN grafo;
    private boolean[][] matSuc;   // matriz de sucesores
    private double[][] matProb;  // matriz de probabilidades de transición

    /** el primer índice (fila)
     *  corresponde a la etapa etapa y el segundo índice (columna=
     *  corresponde a la etapa siguiente (etapa+1)
     */
    /**
     * Atención: la matriz no queda cargada, sólo se crea.
     * @param grafo
     * @param etapa
     * @param cantFilas
     * @param cantColumnas
     */
    public MatrizTransicion(GrafoN grafo, int etapa, int cantFilas, int cantColumnas) {
        this.grafo = grafo;
        this.cantFilas = cantFilas;
        this.cantColumnas = cantColumnas;
        etapaIni = etapa;
        etapaFin = etapa + 1;
        matSuc = new boolean[cantFilas][cantColumnas];
        matProb = new double[cantFilas][cantColumnas];
    }

    /**
     * Atención: la matriz no queda cargada, sólo se crea.
     * @param grafo
     * @param etapaIni
     * @param etapaFin
     * @param cantFilas
     * @param cantColumnas
     */
    public MatrizTransicion(GrafoN grafo, int etapaIni, int etapaFin, int cantFilas, int cantColumnas) {
        this.grafo = grafo;
        this.cantFilas = cantFilas;
        this.cantColumnas = cantColumnas;
        this.etapaIni = etapaIni;
        this.etapaFin = etapaFin;
        matSuc = new boolean[cantFilas][cantColumnas];
        matProb = new double[cantFilas][cantColumnas];
    }

    /**
     *
     * Atención: la matriz no queda cargada, sólo se crea.
     * @param grafo
     * @param etapa
     */
    public MatrizTransicion(GrafoN grafo, int etapa) {
        this.grafo = grafo;
        etapaIni = etapa;
        etapaFin = etapa + 1;
    }

    public int getEtapaIni() {
        return etapaIni;
    }

    public void setEtapaIni(int etapa) {
        etapaIni = etapa;
    }

    public int getEtapaFin() {
        return etapaFin;
    }

    public void setEtapaFin(int etapa) {
        etapaFin = etapa;
    }

    public int getCantColumnas() {
        return cantColumnas;
    }

    public void setCantColumnas(int cantColumnas) {
        this.cantColumnas = cantColumnas;
    }

    public int getCantFilas() {
        return cantFilas;
    }

    public void setCantFilas(int cantFilas) {
        this.cantFilas = cantFilas;
    }

    public GrafoN getGrafo() {
        return grafo;
    }

    public void setGrafo(GrafoN grafo) {
        this.grafo = grafo;
    }

    public double[][] getMatProb() {
        return matProb;
    }

    public void setMatProb(double[][] matProb) {
        this.matProb = matProb;
    }

    public boolean[][] getMatSuc() {
        return matSuc;
    }

    public void setMatSuc(boolean[][] matSuc) {
        this.matSuc = matSuc;
    }

    /**
     * METODO calculaMatrizEtapa
     *
     * Calcula las probabilidades de transición del grafo asociado
     * de la etapa etapa (o paso) a la etapa siguiente si esta existe
     *
     * @param etapa es la etapa (o paso) para la que se calcula la matriz
     * hasta la siguiente etapa
     */
    public void calculaMatrizEtapa(int etapa) {


        try {
            if (etapa == grafo.getCantEtapas()) {
                throw new XcargaDatos("Se pide matriz de transicion" +
                        "a partir de la última etapa en grafo " + grafo.getNombre());
            } else {
                cantFilas = grafo.getNodos().get(etapa).size();
                cantColumnas = grafo.getNodos().get(etapa + 1).size();
                matSuc = new boolean[cantFilas][cantColumnas];
                matProb = new double[cantFilas][cantColumnas];
                int i = 0;
                for (NodoN n : grafo.getNodos().get(etapa)) {
                    int j = 0;
                    for (NodoN m : grafo.getNodos().get(etapa + 1)) {
                        // si el j-esimo nodo de etapa + 1 no es sucesor del i-esimo de etapa
                        // la probabilidad se asigna igual a 0
                        matSuc[i][j] = false;
                        matProb[i][j] = 0.0;
                        int isuc = 0;
                        for (NodoN suc : n.getSucesores()) {
                            if (suc == m) {
                                matSuc[i][j] = true;
                                matProb[i][j] = grafo.getNodos().get(etapa).get(i).getProbSucesores().get(isuc);
                            }
                            isuc++;
                        }
                        j++;
                    }
                    i++;
                }
            }

        } catch (XcargaDatos e) {
            System.out.println(e.getDescripcion());
        }

    }

    /**
     * METODO calculaMatrizEtapas
     *
     * Calcula las probabilidades de transición del grafo asociado
     * de la etapa (o paso) etapaIni a la etapa etapaFin si esta existe
     * La matriz tiene que tener cargado el grafoN al que corresponde
     * y creadas sus matrices matSuc y matProb en el constructor
     *
     * @param etapaIni es la etapa inicial de la matriz
     *
     * @param etapaFin es la etapa final de la matriz
     *
     */
    public void calculaMatrizEtapas(int etapaIni, int etapaFin) {
        this.etapaIni = etapaIni;
        this.etapaIni = etapaIni;
        try {
            if (etapaIni == grafo.getCantEtapas()) {
                throw new XcargaDatos("ERROR: Se pide matriz de transicion " +
                        "a partir de la última etapa en grafo " + grafo.getNombre());
            } else if (etapaIni >= etapaFin) {
                throw new XcargaDatos("ERROR: Se pide matriz de transicion" +
                        "con etapa final menor o igual a la inicial en grafo " + grafo.getNombre());
            } else {
                cantFilas = grafo.getNodos().get(etapaIni).size();
                cantColumnas = grafo.getNodos().get(etapaFin).size();
                matSuc = new boolean[cantFilas][cantColumnas];
                matProb = new double[cantFilas][cantColumnas];
                MatrizTransicion matrizAux = new MatrizTransicion(grafo, etapaIni);
                matrizAux.calculaMatrizEtapa(etapaIni);
                for (int ie = etapaIni + 1; ie < etapaFin; ie++) {
                    MatrizTransicion matrizAdic = new MatrizTransicion(grafo, ie);
                    matrizAdic.calculaMatrizEtapa(ie);
                    MatrizTransicion matrizComp;
                    matrizComp = matrizAux.componeConSiguiente(matrizAdic);
                    matrizAux = matrizComp;

                }
                for (int ifil = 1; ifil <= cantFilas; ifil++) {
                    for (int icol = 1; icol <= cantColumnas; icol++) {
                        matSuc[ifil - 1][icol - 1] = matrizAux.getMatSuc()[ifil - 1][icol - 1];
                        matProb[ifil - 1][icol - 1] = matrizAux.getMatProb()[ifil - 1][icol - 1];
                    }
                }

            }
        } catch (XcargaDatos e) {
            System.out.println(e.getDescripcion());
        }

    }

    /**
     * METODO componeConSiguiente
     *
     * Calcula la matriz compuesta de aplicar la matriz (this) y otra matriz
     * (this) y matrizSig tienen que ser coherentes en dimensiones y etapas,
     * de lo contrario da error.
     * La matriz original y la matriz siguiente matSig pueden ser de una o más etapas
     * el resultado queda en la matriz matComp que se crea
     *
     * @param matrizSig es la matriz siguiente con la que se compone (this)
     */
    public MatrizTransicion componeConSiguiente(MatrizTransicion matrizSig) {


        MatrizTransicion matComp = new MatrizTransicion(grafo, etapaIni, matrizSig.getEtapaFin(),
                cantFilas, matrizSig.getCantColumnas());
        try {
            if (cantColumnas != matrizSig.getCantFilas()) {
                throw new XcargaDatos("Se quiere componer matrices de dimension distinta" +
                        " en grafo " + grafo.getNombre());

            } else if (etapaFin != matrizSig.getEtapaIni()) {
                throw new XcargaDatos("Se quiere componer matrices de etapas no sucesivas" +
                        " en grafo " + grafo.getNombre());
            } else if (grafo != matrizSig.getGrafo()) {
                throw new XcargaDatos("Se quiere componer matrices de grafos distintos" +
                        " Grafos: " + grafo.getNombre() + " y " + matrizSig.getGrafo().getNombre());
            } else {
                // ACA HAY DOS PRODUCTOS MATRICIALES EN ARRAYS QUE HABRIA QUE TRAER DE OTRO LADO
                for (int ifil = 1; ifil <= cantFilas; ifil++) {
                    for (int icol = 1; icol <= cantColumnas; icol++) {
                        matComp.getMatSuc()[ifil - 1][icol - 1] = false;
                        matComp.getMatProb()[ifil - 1][icol - 1] = 0.0;
                        for (int ik = 1; ik <= matrizSig.getCantFilas(); ik++) {
                            matComp.getMatSuc()[ifil - 1][icol - 1] = matComp.getMatSuc()[ifil - 1][icol - 1] ||
                                    (matSuc[ifil - 1][ik - 1] && matrizSig.getMatSuc()[ik - 1][icol - 1]);
                            matComp.getMatProb()[ifil - 1][icol - 1] +=
                                    matProb[ifil - 1][ik - 1] * matrizSig.getMatProb()[ik - 1][icol - 1];
                        }
                    }

                }
            }
        } catch (XcargaDatos e) {
            System.out.println(e.getDescripcion());
        }
        return matComp;
    }

    @Override
    public String toString() {
        String texto = "M";
        for (int r = 1; r < 100; r++) {
            texto += "=";
        }
        texto += "\n";
        texto += "Matriz de sucesores del grafo " + grafo.getNombre() + "\n";
        texto += "Etapa inicial: " + etapaIni + "   Etapa final: " + etapaFin + "\n";
        for (int ifil = 1; ifil <= matSuc.length; ifil++) {
            texto += "ordinal nodo inicial = " + ifil + "  Sucesores (marcados con 1): ";
            for (int icol = 1; icol <= matSuc[ifil - 1].length; icol++) {
                if (matSuc[ifil - 1][icol - 1]) {
                    texto += " 1 ";
                } else {
                    texto += " 0 ";
                }
            }
            texto += "\n";
        }
        texto += "Matriz de probabilidades de transicion del grafo " + grafo.getNombre() + "\n";
        texto += "Etapa inicial: " + etapaIni + "   Etapa final: " + etapaFin + "\n";
        for (int ifil = 1; ifil <= matProb.length; ifil++) {
            texto += "ordinal nodo inicial = " + ifil + "probs. transición: ";
            for (int icol = 1; icol <= matProb[ifil - 1].length; icol++) {
                texto += matProb[ifil - 1][icol - 1] + "    ";

            }
            texto += "\n";
        }
        return texto;

    }

    public static void main(String[] args) throws XcargaDatos {

        DatosGeneralesEstudio datGen1 = new DatosGeneralesEstudio();
        CargaDatosGenerales cargadorDG = new CargaDatosGenerales() {
        };
        String dirDatGen = "D:/Java/PruebaJava/V2-DatosGenerales.txt";
        try {
            cargadorDG.cargarDatosGen(dirDatGen, datGen1);
        } catch (XcargaDatos ex) {
            System.out.println("------ERROR-------");
            System.out.println(ex.getDescripcion());
            System.out.println("------------------\n");

        }

        GrafoN grafoNaturaleza = new GrafoN();
        CargaGrafoN2 cargaNat = new CargaGrafoN2() {
        };

        String dirArchivo = "D:/Java/PruebaJava2/V8-GrafoNaturaleza.txt";
        int cantPasos = datGen1.getCantPasos();
        grafoNaturaleza = cargaNat.cargarGrafoN(dirArchivo, cantPasos, true);
        MatrizTransicion matPrueba1 = new MatrizTransicion(grafoNaturaleza, 3);
        matPrueba1.calculaMatrizEtapa(3);
        String texto = matPrueba1.toString();
        System.out.print(texto);
        int filas3 = grafoNaturaleza.getNodos().get(3).size();
        int cols5 = grafoNaturaleza.getNodos().get(5).size();
        MatrizTransicion matPrueba2 = new MatrizTransicion(grafoNaturaleza, 3, 5, filas3, cols5);
        matPrueba2.calculaMatrizEtapas(3, 5);
        texto = matPrueba2.toString();
        System.out.print(texto);


    }
}


