/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaleza3;

import dominio.DatosGeneralesEstudio;
import java.util.ArrayList;
import naturaleza3.Componente2.TipoComp;
import naturaleza3.ConjNodosN.TConjNodoN;
import persistenciaMingo.CargaDatosGenerales;
import persistenciaMingo.LeerDatosArchivo;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut600232
 */
public class CargaGrafoN2 {

     /**
      * DERIVADA DE CargaGrafoN pero la observabilidad es de todo el componente
      */
     public enum TipoCargaCDP {

          TODAS, // todas las componentes de cierta clase
          DETP, // sólo las componentes de la clase que determinan parque
          NODETP;     // sólo las componentes de la clase que no determinan parque
     }

     /**
      * Crea un GrafoN a partir de los datos de dirArchivo.
      * Construye las bolsas bolsaCI y bolsaCDP de conjuntos de información y conjuntos
      * que determinan parque del grafo.
      *
      * Las componentes leídas deben tener nodos datados en cada uno de
      * los pasos de tiempo del estudio
      * Los arcos van entre nodos de pasos de tiempo consecutivos
      *
      * El tratamiento por bloques de pasos de tiempo debe ser posterior a la lectura
      * de los datos de cada componente
      *
      * impDet = true ordena impresión detallada de la generación del grafo por componente
      */
     public GrafoN cargarGrafoN(String dirArchivo, int cantPasos,
             boolean impDet) throws XcargaDatos {

          CargaComponente2 cargaC = new CargaComponente2() {
          };
          ArrayList<Componente2> componentes;
          ArrayList<Componente2> componentesIndep;
          ArrayList<Componente2> componentesDep;

          componentes = new ArrayList<Componente2>();
          componentesIndep = new ArrayList<Componente2>();
          componentesDep = new ArrayList<Componente2>();

          /* Carga de las componentes del grafo. */
          try {
               componentes = cargaC.cargarComponentes(dirArchivo);
          } catch (Exception e) {
               System.out.println("*/*/*/*/*/*/* ERROR */*/*/*/*/*/*\n");
               System.out.println(e.toString() + "\n");
               System.out.println("*/*/**/*/*/*/*/*/*/*/*/*/**/*/*/*\n");
          }
          boolean hayNoObsCDPIndep = false;
          /**
           * true si hay al menos una componente no observada que cambia
           * el parque, que debe ser independiente necesariamente
           */
          // Asignación de las componentes independientes
          for (Componente2 c : componentes) {
               if (c.getTipo() == TipoComp.Indep) {
                    componentesIndep.add(c);
               }
          }

          // Asignación de las componentes dependientes
          for (Componente2 c : componentes) {
               if (c.getTipo() == TipoComp.Dep) {
                    componentesDep.add(c);
               }
          }
          GrafoN gAgregado = new GrafoN();
          BolsaDeConjsNodosN bolsaCI = new BolsaDeConjsNodosN(cantPasos);
          bolsaCI.setTipo(TConjNodoN.Info);
          BolsaDeConjsNodosN bolsaCDP = new BolsaDeConjsNodosN(cantPasos);
          bolsaCDP.setTipo(TConjNodoN.DetParque);
          bolsaCDP.setBolsaCIAsoc(bolsaCI);

          /**
           * Crea un grafoN con las componentes independientes observadas
           * incluso las que cambian el parque si existen
           */
          boolean inicio = true;
          TipoCargaCDP cambiaParque = TipoCargaCDP.TODAS;
          boolean observada = true;
          gAgregado = agregaCompsIndep(null, componentesIndep,
                  cantPasos, inicio, observada, cambiaParque,
                  impDet);


          inicio = false;
          /**
           * Le agrega las componentes dependientes observadas
           */
          GrafoN gAgregado2 = agregaCompsDep(gAgregado,
                  componentesDep, cantPasos,
                  observada,
                  impDet);

          /**
           * Tiene un grafo que tiene todas las variables observadas y sólo ellas
           * Carga la bolsa de conjuntos de información y carga el ConjInfo en los nodos
           * del grafo
           */
          cargaBolsaCI(gAgregado2, bolsaCI, cantPasos, TConjNodoN.Info);

          /**
           * Carga las componentes no observadas independientes
           * que determinan parque, si existen
           * (No hay componentes que cambien el parque y no sean independientes)
           */
          observada = false;
          cambiaParque = TipoCargaCDP.DETP;
          GrafoN gAgregado3 = agregaCompsIndep(gAgregado2, componentesIndep,
                  cantPasos, inicio, observada, cambiaParque,
                  impDet);

          cargaBolsaCI(gAgregado3, bolsaCDP, cantPasos, TConjNodoN.DetParque);

          cambiaParque = TipoCargaCDP.NODETP;

          /**
           * Carga las restantes componentes no observadas independientes
           */
          GrafoN gAgregado4 = agregaCompsIndep(gAgregado3, componentesIndep,
                  cantPasos, inicio, observada, cambiaParque,
                  impDet);

          /**
           * Carga las componentes no observadas dependientes
           */
          GrafoN gAgregado5 = agregaCompsDep(gAgregado4,
                  componentesDep, cantPasos,
                  observada, false);

          /**
           * Carga el nodo inicial del GrafoN
           */
          ArrayList<String> valIni = cargarNodoInicial(dirArchivo, gAgregado5);
          NodoN nodoIni = gAgregado5.nodoDeValores(1, valIni);
          if (nodoIni == null) {
               throw new XcargaDatos("Error al especificar nodo inicial");
          } else {
               gAgregado5.setNodoInicial(nodoIni);
          }


          gAgregado5.cargaProb2();

          /**
           * Carga los nodos en cada conjunto de nodos de bolsaCI y bolsaCDP
           * carga el grafo en cada nodo.
           * Carga los valores de tipos de datos en todos los nodos.
           */
          for (int t = 0; t <= cantPasos; t++) {
               for (NodoN n : gAgregado5.getNodos().get(t)) {
                    n.setGrafoN(gAgregado5);
                    ConjNodosN cI = n.getConjInfo();
                    ConjNodosN cDP = n.getConjDetParque();
                    bolsaCI.cargaNodoEnCI(t, cI, n);
                    bolsaCDP.cargaNodoEnCI(t, cDP, n);
               }
          }


          /**
           * Calcula las probabilidades condicionales de cada nodo de un ConjInfo
           * como la probabilidad del nodo dividida la probabilidad
           * de todos los nodos del mismo.
           */
          bolsaCI.calcProbCond2(TConjNodoN.Info);
          bolsaCI.setGrafoN(gAgregado5);
          gAgregado5.setBolsaCI(bolsaCI);

          /**
           * Calcula las probabilidades condicionales de cada nodoN de un
           * conjunto determinante de parque ConjDetParque
           * Un ConjDetParque es el conjunto de nodosN que resultan del grafo que
           * compone las componentes observadas y las componentes que cambian parque
           * suponiendo que estas últimas no son observadas.
           */
          bolsaCDP.calcProbCond2(TConjNodoN.DetParque);
          bolsaCDP.setGrafoN(gAgregado5);
          gAgregado5.setBolsaCDP(bolsaCDP);


//          /**
//           * Carga el nodo inicial del GrafoN
//           */
//          ArrayList<String> valIni = cargarNodoInicial(dirArchivo, gAgregado5);
//          NodoN nodoIni = gAgregado5.nodoDeValores(1, valIni);
//          if (nodoIni == null) {
//               throw new XcargaDatos("Error al especificar nodo inicial");
//          } else {
//               gAgregado5.setNodoInicial(nodoIni);
//          }

          return gAgregado5;
     }

     public GrafoN agregaCompsIndep(GrafoN gAgregado,
             ArrayList<Componente2> componentesIndep,
             int cantPasos, boolean inicio,
             boolean tipoObs, TipoCargaCDP cambiaParque,
             boolean impDet) throws XcargaDatos {
          /**
           * Agrega al GrafoN gOrigen un conjunto de componentes independientes
           * y las devuelve en un nuevo grafo
           * Si inicio = true gOrigen es null y no hay grafo de origen
           * Según el valor de Observada considera las componentes observadas
           * o las no observadas
           * Si cambiaParque = true, sólo considera componentes que cambian
           * el parque
           * Si cambiaParque = false, considera las componentes cambien o no el parque
           * Si está agregando componentes no observadas, carga en bolsaCD
           * los conjuntos de información que se generan
           */
          int icomp = 0;
          GrafoN gInicio = null;
          GrafoN gAdic = null;
          GrafoN g = gAgregado;
          String nombreAgregado;
          String texto;
          if (!inicio) {
               nombreAgregado = gAgregado.getNombre();
          } else {
               nombreAgregado = "Inicio ";
          }

          /**
           * Carga en g el grafo inicial que puede ser null
           */
          if (inicio) {
               /**
                * Crea un grafo lineal con la variable aleatoria
                * observada "nada" cuyo único valor es "nada" para que siempre
                * exista un grafo de variables observadas
                */
               ArrayList<String> aNada = new ArrayList<String>();
               aNada.add("nada");
               gInicio = new GrafoN(cantPasos, "Inicio", true);
               VariableNat vNnada = new VariableNat("nada", aNada);
               vNnada.setObservadaEnOptim(true);
               ArrayList<VariableNat> aVN = new ArrayList<VariableNat>();
               aVN.add(vNnada);
               gInicio.setVarGrafo(aVN);
               for (int i = 0; i <= cantPasos; i++) {
                    NodoN nN = new NodoN();
                    nN.setGrafoN(gInicio);
                    nN.setTiempo(i);
                    nN.setOrdinal(1);
                    nN.setProbabilidad(1.0);
                    nN.setValoresVN(aNada);
                    ArrayList<NodoN> auxAN = new ArrayList<NodoN>();
                    gInicio.getNodos().add(auxAN);
                    gInicio.getNodos().get(i).add(nN);
               }
               for (int i = 0; i <= cantPasos - 1; i++) {
                    NodoN nsuc = gInicio.getNodos().get(i + 1).get(0);
                    gInicio.getNodos().get(i).get(0).getSucesores().add(nsuc);
                    gInicio.getNodos().get(i).get(0).getProbSucesores().add(1.0);
               }
               gAgregado = gInicio;
               g = gAgregado;
          }

          for (Componente2 c : componentesIndep) {

               if (c.getObservada() == tipoObs &
                       (cambiaParque == TipoCargaCDP.TODAS ||
                       cambiaParque == TipoCargaCDP.NODETP & c.cambiaParque() == false ||
                       cambiaParque == TipoCargaCDP.DETP & c.cambiaParque() == true)) {

                    //			icomp++;
                    gAdic = new GrafoN(cantPasos, c.getNombre(), true);
                    gAdic.cargaNodos(c.getNodos());
                    gAdic.cargaArcos(c.getArcos());
//                    gAdic.cargaProb();
                    gAdic.setNombre(c.getNombre());
                    ArrayList<Boolean> valObs = new ArrayList<Boolean>();
                    for (int j = 0; j < c.getVariables().size(); j++) {
                         valObs.add(tipoObs);
                    }
                    gAdic.cargaVarGrafo(c.getVariables(), c.getValoresDe(),
                            valObs);
                    gAdic.setNombre(c.getNombre());
                    if (impDet) {
                         texto = gAdic.toString();
                         System.out.print(texto);
                    }
                    gAgregado = g.componeGrafo(gAdic, 2);
                    g = new GrafoN(cantPasos, "g", true);
                    g = gAgregado;
                    nombreAgregado += " + " + gAdic.getNombre();
                    g.setNombre(nombreAgregado);

                    //	esta es la impresión de los grafos que se van generando
                    if (impDet) {
                         texto = g.toString();
                         System.out.print(texto);
                    }
               }
          }
          return gAgregado;
     }

     public GrafoN agregaCompsDep(GrafoN gAgregado,
             ArrayList<Componente2> componentesDep,
             int cantPasos, boolean tipoObs, boolean impDet)
             throws XcargaDatos {

          /**
           *  AGREGADO DE VARIABLES DE LAS COMPONENTES DEPENDIENTES
           */
          int icomp = 0;
          String texto;
          String nombreAgregado = gAgregado.getNombre();


          ArrayList<String> auxS;
          ArrayList<VariableNat> auxVN;
          ArrayList<VariableNat> auxVNDepDe;
          VariableNat vdet;

          for (Componente2 c : componentesDep) {
               if (c.getObservada() == tipoObs) {
                    //			int icomp++;
                    auxVN = new ArrayList<VariableNat>();  // en este se carga las nuevas VN
                    auxVNDepDe = new ArrayList<VariableNat>();  // en este se carga las VN preexistentes que condicionan las nuevas
                    // agrega al grafo las variables de la naturaleza de la nueva componente dependiente
                    int is = 0;
                    for (String s : c.getVariables()) {
                         VariableNat vn = new VariableNat(s, c.getValoresDe().get(is));
                         vn.setObservadaEnOptim(tipoObs);

                         for (String sdet : c.getDependenDe()) {
                              vdet = gAgregado.devuelveVN(sdet);
                              auxVNDepDe.add(vdet);
                         }
                         auxVN.add(vn);
                         is++;
                    }

                    /**
                     * Agrega las variables de la componente dependiente
                     **/
                    gAgregado.getVarGrafo().addAll(auxVN);

                    /**
                     * recorre los nodos del árbol y carga los valores de las nuevas variables
                     * el primer nodo (t=0) no se recorre porque las variables de la naturaleza no
                     * toman valores significativos
                     **/
//                    for (int t = 1; t < gAgregado.getNodos().size(); t++) {
                    for(int t = 1; t <= cantPasos; t++) {
                         for (int ord = 1; ord <= gAgregado.getNodos().get(t).size(); ord++) {
                              NodoN n;
                              boolean agregar;
                              boolean cargado = false;
                              n = gAgregado.getNodos().get(t).get(ord - 1);
                              for (ArrayList<String> f : c.getFuncion()) {
                                   if (f.get(f.size() - 1).equalsIgnoreCase("todos") ||
                                           Integer.parseInt(f.get(f.size() - 1)) == t) {
                                        agregar = true;
                                        for (int j = 0; j < c.getDependenDe().size(); j++) {
                                             agregar = agregar &&
                                                     f.get(j).equalsIgnoreCase(gAgregado.valorVariableNat(n, auxVNDepDe.get(j)));
                                        }
                                        if (agregar) {
                                             for (int j = c.getDependenDe().size(); j < f.size() - 1; j++) {
                                                  n.getValoresVN().add(f.get(j));
                                                  cargado = true;
                                             }
                                        }
                                   }
                              }
                              // Finaliza el tratamiento de un nodo
                              if (!cargado) {
                                   throw new XcargaDatos("El nodo " + n.toString() + " no recibió valores de componente " +
                                           c.getNombre());
                              }
                         }
                    }
                    nombreAgregado += " + " + c.getNombre();
                    gAgregado.setNombre(nombreAgregado);
                    //			if (impDet){
                    //				texto = gAgregado.toString();
                    //				System.out.print(texto);
                    //			}
               }
          }
          return gAgregado;
     }


     /**
      * Devuelve los valores simbólicos de las variables de la naturaleza
      * del nodo inicial, en el orden de las variables del grafoN.
      * Verifica que los nombres simbólicos y de las variables son correctos.
      * @param dirArchivo
      * @param grafoN
      * @return
      * @throws XcargaDatos
      */
     public static ArrayList<String> cargarNodoInicial(
            String dirArchivo, GrafoN grafoN) throws XcargaDatos{

        ArrayList<ArrayList<String>> datAr = LeerDatosArchivo.getDatos(dirArchivo);
        ArrayList<String> resultado = new ArrayList<String>();
        String textoIni = "&NODO_INICIAL"; // Marca de lista de
                                                 // componentes base a cargar.
        String textoFin = "&FIN"; // Marca de fin de la lista de recuros
                                 // base a cargar.
        boolean encontrado;
        int i, j;

        // Búsqueda de la definición del nodo inicial
        encontrado = false;
        i = 0;
        while( !encontrado && (i <  datAr.size()) ){
            if( textoIni.equalsIgnoreCase(datAr.get(i).get(0)) ) {
                encontrado = true;
            }
            i++;
        }
        if(!encontrado) throw new XcargaDatos("No se encontraron datos del nodo inicial.");
        // Si se encontró la marca de inicio del nodo se busca la lista de variables y sus valores
        // con excepción de la variable nada que tiene todo GrafoN

        String nombre = "";
        String valor = "";
        String vleido ="";  // valor de la variable leído
        String nleido="";  // nombre de la variable leído
        for(VariableNat vn: grafoN.getVarGrafo()){
            encontrado = false;
            nombre = vn.getNombre();
            if(! nombre.equalsIgnoreCase("nada")){
                j = 0;
                do{
                    nleido = datAr.get(i+j).get(1);
                    vleido = datAr.get(i+j).get(3);
                    if(nleido.equalsIgnoreCase(nombre)){
                        if(vn.getValores().contains(vleido)){
                            resultado.add(vleido);
                            encontrado = true;
                        }else{
                            throw new XcargaDatos("Se especificó un valor inexistente " + vleido +
                                    "para la VariableNat" + nombre );
                        }
                    }
                    j++;
                }while(!encontrado & !(datAr.get(i+j).get(0).equalsIgnoreCase(textoFin)) );
            if (encontrado == false) throw new XcargaDatos("La VariableNat " + nombre + " no tiene valor ");
            }

        }
        return resultado;




    }

     public void cargaBolsaCI(GrafoN grafo, BolsaDeConjsNodosN bolsa,
             int cantPasos, TConjNodoN tipo) {
          /**
           * Carga la bolsa BolsaCI o BolsaCDP a partir del Grafo grafo, que es la composición
           * de todas las componentes observadas, (independientes y dependientes)
           * En grafo, los nodos quedan con el CI o CDP asociado
           */
          bolsa.setGrafoN(grafo);
          if (tipo == TConjNodoN.Info) {
               bolsa.setVariablesObservadas(grafo.getVarGrafo());
          } else {
               ArrayList<VariableNat> auxVN = new ArrayList<VariableNat>();
               auxVN.addAll(grafo.getVarGrafo());
               auxVN.removeAll(bolsa.getBolsaCIAsoc().getVariablesObservadas());
               bolsa.getVariablesObservadas().
                       addAll(bolsa.getBolsaCIAsoc().getVariablesObservadas());
               bolsa.getVariablesDetP().addAll(auxVN);
          }
          for (int t = 0; t <= cantPasos; t++) {
               int ordinal = 1;
               ArrayList<NodoN> nodost = grafo.getNodos().get(t);
               for (NodoN n : nodost) {
                    if (tipo == TConjNodoN.Info) {
                         // La bolsa es de CI
                         ConjNodosN cI = new ConjNodosN(grafo, t);
                         cI.setOrdinal(ordinal);
                         ordinal++;
                         cI.setTipoConj(tipo);
                         cI.setValoresObservadas(n.getValoresVN());
                         bolsa.getConjuntosNodosN().get(t).add(cI);
                         n.setConjInfo(cI);
                    } else {
                         // La bolsa es de CDP
                         ConjNodosN cDP = new ConjNodosN(grafo, t);
                         cDP.setOrdinal(ordinal);
                         ordinal++;
                         cDP.setTipoConj(tipo);
                         bolsa.getConjuntosNodosN().get(t).add(cDP);
                         cDP.setCIAsociado(n.getConjInfo());
                         n.getConjInfo().getCDPsAsociados().add(cDP);
                         ArrayList<String> auxSCI = new ArrayList<String>();
                         auxSCI = cDP.getCIAsociado().getValoresObservadas();
                         cDP.setValoresObservadas(auxSCI);
                         ArrayList<String> auxS = new ArrayList<String>();
                         auxS.addAll(n.getValoresVN());
                         n.setConjDetParque(cDP);
                         for (int i = auxSCI.size(); i < (auxS.size()); i++) {
                              cDP.getValoresDetP().add(auxS.get(i));
                         }
                    }
               }
          }
          /**
           * Carga los CI sucesores de cada CI en la bolsa y sus probabilidades de
           * pasaje.
           * Se usa la propiedad de los grafos que se van componiendo, de que el ordinal
           * del nodoN en su paso, es igual al lugar que ocupa en el ArrayList + 1
           */
          for (int t = 0; t < cantPasos; t++) {
               ArrayList<NodoN> nodost = grafo.getNodos().get(t);
               for (int i = 0; i < bolsa.getConjuntosNodosN().get(t).size(); i++) {
                    ArrayList<NodoN> sucesores = nodost.get(i).getSucesores();
                    for (NodoN nsuc : sucesores) {
                         int indsuc = nsuc.getOrdinal();
                         double prob = nodost.get(i).probDeUnSucesor(nsuc);
                         ConjNodosN cIsuc = bolsa.getConjuntosNodosN().get(t + 1).get(indsuc - 1);
                         bolsa.getConjuntosNodosN().get(t).get(i).getSucesores().add(cIsuc);
                         bolsa.getConjuntosNodosN().get(t).get(i).getProbSucesores().add(prob);
                    }
               }
          }
     }

     public static void main(String[] args) throws XcargaDatos {

          DatosGeneralesEstudio datGen1 = new DatosGeneralesEstudio();
          CargaDatosGenerales cargadorDG = new CargaDatosGenerales() {
          };
          String dirDatGen = "D:/Java/PruebaJava2/V5-DatosGenerales.txt";

          cargadorDG.cargarDatosGen(dirDatGen, datGen1);

          GrafoN grafoNaturaleza = new GrafoN();
          CargaGrafoN2 cargaNat = new CargaGrafoN2() {
          };

          String dirArchivo = "D:/Java/PruebaJava2/V13-GrafoNaturaleza.txt";
          int cantPasos = datGen1.getCantPasos();
          grafoNaturaleza = cargaNat.cargarGrafoN(dirArchivo, cantPasos, false);

          String texto = grafoNaturaleza.toString();
//		System.out.print(texto);

          texto = grafoNaturaleza.getBolsaCI().toString();
//		System.out.print(texto);

          texto = grafoNaturaleza.getBolsaCDP().toString();
          System.out.print(texto);


          ListaEscenariosCN listaEsc =
                  new ListaEscenariosCN(grafoNaturaleza.getBolsaCDP(),
                  "Listado de escenarios de conjuntos que determinan parque");
          listaEsc.cargaListaEsc();
          texto = listaEsc.toString();
          System.out.print(texto);

     }
}














