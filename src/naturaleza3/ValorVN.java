/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package naturaleza3;

/**
 *
 * @author ut469262
 */
public class ValorVN {
	private VariableNat varN;
	private String valorVN;

	public ValorVN(VariableNat varN, String valorVN) {
		this.varN = varN;
		this.valorVN = valorVN;
	}

	public String getValorVN() {
		return valorVN;
	}

	public void setValorVN(String valorVN) {
		this.valorVN = valorVN;
	}

	public VariableNat getVarN() {
		return varN;
	}

	public void setVarN(VariableNat varN) {
		this.varN = varN;
	}

	


}
