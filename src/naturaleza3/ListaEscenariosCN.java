/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaleza3;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class ListaEscenariosCN implements Serializable {
     /**
      * Lista de todos los escenarios posibles de ciertas VN de
      * un grafo de la naturaleza
      */
     private String nombre;
     private BolsaDeConjsNodosN bolsaC;
     /**
      * BolsaDeConjsNodosN en la cual se calculan los escenarios:
      * Puede ser una bolsaCI o una bolsaCDP
      */
     private int cantEtapas;
     /**
      * Cantidad de etapas sin la etapa 0, es igual a la cantidad de etapas
      * de grafoN
      */
     private ArrayList<VariableNat> variablesObs;
     private ArrayList<VariableNat> variablesDetP;
     /**
      * lista de las variables de la naturaleza observadas y si
      * existen lista de variables no observadas que determinan parque
      */
     public ArrayList<EscenarioCN> escenarios;

     public ListaEscenariosCN(BolsaDeConjsNodosN bolsaC, String nombre) {
          this.nombre = nombre;
          this.bolsaC = bolsaC;
          escenarios = new ArrayList<EscenarioCN>();
          GrafoN grafo = new GrafoN();
          grafo = bolsaC.getGrafoN();
          cantEtapas = grafo.getCantEtapas();
          variablesObs = bolsaC.getVariablesObservadas();
          variablesDetP = bolsaC.getVariablesDetP();
     }

     public BolsaDeConjsNodosN getBolsaC() {
          return bolsaC;
     }

     public void setBolsaC(BolsaDeConjsNodosN bolsaC) {
          this.bolsaC = bolsaC;
     }

     public int getCantEtapas() {
          return cantEtapas;
     }

     public void setCantEtapas(int cantEtapas) {
          this.cantEtapas = cantEtapas;
     }

     public ArrayList<EscenarioCN> getEscenarios() {
          return escenarios;
     }

     public void setEscenarios(ArrayList<EscenarioCN> escenarios) {
          this.escenarios = escenarios;
     }

     public String getNombre() {
          return nombre;
     }

     public void setNombre(String nombre) {
          this.nombre = nombre;
     }

     public ArrayList<VariableNat> getVariablesDetP() {
          return variablesDetP;
     }

     public void setVariablesDetP(ArrayList<VariableNat> variablesDetP) {
          this.variablesDetP = variablesDetP;
     }

     public ArrayList<VariableNat> getVariablesObs() {
          return variablesObs;
     }

     public void setVariablesObs(ArrayList<VariableNat> variablesObs) {
          this.variablesObs = variablesObs;
     }

     /**
      *  Carga la lista de escenarios que resulta de la BolsaDeConjsNodosN
      *  bolsaC de (this).
      */
     public void cargaListaEsc() {
          GrafoN grafo = bolsaC.getGrafoN();
          cantEtapas = grafo.getCantEtapas();

          for (int e = 0; e <= cantEtapas; e++) {
               ArrayList<EscenarioCN> nuevosEsc = new ArrayList<EscenarioCN>();
               int cantEsc = 0;
               if (e == 0) {
                    for (ConjNodosN ci : bolsaC.getConjuntosNodosN().get(e)) {
                         EscenarioCN esc = new EscenarioCN(bolsaC);
                         esc.setCantEtapas(0);
                         esc.getConjsNodosN().add(ci);
                         nuevosEsc.add(esc);
                         cantEsc++;
                    }
               } else {
                    for (int ies = 0; ies < escenarios.size(); ies++) {
                         EscenarioCN escBase = escenarios.get(ies);
                         ArrayList<ConjNodosN> auxSuc = new ArrayList<ConjNodosN>();
                         auxSuc.addAll(escBase.getConjsNodosN().get(e - 1).getSucesores());
                         ArrayList<Double> auxD = new ArrayList<Double>();
                         auxD.addAll(escBase.getConjsNodosN().get(e - 1).getProbSucesores());
                         nuevosEsc.addAll(escBase.generarHijos(bolsaC, auxSuc, auxD));
                         cantEsc += auxSuc.size();
                    }
               }
               String texto = "Etapa = " + e + "cantidad escenarios= " + cantEsc + "\n";
               System.out.print(texto);
               escenarios = nuevosEsc;
          }
          for (int ies = 0; ies < escenarios.size(); ies++) {
               escenarios.get(ies).setNumesc(ies + 1);
          }
     }

     @Override
     public String toString() {
          String texto = "=================================================" + "\n";

          texto += "COMIENZA DESCRIPCION DE UNA LISTA DE ESCENARIOS" + "\n";
          texto += "=================================================" + "\n";
          texto += "NOMBRE: " + nombre + "\n";

          for (EscenarioCN esc : escenarios) {
               texto += esc.toString() + "\n";
          }
          return texto;
     }

     public String toStringCorto() {
          String texto = "=================================================" + "\n";

          texto += "COMIENZA DESCRIPCION DE UNA LISTA DE ESCENARIOS";
          texto += "=================================================" + "\n";
          texto += "NOMBRE: " + nombre + "\n";

          for (EscenarioCN esc : escenarios) {
               texto += esc.toStringCorto() + "\n";
          }
          return texto;
     }
}
