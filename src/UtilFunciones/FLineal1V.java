/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilFunciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 * Función lineal a + bx
 */
public class FLineal1V extends Funcion1V implements Serializable{
    protected double a;
    protected double b;

    public FLineal1V(TipoF1V tipo, double a, double b) {
        super(tipo);
        this.a = a;
        this.b = b;
    }


    @Override
    /**
     * El segundo argumento params se pone solo por compatibilidad.
     */
    public double evaluar(double x, ArrayList<Double> params){
        double result = a + b*x;
        return result;
    }

    public void leerParam(ArrayList<Double> params){
        a = params.get(0);
        b = params.get(1);
    }


}
