/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilFunciones;

import UtilFunciones.Funcion1V.TipoF1V;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class FLineal1VTopes extends FLineal1V implements Serializable{
    private double topeInf;
    private double topeSup;

    public FLineal1VTopes(TipoF1V tipo, double a, double b, double topeInf, double topeSup) {
        super(tipo, a, b);
        this.topeInf = topeInf;
        this.topeSup = topeSup;
    }
    
    @Override
    /**
     * El segundo argumento params se pone por compatibilidad.
     */
    public double evaluar(double x, ArrayList<Double> params){
        double result;
        result = a + b*x;
        if(result > topeSup) result = topeSup;
        if(result < topeInf) result = topeInf;
        return result;            
    }

    @Override
    public void leerParam(ArrayList<Double> params){
        a = params.get(0);
        b = params.get(1);
        topeInf = params.get(2);
        topeSup = params.get(3);
    }



}
