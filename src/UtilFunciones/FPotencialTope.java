/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilFunciones;

import GrafoEstados.Objetivo;
import GrafoEstados.PeriodoDeTiempo;
import GrafoEstados.ResSimul;
import persistenciaMingo.XcargaDatos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 *
 * La función es:
 *
 * f(x) = x si x <= xo
 * f(x) = x + k*xo*[(x-xo)/xo]**a si x > xo
 *
 * Se supone xo>0
 *
 * Factor de crecimiento de la función de xo a 2*xo     F2 (debe ser >2)
 * Factor de crecimiento de la derivada de xo a 2*xo	D2
 *
 * a = (D2-1)/(F2-2)  ;   k = F2 - 2
 * Para que la f(x) sea convexa es necesario que:
 * F2-D2 < 1 , o lo que es equivalente a > 1;
 * F2 > 2 , o lo que es equivalente k > 0;
 *
 * Para que la función sea lineal f(x)=x se toma k=0, (a cualquier valor por ejemplo a=1)
 *
 */
public class FPotencialTope  extends Funcion1V implements Serializable{
    private double xcero;  // es un parámetro que depende del tiempo
    /**
     * Los siguientes son parámetros de forma que no cambian con el paso de tiempo.
     */
    private double k;
    private double a;
    private double f2;
    private double d2;

    public FPotencialTope() {
        super(TipoF1V.POTENCIAL_CON_TOPES);

    }


    public FPotencialTope (double k, double a) throws XcargaDatos {
        super(TipoF1V.POTENCIAL_CON_TOPES);

        if(a<=1 || k <= 0) throw new XcargaDatos("función f no es convexa");
        this.k = k;
        this.a = a;
    }

    public FPotencialTope (double f2, double d2, String nada) throws XcargaDatos{
        /**
         * Se agrega un argumento inútil nada al final para que pueda existir este constructor.
         */
        super(TipoF1V.POTENCIAL_CON_TOPES);
        if(f2 - d2 < 1 | f2 < 2)throw new XcargaDatos("función f no es convexa");
        a = (d2-1)/(f2-2);
        k = f2 - 2;
    }


    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getK() {
        return k;
    }

    public void setK(double k) {
        this.k = k;
    }

    public double getXcero() {
        return xcero;
    }

    public void setXcero(double xcero) {
        this.xcero = xcero;
    }

    public double getD2() {
        return d2;
    }

    public void setD2(double d2) {
        this.d2 = d2;
    }

    public double getF2() {
        return f2;
    }

    public void setF2(double f2) {
        this.f2 = f2;
    }


    public void calcAyK(){
        if(Math.abs(f2-2)>0.000001){
            a = (d2-1)/(f2-2);
            k = f2 - 2;
        }else{
            k = 0.;
            a= 1.;
        }
    }





 /**
  *************************************************************************
  * Comienzan las funciones que sobreescriben las de Funcion1V
  *************************************************************************
  */
    @Override
    /**
     * El primer argumento de información es un ArrayList de ParRealPerTiempo
     * con los topes inferiores por período de tiempo.
     */
    public double evaluar(double x, ArrayList<Double> params) throws XcargaDatos{
        if(params.size()<1) throw new XcargaDatos(" una FPotencialTope recibió más de un parámetro que " +
                "depende del tiempo");
        xcero = params.get(0);
        if(xcero<0) throw new XcargaDatos("El valor de xcero es negativo ");
        if(x<= xcero) return x;
        double result = x + k*xcero*Math.pow( (x-xcero)/xcero , a);
        return result;
    }
    
    
    

 /**
  *************************************************************************
  * Terminan las funciones sobreescritas
  *************************************************************************
  */


    @Override
    public String toString(){
        String texto = "Función potencial tope:   ";
        texto += "k = " + k + "\t";
        texto += "a = " + a + "\t";
        texto += "f2 = " + f2 + "\t";
        texto += "d2 = " + d2 + "\t";
        return texto;
    }


}
