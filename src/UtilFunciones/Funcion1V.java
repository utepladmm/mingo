/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package UtilFunciones;

import java.io.Serializable;
import java.util.ArrayList;

import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class Funcion1V implements Serializable{
    protected TipoF1V tipo;
    protected int cantParametros;
    

    public Funcion1V(TipoF1V tipo) {
        this.tipo = tipo;
    }


 /**
  *************************************************************************
  * Comienzan las funciones a sobreescribir por la clases que heredan
  *************************************************************************
  */

    /**
     * Evalúa la función suponiendo que ya tiene cargados todos sus parámetros
     * de escala y de forma
     * @param x el valor del argumento de la función
     * @param params es el conjunto de parámetros de la función.
     * @return result el resultado, un real
     * @throws persistenciaMingo.XcargaDatos
     */
    public double evaluar(double x, ArrayList<Double> params) throws XcargaDatos {
        double result = 0;
        return result;
    }


    public void cargarParametros(ArrayList<Double> params){

    }



 /**
  *************************************************************************
  * Terminan las funciones a sobreescribir
  *************************************************************************
  */


    public TipoF1V getTipo() {
        return tipo;
    }

    public void setTipo(TipoF1V tipo) {
        this.tipo = tipo;
    }

     public int getCantParametros() {
          return cantParametros;
     }

     public void setCantParametros(int cantParametros) {
          this.cantParametros = cantParametros;
     }
    


    public enum TipoF1V{
        LINEAL,
        LINEAL_CON_TOPES,
        POTENCIAL_CON_TOPES;
    }
}
