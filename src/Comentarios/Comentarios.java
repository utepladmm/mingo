/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Comentarios;

/**
 *
 * @author ut469262
 */
public class Comentarios {

    /**
     *
     * 
     *
     * COSAS A HACER
     *

     * - Verificar los directorios que resultan de los tipos de datos antes de lanzar
     * las corridas
     *
     * - Restricciones significativas que dependen de una variable de la naturaleza
     *
     * - Restricciones lógicas que requieren pasarle al metodo evaluar un portafolio
     * y un portafolio(decision). ESTO LO ESTOY HACIENDO
     *
     * - OJO QUE NO SERIALIZA BIEN LA CLASE ESTUDIO. Probablemente se debe a que no
     * puede serializar GrafoE
     *
     * - Probar cantidad de módulos variable con el tiempo absoluto
     *
     * - Duda sobre la especificación del nodo inicial del GrafoN: dar el nodo inicial
     * de cada componente.
     *
     * - Rest. de inicio deben aplicarse solo a recursos elegibles
     * - OJO topeo de tiempos de construcción
     * - OJO parametros del objetivo que dependen del tiempo.
     * - ojo ¿que valores toma para la entrada de recursos existentes: los de V13 rec base o los
     * de expansion forzada?
     *
     * - UN PORTAFOLIO NO PRECISA TENER COMO ATRIBUTO EL PERIODO DE TIEMPO
     *
     * OBSERVACIONES
     *
     * - Hubo un error al intentar serializar grafoE, probablemente vinculado l bug
     * que se describe en http://bugs.sun.com/view_bug.do?bug_id=4152790
     * StackOverFlowError serializing/deserializing a large graph of objects.
     * La solución que le di fue cargar con null el grafoE asociado a los Casos cuando
     * se serializa un estudio (que contiene sus Casos). Hay que volver a asignar el
     * grafoE al caso para usarlo.
     * Esto impide conservar los grafoE en persistenciaMingo, hay que crearlo cada vez.
     * 
     * - OJO el vector informacion tiene largo absurdo 
     *
     *
     * COMO UTILIZAR FECHAS PARA LA CANTIDAD DE MODULO Y POTENCIA NOMINAL DE UN MÓDULO DE UN RECURSO BASE.
     * 
     * - En CargaRecBase.cargarRec los dos campos se cargan como DatoT.
     * Hay que verificar que la primera fecha de ambos sea la misma y que los datos sean no nulos
     * Si el DatoT es tipo FECHA verificar que el recurso no depende de la edad y cargar tiempoAbsolutoBase ???
     * 
     * - En Recurso
     * Crear cantModF(ta) que si el recurso no depende de la edad, da la cantidad promedio de modulos en el ta.
     * Crear cantProdSimpF(pr, ta) que según los valores de diaIni, diaFin y formaCalculo del producto pr calcula
     * la cantidad de producto en ta.
     * 
     * - En RecOTrans hay que reescribir anualidInvRot (que para Recursos tiene en cuenta todos los módulos)
     * teniendo en cuenta la fecha de entrada de los RecursosBase que no dependen de la edad. OJO con el efecto de actualización
     * porque la anualidad se calcula a la mitad del período anual.
     * 
     * - El método anualidInv de Bloque replica anualidInv de RecOTrans. Habría que investigar si no conviene que el método 
     * de Bloque use al de RecOTrans.
     * 
     *
     *
     *
     *
     */

}
