/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaMingo;

import dominio.ConjDemandas;
import dominio.DatosGeneralesEstudio;
import dominio.Demanda;
import dominio.Falla;
import dominio.FallaPorcentual;
import dominio.ListaReales;
import dominio.Punto;
import dominio.TiempoAbsoluto;
import java.util.ArrayList;
import naturaleza3.GrafoN;
import naturaleza3.VariableNat;

/**
 *
 * @author ut469262
 */
public class CargaDemandas {

    public void cargarConjDemandas(String dirArchivo, ConjDemandas conjDem,
            DatosGeneralesEstudio datGen, GrafoN grafo) throws XcargaDatos {
//            throws FileNotFoundException, IOException, XcargaDatos{
        /**
         *	Si alguna demanda es aleatoria debe chequearse que los valores
         *  simbólicos de demanda sean los del grafo de la naturaleza para la variable
         *  de demanda asociada; esto no se hace aquí
         */
        // Datos de todo el archivo.
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        // Datos del recurso base a cargar.
        Demanda dem;
        // Lista de los recursos base a cargar.
        ArrayList<String> listaDemandas = new ArrayList<String>();

        // Obtención de todos los datos del archivo.
        LeerDatosArchivo cargador = new LeerDatosArchivo() {
        };
        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        // Carga de la lista de demandas a cargar.
        listaDemandas = cargarListDem(datAr);
        // Carga de las demandas.
        for (String nombDem : listaDemandas) {

            cargarDem(datAr, nombDem, conjDem, datGen, grafo);

        }
    }

    private ArrayList<String> cargarListDem(ArrayList<ArrayList<String>> datAr)
            throws XcargaDatos {
        ArrayList<String> resultado = new ArrayList<String>();
        String iniListaRecB = "&LISTA_DEMANDAS"; // Marca de lista de
        // demandas a cargar.
        String listaFin = "&FIN"; // Marca de fin de la lista de demandas
        // cargar.
        boolean encontrado;
        int i, j;

        // Búsqueda de la lista de demandas a cargar.
        encontrado = false;
        i = 0;
        while (!encontrado && (i < datAr.size())) {
            if (iniListaRecB.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                encontrado = true;
            }
            i++;
        }
        // Si se encontró la marca de inicio de la lista y
        // no sigue la marca de fin de la lista, se carga la lista de recursos.
        if (encontrado && (i < datAr.size())
                && listaFin.compareToIgnoreCase(datAr.get(i).get(0)) != 0) {
            for (j = 0; j < datAr.get(i).size(); j++) {
                resultado.add(datAr.get(i).get(j));
            }
        } else {
            throw new XcargaDatos("No se encontró la lista de demandas.");
        }

        return resultado;
    }

    private void cargarDem(ArrayList<ArrayList<String>> datAr,
            String nombDem, ConjDemandas conjDem,
            DatosGeneralesEstudio datGen,
            GrafoN grafo) throws XcargaDatos {
        // Marca de inicio de los datos de un recurso base.
        String ini = "&DEMANDA";
        // Marca de fin de los datos de un recurso base.
        String fin = "&FIN";
        Demanda dem = new Demanda();
        Punto pConex;
        String dato;
        int i, j;
        ArrayList<Integer> auxI;
        ArrayList<Double> auxD;
        ArrayList<ArrayList<Double>> auxDD;
        ArrayList<String> auxS;
        ListaReales auxLR;
        boolean encontrado = false;
        boolean noSeReconoceEtiqueta = false;
        int valoresRequeridos = 0;
        int valoresLeidos = 0;

        // Búsqueda del nombre de la demanda a cargar.
        i = 0;
        while (!encontrado && (i + 1 < datAr.size())) {
            if (ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                if (datAr.get(i + 1).size() > 1 && 0
                        == nombDem.compareToIgnoreCase(datAr.get(i + 1).get(1))) {
                    encontrado = true;
                }
            }
            i++;
        }
        if (!encontrado) {
            throw new XcargaDatos("No se encontraron los datos de la demanda: "
                    + nombDem);
        }

        while (i < datAr.size()
                && fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0) {
            //Carga la información correspondiente a los datos de la demanda.
            try {
                if (0 == dato.compareToIgnoreCase("nombre")) {
                    dem.setNombre(datAr.get(i).get(1));
                } else if (0 == dato.compareToIgnoreCase("punto")) {
                    /*
                    pConex = new Punto( Double.parseDouble(datAr.get(i).get(1)),
                    Double.parseDouble(datAr.get(i).get(2)),
                    Double.parseDouble(datAr.get(i).get(3)) );
                    recB.setPuntoConex(pConex);
                     */
                } else if (0 == dato.compareToIgnoreCase("tipo")) {
                    dem.setTipo(datAr.get(i).get(1));
                } else if (0 == dato.compareToIgnoreCase("subtipo1")) {
                    dem.setSubtipo1(datAr.get(i).get(1));
                } else if (0 == dato.compareToIgnoreCase("subtipo2")) {
                    dem.setSubtipo2(datAr.get(i).get(1));
                } else if (0 == dato.compareToIgnoreCase("tipo_falla")) {
                    dem.setTipofalla(datAr.get(i).get(1));

                } else if (0 == dato.compareToIgnoreCase("aleatoria")) {
                    dem.setAleatoria(0 == (datAr.get(i).get(1)).compareToIgnoreCase("si"));
                } else if (0 == dato.compareToIgnoreCase("tiempo_base_demanda")) {
                    dem.setTiempoAbsolutoBase(new TiempoAbsoluto((datAr.get(i).get(1))));
                } else if (0 == dato.compareToIgnoreCase("energias")) {
                    auxDD = new ArrayList<ArrayList<Double>>();
                    while (0 == datAr.get(i).get(0).compareToIgnoreCase("energias")) {
                        dem.getValoresVA().add(datAr.get(i).get(1));
                        auxD = new ArrayList<Double>();
                        valoresLeidos = 0;
                        for (j = 2; j < datAr.get(i).size(); j++) {
                            auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                            valoresLeidos++;
                        }
                        valoresRequeridos = datGen.getCalculadorDePlazos().hallaAvanceTiempo(dem.getTiempoAbsolutoBase(), datGen.getFinEstudio()) + 1;
                        if (valoresLeidos < valoresRequeridos) {
                            throw new XcargaDatos("Error en "
                                    + "cantidad de valores leídos de energía de la demanda " + dem.getNombre());
                        }
                        auxDD.add(auxD);
                        i++;
                    }
                    dem.setDemEnergia(auxDD);
                    i--;
                } else if (0 == dato.compareToIgnoreCase("paso_a_productos")) {
                    String nomProd = datAr.get(i).get(1);
                    ListaReales lr = new ListaReales(nomProd);
                    valoresLeidos = 0;
                    for (j = 2; j < datAr.get(i).size(); j++) {
                        lr.getValores().add(Double.parseDouble(datAr.get(i).get(j)));
                        valoresLeidos++;
                    }
//					if(valoresLeidos != valoresRequeridos)throw new XcargaDatos("Error en " +
//								"cantidad de valores leídos de paso a productos de la demanda" + dem.getNombre());
                    /**
                     * Se eliminó la excepción porque se pasó a suponer que se toma el último
                     * valor leído.
                     */
                    dem.getPasoAProductos().add(lr);
                } else if (0 == dato.compareToIgnoreCase("energia_firme_manten")) {
                    auxD = new ArrayList<Double>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }
                    dem.setEnergiaFirmeManten(auxD);


                } else {
                    noSeReconoceEtiqueta = true;
                }
            } catch (Exception ex) {
                System.out.println(ex.toString());
                throw new XcargaDatos("Demanda: " + nombDem
                        + ". No se pudo cargar el dato: " + dato
                        + ". Se generó la excepción: " + ex.toString());
            } // Fin del catch.
            if (noSeReconoceEtiqueta) {
                throw new XcargaDatos("Demanda: " + nombDem
                        + ". No se reconoce la etiqueta: " + dato);
            }
            i++;
        } // Fin de while.

        /**
         * Si la demenda es aleatoria verifica los datos
         * - si el nombre de la variable aleatoria
         *   no existe en el grafo de la naturaleza la excepción la lanza el
         *   método devuelveVN
         * - los valores que puede tomar son los mismos
         */
        if (dem.isAleatoria()) {
            VariableNat vn = new VariableNat();
            vn = grafo.devuelveVN(nombDem + "_DEM");
            dem.setVAleatoria(vn);
            boolean ident;
            if (!(dem.getValoresVA().containsAll(vn.getValores())
                    && vn.getValores().containsAll(dem.getValoresVA()))) {
                throw new XcargaDatos("Los valores de la variable"
                        + "aleatoria de la demanda " + nombDem
                        + "no coinciden con los del grafo de la naturaleza");
            }

        } else {
            /**
             * Como la demanda no es aleatoria en el grafo de la naturaleza no aparece
             * una variable aleatoria asociada y debe crearse
             */
            VariableNat vn = new VariableNat();
            vn.setNombre(nombDem + "_DEM");
            dem.setVAleatoria(vn);
            vn.setValores(dem.getValoresVA());

        }
//   NO BORRAR ESTO, SE USARÁ CUANDO SE CARGUE EL TIPO DE FALL        
//        /**
//         * Comienza a leer los datos específicos según el tipo de demanda
//         * Antes de invocar la respectiva rutina de carga crea el tipo de falla
//         * adecuado, de la subclase necesaria.
//         */
//        if (dem.getTipofalla().equalsIgnoreCase("falla_porcentual")) {
//
//            Falla fallaAux = new Falla(dem.getNombre(), dem.getTipofalla());
//            FallaPorcentual falla = new FallaPorcentual();
//            falla.cargarFalla(fallaAux);
//
//            CargaFallaPorcentual cargaFall = new CargaFallaPorcentual() {
//            };
//            cargaFall.cargar(datAr, nombDem, falla);
//            dem.setFalla(falla);

//        }
        conjDem.getDemandas().add(dem);        
        /**
         * Carga los productos de la demanda a partir de las energías y los
         * coeficientes de paso a productos.
         */
        dem.cargaCantProductos(datGen, grafo);
    }
}
