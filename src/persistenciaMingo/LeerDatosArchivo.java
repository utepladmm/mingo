/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package persistenciaMingo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class LeerDatosArchivo {

    /**
     * Lee un archivo de texto por líneas y separa dentro de las líneas por blancos,
     * eliminando comentarios //
     * @param dirArchivo
     * @return resultado es el ArrayList<ArrayList<String>> con el texto separado en líneas
     */
    public static ArrayList<ArrayList<String>> getDatos(String dirArchivo){
        BufferedReader entrada = null;
        ArrayList<ArrayList<String>> resultado =
                                            new ArrayList<ArrayList<String>>();
        ArrayList<String> lineaResul;
        String[] datosLinea;
        String linea;
        String sep = "[\\s]+"; // Separador de datos.
        String coment = "//"; // Indicador de comentario.
        int posCom; // Posición del inicio del comentario.
        int j;

        try{
            File archivo = new File(dirArchivo);
            entrada = new BufferedReader(new FileReader(archivo));
            while( (linea = entrada.readLine()) != null){
                // Eliminación de los comentarios de las líneas.
                if( (posCom = linea.indexOf(coment)) > -1 ){
                    linea = (posCom == 0)? "" : linea.substring(0, posCom);
                }
                // Si la línea no es una línea de blancos, entonces se agregan
                // los datos y se agrega una nueva "línea" a resultado.
                if( !linea.matches("[\\s]*" ) ){
                    datosLinea = linea.split(sep);
                    lineaResul = new ArrayList<String>();
                    for(j = 0; j < datosLinea.length; j++){
                        /* Si el dato NO es un string de longitud cero "",
                         * lo cual podría ocurrir si
                         * la línea comienza por ej. con tabulador, se agrega
                         * el dato. */
                        if( !datosLinea[j].equalsIgnoreCase("") ){
                            lineaResul.add(datosLinea[j]);
                        }
                    }
                    resultado.add(lineaResul);
                }
            }
            entrada.close();
        }catch(FileNotFoundException ex){
                System.out.println("------ERROR-------");
            System.out.println("No se encontró el archivo: " + dirArchivo);
            System.out.println("------------------\n");
        }catch(IOException ex){
            System.out.println("------ERROR-------");
            System.out.println("Al leer el archivo: " + dirArchivo +
                    ", se produjo la excepción: " + ex.toString() );
            System.out.println("------------------\n");
        }


        return resultado;
    }
    
    /**
     * Lee un archivo de texto por l�neas 
     *
     * @param dirArchivo
     * @return resultado es el ArrayList<String> con el texto separado en líneas
     */
    public static ArrayList<String> leerLineas(String dirArchivo){
        BufferedReader entrada = null;
        ArrayList<String> resultado = new ArrayList<String>();
        String[] datosLinea;
        String linea;
        String coment = "//"; // Indicador de comentario.
        int posCom; // Posición del inicio del comentario.
        int j;

        try{ 
            File archivo = new File(dirArchivo);
            entrada = new BufferedReader(new FileReader(archivo));
            while( (linea = entrada.readLine()) != null){
            	resultado.add(linea);
                
            }
            entrada.close();
        }catch(FileNotFoundException ex){
                System.out.println("------ERROR-------");
            System.out.println("No se encontró el archivo: " + dirArchivo);
            System.out.println("------------------\n");
        }catch(IOException ex){
            System.out.println("------ERROR-------");
            System.out.println("Al leer el archivo: " + dirArchivo +
                    ", se produjo la excepción: " + ex.toString() );
            System.out.println("------------------\n");
        }


        return resultado;
    }    
    
    /**
     * Lee un archivo de texto por l�neas y separa dentro de las l�neas por un separador
     * eliminando comentarios //
     * @param dirArchivo
     * @param separador 
     * @return resultado es el ArrayList<ArrayList<String>> con el texto separado en líneas
     */
    public static ArrayList<ArrayList<String>> leerTextoSep(String dirArchivo, String sep){
        BufferedReader entrada = null;
        ArrayList<ArrayList<String>> resultado =
                                            new ArrayList<ArrayList<String>>();
        ArrayList<String> lineaResul;
        String[] datosLinea;
        String linea;
//        String sep = "[\\s]+"; // Separador de datos.
        String coment = "//"; // Indicador de comentario.
        int posCom; // Posición del inicio del comentario.
        int j;

        try{ 
            File archivo = new File(dirArchivo);
            entrada = new BufferedReader(new FileReader(archivo));
            while( (linea = entrada.readLine()) != null){
                // Eliminación de los comentarios de las líneas.
                if( (posCom = linea.indexOf(coment)) > -1 ){
                    linea = (posCom == 0)? "" : linea.substring(0, posCom);
                }
                // Si la línea no es una línea de blancos, entonces se agregan
                // los datos y se agrega una nueva "l�nea" a resultado.
                if( !linea.matches("[\\s]*" ) ){
                    datosLinea = linea.split(sep);
                    lineaResul = new ArrayList<String>();
                    for(j = 0; j < datosLinea.length; j++){
                        /* Si el dato NO es un string de longitud cero "",
                         * lo cual podría ocurrir si
                         * la línea comienza por ej. con tabulador, se agrega
                         * el dato. */
                        if( !datosLinea[j].equalsIgnoreCase("") ){
                            lineaResul.add(datosLinea[j]);
                        }
                    }
                    resultado.add(lineaResul);
                }
            }
            entrada.close();
        }catch(FileNotFoundException ex){
                System.out.println("------ERROR-------");
            System.out.println("No se encontró el archivo: " + dirArchivo);
            System.out.println("------------------\n");
        }catch(IOException ex){
            System.out.println("------ERROR-------");
            System.out.println("Al leer el archivo: " + dirArchivo +
                    ", se produjo la excepción: " + ex.toString() );
            System.out.println("------------------\n");
        }


        return resultado;
    }    
    
    public static void main(String[] args) {
    	String dir = "X:/MOP/2018/Modelo_futuro/datos/termicos/prueba/TGexp.txt";
    	ArrayList<String> result;
    	result = leerLineas(dir);
    	System.out.println("termino lectura");
    	
    }
    

}
