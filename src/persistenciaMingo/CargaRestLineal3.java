/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistenciaMingo;

import GrafoEstados.ConjRestGrafoE;
import Restricciones3.RestLineal3;
import Restricciones3.RestLineal3.Operador;
import TiposEnum.EstadosRec;
//import dominio.Bloque.EstadosRec;
import dominio.ConjRecB;
import dominio.ConjTransB;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import dominio.RecursoBase;
import dominio.TiempoAbsoluto;
import dominio.TransforBase;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class CargaRestLineal3 {

    public void cargarRestLineal(String dirArchivo,
		ConjRecB conjRB, ConjTransB conjTB, Estudio estudio,
		ConjRestGrafoE conRG3) throws XcargaDatos {
		// Carga la totalidad de las restricciones lineales
		// de un archivo de restricciones

        // Datos de todo el archivo.
        System.out.print("Se leen restricciones lineales en    " + dirArchivo + "\n");
        DatosGeneralesEstudio datGen = estudio.getDatosGenerales();

        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();

        // Obtención de todos los datos del archivo.
        LeerDatosArchivo cargador = new LeerDatosArchivo() {};
        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        // Marcas de inicio y fin de los datos de una restricción lineal
        String ini = "&LINEAL";
        String fin = "&FIN";
        String dato;
        int i, j;
        ArrayList<Integer> auxI;
        ArrayList<Double> auxD;
		ArrayList<String> auxS;
        boolean noSeReconoceEtiqueta = false;
        boolean encontrado = false;
        boolean encontro1 = false;

        i = 0;
        while( i+1 < datAr.size() ){
            // Búsqueda de una etiqueta de inicio de restricción lineal
            while( !encontro1 && (i + 1 <  datAr.size()) ){
                if( ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0){
                    encontrado = true;
                    encontro1 = true;
                }
                i++;
            }
            if(encontro1){
                // encontró una restricción
                encontro1 = false;
                RestLineal3 restL = new RestLineal3(estudio);

                while( i < datAr.size() &&
                        fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0 ){
                    //Carga la información correspondiente a los datos del recurso base.
                    try{
                        if( 0 == dato.compareToIgnoreCase("nombre_rest") ){
                              String nombRest = "";
                              for(j=1; j<datAr.get(i).size(); j++){
                                   nombRest += datAr.get(i).get(j);
                                   if (j<datAr.get(i).size()-1) nombRest = nombRest + " ";
                              }
                              restL.setNombre(nombRest);
                        }else if( 0 == dato.compareToIgnoreCase("es_heuristica") ){
                           if(datAr.get(i).get(1).equalsIgnoreCase("SI")){
                               restL.setEsHeuristica(true);
                           }else if(datAr.get(i).get(1).equalsIgnoreCase("NO")){
                               restL.setEsHeuristica(false);
                           }else throw new XcargaDatos("Una restricciónl lineal tiene mal especificado el valor esHeuristica");
                        }else if( 0 == dato.compareToIgnoreCase("tiempo_absoluto") ){
                           TiempoAbsoluto ta = new TiempoAbsoluto(datAr.get(i).get(1));
                           if(ta.getAnio() !=estudio.getDatosGenerales().getTiempoAbsolutoPasos().get(0).getAnio() ){
                                       throw new XcargaDatos("El tiempo base de la restricción +"
                                               +restL.getNombre() +": " +ta.getAnio() +" es distinto que el año "
                                               + "inicial del estudio "+estudio.getDatosGenerales().getTiempoAbsolutoPasos().get(0).getAnio()+"");
                           }
                           restL.setTiempoAbsolutoBase(ta);
                        }else if(0 == dato.compareToIgnoreCase("entera") ){
                           String s = datAr.get(i).get(1);
                           if(s.equalsIgnoreCase("SI")){
                               restL.setEsEntera(true);
                           }else if(s.equalsIgnoreCase("NO")){
                               restL.setEsEntera(false);
                           }else{
                               throw new XcargaDatos("La restricción no especifica SI o NO en esEntera");
                           }
                        }else if(0 == dato.compareToIgnoreCase("objetos")){
                           for(j = 1; j < datAr.get(i).size(); j++){
                               String s = datAr.get(i).get(j);
                               RecursoBase rb = conjRB.getUnRecB(s);
                               TransforBase tb = conjTB.getUnaTransB(s);
                               if(rb != null){
                                   restL.getObjetos().add(rb);
                               }else if(tb != null){
                                   restL.getObjetos().add(tb);
                               }else{
                                   throw new XcargaDatos("La restricción especifica un nombre de objeto que no es " +
                                           "de RecursoBase ni TransforBase");
                               }
                           }

                        }else if(0 == dato.compareToIgnoreCase("coef")){
                            if(restL.isEsEntera()){
                               for(j = 1; j < datAr.get(i).size(); j++){
                                   restL.getCoefEnteros().add(Integer.parseInt(datAr.get(i).get(j)));
                               }
                            }else{
                               for(j = 1; j < datAr.get(i).size(); j++){
                                   restL.getCoefReales().add(Double.parseDouble(datAr.get(i).get(j)));
                               }
                            }
                        }else if(0 == dato.compareToIgnoreCase("operador")){
                            String s = datAr.get(i).get(1);
                            if(s.equalsIgnoreCase("IGUAL")){
                                restL.setOperador(Operador.IGUAL);
                            }else if(s.equalsIgnoreCase("MENOR")){
                                restL.setOperador(Operador.MENOR);
                            }else if(s.equalsIgnoreCase("MENOR_O_IGUAL")){
                                restL.setOperador(Operador.MENOR_O_IGUAL);
                            }else if(s.equalsIgnoreCase("MAYOR")){
                               restL.setOperador(Operador.MAYOR);
                            }else if(s.equalsIgnoreCase("MAYOR_O_IGUAL")){
                               restL.setOperador(Operador.MAYOR_O_IGUAL);
                            }

                        }else if(0 == dato.compareToIgnoreCase("tind")){
                            String s = datAr.get(i).get(1);
                            if(restL.isEsEntera()){
                                for (j = 1; j < datAr.get(i).size(); j++){
                                    restL.getTindEntero().add(Integer.parseInt(datAr.get(i).get(j)));
                                }

                            }else{
                                for (j = 1; j < datAr.get(i).size(); j++){
                                    restL.getTindReal().add(Double.parseDouble(datAr.get(i).get(j)));
                                }
                            }
                        }else if(0 == dato.compareToIgnoreCase("estados_rec")){
                           String s = datAr.get(i).get(1);
                           restL.setEstadosRec(EstadosRec.valueOf(s));
                        }else noSeReconoceEtiqueta = true;

                    }catch(Exception ex){
                    System.out.println( ex.toString() );
                    throw new XcargaDatos("Recurso o tranformación: "  +
                            ". No se pudo cargar el dato: " + dato +
                            ". Se generó la excepción: " + ex.toString());
                    } // Fin del catch.
                    if( noSeReconoceEtiqueta ){
                        throw new XcargaDatos("En la restricción lineal "  +
                                "no se reconoce la etiqueta: " + dato);
                    }
                    i++;
                } // Fin de while.

                if(restL.getEstadosRec() == null) throw new XcargaDatos("Una restricción" +
                        "lineal no establece ESTADOS_REC");
                conRG3.getRestricciones().add(restL);
            } // Fin de if
            i++;
        } // Fin de while buscando restricciones
        if(!encontrado) System.out.println("No se encontraron restricciones lineales");
    }
}
