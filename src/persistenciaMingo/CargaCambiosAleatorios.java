/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaMingo;

import TiposEnum.EstadosRec;
import dominio.CambioAleatorio;
import dominio.ConjCambiosAleat;
import dominio.ConjRecB;
import dominio.ListaReales;
import dominio.Recurso;
import dominio.RecursoBase;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import naturaleza3.GrafoN;
import naturaleza3.VariableNat;

/**
 *
 * @author ut469262
 */
public abstract class CargaCambiosAleatorios {

    public void cargarConjCambiosAleat(String dirArchivo, ConjCambiosAleat conjCA,
            ConjRecB conjRB, GrafoN grafo) throws XcargaDatos {
//            throws FileNotFoundException, IOException, XcargaDatos{
        // Datos de todo el archivo.
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();

        CambioAleatorio cambioA;
        // Lista de los cambios aleatorios a cargar.
        ArrayList<String> listaCambios = new ArrayList<String>();

        // Obtención de todos los datos del archivo.
        LeerDatosArchivo cargador = new LeerDatosArchivo() {
        };
        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        // Carga de la lista de transformaciones a cargar.
        listaCambios = cargarListCamb(datAr);
        // Carga de las transformaciones.
        for (String nombCamb : listaCambios) {
            cambioA = new CambioAleatorio();
            cargarCambio(cambioA, datAr, nombCamb, conjRB, conjCA, grafo);
            // Agregación del recurso base al conjunto de recursos base.
            conjCA.agregar(cambioA);
        }
    }

    private ArrayList<String> cargarListCamb(ArrayList<ArrayList<String>> datAr)
            throws XcargaDatos {
        ArrayList<String> resultado = new ArrayList<String>();
        String iniLista = "&LISTA_CAMBIOS_ALEATORIOS"; // Marca de lista de
        // cambios aleatorios a cargar.
        String finLista = "&FIN"; // Marca de fin de la lista

        boolean encontrado;
        int i, j, k;

        // Búsqueda de la lista de recursos a cargar.
        encontrado = false;
        i = 0;
        while (!encontrado && (i < datAr.size())) {
            if (iniLista.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                encontrado = true;
            }
            i++;
        }
        // Si se encontró la marca de inicio de la lista y
        // no sigue la marca de fin de la lista, se carga la lista de recursos.
        if (encontrado && (i < datAr.size()) &&
                finLista.compareToIgnoreCase(datAr.get(i).get(0)) != 0) {
            for (j = 0; j < datAr.get(i).size(); j++) {
                resultado.add(datAr.get(i).get(j));
            }
        } else {
//            int selec;
//            selec = JOptionPane.showOptionDialog (null, "LA LISTA DE CAMBIOS ALEATORIOS ESTÁ VACÍA",
//            "Consulta sobre Cambios aleatorios", JOptionPane.OK_CANCEL_OPTION,
//            JOptionPane.QUESTION_MESSAGE, null,
//            new Object[] { "Detener la ejecución", "Seguir"}, null);
//            // Devuelve 0, 1 o 2 según la opción
//            if(selec == 0) throw new XcargaDatos("La lista de cambios aleatorios está vacía.");
        }

        return resultado;
    }


    /**
     * Carga los cambios aleatorios.
     * Presupone que ya se han leído los RecursosBase y el GrafoN
     * y por lo tanto fue leída la VariableNat asociada al cambio.
     * @param cambioA
     * @param datAr
     * @param nombCamb
     * @param conjRB
     * @param conjCA
     * @param grafo
     * @throws XcargaDatos
     */
    private void cargarCambio(CambioAleatorio cambioA, ArrayList<ArrayList<String>> datAr,
            String nombCamb, ConjRecB conjRB, ConjCambiosAleat conjCA,
            GrafoN grafo) throws XcargaDatos {
        // Marca de inicio de los datos de un cambio aleatorio
        String ini = "&CAMBIO_ALEATORIO";
        // Marca de fin de los datos de un cambio aleatorio
        String fin = "&FIN";
        String dato;
        int i, j;
        ArrayList<Integer> auxI;
        ArrayList<Double> auxD;
        ArrayList<String> auxS;
        ListaReales auxLR;
        boolean encontrado = false;
        boolean noSeReconoceEtiqueta = false;


        // Búsqueda del nombre del cambio aleatorio a cargar.
        i = 0;
        while (!encontrado && (i + 1 < datAr.size())) {
            if (ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                if (datAr.get(i + 1).size() > 1 && 0 ==
                        nombCamb.compareToIgnoreCase(datAr.get(i + 1).get(1))) {
                    encontrado = true;
                }
            }
            i++;
        }
        if (!encontrado) {
            throw new XcargaDatos("No se encontraron los datos del cambio aleatorio: " +
                    nombCamb);
        }

        while (i < datAr.size() &&
                fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0) {
            //Carga la información correspondiente a los datos del recurso base.
            try {
                if (dato.equalsIgnoreCase("nombre")) {
                    cambioA.setNombre(datAr.get(i).get(1));
                    cambioA.setNomVN(datAr.get(i).get(1) + "_CAL");
                    cambioA.setVarNat(grafo.devuelveVN(datAr.get(i).get(1) + "_CAL"));
                    if (conjCA.getUnCambioA(datAr.get(i).get(1)) != null) {
                        throw new XcargaDatos("Aparecen dos cambios aleatorios de nombre" + dato);
                    }
                } else if (dato.equalsIgnoreCase("recurso_origen")) {

                    if (conjRB.getUnRecB(datAr.get(i).get(1)) != null) {
                        int mult = Integer.parseInt(datAr.get(i).get(3));
                        String estado = EstadosRec.valueOf(datAr.get(i).get(2));
                        RecursoBase rb = conjRB.getUnRecB(datAr.get(i).get(1));
                        Recurso rec = new Recurso(rb, estado, 1);
                        cambioA.addRecOrigen(rec);
                        cambioA.addMulOrigen(mult);                                    
                    } else {
                        throw new XcargaDatos("No existe el recurso origen " + datAr.get(i).get(1));
                    }                                      
                    
                } else if (dato.equalsIgnoreCase("recurso_destino")) {
                    if (conjRB.getUnRecB(datAr.get(i).get(1)) != null) {                    
                        int mult = Integer.parseInt(datAr.get(i).get(3));
                        String estado = EstadosRec.valueOf(datAr.get(i).get(2));
                        RecursoBase rb = conjRB.getUnRecB(datAr.get(i).get(1));
                        Recurso rec = new Recurso(rb, estado, 1);
                        cambioA.addRecOrigen(rec);
                        cambioA.addMulOrigen(mult);         
                    } else {
                        throw new XcargaDatos("No existe el recurso destino " + datAr.get(i).get(1));
                    }            

                } else {
                    noSeReconoceEtiqueta = true;
                }
            } catch (Exception ex) {
                System.out.println(ex.toString());
                throw new XcargaDatos("Cambio aleatorio: " + nombCamb +
                        ". No se pudo cargar el dato: " + dato +
                        ". Se generó la excepción: " + ex.toString());
            } // Fin del catch.
            if (noSeReconoceEtiqueta) {
                throw new XcargaDatos("Recurso: " + nombCamb +
                        ". No se reconoce la etiqueta: " + dato);
            }
            i++;
        } // Fin de while.
        /** Verifica los datos de valores de la variable aleatoria
         * - si el nombre de la variable aleatoria
         *   no existe en el grafo de la naturaleza la excepción la lanza el
         *   método devuelveVN
         * - los valores que puede tomar son si y no
         */
        VariableNat vn = new VariableNat();
        vn = cambioA.getVarNat();
        boolean verificaValores;
        for (String st : vn.getValores()) {
            if (!(st.equalsIgnoreCase("SI") || st.equalsIgnoreCase("NO"))) {
                throw new XcargaDatos("Los valores de la variable" +
                        "aleatoria del cambio aleatorio " + nombCamb +
                        "no coinciden con los del grafo de la naturaleza");
            }
        }
    }
}
