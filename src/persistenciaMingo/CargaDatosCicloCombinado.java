package persistenciaMingo;

import java.util.ArrayList;

import dominio.Combustible;
import dominio.Estudio;
import dominio.GeneradorCicloCombinado;
import dominio.GeneradorTermico;
import dominio.ListaReales;

public class CargaDatosCicloCombinado {
	
    public void cargar(ArrayList<ArrayList<String>> datAr, GeneradorCicloCombinado genCC,
            Estudio est) throws XcargaDatos {
         //  A partir del texto en datAr carga los datos del GeneradorTermico genCC que ya existe
         //  y tiene los datos de RecursoBase de ese generador térmico

         // Marca de inicio de los datos específicos de un recurso base.
         String ini = "&DATOS_ESPECIFICOS";
         // Marca de fin de los datos específicos de un recurso base.
         String fin = "&FIN";

         String dato;
         int i, j;
         ArrayList<Integer> auxI;
         ArrayList<Double> auxD;
         ListaReales auxLR;
         boolean encontrado = false;
         boolean noSeReconoceEtiqueta = false;

         // Búsqueda del nombre del recurso base a cargar.
         i = 0;
         while (!encontrado && (i + 1 < datAr.size())) {
              if (ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                   if (datAr.get(i + 1).size() > 1 && 0 ==
                           genCC.getNombre().compareToIgnoreCase(datAr.get(i + 1).get(1))) {
                        encontrado = true;
                   }
              }
              i++;
         }
         if (!encontrado) {
              throw new XcargaDatos("No se encontraron los datos específicos del recurso: " +
                      genCC.getNombre());
         }

         while (i < datAr.size() &&
                 fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0) {
              //Carga la información correspondiente a los datos del recurso base.
              try {
                   if (0 == dato.compareToIgnoreCase("nombre")) {
                   } else if (0 == dato.compareToIgnoreCase("mintec")) {
                        genCC.setMintec(datAr.get(i).get(1));
                   } else if (0 == dato.compareToIgnoreCase("potencia_Mintec")) {
                        genCC.setPotenciaMintec(Double.parseDouble(datAr.get(i).get(1)));
                   } else if (0 == dato.compareToIgnoreCase("dpotan")) {
                        genCC.setDpotan(Double.parseDouble(datAr.get(i).get(1)));
                   } else if (0 == dato.compareToIgnoreCase("rend_potencia_nom")) {
                        genCC.setRendPotenciaNom(Double.parseDouble(datAr.get(i).get(1)));
                   } else if (0 == dato.compareToIgnoreCase("rend_potencia_min")) {
                        genCC.setRendPotenciaMin(Double.parseDouble(datAr.get(i).get(1)));
                   } else if (0 == dato.compareToIgnoreCase("drenan")) {
                        genCC.setDrenan(Double.parseDouble(datAr.get(i).get(1)));
                   } else if (0 == dato.compareToIgnoreCase("disponibilidad")) {
                        genCC.setDisponibilidad(Double.parseDouble(datAr.get(i).get(1)));
                   } else if (0 == dato.compareToIgnoreCase("combustibles")) {
                        for (j = 1; j < datAr.get(i).size(); j++) {
                             String nom = datAr.get(i).get(j);
                             Combustible comb = (Combustible) est.getConjFuentes().fuenteDeNombre(nom);
                             genCC.getCombustibles().add(comb);
                        }
                   } else if (0 == dato.compareToIgnoreCase("dpotcs")) {
                        genCC.setdPotCS(Double.parseDouble(datAr.get(i).get(1)));
                   } else if (0 == dato.compareToIgnoreCase("drencs")) {
                        genCC.setdRenCS(Double.parseDouble(datAr.get(i).get(1)));
                   } else if (0 == dato.compareToIgnoreCase("coymv")) {
                        genCC.setcOYMV(Double.parseDouble(datAr.get(i).get(1)));
                   } else if (0 == dato.compareToIgnoreCase("coymf")) {
                        genCC.setcOYMF(Double.parseDouble(datAr.get(i).get(1)));
                   } else if (0 == dato.compareToIgnoreCase("manten")) {
                   	ArrayList<String> aux = new ArrayList<String>();
                       for (j = 1; j < datAr.get(i).size(); j++) {
                            aux.add(datAr.get(i).get(j));
                       }
                       genCC.getManten().add(aux);
                   } else {
                        noSeReconoceEtiqueta = true;
                   }
              } catch (Exception ex) {
                   System.out.println(ex.toString());
                   throw new XcargaDatos("Generador ciclo combinado: " + genCC.getNombre() +
                           ". No se pudo cargar el dato: " + dato +
                           ". Se generó la excepción: " + ex.toString());
              } // Fin del catch.
              if (noSeReconoceEtiqueta) {
                   throw new XcargaDatos("Recurso: " + genCC.getNombre() +
                           ". No se reconoce la etiqueta: " + dato);
              }
              i++;
         } // Fin de while.

    }
}
	


