/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaMingo;

/**
 *
 * @author ut469262
 */
import GrafoEstados.Numerario;
import TiposEnum.EstadosRec;
import dominio.CambioAleatorio;
import dominio.ConjCambiosAleat;
import dominio.ConjRecB;
import dominio.ConjTransB;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import dominio.ListaReales;
import dominio.Recurso;
import dominio.RecursoBase;
import dominio.TransforBase;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ut600232
 */
public abstract class CargaTransBase {

    public void cargarConjTransBase(String dirArchivo, ConjTransB conjTRB,
            ConjRecB conjRB, ConjCambiosAleat conjCA, Estudio est) throws XcargaDatos {
//            throws FileNotFoundException, IOException, XcargaDatos{
        // Datos de todo el archivo.
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        // Datos del recurso base a cargar.
        TransforBase transB;
        // Lista de los recursos base a cargar.
        ArrayList<String> listaTransfor = new ArrayList<String>();

        // ObtenciÃ³n de todos los datos del archivo.
        LeerDatosArchivo cargador = new LeerDatosArchivo() {
        };
        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        // Carga de la lista de transformaciones a cargar.
        listaTransfor = cargarListTrans(datAr);

        // Carga de las transformaciones.
        for (String nombTrans : listaTransfor) {
            transB = new TransforBase(est);
            cargarTrans(transB, datAr, nombTrans, conjRB, conjTRB, conjCA, est);
            // AgregaciÃ³n del recurso base al conjunto de recursos base.
            conjTRB.agregar(transB);
        }
    }

    public ArrayList<String> cargarListTrans(ArrayList<ArrayList<String>> datAr)
            throws XcargaDatos {
        ArrayList<String> resultado = new ArrayList<String>();
        String iniLista = "&LISTA_TRANSFORMACIONES_BASE"; // Marca de lista de
        // recursos base a cargar.
        String finLista = "&FIN"; // Marca de fin de la lista de recuros
        // base a cargar.
        boolean encontrado;
        int i, j, k;

        // BÃºsqueda de la lista de recursos a cargar.
        encontrado = false;
        i = 0;
        while (!encontrado && (i < datAr.size())) {
            if (iniLista.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                encontrado = true;
            }
            i++;
        }
        // Si se encontrÃ³ la marca de inicio de la lista y
        // no sigue la marca de fin de la lista, se carga la lista de transformaciones.
        if (encontrado && (i < datAr.size()) &&
                finLista.compareToIgnoreCase(datAr.get(i).get(0)) != 0) {
            for (j = 0; j < datAr.get(i).size(); j++) {
                resultado.add(datAr.get(i).get(j));
            }
        } else {
//            int selec;
//            selec = JOptionPane.showOptionDialog(null, "LA LISTA DE TRANSFORMACIONES BASE ESTÃ� VACÃ�A",
//                    "Consulta sobre TranformacionesBase", JOptionPane.OK_CANCEL_OPTION,
//                    JOptionPane.QUESTION_MESSAGE, null,
//                    new Object[]{"Detener la ejecuciÃ³n", "Seguir"}, null);
//            // Devuelve 0, 1 o 2 segÃºn la opciÃ³n
//            if (selec == 0) {
//                throw new XcargaDatos("No se encontrÃ³ la lista de transformaciones.");
//            }
        }
        return resultado;
    }

    private void cargarTrans(TransforBase transB, ArrayList<ArrayList<String>> datAr,
            String nombTrans, ConjRecB conjRB, ConjTransB conjTRB, ConjCambiosAleat conjCA, Estudio est) throws XcargaDatos {
        // Marca de inicio de los datos de un recurso base.
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int cantNum = datGen.getNumerarios().size();
        String ini = "&TRANSFORMACION_BASE";
        // Marca de fin de los datos de un recurso base.
        String fin = "&FIN";
        String dato;
        int i, j;
        ArrayList<Integer> auxI;
        ArrayList<Double> auxD;
        ArrayList<String> auxS;
        ListaReales auxLR;
        boolean encontrado = false;
        boolean noSeReconoceEtiqueta = false;
        // ojo tengo que sacar de algun lado la lista de recursos base

        // BÃºsqueda del nombre de la transformaciÃ³n a cargar.
        i = 0;
        while (!encontrado && (i + 1 < datAr.size())) {
            if (ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                if (datAr.get(i + 1).size() > 1 && 0 ==
                        nombTrans.compareToIgnoreCase(datAr.get(i + 1).get(1))) {
                    encontrado = true;
                }
            }
            i++;
        }
        if (!encontrado) {
            throw new XcargaDatos("No se encontraron los datos de la transformaciÃ³n: " +
                    nombTrans);
        }
// System.out.println("- Se va a iniciar el while.(" + nombTrans + ")\n");

        while (i < datAr.size() &&
                fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0) {
            //Carga la informaciÃ³n correspondiente a los datos del recurso base.
            try {
                if (dato.equalsIgnoreCase("nombre")) {
                    transB.setNombre(datAr.get(i).get(1));
                    if (conjTRB.getUnaTransB(transB.getNombre()) != null) {
                        throw new XcargaDatos("Aparecen dos transformaciones de nombre " + transB.getNombre());
                    }
                } else if (dato.equalsIgnoreCase("clase")) {
                    transB.setClase(datAr.get(i).get(1));
                } else if (dato.equalsIgnoreCase("tipo")) {
                    transB.setTipo(datAr.get(i).get(1));
                } else if (dato.equalsIgnoreCase("recurso_origen")) {
                    
                    
                    if (conjRB.getUnRecB(datAr.get(i).get(1)) != null) {
                        int mult = Integer.parseInt(datAr.get(i).get(3));
                        String estado = EstadosRec.valueOf(datAr.get(i).get(2));
                        RecursoBase rb = conjRB.getUnRecB(datAr.get(i).get(1));
                        Recurso rec = new Recurso(rb, estado, 1);
                        transB.addRecOrigen(rec);
                        transB.addMulOrigen(mult);
                        transB.addMinVidaRes(Integer.parseInt(datAr.get(i).get(4)));
                    } else {
                        throw new XcargaDatos("No existe el recurso origen " + datAr.get(i).get(1));
                    }                                   
                
                    
                } else if (dato.equalsIgnoreCase("recurso_transitorio")) {
                    
                    
                    if (conjRB.getUnRecB(datAr.get(i).get(1)) != null) {
                        // Los recursos transitorios tienen siempre indicador de tiempo 0, demora 0 e inversiÃ³n aleatoria 1
                        int mult = Integer.parseInt(datAr.get(i).get(3));
                        String estado = EstadosRec.valueOf(datAr.get(i).get(2));
                        RecursoBase rb = conjRB.getUnRecB(datAr.get(i).get(1));
                        Recurso rec = new Recurso(rb, estado, 1);
                        rec.setValorDemAl(0);
                        rec.setValorInvAl(1.0);
                        transB.addRecTransit(rec);
                        transB.addMulTransit(mult);

                    } else {
                        throw new XcargaDatos("No existe el recurso origen " + datAr.get(i).get(1));
                    }                                   
                                    
                    
                 } else if (dato.equalsIgnoreCase("recurso_destino")) {
                    
                    
                    if (conjRB.getUnRecB(datAr.get(i).get(1)) != null) {
                        int mult = Integer.parseInt(datAr.get(i).get(3));
                        String estado = EstadosRec.valueOf(datAr.get(i).get(2));
                        RecursoBase rb = conjRB.getUnRecB(datAr.get(i).get(1));
                        Recurso rec = new Recurso(rb, estado, 1);
                        transB.addRecDestino(rec);
                        transB.addMulDestino(mult);
                        transB.addEdadRecDestino(datAr.get(i).get(4));
                    } else {
                        throw new XcargaDatos("No existe el recurso origen " + datAr.get(i).get(1));
                    }                     
                                       

                } else if (0 == dato.compareToIgnoreCase("periodo_const")) {
                    transB.setPerConstr(Integer.parseInt(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("demora_aleat")) {
                    transB.setTieneDemoraAl(0 ==
                            "si".compareToIgnoreCase(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("inversion")) {
                    Numerario num = datGen.devuelveNumerario(datAr.get(i).get(1));
                    if (num == null) {
                        throw new XcargaDatos("No existe el numerario de nombre " +
                                datAr.get(i).get(1) + " que aparece en " + transB.getNombre());
                    }
                    int indNum = datGen.getNumerarios().indexOf(num);
                    auxD = new ArrayList<Double>();
                    for (j = 2; j < datAr.get(i).size(); j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }
                    transB.getInversiones().set(indNum, auxD);

// EL CODIGO SIGUIENTE SE ELIMINO PORQUE LAS TRANSFORMACIONES BASE NO TIENEN COSTO FIJO
// EL COSTO FIJO LO TIENEN LOS RECURSOS RESULTANTES.
//                }else if( 0 == dato.compareToIgnoreCase("costo_fijo") ){
//                   Numerario num = datGen.devuelveNumerario(datAr.get(i).get(1));
//                   if(num == null) throw new XcargaDatos("No existe el numerario de nombre "+
//                           datAr.get(i).get(1) + " que aparece en " + transB.getNombre());
//                   int indNum = datGen.getNumerarios().indexOf(num);
//                   auxD = new ArrayList<Double>();
//                   for(j = 2; j < datAr.get(i).size(); j++){
//                       auxD.add(Double.parseDouble(datAr.get(i).get(j)));
//                   }
//                   transB.getCostosFijos().set(indNum, auxD);
                    
                } else if(0 == dato.compareToIgnoreCase("ESCALAMIENTO_INV")){
                	auxD = new ArrayList<Double>();
                	for (j = 1; j < datAr.get(i).size(); j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }
                	transB.setEscalamientoInv(auxD);      	            	
                } else if(0 == dato.compareToIgnoreCase("ESCALAMIENTO_CFIJO")){
                	auxD = new ArrayList<Double>();
                	for (j = 1; j < datAr.get(i).size(); j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }
                	transB.setEscalamientocFijo(auxD);                     		

                } else if (0 == dato.compareToIgnoreCase("valores_dem_aleat")) {
                    auxS = new ArrayList<String>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxS.add(datAr.get(i).get(j));
                    }
                    transB.setValoresDemAlS(auxS);
                } else if (0 == dato.compareToIgnoreCase("producto")) {
                    auxLR = new ListaReales(datAr.get(i).get(1));
                    for (j = 2; j < datAr.get(i).size(); j++) {
                        auxLR.addValor(Double.parseDouble(datAr.get(i).get(j)));
                    }

                } else if (0 == dato.compareToIgnoreCase("inversion_aleat")) {
                    transB.setTieneInvAleat(0 ==
                            "si".compareToIgnoreCase(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("valores_inv_aleat")) {
                    auxS = new ArrayList<String>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxS.add(datAr.get(i).get(j));
                    }
                    transB.setValoresInvAlS(auxS);
                } else {
                    noSeReconoceEtiqueta = true;
                }
            } catch (Exception ex) {
                System.out.println(ex.toString());
                throw new XcargaDatos("TransformaciÃ³n: " + nombTrans +
                        ". No se pudo cargar el dato: " + dato +
                        ". Se generÃ³ la excepciÃ³n: " + ex.toString());
            } // Fin del catch.
            if (noSeReconoceEtiqueta) {
                throw new XcargaDatos("Recurso: " + nombTrans +
                        ". No se reconoce la etiqueta: " + dato);
            }
            i++;
        } // Fin de while.

        /**
         * Carga datos por defecto de variables que no hayan sido leÃ­das
         */
        for (int inum = 0; inum < cantNum; inum++) {
//           if(transB.getCostosFijos().get(inum).isEmpty()) transB.getCostosFijos().get(inum).add(0.0);
            if (transB.getInversiones().get(inum).isEmpty()) {
                transB.getInversiones().get(inum).add(0.0);
            }
        }

        /**
         * Verifica que los recursosBase origen no estÃ¡n sujetos a ningÃºn
         * cambio aleatorio
         */
        for (Recurso rec : transB.getRecursosOrigen()) {
            for (CambioAleatorio ca : conjCA.getCambios()) {
                
                if(ca.aplicaARB(rec.devuelveRB())){                                 
                    throw new XcargaDatos(
                            "El recursoBase " + rec.getRecBase().getNombre() + " que aparece en una transformacion " +
                            "estÃ¡ sujeto a cambio aleatorio");
                }
            }
        }

    }        
}
