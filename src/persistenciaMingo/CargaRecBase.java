/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaMingo;

import GrafoEstados.Numerario;
import TiposEnum.ExistEleg;
import TiposEnum.SubTipo1;
import TiposEnum.SubTipo2;
import TiposEnum.TipoRec;
import dominio.Acumulador;
import dominio.BiomasaEstac;
import dominio.BiomasaFormul;
import dominio.ConjRecB;
import dominio.DatosGeneralesEstudio;
import dominio.ExportEstac;
import dominio.ExportFormul;
import dominio.GeneradorCicloCombinado;
import dominio.GeneradorEolico;
import dominio.ListaReales;
import dominio.Punto;
import dominio.RecursoBase;
import dominio.GeneradorTermico;
import dominio.GeneradorHidro;
import dominio.GeneradorFotovoltaico;
import dominio.ImportEstac;
import dominio.ImportFormul;
import dominio.CantProductoSimple;
import dominio.Estudio;
import dominio.Producto;

//import dominio.RecursoBase.TipoRec;
import dominio.TiempoAbsoluto;
import java.util.ArrayList;

/**
 *
 * @author ut600232
 */
public abstract class CargaRecBase {

    public void cargarConjRecBase(String dirArchivo, ConjRecB conjRB, Estudio est) throws XcargaDatos {
//            throws FileNotFoundException, IOException, XcargaDatos{
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        // Datos de todo el archivo.
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        // Datos del recurso base a cargar.

        // Lista de los recursos base a cargar.
        ArrayList<String> listaRecursos = new ArrayList<String>();

        // Obtenci�n de todos los datos del archivo.

        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        // Carga de la lista de recursos a cargar.
        listaRecursos = cargarListRec(datAr);
        // Carga de los recursos.
        for (String nombRec : listaRecursos) {
//            recB = new RecursoBase();
            cargarRec(datAr, nombRec, conjRB, est);
            // Agregación del recurso base al conjunto de recursos base.
//            conjRB.agregar(recB);
        }
    }

    public ArrayList<String> cargarListRec(ArrayList<ArrayList<String>> datAr)
            throws XcargaDatos {
        ArrayList<String> resultado = new ArrayList<String>();
        String iniListaRecB = "&LISTA_RECURSOS_BASE"; // Marca de lista de
        // recursos base a cargar.
        String listaFin = "&FIN"; // Marca de fin de la lista de recuros
        // base a cargar.
        boolean encontrado;
        int i, j;
        // B�squeda de la lista de recursos a cargar.
        encontrado = false;
        i = 0;
        while (!encontrado && (i < datAr.size())) {
            if (iniListaRecB.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                encontrado = true;
            }
            i++;
        }
        // Si se encontr� la marca de inicio de la lista y
        // no sigue la marca de fin de la lista, se carga la lista de recursos.
        if (encontrado && (i < datAr.size()) &&
                listaFin.compareToIgnoreCase(datAr.get(i).get(0)) != 0) {
            for (j = 0; j < datAr.get(i).size(); j++) {
                resultado.add(datAr.get(i).get(j));
            }
        } else {
            throw new XcargaDatos("No se encontr� la lista de recursos.");
        }
        return resultado;
    }


    /**
     * Carga los datos de un RecursoBase.
     * Presupone que se ha leído el GrafoN y por lo tanto que existen
     * las variables aleatorias que el RecursoBase requiera para demora aleatoria,
     * inversi�n aleatoria y costo fijo aleatorio.
     */
    private void cargarRec(ArrayList<ArrayList<String>> datAr,
            String nombRec, ConjRecB conjRB, Estudio est) throws XcargaDatos {

        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int cantPasosEst = datGen.getCantPasos();
        int cantNum = datGen.getNumerarios().size();
        // Marca de inicio de los datos de un recurso base.
        String ini = "&RECURSO_BASE";
        // Marca de fin de los datos de un recurso base.
        String fin = "&FIN";
        RecursoBase recB = new RecursoBase(est);

        Punto pConex;
        String dato;
        int i, j;
        ArrayList<Integer> auxI;
        ArrayList<Double> auxD;
        ArrayList<String> auxS;
        ListaReales auxLR;
        boolean encontrado = false;
        boolean noSeReconoceEtiqueta = false;

        // B�squeda del nombre del recurso a cargar.
        i = 0;
        while (!encontrado && (i + 1 < datAr.size())) {
            if (ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                if (datAr.get(i + 1).size() > 1 && 0 ==
                        nombRec.compareToIgnoreCase(datAr.get(i + 1).get(1))) {
                    encontrado = true;
                }
            }
            i++;
        }
        if (!encontrado) {
            throw new XcargaDatos("No se encontraron los datos del recurso: " +
                    nombRec);
        }

        while (i < datAr.size() &&
                fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0) {
            //Carga la información correspondiente a los datos del recurso base.
            try {
                if (0 == dato.compareToIgnoreCase("nombre")) {
                    recB.setNombre(datAr.get(i).get(1));
                    System.out.println(datAr.get(i).get(1));
                    if (conjRB.getUnRecB(recB.getNombre()) != null) {
                        throw new XcargaDatos("Recurso: " + nombRec +
                                " aparece dos veces ");
                    }
                } else if (0 == dato.compareToIgnoreCase("clase")) {
                    recB.setClase(ExistEleg.valueOf(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("equiv_en_simul_a")) {
                    recB.setNombreEquivEnSimul(datAr.get(i).get(1));
                } else if (0 == dato.compareToIgnoreCase("potencia_nominal")) {
                    auxD = new ArrayList<Double>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }
                    recB.setPotNominalTABase(auxD.get(0));
                    recB.setPotNominalT(auxD);
                } else if (0 == dato.compareToIgnoreCase("cant_modulos")) {
                    auxI = new ArrayList<Integer>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxI.add(Integer.parseInt(datAr.get(i).get(j)));
                    }
//				   recB.setCantModulos(auxI.get(0));
                    recB.setCantModulosT(auxI);
                } else if (0 == dato.compareToIgnoreCase("punto")) {
                    /*
                    pConex = new Punto( Double.parseDouble(datAr.get(i).get(1)),
                    Double.parseDouble(datAr.get(i).get(2)),
                    Double.parseDouble(datAr.get(i).get(3)) );
                    recB.setPuntoConex(pConex);
                     */
                } else if (0 == dato.compareToIgnoreCase("tipo")) {
                    if (datAr.get(i).get(1).equalsIgnoreCase("generador")) {
                        recB.setTipo(TipoRec.GEN);
                    } else if (datAr.get(i).get(1).equalsIgnoreCase("carga")) {
                        recB.setTipo(TipoRec.CARG);
                    }
                } else if (0 == dato.compareToIgnoreCase("subtipo1")) {
                    recB.setSubtipo1(SubTipo1.valueOf(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("subtipo2")) {
                    recB.setSubtipo2(datAr.get(i).get(1));
                } else if (0 == dato.compareToIgnoreCase("tiene_edad")) {
                    recB.setTieneEdad(0 ==
                            "si".compareToIgnoreCase(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("depende_tiempo")) {
                    recB.setDependenciaDeT(datAr.get(i).get(1));
//                }else if( 0 == dato.compareToIgnoreCase("depende_segun") ){
//                    recB.setDependenciaSegun(datAr.get(i).get(1));
                } else if (0 == dato.compareToIgnoreCase("tiempo_absoluto_base")) {
                    recB.setTiempoAbsolutoBase(new TiempoAbsoluto(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("ultimo_tiempo_absoluto")) {
                    recB.setUltimoTiempoAbsoluto(new TiempoAbsoluto(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("vida_util")) {
                    recB.setVidaUtil(Integer.parseInt(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("periodo_const")) {
                    recB.setPerConstr(Integer.parseInt(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("demora_aleat")) {
                    recB.setTieneDemoraAl(0 ==
                            "si".compareToIgnoreCase(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("valores_dem_aleat")) {
                    auxS = new ArrayList<String>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxS.add(datAr.get(i).get(j));
                    }
                    recB.setValoresDemAlS(auxS);
                } else if (0 == dato.compareToIgnoreCase("producto")) {
                    Producto p = new Producto();
                    p = datGen.devuelveProducto(datAr.get(i).get(1));
                    if (p == null) {
                        throw new XcargaDatos("El producto " + datAr.get(i).get(1) +
                                " no existe");
                    } else {
                        CantProductoSimple of =
                                new CantProductoSimple(p,
                                Double.parseDouble(datAr.get(i).get(2)));
                        recB.getCantsDeProductos1Mod().add(of);

                    }
                } else if (0 == dato.compareToIgnoreCase("inversion")) {
                    Numerario num = datGen.devuelveNumerario(datAr.get(i).get(1));
                    if (num == null) {
                        throw new XcargaDatos("No existe el numerario de nombre " +
                                datAr.get(i).get(1) + " que aparece en " + recB.getNombre());
                    }
                    int indNum = datGen.getNumerarios().indexOf(num);
                    auxD = new ArrayList<Double>();
                    for (j = 2; j < datAr.get(i).size(); j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }
                    recB.getInversiones().set(indNum, auxD);

                } else if (0 == dato.compareToIgnoreCase("costo_fijo")) {
                    Numerario num = datGen.devuelveNumerario(datAr.get(i).get(1));
                    if (num == null) {
                        throw new XcargaDatos("No existe el numerario de nombre " +
                                datAr.get(i).get(1) + " que aparece en " + recB.getNombre());
                    }
                    int indNum = datGen.getNumerarios().indexOf(num);
                    auxD = new ArrayList<Double>();
                    for (j = 2; j < datAr.get(i).size(); j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }
                    recB.getCostosFijos().set(indNum, auxD);

                } else if (0 == dato.compareToIgnoreCase("inversion_aleat")) {
                    recB.setTieneInvAleat(0 ==
                            "si".compareToIgnoreCase(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("valores_inv_aleat")) {
                    auxS = new ArrayList<String>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxS.add(datAr.get(i).get(j));
                    }
                    recB.setValoresInvAlS(auxS);
                } else if (0 == dato.compareToIgnoreCase("costo_fijo_aleat")) {                    
                    recB.setTieneCFijAleat(0 ==
                            "si".compareToIgnoreCase(datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("valores_cfijo_aleat")) {
                    auxS = new ArrayList<String>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxS.add(datAr.get(i).get(j));
                    }
                    recB.setValoresCFijAlS(auxS);
                } else if (0 == dato.compareToIgnoreCase("aleat_operativa")) {
                    recB.setAleatOper(datAr.get(i).get(1));                   
                } else if(0 == dato.compareToIgnoreCase("ESCALAMIENTO_INV")){
                	auxD = new ArrayList<Double>();
                	for (j = 1; j < datAr.get(i).size(); j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }                	
                	recB.setEscalamientoInv(auxD);      	            	
                } else if(0 == dato.compareToIgnoreCase("ESCALAMIENTO_CFIJO")){
                	auxD = new ArrayList<Double>();
                	for (j = 1; j < datAr.get(i).size(); j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }
                	recB.setEscalamientocFijo(auxD);  
                } else if(0 == dato.compareToIgnoreCase("ENTRADA_DATOS")){
                	if(datAr.get(i).get(1).equals("EXTERNA")) recB.setLecturaDatosExt(true);                	
                } else {
                    noSeReconoceEtiqueta = true;
                }
//                if(!recB.getDependenciaDeT().equalsIgnoreCase("NO") && recB.getEscalamientocFijo()!=null){
//                	System.out.println("EL RECURSO TIENE DEPENDENCIA DEL TIEMPO Y ESCALAMIENTO DE COSTOS FIJOS");
//                	System.exit(1);
//                }
                if(!recB.getDependenciaDeT().equalsIgnoreCase("NO") && recB.getClase().equalsIgnoreCase("ELEG") && recB.getEscalamientocFijo()!=null){
                	System.out.println("EL RECURSO ELEGIBLE" + recB.getNombre() + " TIENE DEPENDENCIA DEL TIEMPO Y ESCALAMIENTO DE COSTOS FIJOS");
                	System.exit(1);
                }                	
            } catch (Exception ex) {
                System.out.println(ex.toString());
                throw new XcargaDatos("Recurso: " + nombRec +
                        ". No se pudo cargar el dato: " + dato +
                        ". Se generó la excepción: " + ex.toString());
            } // Fin del catch.
            if (noSeReconoceEtiqueta) {
                throw new XcargaDatos("Recurso: " + nombRec +
                        ". No se reconoce la etiqueta: " + dato);
            }
            i++;
        } // Fin de while.

        /**
         * Carga datos por defecto de variables que no hayan sido leídas
         */
        if (recB.getUltimoTiempoAbsoluto().getAnio() == 0) {
            recB.setUltimoTiempoAbsoluto(datGen.getFinEstudio());
        }
        if (recB.getVidaUtil() == 0) {
            recB.setVidaUtil(99999);
        }
        for (int inum = 0; inum < cantNum; inum++) {
            if (recB.getCostosFijos().get(inum).isEmpty()) {
                recB.getCostosFijos().get(inum).add(0.0);
            }
            if (recB.getInversiones().get(inum).isEmpty()) {
                recB.getInversiones().get(inum).add(0.0);
            }
        }

        /**
         * Verifica errores por datos imprescindibles no entrados
         */
        if (recB.getClase().equalsIgnoreCase(ExistEleg.ELEG) & recB.getPerConstr() < 1) {
            throw new XcargaDatos("Se especific� un per�odo de construcci�n menor que 1 para el " +
                    "RecursoBase elegible " + recB.getNombre());
        }
        if (  (recB.getClase().equalsIgnoreCase(ExistEleg.ELEG) & recB.getVidaUtil()<1) ||
                (recB.getTieneEdad() & recB.getVidaUtil()<1) ) {
            throw new XcargaDatos("Se especificó un período de construcción menor que 1 para el " +
                    "RecursoBase elegible " + recB.getNombre());
        }


        /**
         * Comienza a leer los datos específicos según el tipo de recurso base
         * Antes de invocar la respectiva rutina de carga crea el tipo de recurso base
         * adecuado, de la subclase necesaria. Puede hacerlo porque RecursoBase tiene
         * la información sobre la subclase adecuada
         */
        if (recB.getTipo().equalsIgnoreCase(TipoRec.GEN) && recB.getSubtipo1().equalsIgnoreCase(SubTipo1.TERM)) {
            GeneradorTermico genTer = new GeneradorTermico();
            genTer.cargarRB(recB);
            if(!recB.isLecturaDatosExt()){    
	            CargaDatosTermico cargaTer = new CargaDatosTermico() {};
	            cargaTer.cargar(datAr, genTer, est);
            }
            conjRB.getRecursosB().add(genTer);
            
        } else if (recB.getTipo().equalsIgnoreCase(TipoRec.GEN) && recB.getSubtipo1().equalsIgnoreCase(SubTipo1.CCOMB)) {
            GeneradorCicloCombinado genCC = new GeneradorCicloCombinado();
            genCC.cargarRB(recB);
            if(!recB.isLecturaDatosExt()){    
	            CargaDatosCicloCombinado cargaCC = new CargaDatosCicloCombinado() {};
	            cargaCC.cargar(datAr, genCC, est);
            }
            conjRB.getRecursosB().add(genCC);            

        } else if (recB.getTipo().equalsIgnoreCase(TipoRec.GEN) && recB.getSubtipo1().equalsIgnoreCase(SubTipo1.EOLO)) {
            GeneradorEolico genEol = new GeneradorEolico();
            genEol.cargarRB(recB);
            if(!recB.isLecturaDatosExt()){    
	            CargaDatosEolico cargaEol = new CargaDatosEolico() {};
	            cargaEol.cargar(datAr, genEol);
            }
            conjRB.getRecursosB().add(genEol);
        } else if (recB.getTipo().equalsIgnoreCase(TipoRec.GEN) && recB.getSubtipo1().equalsIgnoreCase(SubTipo1.FOTO)) {
            GeneradorFotovoltaico genSol = new GeneradorFotovoltaico();
            genSol.cargarRB(recB);
            if(!recB.isLecturaDatosExt()){
            CargaDatosFoto cargaSol = new CargaDatosFoto() {};
            cargaSol.cargar(datAr, genSol);            
            }
            conjRB.getRecursosB().add(genSol);            
            
            
        } else if (recB.getTipo().equalsIgnoreCase(TipoRec.GEN) && recB.getSubtipo1().equalsIgnoreCase(SubTipo1.ACUM)) {
            Acumulador acum = new Acumulador();
            acum.cargarRB(recB);
            if(!recB.isLecturaDatosExt()){
            CargaDatosAcum cargaAcum = new CargaDatosAcum() {};
            cargaAcum.cargar(datAr, acum);            
            }
            conjRB.getRecursosB().add(acum);                    

        } else if (recB.getTipo().equalsIgnoreCase(TipoRec.GEN) && recB.getSubtipo1().equalsIgnoreCase(SubTipo1.HID)) {
            GeneradorHidro genHid = new GeneradorHidro();
            genHid.cargarRB(recB);
            if(!recB.isLecturaDatosExt()){    
	            CargaDatosHidro cargaHid = new CargaDatosHidro() {};
	            cargaHid.cargar(datAr, genHid);
            }
            conjRB.getRecursosB().add(genHid);


        } else if (recB.getTipo().equalsIgnoreCase(TipoRec.GEN) && recB.getSubtipo1().equalsIgnoreCase(SubTipo1.IMP)) {

            if (recB.getSubtipo2().equalsIgnoreCase(SubTipo2.ESTAC)) {
                ImportEstac impEst = new ImportEstac();
                impEst.cargarRB(recB);
                if(!recB.isLecturaDatosExt()){
                CargaDatosImpEstac cargaImpEst = new CargaDatosImpEstac() {};
                cargaImpEst.cargar(datAr, impEst);
                }
                conjRB.getRecursosB().add(impEst);

            }
            if (recB.getSubtipo2().equalsIgnoreCase(SubTipo2.FORMUL)) {
                ImportFormul impFor = new ImportFormul();
                impFor.cargarRB(recB);
                if(!recB.isLecturaDatosExt()){
                CargaDatosImpFormul cargaImpFor = new CargaDatosImpFormul() {};                
                cargaImpFor.cargar(datAr, impFor);
                }
                conjRB.getRecursosB().add(impFor);

            }
        } else if (recB.getTipo().equalsIgnoreCase(TipoRec.GEN) && recB.getSubtipo1().equalsIgnoreCase(SubTipo1.BIO)) {

            if (recB.getSubtipo2().equalsIgnoreCase(SubTipo2.ESTAC)) {
                BiomasaEstac bioEst = new BiomasaEstac();
                bioEst.cargarRB(recB);
                if(!recB.isLecturaDatosExt()){
	                CargaDatosBioEstac cargaBioEst = new CargaDatosBioEstac() {};
	                cargaBioEst.cargar(datAr, bioEst);
                }
                conjRB.getRecursosB().add(bioEst);

            }
            if (recB.getSubtipo2().equalsIgnoreCase(SubTipo2.FORMUL)) {
                BiomasaFormul bioFor = new BiomasaFormul();
                bioFor.cargarRB(recB);
                if(!recB.isLecturaDatosExt()){
	                CargaDatosBioFormul cargaBioFor = new CargaDatosBioFormul() {};
	                cargaBioFor.cargar(datAr, bioFor);
                }
                conjRB.getRecursosB().add(bioFor);

            }
            
        } else if (recB.getTipo().equalsIgnoreCase(TipoRec.CARG) 
                && recB.getSubtipo1().equalsIgnoreCase(SubTipo1.EXP)) {

            if (recB.getSubtipo2().equalsIgnoreCase(SubTipo2.ESTAC)) {
                ExportEstac expEst = new ExportEstac();
                expEst.cargarRB(recB);
                if(!recB.isLecturaDatosExt()){
	                CargaDatosExpEstac cargaExpEst = new CargaDatosExpEstac() {};
	                cargaExpEst.cargar(datAr, expEst);
                }
                conjRB.getRecursosB().add(expEst);

            } else if (recB.getSubtipo2().equalsIgnoreCase(SubTipo2.FORMUL)) {
                ExportFormul expFor = new ExportFormul();
                expFor.cargarRB(recB);
                if(!recB.isLecturaDatosExt()){
	                CargaDatosExpFormul cargaExpFor = new CargaDatosExpFormul() {};
	                cargaExpFor.cargar(datAr, expFor);
                }
                conjRB.getRecursosB().add(expFor);

            }

        }
    }
}
