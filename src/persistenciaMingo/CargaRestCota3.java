/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaMingo;

import GrafoEstados.ConjRestGrafoE;
import Restricciones3.RestCota3;
//import dominio.Bloque.EstadosRec;
//import dominio.Bloque.TipoBloque;
import TiposEnum.EstadosRec;
import TiposEnum.ExistEleg;
import dominio.ConjRecB;
import dominio.ConjTransB;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import dominio.RecursoBase;
//import dominio.RecursoBase.ExistEleg;
import dominio.TiempoAbsoluto;
import dominio.TransforBase;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public abstract class CargaRestCota3 {

     public void cargaRestCota(String dirArchivo,
             ConjRecB conjRB, ConjTransB conjTB, Estudio estudio,
             ConjRestGrafoE conRIC) throws XcargaDatos {
          // Busca restricciones de cota de todos los recursos de conjRB
          // y de todas las transformacion de conjTB

          // Datos de todo el archivo.
          System.out.print("Se leen restricciones de cota en   " + dirArchivo + "\n");
          DatosGeneralesEstudio datGen = estudio.getDatosGenerales();

          ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();

          // Obtención de todos los datos del archivo.
//          LeerDatosArchivo cargador = new LeerDatosArchivo() {
//          };
          datAr = LeerDatosArchivo.getDatos(dirArchivo);
          // Carga de la lista de recursos a cargar.
          // Carga de los recursos.
          for (RecursoBase rb : conjRB.getRecursosB()) {
               if (rb.getClase().equalsIgnoreCase(ExistEleg.ELEG)) {
                    cargarRestCota(datAr, rb, null, conRIC, estudio);
               }
          }
          for (TransforBase tr : conjTB.getTransforsB()) {
               cargarRestCota(datAr, null, tr, conRIC, estudio);
          }
     }

     private void cargarRestCota(ArrayList<ArrayList<String>> datAr,
             RecursoBase rb, TransforBase tb, ConjRestGrafoE conRIC,
             Estudio estudio) throws XcargaDatos {

          /**
           * Carga los datos de las restricciones de cota de un RecursoBase o una
           * TransformaciónBase. Puede haber cualquier número de restricciones.
           */
          // Marca de inicio de los datos de un recurso base.
          assert (rb == null | tb == null) : "Método cargarRestInic recibió" +
                  "un recursoBase y una transforBase a la vez";
          String ini;
          String nomb;
          RestCota3 resIC;
          boolean encontrado = false;
          boolean encontro1 = false;
          boolean noSeReconoceEtiqueta = false;

          // Marca de fin de los datos de un recurso base.
          String fin = "&FIN";

          String dato;

          ArrayList<Integer> auxI;
          ArrayList<Double> auxD;
          ArrayList<String> auxS;

          int i = 0;
          int j = 0;


          if (!(rb == null)) {
               nomb = rb.getNombre();
               ini = "&RECURSO_BASE";
          } else {
               nomb = tb.getNombre();
               ini = "&TRANSFORMACION_BASE";
          }

          while (i + 1 < datAr.size()) {

               // Búsqueda del nombre del RecursoBase o TransforBase.

               while (!encontro1 && (i + 1 < datAr.size())) {
                    if (ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                         if (datAr.get(i + 1).size() > 1 && 0 ==
                                 nomb.compareToIgnoreCase(datAr.get(i + 1).get(1))) {
                              encontrado = true;
                              encontro1 = true;
                         }
                    }
                    i++;
               }
               if (encontro1) {
                    // encontró una restricción con el RecursoBase o TransforBase.
                    if (rb != null) {
                         ini = "&RECURSO_BASE";
                         nomb = rb.getNombre();
                         resIC = new RestCota3(estudio, "REC", rb);
                    } else {
                         ini = "&TRANSFORMACION_BASE";
                         nomb = tb.getNombre();
                         resIC = new RestCota3(estudio, "TRANS", tb);
                    }
                    encontro1 = false;
                    while (i < datAr.size() &&
                            fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0) {
                         boolean primeraCota = false;

                         try {
                              if (0 == dato.compareToIgnoreCase("nombre_rest")) {
                                   String nombRest = "";
                                   for(j=1; j<datAr.get(i).size(); j++){
                                        nombRest += datAr.get(i).get(j);
                                        if (j<datAr.get(i).size()-1) nombRest = nombRest + " ";
                                   }
                                   resIC.setNombre(nombRest);
                              } else if (0 == dato.compareToIgnoreCase("tiempo_absoluto")) {
                                   TiempoAbsoluto ta = new TiempoAbsoluto(datAr.get(i).get(1));
                                   if(ta.getAnio() !=estudio.getDatosGenerales().getTiempoAbsolutoPasos().get(0).getAnio() ){
                                       throw new XcargaDatos("El tiempo base de la restricción +"
                                               +resIC.getNombre() +": " +ta.getAnio() +" es distinto que el año "
                                               + "inicial del estudio "+estudio.getDatosGenerales().getTiempoAbsolutoPasos().get(0).getAnio()+"");
                                   }
                                   resIC.setTiempoAbsolutoBase(ta);

                              } else if (0 == dato.compareToIgnoreCase("cota_superior") ||
                                      0 == dato.compareToIgnoreCase("cota_inferior")) {
                                   if (primeraCota == true) {
                                        throw new XcargaDatos("Una restricción tiene dos datos de cota");
                                   }
                                   primeraCota = true;
                                   for (j = 1; j < datAr.get(i).size(); j++) {
                                        resIC.getCota().add(Integer.parseInt(datAr.get(i).get(j)));
                                   }
                                   if (0 == dato.compareToIgnoreCase("cota_superior")) {
                                        resIC.setInfSup("SUP");
                                   } else {
                                        resIC.setInfSup("INF");
                                   }
                              } else if (0 == dato.compareToIgnoreCase("es_heuristica")) {
                                   String s = datAr.get(i).get(1);
                                   if (s.equalsIgnoreCase("SI")) {
                                        resIC.setEsHeuristica(true);
                                   } else if (s.equalsIgnoreCase("NO")) {
                                        resIC.setEsHeuristica(false);
                                   } else {
                                        throw new XcargaDatos("La restricción no especifica SI o NO en Heurística");
                                   }
                              } else if (0 == dato.compareToIgnoreCase("estados_rec")) {
                                   String s = datAr.get(i).get(1);
                                   resIC.setEstadosRec(EstadosRec.valueOf(s));
                              } else if (0 == dato.compareToIgnoreCase("nombre")) {
                                   // EL NOMBRE YA SE RECONOCIÓ AL PRINCIPIO, no se hace nada
                              } else {
                                   noSeReconoceEtiqueta = true;
                              }


                         } catch (Exception ex) {
                              System.out.println(ex.toString());
                              throw new XcargaDatos("Recurso o tranformación: " + nomb +
                                      ". No se pudo cargar el dato: " + dato +
                                      ". Se generó la excepción: " + ex.toString());
                         } // Fin del catch.
                         if (noSeReconoceEtiqueta) {
                              throw new XcargaDatos("Recurso o transformacion: " + nomb +
                                      ". No se reconoce la etiqueta: " + dato);
                         }
                         i++;
                    } // Fin de while.

                    if (resIC.isEsHeuristica()) {
                         resIC.setEstadosRec(EstadosRec.OPE);
                    } else {
                         if (resIC.getEstadosRec() == null) {
                              throw new XcargaDatos("La restricción de cota de " +
                                      nomb + " no especifica ESTADOS_REC");
                         }
                    }
                    conRIC.getRestricciones().add(resIC);
               } // Fin del if
          }  // Fin de while buscando restricciones
          if (!encontrado) {
               System.out.println("No se encontraron restricciones de cota del RecursoBase o Tranformacion " + nomb);
          }
     }
}
