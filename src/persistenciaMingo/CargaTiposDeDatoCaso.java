/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistenciaMingo;
import dominio.Caso;
import dominio.ConjTiposDeDato;
import dominio.Estudio;
import dominio.ParTipoDatoValor;
import dominio.TipoDeDatos;
import java.util.ArrayList;

/**
 * Carga los valores de los TipoDeDatos fijos del Caso si difieren de los del Estudio
 * @author ut469262
 */
public class CargaTiposDeDatoCaso {

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */







    /**
     * 
     * @param dirArchivo es el archivo incluso path donde están los valores de los tipos de dato
     * fijo del caso.
     * @param conjTD es el conjunto de tipos de datos del Estudio
     * @param informacion
     * @throws persistenciaMingo.XcargaDatos
     */
    public static void cargarTiposDato(String dirArchivo,
        ConjTiposDeDato conjTD, ArrayList<Object> informacion) throws XcargaDatos{

        Estudio est = (Estudio)informacion.get(0);
        Caso caso = (Caso)informacion.get(1);
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        // Obtención de todos los datos del archivo.
        LeerDatosArchivo cargador = new LeerDatosArchivo() {};
        String dirTiposDatoCaso = dirArchivo + "/tiposDeDatosCaso.txt";
        datAr = LeerDatosArchivo.getDatos(dirTiposDatoCaso);
        int i;


        i = 0;
        while( i < datAr.size() ){
            String dato = datAr.get(i).get(0);

            try{
                if( 0 == dato.compareToIgnoreCase("nombre") ){

                    if(0 == datAr.get(i).get(2).compareToIgnoreCase("variable_nat")){
                        throw new XcargaDatos("En los valores fijos del caso " +
                                "aparece una variable aleatoria");
                    }else if(0 == datAr.get(i).get(2).compareToIgnoreCase("fijo")){
                        String nombre = datAr.get(i).get(1);
                        String valor = datAr.get(i).get(3);
                        TipoDeDatos tipoD = est.getConjTiposDatos().tipoDeNombre(nombre);
                        caso.cambiaValorTDFijo(tipoD, valor);
                    }else{
                        throw new XcargaDatos("No aparece la etiqueta" + " variable_nat o fijo que es necesaria");
                    }
                }else{
                    throw new XcargaDatos("No aparece la etiqueta nombre");
                }
            }catch(Exception ex){
                    throw new XcargaDatos(ex.toString());
            }
            i++;
        } // Fin de while.
    }

}
