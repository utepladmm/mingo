/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistenciaMingo;

import dominio.ConjTiposDeDato;
import dominio.Estudio;
import dominio.ParTipoDatoValor;
import dominio.TipoDeDatos;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 *
 * Carga los TipoDeDatos preexistentes de conjTD con la variable aleatoria asociada o el valor
 * fijo asociado.
 */
public class CargaTiposDeDatoEst {

    public static void cargarTiposDato(String dirArchivo,
        ConjTiposDeDato conjTD, Estudio est) throws XcargaDatos{
        
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        // Obtención de todos los datos del archivo.
        LeerDatosArchivo cargador = new LeerDatosArchivo() {};
        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        int i;


        i = 0;
        while( i < datAr.size() ){
            String dato = datAr.get(i).get(0);

            try{
                if( 0 == dato.compareToIgnoreCase("nombre") ){
                    String nombre = datAr.get(i).get(1);
                    TipoDeDatos tipo = conjTD.tipoDeNombre(nombre);

                    if(0 == datAr.get(i).get(2).compareToIgnoreCase("variable_nat")){
                        // el TipoDeDatos tiene una variable aleatoria asociada
                        tipo.setVNDelDato(est.getGrafoN().devuelveVN(datAr.get(i).get(3)));
                        tipo.setAleat(true);
//                        tipo.setValorDelDato(null);
                    }else if(0 == datAr.get(i).get(2).compareToIgnoreCase("fijo")){
                        // el TipoDeDatos es fijo
                        ParTipoDatoValor pTDV = new ParTipoDatoValor();

                        pTDV.setTipoDatos(tipo);
                        pTDV.setValor(datAr.get(i).get(3));

                        tipo.setAleat(false);
                        tipo.setVNDelDato(null);
                        est.getValoresTDFijosEst().add(pTDV);
                    }else{
                        throw new XcargaDatos("No aparece la etiqueta" + " variable_nat o fijo que es necesaria");
                    }
                }else{
                    throw new XcargaDatos("No aparece la etiqueta nombre");
                }
            }catch(Exception ex){
                    throw new XcargaDatos(ex.toString());
            }

            i++;
        } // Fin de while.
    }
}
