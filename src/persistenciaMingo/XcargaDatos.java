/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaMingo;

/**
 *
 * @author ut600232
 */
public class XcargaDatos extends Exception {

    String descripcion;

    public XcargaDatos() {
        descripcion = "";
    }

    public XcargaDatos(String texto) {
        descripcion = texto;
    }

    public void setDescripcion(String texto) {
        descripcion = texto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
