/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaMingo;

import dominio.ImportFijo;
import dominio.ListaReales;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class CargaDatosImpFijo {

    private ArrayList<Integer> auxI;
    private ArrayList<Double> auxD;
    private ArrayList<ArrayList<Double>> auxDD;

    public void cargar(ArrayList<ArrayList<String>> datAr, ImportFijo imp) throws XcargaDatos {
        //  A partir del texto en datAr carga los datos de la ImportEsta impEst que ya existe
        //  y tiene los datos de RecursoBase de esa importacion
        String ini = "&DATOS_ESPECIFICOS";
        // Marca de fin de los datos específicos de un recurso base.
        String fin = "&FIN";

        String dato;
        int i, j;

        ListaReales auxLR;
        boolean encontrado = false;


        // Búsqueda del nombre del recurso base a cargar.
        i = 0;
        while (!encontrado && (i + 1 < datAr.size())) {
            if (ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                if (datAr.get(i + 1).size() > 1 && 0
                        == imp.getNombre().compareToIgnoreCase(datAr.get(i + 1).get(1))) {
                    encontrado = true;
                }
            }
            i++;
        }
        if (!encontrado) {
            throw new XcargaDatos("No se encontraron los datos específicos del recurso: "
                    + imp.getNombre());
        }

        boolean noSeReconoceEtiqueta = false;

        while (i < datAr.size()
                && fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0) {
            try {
                if (0 == dato.compareToIgnoreCase("nombre")) {
                } else if (0 == dato.compareToIgnoreCase("pais")) {
                    imp.setPais((datAr.get(i).get(1)));
                } else if (0 == dato.compareToIgnoreCase("ref")) {
                    imp.setRef(datAr.get(i).get(1));
                } else if (0 == dato.compareToIgnoreCase("es_unitario_ref")) {
                    boolean uni = false;
                    if (datAr.get(i).get(1).equalsIgnoreCase("SI")) {
                        imp.setRefUnitario(true);
                    }
                } else if (0 == dato.compareToIgnoreCase("escest")) {
                    auxD = new ArrayList<Double>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }
                    imp.setEscEst(auxD);
                } else if (0 == dato.compareToIgnoreCase("dispest")) {
                    auxD = new ArrayList<Double>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }
                    imp.setDispEst(auxD);
                } else {
                    noSeReconoceEtiqueta = true;
                }
            } catch (Exception ex) {
                System.out.println(ex.toString());
                throw new XcargaDatos("Importacion: " + imp.getNombre()
                        + ". No se pudo cargar el dato: " + dato
                        + ". Se generó la excepción: " + ex.toString());
            } // Fin del catch.
            if (noSeReconoceEtiqueta) {
                throw new XcargaDatos("Recurso: " + imp.getNombre()
                        + ". No se reconoce la etiqueta: " + dato);
            }
            i++;
        } // Fin de while.
    }
}
