/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaMingo;

/**
 *
 * @author ut469262
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import TiposEnum.DependenciaDeT;
import dominio.Bloque;
import dominio.ConjRecB;
import dominio.ConjTransB;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import dominio.Expansion;
import dominio.Recurso;
import dominio.RecursoBase;
import dominio.TiempoAbsoluto;
import dominio.TransforBase;
import dominio.Transformacion;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class CargaExpaForzada {

    public void cargarExpDef(String dirArchivo, Estudio est,
            ConjRecB conjRB, ConjTransB conjTB, Expansion expansionPorDefecto) throws XcargaDatos {
        /**
         * Carga una expansión por defecto
         *
         */
        DatosGeneralesEstudio datosGen = est.getDatosGenerales();
        expansionPorDefecto.setCantPasos(datosGen.getCantPasos());
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        LeerDatosArchivo cargador = new LeerDatosArchivo() {
        };
        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        String ini = "&EXPANSION_POR_DEFECTO";
        String fin = "&FIN";
        String dato;
        int i, j;
        RecursoBase rb;
        ArrayList<Integer> auxI;
        ArrayList<Double> auxD;
        ArrayList<String> auxS;
        int[] arrI = null;
        boolean encontrado = false;
        boolean noSeReconoceEtiqueta = false;

        i = 0;
        while (!encontrado && (i + 1 < datAr.size())) {
            if (ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                encontrado = true;
            }
            i++;
        }
        if (!encontrado) {
            throw new XcargaDatos("No se encontró la expansión en archivo " +
                    dirArchivo);
        }
        int paso = 0;
        TiempoAbsoluto ta = null;

        while (i < datAr.size() &&
                fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0) {
            //Carga la información correspondiente a los datos del recurso base.
            try {
                if (0 == dato.compareToIgnoreCase("existentes")) {
                    paso = 1;
                    ta = datosGen.getInicioEstudio();
                } else if (0 == dato.compareToIgnoreCase("paso")) {
                    String stabs = datAr.get(i).get(1);
                    // stabs es un string que representa un tiempo absoluto en texto
                    ta = new TiempoAbsoluto(stabs);
                    paso = datosGen.pasoDeTiempoAbsoluto(ta);
                } else if (0 == dato.compareToIgnoreCase("recurso")) {
                    String nombre = datAr.get(i).get(1);
                    String estado = datAr.get(i).get(2);
                    int mult = Integer.parseInt(datAr.get(i).get(3));
                    if ((rb = conjRB.getUnRecB(nombre)) == null) {
                        throw new XcargaDatos(
                                "Error en expansión por defecto, RecursoBase: " +
                                nombre + " no existe en el conjunto de recursosBase");
                    } else {
                        Recurso rec = new Recurso();
                        rec.setRecBase(rb);
                        rec.setEstado(estado);

                        if (rb.getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.NO)) {
                            rec.setIndicadorTiempo(Integer.parseInt(datAr.get(i).get(3)));
                        }
                        Bloque blo = new Bloque(rec, mult, ta);
                        expansionPorDefecto.cargaBloqueEnUnTA(blo, ta);
                    }
                } else if (0 == dato.compareToIgnoreCase("transformacion")) {
                    String nombre = datAr.get(i).get(1);
                    String estado = datAr.get(i).get(2);
                    int mult = Integer.parseInt(datAr.get(i).get(3));
                    TransforBase trb = new TransforBase(est);
                    if ((trb = conjTB.getUnaTransB(nombre)) == null) {
                        throw new XcargaDatos(
                                "Error en expansión por defecto, TransforBase: " + nombre);
                    } else {
                        Transformacion tra = new Transformacion();
                        tra.setTransBase(trb);
                        tra.setEstadoString(estado);
                        Bloque blo = new Bloque(tra, mult, ta);
                        expansionPorDefecto.cargaBloqueEnUnTA(blo, ta);
                    }
                } else {
                    noSeReconoceEtiqueta = true;
                }
            } catch (Exception ex) {
                System.out.println(ex.toString());
                throw new XcargaDatos("Error en la lectura de expansión");
            } // Fin del catch.
            if (noSeReconoceEtiqueta) {
                throw new XcargaDatos("No se reconoce la etiqueta en archivo de datos generales: " + dato);
            }
            i++;
        } // Fin de while.

    }
//	public static void main(String[] args){
//        ConjRecB c1RB = new ConjRecB();
//		ConjTransB c1TB = new ConjTransB();
//		DatosGeneralesEstudio dGE = new DatosGeneralesEstudio();
//		String texto = "";
//
//
//        CargaRecBase cargadorRB = new CargaRecBase() {};
//		CargaTransBase cargadorTB =new CargaTransBase() {};
//		CargaDatosGenerales cargadorDGE = new CargaDatosGenerales() {};
//		CargaExpaForzada cargadorCED = new CargaExpaForzada (){};
////		CargaDatosTermico carTer = new CargaDatosTermico() {};
//        try{
//
//			String dirArchivo = "D:/Java/PruebaJava3/V5-DatosGenerales.txt";
//			cargadorDGE.cargarDatosGen(dirArchivo, dGE);
//
//			dirArchivo = "D:/Java/PruebaJava3/V12-RecursosBase.txt";
//            cargadorRB.cargarConjRecBase(dirArchivo, c1RB, dGE);
//
//			dirArchivo = "D:/Java/PruebaJava3/V5-TransformacionesBase.txt";
//			cargadorTB.cargarConjTransBase(dirArchivo, c1TB, c1RB);
//
//
//
//			dirArchivo = "D:/Java/PruebaJava3/V7-ExpansionForzadaEstudio.txt";
//			Expansion ePD = new Expansion("Expansion por defecto", dGE.getCantPasos());
//			cargadorCED.cargarExpDef(dirArchivo, dGE, c1RB, c1TB, ePD);
//			texto = ePD.toString(dGE);
//			System.out.println(texto + "\n" );
//
//        }catch(XcargaDatos ex){
//            System.out.println("------ERROR-------");
//            System.out.println(ex.getDescripcion());
//            System.out.println("------------------\n");
//		}
//    }
}



