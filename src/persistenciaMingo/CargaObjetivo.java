/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaMingo;

import dominio.ObjNumCron;
import GrafoEstados.Objetivo;
import UtilFunciones.FPotencialTope;
import dominio.Caso;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import dominio.TiempoAbsoluto;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ut469262
 */
public class CargaObjetivo {

    /**
     * 
     * @param dirArchivo
     * @param caso 
     * @param est
     * @param objEst es true si se está leyendo el objetivo del estudio, de lo contrario
     * se está leyendo el objetivo de un caso.
     * @throws XcargaDatos
     */
    public static void cargarObj(String dirArchivo, Caso caso, Estudio est, boolean objEst) throws XcargaDatos {
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        String nombre;
        if (objEst == true) {
            nombre = "Estudio";
        } else {
            nombre = "Caso " + caso.toStringCorto();
        }
        Objetivo objetivo = null;
        // Datos de todo el archivo.
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        LeerDatosArchivo cargador = new LeerDatosArchivo() {
        };
        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        // Marca de inicio de los datos de un Objetivo.
        String ini = "&OBJETIVO";
        // Marca de fin de los datos de un recurso base.
        String fin = "&FIN";
        int i, j;
        String dato;
        boolean encontrado = false;
        i = 0;
        // llega a la linea inicial
        do {
            dato = datAr.get(i).get(0);
            if (dato.equalsIgnoreCase(ini)) {
                encontrado = true;
            }
            i++;
        } while (!encontrado & i < datAr.size());

        if (!encontrado) {
            // Tratamiento si no encuentra el objetivo
            if(objEst){
                // Se está leyendo el Objetivo del Estudio
                int seguir = JOptionPane.showConfirmDialog(null, "El Estudio no tiene objetivo. ¿Quiere continuar?");
                if (seguir != JOptionPane.OK_OPTION) {
                    throw new XcargaDatos("Se detuvo la corrida porque el Estudio no tiene objetivo");
                }
            }else{
                // Se está leyendo el Objetivo de un Caso
                if(est.getObjEst()==null){
                    throw new XcargaDatos("El Caso no tiene objetivo y el Estudio tampoco");
                }
                int seguir = JOptionPane.showConfirmDialog(null, "El Caso no tiene objetivo. ¿Quiere usar el del Estudio?");                
                if (seguir != JOptionPane.OK_OPTION) {
                    throw new XcargaDatos("Se detuvo la corrida porque el Caso no tiene objetivo");
                }                
            }
            
        }
        // i quedó en la línea siguiente a la etiqueta inicial
        try {
            if (0 == dato.compareToIgnoreCase("TIPO_OBJ")
                    || datAr.get(i).get(1).equalsIgnoreCase("OBJ_NUM_CRON")) {
                /**
                 * Objetivo de tipo OBJ_NUM_CRON que promedia costos
                 * de numerarios para hallar un costo por crónica y calcula el valor esperado
                 * en las crónicas de una función de utilidad.
                 */
                i++;
                objetivo = new ObjNumCron();
                FPotencialTope funcPT = new FPotencialTope();
                ((ObjNumCron) objetivo).setFuncModif(funcPT);
                ((ObjNumCron) objetivo).setDatGen(datGen);
                if (datAr.get(i).get(0).equalsIgnoreCase("TIPO_FUNC_MODIF")
                        && datAr.get(i).get(1).equalsIgnoreCase("potencial_tope")) {
                    i++;
                } else {
                    throw new XcargaDatos("error en la lectura del objetivo del "
                            + nombre + " tipo función");
                }
                if (0 == datAr.get(i).get(0).compareToIgnoreCase("pond_numerarios")) {
                    ArrayList<Double> auxD = new ArrayList<Double>();
                    for (j = 1; j < datGen.getNumerarios().size() + 1; j++) {
                        auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                    }
                    ((ObjNumCron) objetivo).setPonderadores(auxD);
                } else {
                    throw new XcargaDatos("error en la lectura del objetivo del "
                            + nombre + " en los ponderadores");
                }
                i++;
                if (0 == datAr.get(i).get(0).compareToIgnoreCase("parametros_forma")) {
                    if (0 == datAr.get(i).get(1).compareToIgnoreCase("F2=")
                            || 0 == datAr.get(i).get(3).compareToIgnoreCase("D2=")) {
                        funcPT.setD2(Double.parseDouble(datAr.get(i).get(2)));
                        funcPT.setD2(Double.parseDouble(datAr.get(i).get(4)));
                        funcPT.calcAyK();
                        ((ObjNumCron) objetivo).setFuncModif(funcPT);
                        i++;
                    } else {
                        throw new XcargaDatos("error en la lectura del objetivo del "
                                + nombre + " en los parámetros de forma");
                    }
                }
                if (0 == datAr.get(i).get(0).compareToIgnoreCase("TIEMPO_ABSOLUTO_BASE_PARAMS")) {
                    objetivo.settAbsolutoBaseObjetivo(new TiempoAbsoluto(datAr.get(i).get(1)));
                } else {
                    throw new XcargaDatos("error en la lectura de tiempo absoluto base "
                            + "del objetivo en el " + nombre);
                }
                i++;
                if (0 == datAr.get(i).get(0).compareToIgnoreCase("parametros_dep_tiempo")) {
                    if (0 == datAr.get(i).get(1).compareToIgnoreCase("x0lim")) {
                        ArrayList<ArrayList<Double>> auxDD = new ArrayList<ArrayList<Double>>();
                        ArrayList<Double> auxD = new ArrayList<Double>();
                        auxDD.add(auxD);
                        for (j = 2; j < datAr.get(i).size(); j++) {
                            auxD.add(Double.parseDouble(datAr.get(i).get(j)));
                        }
                        ((ObjNumCron) objetivo).setParamDepTiempo(auxDD);
                        i++;
                    } else {
                        throw new XcargaDatos("error en la lectura del objetivo del "
                                + nombre + " en los parámetros de escala");
                    }
                }

            }
            // ACA HABRIA QUE AGREGAR EVENTUALES OTROS TIPOS DE OBJETIVOS

            if (objEst) {
                est.setObjEst(objetivo);
            } else {
                caso.setObj(objetivo);
            }


        } catch (Exception ex) {
            throw new XcargaDatos("No se pudo cargar el objetivo, excepción: " + ex.toString());
        } // Fin del catch.

    }
}
