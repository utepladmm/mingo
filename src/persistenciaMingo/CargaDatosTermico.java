/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistenciaMingo;

import dominio.Combustible;
import dominio.Estudio;
import dominio.GeneradorTermico;
import dominio.ListaReales;
import dominio.RecursoBase;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public abstract class CargaDatosTermico {

     public void cargar(ArrayList<ArrayList<String>> datAr, GeneradorTermico genTer,
             Estudio est) throws XcargaDatos {
          //  A partir del texto en datAr carga los datos del GeneradorTermico genTer que ya existe
          //  y tiene los datos de RecursoBase de ese generador térmico

          // Marca de inicio de los datos específicos de un recurso base.
          String ini = "&DATOS_ESPECIFICOS";
          // Marca de fin de los datos específicos de un recurso base.
          String fin = "&FIN";

          String dato;
          int i, j;
          ArrayList<Integer> auxI;
          ArrayList<Double> auxD;
          ListaReales auxLR;
          boolean encontrado = false;
          boolean noSeReconoceEtiqueta = false;

          // Búsqueda del nombre del recurso base a cargar.
          i = 0;
          while (!encontrado && (i + 1 < datAr.size())) {
               if (ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                    if (datAr.get(i + 1).size() > 1 && 0 ==
                            genTer.getNombre().compareToIgnoreCase(datAr.get(i + 1).get(1))) {
                         encontrado = true;
                    }
               }
               i++;
          }
          if (!encontrado) {
               throw new XcargaDatos("No se encontraron los datos específicos del recurso: " +
                       genTer.getNombre());
          }

          while (i < datAr.size() &&
                  fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0) {
               //Carga la información correspondiente a los datos del recurso base.
               try {
                    if (0 == dato.compareToIgnoreCase("nombre")) {
                    } else if (0 == dato.compareToIgnoreCase("mintec")) {
                         genTer.setMintec(datAr.get(i).get(1));
                    } else if (0 == dato.compareToIgnoreCase("potencia_Mintec")) {
                         genTer.setPotenciaMintec(Double.parseDouble(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("dpotan")) {
                         genTer.setDpotan(Double.parseDouble(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("rend_potencia_nom")) {
                         genTer.setRendPotenciaNom(Double.parseDouble(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("rend_potencia_min")) {
                         genTer.setRendPotenciaMin(Double.parseDouble(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("drenan")) {
                         genTer.setDrenan(Double.parseDouble(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("disponibilidad")) {
                         genTer.setDisponibilidad(Double.parseDouble(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("combustibles")) {
                         for (j = 1; j < datAr.get(i).size(); j++) {
                              String nom = datAr.get(i).get(j);
                              Combustible comb = (Combustible) est.getConjFuentes().fuenteDeNombre(nom);
                              genTer.getCombustibles().add(comb);
                         }
                    } else if (0 == dato.compareToIgnoreCase("dpotcs")) {
                         genTer.setDPotCS(Double.parseDouble(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("drencs")) {
                         genTer.setDRenCS(Double.parseDouble(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("coymv")) {
                         genTer.setCOYMV(Double.parseDouble(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("coymf")) {
                         genTer.setCOYMF(Double.parseDouble(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("manten")) {
                    	ArrayList<String> aux = new ArrayList<String>();
                        for (j = 1; j < datAr.get(i).size(); j++) {
                             aux.add(datAr.get(i).get(j));
                        }
                        genTer.getManten().add(aux);
                    } else {
                         noSeReconoceEtiqueta = true;
                    }
               } catch (Exception ex) {
                    System.out.println(ex.toString());
                    throw new XcargaDatos("Generador térmico: " + genTer.getNombre() +
                            ". No se pudo cargar el dato: " + dato +
                            ". Se generó la excepción: " + ex.toString());
               } // Fin del catch.
               if (noSeReconoceEtiqueta) {
                    throw new XcargaDatos("Recurso: " + genTer.getNombre() +
                            ". No se reconoce la etiqueta: " + dato);
               }
               i++;
          } // Fin de while.

     }
}
