package dominio;

import java.io.Serializable;

/**
*
* @author ut469262
*/
public class GeneradorFotovoltaico extends Generador implements Serializable{
	
	private String ref;
   /**
    * Es el nombre del archivo, sin extensi�n ni path donde est�n, que tienen
    * los archivos de potencia y costo unitario del generador e�lico, cuyas extensiones
    * son .pot y .cos respectivamente.
    */

	public GeneradorFotovoltaico(String ref) {
		this.ref = ref;
	}

	public GeneradorFotovoltaico() {
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	@Override
	public String toString() {
				String texto ="";
		texto += super.toString() + "\n";
		texto += "Nombre referencia para archivos " + ref + "\n";

       return texto;
	}
	
}

