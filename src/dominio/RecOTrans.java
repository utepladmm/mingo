/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import GrafoEstados.Numerario;
import TiposEnum.EstadosRec;
import persistenciaMingo.XcargaDatos;

import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public abstract class RecOTrans {

    
    /**
     * Devuelve true si dos RecOTrans son del mismo RecursoBase o TransforBase
     * @param otro
     * @return 
     */
    public boolean igualRBOTB(RecOTrans otro) {

        if (this instanceof Recurso) {
            if (otro instanceof Transformacion) {
                return false;
            }
            Recurso thisR = (Recurso) this;
            Recurso otroR = (Recurso) otro;
            if(thisR.getRecBase()!=otroR.getRecBase()) return false;
            return true;
        }else{
            if (otro instanceof Recurso) {
                return false;
            }
            Transformacion thisR = (Transformacion) this;
            Transformacion otroR = (Transformacion) otro;
            if(thisR.getTransBase()!=otroR.getTransBase()) return false;
            return true;                        
        }
    }
    
    /**
     * Devuelve el RecursoBase de this si this es un Recurso y null si es una 
     * Transformacion
     */
    public RecursoBase devuelveRB(){
        if(this instanceof Recurso)
            return ((Recurso)this).getRecBase();
        return null;
    }
    
    /**
     * Devuelve la TranforBase de this si this es una Transformacion y null si es un
     * Recurso
     */
    public TransforBase devuelveTB(){
        if(this instanceof Transformacion)
            return ((Transformacion)this).getTransBase();
        return null;
    }  
    
    /**
     * Devuelve el per�odo de construcci�n
     * @return
     */
    public int devuelvePerConst(Estudio est, TiempoAbsoluto ta) throws XcargaDatos {
    	if(this instanceof Recurso){
    		return devuelveRB().getPerConstr(ta, est);
    	} else {
    		return devuelveTB().getPerConstr(ta, est);
    	}
    }
    
    /**
     * Devuelve true si this es un Recurso del RecursoBase rb 
     * y false en otro caso
     * @param rb
     * @return 
     */
    public boolean esDeRB(RecursoBase rb){
        if(this instanceof Transformacion) return false;
        Recurso esteR = (Recurso) this;
        if(esteR.getRecBase()!=rb) return false;
        return true;        
    }

    /**
     * Devuelve true si this es de la TransforBase tb y false en otro caso
     * @param rb
     * @return 
     */
    public boolean esDeTB(TransforBase tb){
        if(this instanceof Recurso) return false;
        Transformacion estaT = (Transformacion) this;
        if(estaT.getTransBase()!=tb) return false;
        return true;        
    }    
    
    

    /**
     **************************************************************************
     * Inicia métodos a ser sobreescritos por Recurso y Transformacion
     **************************************************************************
     */    
    
    

    public String toStringCorto() {
        String texto = "";
        return texto;
    }

    public String toStringMuyCorto(boolean usaEquiv) {
        return "";
    }
    
    
    public String getEstado(){
        return "";
    }

    public double getValorInvAl() {
        return 1.0;
    }

    public int getValorDemAl() {
        return 0;
    }

    public String getClase() {
        return "";
    }
    
    public int getIndicadorTiempo(){
        return -1;
    }

    /**
     *************************************************************************
     * Finalizan métodos a ser sobreescritos por Recurso y Transformacion
     *************************************************************************
     */
    
    
    
    /**
     * Devuelve el valor para cada numerario de la anualidad de inversión del recurso
     * o transformacion de este RecOTrans teniendo en cuenta las eventuales
     * demoras aleatorios y valor aleatorio de la inversión.
     * Se consideran todos los módulos del RecursoBase asociado si es un Recurso.
     * Se supone que las inversiones por paso de tiempo están cargadas en el instante medio
     * de cada paso en el período de construcción.
     * Las anualidades se calculan para el instante medio de los pasos de la vida útil.
     * 
     * @param est es el Estudio
     * @param ta es el TiempoAbsoluto en el que vive el Recurso o Transformacion
     *
     */
    public ArrayList<Double> anualidInvRoT(Estudio est, TiempoAbsoluto ta) throws XcargaDatos {
        ArrayList<Double> anInv = new ArrayList<Double>();
        int vidaUtil = 0;
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        double tasaPaso = datGen.getTasaDescPaso();
        double factorDescPaso = 1 / (1 + tasaPaso);
        ArrayList<ArrayList<Double>> inversiones = new ArrayList<ArrayList<Double>>();
        Recurso rec = null;
        Transformacion trans = null;
        if (this instanceof Recurso) {
            if (! this.getEstado().equalsIgnoreCase(EstadosRec.OPE) ) {
                throw new XcargaDatos("Se pidió la anualidad" +
                        "de un recurso cuyo estado no es OPE");
            }
            rec = (Recurso)this;
            RecursoBase rB = rec.getRecBase();
            inversiones = rB.getInversiones();
            vidaUtil = rB.getVidaUtil();
        } else {
            if (!  (this.getEstado().equalsIgnoreCase(EstadosRec.FALL) ||
                    this.getEstado().equalsIgnoreCase("TERMIN"))        ) {
                throw new XcargaDatos("Se pidió la anualidad" +
                        "de una transformacion cuyo estado no es FALL ni TERMIN");
            }
            trans = (Transformacion)this;
            TransforBase tB = trans.getTransBase();
            inversiones = tB.getInversiones();
            /**
             * Se conviene en que las inversiones en transformaciones fallidas se amortizan en
             * 20 años, por lo que no se altera el valor 20 cargado al crear la variable.
             */
        }
        int inum = 0;
        /**
         * Se va actualizando al instante medio del paso de entrada en servicio los
         * valores de inversión de cada numerario
         */
        for (Numerario num : datGen.getNumerarios()) {
            double inv1Num = 0.0;
            if (!inversiones.get(inum).isEmpty()) {
                int perConst = inversiones.get(inum).size();
                for (int paso = 0; paso < perConst; paso++) {
                    inv1Num += inversiones.get(inum).get(paso) / Math.pow(factorDescPaso, perConst - paso);
                }
            }
            inum++;
            double factorAnualiz = (1 - Math.pow(factorDescPaso, vidaUtil)) / (1 - factorDescPaso);
            double an1Num = inv1Num / factorAnualiz;

            /**
             * Si el RecOTrans es de un Recurso se multiplica por la cantidad de
             * promedio módulos que tiene el RecursoBase en ta
             */
            if (this instanceof Recurso) {
                an1Num = an1Num * rec.cantMod(datGen, ta);
            }

            /**
             * Se cambia la anualidad por efecto de demora aleatoria de inversión y monto aleatorio de inversión
             */
            an1Num = an1Num * multMontoInv(est);
            anInv.add(an1Num);
        }
        return anInv;
    }    
    
   
    
    /**
     * Se puede aplicar a Recursos de estado OPE o a
     * Transformaciones de estado FALL, de lo contrario da error, y devuelve un número
     * que resume el efecto de monto de inversión aleatoria y demora aleatoria de construcción
     *
     * @return multM es el real por el que hay que multiplicar la anualidad para aplicarle
     * el efecto de monto de inversión aleatoria y demora aleatoria de construcción.
     */
    public double multMontoInv(Estudio est) {
        double factorDesc = 1 / (1 + est.getDatosGenerales().getTasaDescPaso());
        double multM = 1;
        int demoraAleat = this.getValorDemAl();
        double valorInvAleat = this.getValorInvAl();
        multM = multM / (Math.pow(factorDesc, demoraAleat));
        multM = multM * valorInvAleat;
        return multM;
    }    
    
    
    
    /**
     * Devuelve el valor para cada numerario de inversi�n actualizada del recurso
     * o transformacion de este RecOTrans, CUANDO LA INVERSI�N SE CARGA TODA EN EL PASO DE DECISI�N
     * OJO DE ENTRADA NO SE EST� teniendo en cuenta las eventuales
     * demoras aleatorios y valor aleatorio de la inversión.
     * 
     * Se consideran todos los módulos del RecursoBase asociado si es un Recurso.
     * Se supone que las inversiones por paso de tiempo están cargadas en el instante medio
     * de cada paso en el período de construcción.
     * Las anualidades se calculan para el instante medio de los pasos de la vida útil.
     * 
     * @param est es el Estudio
     * @param ta es el TiempoAbsoluto en el que vive el Recurso o Transformacion (EN EL QUE SE TOMA LA DECISI�N)
     *
     */
    public ArrayList<Double> invHRoTPasoDecision(Estudio est, TiempoAbsoluto ta) throws XcargaDatos {
    	int T = est.getDatosGenerales().getFinEstudio().getAnio(); 
    	int t = ta.getAnio();
    	int tabase = 0;  // anio del tiempo absoluto base del recursobase o anio inicial de la corrida para transformaci�n base
        ArrayList<Double> invH = new ArrayList<Double>();
        ArrayList<Double> escalamientoInv;
        int vidaUtil = 0;
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        double tasaPaso = datGen.getTasaDescPaso();
        double factorDescPaso = 1 / (1 + tasaPaso);
        ArrayList<ArrayList<Double>> inversiones = new ArrayList<ArrayList<Double>>();
        Recurso rec = null;
        Transformacion trans = null;
        if (this instanceof Recurso) {
            if (! (this.getEstado().equalsIgnoreCase(EstadosRec.CON) && this.getIndicadorTiempo()== -devuelvePerConst(est,ta)) ){
                throw new XcargaDatos("Se pidió la anualidad" +
                        "de un recurso cuyo estado no es OPE tiempo 0");
            }
            rec = (Recurso)this;
            RecursoBase rB = rec.getRecBase();
            tabase = rB.getTiempoAbsolutoBase().getAnio();
            inversiones = rB.getInversiones();
            vidaUtil = rB.getVidaUtil();
            escalamientoInv = rB.getEscalamientoInv();
        } else {
            if (! (this.getEstado().equalsIgnoreCase(EstadosRec.CON)&& this.getIndicadorTiempo()==- devuelvePerConst(est,ta))){
                throw new XcargaDatos("Se pidió la anualidad" +
                        "de una transformacion cuyo estado no es OPE tiempo 0");
            }
            trans = (Transformacion)this;
            TransforBase tB = trans.getTransBase();
            tabase = est.getDatosGenerales().getInicioEstudio().getAnio();
            inversiones = tB.getInversiones();
            escalamientoInv = tB.getEscalamientoInv();
        }
        int inum = 0;
        /**
         * Se va actualizando al instante medio del paso de entrada en servicio los
         * valores de inversión de cada numerario
         */
        int perConst = inversiones.get(inum).size();
        
        double auxActHorizonte = (1-Math.pow(factorDescPaso, T-t-perConst+1))/(1-factorDescPaso);
        for (Numerario num : datGen.getNumerarios()) {
        	if((t + perConst<=T)){
	            double inv1Num = 0.0;
	            if (!inversiones.get(inum).isEmpty()) {               
	                for (int paso = 0; paso < perConst; paso++) {
	                    inv1Num += inversiones.get(inum).get(paso) / Math.pow(factorDescPaso, perConst - paso);
	                }
	            }
	            inum++;
	            double factorAnualiz = (1 - Math.pow(factorDescPaso, vidaUtil)) / (1 - factorDescPaso);     
	            double an1Num = inv1Num / factorAnualiz;
	
	            /**
	             * Si el RecOTrans es de un Recurso se multiplica por la cantidad de
	             * promedio módulos que tiene el RecursoBase en ta
	             */
	            if (this instanceof Recurso) {
	                an1Num = an1Num * rec.cantMod(datGen, ta);
	            }
	            // ojojojojojojojojojojojojojo
	            // TODO QUE PASA CON LA CANTIDAD DE M�DULOS DE LAS TRANSFORMACIONES????????????????????????????????
	
	//            /**
	//             * Se cambia la anualidad por efecto de demora aleatoria de inversión y monto aleatorio de inversión
	//             */
	//            an1Num = an1Num * multMontoInv(est);
	             //double invH1Num = an1Num*Math.pow(factorDescPaso,perConst+0.5)*auxActHorizonte;    
	             // Aplica el factor de escalamiento de la inversi�n
	             int desplazamiento = t - tabase; // desplazamiento en a�os para calcular el escalamiento de inversiones
	             desplazamiento = Math.min(desplazamiento, escalamientoInv.size()-1);
	             //invH.add(invH1Num*escalamientoInv.get(desplazamiento));
	             // Calcular el valor residual VR:
	             int desplazamientoFin =  Math.min(T+1 - tabase, escalamientoInv.size()-1);                 
	             double an1NumFin = an1Num*(escalamientoInv.get(desplazamientoFin)); 
	             
	             double invIniConst1Num = inv1Num * Math.pow(factorDescPaso, perConst+0.5) *escalamientoInv.get(desplazamiento) ; // idem inversi�n total a costo del a�o t 
	             double valResidual1Num; // valor residual a costo del a�o T+1 actualizado al inicio del paso t (primer paso del per�odo de construcci�n)
	             valResidual1Num= Math.pow(factorDescPaso, T+1.5-t)* an1NumFin * (1- Math.pow(factorDescPaso, t+ perConst + vidaUtil-T-1)) / (1- factorDescPaso);
	             invH.add(invIniConst1Num - valResidual1Num);
        	}else{
        		invH.add(0.0);
        	}
        }
        return invH;
    }    
    
    @Override
    public boolean equals(Object obj) {
        if (this instanceof Recurso) {
            if (obj instanceof Transformacion) {
                return false;
            }
            Recurso este = (Recurso) this;
            Recurso otro = (Recurso) obj;
            return este.equals(otro);
        } else {
            if (obj instanceof Recurso) {
                return false;
            }
            Transformacion este = (Transformacion) this;
            Transformacion otro = (Transformacion) obj;
            return este.equals(otro);
        }
    }

    @Override
    public int hashCode() {
        if (this instanceof Recurso) {
            return ((Recurso) this).hashCode();
        } else {
            return ((Transformacion) this).hashCode();
        }
    }
}
