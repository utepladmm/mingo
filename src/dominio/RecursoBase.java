/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import TiposEnum.DependenciaDeT;
import TiposEnum.ExistEleg;
import TiposEnum.SubTipo1;
import TiposEnum.SubTipo2;
import UtilitariosGenerales.DatoT;
import java.util.ArrayList;
import java.io.Serializable;
import naturaleza3.VariableNat;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut600232
 */
public class RecursoBase implements Serializable, Comparable {

//     private Estudio est;
    private String nombre;
     /**
     * En la simulaciÃ³n el RecursoBase this se sustituye por el de nombre nombreEquivEnSimul
     * que se comporta igual. Permite que RecursosBase tengan diferencia en sus costos
     * de inversiÃ³n o costos fijos sin necesidad de multiplicar las simulaciones
     *
     * Si es el String vacÃ­o el recurso no tiene equivalente en simulaciÃ³n
     */
    private String nombreEquivEnSimul;
   
    private RecursoBase equivEnSimul;
    private String clase;
    /**
     * El RecursoBase es comun si es EXIST, no es recurso origen de Transformaciones
     * ni estÃ¡ sujeto a CambioAleatorios.
     */
    private boolean comun;
    /**
     * esOrigenTB es true si el RecursoBase es origen de alguna TransformacionBase.
     * 
     */
    private boolean esOrigenTB;
    /**
     * Potencia nominal de un m�dulo en el tiempoAbsolutoBase, o si las potencias nominales
     * se entran con un DatoT, potencia nominal en la primera fecha del DatoT.
     */
    private Double potNominalTABase;
    private Punto puntoConex;
    private String tipo;
    private String subtipo1;
    private String subtipo2;
    private boolean tieneEdad;
     /**
     * Toma valores en: {no, edad, paso_de_tiempo}
     * Si depende de la edad tiene que tener edad.
     * Los datos de potNominalT, cantModulosT y costosFijos estÃ¡n sujetos a
     * un tratamiendo especial si dependenciaDeT es edad o paso_de_tiempo.
     */
    private String dependenciaDeT;
    
   /**
     * Tiempo absoluto base a partir del cual se mide la dependencia segÃºn
     * PASO_DE_TIEMPO cuando no se introducen datos por fecha.
     * Un RecursoBase no puede existir antes de su tiempoAbsolutoBase.
     * tiempoAbsolutoBase se debe establecer con el mismo cÃ³digo que se determinÃ³ en
     * DatosGeneralesEstudio
     * Ejemplo: 2010 o bien 2014.1 (esto Ãºltimo si hay mÃ¡s de un paso por aÃ±o)
     */
    private TiempoAbsoluto tiempoAbsolutoBase;
    
    /**
     * Ultimo tiempo absoluto de existencia del recursoBase, cualquiera sea su
     * clase o dependencia del tiempo, fijo, edad o dependencia del paso de tiempo
     * El recurso base vive inclusive en ese Ãºltimo paso.
     */
    private TiempoAbsoluto ultimoTiempoAbsoluto;
    
    private ArrayList<Double> escalamientoInv;  // variaci�n en por uno del precio de inversiones overhead 
    										    // tomando com o base 1 en el tiempo absoluto base
    
    private ArrayList<Double> escalamientocFijo;  // variaci�n en por uno del precio de costos fijo 
    											  // tomando com o base 1 en el tiempo absoluto base
    
    
    /**
     *  Si es true, los datos espec�ficos del RecursoBase no se toman de los archivos de entrada del Mingo
     *  sino de una fuente externa: en el ImplementadorMop se toman de un datatype DatosCorridaMingo
     */
    private boolean lecturaDatosExt; 
    
    
    /**
     *  Potencia nominal por paso de tiempo o edad en pasos de tiempo
     *  en caso de que dependa de la edad o el paso de tiempo.
     *  El get(0):
     *  - tiene la potencia en el primer paso de tiempo de la vida
     *    si el recurso depende de la edad
     *    edad es el ordinal del paso de tiempo de vida empezando en 0,
     *    como la edad de las personas. Es decir la edad empieza en 0,
     *    0, 1, 2, 3,..
     *  - tiene la potencia en el tiempoAbsolutoBase
     *    si el recurso depende del paso de tiempo absoluto
     *  ATENCION !!!!!!!!!
     *  En los get(i) consecutivos se colocan las potencias en los pasos
     *  de tiempo sucesivos. Si los valores existentes:
     *  - no llegan al Ãºltimo paso de vida Ãºtil en los recursos que dependen de la edad
     *  - no llegan al ultimoTiempoAbsoluto en los recursos que dependen del paso de tiempo,
     *  entonces el Ãºltimo valor existente se extiende los pasos de tiempo que sea necesario.
     *
     */
    private ArrayList<Double> potNominalT;
    /**
     *  Igual convenciÃ³n que para potNominalT
     */
    private ArrayList<Integer> cantModulosT;
    
    /**
     * Si el RecursoBase depende del tiempo, los datos detallados de potencia nominal 
     * y cantidad de mÃ³dulos se pueden leer y almacenar opcionalmente en objetos DatoT.
     * Los dos DatoT potNominalDT y cantModulosDT deben tener un primer valor no nulo en la misma fecha Calendar
     * En ese caso se carga tambiÃ©n a partir del DatoT el tiempoAbsolutoBase, que es el TiempoAbsoluto
     * de la primera fecha de los DatoT.
     * 
     * En los datos de entrada se reconoce que se leerÃ¡ un DatoT porque existen caracteres "/" y ":"
     */
    
    private DatoT potNominalDT;
    private DatoT cantModulosDT;    
    
    /**
     * La vida Ãºtil se mide en aÃ±os. Solo puede tener vidaUtil
     * un recurso con edad
     */
    private int vidaUtil;
    
    private int perConstr;
    private boolean tieneDemoraAl;
    /**
     * VN que determina la demora de inversiÃ³n si admite aleatoriedad.
     * Sus valores son strings con el multiplicador de la inversiÃ³n
     * El nombre de la variable es el nombre del RecursoBase, sufijo _DEMAL
     */
    private VariableNat vNDemoraAl;   
    private ArrayList<String> valoresDemAlS;
      /**
     * Al calcular el sucesor sin crecimiento de un parque que vive en
     * el paso t en la etapa e, pasando al paso t+k en la etapa e+1,
     * para los recursos con demora aleatoria cuyo plazo de construcciÃ³n
     * normal finalizÃ³ entre t+1 y t+k inclusive se observa el valor de la
     * variable aleatoria asociada el recursoBase en el paso t+k
     *
     */
    private ArrayList<Integer> valoresDemAl;
    /**
     * ofertasDeproductos contiene para cada uno de los productos, la oferta
     * del producto que hace cada mÃ³dulo, cuando la potencia nominal es:
     * - la potencia fija por mÃ³dulo si el recurso no depende del tiempo ni la edad
     * - la del tiempoAbsolutoBase si depende del tiempo
     * - la del primer aÃ±o de vida si depende de la edad
     *
     * Al cambiar la potencia nominal y la cantidad de mÃ³dulos
     * la oferta de los productos varia proporcionalmente
     *
     * Cada OfertaProductoSimple consta de un Producto
     * y una cantidad ofertada Ãºnica
     *
     * No es relevante el orden en la lista
     *
     */
    private ArrayList<CantProductoSimple> cantsDeProductos1Mod;
    /**
     * InversiÃ³n en MUS$ por cada mÃ³dulo de cada numerario y en cada paso de tiempo.
     * Primer Ã­ndice: es el Ã­ndice de numerarios, en el orden en que estÃ¡n en DatosGeneralesEstudio.
     *
     * Segundo Ã­ndice: En el get(0) estÃ¡ la inversiÃ³n del primer paso de tiempo de construcciÃ³n que se supone
     * cargada en el instante medio del paso. En el get(size-1) estÃ¡ la inversiÃ³n en el aÃ±o
     * anterior al de entrada en servicio normal.
     */
    private ArrayList<ArrayList<Double>> inversiones;
     /**
     * Costo fijo en MUS$ por cada mÃ³dulo de cada numerario y en cada paso de tiempo.
     * Primer Ã­ndice: es el Ã­ndice de numerarios, en el orden en que estÃ¡n en DatosGeneralesEstudio.
     *
     * Segundo Ã­ndice: En el get(0) estÃ¡ el costoFijo del primer paso de tiempo de operaciÃ³n.
     *
     *  Igual convenciÃ³n que para potNominalT
     */
    private ArrayList<ArrayList<Double>> costosFijos;
    private boolean tieneInvAleat;
    /**
     * VN que determina la inversiÃ³n si admite aleatoriedad.
     * Sus valores son strings con el multiplicador de la inversiÃ³n
     * El nombre de la variable es el nombre del RecursoBase, sufijo _INVAL
     */
    private VariableNat vNInvAl;   
    private ArrayList<String> valoresInvAlS;
    /**
     * Al calcular el sucesor sin crecimiento de un parque que vive en
     * el paso t en la etapa e, pasando al paso t+k en la etapa e+1,
     * para los recursos con inversiÃ³n aleatoria cuyo plazo de construcciÃ³n normal
     * finalizÃ³ entre t+1 y t+k inclusive se observa el valor de la
     * variable aleatoria asociada el recursoBase en el paso t+k
     *
     */
    private ArrayList<Double> valoresInvAl;    
    private boolean tieneCFijAleat;
    /**
     * VN que determina los costos fijos si son aleatorios.
     * Sus valores son strings con el multiplicador del costo fijo
     * El nombre de la variable es el nombre del RecursoBase, sufijo _CFIAL
     */
    private VariableNat vNCFijAl;  
    private ArrayList<String> valoresCFijAlS;
    /**
     * Al calcular el costo de paso de un Parque se multiplica por el
     * valor asociado de valoresCFijAl, segÃºn el valor de la variable aleatoria asociada.
     */
    private ArrayList<Double> valoresCFijAl; 
    private boolean admiteCambioAleatorio;
    /**
     * Es la posiciÃ³n en el cÃ³digo de Recursos no comunes y Transformaciones de Parque, 
     * cBloques, del primer recurso asociado a este RecursoBase.
     */
    private int posicionRecursoInicial;
    /**
     * Es la posiciÃ³n en el cÃ³digo de Recursos no comunes y Transformaciones de Parque, 
     * cBloques, del primer recurso OPE nuevo (Ãºnico o primer caso de demora aleatoria e inversiÃ³n aleatoria)
     * asociado a este RecursoBase.
     * SE EMPLEA PARA INDICAR EL Recurso QUE SERÃ� EMPLEADO PARA ENCONTRAR PARQUES CONTIGUOS.
     */
    private int posicionRecursoOPE;
    /**
     * Es un texto que describe el origen de los datos de variables aleatorias de operaciÃ³n
     * No se trata de las variables del MINGO sino de las del modelo de operaciÃ³n.
     * En el caso de la implementaciÃ³n EDF indica SORTECRON = 0(correlaciÃ³n con hidro crÃ³nica a crÃ³nica) o
     * SORTECRON = 1 (sorteos independientes)
     */
    private String aleatOper; 
    

    public RecursoBase() {
    }

    public RecursoBase(Estudio est) {
//     public RecursoBase(){
//          this.est = est;
        nombre = "";
//		cantModulos = 1;
        nombreEquivEnSimul = "";
        equivEnSimul = null;
        potNominalTABase = 0D;
        cantModulosT = new ArrayList<Integer>();
        potNominalT = new ArrayList<Double>();
        clase = ExistEleg.EXIST;
        puntoConex = new Punto();
        tipo = "GEN";
        subtipo1 = null;
        subtipo2 = null;
        tieneEdad = false;
        dependenciaDeT = DependenciaDeT.NO;
//		dependenciaSegun = "";
        tiempoAbsolutoBase = new TiempoAbsoluto();
        ultimoTiempoAbsoluto = new TiempoAbsoluto();
        vidaUtil = 0;
        perConstr = 0;
        tieneDemoraAl = false;
        valoresDemAlS = new ArrayList<String>();
        valoresDemAl = new ArrayList<Integer>();
        cantsDeProductos1Mod = new ArrayList<CantProductoSimple>();
        inversiones = new ArrayList<ArrayList<Double>>();
        costosFijos = new ArrayList<ArrayList<Double>>();
        for (int inum = 0; inum < est.getDatosGenerales().getNumerarios().size(); inum++) {
            ArrayList<Double> auxDI = new ArrayList<Double>();
            inversiones.add(auxDI);
            ArrayList<Double> auxDCF = new ArrayList<Double>();
            costosFijos.add(auxDCF);
        }
        valoresInvAlS = new ArrayList<String>();
        valoresInvAl = new ArrayList<Double>();
        valoresCFijAlS = new ArrayList<String>();
        valoresCFijAl = new ArrayList<Double>();
        tieneInvAleat = false;
        tieneCFijAleat = false;
    }

    public int compareTo(Object o) {
        RecursoBase orb = (RecursoBase) o;
        if (clase.equalsIgnoreCase(ExistEleg.EXIST) & !orb.getClase().equalsIgnoreCase(ExistEleg.EXIST)) {
            return -1;
        }
        if (!clase.equalsIgnoreCase(ExistEleg.EXIST) & orb.getClase().equalsIgnoreCase(ExistEleg.EXIST)) {
            return 1;
        }
        if (clase.equalsIgnoreCase(ExistEleg.ELEG) & orb.getClase().equalsIgnoreCase(ExistEleg.TRANS)) {
            return -1;
        }
        if (clase.equalsIgnoreCase(ExistEleg.TRANS) & orb.getClase().equalsIgnoreCase(ExistEleg.ELEG)) {
            return 1;
        }
        // la clase de ambos objetos es igual y se comparan los nombres
        return nombre.compareToIgnoreCase(orb.getNombre());
    }

//    public enum ExistEleg {
//
//        EXIST,
//        /**
//         * EXIST: existe ya al comienzo del paso t=1
//         */
//        ELEG,
//        /**
//         * ELEG: no existe al comienzo del paso t=1 y existirÃ¡ sÃ³lo si
//         * aparece en una expansiÃ³n por defecto del estudio o del caso
//         * o bien porque es seleccionado en la optimizaciÃ³n, o bien existe como
//         * resultado de un cambio aleatorio de recursos preexistentes
//         */
//        TRANS;
//
//        /**
//         * TRANS: sÃ³lo puede aparecer como recurso transitorio de una transformaciÃ³n
//         * Las transformaciones pueden tener recursos destino ELEG o TRANS
//         */
//        public static int hashSustituto(ExistEleg tipo) {
//            int hashS = 0;
//            if (tipo == ExistEleg.EXIST) {
//                hashS = (ExistEleg.EXIST).hashCode();
//            } else if (tipo == ExistEleg.ELEG) {
//                hashS = (ExistEleg.ELEG).hashCode();
//            } else if (tipo == ExistEleg.TRANS) {
//                hashS = (ExistEleg.TRANS).hashCode();
//            }
//            return hashS;
//        }
//    }
//    public enum DependenciaDeT {
//
//        NO, EDAD, PASO_DE_TIEMPO;
//
//        /**
//         *  NO el recurso base no varÃ­a con la edad ni el paso de tiempo
//         *  EDAD el recurso base varÃ­a con su propia edad
//         *  PASO_DE_TIEMPO el recurso base varÃ­a segÃºn el paso de tiempo en que vive
//         */
//        public static int hashSustituto(DependenciaDeT tipo) {
//            int hashS = 0;
//            if (tipo == DependenciaDeT.NO) {
//                hashS = ("NO").hashCode();
//            } else if (tipo == DependenciaDeT.EDAD) {
//                hashS = ("EDAD").hashCode();
//            } else if (tipo == DependenciaDeT.PASO_DE_TIEMPO) {
//                hashS = ("PASO_DE_TIEMPO").hashCode();
//            }
//            return hashS;
//        }
//    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreEquivEnSimul() {
        return nombreEquivEnSimul;
    }

    public RecursoBase getEquivEnSimul() {
        return equivEnSimul;
    }

    public void setEquivEnSimul(RecursoBase equivEnSimul) {
        this.equivEnSimul = equivEnSimul;
    }

    public void setNombreEquivEnSimul(String nombreEquivEnSimul) {
        this.nombreEquivEnSimul = nombreEquivEnSimul;
    }

    public boolean tieneEquivEnSimul() {
        if (nombreEquivEnSimul.equalsIgnoreCase("")) {
            return false;
        } else {
            return true;
        }
    }

    public boolean esComun() {
        return comun;
    }

    public void setEsComun(boolean comun) {
        this.comun = comun;
    }

    public boolean esOrigenTB() {
        return esOrigenTB;
    }

    public void setEsOrigenTB(boolean esOrigenTB) {
        this.esOrigenTB = esOrigenTB;
    }

    public DatoT getCantModulosDT() {
        return cantModulosDT;
    }

    public void setCantModulosDT(DatoT cantModulosDT) {
        this.cantModulosDT = cantModulosDT;
    }

    public DatoT getPotNominalDT() {
        return potNominalDT;
    }

    public void setPotNominalDT(DatoT potNominalDT) {
        this.potNominalDT = potNominalDT;
    }
        

    public Double getPotNominalTABase() {
        return potNominalTABase;
    }

    public void setPotNominalTABase(Double potNominalTABase) {
        this.potNominalTABase = potNominalTABase;
    }
    
    
    

public boolean isComun() {
		return comun;
	}

	public void setComun(boolean comun) {
		this.comun = comun;
	}

	public ArrayList<Double> getEscalamientoInv() {
		return escalamientoInv;
	}

	public void setEscalamientoInv(ArrayList<Double> escalamientoInv) {
		this.escalamientoInv = escalamientoInv;
	}

	public ArrayList<Double> getEscalamientocFijo() {
		return escalamientocFijo;
	}

	public void setEscalamientocFijo(ArrayList<Double> escalamientocFijo) {
		this.escalamientocFijo = escalamientocFijo;
	}

	public VariableNat getvNDemoraAl() {
		return vNDemoraAl;
	}

	public void setvNDemoraAl(VariableNat vNDemoraAl) {
		this.vNDemoraAl = vNDemoraAl;
	}

	public VariableNat getvNInvAl() {
		return vNInvAl;
	}

	public void setvNInvAl(VariableNat vNInvAl) {
		this.vNInvAl = vNInvAl;
	}

	public VariableNat getvNCFijAl() {
		return vNCFijAl;
	}

	public void setvNCFijAl(VariableNat vNCFijAl) {
		this.vNCFijAl = vNCFijAl;
	}

	public boolean isEsOrigenTB() {
		return esOrigenTB;
	}

	//	public int getCantModulos() {
//		return cantModulos;
//	}
//
//	public void setCantModulos(int cantModulos) {
//		this.cantModulos = cantModulos;
//	}
    public ArrayList<Integer> getCantModulosT() {
        return cantModulosT;
    }

    public void setCantModulosT(ArrayList<Integer> cantModulosT) {
        this.cantModulosT = cantModulosT;
    }

    public ArrayList<Double> getPotNominalT() {
        return potNominalT;
    }

    public void setPotNominalT(ArrayList<Double> potNominalT) {
        this.potNominalT = potNominalT;
    }

    public boolean tieneDemoraAl() {
        return tieneDemoraAl;
    }

    public boolean tieneEdad() {
        return tieneEdad;
    }

    public void setPosicionRecursoInicial(int posicionRecursoInicial) {
        this.posicionRecursoInicial = posicionRecursoInicial;
    }

    public int getPosicionRecursoInicial() {
        return posicionRecursoInicial;
    }

    public int getPosicionRecursoOPE() {
        return posicionRecursoOPE;
    }

    public void setPosicionRecursoOPE(int posicionRecursoOPE) {
        this.posicionRecursoOPE = posicionRecursoOPE;
    }

    public ArrayList<String> getValoresDemAlS() {
        return valoresDemAlS;
    }

    public void setValoresDemAlS(ArrayList<String> valoresDemAlS) {
        this.valoresDemAlS = valoresDemAlS;
    }

    public ArrayList<String> getValoresInvAlS() {
        return valoresInvAlS;
    }

    public void setValoresInvAlS(ArrayList<String> valoresInvAlS) {
        this.valoresInvAlS = valoresInvAlS;
    }

    public ArrayList<String> getValoresCFijAlS() {
        return valoresCFijAlS;
    }

    public void setValoresCFijAlS(ArrayList<String> valoresCFijAlS) {
        this.valoresCFijAlS = valoresCFijAlS;
    }

    public String getAleatOper() {
        return aleatOper;
    }

    public void setAleatOper(String aleatOper) {
        this.aleatOper = aleatOper;
    }
    
    
    

    public boolean isLecturaDatosExt() {
		return lecturaDatosExt;
	}

	public void setLecturaDatosExt(boolean lecturaDatosExt) {
		this.lecturaDatosExt = lecturaDatosExt;
	}

	/** Halla la potencia nominal por cada mÃ³dulo cuando el recursoBase
     *  es invariable o depende del paso de tiempo
     *
     *  Devuelve cero cuando el tiempo absoluto
     *  ta es anterior a tiempoAbsolutoBase
     *  o bien ta es posterior al ultimoTiempoAbsoluto
     *
     *  Extrapola los valores del Ãºltimo ta con dato hasta ultimoTiempoAbsoluto
     **/
    public double potNom1ModDeTiempoAbsoluto(DatosGeneralesEstudio datGen,
            TiempoAbsoluto ta) throws XcargaDatos {
        if(tiempoAbsolutoBase == null){
            throw new XcargaDatos("El recurso " + nombre 
                + " que depende del tiempo, no tiene tiempoAbsolutoBase");
        }
        double pot = 0.0;
        if((this).getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.EDAD)) {
            throw new XcargaDatos("El recurso " + nombre + "depende de la edad y se invoco "
                + "potNominalDeTiempoAbsoluto");
        }
        if (datGen.getCalculadorDePlazos().hallaAvanceTiempo(ta, ultimoTiempoAbsoluto) < 0) {
            return 0.0;
        } else if (datGen.getCalculadorDePlazos().hallaAvanceTiempo(tiempoAbsolutoBase, ta) < 0) {
            return 0.0;
        } else if (dependenciaDeT.equalsIgnoreCase(DependenciaDeT.NO)) {
            pot = potNominalT.get(0);
            return pot;
        } else if (dependenciaDeT.equalsIgnoreCase(DependenciaDeT.PASO_DE_TIEMPO)) {
            int avance = datGen.getCalculadorDePlazos().
                    hallaAvanceTiempo(tiempoAbsolutoBase, ta);

            if (avance < potNominalT.size()) {
                // Hay dato explÃ­cito para el tiempoAbsoluto ta
                return potNominalT.get(avance);
            } else {
                // Se toma el dato del Ãºltimo tiempoAbsoluto con dato
                return potNominalT.get(potNominalT.size() - 1);
            }
        }
        return pot;
    }

    /** Halla la cantidad de mÃ³dulos cuando el recursoBase
     *  es invariable o depende del paso de tiempo
     *
     *  Devuelve cero cuando el tiempo absoluto
     *  ta es anterior a tiempoAbsolutoBase
     *  o bien ta es posterior al ultimoTiempoAbsoluto
     *
     *  Extrapola los valores del Ãºltimo ta con dato hasta ultimoTiempoAbsoluto
     *
     **/
    public int cantModulosDeTiempoAbsoluto(DatosGeneralesEstudio datGen,
            TiempoAbsoluto ta) {
        assert (tiempoAbsolutoBase != null) : "El recurso " + nombre
                + "que depende del tiempo, no tiene tiempoAbsolutoBase";
        int cant = 0;
        assert (!(this).getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.EDAD)) :
                "El recurso " + nombre + "depende de la edad y se invoco "
                + "cantModulosDeTiempoAbsoluto";
        if (datGen.getCalculadorDePlazos().hallaAvanceTiempo(ta, ultimoTiempoAbsoluto) < 0) {
            return 0;
        } else if (datGen.getCalculadorDePlazos().hallaAvanceTiempo(tiempoAbsolutoBase, ta) < 0) {
            return 0;
        } else if (dependenciaDeT.equalsIgnoreCase(DependenciaDeT.NO)) {
            cant = cantModulosT.get(0);
            return cant;
        } else if (dependenciaDeT.equalsIgnoreCase(DependenciaDeT.PASO_DE_TIEMPO)) {
            int avance = datGen.getCalculadorDePlazos().
                    hallaAvanceTiempo(tiempoAbsolutoBase, ta);
            if (avance < cantModulosT.size()) {
                // Hay dato explÃ­cito para el tiempoAbsoluto ta
                return cantModulosT.get(avance);
            } else {
                // Se toma el dato del Ãºltimo tiempoAbsoluto con dato
                return cantModulosT.get(cantModulosT.size() - 1);
            }
        }
        return cant;
    }

    /** Halla los costos fijos de un mÃ³dulo para todos los numerarios cuando el recursoBase
     *  es invariable o depende del paso de tiempo
     *
     *  Devuelve cero cuando el tiempo absoluto
     *  ta es anterior a tiempoAbsolutoBase
     *  o bien ta es posterior al ultimoTiempoAbsoluto
     *
     *  Extrapola los valores del Ãºltimo ta con dato hasta ultimoTiempoAbsoluto
     *
     **/
    public ArrayList<Double> costoFijo1ModDeTiempoAbsoluto(Estudio est,
            TiempoAbsoluto ta) {
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        assert (!(this).getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.EDAD)) :
                "El recurso " + nombre + "depende de la edad y se invoco "
                + "cantModulosDeTiempoAbsoluto";
        ArrayList<Double> costos = new ArrayList<Double>();
        int cantNum = est.getDatosGenerales().getNumerarios().size();
        for (int inum = 0; inum < cantNum; inum++) {
            if (datGen.getCalculadorDePlazos().hallaAvanceTiempo(ta, ultimoTiempoAbsoluto) < 0) {
                costos.add(0.0);
            } else if (dependenciaDeT.equalsIgnoreCase(DependenciaDeT.NO)) {
                costos.add(costosFijos.get(inum).get(costosFijos.get(inum).size() - 1));
            } else if (dependenciaDeT.equalsIgnoreCase(DependenciaDeT.PASO_DE_TIEMPO)) {
                assert (tiempoAbsolutoBase != null) : "El recurso " + nombre
                        + "que depende del tiempo, no tiene tiempoAbsolutoBase";
                int avance = datGen.getCalculadorDePlazos().
                        hallaAvanceTiempo(tiempoAbsolutoBase, ta);
                if (avance < 0) {
                    costos.add(0.0);
                } else if (avance < costosFijos.get(inum).size()) {
                    // Hay dato explÃ­cito para el tiempoAbsoluto ta
                    costos.add(costosFijos.get(inum).get(avance));
                } else {
                    // Se toma el dato del Ãºltimo tiempoAbsoluto con dato
                    costos.add(costosFijos.get(inum).get(costosFijos.get(inum).size() - 1));
                }
            }
        }
        return costos;
    }

    /**
     *  Devuelve la potencia nominal de un mÃ³dulo del recurso base
     *  dada una edad del recurso asociado.
     *  Devuelve cero si para la edad la potencia
     *  no estÃ¡ definida.
     *  La edad de los recursos empieza en cero
     *  No se puede aplicar a RecursosBase invariables
     *  o que dependen del tiempo
     */
    public double potNom1ModDeEdad(int edad) throws XcargaDatos {

        assert ((this).getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.EDAD)) :
                "El recurso " + nombre + "no depende de la edad y se invocÃ³ "
                + "potenciaNominalDeEdad";
        if (potNominalT.isEmpty()) {
            throw new XcargaDatos("El recurso " + nombre
                    + " , que depende de la edad no tiene datos de potNominalT");
        }
        if (edad < potNominalT.size()) {
            return potNominalT.get(edad);
        } else if (edad < vidaUtil) {
            // se toma el Ãºltimo valor con dato
            return potNominalT.get(potNominalT.size() - 1);
        } else {
            // se superÃ³ la vida Ãºtil
            return 0.0;
        }
    }

    /**
     *  Devuelve la cantidad de mÃ³dulos del recurso base
     *  dada una edad del recurso asociado.
     *  Devuelve cero si se pide la cant de mÃ³dulos para una edad mayor a la vida Ãºtil.
     *  Si para una edad no alcanzan los datos, supone vÃ¡lido el dato del Ãºltimo paso de tiempo con dato.
     *  La edad de los recursos empieza en cero
     *  No se puede aplicar a RecursosBase invariables
     *  o que dependen del tiempo
     */
    public int cantModulosDeEdad(int edad) throws XcargaDatos {

        assert ((this).getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.EDAD)) :
                "El recurso " + nombre + "no depende de la edad y se invocÃ³ "
                + "potenciaNominalDeEdad";

        if (cantModulosT.isEmpty()) {
            throw new XcargaDatos("El recurso " + nombre
                    + " , que depende de la edad no tiene datos de candModulosT");
        }
        if (edad < cantModulosT.size()) {
            return cantModulosT.get(edad);
        } else if (edad < vidaUtil) {
            // se toma el Ãºltimo valor con dato
            return cantModulosT.get(cantModulosT.size() - 1);
        } else {
            // se superÃ³ la vida Ãºtil
            return 0;
        }
    }

    /**
     *  Devuelve el costo fijo por numerario de un mÃ³dulo del recurso base
     *  dada una edad del recurso asociado.
     *  Devuelve cero si se pide la cant de mÃ³dulos para una edad mayor a la vida Ãºtil.
     *  Si para una edad no alcanzan los datos, supone vÃ¡lido el dato del Ãºltimo paso de tiempo con dato.
     *  La edad de los recursos empieza cero
     *  No se puede aplicar a RecursosBase invariables
     *  o que dependen del tiempo
     */
    public ArrayList<Double> costoFijo1ModDeEdad(Estudio est, int edad) throws XcargaDatos {
        assert ((this).getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.EDAD)) :
                "El recurso " + nombre + "no depende de la edad y se invoco "
                + "cantModulosDeEdad";
        ArrayList<Double> costos = new ArrayList<Double>();
        int cantNum = est.getDatosGenerales().getNumerarios().size();
        for (int inum = 1; inum <= cantNum; inum++) {
            String nombreNum = est.getDatosGenerales().getNumerarios().get(inum).getNombre();
            if (costosFijos.get(inum).isEmpty()) {
                throw new XcargaDatos("El recurso " + nombre
                        + " , que depende de la edad no tiene datos de costos fijos para el numerario " + nombreNum);
            }
            if (edad < costosFijos.get(inum).size()) {
                costos.add(costosFijos.get(inum).get(edad));
            } else if (edad < vidaUtil) {
                // se toma el Ãºltimo valor con dato
                costos.add(costosFijos.get(inum).get(costosFijos.get(inum).size() - 1));
            } else {
                // se superÃ³ la vida Ãºtil
                costos.add(0.0);
            }
        }
        return costos;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

//    public void setClase(String clase) {
//        assert (clase.equalsIgnoreCase("EXISTENTE") ||
//                clase.equalsIgnoreCase("ELEGIBLE") ||
//                clase.equalsIgnoreCase("TRANSITORIO")) :
//                "clase de un recurso no es de los tipos vÃ¡lidos, recurso :" +
//                this.nombre + " - clase leÃ­da:" + clase;
//        this.clase = ExistEleg.ELEG;
//        if (clase.equalsIgnoreCase("EXISTENTE")) {
//            this.clase = ExistEleg.EXIST;
//        }
//        if (clase.equalsIgnoreCase("TRANSITORIO")) {
//            this.clase = ExistEleg.TRANS;
//        }
//    }
    public Punto getPuntoConex() {
        return puntoConex;
    }

    public void setPuntoConex(Punto puntoConex) {
        this.puntoConex = puntoConex;
    }

//     public Estudio getEst() {
//          return est;
//     }
//
//     public void setEst(Estudio est) {
//          this.est = est;
//     }
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSubtipo1() {
        return subtipo1;
    }

    public void setSubtipo1(String subt1) throws XcargaDatos {
        if (subt1.equalsIgnoreCase("termico") || subt1.equalsIgnoreCase("t�rmico")
                || subt1.equalsIgnoreCase("TERM")) {
            subtipo1 = SubTipo1.TERM;
        } else if (subt1.equalsIgnoreCase("cicloCombinado") || subt1.equalsIgnoreCase("cicloComb")
                || subt1.equalsIgnoreCase("CCOMB")) {
            subtipo1 = SubTipo1.CCOMB;            
        } else if (subt1.equalsIgnoreCase("hidraulico") || subt1.equalsIgnoreCase("hidráulico")
                || subt1.equalsIgnoreCase("HID")) {
            subtipo1 = SubTipo1.HID;
        } else if (subt1.equalsIgnoreCase("eolico") || subt1.equalsIgnoreCase("e�lico")
                || subt1.equalsIgnoreCase("EOLO")) {
            subtipo1 = SubTipo1.EOLO;
        } else if (subt1.equalsIgnoreCase("biomasa") || subt1.equalsIgnoreCase("bio")) {
            subtipo1 = SubTipo1.BIO;
        } else if (subt1.equalsIgnoreCase("importacion") || subt1.equalsIgnoreCase("importaci�n")
                || subt1.equalsIgnoreCase("IMP")) {
            subtipo1 = SubTipo1.IMP;
        } else if (subt1.equalsIgnoreCase("exportacion") || subt1.equalsIgnoreCase("exportaci�n")
                || subt1.equalsIgnoreCase("EXP")) {
            subtipo1 = SubTipo1.EXP;
        } else if (subt1.equalsIgnoreCase("fotovoltaico") || subt1.equalsIgnoreCase("solar")
                || subt1.equalsIgnoreCase("FOTO")) {
            subtipo1 = SubTipo1.FOTO;
        } else if (subt1.equalsIgnoreCase("acumulaci�n") || subt1.equalsIgnoreCase("acumulador")
                || subt1.equalsIgnoreCase("ACUM")) {
            subtipo1 = SubTipo1.ACUM;
            
        } else {
            throw new XcargaDatos("El RecursoBase " + nombre + " tiene subtipo1 no conocido: " + subt1);
        }
    }

//    public void setSubtipo1(String subtipo1) {
//        this.subtipo1 = subtipo1;
//    }
    public String getSubtipo2() {
        return subtipo2;
    }

//    public void setSubtipo2(String subtipo2) {
//        this.subtipo2 = subtipo2;
//    }
    public void setSubtipo2(String subt2) {
        if (subt2.equalsIgnoreCase("FORMUL")) {
            subtipo2 = SubTipo2.FORMUL;
        } else if (subt2.equalsIgnoreCase("ESTAC")) {
            subtipo2 = SubTipo2.ESTAC;
        } else if (subt2.equalsIgnoreCase("FIJO")) {
            subtipo2 = SubTipo2.FIJO;
        } else {
            subtipo2 = subt2;
        }
    }

    public boolean getTieneEdad() {
        return tieneEdad;
    }

    public void setTieneEdad(boolean tieneEdad) {
        this.tieneEdad = tieneEdad;
    }

    public String getDependenciaDeT() {
        return dependenciaDeT;
    }

//    public void setDependenciaDeT(DependenciaDeT dependenciaDeT) {
//        this.dependenciaDeT = dependenciaDeT;
//    }
    public void setDependenciaDeT(String dependenciaDeT) {
        assert (dependenciaDeT.equalsIgnoreCase("NO")
                || dependenciaDeT.equalsIgnoreCase("EDAD")
                || dependenciaDeT.equalsIgnoreCase("PASO_DE_TIEMPO")) :
                "Dependencia de T de un recurso no es un tipo admisible:"
                + this.nombre + " - dependencia leÃ­da:" + dependenciaDeT;
        if (dependenciaDeT.equalsIgnoreCase("NO")) {
            this.dependenciaDeT = DependenciaDeT.NO;
        } else if (dependenciaDeT.equalsIgnoreCase("EDAD")) {
            this.dependenciaDeT = DependenciaDeT.EDAD;
        } else {
            this.dependenciaDeT = DependenciaDeT.PASO_DE_TIEMPO;
        }

    }

//	public String getDependenciaSegun() {
//		return dependenciaSegun;
//	}
//
//	public void setDependenciaSegun(String dependenciaSegun) {
//		this.dependenciaSegun = dependenciaSegun;
//	}
    public TiempoAbsoluto getTiempoAbsolutoBase() {
        return tiempoAbsolutoBase;
    }

    public void setTiempoAbsolutoBase(TiempoAbsoluto tiempoAbsolutoBase) {
        this.tiempoAbsolutoBase = tiempoAbsolutoBase;
    }

    public TiempoAbsoluto getUltimoTiempoAbsoluto() {
        return ultimoTiempoAbsoluto;
    }

    public void setUltimoTiempoAbsoluto(TiempoAbsoluto ultimoTiempoAbsoluto) {
        this.ultimoTiempoAbsoluto = ultimoTiempoAbsoluto;
    }

    public int getVidaUtil() {
        return vidaUtil;
    }

    public void setVidaUtil(int vidaUtil) {
        this.vidaUtil = vidaUtil;
    }

    public int getPerConstr() {
        return perConstr;
    }

    /**
     * Devuelve el perÃ­odo de construcciÃ³n de un RecursoBase teniendo en cuanta la
     * limitaciÃ³n que depende del TiempoAbsoluto leÃ­da en los DatosGeneralesEstudio
     *
     * @param ta
     * @param est
     * @return
     * @throws persistenciaMingo.XcargaDatos
     */
    public int getPerConstr(TiempoAbsoluto ta, Estudio est) throws XcargaDatos {
        int result = 0;
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int pasoTa = datGen.pasoDeTiempoAbsoluto(ta);
        result = Math.min(datGen.getMaximoPerConst().get(pasoTa), perConstr);
        return result;
    }

    public void setPerConstr(int perConstr) {
        this.perConstr = perConstr;
    }

    public boolean getTieneDemoraAl() {
        return tieneDemoraAl;
    }

    public void setTieneDemoraAl(boolean tieneDemoraAl) {
        this.tieneDemoraAl = tieneDemoraAl;
    }

    public ArrayList<Integer> getValoresDemAl() {
        return valoresDemAl;
    }

    public void setValoresDemAl(ArrayList<Integer> valoresDemAl) {
        this.valoresDemAl = valoresDemAl;
    }

    public boolean getTieneCFijAleat() {
        return tieneCFijAleat;
    }

    public void setTieneCFijAleat(boolean tieneCFijAleat) {
        this.tieneCFijAleat = tieneCFijAleat;
    }

    public VariableNat getVNCFijAl() {
        return vNCFijAl;
    }

    public void setVNCFijAl(VariableNat vNCFijAl) {
        this.vNCFijAl = vNCFijAl;
    }

    public VariableNat getVNDemoraAl() {
        return vNDemoraAl;
    }

    public void setVNDemoraAl(VariableNat vNDemoraAl) {
        this.vNDemoraAl = vNDemoraAl;
    }

    public VariableNat getVNInvAl() {
        return vNInvAl;
    }

    public void setVNInvAl(VariableNat vNInvAl) {
        this.vNInvAl = vNInvAl;
    }

    public ArrayList<Double> getValoresCFijAl() {
        return valoresCFijAl;
    }

    public void setValoresCFijAl(ArrayList<Double> valoresCFijAl) {
        this.valoresCFijAl = valoresCFijAl;
    }

    public ArrayList<CantProductoSimple> getCantsDeProductos1Mod() {
        return cantsDeProductos1Mod;
    }

    public void setCantsDeProductos1Mod(ArrayList<CantProductoSimple> cantsDeProductos1Mod) {
        this.cantsDeProductos1Mod = cantsDeProductos1Mod;
    }

    /**
     *  Devuelve la cantidad de producto supuesto que es una
     *  cantidad de producto simple, en el tiempoAbsolutoBase
     *  o en el primer aÃ±o de vida del recursoBase, cuando tiene la potencia nominal potNominalTABase
     */
    public CantProductoSimple cantBaseUnProd1Mod(Producto pr) {

        boolean encontro = false;
        int i = 0;
        do {
            if (cantsDeProductos1Mod.get(i).getProducto() == pr) {
                encontro = true;
                return cantsDeProductos1Mod.get(i);
            }
            i++;
        } while (!encontro & i < cantsDeProductos1Mod.size());
        assert (false) : "No se encontrÃ³ la oferta del producto " + pr.getNombre() + " del recursoBase " + nombre;
        return null;
    }

    public ArrayList<ArrayList<Double>> getCostosFijos() {
        return costosFijos;
    }

    public void setCostosFijos(ArrayList<ArrayList<Double>> costosFijos) {
        this.costosFijos = costosFijos;
    }

    public ArrayList<ArrayList<Double>> getInversiones() {
        return inversiones;
    }

    public void setInversiones(ArrayList<ArrayList<Double>> inversiones) {
        this.inversiones = inversiones;
    }

    public boolean getTieneInvAleat() {
        return tieneInvAleat;
    }

    public void setTieneInvAleat(boolean tieneInvAleat) {
        this.tieneInvAleat = tieneInvAleat;
    }

    public ArrayList<Double> getValoresInvAl() {
        return valoresInvAl;
    }

    public void setValoresInvAl(ArrayList<Double> valoresInvAl) {
        this.valoresInvAl = valoresInvAl;
    }

    public boolean getAdmiteCambioAleatorio() {
        return admiteCambioAleatorio;
    }

    public void setAdmiteCambioAleatorio(boolean admiteCambioAleatorio) {
        this.admiteCambioAleatorio = admiteCambioAleatorio;
    }

    /**
     *  Carga en this el contenido del RecursoBase rB
     * 
     *  ESTE MÃ‰TODO ES NECESARIO PORQUE DE ENTRADA LOS RECURSOS
     *  BASE NO SE CREAN CON LA CLASE HIJA SINO CON LA RECURSOSBASE
     *  Y SOLO DESPUÃ‰S SE RECREAN CON LA CLASE HIJA ADECUADA
     *  ESTO DEBE CORREGIRSE !!!!!!! Y ELIMINAR ESTE METODO
     *  @param rB el RecursoBase que se quiere copiar sobre this
     */
    public void cargarRB(RecursoBase rB) throws XcargaDatos {
        this.setNombre(rB.getNombre());
//		this.setCantModulos(rB.getCantModulos());
        this.setNombreEquivEnSimul(rB.getNombreEquivEnSimul());
        this.setPotNominalDT(rB.getPotNominalDT());        
        this.setCantModulosDT(rB.getCantModulosDT());
        this.setPotNominalTABase(rB.getPotNominalTABase());
        this.setCantModulosT(rB.getCantModulosT());
        this.setPotNominalT(rB.getPotNominalT());
        this.setClase(rB.getClase());
        this.setPuntoConex(rB.getPuntoConex());
        this.setTipo(rB.getTipo());
        this.setSubtipo1(rB.getSubtipo1());
        this.setSubtipo2(rB.getSubtipo2());
        this.setTieneEdad(rB.getTieneEdad());
        this.setDependenciaDeT(rB.getDependenciaDeT());
        this.setEscalamientocFijo(rB.getEscalamientocFijo());
        this.setEscalamientoInv(rB.getEscalamientoInv());
        this.setTiempoAbsolutoBase(rB.getTiempoAbsolutoBase());
        this.setUltimoTiempoAbsoluto(rB.getUltimoTiempoAbsoluto());
        this.setVidaUtil(rB.getVidaUtil());
        this.setPerConstr(rB.getPerConstr());

        this.setCantsDeProductos1Mod(rB.getCantsDeProductos1Mod());
        this.setInversiones(rB.getInversiones());
        this.setCostosFijos(rB.getCostosFijos());

        this.setTieneDemoraAl(rB.getTieneDemoraAl());
        this.setVNDemoraAl(rB.getVNDemoraAl());
        this.setValoresDemAlS(rB.getValoresDemAlS());

        this.setTieneCFijAleat(rB.getTieneCFijAleat());
        this.setVNCFijAl(rB.getVNCFijAl());
        this.setValoresCFijAl(rB.getValoresCFijAl());

        this.setTieneInvAleat(rB.getTieneInvAleat());
        this.setVNInvAl(rB.getVNInvAl());
        this.setValoresInvAlS(rB.getValoresInvAlS());
        this.setAleatOper(rB.getAleatOper());

    }

    /**
     * Devuelve la posicion en el cÃ³digo de bloques de un parque (que coincide con la posiciÃ³n
     * en el vector de recursos posibles elegibles del Estudio), 
     * de un Recurso ELEG que inicia su construcciÃ³n en el TiempoAbsoluto ta.
     * Tiene en cuenta el mÃ¡ximo perÃ­odo de construcciÃ³n del ta propio del Estudio,
     * por eso la dependencia de ta.
     * Todos los demÃ¡s Recursos del mismo RecursoBase vienen despuÃ©s de este.
     */
    public int posicionCodigoRecInicioConst(Estudio est, TiempoAbsoluto ta) throws XcargaDatos {
        assert (clase.equalsIgnoreCase(ExistEleg.ELEG)) : "Se pidio posiciÃ³n de Recurso que inicia construcciÃ³n"
                + "del RecursoBase no ELEG de nombre: " + nombre;
        int perTa = est.getDatosGenerales().maxPerConstDeTA(ta);
        int ip = posicionRecursoInicial;
        /**
         * Si el perÃ­odo mÃ¡ximo del estudio acota el plazo de construcciÃ³n en ta,
         * no se toma el Recurso con plazo mayor del RecursoBase
         */
        if (perTa < perConstr) {
            ip = ip + perConstr - perTa;
        }
        return ip;

    }

//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final RecursoBase other = (RecursoBase) obj;
//        if ((this.nombre == null) ? (other.nombre != null) : !this.nombre.equals(other.nombre)) {
//            return false;
//        }
//        if (!this.clase.equalsIgnoreCase(other.clase)) {
//            return false;
//        }
//        if (this.potNominalTABase != other.potNominalTABase && (this.potNominalTABase == null || !this.potNominalTABase.equals(other.potNominalTABase))) {
//            return false;
//        }
//        if (this.puntoConex != other.puntoConex && (this.puntoConex == null || !this.puntoConex.equals(other.puntoConex))) {
//            return false;
//        }
//        if (!this.tipo.equalsIgnoreCase(other.tipo)) {
//            return false;
//        }
//        if (!this.subtipo1.equalsIgnoreCase(other.subtipo1)) {
//            return false;
//        }
//        if (!this.subtipo2.equalsIgnoreCase(other.subtipo2)) {
//            return false;
//        }
//        if (this.tieneEdad != other.tieneEdad) {
//            return false;
//        }
//        if (!this.dependenciaDeT.equalsIgnoreCase(other.dependenciaDeT)) {
//            return false;
//        }
//        if (this.tiempoAbsolutoBase != other.tiempoAbsolutoBase && (this.tiempoAbsolutoBase == null || !this.tiempoAbsolutoBase.equals(other.tiempoAbsolutoBase))) {
//            return false;
//        }
//        if (this.ultimoTiempoAbsoluto != other.ultimoTiempoAbsoluto && (this.ultimoTiempoAbsoluto == null || !this.ultimoTiempoAbsoluto.equals(other.ultimoTiempoAbsoluto))) {
//            return false;
//        }
//        if (this.potNominalT != other.potNominalT && (this.potNominalT == null || !this.potNominalT.equals(other.potNominalT))) {
//            return false;
//        }
//        if (this.cantModulosT != other.cantModulosT && (this.cantModulosT == null || !this.cantModulosT.equals(other.cantModulosT))) {
//            return false;
//        }
//        if (this.vidaUtil != other.vidaUtil) {
//            return false;
//        }
//        if (this.perConstr != other.perConstr) {
//            return false;
//        }
//        if (this.tieneDemoraAl != other.tieneDemoraAl) {
//            return false;
//        }
//        if (this.vNDemoraAl != other.vNDemoraAl && (this.vNDemoraAl == null || !this.vNDemoraAl.equals(other.vNDemoraAl))) {
//            return false;
//        }
//        if (this.valoresDemAl != other.valoresDemAl && (this.valoresDemAl == null || !this.valoresDemAl.equals(other.valoresDemAl))) {
//            return false;
//        }
//        if (this.cantsDeProductos1Mod != other.cantsDeProductos1Mod && (this.cantsDeProductos1Mod == null || !this.cantsDeProductos1Mod.equals(other.cantsDeProductos1Mod))) {
//            return false;
//        }
//        if (this.inversiones != other.inversiones && (this.inversiones == null || !this.inversiones.equals(other.inversiones))) {
//            return false;
//        }
//        if (this.costosFijos != other.costosFijos && (this.costosFijos == null || !this.costosFijos.equals(other.costosFijos))) {
//            return false;
//        }
//        if (this.tieneInvAleat != other.tieneInvAleat) {
//            return false;
//        }
//        if (this.valoresInvAl != other.valoresInvAl && (this.valoresInvAl == null || !this.valoresInvAl.equals(other.valoresInvAl))) {
//            return false;
//        }
//
//        if (this.vNInvAl != other.vNInvAl && (this.vNInvAl == null || !this.vNInvAl.equals(other.vNInvAl))) {
//            return false;
//        }
//        if (this.admiteCambioAleatorio != other.admiteCambioAleatorio) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 5;
//        hash = 79 * hash + (this.nombre != null ? this.nombre.hashCode() : 0);
//        hash = 79 * hash + (this.clase != null ? this.clase.hashCode() : 0);
////          hash = 79 * hash + (this.clase != null ? ExistEleg.hashSustituto(this.clase) : 0);
//        hash = 79 * hash + (this.potNominalTABase != null ? this.potNominalTABase.hashCode() : 0);
//        hash = 79 * hash + (this.puntoConex != null ? this.puntoConex.hashCode() : 0);
//        hash = 79 * hash + (this.tipo != null ? this.tipo.hashCode() : 0);
////          hash = 79 * hash + (this.tipo != null ? TipoRec.hashSustituto(this.tipo) : 0);
//        hash = 79 * hash + (this.subtipo1 != null ? this.subtipo1.hashCode() : 0);
////          hash = 79 * hash + (this.subtipo1 != null ? SubTipo1.hashSustituto(this.subtipo1) : 0);
//        hash = 79 * hash + (this.subtipo2 != null ? this.subtipo2.hashCode() : 0);
////          hash = 79 * hash + (this.subtipo2 != null ? SubTipo2.hashSustituto(this.subtipo2) : 0);
//        hash = 79 * hash + (this.tieneEdad ? 1 : 0);
//        hash = 79 * hash + (this.dependenciaDeT != null ? this.dependenciaDeT.hashCode() : 0);
////         hash = 79 * hash + (this.dependenciaDeT != null ? DependenciaDeT.hashSustituto(this.dependenciaDeT) : 0);
//        hash = 79 * hash + (this.tiempoAbsolutoBase != null ? this.tiempoAbsolutoBase.hashCode() : 0);
//        hash = 79 * hash + (this.ultimoTiempoAbsoluto != null ? this.ultimoTiempoAbsoluto.hashCode() : 0);
//        hash = 79 * hash + (this.potNominalT != null ? this.potNominalT.hashCode() : 0);
//        hash = 79 * hash + (this.cantModulosT != null ? this.cantModulosT.hashCode() : 0);
//        hash = 79 * hash + this.vidaUtil;
//        hash = 79 * hash + this.perConstr;
//        hash = 79 * hash + (this.tieneDemoraAl ? 1 : 0);
//        hash = 79 * hash + (this.vNDemoraAl != null ? this.vNDemoraAl.hashCode() : 0);
//        hash = 79 * hash + (this.valoresDemAl != null ? this.valoresDemAl.hashCode() : 0);
//        hash = 79 * hash + (this.cantsDeProductos1Mod != null ? this.cantsDeProductos1Mod.hashCode() : 0);
//        hash = 79 * hash + (this.inversiones != null ? this.inversiones.hashCode() : 0);
//        hash = 79 * hash + (this.costosFijos != null ? this.costosFijos.hashCode() : 0);
//        hash = 79 * hash + (this.tieneInvAleat ? 1 : 0);
//        hash = 79 * hash + (this.valoresInvAl != null ? this.valoresInvAl.hashCode() : 0);
//        hash = 79 * hash + (this.vNInvAl != null ? this.vNInvAl.hashCode() : 0);
//        hash = 79 * hash + (this.admiteCambioAleatorio ? 1 : 0);
////          try {
////               DirectoriosYArchivos.agregaTexto("X:/EDF/2011/PruebasMingo/PruebaJava4/corridas", nombre + "_" + hash + "\r\n");
////          } catch (XcargaDatos ex) {
////               Logger.getLogger(RecursoBase.class.getName()).log(Level.SEVERE, null, ex);
////          }
//        return hash;
//    }

    @Override
    public String toString() {
        String texto = "";
        int i;
        texto += "RECURSO BASE: " + nombre + "\n";
        texto += "Clase: " + clase + "\n";
        texto += "Punto de conexiÃ³n: " + puntoConex.toString() + "\n";
        texto += "Tipo: " + tipo + "\n";
        texto += "Subtipo 1: " + subtipo1 + "\n";
        texto += "Subtipo 2: " + subtipo2 + "\n";
        texto += "Tiene edad: " + tieneEdad + "\n";
        texto += "Dependencia del tiempo: " + dependenciaDeT + "\n";
//		texto += "Dependencia segÃºn: " + dependenciaSegun + "\n";
        texto += "Cantidad de mÃ³dulos: ";
        for (Integer in : cantModulosT) {
            texto += in + "  ";
        }
        texto += "\n";
        texto += "Potencia nominal: ";
        for (Double d : potNominalT) {
            texto += d + "  ";
        }
        texto += "\n";
        texto += "Tiempo absoluto base: " + tiempoAbsolutoBase.toString() + "\n";
        texto += "Ultimo tiempo absoluto: " + ultimoTiempoAbsoluto.toString() + "\n";
        texto += "Vida Ãºtil: " + vidaUtil + "\n";
        texto += "PerÃ­odo de construcciÃ³n: " + perConstr + "\n";
        texto += "Â¿Tiene demora aleatoria? " + tieneDemoraAl + "\n";
        texto += "Valores de la demora aleatoria: ";
        for (i = 0; i < valoresDemAlS.size(); i++) {
            texto += " \t" + valoresDemAlS.get(i);
        }
        texto += "\n";
        texto += "Ofertas de producto por mÃ³dulo (en el tiempoAbsolutoBase "
                + "o primer aÃ±o de edad" + "\n";
        for (i = 0; i < cantsDeProductos1Mod.size(); i++) {
            texto += cantsDeProductos1Mod.get(i).toString() + "\n";
        }
        for (i = 0; i < inversiones.size(); i++) {
            texto += "InversiÃ³n: " + inversiones.get(i).toString() + "\n";
        }
        for (i = 0; i < costosFijos.size(); i++) {
            texto += "Costo fijo: " + costosFijos.get(i).toString() + "\n";
        }
        texto += "Â¿La inversiÃ³n es aleatoria? " + tieneInvAleat + "\n";
        texto += "Valores de la variable aleatoria de inversiÃ³n: ";
        for (i = 0; i < valoresInvAlS.size(); i++) {
            texto += " \t" + valoresInvAlS.get(i);
        }
        texto += "\n";

        return texto;
    }
//	public static void main(String[] args) throws IOException, XcargaDatos{
//
//		Estudio est1 = new Estudio("Estudio de prueba", 5);
//		String directorio = "D:/Java/PruebaJava2";
//		est1.leerEstudio(directorio, true);
//		String texto = "\n" + "EMPIEZA LISTA DE POTENCIAS Y CANTIDAD DE MODULOS" +
//				" DE RECURSOS BASE DEL ESTUDIO" + "\n";
//		System.out.print(texto);
//
//		TiempoAbsoluto ta = new TiempoAbsoluto(2012);
//
//		for (RecursoBase rb : est1.getConjRecB().getRecursosB() ){
//			if (rb.getDependenciaDeT()!=DependenciaDeT.EDAD) {
//				double potencia =
//				rb.potNom1ModDeTiempoAbsoluto(est1.getDatosGenerales(), ta);
//				int cantMod =
//				rb.cantModulosDeTiempoAbsoluto(est1.getDatosGenerales(), ta);
//
//				texto = ta.toString() + "RecursoBase: "
//						+ rb.getNombre() + " potencia = " + potencia
//						+ " cantidad de mÃ³dulos = " + cantMod + "\n" + "\n";
//			}else{
//				texto = rb.getNombre() + " depende de la edad"+ "\n" + "\n";
//			}
//			System.out.print(texto);
//		}
//	}
}
