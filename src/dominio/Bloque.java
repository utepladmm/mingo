/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import GrafoEstados.Numerario;
//import dominio.RecursoBase.TipoRec;
import TiposEnum.EstadosRec;
import TiposEnum.TipoBloque;

import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.NodoN;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class Bloque implements Serializable, Comparable {

    private String tipoBloque; // REC o TRANS
    private TiempoAbsoluto ta;
    /**
     * El objeto puede ser un Recurso o una Transformación
     */
    private RecOTrans objeto;
    /**
     * si es mayor que 1 indica que hay muchos idénticos
     */
    private int multiplicador;   
    private String resumenB;

    /**
     * Estados posibles de un Recurso o Transformacion del bloque.
     */
//     public enum EstadosRec {
//
//          OPE, // operativo
//          CON, // en construcción
//          DEM, // demorado
//          FALL, // fallido (Se aplica a Transformaciones que no se completaron por falta de los recursos origen
//          // pero cuyos costos se cargan igual.
//          TERMIN, // terminado (Se aplica a Transformaciones que se completaron y se mantienen en el parque
//          // para el cálculo de sus anualidades de inversión.
//          TODOS; // cualquiera de los anteriores
//
//          public static int hashSustituto(EstadosRec estadosRec){
//               int hashS = 0;
//               if(estadosRec == EstadosRec.OPE){
//                    hashS = (EstadosRec.OPE).hashCode();
//               }else if(estadosRec == EstadosRec.CON){
//                    hashS = (EstadosRec.CON).hashCode();
//               }else if(estadosRec == EstadosRec.DEM){
//                    hashS = ("DEM").hashCode();
//               }else if(estadosRec == EstadosRec.FALL){
//                    hashS = (EstadosRec.FALL).hashCode();
//               }else if(estadosRec == EstadosRec.TERMIN){
//                    hashS = ("TERMIN").hashCode();
//               }else if(estadosRec == EstadosRec.TODOS){
//                    hashS = ("TODOS").hashCode();
//               }
//               return hashS;
//          }
//
//          public String stringDe(EstadosRec estadosRec){
//               String result = "";
//               if(estadosRec == EstadosRec.OPE){
//                    result = EstadosRec.OPE;
//               }else if(estadosRec == EstadosRec.CON){
//                    result = EstadosRec.CON;
//               }else if(estadosRec == EstadosRec.DEM){
//                    result = "DEM";
//               }else if(estadosRec == EstadosRec.FALL){
//                    result= EstadosRec.FALL;
//               }else if(estadosRec == EstadosRec.TERMIN){
//                    result = "TERMIN";
//               }else if(estadosRec == EstadosRec.TODOS){
//                    result = "TODOS";
//               }
//               return result;
//
//          }
//
//     public boolean equalsSustituto(EstadosRec otro){
//               boolean result = stringDe(this).equalsIgnoreCase(stringDe(otro));
//               return result;
//          }
//     }
    /**
     * Constructor de Bloque
     * @param objeto
     * @param multiplicador
     * @throws persistenciaMingo.XcargaDatos
     */
    public Bloque(RecOTrans objeto, int multiplicador, TiempoAbsoluto ta) throws XcargaDatos {
        this.ta = ta;
        this.objeto = objeto;
        this.multiplicador = multiplicador;
        if (!(objeto instanceof Recurso || objeto instanceof Transformacion)) {
            throw new XcargaDatos("Se trató de construir un bloque con un objeto que no es Recurso ni Transformacion");
        }
        if (objeto instanceof Recurso) {
            tipoBloque = TipoBloque.REC;
        } else if (objeto instanceof Transformacion) {
            tipoBloque = TipoBloque.TRANS;
        }
        boolean usaEquiv = false;
        resumenB = toStringCorto(usaEquiv);
    }

    public Bloque() {
    }

    public int compareTo(Object o) {
        Bloque orb = (Bloque) o;
        // la clase de ambos objetos es igual y se comparan los nombres
        return resumenB.compareToIgnoreCase(orb.getResumenB());
    }

    /**
     * Devuelve el tipoBloque: REC o TRANS
     * @return 
     */
    public String getTipoBloque() {
        return tipoBloque;
    }

    public void setTipoBloque(String tipoBloque) {
        this.tipoBloque = tipoBloque;
    }
    
    /**
     * Devuelve true si el bloque tiene un RecursoBase que es común.
     * @return 
     */
    public boolean recBaseEsComun(){
        boolean result = false;
        if(objeto instanceof Recurso && ((Recurso)objeto).getRecBase().esComun())
            result = true;
        return result;
    }

//     public enum TipoBloque {
//          REC,
//          TRANS;
//
//          public static int hashSustituto(TipoBloque tipo){
//               int hashS = 0;
//               if(tipo == TipoBloque.REC){
//                    hashS = (TipoBloque.REC).hashCode();
//               }else if(tipo == TipoBloque.TRANS){
//                    hashS = (TipoBloque.TRANS).hashCode();
//               }
//               return hashS;
//          }
//
//         public String stringDe(TipoBloque tipo){
//              String result = "";
//              if(tipo == TipoBloque.REC){
//                    result = TipoBloque.REC;
//               }else if(tipo == TipoBloque.TRANS){
//                    result = TipoBloque.TRANS;
//               }
//               return result;
//
//         }
//
//          public boolean equalsSustituto(TipoBloque otro){
//               boolean result = stringDe(this).equalsIgnoreCase(stringDe(otro));
//               return result;
//          }
//
//     }

    public TiempoAbsoluto getTa() {
        return ta;
    }

    public void setTa(TiempoAbsoluto ta) {
        this.ta = ta;
    }

    public int getMultiplicador() {
        return multiplicador;
    }

    public void setMultiplicador(int multiplicador) {
        this.multiplicador = multiplicador;
    }

    public RecOTrans getObjeto() {
        return objeto;
    }

    public String getResumenB() {
        return resumenB;
    }

    public void setResumenB(String resumenB) {
        this.resumenB = resumenB;
    }

    /**
     * Crea el resumen del bloque resumenB
     */
    public void cargaResumenB(){
        boolean usaEquiv = false;
        resumenB = toStringCorto(usaEquiv);
    }
    
    /**
     * Devuelve el Recurso del bloque si el bloque tiene un recurso
     * @return rec es el Recurso
     */
    public Recurso getRecurso() throws XcargaDatos {
        if (!(objeto instanceof Recurso)) {
            throw new XcargaDatos("un objeto " +
                    "de bloque no es un Recurso y se invocó getRecurso");
        }
        Recurso rec;
        rec = (Recurso) objeto;
        return rec;
    }

    /**
     * Devuelve la transformación del bloque
     * @return trans es la transformación
     */
    public Transformacion getTransformacion() throws XcargaDatos {
        if (!(objeto instanceof Transformacion)) {
            throw new XcargaDatos("un objeto " +
                    "de bloque no es un Transformacion y se invocó getTransformacion");
        }
        Transformacion trans;
        trans = (Transformacion) objeto;
        return trans;
    }

    public void setObjeto(RecOTrans objeto) {
        this.objeto = objeto;
    }

    /**
     * Devuelve el tipo de RecursoBase, por ejemplo GeneradorTermico
     * del bloque
     */
    public String tipoDeRB() throws XcargaDatos {
        if (!(objeto instanceof Recurso)) {
            throw new XcargaDatos("El objeto de un bloque " +
                    "no es un recurso");
        }
        String tipo = null;
        if (objeto instanceof Recurso) {
            tipo = ((Recurso) objeto).getRecBase().getTipo();
        }
        return tipo;
    }

    /**
     * Devuelve el estado del objeto Recurso o Transformación del bloque
     */
    public String estadoDeRecOTrans() throws XcargaDatos {
        if (objeto instanceof Recurso) {
            return ((Recurso) objeto).getEstado();
        } else if (objeto instanceof Transformacion) {
            return ((Transformacion) objeto).getEstado();
        } else {
            throw new XcargaDatos("Se pidió el estado de la Transformacion de un bloque " +
                    "que no tiene una transformacion");
        }
    }

    /**
     * Se puede aplicar a bloques de Recurso de estado OPE o a bloques
     * de Transformacion de estado FALL, de lo contrario da error, y devuelve un número
     * que resume el efecto de monto de inversión aleatoria y demora aleatoria de construcción
     *
     * @return multM es el real por el que hay que multiplicar la anualidad para aplicarle
     * el efecto de monto de inversión aleatoria y demora aleatoria de construcción.
     */
    public double multMontoInv(Estudio est) {
        double factorDesc = 1 / (1 + est.getDatosGenerales().getTasaDescPaso());
        double multM = 1;
        int demoraAleat = objeto.getValorDemAl();
        double valorInvAleat = objeto.getValorInvAl();
        multM = multM / (Math.pow(factorDesc, demoraAleat));
        multM = multM * valorInvAleat;
        return multM;
    }

    /**
     * Devuelve la cantidad de producto del producto pr
     * que tiene un bloque (todos los módulos), en el tiempo absoluto ta
     * Si el bloque es de una transformacion lanza una excepción.
     */
    public CantProductoSimple cantProd(Producto pr,
            DatosGeneralesEstudio datGen,
            TiempoAbsoluto ta) throws XcargaDatos {
        if (objeto instanceof Recurso) {
            CantProductoSimple result;
            result = ((Recurso) objeto).cantProdSimp(pr, datGen, ta).clona();
            result.escalarCant(multiplicador);
            return result;
        } else {
            throw new XcargaDatos("Se pidió la cantidad de producto de un bloque con una Transformacion " +
                    "el bloque es" + this.toString());
        }
    }

    /**
     * Devuelve true si el bloque corriente (this) y el bloque bOtro tienen
     * igual objeto, es decir Recurso o Transformación.
     * Dos bloques tienen el mismo objeto si el Recurso
     * Transformacion de ambas es equal (igual usando el método equals respectivo).
     * ATENCIÓN: LOS BLOQUES PUEDEN DIFERIR EN EL MULTIPLICADOR QUE NO ES PARTE DE objeto
     * @param bOtro
     * @return result true si tienen el mismo objeto
     */
    public boolean igualObjeto(Bloque bOtro) {
        boolean result = false;
        if (objeto.equals(bOtro.getObjeto())) {
            result = true;
        }
        return result;
    }

    /**
     * METODO copiaBloque
     * Crea un Bloque copia que es equal al original pero con
     * nuevos objetos Recursos y Tranformacion.
     * Sólo toma los mismos objetos RecursoBase o TransforBase
     * La copia puede ser manipulada sin que el Bloque original se vea afectado,
     * mientras no se alteren los RecursoBase o TransforBase.
     */
    public Bloque copiaBloque() throws XcargaDatos {
        Bloque copia = new Bloque();
        if (objeto instanceof Recurso) {
            copia.setObjeto(((Recurso) objeto).copiaRecurso());
        } else {
            copia.setObjeto(((Transformacion) objeto).copiaTrans());
        }
        copia.setTipoBloque(tipoBloque);
        copia.setResumenB(resumenB);
        copia.setMultiplicador(multiplicador);
        copia.setTa(ta.copiaTiempoAbsoluto());
        return copia;
    }

//    /**
//     * Calcula la anualidad de inversión del recurso o transformacion de este bloque teniendo en cuenta las eventuales
//     * demoras aleatorios y valor aleatorio de la inversión.
//     * Se supone que las inversiones por paso de tiempo están cargadas en el instante medio
//     * de cada paso en el período de construcción.
//     * Las anualidades se calculan para el instante medio de los pasos de la vida útil.
//     *
//     * @param obj es el Objetivo del problema, es relevante porque el peso de los numerarios
//     * de inversión depende del objetivo.
//     */
//    public double anualidInv(Objetivo obj) throws XcargaDatos{
//kk aca hay que crear la que devuelve un double por numerario
//        double anInv = 0;
//        int vidaUtil = 20;
//        DatosGeneralesEstudio datGen = obj.getEst().getDatosGenerales();
//        double tasaPaso = datGen.getTasaDescPaso();
//        double factorDescPaso = 1/(1+tasaPaso);
//        if(obj instanceof ObjGenCron){
//            ObjGenCron objGC = (ObjGenCron)obj;
//            ArrayList<Double> pondNum = objGC.getPonderadores();
//            ArrayList<ArrayList<Double>> inversiones = new ArrayList<ArrayList<Double>>();
//            if(tipoBloque == TipoBloque.REC){
//                Recurso rec = (Recurso)objeto;
//                if(! (rec.getEstado()== EstadosRec.OPE) ) throw new XcargaDatos("Se pidió la anualidad" +
//                        "de un recurso cuyo estado no es OPE");
//                RecursoBase rB = rec.getRecBase();
//                inversiones = rB.getInversiones();
//                vidaUtil = rB.getVidaUtil();
//
//            }else{
//                Transformacion trans = (Transformacion)objeto;
//                if( !(trans.getEstado()== EstadosRec.FALL) ) throw new XcargaDatos("Se pidió la anualidad" +
//                        "de una transformacion cuyo estado no es FALL");
//                TransforBase tB = ((Transformacion)objeto).getTransBase();
//                inversiones = tB.getInversiones();
//                /**
//                 * Se conviene en que las inversiones en transformaciones fallidas se amortizan en
//                 * 20 años, por lo que no se altera el valor 20 cargado al crear la variable.
//                 */
//            }
//            int inum = 0;
//            /**
//             * Se va actualizando al instante medio del paso de entrada en servicio los
//             * valores de inversión de cada numerario
//             */
//            for(Numerario num: datGen.getNumerarios()){
//                if(! inversiones.get(inum).isEmpty()){
//                    int perConst = inversiones.get(inum).size();
//                    for(int paso=0; paso<inversiones.size(); paso ++){
//                        anInv += pondNum.get(inum)*inversiones.get(inum).get(paso)*Math.pow(tasaPaso, perConst-paso) ;
//                    }
//                }
//                inum ++;
//            }
//
//            anInv = anInv*(1-factorDescPaso*Math.pow(tasaPaso, vidaUtil))/(1-factorDescPaso);
//            /**
//             * Se cambia la anualidad por efecto de demora aleatoria de inversión y monto aleatorio de inversión
//             */
//
//
//
//        }else{
//            throw new XcargaDatos("El objetivo no es de un tipo programado y se detectó al calcular la anualidad");
//        }
//        return anInv;
//    }
    
    /**
     * Devuelve el valor para cada numerario de la anualidad de inversión del recurso
     * o transformacion de este bloque teniendo en cuenta las eventuales
     * demoras aleatorios y valor aleatorio de la inversión.
     * Se consideran todos los módulos del RecursoBase asociado si es un bloque de Recurso.
     * Se supone que las inversiones por paso de tiempo están cargadas en el instante medio
     * de cada paso en el período de construcción.
     * Las anualidades se calculan para el instante medio de los pasos de la vida útil.
     *
     */
    public ArrayList<Double> anualidInv(Estudio est) throws XcargaDatos {
        ArrayList<Double> anInv = new ArrayList<Double>();
        int vidaUtil = 0;
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        double tasaPaso = datGen.getTasaDescPaso();
        double factorDescPaso = 1 / (1 + tasaPaso);
        ArrayList<ArrayList<Double>> inversiones = new ArrayList<ArrayList<Double>>();
        Recurso rec = null;
        Transformacion trans;
        if (tipoBloque.equalsIgnoreCase(TipoBloque.REC)) {
            rec = (Recurso) objeto;
            if (!(rec.getEstado().equalsIgnoreCase(EstadosRec.OPE))) {
                throw new XcargaDatos("Se pidió la anualidad" +
                        "de un recurso cuyo estado no es OPE");
            }

            RecursoBase rB = rec.getRecBase();
            inversiones = rB.getInversiones();
            vidaUtil = rB.getVidaUtil();
        } else {
            trans = (Transformacion) objeto;
            if (!(trans.getEstado().equalsIgnoreCase(EstadosRec.FALL) ||
                    trans.getEstado().equalsIgnoreCase("TERMIN"))) {
                throw new XcargaDatos("Se pidió la anualidad" +
                        "de una transformacion cuyo estado no es FALL ni TERMIN");
            }
            TransforBase tB = ((Transformacion) objeto).getTransBase();
            inversiones = tB.getInversiones();
            /**
             * Se conviene en que las inversiones en transformaciones fallidas se amortizan en
             * 20 años, por lo que no se altera el valor 20 cargado al crear la variable.
             */
        }
        int inum = 0;
        /**
         * Se va actualizando al instante medio del paso de entrada en servicio los
         * valores de inversión de cada numerario
         */
        for (Numerario num : datGen.getNumerarios()) {
            double inv1Num = 0.0;
            if (!inversiones.get(inum).isEmpty()) {
                int perConst = inversiones.get(inum).size();
                for (int paso = 0; paso < perConst; paso++) {
                    inv1Num += inversiones.get(inum).get(paso) / Math.pow(factorDescPaso, perConst - paso);
                }
            }
            inum++;
            double factorAnualiz = (1 - factorDescPaso * Math.pow(factorDescPaso, vidaUtil)) / (1 - factorDescPaso);
            double an1Num = inv1Num / factorAnualiz;

            /**
             * Si el bloque es de un Recurso se multiplica por la cantidad de
             * módulos que tiene el RecursoBase
             */
            if (tipoBloque.equalsIgnoreCase(TipoBloque.REC)) {
                RecursoBase rB = rec.getRecBase();
                an1Num = an1Num * rec.cantMod(datGen, ta);
            }

            /**
             * Se cambia la anualidad por efecto de demora aleatoria de inversión y monto aleatorio de inversión
             */
            an1Num = an1Num * multMontoInv(est);

            /**
             * Se multiplica por la cantidad de recursos o transformaciones del bloque (multipliador del Bloque),
             */
            an1Num = an1Num * multiplicador;
            anInv.add(an1Num);

        }
        return anInv;
    }

//    /**
//     * Devuelve el valor para cada numerario, del costo fijo del Recurso
//     * de este bloque, teniendo en cuenta todos los módulos del RecursoBase
//     * Los costos fijos se computan en el instante medio de los pasos de la vida útil.
//     * Las transformaciones no tienen costos fijos.
//     *
//     */
//    public ArrayList<Double> costosFijos(Estudio est) throws XcargaDatos {
//        ArrayList<Double> cosFij = new ArrayList<Double>();
//        DatosGeneralesEstudio datGen = est.getDatosGenerales();
//
//        if (tipoBloque.equalsIgnoreCase(TipoBloque.REC)) {
//            Recurso rec = (Recurso) objeto;
//            if (!(rec.getEstado().equalsIgnoreCase(EstadosRec.OPE))) {
//                throw new XcargaDatos("Se pidió el costo fijo" +
//                        "de un recurso cuyo estado no es OPE");
//            }
//            RecursoBase rB = rec.getRecBase();
//            int inum = 0;
//            double cos1Num;
//            for (Numerario num : datGen.getNumerarios()) {
//                cos1Num = rec.costoFijo1Mod(est, ta).get(inum);
//                /**
//                 * Multiplica por la cantidad de módulos del RecursoBase y por
//                 * el multiplicador del Bloque.
//                 */
//                cos1Num = cos1Num * rec.cantMod(datGen, ta) * multiplicador;
//                cosFij.add(cos1Num);
//                inum++;
//            }
//        }
//        return cosFij;
//    }

    /**
     * Devuelve el valor para cada numerario, del costo fijo del Recurso
     * de este bloque, teniendo en cuenta todos los módulos del RecursoBase
     * Los costos fijos se computan en el instante medio de los pasos de la vida útil.
     * Las transformaciones no tienen costos fijos.
     *
     */
    public ArrayList<Double> costosFijos(Estudio est, NodoN nN) throws XcargaDatos {
        ArrayList<Double> cosFij = new ArrayList<Double>();
        DatosGeneralesEstudio datGen = est.getDatosGenerales();

        if (tipoBloque.equalsIgnoreCase(TipoBloque.REC)) {
            Recurso rec = (Recurso) objeto;
            if (!(rec.getEstado().equalsIgnoreCase(EstadosRec.OPE))) {
                throw new XcargaDatos("Se pidió el costo fijo" +
                        "de un recurso cuyo estado no es OPE");
            }
            RecursoBase rB = rec.getRecBase();
            int inum = 0;
            double cos1Num;
            for (Numerario num : datGen.getNumerarios()) {
                cos1Num = rec.costoFijo1Mod(est, ta, nN).get(inum);
                /**
                 * Multiplica por la cantidad de módulos del RecursoBase y por
                 * el multiplicador del Bloque.
                 */
                cos1Num = cos1Num * rec.cantMod(datGen, ta) * multiplicador;
                cosFij.add(cos1Num);
                inum++;
            }
        }
        return cosFij;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bloque other = (Bloque) obj;
        if (!this.tipoBloque.equalsIgnoreCase(other.tipoBloque)) {
            return false;
        }
        if (this.objeto != other.objeto && (this.objeto == null || !this.objeto.equals(other.objeto))) {
            return false;
        }
        if (this.multiplicador != other.multiplicador) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.tipoBloque != null ? this.tipoBloque.hashCode() : 0);
//          hash = 97 * hash + (this.tipoBloque != null ? TipoBloque.hashSustituto(tipoBloque) : 0);
        hash = 97 * hash + (this.objeto != null ? this.objeto.hashCode() : 0);
        hash = 97 * hash + this.multiplicador;
//          System.out.println("tipoBloque" + this.tipoBloque.hashCode());
//          System.out.println("objeto" + this.objeto.hashCode());
//          System.out.println("multiplicador" + this.multiplicador);
        return hash;

    }

//    @Override
//    public String toString() {
//            String texto = "BLOQUE:" + "\n";
//            if(tipoBloque==TipoBloque.REC){
//                    Recurso recurso = new Recurso();
//                    recurso = (Recurso)objeto;
//                    texto += recurso.toString() + "\n";
//                    texto += "Multiplicador: " + multiplicador + "\n";
//                    return texto;
//            }else{
//                    Transformacion transfor = new Transformacion();
//                    transfor = (Transformacion)objeto;
//                    texto += transfor.toString()+ "\n";
//                    texto += "Multiplicador: " + multiplicador + "\n";
//                    return texto;
//            }
//    }
        
    @Override
    public String toString() {
        String texto = "BLOQUE:" + "\n";
        texto += objeto.toString() + "\n";
        texto += "Multiplicador: " + multiplicador + "\n";
        return texto;

    }


    public String toStringCorto(boolean usaEquiv) {
        String texto = objeto.toStringMuyCorto(usaEquiv);
        texto += "/m" + multiplicador;
        return texto;
    }
//    public static void main(String[] args) throws XcargaDatos, IOException{
//
//		Estudio estudioPrueba = new Estudio("Estudio de prueba", 5);
//		String directorio = "D:/Java/PruebaJava2";
//		estudioPrueba.leerEstudio(directorio, true);
//
//
//		RecursoBase quinta = new RecursoBase(estudioPrueba);
//        RecursoBase SalaB  = new RecursoBase(estudioPrueba);
//		RecursoBase mot200 = new RecursoBase(estudioPrueba);
//		TransforBase matarSalaB = new TransforBase(estudioPrueba);
//
//		quinta = estudioPrueba.getConjRecB().getUnRecB("5a");
//		SalaB  = estudioPrueba.getConjRecB().getUnRecB("6a");
//		mot200 = estudioPrueba.getConjRecB().getUnRecB("Mot200");
//
//		matarSalaB = estudioPrueba.getConjTransB().getUnaTransB("matarSalaB");
//
//        Recurso rquinta = new Recurso();
//        Recurso rSalaB  = new Recurso();
//		Recurso rmot200 = new Recurso();
//		Transformacion tmatarSalaB = new Transformacion();
//
//        rquinta.setRecBase(quinta);
//        rquinta.setEstado(EstadosRec.OPE);
//        rquinta.setIndicadorTiempo(1);
//		System.out.println(rquinta.toStringCorto());
//
//        rSalaB .setRecBase(SalaB );
//        rSalaB .setEstado(EstadosRec.OPE);
//        rSalaB .setIndicadorTiempo(1);
//        System.out.println(rSalaB .toStringCorto());
//
//		rmot200.setRecBase(mot200);
//        rmot200.setEstado(EstadosRec.OPE);
//        rmot200.setIndicadorTiempo(1);
//        System.out.println(rmot200.toStringCorto());
//
//		tmatarSalaB.setTransBase(matarSalaB);
//        tmatarSalaB.setEstado(EstadosRec.CON);
//        tmatarSalaB.setIndicadorTiempo(1);
//        System.out.println(tmatarSalaB.toStringCorto());
//		TiempoAbsoluto ta = new TiempoAbsoluto(2012);
//
//		Bloque bquinta = new Bloque(rquinta, 1, ta);
//		Bloque bsalaB = new Bloque(rSalaB, 1, ta);
//		Bloque bMot200 = new Bloque(rmot200, 2, ta);
//		Bloque bmatarSalaB = new Bloque(tmatarSalaB, 1, ta);
//
//		CantProductoSimple oferta;
//		Producto pr = new Producto("potencia_firme");
//
//		oferta = bquinta.cantProd(pr, estudioPrueba.getDatosGenerales(), ta);
//		System.out.print(oferta.toString());
//    }
}
