/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;

import persistenciaMingo.XcargaDatos;

/**
 * clase CantProductoSimple
 * 
 *
 * Esta clase describe la forma de la cant y demanda de productos
 * que hacen los recursosBase, Demandas y Cargas
 * @author ut469262
 *
 */
public class CantProductoSimple implements Serializable {

     private Producto producto;
     /**
      * cantidad escalar del producto
      */
     private Double cant;  

     public CantProductoSimple(Producto producto, Double cant) {
          this.producto = producto;
          this.cant = cant;
     }

     public CantProductoSimple(Producto producto) {
          this.producto = producto;
     }
     
     public CantProductoSimple() {
     }     
     

     public Double getCant() {
          return cant;
     }

     public void setCant(Double cant) {
          this.cant = cant;
     }

     public Producto getProducto() {
          return producto;
     }

     public void setProducto(Producto producto) {
          this.producto = producto;
     }

     /**
      * Devuelve una copia con idéntico producto y la misma cantidad
      * de producto que la CantProductoSimple this. La copia puede alterarse
      * sin modificar el original.
      */
     public CantProductoSimple clona() {
          CantProductoSimple result = new CantProductoSimple(producto, cant);
          return result;
     }

     /**
      * Suma a la cantidad corriente otra c2
      */
     public void sumaCant(CantProductoSimple c2) {
          assert (producto == c2.getProducto()) :
                  "Error se suman cants de productos distintos";
          cant = cant + c2.getCant();
     }

     /**
      * Crea una nueva CantProductoSimple que es la suma de c1 y c2, sin alterar los sumandos
      *
      * @param c1 es un sumando
      * @param c2 es otro sumando
      * @return suma es la suma de c1 y c2.
      */
     public static CantProductoSimple suma2Cants(CantProductoSimple c1, CantProductoSimple c2) {
          CantProductoSimple suma = c1.clona();
          suma.sumaCant(c2);
          return suma;
     }

     /**
      * Multiplica la cantidad de producto corriente this
      * por un escalar double;
      */
     public void escalarCant(double esc) {
          cant = cant * esc;
     }

     /**
      * Multiplica la cantidad de producto corriente this
      * por un escalar entero;
      */
     public void escalarCant(int esc) {
          cant = cant * (double) esc;
     }

     /**
      * Pone en c1 la cantidad nula
      */
     public void anularCant() {
          cant = 0.0;
     }

     /**
      * METODO comparaCant
      * Compara la cantidad (this) con c2
      * @param c2 es la cantidad a comparar con (this)
      *
      * @return result:
      *  1 si la cantidad (this) es mayor que c2,
      *  0 si la cantidad (this) es igual a c2, y
      *  -1 si la cantidad (this) es menor que c2
      */
     public int comparaCant(CantProductoSimple c2) {

          assert (producto == c2.getProducto()) : "Los productos no son iguales " + producto.toString() + " " + c2.toString();
          int result;
          if (cant > c2.getCant()) {
               result = 1;
          } else if (cant < c2.getCant()) {
               result = -1;
          } else {
               result = 0;
          }
          return result;
     }


     /**
      * Devuelve la proporción que la cantidad c1 es respecto a la cantidad c2
      * @param c1 es la cantidad divisor
      * @param c2 es la antidad dividendo
      * @return
      */
     public static double proporcionDeDosCant(CantProductoSimple c1, CantProductoSimple c2) throws XcargaDatos{
         double prop = 0.0;
         if(c2.getCant()==0.0) throw new XcargaDatos("Se pidió la propoción de Producto respecto a" +
                 "una cantidad nula");
         prop = c1.getCant()/c2.getCant();
         return prop;
     }


     @Override
     public boolean equals(Object obj) {
          if (obj == null) {
               return false;
          }
          if (getClass() != obj.getClass()) {
               return false;
          }
          final CantProductoSimple other = (CantProductoSimple) obj;
          if (this.producto != other.producto && (this.producto == null || !this.producto.equals(other.producto))) {
               return false;
          }
          if (this.cant != other.cant && (this.cant == null || !this.cant.equals(other.cant))) {
               return false;
          }
          return true;
     }

     @Override
     public int hashCode() {
          int hash = 3;
          hash = 53 * hash + (this.producto != null ? this.producto.hashCode() : 0);
          hash = 53 * hash + (this.cant != null ? this.cant.hashCode() : 0);
          return hash;
     }

     @Override
     public String toString() {
          String texto = "\n" + "PRODUCTO: ";
          texto += producto.toString();
          texto += "cant: " + cant.toString() + "\n";
          return texto;
     }

     public String toStringCorto() {
          String texto = producto.getNombre() + " ";
          texto += UtilitariosGenerales.Numeros.redondeaDouble(cant, 2) + "  ";
          return texto;
     }

     public String toStringCortoLinea() {
          String texto = producto.getNombre() + " ";
          texto += UtilitariosGenerales.Numeros.redondeaDouble(cant, 2) + ",";
          return texto;
     }

     public static void main(String[] args) {
          Producto pr = new Producto("Energía");
          CantProductoSimple c1 = new CantProductoSimple(pr, 10.0);
          CantProductoSimple c2 = new CantProductoSimple(pr, 10.0);
          c1.sumaCant(c2);
          c1.escalarCant(1.5);
          String texto = "La cantidad debe ser 30 y es : " + c1.getCant() + "\n";
          System.out.print(texto);


     }
}
