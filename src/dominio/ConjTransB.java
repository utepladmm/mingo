/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import dominio.DatosGeneralesEstudio;
import persistenciaMingo.CargaDatosGenerales;
import persistenciaMingo.CargaRecBase;
import persistenciaMingo.CargaTransBase;
import persistenciaMingo.LeerDatosArchivo;
import persistenciaMingo.XcargaDatos;

import java.util.ArrayList;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;

/**
 *
 * @author ut469262
 */
public class ConjTransB implements Serializable{

    private ArrayList<TransforBase> transforsB;
    private int iterador;

    public ConjTransB(){
        transforsB = new ArrayList<TransforBase>();
        iterador = 0;
    }

    public void agregar(TransforBase transB){
        transforsB.add(transB);
    }

    public void remover(String nomTransB){
        for(TransforBase trB: transforsB){
            if( nomTransB.compareToIgnoreCase(trB.getNombre()) == 0 ){
                transforsB.remove(trB);
            }
        }
    }

    public ArrayList<TransforBase> getTransforsB() {
        return transforsB;
    }

    public void setTransforsB(ArrayList<TransforBase> transforsB) {
        this.transforsB = transforsB;
    }

    /**
     * Devuelve la TransformacionBase de nombre nombre contenida en el 
     * comjunto o null si no está en el conjunto
     * @param nombre
     * @return 
     */
    public TransforBase getUnaTransB(String nombre){
        TransforBase transB = null;
        if(!transforsB.isEmpty()){
			for(TransforBase trB: transforsB){
				if( trB.getNombre().equalsIgnoreCase(nombre) ){
					transB = trB;
				}
			}
		}
       	return transB;
    }
    
    public void iniciaIterador(){
        iterador = 0;        
    }
    
    public TransforBase siguienteRB(){ 
        TransforBase tb;
        if(iterador>=transforsB.size()){
            tb = null;
        }else{
            tb = transforsB.get(iterador);        
        }
        iterador ++;
        return tb;
    }    

    public void ordenaConjunto(){
        Collections.sort(transforsB);
    }

    @Override
    public String toString(){

        String texto = "====================================================" + "\n";
		texto += "COMIENZA CONJUNTO DE TRANSFORMACIONES BASE " + "\n";
        texto += "====================================================" + "\n";

        int i;
        for(i = 0; i < transforsB.size(); i++){
            texto += transforsB.get(i).toString() + "\n\n";
        }
        return texto;
    }

	public String toStringCorto(){
		String texto = "====================================================" + "\n";
		texto += "COMIENZA CONJUNTO DE TRANSFORMACIONES BASE " + "\n";
        texto += "====================================================" + "\n";

        int i;
        for(i = 0; i < transforsB.size(); i++){
            texto += transforsB.get(i).toStringCorto() + "\n";
        }
        return texto;

	}

//    public static void main(String[] args){
//        ConjTransB t1TRB = new ConjTransB();
//        String dirArchivo = "D:/Java/PruebaJava/V4-TransformacionesBase.txt";
//        CargaTransBase cargador = new CargaTransBase() {};
//                try{
//         cargador.cargarConjTransBase(dirArchivo, t1TRB);
//        }catch(XcargaDatos ex){
//            System.out.println("------ERROR-------");
//            System.out.println(ex.getDescripcion());
//            System.out.println("------------------\n");}
//        }catch(FileNotFoundException ex){
//            System.out.println("------ERROR-------");
//            System.out.println("No se encontró el archivo: " + dirArchivo);
//            System.out.println("------------------\n");
//        }catch(IOException ex){
//            System.out.println("------ERROR-------");
//            System.out.println("Al leer el archivo: " + dirArchivo +
//                    ", se produjo la excepción: " + ex.toString() );
//            System.out.println("------------------\n");
//        }
//        System.out.println( t1TRB.toString() + "\n" );
//    }

//	public static void main(String[] args){
//        ConjRecB c1RB = new ConjRecB();
//		ConjTransB c1TB = new ConjTransB();
//		DatosGeneralesEstudio datosGen = new DatosGeneralesEstudio();
//		String texto = "";
//		String dirArchivoDGE = "D:/Java/PruebaJava2/V3-DatosGeneralesEstudio.txt";
//        String dirArchivoRB = "D:/Java/PruebaJava2/V9-RecursosBase.txt";
//        String dirArchivoTB = "D:/Java/PruebaJava2/V5-TransformacionesBase.txt";
//        CargaDatosGenerales cargadorDGE = new CargaDatosGenerales() {};
//		CargaRecBase cargadorRB = new CargaRecBase() {};
//		CargaTransBase cargadorTB = new CargaTransBase() {};
//
//        try{
//			cargadorDGE.cargarDatosGen(dirArchivoDGE, datosGen);
//
//            cargadorRB.cargarConjRecBase(dirArchivoRB, c1RB, datosGen);
//			texto = c1RB.toString();
//			System.out.println(texto + "\n" );
//
//			cargadorTB.cargarConjTransBase(dirArchivoTB, c1TB, c1RB);
//			texto = c1TB.toStringCorto();
//			System.out.print(texto);
//
//        }catch(XcargaDatos ex){
//            System.out.println("------ERROR-------");
//            System.out.println(ex.getDescripcion());
//            System.out.println("------------------\n");
//		}
//
//
//    }

}

