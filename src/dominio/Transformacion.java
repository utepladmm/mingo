/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

//import dominio.Bloque.EstadosRec;
import java.io.Serializable;
import java.util.ArrayList;

import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class Transformacion extends RecOTrans implements Serializable {

     private TransforBase transBase;
     private String estado;
     /**
      *  El estado de una transformacion puede ser CON o DEM
      * UNA TRANSFORMACIÓN UNA VEZ COMPLETADA O CON RESULTADO FALLIDA QUEDA EN EL PARQUE
      * AL SOLO EFECTO DEL CALCULO DE LOS COSTOS DE ANUALIDAD DE INVERSION
      *
      * LAS INVERSIONES DE LA TRANSFORMACION TERMINADA O FALLIDA QUEDAN EN LOS PARQUES PARA
      * CONSERVAR EL COSTO DE INVERSION. LA INVERSION DE LOS RECURSOS DESTINO DEBE TENER
      * EN CUENTA ESTO PARA NO DUPLICAR INVERSIONES.
      * UN CRITERIO POSIBLE ES QUE LOS RECURSOS DESTINO CONSERVEN LA ANUALIDAD DE LOS
      * RECURSOS ORIGEN.
      *
      * LAS TRANSFORMACIONES TERMINADAS O FALLIDAS NO APORTAN COSTOS VARIABLES
      * QUE DEBEN PROCEDER DE LOS RECURSOS DESTINO.
      */
     private int indicadorTiempo;
     private ArrayList<Integer> edadRecursosDestino;
     private Double valorInvAl;
     /**
      *	Si el recurso base admite aleatoriedad de la inversión el factor multiplicativo
      *   de la inversión de este recurso que resultó, de entre los posibles
      */
     private int valorDemAl;

     /**
      *	Si el recurso base admite demora aleatoria en la construcción
      *   valor de la demora de este recurso que resultó, de entre los posibles
      */
     public Transformacion(TransforBase transBase, String estado, int indicadorTiempo) {
          this.transBase = transBase;
          this.estado = estado;
          this.indicadorTiempo = indicadorTiempo;
     }

     public Transformacion() {
     }

     public String getEstado() {
          return estado;
     }

     public void setEstado(String estado) {
          this.estado = estado;
     }

     public int getIndicadorTiempo() {
          return indicadorTiempo;
     }

     public void setIndicadorTiempo(int indicadorTiempo) {
          this.indicadorTiempo = indicadorTiempo;
     }

     public TransforBase getTransBase() {
          return transBase;
     }

     public void setTransBase(TransforBase transBase) {
          this.transBase = transBase;
     }

     public String nombreObjeto() {
          return transBase.getNombre();
     }

    @Override
     public int getValorDemAl() {
          return valorDemAl;
     }

     public void setValorDemAl(int valorDemAl) {
          this.valorDemAl = valorDemAl;
     }

     @Override
     public double getValorInvAl() {
          return valorInvAl;
     }
     
     
    @Override
     public String getClase(){
         return transBase.getClase();
     }

     public void setValorInvAl(Double valorInvAl) {
          this.valorInvAl = valorInvAl;
     }

     public void setEstadoString(String str) throws XcargaDatos {
          if (str.equalsIgnoreCase("CON")) {
               estado = "CON";
          } else if (str.equalsIgnoreCase("DEM")) {
               estado = "DEM";
          } else if (str.equalsIgnoreCase("FALL")) {
               estado = "FALL";
          } else {
               throw new XcargaDatos("Error en estado de recurso: " + str);
          }
     }

     /**
      * Crea una copia de la Transformacion corriente (this) como un nuevo objeto
      * que puede cambiarse sin alterar el original
      * @return copia
      */
     public Transformacion copiaTrans() {
          Transformacion copia = new Transformacion(transBase, estado, indicadorTiempo);
          copia.setValorDemAl(valorDemAl);
          copia.setValorInvAl(valorInvAl);
          return copia;
     }

     @Override
     public boolean equals(Object obj) {
          if (obj == null) {
               return false;
          }
          if (getClass() != obj.getClass()) {
               return false;
          }
          final Transformacion other = (Transformacion) obj;
          if (this.transBase != other.transBase && (this.transBase == null || !this.transBase.equals(other.transBase))) {
               return false;
          }
          if (this.estado != other.estado) {
               return false;
          }
          if (this.indicadorTiempo != other.indicadorTiempo) {
               return false;
          }
          if (this.edadRecursosDestino != other.edadRecursosDestino && (this.edadRecursosDestino == null || !this.edadRecursosDestino.equals(other.edadRecursosDestino))) {
               return false;
          }
          if (this.valorInvAl != other.valorInvAl && (this.valorInvAl == null || !this.valorInvAl.equals(other.valorInvAl))) {
               return false;
          }
          if (this.valorDemAl != other.valorDemAl) {
               return false;
          }
          return true;
     }

     @Override
     public int hashCode() {
          int hash = 7;
          hash = 97 * hash + (this.transBase != null ? this.transBase.hashCode() : 0);
          hash = 97 * hash + (this.estado != null ? this.estado.hashCode() : 0);
//          hash = 97 * hash + (this.estado != null ? EstadosRec.hashSustituto(this.estado) : 0);
          hash = 97 * hash + this.indicadorTiempo;
          hash = 97 * hash + (this.edadRecursosDestino != null ? this.edadRecursosDestino.hashCode() : 0);
          hash = 97 * hash + (this.valorInvAl != null ? this.valorInvAl.hashCode() : 0);
          hash = 97 * hash + this.valorDemAl;
          return hash;
     }

     @Override
     public String toString() {
          String texto = "TRANSFORMACION";
          texto += transBase.toString() + "\n";
          texto += "estado: " + estado.toString() + "\n";
          texto += "indicadorTiempo: " + indicadorTiempo + "\n";
          texto += "\n";
          return texto;
     }

     @Override
     public String toStringCorto() {
          String texto = "Transformacion: ";
          texto += transBase.getNombre().toString() + "--";
          texto += "estado: " + estado.toString() + "--";
          texto += "indicadorTiempo: " + indicadorTiempo;
          return texto;
     }

     @Override
     public String toStringMuyCorto(boolean usaEquiv) {
          // usaEquiv se pone por compatibilidad con el método de Recurso pero no se usa
          String texto = "";
          texto += transBase.getNombre() + "/";
          texto += estado.toString() + "/";
          texto += "indt" + indicadorTiempo + "/";
          return texto;
     }
//    public static void main(String[] args){
//        TransforBase trb1 = new TransforBase();
//        TransforBase trb2 = new TransforBase();
//        trb1.setNombre("trb1");
//        trb2.setNombre("trb2");
//        Transformacion tra = new Transformacion();
//        Transformacion trb = new Transformacion();
//        tra.setTransBase(trb1);
//        tra.setEstado(EstadosRec.CON);
//        tra.setIndicadorTiempo(1);
//        trb.setTransBase(trb1);
//        trb.setEstado(EstadosRec.DEM);
//        trb.setIndicadorTiempo(1);
//        trb.toString();
//
//        System.out.println(tra.toString());
//        System.out.println(trb.toString());
//        if (tra.equals(trb)) System.out.println("tra equals trb es true");
//
//    }
}
