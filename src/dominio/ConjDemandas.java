/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import AlmacenSimulaciones.ImplementadorGeneral;
import ImplementaEDF.ImplementadorEDF;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.ConjNodosN;
import naturaleza3.GrafoN;
import naturaleza3.MatrizTransicion;
import naturaleza3.NodoN;
import naturaleza3.VariableNat;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class ConjDemandas implements Serializable {

    private Estudio estudio;
    private ArrayList<Demanda> demandas;

    public ConjDemandas(Estudio estudio, ArrayList<Demanda> demandas) {
        this.estudio = estudio;
        demandas = new ArrayList<Demanda>();
    }

    public ConjDemandas(Estudio estudio) {
        this.estudio = estudio;
        demandas = new ArrayList<Demanda>();
    }

    public ArrayList<Demanda> getDemandas() {
        return demandas;
    }

    public void setDemandas(ArrayList<Demanda> demandas) {
        this.demandas = demandas;
    }

    /**
     * METODO prodMinDem
     *
     * Calcula la cantidad de un producto requerida por el conjunto de las demandas
     * menor posible entre los futuros posibles, dado que en se está en el
     * tiempo absoluto ta y se considera el futuro tcons pasos de tiempo hacia adelante.
     * SE EMPLEA EL grafoN DE PASOS COMPLETOS ANTES DE PASAR A ETAPAS. La cantidad puede
     * ser manipulada sin afectar otros datos.
     *
     * @param est es el estudio
     * @param pr es el producto
     * @param ta tiempoAbsoluto inicial
     * @param CI conjunto de información en el que se está en ta
     * @param tcons plazo expresado en tal que se toma la demanda en ta+tcons
     * @return cant es la cantidad de producto minima posible requerida por el
     * conjunto de las demandas dado que en ta se está enel conjunto de información CI.
     *
     */
    public CantProductoSimple prodMinDem(Producto pr, TiempoAbsoluto ta,
            ConjNodosN CI, int tcons) throws XcargaDatos {

        DatosGeneralesEstudio datGen = estudio.getDatosGenerales();
        GrafoN grafoNP = estudio.getGrafoNPasos();
        CantProductoSimple cant = new CantProductoSimple(pr, 0.0);
        int pasoIni = datGen.pasoDeTiempoAbsoluto(ta);
        int pasoFin = pasoIni + tcons;
        TiempoAbsoluto taMasTcons = datGen.tiempoAbsolutoDePaso(pasoFin);

        /**
         * ATENCION CON ESTO, SI EL PASO FINAL SE SALE DEL ESTUDIO SE TOMA CANTPASOS
         */
        boolean hayAleat = false;
        for (Demanda d : demandas) {
            if (d.isAleatoria()) {
                hayAleat = true;
            }
        }

        if (hayAleat) {
            // alguna demanda es aleatoria, hay que recorrer todos los futuros posibles
            if (pasoFin > datGen.getCantPasos()) {
                pasoFin = datGen.getCantPasos();
            }
            int cantFilas = grafoNP.getNodos().get(pasoIni).size();
            int cantColumnas = grafoNP.getNodos().get(pasoFin).size();
            MatrizTransicion matTran = new MatrizTransicion(grafoNP,
                    pasoIni, pasoFin, cantFilas, cantColumnas);
            matTran.calculaMatrizEtapas(pasoIni, pasoFin);
            /**
             * SE SUPONE QUE LA VARIABLE DE LA NATURALEZA QUE DETERMINA
             * LA DEMANDA ES SIEMPRE OBSERVABLE
             * Por lo tanto alcanza con considerar un sólo nodo representativo del CI para
             * ver los posibles valores de demanda futuros
             *
             * Los nodos sucesores de ese representativo al cabo de tcons pasos
             * se almacenan en listaNSuc
             */
            NodoN nrep = CI.getNodos().get(0);
            int ordinalFila = nrep.getOrdinal();
            ArrayList<NodoN> listaNSuc = new ArrayList<NodoN>();
            CantProductoSimple cPMin = new CantProductoSimple(pr, 99999999.9);
            for (int jcol = 1; jcol < cantColumnas; jcol++) {
                CantProductoSimple cPDem = new CantProductoSimple(pr, 0.0);
                if (matTran.getMatSuc()[ordinalFila][jcol]) {
                    NodoN nsuc = grafoNP.getNodos().get(pasoFin).get(jcol);
                    for (Demanda d : demandas) {
                        String valorVA = nsuc.valorDeUnaVN(d.getVAleatoria());
                        cPDem.sumaCant(d.cantProd(pr, datGen, taMasTcons, valorVA));
                    }
                    if (cPDem.comparaCant(cPMin) < 0) {
                        cPMin = cPDem;
                    }
                }
            }
            cant = cPMin;
        } else {
            /**
             * Ninguna demanda es aleatoria se toma la demanda determinística sin más
             */
            String valorVA;
            CantProductoSimple cPDem = new CantProductoSimple(pr, 0.0);
            for (Demanda d : demandas) {
                valorVA = d.getValoresVA().get(0);
                cPDem.sumaCant(d.cantProd(pr, datGen, taMasTcons, valorVA));
            }
            cant = cPDem;
        }
        return cant;
    }

    /**
     * Devuelve la CantProductoSimple requerida de cada uno de los Productos del Estudio
     * en el NodoE nE, en el orden en que aparecen en el estudio.
     * @param nE es el nodo del que se quiere saber la demanda de productos.
     * @return cantPDem es la lista de cantidades de producto de la demanda en nE, con los productos
     * en el orden en que aparecen en el Estudio
     */
    public ArrayList<CantProductoSimple> cantProdsDemanda(NodoN nN, int etapa, Estudio est) throws XcargaDatos {

        ArrayList<CantProductoSimple> cantPDem = new ArrayList<CantProductoSimple>();
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int paso = datGen.pasoRepDeEtapa(etapa);
        TiempoAbsoluto ta = datGen.tiempoAbsolutoDePaso(paso);
        ArrayList<Producto> productos = est.getDatosGenerales().getProductos();
        for (Producto pr : productos) {
            CantProductoSimple cant = new CantProductoSimple(pr);
            cant.setCant(0.0);
            for (Demanda dem : demandas) {
                String valorVA;
                if (dem.isAleatoria()) {
                    VariableNat vDem = dem.getVAleatoria();
                    valorVA = nN.valorDeUnaVN(vDem);
                } else {
                    valorVA = dem.getValoresVA().get(0);
                }
                CantProductoSimple cant1D = new CantProductoSimple(pr);
                cant1D = dem.cantProd(pr, datGen, ta, valorVA);
                cant.sumaCant(cant1D);
            }
            cantPDem.add(cant);
        }
        return cantPDem;
    }

    
    /**
     * Devuelve la cantidad de un Producto pr de todas las demandas en el conjunto de demandas
     * @param pr es el Producto
     * @param nN es el NodoN de la naturaleza
     * @param etapa es la etapa del Estudio.
     * @param est es el Estudio.
     */
    public CantProductoSimple cantDeUnProd(Producto pr, NodoN nN, int etapa, Estudio est) throws XcargaDatos{
        CantProductoSimple cant ;        
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int paso = datGen.pasoRepDeEtapa(etapa);
        TiempoAbsoluto ta = datGen.tiempoAbsolutoDePaso(paso);        
        cant = new CantProductoSimple(pr);
        cant.setCant(0.0);
        for (Demanda dem : demandas) {
            String valorVA;
            if (dem.isAleatoria()) {
                if (nN==null){
                    System.out.println("el nodo nN es NULL");
                    valorVA = dem.getValoresVA().get(0);
                } else {
                    if (nN.getOrdinal()==0){
                    System.out.println("el nodo nN tiene ordinal=0");
                    valorVA = dem.getValoresVA().get(0);
                    } else {       
                        VariableNat vDem = dem.getVAleatoria();
                        valorVA = nN.valorDeUnaVN(vDem);  // el nodo nN es null porque se creo así!!!! no se puede usar
                    }
                }
                              
            } else {
                valorVA = dem.getValoresVA().get(0);
            }
            CantProductoSimple cant1D = new CantProductoSimple(pr);
            cant1D = dem.cantProd(pr, datGen, ta, valorVA);
            cant.sumaCant(cant1D);
        }
        return cant;
    }

    @Override
    public String toString() {
        String texto = "====================================================" + "\n";
        texto += "COMIENZA CONJUNTO DE DEMANDAS " + "\n";
        texto += "====================================================" + "\n";

        for (int i = 0; i < demandas.size(); i++) {
            texto += demandas.get(i).toString() + "\n";
        }
        return texto;

    }

    public static void main(String[] args) throws IOException, XcargaDatos {

        String dirBase = "D:/Java/PruebaJava4";
        String dirEstudio = dirBase + "/" + "Datos_Estudio";
        String dirImplementacion = dirBase + "/" + "Datos_Implementacion";
        String dirCorridas = dirBase + "/" + "corridas";
        ImplementadorGeneral impG = new ImplementadorEDF();
        impG.inicializaImplementador(dirImplementacion, dirEstudio, dirCorridas);
        Estudio est = new Estudio("Estudio de prueba", 4, impG);
        String directorio = "D:/Java/PruebaJava3";
        est.leerEstudio(directorio, true);
        String texto = est.toString();
        System.out.print(texto);

        Producto pr = est.getDatosGenerales().getProductos().get(0);
        TiempoAbsoluto ta = new TiempoAbsoluto("2010");
        int tcons = 2;
        ConjNodosN CI = est.getGrafoN().getBolsaCI().getConjuntosNodosN().get(1).get(0);
        CantProductoSimple cPS = est.getConjDemandas().prodMinDem(pr, ta, CI, tcons);
        System.out.print(cPS.toString());

    }
}
