/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import Restricciones3.RestDeProducto3;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class Producto implements Serializable {

    /**
     * Cada producto da lugar a un balance entre recursos que generan 
     * esos productos y demanda y recursos que consumen esos productos.
     * El balance tiene que dar lugar a un excedente para que un parque sea
     * factible
     * Ejemplo: potencia máxima, potencia firme, pueden ser dos productos.
     */
    private String nombre;
    private String descripcion;
    private String tipo;
    private ArrayList<RestDeProducto3> restricciones;
    // las restricciones de producto asociadas a este producto en el estudio
    /**
     * Atributos del producto relevantes para los RecursoBase que dependen del tiempo
     */
    private int diaIni;  // primer día del año del período en que se hace el balance del producto (1 a 366)
    private int diaFin;  // último día del año (1 a 366)
    private String formaCalculo; // puede ser MIN o PROM
    public static final String MIN = "MIN";    // El balance se hace con el mínimo del producto en el período
    public static final String PROM = "PROM";  // El balance se hace con el promedio del producto en el período      

    public Producto() {
        nombre = "";
        descripcion = "";
    }

    public Producto(String nombre) {
        this.nombre = nombre;
    }

    public Producto(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public ArrayList<RestDeProducto3> getRestricciones() {
        return restricciones;
    }

    public void setRestricciones(ArrayList<RestDeProducto3> restricciones) {
        this.restricciones = restricciones;
    }

    public int getDiaFin() {
        return diaFin;
    }

    public void setDiaFin(int diaFin) {
        this.diaFin = diaFin;
    }

    public int getDiaIni() {
        return diaIni;
    }

    public void setDiaIni(int diaIni) {
        this.diaIni = diaIni;
    }
    
    

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Producto other = (Producto) obj;
        if ((this.nombre == null) ? (other.nombre != null) : !this.nombre.equals(other.nombre)) {
            return false;
        }
        if ((this.descripcion == null) ? (other.descripcion != null) : !this.descripcion.equals(other.descripcion)) {
            return false;
        }
        if ((this.tipo == null) ? (other.tipo != null) : !this.tipo.equals(other.tipo)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.nombre != null ? this.nombre.hashCode() : 0);
        hash = 47 * hash + (this.descripcion != null ? this.descripcion.hashCode() : 0);
        hash = 47 * hash + (this.tipo != null ? this.tipo.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        String texto = "";
        texto += "Nombre del producto: " + nombre + "\n";
        texto += "Descripción del producto: " + descripcion + "\n";
        texto += "Tipo del producto: " + tipo + "\n";
        return texto;
    }
}
