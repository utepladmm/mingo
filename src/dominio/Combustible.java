/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;

/**
 *
 * @author ut469262
 */
public class Combustible extends Fuente implements Serializable {

    /**
     * Carga en el combustible this los datos de la fuente fuen
     */
    public void cargaFuente(Fuente fuen){
         tipo = fuen.getTipo();
         nombre = fuen.getNombre();
         requiereInyector = fuen.isRequiereInyector();
         disponibilidad = fuen.getDisponibilidad();
         potMax = fuen.getPotMax();
         energMaxPaso = fuen.getEnergMaxPaso();
         iniDisp = fuen.getIniDisp();
         finDisp = fuen.getFinDisp();
         tiempoAbsolutoBase = fuen.getTiempoAbsolutoBase();
         potMaxT = fuen.getPotMaxT();
         energMaxPasoT = fuen.getEnergMaxPasoT();
    }

}
