/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;

/**
 *
 * @author ut469262
 */
public class GeneradorHidro extends Generador implements Serializable{
	private String directorioDatos;

	public GeneradorHidro(String directorioDatos) {
		this.directorioDatos = directorioDatos;
	}

	public GeneradorHidro() {
	}

	public String getDirectorioDatos() {
		return directorioDatos;
	}

	public void setDirectorioDatos(String directorioDatos) {
		this.directorioDatos = directorioDatos;
	}

	@Override
	public String toString() {

		String texto ="";
		texto += super.toString() + "\n";
		texto += "Directorio de datos unitarios " + directorioDatos + "\n";

        return texto;



	}

	



}
