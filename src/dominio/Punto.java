/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;

/**
 *
 * @author ut600232
 */
public class Punto implements Serializable {

     private String nombre;
     private Double x;
     private Double y;
     private Double z;

     public Punto(String nombre) {
          nombre = "";
     }

     public Punto() {
          nombre = "";
          x = 0D;
          y = 0D;
          z = 0D;
     }

     public Punto(String nombre, Double x, Double y, Double z) {
          this.nombre = nombre;
          this.x = x;
          this.y = y;
          this.z = z;
     }

     public String getNombre() {
          return nombre;
     }

     public void setNombre(String nombre) {
          this.nombre = nombre;
     }

     public void setX(Double x) {
          this.x = x;
     }

     public void setY(Double y) {
          this.y = y;
     }

     public void setZ(Double z) {
          this.z = z;
     }

     public Double getX() {
          return x;
     }

     public Double getY() {
          return y;
     }

     public Double getZ() {
          return z;
     }

     @Override
     public boolean equals(Object obj) {
          if (obj == null) {
               return false;
          }
          if (getClass() != obj.getClass()) {
               return false;
          }
          final Punto other = (Punto) obj;
          if ((this.nombre == null) ? (other.nombre != null) : !this.nombre.equals(other.nombre)) {
               return false;
          }
          if (this.x != other.x && (this.x == null || !this.x.equals(other.x))) {
               return false;
          }
          if (this.y != other.y && (this.y == null || !this.y.equals(other.y))) {
               return false;
          }
          if (this.z != other.z && (this.z == null || !this.z.equals(other.z))) {
               return false;
          }
          return true;
     }

     @Override
     public int hashCode() {
          int hash = 7;
          hash = 83 * hash + (this.nombre != null ? this.nombre.hashCode() : 0);
          hash = 83 * hash + (this.x != null ? this.x.hashCode() : 0);
          hash = 83 * hash + (this.y != null ? this.y.hashCode() : 0);
          hash = 83 * hash + (this.z != null ? this.z.hashCode() : 0);
          return hash;
     }

     

     @Override
     public String toString() {
          String texto = "";
          texto += "(" + x + ", " + y + ", " + z + ")";
          return texto;
     }
}
