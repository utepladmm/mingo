/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class ImportFormul extends Importacion implements Serializable{
	private String pais;
	private int ntram;
	private ArrayList<Double> limTram;
	private ArrayList<Double> preTram;
	private ArrayList<Double> facTram;
	private ArrayList<ArrayList<Double>> potTram;
	private ArrayList<Double> dispTram;

	public ImportFormul(String pais, int ntram, ArrayList<Double> limTram, ArrayList<Double> preTram, ArrayList<ArrayList<Double>> potTram, ArrayList<Double> dispTram) {
		this.pais = pais;
		this.ntram = ntram;
		this.limTram = limTram;
		this.preTram = preTram;
		this.potTram = potTram;
		/**
		 * poTram tiene postes en el primer indice (fila) y tramo en el segundo
		 * índice (columna)
		 */
		this.dispTram = dispTram;
	}

	public ImportFormul() {
        limTram = new ArrayList<Double>();
        preTram = new ArrayList<Double>();
        facTram = new ArrayList<Double>();
        potTram = new ArrayList<ArrayList<Double>>() ;
        dispTram = new ArrayList<Double>() ;        
	}


	public ArrayList<Double> getDispTram() {
		return dispTram;
	}

	public void setDispTram(ArrayList<Double> dispTram) {
		this.dispTram = dispTram;
	}

	public ArrayList<Double> getLimTram() {
		return limTram;
	}

	public void setLimTram(ArrayList<Double> limTram) {
		this.limTram = limTram;
	}

	public int getNtram() {
		return ntram;
	}

	public void setNtram(int ntram) {
		this.ntram = ntram;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public ArrayList<Double> getFacTram() {
		return facTram;
	}

	public void setFacTram(ArrayList<Double> facTram) {
		this.facTram = facTram;
	}



	public ArrayList<ArrayList<Double>> getPotTram() {
		return potTram;
	}

	public void setPotTram(ArrayList<ArrayList<Double>> potTram) {
		this.potTram = potTram;
	}

	public ArrayList<Double> getPreTram() {
		return preTram;
	}

	public void setPreTram(ArrayList<Double> preTram) {
		this.preTram = preTram;
	}

	@Override
	public String toString() {
		String texto ="";
		texto += super.toString() + "\n";
		texto += "pais: " + pais + "\n";
		texto += "límites superiores de tramos de precio del país de origen" + "\n";
		for (int i=0; i<limTram.size(); i++){
			texto += limTram.get(i).toString() + "  ";
		}
		texto += "\n";
		texto += "precio fijo por tramo" + "\n";
		for (int i=0; i<preTram.size(); i++){
			texto += preTram.get(i).toString() + "  ";
		}
		texto += "\n";
		texto += "factor proporcional al precio spot del país de origen por tramo" + "\n";
		for (int i=0; i<facTram.size(); i++){
			texto += facTram.get(i).toString() + "  ";
		}
		texto += "\n";
		texto += "potencia por poste (fila) y tramo (columna)" + "\n";
		for (int i=0; i<potTram.size(); i++){
			for (int j=0; j<potTram.get(i).size(); j++){
				texto += potTram.get(i).get(j).toString() + "  ";
			}
			texto += "\n";
		}

        return texto;

	}

}




