/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;

/**
 *
 * @author ut469262
 */
public class Falla implements Serializable{
	private String demanda;   // nombre de la demanda a la que se asocia esa falla
	private String tipoFalla; // tipo de la falla: por ahora sólo falla_porcentual

	public Falla(String demanda, String tipoFalla) {
		this.demanda = demanda;
		this.tipoFalla = tipoFalla;
	}

	public Falla() {
	}

	

	public String getDemanda() {
		return demanda;
	}

	public void setDemanda(String demanda) {
		this.demanda = demanda;
	}

	public String getTipoFalla() {
		return tipoFalla;
	}

	public void setTipoFalla(String tipoFalla) {
		this.tipoFalla = tipoFalla;
	}

	public void cargarFalla(Falla fal){
	// carga en la falla los datos otra falla fal
		this.demanda = fal.getDemanda();
		this.tipoFalla = fal.getTipoFalla();

	}

	@Override
	public String toString() {
		String texto = "";
        texto += "Nombre de la demanda a la que se asocia: " + demanda + "\n";
        texto += "Tipo de falla: " + tipoFalla;
		return texto;
	}





}
