/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

/**
 *
 * @author D230513
 */
public class ResSimulDB {
    private double[][] matEner;
    private double[][] matCost;    
    /**
     * Son los resultados detallados por central y la demanda 
     * (en la base de datos "fuentes",y por crónica
     * 
     * matEner[i-crónica][i-fuente]
     */
    private String[] nombFuente;
    private String[] tipo;
    private String[] subtipo1;
    private String[] subtipo2;
    private int[] cronicas;   // Ejemplo 1909.
    private int anio;
    
    public ResSimulDB(int numCron, int numFuentes){
        matEner = new double[numCron][numFuentes];
        nombFuente = new String[numFuentes];
        cronicas = new int[numCron];    
    }

    public int[] getCronicas() {
        return cronicas;
    }

    public void setCronicas(int[] cronica) {
        this.cronicas = cronica;
    }

    public double[][] getMatCost() {
        return matCost;
    }

    public void setMatCost(double[][] matCost) {
        this.matCost = matCost;
    }

    public double[][] getMatEner() {
        return matEner;
    }

    public void setMatEner(double[][] matEner) {
        this.matEner = matEner;
    }

    public String[] getNombFuente() {
        return nombFuente;
    }

    public void setNombFuente(String[] nombFuente) {
        this.nombFuente = nombFuente;
    }

    public String[] getSubtipo1() {
        return subtipo1;
    }

    public void setSubtipo1(String[] subtipo1) {
        this.subtipo1 = subtipo1;
    }

    public String[] getSubtipo2() {
        return subtipo2;
    }

    public void setSubtipo2(String[] subtipo2) {
        this.subtipo2 = subtipo2;
    }

    public String[] getTipo() {
        return tipo;
    }

    public void setTipo(String[] tipo) {
        this.tipo = tipo;
    }
    
    
    
    public int cantDeCronicas(){
        return cronicas.length;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }
        
    
}
