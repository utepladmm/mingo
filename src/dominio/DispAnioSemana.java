/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Almacena un registro de disponibilidad tomando años
 * cronológicos (ejemplo 2012) y semanas del año
 * @author ut469262
 */
public class DispAnioSemana implements Serializable{

	private int aninidisp;
	private int anfindisp;
	private ArrayList<Integer> seminidisp;
	private ArrayList<Integer> semfindisp;

	public DispAnioSemana() {
		aninidisp = 0;
		anfindisp = 0;
		seminidisp = new ArrayList<Integer>();
		semfindisp = new ArrayList<Integer>();
	}

	public int getAnfindisp() {
		return anfindisp;
	}

	public void setAnfindisp(int anfindisp) {
		this.anfindisp = anfindisp;
	}

	public int getAninidisp() {
		return aninidisp;
	}

	public void setAninidisp(int aninidisp) {
		this.aninidisp = aninidisp;
	}

	public ArrayList<Integer> getSemfindisp() {
		return semfindisp;
	}

	public void setSemfindisp(ArrayList<Integer> semfindisp) {
		this.semfindisp = semfindisp;
	}

	public ArrayList<Integer> getSeminidisp() {
		return seminidisp;
	}

	public void setSeminidisp(ArrayList<Integer> seminidisp) {
		this.seminidisp = seminidisp;
	}


	@Override
	public String toString() {
		String texto = "\n";
		texto += "aninidisp = " + aninidisp + "  ";
		texto += "anfindisp = " + anfindisp + "\n";
		texto += "semanas de inicio: ";
		for (int i =0; i<seminidisp.size(); i++){
			texto += seminidisp.get(i) + "  ";
		}
		texto += "\n";
		texto += "semanas de fin:    ";
		for (int i =0; i<semfindisp.size(); i++){
			texto += semfindisp.get(i) + "  ";
		}
		texto += "\n";
		return texto;
	}

}
