/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;


import AlmacenSimulaciones.ImplementadorGeneral;
import ImplementaEDF.ImplementadorEDF;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.CargaGrafoN2;
import naturaleza3.GrafoN;
import persistenciaMingo.CargaCambiosAleatorios;
import persistenciaMingo.CargaDatosGenerales;
import persistenciaMingo.CargaRecBase;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class ConjCambiosAleat implements Serializable{

	private ArrayList<CambioAleatorio> cambios;

	public ConjCambiosAleat() {
		cambios = new ArrayList<CambioAleatorio>();
	}

	public ArrayList<CambioAleatorio> getCambios() {
		return cambios;
	}

	public void setCambios(ArrayList<CambioAleatorio> cambios) {
		this.cambios = cambios;
	}


    /**
     * Agrega el cambio aleatorio cambioA al conjunto de
     * cambios aleatorios (this).
     * @param cambioA
     */
    public void agregar(CambioAleatorio cambioA){
        cambios.add(cambioA);
    }

    /**
     * Elimina el cambio aleatorio de nombre nomCambioA del conjunto de
     * cambios aleatorios (this). Atención remueve el idéntico por == y no
     * usa equals.
     * @param nomCambioA
     */
    public boolean remover(String nomCambioA){
        boolean result = false;
        CambioAleatorio aEliminar = null;
        for(CambioAleatorio cA: cambios){
            if( nomCambioA.compareToIgnoreCase(cA.getNombre()) == 0 ){
                aEliminar = cA;
                result = true;
            }
        }
        cambios.remove(aEliminar);
        return result;
    }

    /**
     * Elimina el cambio aleatorio de nombre nomCambioA del conjunto de
     * cambios aleatorios (this).
     *
     * @param nomCambioA
     */
    public boolean remover(CambioAleatorio cambioA){
        boolean result = false;
        CambioAleatorio aEliminar = null;
        for(CambioAleatorio cA: cambios){
            if( cA == cambioA ){
                aEliminar = cA;
                result = true;
            }
        }
        cambios.remove(aEliminar);
        return result;
    }



    /**
     * Devuelve el cambioAleatorio de nombre nombre contenida en el
     * conjunto o null si no está en el conjunto
     *
     * @param nombre es el String nombre del cambio aleatorio a devolver
     */
	 public CambioAleatorio getUnCambioA(String nombre){

        CambioAleatorio cambioA = null;
		if(!cambios.isEmpty()){
			for(CambioAleatorio cA: cambios){
				if( cA.getNombre().equalsIgnoreCase(nombre) ){
					cambioA = cA;
				}
			}
		}
       	return cambioA;
    }


	@Override
	public String toString() {
        String texto = "====================================================" + "\n";
		texto += "COMIENZA CONJUNTO DE CAMBIOS ALEATORIOS " + "\n";
        texto += "====================================================" + "\n";

		for (int i=0; i< cambios.size(); i++){
			texto += cambios.get(i).toString() + "\n";
		}				
		return texto;

	}





    public static void main(String[] args) throws IOException, XcargaDatos{


        String dirBase = "D:/Java/PruebaJava4";
        String dirEstudio = dirBase + "/" + "Datos_Estudio";
        String dirImplementacion = dirBase + "/" + "Datos_Implementacion";
        String dirCorridas = dirBase + "/" + "corridas";
        ImplementadorGeneral impG = new ImplementadorEDF();
        impG.inicializaImplementador(dirImplementacion, dirEstudio, dirCorridas);
        String directorio = "D:/Java/PruebaJava3";
		String texto = "";
        Estudio est = new Estudio(directorio, 4, impG);
        est.leerEstudio(directorio, false);
        ConjCambiosAleat conjCAl = est.getConjCambAleat();
        System.out.print(conjCAl.toString());
        CambioAleatorio cambio1 = conjCAl.getUnCambioA("muereSalaB");
        conjCAl.remover(cambio1);
        System.out.print(conjCAl.toString());

    }



}




