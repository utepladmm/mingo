/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.VariableNat;
import persistenciaMingo.XcargaDatos;

/**
 * Da los valores de cantidades de producto, para un producto
 * y valor de una variable de la naturaleza
 * para todos los pasos de tiempo absolutos
 * del estudio, para un valor simbólico dado de una variable aleatoria
 * @author ut469262
 */
public class EscenarioCantProducto implements Serializable{

	private VariableNat varN;
	private String valorVN; // valor de la variable varN
	private Producto producto;
	private TiempoAbsoluto tiempoAbsolutoBase;
        /**
	 * El primer índice recorre productos y el segundo pasos de tiempo absoluto
	 */
	private ArrayList<CantProductoSimple> cantidades;
	
	public EscenarioCantProducto(VariableNat varN, String valorVN,
			Producto producto,TiempoAbsoluto tiempoAbsolutoBase) {
		
                this.varN = varN;
		this.valorVN = valorVN;
		this.producto = producto;
		this.tiempoAbsolutoBase = tiempoAbsolutoBase;
		cantidades = new ArrayList<CantProductoSimple>();
	}

	public ArrayList<CantProductoSimple> getCantidades() {
		return cantidades;
	}

	public void setCantidades(ArrayList<CantProductoSimple> cantidades) {
		this.cantidades = cantidades;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}


	public TiempoAbsoluto getTiempoAbsolutoBase() {
		return tiempoAbsolutoBase;
	}

	public void setTiempoAbsolutoBase(TiempoAbsoluto tiempoAbsolutoBase) {
		this.tiempoAbsolutoBase = tiempoAbsolutoBase;
	}

	public String getValorVN() {
		return valorVN;
	}

	public void setValorVN(String valorVN) {
		this.valorVN = valorVN;
	}

	public VariableNat getVarN() {
		return varN;
	}

	public void setVarN(VariableNat varN) {
		this.varN = varN;
	}


    /**
     * Devuelve la cantidad de producto para el tiempo absoluto ta
     * @param ta es el TiempoAbsoluto para el que se pide la cantidad de producto
     * @param datGen son los DatosGeneralesEstudio
     */
	public CantProductoSimple DevuelveCantProd(TiempoAbsoluto ta,
			DatosGeneralesEstudio datGen) throws XcargaDatos{


		CantProductoSimple cant = new CantProductoSimple(producto);
		int avance = datGen.getCalculadorDePlazos().
				hallaAvanceTiempo(tiempoAbsolutoBase, ta);
		if (avance<0 || avance>= cantidades.size()){
				throw new XcargaDatos("Error en EscenarioCantProducto - producto " +
				producto + " variable aleatoria " + varN.toString()+
				"Valor de la variable " + valorVN);
		}else cant = cantidades.get(avance);
		return cant;
	}

	@Override
	public String toString() {
		String texto = "COMIENZA escenarioCantProducto" + "\n";
		texto += "VariableNat " + varN.getNombre() + " - Valor de la variable " +
				valorVN + " - Producto " + producto.getNombre() + "\t" +
				tiempoAbsolutoBase.toString() + "\n";
		for (CantProductoSimple c: cantidades){
			texto += c.getCant().toString() + "\t";
		}

		return texto;
	}





}
