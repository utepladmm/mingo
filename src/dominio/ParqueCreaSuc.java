/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import TiposEnum.EstadosRec;
import TiposEnum.TipoBloque;
import UtilitariosGenerales.CalculadorDePlazos;
//import dominio.Bloque.EstadosRec;
import java.lang.Boolean;
import java.util.ArrayList;
import java.util.Collections;
import naturaleza3.NodoN;
import naturaleza3.VariableNat;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 *
 * Esta clase proporciona métodos que sirven para crear el sucesor de un Parque
 * y son invocados por el método sucesorDeUnPort de la clase Parque.
 *
 * Los métodos se deben aplican a pIni sucesivamente y en la secuencia:
 * los cambios aleatorios (método aplicaCambiosAleatorios);
 * las transformaciones, (método avanzaTiempoTransformaciones);
 * las modificaciones a los bloques de recursos, (método avanzaTiempoRecursos).
 *
 * Se van agregando a pFin los bloques resultantes de esos pasos.
 * Al final de todos esos pasos, en pFin está el parque sucesor.
 */
public class ParqueCreaSuc {

    /**
     * Agrega un Bloque b a un ArrayList<Bloque> bloquesAL considerando la posibilidad de que
     * ya exista un bloque con el mismo objeto en el AL, en cuyo caso
     * se aumenta el multiplicador del bloque existente (o se disminuye si el
     * multiplicador del bloque a agregar es negativo).
     * SE PRESUPONE QUE bloqueAL TIENE A LO SUMO UN BLOQUE CON CADA OBJETO
     *
     * @param b es el bloque a agregar
     * @param bloquesAL es al ArrayList<Bloque>
     */
    public static void agregaBloque(ArrayList<Bloque> bloquesAL, Bloque b) throws XcargaDatos, XBloqueMultNeg {

        for (Bloque bAL : bloquesAL) {
            if (b.igualObjeto(bAL)) {
                int multSuma = b.getMultiplicador() + bAL.getMultiplicador();
                if (multSuma < 0) {
                    throw new XBloqueMultNeg("Un multiplicador dio negativo al agregar el Bloque: "
                            + b.getRecurso().toStringMuyCorto(false));
                }
                bAL.setMultiplicador(multSuma);
                return;
            }
        }
        if (b.getMultiplicador() < 0) {
            throw new XBloqueMultNeg("Un multiplicador dio negativo al agregar el Bloque: "
                    + b.getRecurso().toStringMuyCorto(false));
        }
        bloquesAL.add(b);
        Collections.sort(bloquesAL);
    }

    /**
     * Crea un ArrayList<Bloque> de Bloques ordenados que agrega a los Bloques de bloquesIni
     * las expansiones forzadas del estudio y del caso entre el paso siguiente al taIni, y el tiempoAbsoluto
     * taFin inclusive. taFin debe ser el tiempoAbsoluto del paso representativo de la etapa e+1.
     *
     * @param pIni es el parque inicial
     * @param taIni es el tiempo absoluto inicial en el que vive bloquesIni
     * @param taFin es el último tiempo absoluto cuyas expansiones se incluyen.
     * @param comunes si es true, indica que se deben agregar los bloques con Recursos comunes
     * @param nocomunes si es true, indica que se deben agregar los bloques con Recursos que no son comunes
     * y los bloques de Transformaciones.
     * @param informacion
     * @return bloquesFin son los bloques resultantes.
     */
    public static ArrayList<Bloque> agregaExpsForzadas(ArrayList<Bloque> bloquesIni,
            TiempoAbsoluto taIni,
            TiempoAbsoluto taFin,
            boolean comunes,
            boolean nocomunes,
            ArrayList<Object> informacion) throws XcargaDatos, XBloqueMultNeg {

        ArrayList<Bloque> bloquesFin = new ArrayList<Bloque>();
        for (Bloque b : bloquesIni) {
            Bloque bCopia = b.copiaBloque();
            bloquesFin.add(bCopia);
        }
        Estudio est = (Estudio) informacion.get(0);
        CalculadorDePlazos calcP = est.getDatosGenerales().getCalculadorDePlazos();
        Caso caso = (Caso) informacion.get(1);
        Expansion expEst = est.getExpansionForzadaEstudio();
        Expansion expCaso = caso.getExpansionForzadaCaso();
        ArrayList<Expansion> expansiones = new ArrayList<Expansion>();
        expansiones.add(expEst);
        expansiones.add(expCaso);
        TiempoAbsoluto taCorr = taIni.copiaTiempoAbsoluto();
        do {
            taCorr = calcP.hallaTiempoAbsoluto(taCorr, 1);
            for (Expansion exp : expansiones) {
                for (Bloque ba : exp.bloquesDeTiempoAbsoluto(taCorr)) {
                    if (ba.getObjeto() instanceof Recurso && ba.getRecurso().getRecBase().esComun() && comunes
                            || ba.getObjeto() instanceof Recurso && !(ba.getRecurso().getRecBase().esComun()) && nocomunes
                            || ba.getObjeto() instanceof Transformacion && nocomunes) {
                        agregaBloque(bloquesFin, ba);
                    }
                }
            }
        } while (calcP.hallaAvanceTiempo(taCorr, taFin) < 0);
        Collections.sort(bloquesFin);
        return bloquesFin;
    }

    /**
     * Agraga al Parque this los Recursos y Transformaciones NO COMUNES 
     * de la expansión forzada del estudio y del caso,
     * entre el paso siguiente al taIni, y el tiempoAbsoluto taFin inclusive
     * Avanza la etapa del parque hasta la de taFin
     * taFin debe ser el tiempoAbsoluto del paso representativo de la etapa e+1.
     *           
     * 
     * ES DECIR NO AGREGA LOS Recursos y Transformaciones COMUNES, QUE NO TIENEN LUGAR EN 
     * EL CÓDIGO DEL PARQUE
     * 
     *
     * @param taIni es el TiempoAbsoluto inicial en el que vive bloquesIni
     * @param taFin es el último TiempoAbsoluto cuyas expansiones se incluyen.
     * @param efin es la etapa de TiempoAbsoluto taFin.
     * @param informacion
     * 
     */
    public static void agregaExpsForzadasCE(Parque parque,
            TiempoAbsoluto taIni,
            TiempoAbsoluto taFin,
            int efin,
            ArrayList<Object> informacion) throws XcargaDatos, XBloqueMultNeg {

        Estudio est = (Estudio) informacion.get(0);
        CalculadorDePlazos calcP = est.getDatosGenerales().getCalculadorDePlazos();
        Caso caso = (Caso) informacion.get(1);
        Expansion expTotal = caso.getExpansionForzadaTotal();
        TiempoAbsoluto taCorr = taIni.copiaTiempoAbsoluto();
        do {
            taCorr = calcP.hallaTiempoAbsoluto(taCorr, 1);
            for (Bloque ba : expTotal.bloquesDeTiempoAbsoluto(taCorr)) {
                if (ba.getObjeto() instanceof Recurso && !(ba.getRecurso().getRecBase().esComun())
                        || ba.getObjeto() instanceof Transformacion) {
                    int indice = est.indiceEnPosibles(ba.getObjeto());
                    int mult = ba.getMultiplicador();
                    parque.getCBloques()[indice] = parque.getCBloques()[indice] + mult;
                }
            }
        } while (calcP.hallaAvanceTiempo(taCorr, taFin) < 0);
        parque.setEtapa(efin);
        return;
    }

    /**
     * Aplica a los bloques de bloquesIni los cambios aleatorios. Se eliminan de bloquesIni
     * los bloques cambiados y se agrega a bloquesFin los bloques resultantes del cambio
     * aleatorio.
     *
     * @param bloquesIni bloques iniciales de la etapa e.
     * @param bloquesFin bloques finales de la etapa e+1.
     * @param taFin TiempoAbsoluto de la etapa e+1.
     * @param ne nodoN de la etapa e.
     * @param nemas1 nodoN de la etapa e+1.
     * @param conjCA es el conjunto de cambios aleatorios del estudio.
     * @param informacion es el paquete de información get(0) tiene el estudio
     * y get(1) tiene el caso.
     *
     */
    public static void aplicaCambiosAleatorios(ArrayList<Bloque> bloquesIni, ArrayList<Bloque> bloquesFin,
            TiempoAbsoluto taFin,
            NodoN ne, NodoN nemas1,
            ConjCambiosAleat conjCA, ArrayList<Object> informacion) throws XcargaDatos, XBloqueMultNeg {
        for (CambioAleatorio cA : conjCA.getCambios()) {
            VariableNat vCA = cA.getVarNat();
            if (ne.valorDeUnaVN(vCA).equalsIgnoreCase("NO")
                    && nemas1.valorDeUnaVN(vCA).equalsIgnoreCase("SI")) {
                // La VariableNat asociada cambia de No a Si entre los dos nodos
                ArrayList<Bloque> aReducir = new ArrayList<Bloque>();
                ArrayList<Integer> cuantoReduce = new ArrayList<Integer>();
                ArrayList<Bloque> aEliminar = new ArrayList<Bloque>();
                boolean estanTodos = true;
                int indice = 0;
                for (Recurso rec : cA.getRecursosOrigen()) {
                    RecursoBase rb = rec.devuelveRB();
                    int multrb = cA.getMulRecOrigen().get(indice);
                    boolean estaUno = false;
                    for (Bloque b : bloquesIni) {
                        if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC)
                                && rb == ((Recurso) b.getObjeto()).getRecBase()
                                && b.getMultiplicador() >= multrb) {
                            if (b.getMultiplicador() > multrb) {
                                aReducir.add(b);
                                cuantoReduce.add(multrb);
                            } else {
                                aEliminar.add(b);
                            }
                            estaUno = true;
                            break;
                        }
                    }
                    if (!estaUno) {
                        estanTodos = false;
                        break;
                    }
                    indice++;
                }
                if (estanTodos) {
                    // Los recursos origen están todos en bloquesIni y tiene lugar el cambio aleatorio
                    // Se reduce el multiplicador de los bloques de bloquesIni que no se eliminan
                    indice = 0;
                    for (Bloque b : aReducir) {
                        b.setMultiplicador(b.getMultiplicador() - cuantoReduce.get(indice));
                        indice++;
                    }
                    // Se eliminan los bloques de bloquesIni que deben eliminarse
                    for (Bloque b : aEliminar) {
                        bloquesIni.remove(b);
                    }
                    // Se cargan en bloquesFin los bloques resultantes del cambio aleatorio
                    indice = 0;
                    for (Recurso rec : cA.getRecursosDestino()) {
                        /*
                         * El indicadorTiempo se pone en 0, en el primer año de vida, como la edad de las personas.
                         */
                        Bloque blo = new Bloque(rec, 1, taFin);
                        blo.setMultiplicador(cA.getMulRecDestino().get(indice));
                        agregaBloque(bloquesFin, blo);
                        indice++;
                    }
                } else {
                    /**
                     * OJOJOJOJOJOJOJOJOJOJOJOJOJO ACA HAY QUE OCASIONAR MUERTE DE LA CORRIDA POR
                     * INCONSISTENCIA. 
                     */
                }
            }
        }
        Collections.sort(bloquesFin);
    }

    /**
     * Aplica a un Parque los cambios aleatorios. Se eliminan de bloquesIni
     * los bloques cambiados y se agrega a bloquesFin los bloques resultantes del cambio
     * aleatorio.
     *
     * @param bloquesFin bloques finales de la etapa e+1.
     * @param taFin TiempoAbsoluto de la etapa e+1.
     * @param ne nodoN de la etapa e.
     * @param nemas1 nodoN de la etapa e+1.
     * @param conjCA es el conjunto de cambios aleatorios del estudio.
     * @param informacion es el paquete de información get(0) tiene el estudio
     * y get(1) tiene el caso.
     *
     * Se supone que la etapa ya fue puesta en e+1.
     */
    public static void aplicaCambiosAleatoriosCE(Parque parque,
            TiempoAbsoluto taFin,
            NodoN ne, NodoN nemas1,
            ConjCambiosAleat conjCA, ArrayList<Object> informacion) throws XcargaDatos, XBloqueMultNeg {

        Estudio est = (Estudio) informacion.get(0);

        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();

        boolean soloNoComunes = true;
        boolean prod = false;
        boolean usaEquiv = false;
        boolean soloNoNulos = true;
        informacionImp.add(soloNoComunes);
        informacionImp.add(prod);
        informacionImp.add(usaEquiv);
        informacionImp.add(soloNoNulos);
        for (CambioAleatorio cA : conjCA.getCambios()) {
            VariableNat vCA = cA.getVarNat();
            if (ne.valorDeUnaVN(vCA).equalsIgnoreCase("NO")
                    && nemas1.valorDeUnaVN(vCA).equalsIgnoreCase("SI")) {
                // La VariableNat asociada cambia de No a Si entre los dos nodos

                boolean estanTodos = true;
                int indRec = 0;
                for (Recurso rec : cA.getRecursosOrigen()) {
                    // cantEnParque es la cantidad de unidades del Recurso rec en parque con el estado 
                    // requerido por el cA
                    // multrb es la cantidad que requiere el cambio aleatorio cA
                    int cantEnParque = parque.cantDeUnRecBaseCE(rec.getRecBase(), rec.getEstado(), informacion);
                    int multrb = cA.mulRecursoOrigen(rec);
                    if (cantEnParque < multrb) {
                        estanTodos = false;
                        break;
                    }
                    indRec++;
                }
                if (estanTodos) {
                    // Los recursos origen están todos en cantidad suficiente en el parque 
                    // y tiene lugar el cambio aleatorio
                    //
                    // Se reduce el código entero de los Recursos origen en el código del parque
                    for (Recurso recCA : cA.getRecursosOrigen()) {
                        int indIni = est.primerIndiceEnPosibles(recCA.getRecBase(), recCA.getEstado());
                        int aRestar = cA.mulRecursoOrigen(recCA);
                        int ind = indIni;
                        while (aRestar > 0 && ind < est.getCantRecNoComunes()) {
                            Recurso rec = (Recurso) est.recNoComunYTransPosible(ind);
                            if (rec.getRecBase() == recCA.getRecBase()
                                    || rec.getEstado().equalsIgnoreCase(recCA.getEstado())) {
                                if (aRestar >= parque.codigoDeInd(ind)) {
                                    aRestar = aRestar - parque.codigoDeInd(ind);
                                    parque.cargaCodigo(ind, 0);

                                } else {
                                    parque.cargaCodigo(ind, parque.codigoDeInd(ind) - aRestar);
                                    aRestar = 0;
                                }
                            }
                        }
                        if (aRestar > 0) {


                            throw new XcargaDatos("error en cambio aleatorio "
                                    + cA.getNombre() + "al reducir recursos origen de parque" + parque.toStringCorto(informacion, informacionImp));

                        }
                    }
                    // Se aumenta el código de los Recursos destino
                    for (Recurso rec : cA.getRecursosDestino()) {
                        /*
                         * Demora aleatoria se pone en 0.
                         * Inversión aleatoria se pone en 1.
                         * El indicadorTiempo se pone en 0, en el primer año de vida, como la edad de las personas.
                         */
                        int ind = est.indiceEnPosibles(rec, rec.getEstado(), 1, 0, 1);
                        int mult = cA.mulRecursoDestino(rec);
                        parque.cargaCodigo(ind, parque.codigoDeInd(ind) + mult);
                    }
                } else {
                    /**
                     * Los Recursos no están en cantidad suficiente se lanza una excepción:
                     * 
                     */
                    throw new XcargaDatos("error en cambio aleatorio "
                            + cA.getNombre() + "al reducir recursos origen de parque" + parque.toStringCorto(informacion, informacionImp));
                }
            }
        }
    }

//    /**
//     *
//     * @param bloquesIni ArrayList<Bloque> inicial que contiene transformaciones y del que se eliminan
//     * las transformaciones y los recursos origen y transitorios de las transformaciones
//     * competadas.
//     * @param bloquesFin ArrayList<Bloque> final con las transformaciones no completadas que tuvieron avance
//     * del tiempo, y con los recursos destino de las transformaciones completadas.
//     *
//     * @param ne nodoN de la etapa e.
//     * @param nemas1 nodoN de la etapa e+1.
//     * @param ta es el TiempoAbsoluto de la etapa e en la que viven los bloques bloquesIni.
//     * @param avanceTiempo es el avance en pasos de tiempo desde la etapa e hasta la etapa e+1
//     * @param informacion
//     * @throws persistenciaMingo.XcargaDatos
//     */
//    public static void avanzaTiempoTransformaciones(ArrayList<Bloque> bloquesIni, ArrayList<Bloque> bloquesFin,
//            NodoN ne, NodoN nemas1,
//            TiempoAbsoluto ta, int avanceTiempo,
//            ArrayList<Object> informacion) throws XcargaDatos, XBloqueMultNeg {
//        /**
//         * TRANSFORMACIONES QUE COMPLETAN SU CONSTRUCCION INCLUSO DEMORA:
//         * Crea en bloquesFin los bloques de recursos resultantes.
//         * Elimina de bloquesIni los bloques de recursos origen o transitorios según el caso.
//         * Elimina el bloque de la transformación de bloquesIni
//         * Agrega en bloquesFin la transformación con EstadosRec.TERMIN
//         *
//         * TRANSFORMACIONES QUE QUEDAN EN CONSTRUCCIÓN O DEMORADAS
//         * Crea en bloquesFin el bloque de la transformación con los cambios por avance
//         * de tiempo.
//         * Elimina el bloque de la transformación de pIni
//         *
//         * Como resultado del método en pIni no queda ningún bloque con una transformación.
//         * En cambio quedan los bloques de recursos transitorios o origen si la transformación no se completó aún.
//         */
//        Estudio est = (Estudio) informacion.get(0);
//        CalculadorDePlazos calcP = est.getDatosGenerales().getCalculadorDePlazos();
//        TiempoAbsoluto taFin = ta.copiaTiempoAbsoluto();
//        taFin.avanzaNPasos(avanceTiempo, calcP);
//        ArrayList<Bloque> aEliminarDePIni = new ArrayList<Bloque>();
//        ArrayList<Bloque> aAgregarAPIni = new ArrayList<Bloque>();
//        ArrayList<RecursoBase> rBOrigenAEliminar = new ArrayList<RecursoBase>();
//
//        /**
//         * ---------------------------------------------------------------------
//         * Carga recursos transitorios de las transformaciones recién
//         * incorporadas mediante la decisión de etapa e, si es que tienen dichos
//         * recursos transitorios y en ese caso elimina los recursos origen
//         * ---------------------------------------------------------------------
//         */
//        int maxPConstDatGen = est.getDatosGenerales().maxPerConstDeTA(ta);
//        for (Bloque b : bloquesIni) {
//            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.TRANS)) {
//                Transformacion tr = b.getTransformacion();
//                TransforBase tBase = tr.getTransBase();
//                // -perConst es el indicador de tiempo de un Transformacion recién agregada en una decisión.
//                int perConst = Math.min(maxPConstDatGen, tBase.getPerConstr(ta, est));
//                if (tr.getEstado().equalsIgnoreCase(EstadosRec.CON) && tr.getIndicadorTiempo() == (-perConst)
//                        && !(tBase.getRecursosTransitorios().isEmpty())) {
//                    /**
//                     * La transformación recién se incorporó en una decisión.
//                     * Si existen recursos transitorios deben agregarse.
//                     * Crea los recursos transitorios para agregar luego
//                     */
//                    int indTrans = 0;
//                    for (Recurso rec : tBase.getRecursosTransitorios()) {
//
//                        // ATENCIÓN EL RECURSO TRANSITORIO SE CARGA CON INDICADOR DE TIEMPO 0
//                        Bloque bTrans = new Bloque(rec, tBase.getMulRecTransit().get(indTrans), ta);
//                        aAgregarAPIni.add(bTrans);
//                        indTrans++;
//                    }
//                    /**
//                     * Marca los recursosBase origen a eliminar
//                     * ATENCIÓN: TODOS LOS BLOQUES CON RECURSOS BASE COMO LOS
//                     * RECURSOS ORIGEN SON SUSTITUÍDOS.
//                     */
//                    for (Recurso rElim : tBase.getRecursosOrigen()) {
//                        RecursoBase rb = rElim.getRecBase();
//                        rBOrigenAEliminar.add(rb);
//                    }
//                }
//            }
//        }
//        // Marca los recursos origen a eliminar
//
//        for (Bloque b : bloquesIni) {
//            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC)
//                    && rBOrigenAEliminar.contains(b.getRecurso().getRecBase())) {
//                aEliminarDePIni.add(b);
//            }
//        }
//
//        // Elimina los recursos origen
//        for (Bloque b : aEliminarDePIni) {
//            bloquesIni.remove(b);
//        }
//        // Agrega los recursos transitorios
//        for (Bloque b : aAgregarAPIni) {
//            agregaBloque(bloquesIni, b);
//        }
//
//        /**
//         * ---------------------------------------------------------------------
//         * Procesa las transformaciones
//         * ---------------------------------------------------------------------
//         */
//        aEliminarDePIni = new ArrayList<Bloque>();  // guarda bloques de pIni a aliminar
//        ArrayList<Bloque> transAEjecutar = new ArrayList<Bloque>();
//
//
//        Bloque bNue;
//        /**
//         * transAEjecutar guarda los bloques con las transformaciones a ejecutar
//         * una vez que se hayan recorrido todos los bloques de pIni.
//         * Esos bloques no aparecerán en pFin
//         */
//        for (Bloque b : bloquesIni) {
//            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.TRANS)) {
//                Transformacion tr = b.getTransformacion();
//                TransforBase tBase = tr.getTransBase();
//                if (tr.getEstado().equalsIgnoreCase(EstadosRec.DEM)) {
//                    /**
//                     * La transformacion está demorada
//                     */
//                    int demoraRestante = tr.getIndicadorTiempo();
//                    if (demoraRestante + avanceTiempo < 0) {
//                        // la transformacion continúa demorada
//                        bNue = b.copiaBloque();
//                        aEliminarDePIni.add(b);
//                        bNue.getTransformacion().setIndicadorTiempo(tr.getIndicadorTiempo() + avanceTiempo);
//                        agregaBloque(bloquesFin, bNue);
//                    } else {
//                        // la transformación debe aplicarse porque termina la demora
//                        transAEjecutar.add(b);
//                    }
//                } else {
//                    /**
//                     *  La transformacion está en el período normal de construcción
//                     */
//                    int avanceConstruccion = tr.getIndicadorTiempo();
//                    if (avanceConstruccion + avanceTiempo >= 0) {
//                        // se completó el período normal de construcción
//                        if (!tBase.getTieneDemoraAl()) {
//                            // la transformación no tiene demora y debe aplicarse
//                            transAEjecutar.add(b);
//                        } else if (tBase.getTieneDemoraAl()) {
//                            // la transformación tiene demora
//                            // ATENCIÓN: se toma la demora de la etapa emas1
//                            VariableNat vNDemora = tBase.getVNDemoraAl();
//                            int demora = Integer.parseInt(nemas1.valorDeUnaVN(vNDemora));
//                            b.getTransformacion().setValorDemAl(demora);
//                            if (avanceConstruccion + avanceTiempo - demora >= 0) {
//                                // Se termina la construcción incluso la demora
//                                transAEjecutar.add(b);
//                            } else {
//                                // La transformacion queda demorada
//                                // se carga el avance de construcción resultante
//                                bNue = b.copiaBloque();
//                                aEliminarDePIni.add(b);
//                                bNue.getTransformacion().setIndicadorTiempo(avanceConstruccion + avanceTiempo - demora);
//                                agregaBloque(bloquesFin, bNue);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        /**
//         * Se eliminan de pIni los bloques de las transformaciones que no se completaron
//         * y se pasaron a pFin actualizadas
//         */
//        for (Bloque b : aEliminarDePIni) {
//            bloquesIni.remove(b);
//        }
//
//        /**
//         * Se aplican las transformaciones completadas
//         */
//        for (Bloque btr : transAEjecutar) {
//            aplicaUnaTransformacion(btr, bloquesIni, bloquesFin, taFin, ne, nemas1,
//                    informacion);
//        }
////          pIni.cargaResumenP();
////          pFin.cargaResumenP();
//        Collections.sort(bloquesFin);
//    }
    /**
     * Aplica al Parque parque el avance de tiempo de las transformaciones que existen en él.
     *
     * @param parque es el parque al que se le aplica el avance del tiempo de sus transformaciones.
     * @param ne nodoN de la etapa e.
     * @param nemas1 nodoN de la etapa e+1.
     * @param ta es el TiempoAbsoluto de la etapa e en la que viven los bloques bloquesIni.
     * @param avanceTiempo es el avance en pasos de tiempo desde la etapa e hasta la etapa e+1
     * @param informacion
     * @throws persistenciaMingo.XcargaDatos
     */
    public static void avanzaTiempoTransformacionesCE(Parque parque,
            NodoN ne, NodoN nemas1,
            TiempoAbsoluto ta, int avanceTiempo,
            ArrayList<Object> informacion) throws XcargaDatos, XBloqueMultNeg {
        /**
         * RECORRE LAS TRANSFORMACIONES EN CBLOQUES DE PARQUE.
         * Y CARGA LAS TRANSFORMACIONES RESULTANTES DEL AVANCE DE TIEMPO EN CBLOQUESAUX.
         * - TRANSFORMACIONES QUE COMPLETAN SU CONSTRUCCION INCLUSO DEMORA:
         *   Delega en el método aplicaTransformacionCE lo siguiente:
         *     - Aumenta en cBloques de parque los códigos de recursos resultantes.
         *     - Reduce en cBloques los códigos de recursos origen o transitorios según el caso.
         *
         * - TRANSFORMACIONES QUE QUEDAN EN CONSTRUCCIÓN O DEMORADAS
         *   Aumenta en CBloquesAux el código de la transformación con los cambios por avance de tiempo
         * 
         * AL FINAL LO QUE QUEDA EN CBLOQUESAUX LO VUELVE A CARGAR EN PARQUE
         * Como las transformaciones que se completan no aumentan el código en cBloqueAux.
         * desaparecen del parque.
         * 
         * 
         *
         */
        Estudio est = (Estudio) informacion.get(0);
        CalculadorDePlazos calcP = est.getDatosGenerales().getCalculadorDePlazos();
        TiempoAbsoluto taFin = ta.copiaTiempoAbsoluto();
        taFin.avanzaNPasos(avanceTiempo, calcP);


        /**
         * ---------------------------------------------------------------------
         * Carga recursos transitorios de las transformaciones recién
         * incorporadas mediante la decisión de etapa e, si es que tienen dichos
         * recursos transitorios y en ese caso elimina los recursos origen
         * ---------------------------------------------------------------------
         * 
         * ATENCION: NO SE IMPLEMENTARON LOS RECURSOS TRANSITORIOS
         */
        
        int maxPConstDatGen = est.getDatosGenerales().maxPerConstDeTA(ta);
      
        Parque.anulaCBloquesAux();

        // Recorre las transformaciones en cBloques
        for (int ind = est.getCantRecNoComunes(); ind < est.getCantRecNoComunesYTransPosibles(); ind++) {
            int cantTrans = parque.codigoDeInd(ind);
            // cantTrans es la cantidad de transformaciones idénticas del parque.
            if (cantTrans > 0) {
                /**
                 *  Hay alguna una Transformación trans en el Parque parque en el lugar ind de cBloques 
                 * 
                 *  Los datos de trans son:
                 *  estado
                 *  tb 
                 *  indicTiempo 
                 *  tb tiene período de construcción normal perconst
                 * 
                 *  Se van modificando los códigos en cBloquesAux según el caso
                 *  Al final de todo se hace en un único lugar la reducción del código de la
                 *  Transformacion de índice ind que se está tratando.
                 */
                Transformacion trans = (Transformacion) est.recNoComunYTransPosible(ind);
                TransforBase tb = trans.getTransBase();
                // -perConst es el indicador de tiempo de un Transformacion recién agregada en una decisión.
//                int perConst = Math.min(maxPConstDatGen, tb.getPerConstr(ta, est));
                String estado = trans.getEstado();

                if (estado.equalsIgnoreCase(EstadosRec.CON)) {
                    int avanceConstruccion = trans.getIndicadorTiempo();
                    if (avanceConstruccion + avanceTiempo >= 0) {
                        // Se completó el período normal de construcción
                        // Se carga el valor de inversión aleatoria o 1.0 por defecto.
                        double invAl = 1.0;
                        if (tb.getTieneInvAleat() == true) {
                            VariableNat vNInvAl = tb.getVNInvAl();
                            invAl = Double.parseDouble(nemas1.valorDeUnaVN(vNInvAl));
                        }
                        if (!tb.getTieneDemoraAl()) {
                            // La Transformacion no admite demora
//                            int indicTiempoAux = avanceConstruccion + avanceTiempo;
                            aplicaTransformacionCE(parque, trans, cantTrans, invAl, 0, informacion);
                        } else {
                            // La Transformacion admite demora
                            // ATENCIÓN: se toma la demora de la etapa emas1
                            VariableNat vNDemora = tb.getVNDemoraAl();
                            int demora = Integer.parseInt(nemas1.valorDeUnaVN(vNDemora));
                            if (avanceConstruccion + avanceTiempo - demora < 0) {
                                // La Transformacion queda demorada se carga la demora restante
                                // y el valor de demora total                                
                                int indicTiempoAux = avanceConstruccion + avanceTiempo - demora;
                                int indiceEnAux = est.indiceEnPosibles(tb, EstadosRec.DEM, invAl, demora, indicTiempoAux);
                                Parque.cargaCodigoAux(indiceEnAux, Parque.codigoDeIndAux(indiceEnAux) + parque.codigoDeInd(ind));

                            } else {
                                // Se terminó la demora                                                                
//                                int indicTiempoAux = avanceConstruccion + avanceTiempo - demora;
                                aplicaTransformacionCE(parque, trans, cantTrans, invAl, demora, informacion);
                            }
                        }
                    } else {
                        // No se completó al período normal de construcción
                        int indicTiempoAux = avanceConstruccion + avanceTiempo;
                        int indiceEnAux = est.indiceEnPosibles(tb, EstadosRec.CON, trans.getValorInvAl(), trans.getValorDemAl(), indicTiempoAux);
                        Parque.cargaCodigoAux(indiceEnAux, Parque.codigoDeIndAux(indiceEnAux) + parque.codigoDeInd(ind));
                    }
                } else if (trans.getEstado().equalsIgnoreCase(EstadosRec.DEM)) {
                    /**
                     * La transformacion está demorada
                     */
                    int demoraRestante = trans.getIndicadorTiempo();
                    if (demoraRestante + avanceTiempo < 0) {
                        /**
                         * La transformacion continúa demorada
                         */
                        int indicadorTiempoAux = demoraRestante + avanceTiempo;
                        int indiceEnAux = est.indiceEnPosibles(tb, EstadosRec.DEM, trans.getValorInvAl(), trans.getValorDemAl(), indicadorTiempoAux);
                        Parque.cargaCodigoAux(indiceEnAux, Parque.codigoDeIndAux(indiceEnAux) + parque.codigoDeInd(ind));
                    } else {
                        // la transformación debe aplicarse porque termina la demora
                        aplicaTransformacionCE(parque, trans, cantTrans, trans.getValorInvAl(), trans.getValorDemAl(), informacion);
                    }
                } else {
                    /**
                     *  La Transformacion está en estado TERMIN o FALL y no se modifica con el avance del tiempo
                     *  se aumenta el código en cBloquesAux en ind
                     *  en la cantidad cBloques que está en ind.
                     */
                    Parque.cargaCodigoAux(ind, Parque.codigoDeIndAux(ind) + parque.codigoDeInd(ind));
                }
            }
        }
        // Carga el codigoAux de las transformaciones al codigo del parque
        for (int i = est.getCantRecNoComunes(); i < est.getCantRecNoComunesYTransPosibles(); i++) {
            parque.cargaCodigo(i, Parque.codigoDeIndAux(i));
        }
    }
    

//    /**
//     * Aplica una transformación a pIni eliminando de él los bloques de los recursos
//     * origen o transitorios y el bloque de la transformación. Crea los bloques
//     * de los nuevos recursos en pFin. Crea el bloque de la transformación completa
//     * con EstadosRec.TERMIN
//     * @param blTrans es el bloque de pIni con las transformación a aplicar.
//     * @param pIni es el parque inicial del que se eliminan los
//     * bloques con los recursos origen o transitorios y el bloque con la transformación.
//     * @param pFin es el parque final en el que se agregan los recursos
//     * destino.
//     * @param taFin es el tiempo absoluto de la etapa e+1.
//     * @param ne nodoN de la etapa e.
//     * @param nemas1 nodoN de la etapa e+1.
//     * @param avanceTiempo es el avance en pasos de tiempo desde e hasta e+1.
//     * @param informacion.
//     */
//    public static void aplicaUnaTransformacion(Bloque blTrans, ArrayList<Bloque> bloquesIni, ArrayList<Bloque> bloquesFin,
//            TiempoAbsoluto taFin,
//            NodoN ne, NodoN nemas1,
//            ArrayList<Object> informacion) throws XcargaDatos, XBloqueMultNeg {
//        // Agrega a pFin los bloques de recursos destino
//        TransforBase tb = blTrans.getTransformacion().getTransBase();
//        boolean estanTodos = true;
////        int minimo = 0;
////        int maximo = 9999;
//        if (!tb.getRecursosTransitorios().isEmpty()) {
//            /**
//             * La transformación tiene recursos transitorios que aparecieron
//             * cuando se comenzó su construcción (al agregarse la decision al parque)
//             * y sustituyeron a los recursos origen
//             */
//            ArrayList<Bloque> aEliminar = new ArrayList<Bloque>();
//            /**
//             * ATENCIÓN: SE ELIMINAN TODOS LOS BLOQUES QUE TENGAN RECURSOS BASE
//             * COMO LOS TRANSITORIOS
//             */
//            for (RecursoBase rb : tb.getRecursosTransitorios()) {
//                for (Bloque b : bloquesIni) {
//                    if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC) && b.getRecurso().getRecBase() == rb) {
//                        aEliminar.add(b);
//                    }
//                }
//            }
//            for (Bloque b : aEliminar) {
//                bloquesIni.remove(b);
//            }
//        }
//
//        if (tb.getRecursosTransitorios().isEmpty()) {
//            /**
//             * La transformacion no tiene recursos transitorios y por lo tanto
//             * en el parque pIni permanecen los recursos origen
//             * Se reduce el multiplicador o se eliminan los bloques asociados
//             * a esos recursos origen en pIni.
//             */
//            ArrayList<Bloque> aReducir = new ArrayList<Bloque>();
//            ArrayList<Integer> cuantoReduce = new ArrayList<Integer>();
//            ArrayList<Bloque> aEliminar = new ArrayList<Bloque>();
//            int indice = 0;
//            for (RecursoBase rb : tb.getRecOrigenConRep()) {
//                int multrb = tb.getMulRBOrigen().get(indice);
//                boolean estaUno = false;
//                for (Bloque b : bloquesIni) {
//                    if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC)
//                            && rb == b.getRecurso().getRecBase()
//                            && b.getMultiplicador() >= multrb) {
//                        if (b.getMultiplicador() > multrb) {
//                            aReducir.add(b);
//                            cuantoReduce.add(multrb);
//                        } else {
//                            aEliminar.add(b);
//                        }
//                        estaUno = true;
//                        break;
//                    }
//                }
//                if (!estaUno) {
//                    estanTodos = false;
//                    break;
//                }
//                indice++;
//            }
//            if (estanTodos) {
//                // Los recursos origen están todos en pIni y tiene lugar la transformación
//                // Se reduce el multiplicador de los bloques de pIni que no se eliminan
//                indice = 0;
//
//                for (Bloque b : aReducir) {
//                    Bloque bNue = b.copiaBloque();
//                    bNue.setMultiplicador(b.getMultiplicador() - cuantoReduce.get(indice));
//                    agregaBloque(bloquesIni, bNue);
//                    aEliminar.add(b);
//                    indice++;
//                }
//                // Se eliminan los bloques de pIni que deben eliminarse
//                for (Bloque b : aEliminar) {
//                    bloquesIni.remove(b);
//                }
//            } else {
//                // los recursos origen de la transformación no están todos en pIni
//                // se mantiene la transformación en pFin con los costos asociados
//                // y se mantienen los recursos origen en pIni
//                Bloque bTransNue = blTrans.copiaBloque();
//                bTransNue.getTransformacion().setEstado(EstadosRec.FALL);
//                bloquesFin.add(bTransNue);
//            }
//        }
//
//        // Agrega a pFin los bloques de los recursos destino con tipo OPE, si estaban todos los bloques origen
//        if (estanTodos) {
//            for (RecursoBase rb : tb.getRecursosDestino()) {
//                Recurso rec = new Recurso(rb, EstadosRec.OPE, 0);
//                //ATENCIÓN CON LA EDAD QUE ACA NO SE ARREGLÓ Y TODO NACE CON EDAD 0
//                Bloque blo = new Bloque(rec, 1, taFin);
//                agregaBloque(bloquesFin, blo);
//            }
//        }
//        // Agrega  a pFin el bloque de la transformación completa con EstadosRec.TERMIN
//        Bloque bTransNue = blTrans.copiaBloque();
//        bTransNue.getTransformacion().setEstado(EstadosRec.TERMIN);
//
//        // Elimina el bloque de la transformacion de pIni.
//        bloquesIni.remove(blTrans);
////          pIni.cargaResumenP();
////          pFin.cargaResumenP();
//        Collections.sort(bloquesFin);
//    }
    /**
     * 
     * Aplica las Transformaciones de un código dado en CBloques
     * 
     * @param parque es el parque al que hay que aplicarle la Transformacion
     * @param trans es la transformación que hay que aplicar
     * @param cantTrans es la cantidad de transformaciones de ese tipo a aplicar
     * @param invAl es la inversión aleatoria que debe tener la Transformacion FALL o TERMIN resultante
     * @param demora es la demora aleatoria, idem.
     * 
     */
    public static void aplicaTransformacionCE(Parque parque, Transformacion trans, int cantTrans, double invAl, int demora,
            ArrayList<Object> informacion) throws XcargaDatos {
        /**
         * Reduce los códigos de los Recursos origen en el Parque parque
         * Aumenta los códigos de los Recursos destino en Parque parque
         * Aumenta el código de la Tranformacion en estado TERMIN o FALL según
         * tenga éxito o no, en cBloqueAux.
         * DEBERÍA HACER LO SIGUIENTE QUE NO ESTÁ IMPLEMENTADO:
         * Si no encuentra Recursos origen suficientes no aplica la transformacion
         * y deja en cBloques una Transformacion FALL (fallida) que cargará con los
         * costos de inversión del error cometido.
         */
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        boolean soloNoComunes = true;
        boolean prod = false;
        boolean usaEquiv = false;
        boolean soloNoNulos = true;
        informacionImp.add(soloNoComunes);
        informacionImp.add(prod);
        informacionImp.add(usaEquiv);
        informacionImp.add(soloNoNulos);
        Estudio est = (Estudio) informacion.get(0);
        TransforBase tb = trans.getTransBase();
        boolean estanTodos = true;
        int indRec = 0;
        for (int icant = 1; icant <= cantTrans; icant++) {
            for (Recurso rec : tb.getRecursosOrigen()) {
                // cantEnParque es la cantidad de unidades del Recurso rec en parque con el estado 
                // requerido por la Transformacion tb.
                // multrb es la cantidad que requiere la transformación
                int cantEnParque = parque.cantDeUnRecBaseCE(rec.getRecBase(), rec.getEstado(), informacion);
                int multrb = tb.multDeUnRecOrigen(rec);
                if (cantEnParque < multrb) {
                    estanTodos = false;
                    break;
                }
                indRec++;
            }
            if (estanTodos) {
                // Los recursos origen están todos en cantidad suficiente en el parque 
                // y tiene lugar la Transformacion
                //
                // Se reduce el código entero de los Recursos origen en el código del parque
                for (Recurso recTB : tb.getRecursosOrigen()) {
                    int indIni = est.primerIndiceEnPosibles(recTB.getRecBase(), recTB.getEstado());
                    int aRestar = tb.multDeUnRecOrigen(recTB);
                    int ind = indIni;
                    while (aRestar > 0 && ind < est.getCantRecNoComunes()) {
                        Recurso rec = (Recurso) est.recNoComunYTransPosible(ind);
                        if (rec.getRecBase() == recTB.getRecBase()
                                || rec.getEstado().equalsIgnoreCase(recTB.getEstado())) {
                            if (aRestar >= parque.codigoDeInd(ind)) {
                                aRestar = aRestar - parque.codigoDeInd(ind);
                                parque.cargaCodigo(ind, 0);

                            } else {
                                parque.cargaCodigo(ind, parque.codigoDeInd(ind) - aRestar);
                                aRestar = 0;
                            }
                        }
                    }
                    if (aRestar > 0) {
                        throw new XcargaDatos("error en Transformacion "
                                + tb.getNombre() + "al reducir recursos origen de parque" + parque.toStringCorto(informacion, informacionImp));
                    }
                }
                // Se aumenta el código de los Recursos destino
                for (Recurso rec : tb.getRecursosDestino()) {
                    /*
                     * Demora aleatoria se pone en 0.
                     * Inversión aleatoria se pone en 1.
                     * El indicadorTiempo se pone en 0, en el primer año de vida, como la edad de las personas.
                     */
                    int ind = est.indiceEnPosibles(rec.getRecBase(), rec.getEstado(), 1, 0, 0);
                    int mult = tb.multDeUnRecDestino(rec);
                    parque.cargaCodigo(ind, parque.codigoDeInd(ind) + mult);
                }
                // Se aumenta el codigo de las Transformaciones terminadas en codigoAux. 
                int indt = est.indiceEnPosibles(tb, EstadosRec.TERMIN, invAl, demora, 0);
                Parque.cargaCodigoAux(indt, Parque.codigoDeIndAux(indt) + 1); 
            } else {
                /**
                 * Los Recursos no están en cantidad suficiente se lanza una excepción:
                 * 
                 */
                throw new XcargaDatos("error en transformación "
                        + tb.getNombre() + " al reducir recursos origen de parque" + parque.toStringCorto(informacion, informacionImp));
                // ATENCIÓN: ESTO PODRÍA RESOLVERSE AGREGANDO UNA TRANSFORMACION FALLIDA AL PARQUE CON UN COSTO INÚTIL
            }
        }

    }

    /**
     * Avanza los plazos de construcción y demoras de los recursos
     * en construcción en pIni y si se completan crea los recursos operativo en pFin.
     *
     * Elimina los recursos que mueren por edad o paso de tiempo último de pIni y no
     * los agrega en pFin
     * 
     * La inversión aleatoria se carga según el valor de la VN en la etapa e+1, cuando
     * un recurso CON completa su construcción.
     * 
     * La demora aleatoria se carga según el valor de la VN en la etapa e+1, cuando un 
     * recurso CON     
     *
     * @param bloquesIni ArrayList<Bloque> inicial que contiene sólo recursos a los que avanzar el tiempo
     * ATENCION bloquesIni NO SE CONSERVA.
     * @param bloquesFin ArrayList<Bloque> final con el producto del avance de tiempo de los recursos de pIni.
     * @param tamas1 TiempoAbsoluto de la etapa e+1
     * @param ne nodoN de la etapa e.
     * @param nemas1 nodoN de la etapa e+1.
     * @param avanceTiempo es el avance en pasos de tiempo desde e hasta e+1
     * @param informacion
     * @throws persistenciaMingo.XcargaDatos
     */
    public static void avanzaTiempoRecursos(ArrayList<Bloque> bloquesIni, ArrayList<Bloque> bloquesFin,
            NodoN ne, NodoN nemas1, TiempoAbsoluto tamas1, int avanceTiempo, ArrayList<Object> informacion)
            throws XcargaDatos, XBloqueMultNeg {
        DatosGeneralesEstudio datGen = ((Estudio) informacion.get(0)).getDatosGenerales();
        ArrayList<Bloque> aEliminar = new ArrayList<Bloque>();

        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        informacionImp.add(true);
        informacionImp.add(false);
        for (Bloque b : bloquesIni) {
            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC)) {
                /**
                 * Se procesan sólo los bloques de recursos, ya que las transformaciones
                 * ya se aplicaron
                 */
                Recurso rec = b.getRecurso();
                String estado = rec.getEstado();
                RecursoBase rb = b.getRecurso().getRecBase();
                Bloque bloNue = b.copiaBloque();
                Recurso recNue = bloNue.getRecurso();
                if (estado.equalsIgnoreCase(EstadosRec.OPE)) {
                    /**
                     * El recurso está operativo
                     */
                    int edadIni = rec.getIndicadorTiempo();
                    TiempoAbsoluto ultTa = rb.getUltimoTiempoAbsoluto();
                    if (rb.getTieneEdad() && (edadIni + avanceTiempo) > rb.getVidaUtil()
                            || (datGen.getCalculadorDePlazos().hallaAvanceTiempo(tamas1, ultTa) < 0)) {
                        // el recurso muere antes de la etapa e+1 y no se agrega a pFin
                        // igual se eliminará de pIni
                        aEliminar.add(b);

                    } else {
                        // el recurso envejece pero no muere, o no envejece y debe existir en pFin
                        if (rb.getTieneEdad()) {
                            recNue.setIndicadorTiempo(rec.getIndicadorTiempo() + avanceTiempo);
                        }
                        agregaBloque(bloquesFin, bloNue);
                        aEliminar.add(b);

                    }
                } else if (rec.getEstado().equalsIgnoreCase(EstadosRec.CON)) {
                    /**
                     * El recurso está en construcción en su período normal
                     */
                    int avanceConstruccion = rec.getIndicadorTiempo();
                    if (avanceConstruccion + avanceTiempo >= 0) {
                        // Se completó el período normal de construcción
                        // Se carga el valor de inversión aleatoria o 1.0 por defecto.
                        recNue.setValorInvAl(1.0);
                        if (recNue.getRecBase().getTieneInvAleat() == true) {
                            VariableNat vNInvAl = recNue.getRecBase().getVNInvAl();
                            recNue.setValorInvAl(Double.parseDouble(nemas1.valorDeUnaVN(vNInvAl)));
                        }
                        if (!rb.getTieneDemoraAl()) {
                            // El recurso no tiene demora y debe agregarse a pFin
                            int indicTiempo = avanceConstruccion + avanceTiempo;
                            poneOperativoRecurso(recNue, bloNue, b, rb, indicTiempo, bloquesFin, aEliminar);

                        } else {
                            // El recurso admite demora
                            // ATENCIÓN: se toma la demora de la etapa emas1
                            VariableNat vNDemora = rb.getVNDemoraAl();
                            int demora = Integer.parseInt(nemas1.valorDeUnaVN(vNDemora));
                            if (avanceConstruccion + avanceTiempo - demora < 0) {
                                // El recurso queda demorado se carga la demora restante
                                // y el valor de demora total
                                recNue.setEstado(EstadosRec.DEM);
                                recNue.setIndicadorTiempo(avanceConstruccion + avanceTiempo - demora);
                                recNue.setValorDemAl(demora);
                                agregaBloque(bloquesFin, bloNue);
                                aEliminar.add(b);

                            } else {
                                // Se terminó la demora y se agrega el recurso a pFin como operativo.                               
                                recNue.setValorDemAl(demora);
                                int indicTiempo = avanceConstruccion + avanceTiempo - demora;
                                poneOperativoRecurso(recNue, bloNue, b, rb, indicTiempo, bloquesFin, aEliminar);

                            }
                        }
                    }
                } else if (estado.equalsIgnoreCase(EstadosRec.DEM)) {
                    /**
                     * El recurso está demorado
                     */
                    int avanceConstruccion = rec.getIndicadorTiempo();
                    if (avanceConstruccion + avanceTiempo >= 0) {
                        int indicTiempo = avanceConstruccion + avanceTiempo;
                        poneOperativoRecurso(recNue, bloNue, b, rb, indicTiempo, bloquesFin, aEliminar);
                    } else {
                        // El recurso queda demorado
                        // se carga la demora restante
                        bloNue.getRecurso().setIndicadorTiempo(avanceConstruccion + avanceTiempo);
                        bloNue.cargaResumenB();
                        agregaBloque(bloquesFin, bloNue);
                        aEliminar.add(b);
                    }
                }
            }
        }

        // Elimina de pIni los bloques que fueron procesados

        for (Bloque b : aEliminar) {
            bloquesIni.remove(b);
        }
//          pIni.cargaResumenP();
//          pFin.cargaResumenP();
        if (!bloquesIni.isEmpty()) {
            throw new XcargaDatos("Error al procesar el parque "
                    + bloquesIni.toString());
        }
    }

    public static void poneOperativoRecurso(Recurso recNue, Bloque bloNue, Bloque b,
            RecursoBase rb, int indicTiempo,
            ArrayList<Bloque> bloquesFin, ArrayList<Bloque> aEliminar) throws XcargaDatos, XBloqueMultNeg {

        recNue.setEstado(EstadosRec.OPE);
        if (rb.getTieneEdad() == true) {
            recNue.setIndicadorTiempo(indicTiempo);
        } else {
            recNue.setIndicadorTiempo(0);
        }
        bloNue.cargaResumenB();
        agregaBloque(bloquesFin, bloNue);
        aEliminar.add(b);
    }

    /**
     * Avanza los plazos de construcción y demoras de los recursos
     * en construcción en parque y si se completan crea los recursos operativos.
     *
     * Elimina de parque los recursos que mueren por edad o paso de tiempo último
     * 
     * La inversión aleatoria se carga según el valor de la VN en la etapa e+1, cuando
     * un recurso CON completa su construcción.
     * 
     * La demora aleatoria se carga según el valor de la VN en la etapa e+1, cuando un 
     * recurso CON     
     *
     * @param parque es el parque que resulta modificado    
     * @param tamas1 TiempoAbsoluto de la etapa e+1
     * @param ne nodoN de la etapa e.
     * @param nemas1 nodoN de la etapa e+1.
     * @param avanceTiempo es el avance en pasos de tiempo desde e hasta e+1
     * @param informacion
     * @throws persistenciaMingo.XcargaDatos
     */
    public static void avanzaTiempoRecursosCE(Parque parque, NodoN ne, NodoN nemas1,
            TiempoAbsoluto tamas1, int avanceTiempo, ArrayList<Object> informacion)
            throws XcargaDatos, XBloqueMultNeg {
        /**
         * Se va cargando en cBloquesAux de parque los códigos resultantes del avance
         * del tiempo.
         * ESTO FUNCIONA PORQUE EL AVANCE DEL INDICADOR DE TIEMPO ES EN EL SENTIDO DE INDICE
         * CRECIENTE EN CBLOQUES Y CBLOQUESAUX.
         * Al final el contenido de cBloquesAux se pasa a cBloques.
         */
        Estudio est = (Estudio) informacion.get(0);
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        Parque.anulaCBloquesAux();  // se inicializa cBloquesAux en ceros.

        // Recorre todos los Recursos no comunes del Estudio
        for (int ind = 0; ind < est.getCantRecNoComunes(); ind++) {
            // cantRec es la cantidad de Recursos idénticos del parque.
            int cantRec = parque.codigoDeInd(ind);
            if (cantRec > 0) {
                Recurso rec = (Recurso) est.recNoComunYTransPosible(ind);
                String estado = rec.getEstado();
                RecursoBase rb = rec.getRecBase();
                if (estado.equalsIgnoreCase(EstadosRec.OPE)) {
                    /**
                     * El Recurso está operativo
                     */
                    int edadIni = rec.getIndicadorTiempo();
                    TiempoAbsoluto ultTa = rb.getUltimoTiempoAbsoluto();
                    if (rb.getTieneEdad() && (edadIni + avanceTiempo) > rb.getVidaUtil()
                            || (datGen.getCalculadorDePlazos().hallaAvanceTiempo(tamas1, ultTa) < 0)) {
                        // el recurso muere antes de la etapa e+1, será eliminado al final y no se agrega nada.
                    } else {
                        // el recurso envejece pero no muere, o no envejece y sigue existiendo.                        
                        int indicTiempoAux;
                        if (rb.getTieneEdad()) {
                            indicTiempoAux = rec.getIndicadorTiempo() + avanceTiempo;
                        } else {
                            indicTiempoAux = rec.getIndicadorTiempo();
                        }
                        int indiceEnAux = est.indiceEnPosibles(rb, EstadosRec.OPE, rec.getValorInvAl(), rec.getValorDemAl(), indicTiempoAux);
                        Parque.cargaCodigoAux(indiceEnAux, Parque.codigoDeIndAux(indiceEnAux) + parque.codigoDeInd(ind));
                    }
                } else if (rec.getEstado().equalsIgnoreCase(EstadosRec.CON)) {
                    /**
                     * El Recurso está en construcción en su período normal.
                     */
                    int avanceConstruccion = rec.getIndicadorTiempo();
                    double invAl;
                    if (avanceConstruccion + avanceTiempo >= 0) {
                        // Se completó el período normal de construcción
                        // Se carga el valor de inversión aleatoria o 1.0 por defecto.
                        // ATENCIÓN: se toma la inversión aleatoria de la etapa emas1
                        invAl = 1.0;
                        if (rb.getTieneInvAleat() == true) {
                            VariableNat vNInvAl = rb.getVNInvAl();
                            invAl = Double.parseDouble(nemas1.valorDeUnaVN(vNInvAl));
                        }
                        if (!rb.getTieneDemoraAl()) {
                            // El recurso no admite demora aleatoria
                            int indicTiempoAux = avanceConstruccion + avanceTiempo;
                            int indiceEnAux = est.indiceEnPosibles(rb, EstadosRec.OPE, invAl, rec.getValorDemAl(), indicTiempoAux);
                            Parque.cargaCodigoAux(indiceEnAux, Parque.codigoDeIndAux(indiceEnAux) + parque.codigoDeInd(ind));
                        } else {
                            // El recurso admite demora aleatoria y debe cargarse ahora
                            // porque se completó la construcción normal.
                            // ATENCIÓN: se toma la demora de la etapa emas1
                            VariableNat vNDemora = rb.getVNDemoraAl();
                            int demora = Integer.parseInt(nemas1.valorDeUnaVN(vNDemora));
                            if (avanceConstruccion + avanceTiempo - demora < 0) {
                                // El recurso queda demorado se carga la demora restante
                                // y el valor de demora total                                
                                int indicTiempoAux = avanceConstruccion + avanceTiempo - demora;
                                int indiceEnAux = est.indiceEnPosibles(rb, EstadosRec.DEM, invAl, demora, indicTiempoAux);
                                Parque.cargaCodigoAux(indiceEnAux, Parque.codigoDeIndAux(indiceEnAux) + parque.codigoDeInd(ind));
                            } else {
                                // Se terminó la demora y se agrega el recurso a pFin como operativo.                                                               
                                int indicTiempoAux = avanceConstruccion + avanceTiempo - demora;
                                int indiceEnAux = est.indiceEnPosibles(rb, EstadosRec.OPE, invAl, demora, indicTiempoAux);
                                Parque.cargaCodigoAux(indiceEnAux, Parque.codigoDeIndAux(indiceEnAux) + parque.codigoDeInd(ind));
                            }
                        }
                    } else {
                        // No se terminó el período normal de construcción
                        // Debe crearse un recurso con la construcción avanzada
                        int indicTiempoAux = avanceConstruccion + avanceTiempo;
                        int indiceEnAux = est.indiceEnPosibles(rb, EstadosRec.CON, rec.getValorInvAl(), 0, indicTiempoAux);
                        Parque.cargaCodigoAux(indiceEnAux, Parque.codigoDeIndAux(indiceEnAux) + parque.codigoDeInd(ind));
                    }
                } else if (estado.equalsIgnoreCase(EstadosRec.DEM)) {
                    /**
                     * El recurso está demorado
                     */
                    int avanceConstruccion = rec.getIndicadorTiempo();
                    if (avanceConstruccion + avanceTiempo >= 0) {
                        // Se completa la construcción
                        int indicTiempoAux = avanceConstruccion + avanceTiempo;
                        int indiceEnAux = est.indiceEnPosibles(rb, EstadosRec.OPE, rec.getValorInvAl(), rec.getValorDemAl(), indicTiempoAux);
                        Parque.cargaCodigoAux(indiceEnAux, Parque.codigoDeIndAux(indiceEnAux) + parque.codigoDeInd(ind));
                    } else {
                        // El recurso queda demorado con la demora restante
                        int indicTiempoAux = avanceConstruccion + avanceTiempo;
                        int indiceEnAux = est.indiceEnPosibles(rb, EstadosRec.DEM, rec.getValorInvAl(), rec.getValorDemAl(), indicTiempoAux);
                        Parque.cargaCodigoAux(indiceEnAux, Parque.codigoDeIndAux(indiceEnAux) + parque.codigoDeInd(ind));
                    }
                }

            }

        }
        // Copia los códigos de los Recursos no comunes de cBloquesAux a cBloques
        for (int i = 0; i < est.getCantRecNoComunes(); i++) {
            parque.cargaCodigo(i, Parque.codigoDeIndAux(i));
        }
    }
}
