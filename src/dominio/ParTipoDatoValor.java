/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;

/**
 *
 * @author ut469262
 */
public class ParTipoDatoValor implements Serializable{
    private TipoDeDatos tipoDatos;
    private String valor;
    /**
     * Es un valor simbólico de la variablable asociada al TipoDeDatos o el valor fijo
     * asociado al tipo si el tipo de datos no es aleatorio
     */

    public ParTipoDatoValor(TipoDeDatos tipoDatos, String valor) {
        this.tipoDatos = tipoDatos;
        this.valor = valor;
    }

    public ParTipoDatoValor() {
    }
    

    public TipoDeDatos getTipoDatos() {
        return tipoDatos;
    }

    public void setTipoDatos(TipoDeDatos tipoDatos) {
        this.tipoDatos = tipoDatos;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    /**
     * Crea una copia del ParTipoDatoValor corriente cuyo valor puede modificarse sin
     * cambiar el original; ATENCION, no se puede alterar el TipoDeDatos.
     */
    public ParTipoDatoValor copiaParTDValor(){

        ParTipoDatoValor pCopia = new ParTipoDatoValor();
        pCopia.setTipoDatos(this.getTipoDatos());
        pCopia.setValor(this.valor);


        return pCopia;

    }

     @Override
     public boolean equals(Object obj) {
          if (obj == null) {
               return false;
          }
          if (getClass() != obj.getClass()) {
               return false;
          }
          final ParTipoDatoValor other = (ParTipoDatoValor) obj;
          if (this.tipoDatos != other.tipoDatos && (this.tipoDatos == null || !this.tipoDatos.equals(other.tipoDatos))) {
               return false;
          }
          if ((this.valor == null) ? (other.valor != null) : !this.valor.equals(other.valor)) {
               return false;
          }
          return true;
     }

     @Override
     public int hashCode() {
          int hash = 5;
          hash = 71 * hash + (this.tipoDatos != null ? this.tipoDatos.hashCode() : 0);
          hash = 71 * hash + (this.valor != null ? this.valor.hashCode() : 0);
          return hash;
     }

    


    
}
