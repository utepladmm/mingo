/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import AlmacenSimulaciones.ImplementadorGeneral;
import GrafoEstados.ConjRestGrafoE;
import GrafoEstados.Objetivo;
import Restricciones3.LectorRestE;
import TiposEnum.DependenciaDeT;
import TiposEnum.EstadosRec;
import TiposEnum.ExistEleg;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import naturaleza3.CargaGrafoN2;
import naturaleza3.GrafoN;
import naturaleza3.VariableNat;
import persistenciaMingo.CargaCambiosAleatorios;
import persistenciaMingo.CargaDatosGenerales;
import persistenciaMingo.CargaDemandas;
import persistenciaMingo.CargaExpaForzada;
import persistenciaMingo.CargaFuentes;
import persistenciaMingo.CargaObjetivo;
import persistenciaMingo.CargaRecBase;
import persistenciaMingo.CargaTiposDeDatoEst;
import persistenciaMingo.CargaTransBase;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class Estudio implements Serializable {

    private String nombre;
    private Date fechaCreacion;  // hay que ver la clase Date para usarla
   /**
     * Directorio donde se encuentran los datos y resultados del Estudio
     * y los subdirectorios de cada Caso. ATENCIÓN: NO ES dirBase sino un
     * subdirectorio de él.
     */
    private String directorio;   
    private DatosGeneralesEstudio datosGenerales;
    private Objetivo objEst;
    /**
     * Lista con los casos del estudio
     */
    private ArrayList<Caso> casos;
    /**
     * Conjunto de tipos de dato
     */
    private ConjTiposDeDato conjTiposDatos; 
    /**
     * Guarda los valores de los TipoDeDatos fijos usados por defecto en los Casos
     * si en los mismos no se establecen otros.
     */
    private ArrayList<ParTipoDatoValor> valoresTDFijosEst; 
    private ConjDemandas conjDemandas;
    private ConjFuentes conjFuentes;
    private ConjCambiosAleat conjCambAleat;
    private ConjRecB conjRecB;
    /**
     * Son los recursos base que son elegibles
     */
    private ConjRecB conjRecBEleg; 
    private ConjTransB conjTransB;
    private ConjTransB conjTransBEleg;
     /**
     * Las anteriores son las listas de Recursos NO COMUNES y Transformaciones que pueden existir,
     * en la combinación de todos los posibles EstadosRec:
     * Recurso:
     * CON:
     * -uno por cada indicadorTiempo (avance de construcción)
     * DEM:
     * -uno por cada indicadorTiempo (avance de construcción)
     * -uno por cada valorInvAl posible si admite inversión aleatoria
     * -uno por cada valorDemAl posible si admite demora aleatoria de construcción 
     * OPE:
     * -uno por cada indicadorTiempo (edad) posible si dependen de la edad
     * -uno por cada valorInvAl posible si admite inversión aleatoria
     * -uno por cada valorDemAl posible si admite demora aleatoria de construcción
     * Transformacion
     * CON:
     * -uno por cada indicadorTiempo (avance de construcción)
     * DEM:
     * -uno por cada indicadorTiempo (avance de construcción)
     * -uno por cada valorInvAl posible si admite inversión aleatoria
     * -uno por cada valorDemAl posible si admite demora aleatoria de construcción 
     * FALL
     * -uno por cada valorInvAl posible si admite inversión aleatoria
     * -uno por cada valorDemAl posible si admite demora aleatoria de construcción
     * TERMIN
     * -uno por cada valorInvAl posible si admite inversión aleatoria
     * -uno por cada valorDemAl posible si admite demora aleatoria de construcción      
     * 
     */
  
    private ArrayList<RecOTrans> recNoComunesYTransPosibles; 
    private int cantRecNoComunesYTransPosibles;
    private int cantRecNoComunes;
   /**
     * Grafo de la naturaleza original
     */
    private GrafoN grafoNPasos; 
    /**
     * Grafo de la naturaleza pasado a etapas
     */
    private GrafoN grafoN; 
     /**
     * ExpansionForzada es el conjunto de recursos existentes o ya
     * decididos que no son objeto de cambio en el estudio, es decir son un
     * dato y no una variable de control
     *
     */
    private Expansion expansionForzadaEstudio; 
    private ConjRestGrafoE conjRestSigInicio;
    private ConjRestGrafoE conjRestSigParque;
    /**
     * Es el nombre del archivo con la BolsaDeSimulaciones que emplea el Estudio
     */
    private String archBolsaSimul;  
    /**
     * OJO HAY QUE DEFINIR EL CONJUNTO DE SIMULACIONES DEL ESTUDIO
     * @param nombre
     */
    public Estudio(String nombre, int cantPasos, ImplementadorGeneral impGen) {
        this.nombre = nombre;
        directorio = impGen.getDirExpansion() + "/Estudio";
//		this.fechaCreacion = fechaCreacion;
        conjTiposDatos = impGen.getConjTD();
        datosGenerales = new DatosGeneralesEstudio();
        casos = new ArrayList<Caso>();
        conjDemandas = new ConjDemandas(this);
        valoresTDFijosEst = new ArrayList<ParTipoDatoValor>();
        conjFuentes = new ConjFuentes();
        conjRecB = new ConjRecB();
        conjTransB = new ConjTransB();
        conjRecBEleg = new ConjRecB();
        conjTransBEleg = new ConjTransB();
        conjCambAleat = new ConjCambiosAleat();
        grafoN = new GrafoN();
        expansionForzadaEstudio = new Expansion("Expansión por defecto", this, cantPasos);
        conjRestSigInicio = new ConjRestGrafoE();
        conjRestSigParque = new ConjRestGrafoE();
        recNoComunesYTransPosibles = new ArrayList<RecOTrans>();         
    }

    public Estudio() {
    }

    public Objetivo getObjEst() {
        return objEst;
    }

    public void setObjEst(Objetivo objEst) {
        this.objEst = objEst;
    }

    public String getDirectorio() {
        return directorio;
    }

    public void setDirectorio(String directorio) {
        this.directorio = directorio;
    }

    public ConjTiposDeDato getConjTiposDatos() {
        return conjTiposDatos;
    }

    public void setConjTiposDatos(ConjTiposDeDato conjTiposDatos) {
        this.conjTiposDatos = conjTiposDatos;
    }

    public ArrayList<ParTipoDatoValor> getValoresTDFijosEst() {
        return valoresTDFijosEst;
    }

    public void setValoresTDFijosEst(ArrayList<ParTipoDatoValor> valoresTDFijosEst) {
        this.valoresTDFijosEst = valoresTDFijosEst;
    }

    public ConjDemandas getConjDemandas() {
        return conjDemandas;
    }

    public void setConjDemandas(ConjDemandas conjDemandas) {
        this.conjDemandas = conjDemandas;
    }

    public ConjRecB getConjRecB() {
        return conjRecB;
    }

    public void setConjRecB(ConjRecB conjRecB) {
        this.conjRecB = conjRecB;
    }

    public ConjTransB getConjTransB() {
        return conjTransB;
    }

    public void setConjTransB(ConjTransB conjTransB) {
        this.conjTransB = conjTransB;
    }

    public ConjCambiosAleat getConjCambAleat() {
        return conjCambAleat;
    }

    public void setConjCambAleat(ConjCambiosAleat conjCambAleat) {
        this.conjCambAleat = conjCambAleat;
    }

    public DatosGeneralesEstudio getDatosGenerales() {
        return datosGenerales;
    }

    public void setDatosGenerales(DatosGeneralesEstudio datosGenerales) {
        this.datosGenerales = datosGenerales;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public GrafoN getGrafoN() {
        return grafoN;
    }

    public void setGrafoN(GrafoN grafoN) {
        this.grafoN = grafoN;
    }

    public ConjTransB getConjTransBEleg() {
        return conjTransBEleg;
    }

    public void setConjTransBEleg(ConjTransB conjTransBEleg) {
        this.conjTransBEleg = conjTransBEleg;
    }

    public GrafoN getGrafoNPasos() {
        return grafoNPasos;
    }

    public void setGrafoNPasos(GrafoN grafoNPasos) {
        this.grafoNPasos = grafoNPasos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Expansion getExpansionForzadaEstudio() {
        return expansionForzadaEstudio;
    }

    public void setExpansionForzada(Expansion expansionForzadaEstudio) {
        this.expansionForzadaEstudio = expansionForzadaEstudio;
    }

    public ArrayList<Caso> getCasos() {
        return casos;
    }

    public void setCasos(ArrayList<Caso> casos) {
        this.casos = casos;
    }

    public ConjRecB getConjRecBEleg() {
        return conjRecBEleg;
    }

    public void setConjRecBEleg(ConjRecB conjRecBEleg) {
        this.conjRecBEleg = conjRecBEleg;
    }

    public ConjFuentes getConjFuentes() {
        return conjFuentes;
    }

    public void setConjFuentes(ConjFuentes conjFuentes) {
        this.conjFuentes = conjFuentes;
    }

    public ConjRestGrafoE getConjRestSigInicio() {
        return conjRestSigInicio;
    }

    public void setConjRestSigInicio(ConjRestGrafoE conjRestSigInicio) {
        this.conjRestSigInicio = conjRestSigInicio;
    }

    public ConjRestGrafoE getConjRestSigParque() {
        return conjRestSigParque;
    }

    public void setConjRestSigParque(ConjRestGrafoE conjRestSigParque) {
        this.conjRestSigParque = conjRestSigParque;
    }

    public String getArchBolsaSimul() {
        return archBolsaSimul;
    }

    public void setArchBolsaSimul(String archBolsaSimul) {
        this.archBolsaSimul = archBolsaSimul;
    }

    public int getCantRecNoComunesYTransPosibles() {
        return cantRecNoComunesYTransPosibles;
    }

    public void setCantRecNoComunesyTransPosibles(int cantRecNoComunesPosibles) {
        this.cantRecNoComunesYTransPosibles = cantRecNoComunesPosibles;
    }

    public int getCantRecNoComunes() {
        return cantRecNoComunes;
    }

    public void setCantRecNoComunes(int cantRecNoComunes) {
        this.cantRecNoComunes = cantRecNoComunes;
    }

    

    public ArrayList<RecOTrans> getRecNoComunesYTransPosibles() {
        return recNoComunesYTransPosibles;
    }

    public void setRecNoComunesYTransPosibles(ArrayList<RecOTrans> recNoComunesYTransPosibles) {
        this.recNoComunesYTransPosibles = recNoComunesYTransPosibles;
    }

    
    public TiempoAbsoluto tiempoAbsolutoDeEtapa(int etapa) throws XcargaDatos{
        
        TiempoAbsoluto ta = datosGenerales.tiempoAbsolutoDeEtapa(etapa);
        return ta;
    }
 
    public TiempoAbsoluto tiempoAbsolutoDePaso(int paso) throws XcargaDatos{
        TiempoAbsoluto ta = datosGenerales.tiempoAbsolutoDeEtapa(paso);
        return ta;
        
    }
    
    public boolean getComContiguosSimple(){
        return datosGenerales.getCombContiguosSimple();
    }
    
    public void seComContiguosSimple(boolean simple){
        datosGenerales.setCombContiguosSimple(simple);
        return;
    }  
    
    public int cantRecBaseEleg(){
        return conjRecBEleg.getRecursosB().size(); 
    }    

    public int cantTransBaseEleg(){
        return conjTransBEleg.getTransforsB().size(); 
    }  
    /**
     * METODO leerEstudio
     *
     * Lee los datos del estudio corriente para dejarlos
     * prontos para las pruebas de otras clases.
     * Incluso reduce el grafo de la naturaleza al grafo por etapas.
     * Es un método provisorio para permitir la prueba
     * de las restantes clases
     *
     * Obsérvese que se están cargando las variables y objetos
     * miembro del estudio
     *
     * El directorio donde busca los datos es un subdirectorio del
     * dirEstudios del ImplemetadorGeneral con el mismo nombre del Estudio
     */
    public void leerEstudio(boolean imprimir) throws IOException, XcargaDatos {

        CargaDatosGenerales cargadorDG = new CargaDatosGenerales() {
        };
        CargaFuentes cargaF = new CargaFuentes();
        CargaRecBase cargadorRB = new CargaRecBase() {
        };
        CargaTransBase cargadorTB = new CargaTransBase() {
        };
        CargaDemandas cargadorDem = new CargaDemandas() {
        };
        CargaExpaForzada cargadorED = new CargaExpaForzada() {
        };
        CargaGrafoN2 cargadorGN = new CargaGrafoN2() {
        };
        CargaCambiosAleatorios cargadorCA = new CargaCambiosAleatorios() {
        };

        /**
         * El orden en que se leen los datos es relevante
         * datosGenerales debe leerse antes que grafoN
         * grafoN debe leerse antes que los conjuntos de recursos, demandas, etc.
         * conjRecB debe leerse antes que conjTransB
         * conjRecB y conjTransB deben leerse antes que expansionForzada
         */
        /**
         * --------------------------------------
         *  Lee los datos generales del estudio
         * --------------------------------------
         */
        String archDatGen = directorio + "/" + "V5-DatosGenerales.txt";
        cargadorDG.cargarDatosGen(archDatGen, datosGenerales);
        TiempoAbsoluto tini = new TiempoAbsoluto();
        tini = datosGenerales.getInicioEstudio();
        int largo = datosGenerales.getCantPasos();
        datosGenerales.setFinEstudio(
                datosGenerales.getCalculadorDePlazos().hallaTiempoAbsoluto(tini, largo - 1));

        /**
         * -------------------------------------
         * Lee el objetivo por defecto del estudio
         * -------------------------------------
         */
        boolean objEstudio = true;
        CargaObjetivo.cargarObj(archDatGen, null, this, objEstudio);

        /**
         * ------------------------------------------------------
         * Lee grafoNpasos que es el grafo de la naturaleza para
         * todos los pasos de tiempo.
         * ------------------------------------------------------
         */
        String dirGrafoN = directorio + "/" + "V13-GrafoNaturaleza.txt";
        boolean impDet = false;
        int cantPasos = datosGenerales.getCantPasos();

        grafoNPasos = GrafoN.creaYLeeGrafoN(dirGrafoN, cantPasos, impDet);

//          grafoNPasos = cargadorGN.cargarGrafoN(dirGrafoN,
//                  cantPasos, impDet);

        /**
         * Pasa a etapas el grafoNpasos
         * En grafoN queda el grafo reducido a etapas
         */
        grafoN = grafoNPasos.pasaAGrafoEtapas(datosGenerales);
//          grafoN = grafoNPasos;


        /**
         * ------------------------------------------------------
         * Lee los valores de tipos de dato del estudio que requiere
         * el implementador
         * ------------------------------------------------------
         */
        String dirTiposDeDatos = directorio + "/" + "tiposDeDatosEst.txt";
        CargaTiposDeDatoEst.cargarTiposDato(dirTiposDeDatos, conjTiposDatos, this);

//        /**
//        * Calcula para todos los NodosN los valores de TipoDeDatos
//        */
//        grafoN.cargaValoresTiposDatos(this);
        /**
         * ----------------------------
         * Lee las fuentes del estudio
         * ----------------------------
         */
        String dirFuentes = directorio + "/" + "V2-Fuentes.txt";
        cargaF.cargarConjFuentes(dirFuentes, conjFuentes);


        /**
         * -----------------------------
         * Lee las demandas del estudio
         * -----------------------------
         */
        String dirDatDem = directorio + "/" + "V4-Demandas.txt";
        cargadorDem.cargarConjDemandas(dirDatDem, conjDemandas,
                datosGenerales, grafoNPasos);
        /**
         * Verifica que ninguna demanda aleatoria tiene una variable de la naturaleza
         * no observable en el grafoN
         */
        for (Demanda dem : conjDemandas.getDemandas()) {
            if (dem.isAleatoria() && !(dem.getVAleatoria().getObservadaEnOptim())) {
                throw new XcargaDatos("La demanda " + dem.getNombre() + "es aleatoria y "
                        + "su variable de la naturaleza es no observada");
            }
        }



        /**
         * ---------------------------------
         * Lee los recursosBase del estudio
         * ---------------------------------
         */
        String dirRecBas = directorio + "/" + "V12-RecursosBase.txt";
        cargadorRB.cargarConjRecBase(dirRecBas, conjRecB, this);
        // Ordena lexicograficamente los RecursosBase
        conjRecB.ordenaConjunto();        
        for (RecursoBase rb : conjRecB.getRecursosB()) {
            if (rb.getClase().equalsIgnoreCase(ExistEleg.ELEG)) {
                conjRecBEleg.getRecursosB().add(rb);
            }
        }


        /**
         * Verifica que existe el RecursoBase de nombre nombreEquivEnSimul
         * para los RecusosBase que tienen equivalente en simulación
         * y lo carga.
         * VERIFICA QUE AMBOS SEAN ELEGIBLES
         */
        ArrayList<RecursoBase> tienenEquiv = new ArrayList<RecursoBase>();
        for (RecursoBase rb : conjRecB.getRecursosB()) {
            if (rb.tieneEquivEnSimul()) {
                if (!rb.getClase().equalsIgnoreCase(ExistEleg.ELEG)) throw new XcargaDatos("El recurso "
                        + rb.getNombre() + " no es EXIST y tiene equivalente en simulación");
                tienenEquiv.add(rb);
            }
        }
        for (RecursoBase rb : tienenEquiv) {
            RecursoBase equiv = conjRecB.getUnRecB(rb.getNombreEquivEnSimul());
            if(!equiv.getClase().equalsIgnoreCase(ExistEleg.ELEG))throw new XcargaDatos("El recurso "
                        + rb.getNombre() + " no es EXIST y es equivalente de otro en simulación");
            rb.setEquivEnSimul(equiv);
        }

        /**
         * ---------------------------------------
         * Lee los cambios aleatorios del estudio
         * ---------------------------------------
         */
        String dirCambAl = directorio + "/" + "V2-CambiosAleatorios.txt";
        cargadorCA.cargarConjCambiosAleat(dirCambAl, conjCambAleat, conjRecB, grafoN);
        /**
         * Verifica que los cambios aleatorios tienen variable de la naturaleza asociada
         * con el nombre correcto en el grafoN y deja indicados los recursosBase
         * que admiten cambios aleatorios.
         * 
         * Carga en los RecursosBase involucrados la indicación de que admiten
         * CambioAleatorio
         * 
         */
        for (CambioAleatorio cA : conjCambAleat.getCambios()) {
            String nomVNCA = cA.getNombre() + "_CAL";
            grafoN.devuelveVN(nomVNCA);
            // si no existe la variableNat hay una excepción que termina la ejecución
            for (Recurso rec : cA.getRecursosOrigen()) {
                RecursoBase rb = rec.getRecBase();
                rb.setAdmiteCambioAleatorio(true);
            }
        }


        /**
         * -----------------------------
         * Lee las transformacionesBase
         * -----------------------------
         */
        String dirTraBas = directorio + "/" + "V6-TransformacionesBase.txt";
        cargadorTB.cargarConjTransBase(dirTraBas, conjTransB, conjRecB, conjCambAleat, this);
        for (TransforBase tb : conjTransB.getTransforsB()) {
            for (Recurso rec : tb.getRecursosTransitorios()) {
                if (rec.getRecBase().getClase().equalsIgnoreCase(ExistEleg.EXIST)) {
                    throw new XcargaDatos("La transformación "
                            + tb.getNombre() + " tiene un RecursoBase transitorio" + rec.getRecBase().getNombre() + "que no es "
                            + "ELEG ni TRANS");
                }
            }
            for (Recurso rec : tb.getRecursosOrigen()) {
                RecursoBase rb = rec.getRecBase();
                rb.setEsOrigenTB(true);
            }
            if (tb.getClase().equalsIgnoreCase(ExistEleg.ELEG)) {
                conjTransBEleg.getTransforsB().add(tb);
            }
        }
        // Ordena lexicograficamente las TransforBase
        conjTransB.ordenaConjunto();

        /**
         * -------------------------------------------------------------------
         * Verificaciones cruzadas sobre variables de la naturaleza de GrafoN
         * -------------------------------------------------------------------
         */
        /**
         * Verifica que las demoras aleatorias y montos aleatorios de inversión y costo fijo de
         * los RecursosBase tienen variable de la naturaleza asociada con el nombre adecuado
         * y que los valores simbólicos String en el GrafoN y en el RecursoBase coinciden.
         * Carga los valores numéricos respectivos en los RecursosBase.
         * Si se cumple la verificación se cargan las variables aleatorias de cada tipo en
         * los RecursosBase
         */
        ArrayList<Double> auxD; 
        ArrayList<Integer> auxI;
        String nom;
        for (RecursoBase rb : conjRecB.getRecursosB()) {
            if (rb.getTieneDemoraAl()) {
                nom = rb.getNombre() + "_DEMAL";
                // si no existe la variableNat hay una excepción que termina la ejecución
                VariableNat vN = grafoN.devuelveVN(nom);
                if (!(vN.getValores().containsAll(rb.getValoresDemAlS()))
                        || !(rb.getValoresDemAlS().containsAll(vN.getValores()))) {

                    throw new XcargaDatos("No coinciden los valores de demora aleatoria del "
                            + "RecursoBase " + rb.getNombre());
                }
                rb.setVNDemoraAl(vN);
                auxI = new ArrayList<Integer>();
                for(String s: rb.getValoresDemAlS()){
                    auxI.add(Integer.parseInt(s));
                }
                rb.setValoresDemAl(auxI);
            }
            if (rb.getTieneInvAleat()) {
                nom = rb.getNombre() + "_INVAL";
                // si no existe la variableNat hay una excepción que termina la ejecución
                VariableNat vN = grafoN.devuelveVN(nom);
                if (!(vN.getValores().containsAll(rb.getValoresInvAlS()))
                        || !(rb.getValoresInvAlS().containsAll(vN.getValores()))) {
                    throw new XcargaDatos("No coinciden los valores de inversión aleatoria del "
                            + "RecursoBase " + rb.getNombre());
                }
                rb.setVNInvAl(vN);
                auxD = new ArrayList<Double>();
                for(String s: rb.getValoresInvAlS()){
                    auxD.add(Double.parseDouble(s));
                } 
                rb.setValoresInvAl(auxD);
            }
            if (rb.getTieneCFijAleat()) {
                nom = rb.getNombre() + "_CFIAL";
                // si no existe la variableNat hay una excepción que termina la ejecución
                VariableNat vN = grafoN.devuelveVN(nom);
                if (!(vN.getValores().containsAll(rb.getValoresCFijAlS()))
                        || !(rb.getValoresCFijAlS().containsAll(vN.getValores()))) {
                    throw new XcargaDatos("No coinciden los valores de inversión aleatoria del "
                            + "RecursoBase " + rb.getNombre());
                }
                rb.setVNCFijAl(vN);
                auxD = new ArrayList<Double>();
                for(String s: rb.getValoresCFijAlS()){
                    auxD.add(Double.parseDouble(s));
                } 
                rb.setValoresCFijAl(auxD);                
            }
        }

        /**
         * Verifica que las demoras aleatorias y montos aleatorios de inversión de
         * las TransforBase tienen variable de la naturaleza asociada con el nombre adecuado
         * y carga los valores numéricos en la TransforBase
         */
        for (TransforBase tb : conjTransB.getTransforsB()) {
            if (tb.getTieneDemoraAl()) {
                nom = tb.getNombre() + "_DEMAL";
                VariableNat vN = grafoN.devuelveVN(nom);
                // si no existe la variableNat hay una excepción que termina la ejecución
                if (!(vN.getValores().containsAll(tb.getValoresDemAlS()))
                        || !(tb.getValoresDemAlS().containsAll(vN.getValores()))) {
                    throw new XcargaDatos("No coinciden los valores de demora aleatoria del "
                            + "RecursoBase " + tb.getNombre());
                }
                tb.setVNDemoraAl(vN);                
                auxI = new ArrayList<Integer>();
                for(String s: tb.getValoresDemAlS()){
                    auxI.add(Integer.parseInt(s));
                }
                tb.setValoresDemAl(auxI);                
            }
            if (tb.getTieneInvAleat()) {
                nom = tb.getNombre() + "_INVAL";
                // si no existe la variableNat hay una excepción que termina la ejecución
                VariableNat vN = grafoN.devuelveVN(nom);
                if (!(vN.getValores().containsAll(tb.getValoresInvAlS()))
                        || !(tb.getValoresInvAlS().containsAll(vN.getValores()))) {
                    throw new XcargaDatos("No coinciden los valores de inversión aleatoria del "
                            + "RecursoBase " + tb.getNombre());
                }
                tb.setVNInvAl(vN);
                auxD = new ArrayList<Double>();
                for(String s: tb.getValoresInvAlS()){
                    auxD.add(Double.parseDouble(s));
                } 
                tb.setValoresInvAl(auxD);                

            }
//               if (tb.getTieneInvAleat()) {
//                    nom = tb.getNombre() + "_CFI";
//                    grafoN.devuelveVN(nom);
//                    // si no existe la variableNat hay una excepción que termina la ejecución
//               }
        }

        /**
         * -------------------------------------
         * Carga el atributo "comun" a los RecursosBase
         * 
         * Si se admite que los cambios aleatorios son propios del Caso
         * va a haber que hacer esto en leerCaso y para cada Caso
         * -------------------------------------
         */
        for (RecursoBase rb : conjRecB.getRecursosB()) {
            if (rb.getClase().equalsIgnoreCase(ExistEleg.EXIST) && rb.getAdmiteCambioAleatorio() == false
                    && rb.esOrigenTB() == false) {
                rb.setEsComun(true);
            } else {
                rb.setEsComun(false);
            }
        } 
        /**
         * -------------------------------------
         * Verifica que los RecursoBase y TransforBase elegibles de DatosGenerales existen
         * -------------------------------------
         */
        
        
        /**
         * -------------------------------------
         * Lee la expansión forzada del estudio
         * -------------------------------------
         */
        String dirExpDef = directorio + "/" + "V7-expansionForzadaEstudio.txt";
        cargadorED.cargarExpDef(dirExpDef, this, conjRecB,
                conjTransB, expansionForzadaEstudio);



        /**
         * -----------------------------------------------------------
         * Lee y publica las restricciones significativas del estudio
         * -----------------------------------------------------------
         */
        String dirRest = directorio + "/";
        String nomArRParque = "V3-RestSigParque.txt";
        String nomArRInicio = "V3-RestSigInicio.txt";
        LectorRestE.cargaConjuntoR(conjRestSigParque,
                dirRest + nomArRParque, conjRecB, conjTransB, this);
        LectorRestE.cargaConjuntoR(conjRestSigInicio,
                dirRest + nomArRInicio, conjRecB, conjTransB, this);
        LectorRestE.publicaConjRest(dirRest + "volcadoRestSigInicio.txt",
                "Rest sig inicio", conjRestSigInicio);
        LectorRestE.publicaConjRest(dirRest + "volcadoRestSigParque.txt",
                "Rest sig parque", conjRestSigParque);
        // Un estudio no tiene restricciones heurísticas propias

        /**
         * Carga los datos de la clase padre RestGrafoE
         * y pasa las restricciones de paso a etapas OJOJOJOJOJO
         */
      
        /**
         * ---------------------------------------
         * Crea la lista con un ejemplar de cada uno de los Recursos 
         * NO COMUNES y Transformaciones posibles. 
         * ATENCIÓN, REQUIERE QUE YA SE HAYA CARGADO LA INDICACION
         * DE RecursosBase COMUNES.
         * ---------------------------------------         
         */
        creaRecYTransNoComunesPosibles();
             
        /**
         * ---------------------------------------
         * Escribe el estudio leído en la consola
         * ---------------------------------------
         */
        if (imprimir) {
            String texto = this.toString();
            System.out.print(texto);
        }
    }

    /**
     * METODO leerEstudio
     *
     * Lee los datos del estudio corriente para dejarlos
     * prontos para las pruebas de otras clases.
     * Incluso reduce el grafo de la naturaleza al grafo por etapas.
     * Es un método provisorio para permitir la prueba
     * de las restantes clases
     *
     * Obsérvese que se están cargando las variables y objetos
     * miembro del estudio
     *
     * @param directorio es un String con el path del directorio raíz del Estudio
     * llamado dirBase en general
     */
    public void leerEstudio(String directorio, boolean imprimir) throws IOException, XcargaDatos {
        this.directorio = directorio + "/Estudio";
        leerEstudio(imprimir);
    }
    
     /**
     * Crea las listas de Recursos y Transformaciones
     * NO COMUNES del Estudio:
     * - recNoComunesPosibles
     * - transPosibles
     * Incluye un ejemplar de cada Recurso y Transformacion 
     * que pueden existir.
     * Marca la posición de inicio del Recurso o Transformacion
     * en el código de Parque de los Recursos y Transformaciones NO COMUNES.
     * 
     * ATENCIÓN, REQUIERE QUE YA SE HAYA CARGADO LA INDICACION
     * DE RecursosBase y TransforBase COMUNES.
     */
    public void creaRecYTransNoComunesPosibles() throws XcargaDatos{
       
        ArrayList<Integer> posiblesDemAl;
        ArrayList<Double> posiblesInvAl;         
        ArrayList<Integer> tomadosDemAl;
        ArrayList<Double> tomadosInvAl;        
        ArrayList<Integer> tomadosIndTiempo = null;
        TiempoAbsoluto taini = datosGenerales.getInicioEstudio();

        /**
         * ----------------------------------
         * Recursos: SE INCLUYEN SÓLO LOS QUE TIENEN RecursoBase NO COMUNES
         * ----------------------------------
         */
        ArrayList<String> casosEstadosRec = new ArrayList<String>();
        // EL ORDEN EN QUE APARECEN LOS RECURSOS ESTA DADO POR LOS ADD SIGUIENTES
        casosEstadosRec.add(EstadosRec.CON);
        casosEstadosRec.add(EstadosRec.DEM);                
        casosEstadosRec.add(EstadosRec.OPE);        
        
        int indice = 0;
        
        for(RecursoBase rb: conjRecB.getRecursosB()){
            if(!rb.esComun()){
                rb.setPosicionRecursoInicial(indice);
                boolean iniciaOPE = true;
                posiblesDemAl = new ArrayList<Integer>();
                posiblesInvAl = new ArrayList<Double>();                    
                tomadosDemAl = new ArrayList<Integer>();
                tomadosInvAl = new ArrayList<Double>();       
                // carga casos posibles de demora aleatoria incorporada a un RB ya construído
                int demoraMax = 0;
                if(rb.getTieneDemoraAl()==true){
                    for(int ic=0; ic<rb.getValoresDemAl().size(); ic++){
                        posiblesDemAl.add(rb.getValoresDemAl().get(ic));
                    }
                    // Encuentra el mayor valor de demora entre los posibles
                    demoraMax = 0;
                    for(int id=0; id<rb.getValoresDemAl().size(); id++){
                        if(posiblesDemAl.get(id)>demoraMax){
                            demoraMax = posiblesDemAl.get(id);
                        }
                    }                                
                }else{
                    posiblesDemAl.add(0);
                }
                // carga casos posibles de inversion aleatoria incorporada a un RB ya construído            
                if(rb.getTieneInvAleat()==true){
                    for(int ic=0; ic<rb.getValoresInvAl().size(); ic++){
                        posiblesInvAl.add(rb.getValoresInvAl().get(ic));
                    }
                }else{
                    posiblesInvAl.add(1.0);
                }

                for(String st: casosEstadosRec){                                    
                    tomadosIndTiempo = new ArrayList<Integer>();                
                    if(st.equalsIgnoreCase(EstadosRec.CON)){
                        /**
                         * Un recurso en construcción se carga por defecto
                         * con demora 0 y valor de inversión aleatoria 1.
                         * Cuando se complete su construcción esos valores pueden cambiar.
                         */
                        tomadosDemAl = new ArrayList<Integer>();
                        tomadosDemAl.add(0);
                        tomadosInvAl = new ArrayList<Double>();                                            
                        tomadosInvAl.add(1.0);
                        for(int it=-rb.getPerConstr(); it<0;it++){
                            tomadosIndTiempo.add(it);
                        }                        
                    }else if(st.equalsIgnoreCase(EstadosRec.DEM)){
                        if(rb.getTieneDemoraAl()==true){
                            tomadosDemAl = posiblesDemAl;
                            for(int it=-demoraMax; it<0;it++){
                                tomadosIndTiempo.add(it);
                            }
                            tomadosInvAl = posiblesInvAl;
                        }else{
                           // No hay ningún caso de demora 
                        }
                    }else if(st.equalsIgnoreCase(EstadosRec.OPE)){
                        tomadosDemAl = posiblesDemAl;
                        tomadosInvAl = posiblesInvAl;                                    
                        if(rb.getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.EDAD)){
                            for(int it=0; it<rb.getVidaUtil();it++){
                                tomadosIndTiempo.add(it);
                            }
                        }else{
                            tomadosIndTiempo.add(0);
                        }
                    }else throw new XcargaDatos("error en EstadosRec de Recurso");                       

                    /**                 
                     * Crea la lista de recursos combinando exhaustivamente
                     */
                    for(Integer it: tomadosIndTiempo){
                        for(Integer da: tomadosDemAl){
                            for(Double ci: tomadosInvAl){                               
                                Recurso rNue = new Recurso(rb, st, it);
                                rNue.setValorDemAl(da);
                                rNue.setValorInvAl(ci);
                                /**
                                 * Para los recursos que no dependen del paso de tiempo se calcula
                                 * la oferta de productos
                                 */                            
                                if(!rb.getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.PASO_DE_TIEMPO)){
                                    for(Producto pr: datosGenerales.getProductos()){
                                        // Se toma el tiempoAbsolutoBase para calcular la oferte de productos
                                        rNue.getCantsProd().add(rNue.cantProdSimp(pr, datosGenerales, rb.getTiempoAbsolutoBase()));
                                    }
                                }
                                recNoComunesYTransPosibles.add(rNue);                            
//                                if(st.equalsIgnoreCase(EstadosRec.CON) && it==rb.getPerConstr()
//                                        && rb.getClase().equalsIgnoreCase(ExistEleg.ELEG)){
//                                    rb.setPosicionRecursoInicial(indice);
//                                }
                                System.out.println(rNue.toStringMuyCorto(false));
                                // carga la posición del Recurso OPE nuevo.
                                if(st.equalsIgnoreCase(EstadosRec.OPE) &
                                   iniciaOPE){
                                    rb.setPosicionRecursoOPE(indice);
                                    iniciaOPE = false;
                                }
                                indice ++;
                            }                            
                        }                               
                    }
                }  

                cantRecNoComunes = recNoComunesYTransPosibles.size();

            }
        }

        /**
         * -------------------------------------------------
         * Transformaciones: SE INCLUYEN TODAS
         * -------------------------------------------------
         */
        casosEstadosRec = new ArrayList<String>();
        // El orden en que aparecen está dado por los add siguientes.
        casosEstadosRec.add(EstadosRec.CON);
        casosEstadosRec.add(EstadosRec.DEM);
        casosEstadosRec.add(EstadosRec.FALL);                
        casosEstadosRec.add(EstadosRec.TERMIN);                        
        for(TransforBase tb: conjTransBEleg.getTransforsB()){
            tb.setPosicionTransformacionInicial(indice);
            posiblesDemAl = new ArrayList<Integer>();
            posiblesInvAl = new ArrayList<Double>();                    
            tomadosDemAl = new ArrayList<Integer>();
            tomadosInvAl = new ArrayList<Double>();       
            // carga casos posibles de demora aleatoria incorporada a una TB ya construída
            int demoraMax = 0;
            if(tb.getTieneDemoraAl()==true){
                for(int ic=0; ic<tb.getValoresDemAl().size(); ic++){
                    posiblesDemAl.add(tb.getValoresDemAl().get(ic));
                }
                // Encuentra el mayor valor de demora entre los posibles
                demoraMax = 0;
                for(int id=0; id<tb.getValoresDemAl().size(); id++){
                    if(posiblesDemAl.get(id)>demoraMax){
                        demoraMax = posiblesDemAl.get(id);
                    }
                }                                
            }else{
                posiblesDemAl.add(0);
            }
            // carga casos posibles de inversion aleatoria incorporada a un RB ya construído            
            if(tb.getTieneInvAleat()==true){
                for(int ic=0; ic<tb.getValoresInvAl().size(); ic++){
                    posiblesInvAl.add(tb.getValoresInvAl().get(ic));
                }
            }else{
                posiblesInvAl.add(1.0);
            }
               
            for(String st: casosEstadosRec){                                    
                tomadosIndTiempo = new ArrayList<Integer>();                
                if(st.equalsIgnoreCase(EstadosRec.CON)){
                    tomadosDemAl = new ArrayList<Integer>();
                    tomadosDemAl.add(0);
                    tomadosInvAl = new ArrayList<Double>();                                            
                    tomadosInvAl.add(0.0);
                    for(int it=-tb.getPerConstr(); it<0;it++){
                        tomadosIndTiempo.add(it);
                    }
                }else if(st.equalsIgnoreCase(EstadosRec.DEM)){
                    // Este caso es EstadosRec.DEM
                    if(tb.getTieneDemoraAl()==true){
                        tomadosDemAl = posiblesDemAl;
                        for(int it=-demoraMax; it<0;it++){
                            tomadosIndTiempo.add(it);
                        }
                        tomadosInvAl = posiblesInvAl;
                    }else{
                       // No hay ningún caso de demora 
                    }   
                }else{
                    /**
                     * Casos EstadosRec.FALL y TERMIN
                     * El tratamiento es el mismo de los Recursos OPE para demora aleatoria
                     * e inversión aleatoria
                     */
                    tomadosDemAl = posiblesDemAl;
                    tomadosInvAl = posiblesInvAl;                                    
                    tomadosIndTiempo.add(0);
                    
                }                       
                            
                /**
                 * Crea la lista de Transformaciones combinando exhaustivamente
                 */
                for(Integer it: tomadosIndTiempo){
                    for(Integer da: tomadosDemAl){
                        for(Double ci: tomadosInvAl){
                            Transformacion tNue = new Transformacion(tb, st, it);
                            tNue.setValorDemAl(da);
                            tNue.setValorInvAl(ci);
 
                            recNoComunesYTransPosibles.add(tNue);                            
//                            if(st.equalsIgnoreCase(EstadosRec.CON) && it==tb.getPerConstr()
//                                    && tb.getClase().equalsIgnoreCase(ExistEleg.ELEG)){
//                                tb.setPosicionTransformacionIniciaConstMax(indice);
//                            }
                            System.out.println(tNue.toStringMuyCorto(false));
                            indice ++;
                        }                            
                    }                               
                }
            }            
        }
        cantRecNoComunesYTransPosibles = recNoComunesYTransPosibles.size();   
        
        /**
         *  Crea cBloquesAux
         */ 
        Parque.creaCBloquesAux(cantRecNoComunesYTransPosibles);
    }

    
    /**
     * Devuelve el RecOTrans que se encuentra en el lugar i-ésimo de 
     * recNoComunesYTransPosibles
     */
    public RecOTrans recNoComunYTransPosible(int i){
        return recNoComunesYTransPosibles.get(i);
    }
    
    
    /**
     * Devuelve el indice (comenzando en cero) en recNoComunesYTransPosibles del
     * Recurso o Transformacion recOTrans
     * y si no lo encuentra devuelve -1
     * @param recOTrans es un RecOTrans.
     * @return el indice del Recurso rec y si lo encuentra -1.
     */
    public int indiceEnPosibles(RecOTrans recOTrans) throws XcargaDatos{
        int indice = recNoComunesYTransPosibles.indexOf(recOTrans);
        if(indice<0) throw new XcargaDatos(" No existe el Recurso o Transformacion " + recOTrans.toStringMuyCorto(false));
        return indice;
    }
     
    /**
     * Devuelve el indice (comenzando en cero) en recNoComunesYTransPosibles del
     * Recurso o Transformacion definido por sus datos y si no lo encuentra devuelve -1
     * @param rot es un RecursoBase o TransforBase pasado como Object
     * @param estado es una constante de la clase EstadosRec (OPE, .....)
     * @param valorInvAl es un valor de inversión aleatoria
     * @param valorDemAl es un valor de demora aleatoria
     * @param indicadorTiempo es un valor de edad, demora, etc, según sea el estado
     */
    public int indiceEnPosibles(Object rot, String estado,
            double valorInvAl, int valorDemAl, int indicadorTiempo){
        int indice = -1;
        boolean coincide = false;
        for(int ind=0; ind<recNoComunesYTransPosibles.size();ind++){
            RecursoBase rb;
            TransforBase tb;
            RecOTrans rotPosible = recNoComunesYTransPosibles.get(ind);
            if(rot instanceof RecursoBase){
                rb = (RecursoBase)rot;
                if( rotPosible.esDeRB(rb)  &&
                    estado.equalsIgnoreCase(rotPosible.getEstado()) &&
                    valorInvAl == rotPosible.getValorInvAl() &&
                    valorDemAl == rotPosible.getValorDemAl() &&
                    indicadorTiempo == rotPosible.getIndicadorTiempo() ){
                    indice = ind;
                    coincide = true;
                }                
            }else{
                tb = (TransforBase)rot;          
                if( rotPosible.esDeTB(tb)  &&
                    estado.equalsIgnoreCase(rotPosible.getEstado()) &&                        
                    valorInvAl == rotPosible.getValorInvAl() &&
                    valorDemAl == rotPosible.getValorDemAl() &&
                    indicadorTiempo == rotPosible.getIndicadorTiempo() ){
                    indice = ind;
                    coincide = true;
                }                                
            }
            if(coincide) break;                              
        }       
        return indice;
    }


    /**
     * Devuelve el primer índice en recNoComunesYTransPosibles en el que hay un
     * RecursoBase rb con estado est
     * @param rb
     * @param estado
     * @return 
     */
    public int primerIndiceEnPosibles(RecursoBase rb, String est) throws XcargaDatos{
        
        for(int ind=0; ind<recNoComunesYTransPosibles.size(); ind++){
            RecOTrans rot = recNoComunesYTransPosibles.get(ind);
            if ( rot.esDeRB(rb)  && rot.getEstado().equalsIgnoreCase(est) ){
                return ind;
            }
        }
        throw new XcargaDatos("No se encontró RecursoBase " 
                + rb.getNombre()  +  "con estado " + est + " en " + recNoComunesYTransPosibles);
    }
    

    @Override
    public String toString() {
        String texto = "==================================================================" + "\n";
        texto += "COMIENZA DESCRIPCION DEL ESTUDIO" + "\n";
        texto += "===================================================================" + "\n";
        texto += "Nombre: " + nombre + "\n";
        texto += datosGenerales.toString() + "\n";
        texto += conjTiposDatos.toString() + "\n";
        texto += conjFuentes.toString() + "\n";
        texto += conjRecB.toStringCorto() + "\n";
        texto += conjTransB.toStringCorto() + "\n";
        texto += grafoN.toString() + "\n";
        texto += expansionForzadaEstudio.toString() + "\n";
        texto += conjRestSigInicio.toString() + "\n";
        texto += conjRestSigParque.toString() + "\n";
        texto += "RECURSOS ELEGIBLES POSIBLES" + "\n";
        for(RecOTrans r: recNoComunesYTransPosibles){
            texto += r.toStringMuyCorto(true) + "\n";
        }       
        texto += "DESCRIPCION DE CASOS" + "\n";                
        texto += casos.toString() + "\n";
        texto += "FIN DE DESCRIPCION DEL ESTUDIO" + "\n";
        texto += "===================================================================" + "\n";
        return texto;
    }
}
