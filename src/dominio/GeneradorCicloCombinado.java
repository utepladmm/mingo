package dominio;

import java.io.Serializable;
import java.util.ArrayList;

public class GeneradorCicloCombinado extends Generador implements Serializable{
	
    private String mintec; // S o P (semanal o por poste)
    private double potenciaMintec; // potencia mínimo técnico de un poste
    private double dpotan;  // reducción anual de potencia nominal no se usa
    private double rendPotenciaNom; // rendimiento a potencia nominal
    private double rendPotenciaMin; // idem a potencia mínimo técnico
    private double drenan; // derateo anual de rendimiento no se usa ahora
    private double disponibilidad; //disponibilidad por tranche
	private ArrayList<Combustible> combustibles; // lista ordenada de combustibles que puede usar
	private double dPotCS; // derateo de potencia por usar combustible secundario
	private double dRenCS; // derateo de rendimiento por usar combustible secundario
	private double cOYMV; // costo variable de OyM en US$/MWh
	private double cOYMF; // costo fijo de OyM en US$/MWh
	
	// Cantidad de m�dulos en mantenimiento por semana del a�o, tiene 52 valores
	private ArrayList<ArrayList<String>> manten;
	public static final int CANTSEM = 52;
	
	
	public GeneradorCicloCombinado() {
        combustibles = new ArrayList<Combustible>();
        
        manten = new ArrayList<ArrayList<String>>();
	}
	
	
	
	public String getMintec() {
		return mintec;
	}
	public void setMintec(String mintec) {
		this.mintec = mintec;
	}
	public double getPotenciaMintec() {
		return potenciaMintec;
	}
	public void setPotenciaMintec(double potenciaMintec) {
		this.potenciaMintec = potenciaMintec;
	}
	public double getDpotan() {
		return dpotan;
	}
	public void setDpotan(double dpotan) {
		this.dpotan = dpotan;
	}
	public double getRendPotenciaNom() {
		return rendPotenciaNom;
	}
	public void setRendPotenciaNom(double rendPotenciaNom) {
		this.rendPotenciaNom = rendPotenciaNom;
	}
	public double getRendPotenciaMin() {
		return rendPotenciaMin;
	}
	public void setRendPotenciaMin(double rendPotenciaMin) {
		this.rendPotenciaMin = rendPotenciaMin;
	}
	public double getDrenan() {
		return drenan;
	}
	public void setDrenan(double drenan) {
		this.drenan = drenan;
	}
	public double getDisponibilidad() {
		return disponibilidad;
	}
	public void setDisponibilidad(double disponibilidad) {
		this.disponibilidad = disponibilidad;
	}
	public ArrayList<Combustible> getCombustibles() {
		return combustibles;
	}
	public void setCombustibles(ArrayList<Combustible> combustibles) {
		this.combustibles = combustibles;
	}
	public double getdPotCS() {
		return dPotCS;
	}
	public void setdPotCS(double dPotCS) {
		this.dPotCS = dPotCS;
	}
	public double getdRenCS() {
		return dRenCS;
	}
	public void setdRenCS(double dRenCS) {
		this.dRenCS = dRenCS;
	}
	public double getcOYMV() {
		return cOYMV;
	}
	public void setcOYMV(double cOYMV) {
		this.cOYMV = cOYMV;
	}
	public double getcOYMF() {
		return cOYMF;
	}
	public void setcOYMF(double cOYMF) {
		this.cOYMF = cOYMF;
	}
	public ArrayList<ArrayList<String>> getManten() {
		return manten;
	}
	public void setManten(ArrayList<ArrayList<String>> manten) {
		this.manten = manten;
	}
	public static int getCantsem() {
		return CANTSEM;
	}


	
	

}
