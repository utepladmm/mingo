/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut600232
 */
public class ListaReales implements Serializable{

    private String nombre;
    private ArrayList<Double> listaValores;

    public ListaReales() {
        setNombre("");
        listaValores = new ArrayList<Double>();
    }

    public ListaReales(String nombr){
        setNombre(nombr);
        listaValores = new ArrayList<Double>();
    }

    public void setNombre(String nombr){
        nombre = nombr;
    }

    public void addValor(Double valor){
        listaValores.add(valor);
    }

    public void setValores(ArrayList<Double> valores){
        int j;
        for( j = 0; j < valores.size(); j++ ){
            addValor(valores.get(j));
        }
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public Double getValor(int posicion){
        return listaValores.get(posicion);
    }

    public ArrayList<Double> getValores(){
        return listaValores;
    }

    public int getDimension(){
        return listaValores.size();
    }

    @Override
    public String toString(){
        String texto = "";
        int j;
        texto += nombre;
        for(j = 0; j < listaValores.size(); j++){
            texto += " \t" + listaValores.get(j);
        }
        return texto;
    }

}
