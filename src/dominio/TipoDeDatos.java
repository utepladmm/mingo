/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;
import naturaleza3.VariableNat;

/**
 *
 * @author ut469262
 * Un TipoDeDatos es un tipo de datos que incide sobre las simulaciones del
 * modelo de operación empleado (por ejemplo: combustibles).
 */
public class TipoDeDatos implements Serializable{
    private String nombre;
    /**
     * Es el nombre de un tipo de datos.
     */
    private boolean aleat;
    /**
     * Indica si el tipo de datos es aleatorio y está asociado a una variable de la
     * naturaleza del GrafoN del MINGO, o por el contrario tiene asociado un String
     * correspondiene a un valor simbólico, o dirección de archivo.
     */
    private VariableNat vNDelDato;
    /**
     * Es la variable de la naturaleza asociada al tipo de datos, o null si no
     * hay variable aleatoria asociada.
     */
//    private String valorDelDato = "";
    /**
     * Es el valor fijo asociado al tipo de datos cuando el tipo no
     * está asociado a una variable aleatoria o el String vacío si
     * hay variable aleatoria asociada al tipo.
     * Un tipo de datos asociado a un valor fijo puede emplearse cuando ese valor fijo
     * es la dirección en la red del sitio donde se buscan los datos en todo el estudio.
     */
    private String direccion;
    /**
     * Es la dirección (en principio el directorio) debajo de la cual se almacenan
     * los datos de este tipo utilizados por el MINGO y en general por el modelo
     * de operación que se está implementando.
     */
     

    public boolean esAleat() {
        return aleat;
    }

    public void setAleat(boolean aleat) {
        this.aleat = aleat;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Devuelve la VariableNat del TipoDeDatos this, si es aleatorio.
     * @return
     */
    public VariableNat getVNDelDato() {
        return vNDelDato;
    }

    public void setVNDelDato(VariableNat vNDelDato) {
        this.vNDelDato = vNDelDato;
    }

     @Override
     public boolean equals(Object obj) {
          if (obj == null) {
               return false;
          }
          if (getClass() != obj.getClass()) {
               return false;
          }
          final TipoDeDatos other = (TipoDeDatos) obj;
          if ((this.nombre == null) ? (other.nombre != null) : !this.nombre.equals(other.nombre)) {
               return false;
          }
          if (this.aleat != other.aleat) {
               return false;
          }
          if (this.vNDelDato != other.vNDelDato && (this.vNDelDato == null || !this.vNDelDato.equals(other.vNDelDato))) {
               return false;
          }
          if ((this.direccion == null) ? (other.direccion != null) : !this.direccion.equals(other.direccion)) {
               return false;
          }
          return true;
     }

     @Override
     public int hashCode() {
          int hash = 7;
          hash = 89 * hash + (this.nombre != null ? this.nombre.hashCode() : 0);
          hash = 89 * hash + (this.aleat ? 1 : 0);
          hash = 89 * hash + (this.vNDelDato != null ? this.vNDelDato.hashCode() : 0);
          hash = 89 * hash + (this.direccion != null ? this.direccion.hashCode() : 0);
          return hash;
     }

//    /**
//     * Devuelve el valor del dato asociado el TipoDeDatos this
//     * si no es aleatorio sino fijo.
//     * @return
//     */
//    public String getValorDelDato() {
//        return valorDelDato;
//    }
//
//    public void setValorDelDato(String valorDelDato) {
//        this.valorDelDato = valorDelDato;
//    }
    
    







    

    @Override
    public String toString(){
        String texto = "TIPO DE DATOS: " + "\n";
        texto += "nombre = " + nombre + "\n";
        texto += "aleat = " + aleat + "\n";
        if( aleat ) {
            texto += "variable de la naturaleza = " + vNDelDato.getNombre();
        }
//        }else {
//            texto += "valor fijo del dato = " + valorDelDato + "\n";
//        }
        return texto;
    }
}
