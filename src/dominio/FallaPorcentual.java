/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class FallaPorcentual extends Falla implements Serializable{
	private int cantEscalones;
	private ArrayList<ArrayList<Double>> anchoEscalones;
	private ArrayList<ArrayList<Double>> costoEscalones;
	/** 
	 * Para las dos variables vale: 
	 * el primer índice corresponde al ordinal del escalón
	 * el segundo índice a paso de tiempo a partir del paso 1
	 * Los valores son:
	 *  ancho (profundidad) de escalones en fracción
	 *  costo en US$/MWh respectivamente
	 */


	public FallaPorcentual() {
		super();
		anchoEscalones = new ArrayList<ArrayList<Double>>();
		costoEscalones = new ArrayList<ArrayList<Double>>();

	}

	public int getCantEscalones() {
		return cantEscalones;
	}

	public void setCantEscalones(int cantEscalones) {
		this.cantEscalones = cantEscalones;
	}

	

	public ArrayList<ArrayList<Double>> getAnchoEscalones() {
		return anchoEscalones;
	}

	public void setAnchoEscalones(ArrayList<ArrayList<Double>> anchoEscalones) {
		this.anchoEscalones = anchoEscalones;
	}

	public ArrayList<ArrayList<Double>> getCostoEscalones() {
		return costoEscalones;
	}

	public void setCostoEscalones(ArrayList<ArrayList<Double>> costoEscalones) {
		this.costoEscalones = costoEscalones;
	}

	@Override
	public String toString() {
		String texto ="";
		texto += super.toString()+ "\n";

		texto += "Cantidad de escalones de falla: " + cantEscalones + "\n";

		texto += "Ancho de escalones como fracción de la demanda por paso de tiempo" + "\n";
		for (int i=0; i<= cantEscalones-1; i++){
			texto += "Escalón " + (i+1) + ":   ";
			for (int j=0; j<anchoEscalones.get(i).size(); j++){
				texto += anchoEscalones.get(i).get(j);
				texto += "   ";
			}
			texto += "\n";
		}

		texto += "Costo de escalones por paso de tiempo en US$/MWh" + "\n";
		for (int i=0; i<= cantEscalones-1; i++){
			texto += "Escalón " + (i+1) + ":   ";
			for (int j=0; j<costoEscalones.get(i).size(); j++){
				texto += costoEscalones.get(i).get(j);
				texto += "   ";
			}
			texto += "\n";
		}
		return texto;


	}







}


