/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.util.ArrayList;
import naturaleza3.VariableNat;

/**
 * Carga y devuelve objetos para distintos pasos de tiempo absolutos
 * a partir de uno base
 * para un valor simbólico dado de una variable aleatoria
 * @author ut469262
 */
public class EscenarioObjeto {
	
	private VariableNat varN;
        /**
         * valor de la variable varN
         */
	private String valorVN; 
	private TiempoAbsoluto tiempoAbsolutoBase;
	private ArrayList<Object> objetos;

	public EscenarioObjeto(VariableNat varN, String valorVN, TiempoAbsoluto tiempoAbsolutoBase) {
		this.varN = varN;
		this.valorVN = valorVN;
		this.tiempoAbsolutoBase = tiempoAbsolutoBase;
		objetos = new ArrayList<Object>();
	}

	public ArrayList<Object> getObjeto() {
		return objetos;
	}

	public void setObjeto(ArrayList<Object> objeto) {
		this.objetos = objetos;
	}

	public TiempoAbsoluto getTiempoAbsolutoBase() {
		return tiempoAbsolutoBase;
	}

	public void setTiempoAbsolutoBase(TiempoAbsoluto tiempoAbsolutoBase) {
		this.tiempoAbsolutoBase = tiempoAbsolutoBase;
	}

	public String getValorVN() {
		return valorVN;
	}

	public void setValorVN(String valorVN) {
		this.valorVN = valorVN;
	}

	public VariableNat getVarN() {
		return varN;
	}

	public void setVarN(VariableNat varN) {
		this.varN = varN;
	}
        /**
	 * Agrega un objeto al final del arrayList
	 */
	public void agregaObjeto (Object obj){
		
		assert (obj != null) : "El objeto " + obj.toString() + " es null";
		objetos.add(obj);


	}






}
