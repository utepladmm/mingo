/* Representa el tiempo absoluto
 * Son períodos
 */
package dominio;

import GrafoEstados.PeriodoDeTiempo;
import UtilitariosGenerales.CalculadorDePlazos;
import java.io.Serializable;
import java.util.Calendar;

/**
 *
 * @author ut536300
 */
public class TiempoAbsoluto extends PeriodoDeTiempo implements Serializable {

     private int anio; //Año absoluto
     private int parteDelAnio;

     /**
      * En caso que un año tenga un solo paso de tiempo, parteDelAnio tiene 1
      * @param anio
      * @param parteDelAnio
      *
      * En texto el usuario especifica un TiempoAbsoluto como dato de entrada
      * en el formato aaaa.pp
      * aaaa es el año, como 2014
      * pp es el ordinal de la parteDelAnio como uno o dos caracteres
      * Ejemplos:    2014.2  o bien 2014.11
      *
      */
     public TiempoAbsoluto(int anio, int parteDelAnio, Calendar fechaIni, Calendar fechaFin) {
          super(fechaIni, fechaFin);
          this.anio = anio;
          this.parteDelAnio = parteDelAnio;
     }

     public TiempoAbsoluto(int anio) {
          this.anio = anio;
          this.parteDelAnio = 1;
     }

     public TiempoAbsoluto() {
          super();
          parteDelAnio = 1;
     }

     /**
      * Constructor a partir de una fecha en el formato "aaaa:p"
      * aaaa es el año y p es la parte del año.
      */
     public TiempoAbsoluto(String st) {
          // st es un string que representa un tiempo absoluto en texto
          anio = Integer.parseInt(st.substring(0, 4));
          if (st.length() > 4) {
               parteDelAnio = Integer.parseInt(st.substring(5));
          } else {
               parteDelAnio = 1;
          }
     }

     public int getAnio() {
          return anio;
     }

     public void setAnio(Integer anio) {
          this.anio = anio;
     }

     public int getParteDelAnio() {
          return parteDelAnio;
     }

     public void setParteDelAnio(Integer parteDelAnio) {
          this.parteDelAnio = parteDelAnio;
     }

     /**
      * Crea una copia del TiempoAbsoluto this que puede cambiarse sin alterar el original
      * @return
      */
     public TiempoAbsoluto copiaTiempoAbsoluto() {
          TiempoAbsoluto copia = new TiempoAbsoluto(anio, parteDelAnio, inicio, fin);
          return copia;
     }

     /**
      * Avanza en una cantidad npasos de pasos de tiempo el TiempoAbsoluto this
      */
     public void avanzaNPasos(int npasos, CalculadorDePlazos calcP) {
          calcP.avanzaTiempoAbsoluto(this, npasos);
     }

     @Override
     public boolean equals(Object obj) {
          if (obj == null) {
               return false;
          }
          if (getClass() != obj.getClass()) {
               return false;
          }
          final TiempoAbsoluto other = (TiempoAbsoluto) obj;
          if (this.anio != other.anio) {
               return false;
          }
          if (this.parteDelAnio != other.parteDelAnio) {
               return false;
          }
          return true;
     }

     @Override
     public int hashCode() {
          int hash = 7;
          hash = 37 * hash + this.anio;
          hash = 37 * hash + this.parteDelAnio;
          return hash;
     }

     @Override
     public String toString() {
          String texto = "Tiempo absoluto: anio " + anio;
          texto += "." + "parte del anio " + parteDelAnio + "\r\n";
          return texto;
     }

    @Override
     public String toStringCorto() {
          String texto = "";
          texto += anio;
          texto += "." + parteDelAnio + "\r\n";
          return texto;
     }

     public static void main(String[] args) {
          TiempoAbsoluto ta;
          String st = "2014";
          ta = new TiempoAbsoluto(st);
          System.out.print(ta.toString());
          st = "2014.1";
          ta = new TiempoAbsoluto(st);
          System.out.print(ta.toString());
     }
}
