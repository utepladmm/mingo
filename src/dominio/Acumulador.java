package dominio;

import java.io.Serializable;

public class Acumulador extends Generador implements Serializable{
	
	
	private String ref;
    /**
     * Es el nombre del archivo, sin extensi�n ni path donde est�n los datos
     * No se usa en implementaci�n MOP.
     */

	public Acumulador(String ref) {
		this.ref = ref;
	}

	public Acumulador() {
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	@Override
	public String toString() {
				String texto ="";
		texto += super.toString() + "\n";
		texto += "Nombre referencia para archivos " + ref + "\n";

        return texto;
	}



}
