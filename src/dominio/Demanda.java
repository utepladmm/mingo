/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import AlmacenSimulaciones.ImplementadorGeneral;
import ImplementaEDF.ImplementadorEDF;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.GrafoN;
import naturaleza3.VariableNat;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class Demanda implements Serializable{
    private String nombre;
    private Punto puntoConex;
    private String tipo;
    private String subtipo1;
    private String subtipo2;
    private String tipofalla;
     /**
     *  true si el escenario de demanda depende de una variable aleatoria definida en el GrafoN
     *  Si la demanda es aleatoria tiene asociado una variable de la naturaleza
     *  cuyo nombre está determinado por el nombre de la demanda con sufijo _DEM
     *  y debe aparecer así en el grafo de la naturaleza.
     */
    private boolean aleatoria;
    /**
     *  Variable aleatoria que se lee en el grafo de la naturaleza
     *  asociada a la demanda.
     *  Si la demanda no es aleatoria igual se define la variable con un valor único en valoresVA.
     *  El  nombre de la vAleatoria es el de la demanda con sufijo "_DEM".
     */
    private VariableNat vAleatoria;
   /**
     *  Estos son los valores simbólicos de la VA asociada a esta demanda
     *  tal como se leen del archivo de demanda y deben chequearse contra los
     *  del grafo de la naturaleza
     *
     *  En la implementación con el modelo de operación EDF
     *  si la demanda es no aleatoria valoresVA tiene un valor único QUE DEBE COINCIDIR
     *  CON EL STRING del campo valor del ParTipoDatoValor cuyo tipo de dato es el de nombre
     *  "demanda". El implementadorEDF tiene un método que verifica que el valor único leído
     *  en la demanda y el campo del ParTipoDatoValor de nombre demanda coincidan.
     */
    private ArrayList<String> valoresVA;
     /**
     * Tiempo absoluto del primer dato de demanda por tiempo absoluto
     * de demEnergia.
     *
     */
    private TiempoAbsoluto tiempoAbsolutoBase;
    /** La serie de valores de demanda por tiempo absoluto en energía
     *  El primer índice es el indicador de nombre simbólico de la variable aleatoria
     *  en el orden en que aparecen en valoresVA
     *  El segundo índice es paso de tiempo
     *  En el get(0) está el valor del tiempoAbsolutoBase y luego los siguientes tiempos absolutos.
     *  ATENCION: SI EN UN PASO DE TIEMPO NO ESTA DEFINIDO
     *  UN VALOR SIMBOLICO, IGUAL HAY QUE RELLENAR EL VALOR CON ALGO
     *
     *  Si la demanda no es aleatoria está en el get(0) del primer Array
     *
     * ATENCION:
     * LOS DATOS DE DEMANDA DE ENERGÍA QUE LEE EL OBJETO DEMANDA TIENEN COMO ÚNICO
     * OBJETO CALCULAR LAS CANTIDADES DE PRODUCTO DE LA DEMANDA.
     * NO SE EMPLEAN PARA CREAR LOS DATOS DE LAS SIMULACIONES.
     * ESTO ES UNA ANOMALÍA QUE HACE QUE HAYA QUE GUARDAR COHERENCIA ENTRE LOS DATOS
     * DE DEMANDA DE ENERGÍA DEL MINGO, QUE SON LEÍDOS POR ESTE OBJETO Y LOS DEL MODELO DE
     * OPERACIÓN
     */
    private ArrayList<ArrayList<Double>> demEnergia;
    /**
     * Número por el que se multiplica la demanda de energía por paso de tiempo
     * para obtener el requerimiento de cada producto.
     * Hay tantas entradas como productos.
     * El indice de pasoAProductos recorre los Productos del Estudio.
     * El índice dentro de la ListaReales es paso de tiempo (sin contar el paso cero).
     * en el get(0) está el valor en el tiempoAbsolutoBase
     * Si no hay suficientes valorse de pasos de tiempo para cubrir los datos de demanda
     * de energía, se toman los datos del último paso de tiempo con valor cargado.
     *
     */
    private ArrayList<ListaReales> pasoAProductos;
    /**
     * Cantidades de productos requeridas por la demanda para cada
     * producto y valor de la variable aleatoria
     * El primer índice es indicador de producto.
     * El segundo índice es el indicador de valor de variable aleatoria
     */
    private ArrayList<ArrayList<EscenarioCantProducto>> cantsDeProductos;
    /**
     * Coeficientes para pasar de GWh de energía anual a
     * MW de potencia firme por subpaso (p.ej. semana) dentro el paso
     * Hay un valor para cada uno de cantSubpasos subpasos dentro
     * del paso
     */
    private ArrayList<Double> energiaFirmeManten;  
    private Falla falla;

    public Demanda(String nombre, Punto puntoConex, ArrayList<ArrayList<Double>> demEnergia,
            ArrayList<ArrayList<Double>> pasoAProductos, ArrayList<Double> energiaFirmeManten) {
        this.nombre = nombre;
        this.puntoConex = puntoConex;
        valoresVA = new ArrayList<String>();
        demEnergia = new ArrayList<ArrayList<Double>>();
        pasoAProductos = new ArrayList<ArrayList<Double>>();
        energiaFirmeManten = new ArrayList<Double>();
        cantsDeProductos = new ArrayList<ArrayList<EscenarioCantProducto>>();
    }

    public Demanda() {
        valoresVA = new ArrayList<String>();
        demEnergia = new ArrayList<ArrayList<Double>>();
        pasoAProductos = new ArrayList<ListaReales>();
        energiaFirmeManten = new ArrayList<Double>();
        cantsDeProductos = new ArrayList<ArrayList<EscenarioCantProducto>>();
    }

    public boolean isAleatoria() {
        return aleatoria;
    }

    public void setAleatoria(boolean aleatoria) {
        this.aleatoria = aleatoria;
    }

    public TiempoAbsoluto getTiempoAbsolutoBase() {
        return tiempoAbsolutoBase;
    }

    public void setTiempoAbsolutoBase(TiempoAbsoluto tiempoAbsolutoBase) {
        this.tiempoAbsolutoBase = tiempoAbsolutoBase;
    }

    public ArrayList<ArrayList<Double>> getDemEnergia() {
        return demEnergia;
    }

    /**
     *   Devuelve una demanda si esta no está sujeta a aleatoriedad.
     *   Devuelve una demanda negativa -1.0 si hay error
     *   y la demanda de energía no ha sido especificada
     *   para el tiempo absoluto ta
     */
    public double demEnergiaDeTiempoAbsoluto(TiempoAbsoluto ta, DatosGeneralesEstudio dGE)
            throws XcargaDatos {

        if (aleatoria == true) {
            throw new XcargaDatos("Se invocó demEnergíaDeTiempoAbsoluto " +
                    "para demandas no aleatorias y la demanda " + nombre + " es aleatoria");
        }
        double energia = -1.0;
        int avance = dGE.getCalculadorDePlazos().hallaAvanceTiempo(
                tiempoAbsolutoBase, ta);
        assert (avance >= 0) : "En demanda " + nombre + " error en avance de tiempo " +
                " - tiempo absoluto: " + ta.toString();
        energia = demEnergia.get(0).get(avance);
        return energia;
    }

    /**
     * METODO demEnergiaDeTiempoAbsoluto
     * se usa para demandas aleatorias o no aleatorias
     *
     * Devuelve una demanda si está sujeta a aleatoriedad
     * Lanza una excepción si la demanda de energía no ha sido especificada
     * para el tiempo absoluto ta
     * 
     * @param ta es el tiempo absoluto del que se quiere la demanda
     * de energía
     * 
     * @param dGE son los datos generales del estudio
     *
     * @param valor VA es el valor simbólico de la variable aleatoria
     * asociada a la demanda; si la demanda no es aleatoria se ignora y se toma
     * "valor_unico"
     * 
     * @return energia es la demanda en energia del tiempoAbsoluto ta
     */
    public double demEnergiaDeTiempoAbsoluto(TiempoAbsoluto ta,
            DatosGeneralesEstudio dGE, String valorVA) throws XcargaDatos {
        if (aleatoria == false) {
            valorVA = "valor_unico";
        }
        double energia = -1.0;
        int avance = dGE.getCalculadorDePlazos().hallaAvanceTiempo(
                tiempoAbsolutoBase, ta);
        assert (avance >= 0) : "En demanda " + nombre + " error en avance de tiempo " +
                " - tiempo absoluto: " + ta.toString();
        int indVA = valoresVA.indexOf(valorVA);
        if (indVA < 0) {
            throw new XcargaDatos("Error valor de variable aleatoria " +
                    "de demanda " + valorVA + " no reconocido");
        }
        if (avance >= demEnergia.get(indVA).size()) {
            throw new XcargaDatos("Faltan datos para la demanda " +
                    nombre + " , no hay datos para el paso de tiempo " + ta.toStringCorto());
        }
        energia = demEnergia.get(indVA).get(avance);
        return energia;
    }

    /**
     * Para cada producto y valor de la variable aleatoria
     * de la demanda, carga la cantidad de producto de la demanda
     * a partir de las energías y los coeficientes de paso a producto
     * de la demanda.
     */
    public void cargaCantProductos(DatosGeneralesEstudio datGen,
            GrafoN grafo) throws XcargaDatos {
        int indVA = 0;
        int indP = 0;
        int jcorr = 0;
        boolean encontro = false;
        for (Producto p : datGen.getProductos()) {
            ArrayList<EscenarioCantProducto> auxArrayECP = new ArrayList<EscenarioCantProducto>();
            for (String st : vAleatoria.getValores()) {
                EscenarioCantProducto eCP = new EscenarioCantProducto(
                        vAleatoria, st, p, tiempoAbsolutoBase);
                indVA = vAleatoria.getValores().indexOf(st);
                encontro = false;
                int indBuscaP = 0;
                do {
                    if (pasoAProductos.get(indBuscaP).getNombre().equalsIgnoreCase(p.getNombre())) {
                        encontro = true;
                    }
                    indBuscaP++;
                } while (!encontro & indBuscaP < pasoAProductos.size());
                if (!encontro) {
                    throw new XcargaDatos("Falta un " +
                            "coeficiente de paso a productos " + p.toString() + st);
                } else {
                    indBuscaP--;
                    for (int j = 0; j < demEnergia.get(indVA).size(); j++) {

                        /**
                         * Si no hay dato de pasoAProductos para el paso de tiempo se toma el valor
                         * del último paso de tiempo con dato
                         */
                        if (j >= pasoAProductos.get(indBuscaP).getDimension()) {
                            jcorr = pasoAProductos.get(indBuscaP).getDimension() - 1;
                        } else {
                            jcorr = j;
                        }
                        CantProductoSimple cps = new CantProductoSimple(p,
                                demEnergia.get(indVA).get(j) *
                                pasoAProductos.get(indBuscaP).getValor(jcorr));
                        eCP.getCantidades().add(cps);
                    }
                }
                auxArrayECP.add(eCP);
                indVA++;
            }
            cantsDeProductos.add(auxArrayECP);
            indP++;
        }
    }

    /**
     * METODO cantProd
     * Devuelve la cantidad de producto de una demanda en un tiempo absoluto
     * y para un valor de la variable aleatoria (o bien para el valor fijo leído con la demanda)
     * Se aplica tanto si la demanda es aleatoria como si no lo es
     */
    public CantProductoSimple cantProd(Producto pr,
            DatosGeneralesEstudio datGen,
            TiempoAbsoluto ta, String valorVA) throws XcargaDatos {


        int indVA=0;

        if (!aleatoria) {
            valorVA = "valor_unico";
        }else{
            indVA = valoresVA.indexOf(valorVA);
            if (indVA < 0) {
                throw new XcargaDatos("Error valor de variable aleatoria " +
                        "de demanda " + valorVA + " no reconocido");

            }                       

        }
        
        CantProductoSimple cant = new CantProductoSimple(pr);
        int i = 0;
        boolean encontrado = false;
        do {
            if (cantsDeProductos.get(i).get(indVA).getProducto() == pr) {
                cant.setProducto(pr);
                cant.setCant(cantsDeProductos.get(i).get(indVA).DevuelveCantProd(ta, datGen).getCant());
                encontrado = true;
            }
            i++;
        } while (!encontrado);
        return cant;
    }

//    /**
//     * METODO cantProd
//     * Devuelve la cantidad de producto de una demanda en un tiempo absoluto
//     * y para un valor de la variable aleatoria.
//     * Sólo se aplica si la demanda es no aleatoria
//     */
//    public CantProductoSimple cantProd (Producto pr,
//			DatosGeneralesEstudio datGen,
//			TiempoAbsoluto ta) throws XcargaDatos{
//
//        if(aleatoria) throw new XcargaDatos("Se usó la función cantProd de demandas" +
//                "no aleatorias y " + nombre + " es aleatoria");
//		CantProductoSimple cant = new CantProductoSimple(pr);
//		int i = 0;
//		int j = 0;
//		boolean encontrado = false;
//		do{
//			do{
//				j=0;
//				if(cantsDeProductos.get(i).get(j).getValorVN().equalsIgnoreCase("valor_unico")
//					& cantsDeProductos.get(i).get(j).getProducto()== pr){
//					cant.setProducto(pr);
//					cant.setCant(cantsDeProductos.get(i).get(j)
//							.DevuelveCantProd(ta, datGen).getCant());
//					encontrado = true;
//				}
//				j++;
//			}while (!encontrado);
//			i++;
//		}while(!encontrado);
//		return cant;
//	}
    public void setDemEnergia(ArrayList<ArrayList<Double>> demEnergia) {
        this.demEnergia = demEnergia;
    }

    public ArrayList<Double> getEnergiaFirmeManten() {
        return energiaFirmeManten;
    }

    public void setEnergiaFirmeManten(ArrayList<Double> energiaFirmeManten) {
        this.energiaFirmeManten = energiaFirmeManten;
    }

    public Falla getFalla() {
        return falla;
    }

    public void setFalla(Falla falla) {
        this.falla = falla;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<ListaReales> getPasoAProductos() {
        return pasoAProductos;
    }

    public void setPasoAProductos(ArrayList<ListaReales> pasoAProductos) {
        this.pasoAProductos = pasoAProductos;
    }

    public ArrayList<ArrayList<EscenarioCantProducto>> getCantsDeProductos() {
        return cantsDeProductos;
    }

    public void setCantsDeProductos(ArrayList<ArrayList<EscenarioCantProducto>> cantsDeProductos) {
        this.cantsDeProductos = cantsDeProductos;
    }

    public Punto getPuntoConex() {
        return puntoConex;
    }

    public void setPuntoConex(Punto puntoConex) {
        this.puntoConex = puntoConex;
    }

    public String getSubtipo1() {
        return subtipo1;
    }

    public void setSubtipo1(String subtipo1) {
        this.subtipo1 = subtipo1;
    }

    public String getSubtipo2() {
        return subtipo2;
    }

    public void setSubtipo2(String subtipo2) {
        this.subtipo2 = subtipo2;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipofalla() {
        return tipofalla;
    }

    public void setTipofalla(String tipofalla) {
        this.tipofalla = tipofalla;
    }

    public VariableNat getVAleatoria() {
        return vAleatoria;
    }

    public void setVAleatoria(VariableNat vAleatoria) {
        this.vAleatoria = vAleatoria;
    }

    public ArrayList<String> getValoresVA() {
        return valoresVA;
    }

    public void setValoresVA(ArrayList<String> valoresVA) {
        this.valoresVA = valoresVA;
    }

    @Override
    public String toString() {
        String texto = "====================================================" + "\n";
        texto += "INICIA DESCRIPCIÓN DEMANDA " + nombre + "\n";
        texto += "Tipo de demanda: " + tipo + "\n";
        texto += "Tipo de falla de la demanda: " + tipofalla + "\n";
        texto += "Demanda es aleatoria: " + aleatoria + "\n";
        texto += "Valores de la variable aleatoria de esta demanda: ";
        for (int i = 0; i < valoresVA.size(); i++) {
            texto += valoresVA.get(i) + "   ";
        }
        texto += "\n";
        texto += "Tiempo absoluto base: " + tiempoAbsolutoBase.toString();
        texto += "\n";
        for (int i = 0; i < valoresVA.size(); i++) {
            texto += "Energías por paso de tiempo para VA = " + valoresVA.get(i) + "  son: ";
            for (int j = 0; j < demEnergia.get(i).size(); j++) {
                texto += demEnergia.get(i).get(j) + "   ";
            }
            texto += "\n";
        }

        texto += "COEFICIENTES DE PASO A PRODUCTOS";
        texto += pasoAProductos.toString();
        texto += "\n";

        texto += "CANTIDADES DE PRODUCTOS" + "\n";
        for (int i = 0; i < cantsDeProductos.size(); i++) {
            for (int j = 0; j < cantsDeProductos.get(i).size(); j++) {
                texto += cantsDeProductos.get(i).get(j).toString();
                texto += "\n";
            }
        }


        texto += "Coeficientes para paso a potencia firme para mantenimientos por subpaso ";
        for (int i = 0; i < energiaFirmeManten.size(); i++) {
            texto += "subpaso" + (i + 1) + "=" + energiaFirmeManten.get(i) + "   ";
        }

        texto += "\n";
        texto += "FALLA ASOCIADA A LA DEMANDA";
        texto += "\n";
        texto += falla.toString();

        return texto;
    }

    public static void main(String[] args) throws XcargaDatos, IOException {
        String dirBase = "D:/Java/PruebaJava4";
        String dirEstudio = dirBase + "/" + "Datos_Estudio";
        String dirImplementacion = dirBase + "/" + "Datos_Implementacion";
        String dirCorridas = dirBase + "/" + "corridas";
        ImplementadorGeneral impG = new ImplementadorEDF();
        impG.inicializaImplementador(dirImplementacion, dirEstudio, dirCorridas);

        Estudio est = new Estudio("Estudio de prueba", 4, impG);
        String directorio = "D:/Java/PruebaJava3";
        est.leerEstudio(directorio, true);
        String texto = est.toString();
        System.out.print(texto);

        TiempoAbsoluto ta = new TiempoAbsoluto("2012");

        double demanda = est.getConjDemandas().getDemandas().get(0).demEnergiaDeTiempoAbsoluto(ta, est.getDatosGenerales());

        texto = "\n";
        System.out.print(texto);
        System.out.print("\n" + "La demanda en " + ta.toString() + " para variable de la naturaleza " + "valor_unico" +
                " es " + demanda);


        Producto pr = new Producto();
        pr = est.getDatosGenerales().devuelveProducto("potenciaFirme");
        CantProductoSimple cant = new CantProductoSimple(pr);

        cant = est.getConjDemandas().getDemandas().get(0).cantProd(pr, est.getDatosGenerales(), ta, "valor_unico");
        texto = cant.toString();
        System.out.print(texto);



    }
}
