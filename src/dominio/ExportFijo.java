/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class ExportFijo extends Exportacion implements Serializable{


    private String pais;
    /**
     * Es el nombre del archivo, sin extensión ni path donde están, que tienen
     * los archivos de potencia y costo unitario del generador eólico, cuyas extensiones
     * son .pot y .cos respectivamente.
     */
    private String ref;
   /**
     * Si refUnitario es true, la potencia en el archivo dado por ref
     * corresponde a 1 MW de potencia nominal.
     * Si refUnitario es false, la potencia en el archivo dato corresponde
     * a la potencia nominal potNominal del RecursoBase padre.
     */
    private boolean refUnitario;
    
    private int nest;
    private ArrayList<Integer> semFinEst;
	private ArrayList<Double> escEst;
	private ArrayList<Double> dispEst;

    public ExportFijo(String pais, String ref, boolean refUnitario, int nest, ArrayList<Integer> semFinEst, ArrayList<Double> escEst, ArrayList<Double> dispEst) {
        this.pais = pais;
        this.ref = ref;
        this.refUnitario = refUnitario;
        this.nest = nest;
        this.semFinEst = semFinEst;
        this.escEst = escEst;
        this.dispEst = dispEst;
    }

	public ExportFijo() {
	}

    public ArrayList<Double> getDispEst() {
        return dispEst;
    }

    public void setDispEst(ArrayList<Double> dispEst) {
        this.dispEst = dispEst;
    }

    public ArrayList<Double> getEscEst() {
        return escEst;
    }

    public void setEscEst(ArrayList<Double> escEst) {
        this.escEst = escEst;
    }

    public int getNest() {
        return nest;
    }

    public void setNest(int nest) {
        this.nest = nest;
    }

    public boolean isRefUnitario() {
        return refUnitario;
    }

    public void setRefUnitario(boolean refUnitario) {
        this.refUnitario = refUnitario;
    }

    public ArrayList<Integer> getSemFinEst() {
        return semFinEst;
    }

    public void setSemFinEst(ArrayList<Integer> semFinEst) {
        this.semFinEst = semFinEst;
    }



	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}



	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	@Override
	public String toString() {
		String texto ="";
		texto += super.toString() + "\n";
		texto += "pais: " + pais + "\n";
		texto += "referencia de los archivos fijos de potencia y costo" + ref + "\n";
		texto += "escalamiento (¿por estación?)" + escEst +"\n";
		texto += "disponibilidad (¿por estación?)" + dispEst;
        return texto;
	}
}

