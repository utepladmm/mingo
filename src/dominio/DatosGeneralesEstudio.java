/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import AlmacenSimulaciones.BolsaDeSimulaciones3;
import GrafoEstados.Numerario;
import UtilitariosGenerales.CalculadorDePlazos;
import UtilitariosGenerales.FCalendar;
import persistenciaMingo.CargaDatosGenerales;
import persistenciaMingo.XcargaDatos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author ut469262
 */
public class DatosGeneralesEstudio implements Serializable {

    private String nombreEstudio;
    private String modeloOp;
    private BolsaDeSimulaciones3 bolsaSimul;
    private int pasosPorAnio;   // Cantidad de pasos iguales por año
   /**
     *  cantidad de pasos de tiempo que se consideran,
     *  adicionales al paso cero
     *  los paso se numeran 0, 1, 2, ...... y tienen también un nombre
     */
    private int cantPasos;
    /**
     *  Cantidad de pasos agregados al final de la optimización en la estimación de costos
     *  Se repite el costo del último paso cantPasosAgregados veces.
     */
    private int cantPasosAgregados;
    /**
     * Tasa de decuento por paso
     */
    private double tasaDescPaso;
    /**
     * factor = 1/(1+tasa)
     */
    private double factorDescPaso; 
    /**
     * tiempo absoluto en que comienza el estudio
     */
    private TiempoAbsoluto inicioEstudio;
    /**
     *  Ultimo tiempo absoluto del estudio
     *  resulta del inicio del Estudio más la cantidad de pasos - 1
     */
    private TiempoAbsoluto finEstudio;
    /** Tiempos absolutos del estudio
     *  En el get(0) hay un valor que no se utiliza
     *  En el get(1) aparece el tiempo absoluto del paso 1 y así sucesivamente
     *  Se usan las funciones
     *  - pasoDeTiempoAbsoluto
     *  - tiempoAbsolutoDePaso
     */
    private ArrayList<TiempoAbsoluto> tiempoAbsolutoPasos;
    /**
     * es la cantidad de etapas sin incluir el paso cero
     */
    private int cantEtapas;
    /**
     *  En cada elemento de etapasPasos están los datos
     *  de una etapa de pasos de tiempo, empezando de la etapa 0 que
     *  consta sólo del paso 0
     *  Cada etapa tiene;
     *  paso inicial de la etapa en [0]
     *  paso final de la etapa en [1]
     *  paso representativo de la etapa en [2]
     *  El elemento get(0) tiene todos ceros en el array[]
     *
     *  ATENCION: LA ULTIMA ETAPA DEBE TENER UN ÚNICO PASO DE TIEMPO  !!!!!!!!
     */
    private ArrayList<int[]> etapasPasos;
    /** cantidad de subpasos en un paso de tiempo
     *  los subpasos son la unidad de tiempo de primer orden en que
     *  se divide un paso: por ejemplo los subpasos pueden ser semanas
     */
    private int cantSubpasos;
    private String tipoCargosFijos; // "ANUALIDADES" se cargan los costos fijos a�o a a�o; "TOTALPASODECISION" Se cargan en el momendo de la dedici�n
    public static final String ANUALIDADES = "ANUALIDADES";
    public static final String TOTALPASODECISION = "TOTALPASODECISION";
    
    private CalculadorDePlazos calculadorDePlazos;
    /**
     * Si la demanda se representa por postes su cantidad
     */
    private int cantPostes;
    /**
     * Cantidad de crónicas de las variables aleatorias del modelo de operación (MOS)
     * que aparecerán en los resultados del MOS y se esperan en los resultados de la
     * simulación
     *
     */
    private int cantCronicas;
    /**
     * Si es true, la combinatoria de portafolios contiguos para determinar si un 
     * portafolio es interior se hace con un sólo valor -1 o 1 en vector de RecursosBase
     * elegibles agregados. Si es false se hace el producto cartesiano de {-1,0,1}
     */
    private boolean combContiguosSimple;
    /**
     * Lista de productos
     */
    private ArrayList<Producto> productos;  
    /**
     * Lista de numerarios
     */
    private ArrayList<Numerario> numerarios; 
    /** maximo período de construcción de los recursos y transformaciones
     *  decididas en el paso t, para t = 1, 2, etc.
     *  los períodos se truncan a este valor. El get(0) corresponde al paso 1.
     */
    private ArrayList<Integer> maximoPerConst;

    public DatosGeneralesEstudio() {
        nombreEstudio = "";
        pasosPorAnio = 1;
        cantPasos = 0;
        tasaDescPaso = 0.1;
        etapasPasos = new ArrayList<int[]>();
        inicioEstudio = new TiempoAbsoluto();
        tiempoAbsolutoPasos = new ArrayList<TiempoAbsoluto>();
        productos = new ArrayList<Producto>();
        numerarios = new ArrayList<Numerario>();
        maximoPerConst = new ArrayList<Integer>();
        calculadorDePlazos = new CalculadorDePlazos(this);
    }

    public String getModeloOp() {
        return modeloOp;
    }

    public void setModeloOp(String modeloOp) {
        this.modeloOp = modeloOp;
    }

    public BolsaDeSimulaciones3 getBolsaSimul() {
        return bolsaSimul;
    }

    public void setBolsaSimul(BolsaDeSimulaciones3 bolsaSimul) {
        this.bolsaSimul = bolsaSimul;
    }

    public int getCantPasos() {
        return cantPasos;
    }

    public void setCantPasos(int cantPasos) {
        this.cantPasos = cantPasos;
    }

    public int getCantPasosAgregados() {
        return cantPasosAgregados;
    }

    public void setCantPasosAgregados(int cantPasosAgregados) {
        this.cantPasosAgregados = cantPasosAgregados;
    }

    public double getFactorDescPaso() {
        return factorDescPaso;
    }

    public void setFactorDescPaso(double factorDescPaso) {
        this.factorDescPaso = factorDescPaso;
    }

    public double getTasaDescPaso() {
        return tasaDescPaso;
    }

    public void setTasaDescPaso(double tasaDescPaso) {
        this.tasaDescPaso = tasaDescPaso;
    }

    public int getCantCronicas() {
        return cantCronicas;
    }

    public void setCantCronicas(int cantCronicas) {
        this.cantCronicas = cantCronicas;
    }

    public boolean getCombContiguosSimple() {
        return combContiguosSimple;
    }

    public void setCombContiguosSimple(boolean combContiguosSimple) {
        this.combContiguosSimple = combContiguosSimple;
    }

    public int getPasosPorAnio() {
        return pasosPorAnio;
    }

    public void setPasosPorAnio(int pasosPorAnio) {
        this.pasosPorAnio = pasosPorAnio;
    }

    public TiempoAbsoluto getInicioEstudio() {
        return inicioEstudio;
    }

    public void setInicioEstudio(TiempoAbsoluto inicioEstudio) {
        this.inicioEstudio = inicioEstudio;
    }

    
    public TiempoAbsoluto getFinEstudio() {
        return finEstudio;
    }

    public void setFinEstudio(TiempoAbsoluto finEstudio) {
        this.finEstudio = finEstudio;
    }

    
    
    public String getTipoCargosFijos() {
		return tipoCargosFijos;
	}

	public void setTipoCargosFijos(String tipoCargosFijos) {
		this.tipoCargosFijos = tipoCargosFijos;
	}

	/**
     * Devuelve una lista de los TiemposAbsolutos del Estudio, empezando desde
     * el primero (omite el lugar vacío del atributo de igual nombre).
     * @return 
     */
    public ArrayList<TiempoAbsoluto> getTiempoAbsolutoPasos() {
        ArrayList<TiempoAbsoluto> al = new ArrayList<TiempoAbsoluto>();
        for(int it = 1; it<tiempoAbsolutoPasos.size(); it++){
            al.add(tiempoAbsolutoPasos.get(it));
        }
        return al;
    }

    public void setTiempoAbsolutoPasos(ArrayList<TiempoAbsoluto> tiempoAbsolutoPasos) {
        this.tiempoAbsolutoPasos = tiempoAbsolutoPasos;
    }

    public int getCantSubpasos() {
        return cantSubpasos;
    }

    public void setCantSubpasos(int cantSubpasos) {
        this.cantSubpasos = cantSubpasos;
    }

    public String getNombreEstudio() {
        return nombreEstudio;
    }

    public void setNombreEstudio(String nombreEstudio) {
        this.nombreEstudio = nombreEstudio;
    }

    /**
     * Devuelve el número de paso cuyo tiempo absoluto es ta
     */
    public int pasoDeTiempoAbsoluto(TiempoAbsoluto ta) throws XcargaDatos {

        int numpaso = tiempoAbsolutoPasos.indexOf(ta);
        if (numpaso < 0) {
            throw new XcargaDatos("No existe el tiempo absoluto" + ta.toString());
        }
        return numpaso;
    }

    /**
     * Devuelve el TiempoAbsoluto del paso t-ésimo, donde el paso
     * en el get(1) es el paso en el primer tiempo absoluto del estudio
     * (El valor en cero no se utiliza)
     */
    public TiempoAbsoluto tiempoAbsolutoDePaso(int p) {
        assert (p > 0 || p <= cantPasos) : "Error en tiempo absoluto de paso" + p;
        return tiempoAbsolutoPasos.get(p);
    }

    /**
     * Devuelve el TiempoAbsoluto del paso representativo de la etapa e-ésima
     */
    public TiempoAbsoluto tiempoAbsolutoDeEtapa(int e) throws XcargaDatos {
        if (e > cantEtapas) {
            throw new XcargaDatos("Se pidió el TiempoAbsoluto de una etapa " + e
                    + " mayor que la cantidad de etapas");
        }
        TiempoAbsoluto ta = tiempoAbsolutoDePaso(pasoRepDeEtapa(e));
        return ta;
    }

    /**
     * Devuelve la etapa a la que pertenece un paso
     */
    public int etapaDeUnPaso(int paso) throws XcargaDatos {

        int etapa = 0;
        boolean encontro = false;
        if (paso == 0) {
            etapa = 0;
            return etapa;
        }
        for (int ip = 1; ip < etapasPasos.size(); ip++) {
            if (etapasPasos.get(ip)[1] >= paso) {
                encontro = true;
                etapa = ip;
                return etapa;
            }
        }
        throw new XcargaDatos("El número de paso es erróneo: paso = " + paso);
    }

    /**
     * Devuelve el paso inicial de la etapa etapa
     * etapa 0 tiene el paso 0
     */
    public int pasoIniDeEtapa(int etapa) {

        int paso = etapasPasos.get(etapa)[0];
        return paso;
    }

    /**
     * Devuelve el paso final de la etapa etapa
     * etapa 0 tiene el paso 0
     */
    public int pasoFinDeEtapa(int etapa) {

        int paso = etapasPasos.get(etapa)[1];
        return paso;
    }

    /**
     * Devuelve el paso representativo de la etapa etapa
     * etapa 0 tiene el paso 0
     */
    public int pasoRepDeEtapa(int etapa) {

        int paso = etapasPasos.get(etapa)[2];
        return paso;
    }

    /**
     *
     * @param stabs es un string que representa un tiempo absoluto en texto
     * @return
     */
    public TiempoAbsoluto tiempoAbsolutoDeUnString(String stabs, DatosGeneralesEstudio datGen) {
        int an = Integer.parseInt(stabs.substring(0, 3));
        int pDA;
        if (stabs.length() > 4) {
            pDA = Integer.parseInt(stabs.substring(5));

        } else {
            pDA = 1;
        }
        Calendar fechaIni = FCalendar.fechaIniDeTa(an, pDA, datGen);
        Calendar fechaFin = FCalendar.fechaFinDeTa(an, pDA, datGen);
        TiempoAbsoluto ta = new TiempoAbsoluto(an, pDA, fechaIni, fechaFin);
        return ta;
    }

    public int getCantEtapas() {
        return cantEtapas;
    }

    public void setCantEtapas(int cantEtapas) {
        this.cantEtapas = cantEtapas;
    }

    public ArrayList<int[]> getEtapasPasos() {
        return etapasPasos;
    }

    public void setEtapasPasos(ArrayList<int[]> etapasPasos) {
        this.etapasPasos = etapasPasos;
    }

    public ArrayList<Numerario> getNumerarios() {
        return numerarios;
    }

    public void setNumerarios(ArrayList<Numerario> numerarios) {
        this.numerarios = numerarios;
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    /**
     * Devuelve el producto de nombre nombre o null si este no existe
     */
    public Producto devuelveProducto(String nombre) {
        Producto prodRes = null;
        for (Producto p : productos) {
            if (p.getNombre().equalsIgnoreCase(nombre)) {
                prodRes = p;
                break;
            }
        }

        return prodRes;
    }

    public void agregarNumerario(Numerario nu) {
        numerarios.add(nu);
    }

    /**
     * Devuelve el numerario de nombre nombre
     * @param nombre es el nombre que se busca
     * @return nume es el numerario
     *
     */
    public Numerario devuelveNumerario(String nombre) {
        boolean encontro = false;
        Numerario nume = null;
        for (Numerario nu : numerarios) {
            if (nu.getNombre().equalsIgnoreCase(nombre)) {
                nume = nu;
                encontro = true;
            }
        }
        return nume;
    }

    public void setProductos(ArrayList<Producto> productos) {
        this.productos = productos;
    }

    public void agregarProducto(Producto pr) {
        productos.add(pr);
    }

//	public Producto getUnProd(String nombre){
//        Producto prod = null;
//        for(Producto pr: productos){
//			if( pr.getNombre().equalsIgnoreCase(nombre) ){
//				prod = pr;
//			}
//        }
//       	return prod;
//	}
    public int getCantPostes() {
        return cantPostes;
    }

    public void setCantPostes(int cantPostes) {
        this.cantPostes = cantPostes;
    }

    public ArrayList<Integer> getMaximoPerConst() {
        return maximoPerConst;
    }

    public void setMaximoPerConst(ArrayList<Integer> maximoPerConst) {
        this.maximoPerConst = maximoPerConst;
    }

    public CalculadorDePlazos getCalculadorDePlazos() {
        return calculadorDePlazos;
    }

    public void setCalculadorDePlazos(CalculadorDePlazos calculadorDePlazos) {
        this.calculadorDePlazos = calculadorDePlazos;
    }

    /**
     * Carga los tiempos absolutos de cada uno de los pasos
     */
    public void cargaTiemposAbsolutosPasos() {
        Calendar fechaIni = null;
        Calendar fechaFin = null;
        TiempoAbsoluto ta = new TiempoAbsoluto(0, 0, fechaIni, fechaFin);
        tiempoAbsolutoPasos.add(ta);
        for (int i = 0; i < cantPasos; i++) {
            tiempoAbsolutoPasos.add(calculadorDePlazos.hallaTiempoAbsoluto(
                    inicioEstudio, i));
        }
    }

    public void cargaFinEstudio() {
        TiempoAbsoluto tini = new TiempoAbsoluto();
        tini = inicioEstudio;
        int largo = cantPasos;
        finEstudio = calculadorDePlazos.hallaTiempoAbsoluto(tini, largo);

    }

    /**
     * Devuelve el máximo período de construcción admisible en el Estudio
     * para las construcciones que se inician en el TiempoAbsoluto ta.
     * Es un dato del estudio que acota los períodos de construcción propios 
     * de RecursosBase y TransforBase.
     * @param ta
     * @return
     */
    public int maxPerConstDeTA(TiempoAbsoluto ta) throws XcargaDatos {
        int paso = pasoDeTiempoAbsoluto(ta);
        return getMaximoPerConst().get(paso);
    }

    @Override
    public String toString() {

        String texto = "====================================================" + "\n";
        texto += "COMIENZAN DATOS GENERALES DEL ESTUDIO " + "\n";
        texto += "====================================================" + "\n";
        int i;
        texto += "NOMBRE ESTUDIO: " + nombreEstudio + "\n";
        texto += "Cantidad entera de pasos en un año: " + pasosPorAnio + "\n";
        texto += "Cantidad de pasos del estudio: " + cantPasos + "\n";
        texto += "Tiempo absoluto inicial del estudio: " + inicioEstudio.toString() + "\n";
        texto += "Tiempo absoluto último del estudio: " + finEstudio.toString() + "\n";
        texto += "Paso inicial dentro del año inicial del estudio: " + inicioEstudio.getParteDelAnio() + "\n";
        texto += "Etapas de pasos" + "\n";
        for (i = 0; i < etapasPasos.size(); i++) {
            texto += "Paso inicial: " + etapasPasos.get(i)[0];
            texto += "- Paso final: " + etapasPasos.get(i)[1];
            texto += "- Paso representativo: " + etapasPasos.get(i)[2] + "\n";
        }
        texto += "Subpasos iguales en cada paso de tiempo: " + cantSubpasos + "\n";
        texto += "Cantidad de postes de la demanda: " + cantPostes + "\n";
        texto += "Cantidad de crónicas: " + cantCronicas + "\n" + "\n";
        texto += "Nombres de los pasos: ";
        for (i = 0; i < tiempoAbsolutoPasos.size(); i++) {
            texto += " \n" + "ordinal paso =" + i + "-nombre del paso: " + tiempoAbsolutoPasos.get(i).toString();
        }
        texto += "\n";
        texto += "PRODUCTOS:" + "\n";
        for (i = 0; i < productos.size(); i++) {
            texto += productos.get(i).toString() + "\n";
        }
        texto += "NUMERARIOS:" + "\n";
        for (i = 0; i < numerarios.size(); i++) {
            texto += numerarios.get(i).toString() + "\n";
        }
        texto += "Maximo período de construccion: " + "\n";
        for (i = 0; i < maximoPerConst.size(); i++) {
            texto += "Paso: " + (i + 1) + " - maximo período = " + maximoPerConst.get(i) + "\n";
        }
        return texto;
    }

    public static void main(String[] args) {

        String texto = "";
        String dirDatGen = "D:/Java/PruebaJava2/V4-DatosGenerales.txt";
        DatosGeneralesEstudio datGen1 = new DatosGeneralesEstudio();

        CargaDatosGenerales cargador = new CargaDatosGenerales() {
        };
        try {
            cargador.cargarDatosGen(dirDatGen, datGen1);
            texto = datGen1.toString();
            System.out.println(texto + "\n");

        } catch (XcargaDatos ex) {
            System.out.println("------ERROR-------");
            System.out.println(ex.getDescripcion());
            System.out.println("------------------\n");
        }
    }
}
