 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import UtilitariosGenerales.CalculadorDePlazos;
import persistenciaMingo.XcargaDatos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 *
 * Expansión: almacena los conjuntos de recursos y transformaciones
 * que deben entrar en servicio o en construcción, en cada paso de tiempo
 * del Estudio./n
 * Tiene datos para TODOS los paso de tiempo del estudio y no sólo los pasos
 * representativos.
 *
 * Se entiende que los recursos y transformaciones en el paso 1
 * de una expansión, son los existentes en comienzo del paso 1.
 *
 * Cuando se crean sucesores de parques que viven en el paso representativo de
 * la etapa e, el paso pre, en el parque sucesor que vive en premas1 (paso representativo
 * de la etapa e+1), se agregan al parque todos los objetos que aparecen en la expansión
 * entre los pasos pre y el anterior a premas1 inclusive.
 * 
 *
 */
public class Expansion implements Serializable {

     private Estudio est;
     private Caso caso;
     private String nombre;
     /**
      * Largo de la expansión en pasos de tiempo, a partir del 1,
      * debe ser igual a cantPasos del Estudio en que se usa.
      */
     private int cantPasos;
     /**
      * El primer índice de los ArrayList anteriores es el paso de tiempo
      * En el get(0) está el primer paso de tiempo del estudio, el paso 1.
      *
      */
     private ArrayList<ArrayList<Bloque>> bloques;
 
     public Expansion(String nombre, Estudio est, int cantPasos) {
          this.nombre = nombre;
          this.est = est;
          this.cantPasos = cantPasos;
          bloques = new ArrayList<ArrayList<Bloque>>();
          for (int t = 0; t < cantPasos; t++) {
               bloques.add(new ArrayList<Bloque>());
          }
     }

     public Estudio getEst() {
          return est;
     }

     public void setEst(Estudio est) {
          this.est = est;
     }

     public Caso getCaso() {
          return caso;
     }

     public void setCaso(Caso caso) {
          this.caso = caso;
     }

     public int getCantPasos() {
          return cantPasos;
     }

     public void setCantPasos(int cantPasos) {
          this.cantPasos = cantPasos;
     }

     public String getNombre() {
          return nombre;
     }

     public void setNombre(String nombre) {
          this.nombre = nombre;
     }

     public ArrayList<ArrayList<Bloque>> getBloques() {
          return bloques;
     }

     public void setBloques(ArrayList<ArrayList<Bloque>> bloques) {
          this.bloques = bloques;
     }

     /**
      * Devuelve la lista de bloques que aparecen en la expansión
      * en el tiempoAbsoluto ta
      */
     public ArrayList<Bloque> bloquesDeTiempoAbsoluto(TiempoAbsoluto ta) throws XcargaDatos {
          DatosGeneralesEstudio datGen = est.getDatosGenerales();
          int paso = datGen.pasoDeTiempoAbsoluto(ta);
          return bloques.get(paso - 1);
     }

     
     
     /**
      * Crea una expansión nueva por adición de los bloques de dos expansiones existentes
      * Los miembros adicionales a los bloques son tomados de exp1.
      */
     public static Expansion suma2Exps(Expansion exp1, Expansion exp2){
         Expansion expSum = new Expansion("Expansión total", exp1.getEst(), exp1.getCantPasos());
         for(int i=0; i<exp1.getCantPasos();i++){
             expSum.bloques.get(i).addAll(exp1.getBloques().get(i));
             expSum.bloques.get(i).addAll(exp2.getBloques().get(i));                          
         }
         return expSum;                          
     }
     
     /**
      * Carga el Bloque de un objeto (Recurso o Transformación) en la expansión,
      * en el tiempo absoluto ta.
      * Si ya existe un bloque con el mismo objeto en el mismo ta
      * hay que aumentar el multiplicador. Si no se crea un nuevo bloque.
      *
      * Verifica que ningún Recurso aparezca en un TiempoAbsoluto
      * anterior al TiempoAbsolutoBase de su RecursoBase, ni en un TiempoAbsoluto
      * posterior al UltimoTiempoAbsoluto de su RecursoBase. En ese caso lanza un error.
      *
      */
     public void cargaBloqueEnUnTA(Bloque bNue, TiempoAbsoluto ta) throws XcargaDatos {
          DatosGeneralesEstudio datGen = est.getDatosGenerales();
          CalculadorDePlazos calc = datGen.getCalculadorDePlazos();
          RecursoBase rb = bNue.getRecurso().getRecBase();
          if (calc.hallaAvanceTiempo(rb.getTiempoAbsolutoBase(), ta)<0  ) {
               throw new XcargaDatos("En la lectura de la expansión en " +
                       ta.toStringCorto() + " el RecursoBase " + rb.getNombre() +
                       " aparece antes de su tiempoAbsolutoBase");
          }
          if (calc.hallaAvanceTiempo(rb.getUltimoTiempoAbsoluto(), ta)>0  ) {
               throw new XcargaDatos("En la lectura de la expansión en " +
                       ta.toStringCorto() + " el RecursoBase " + rb.getNombre() +
                       " aparece después de su ultimoTiempoAbsoluto");
          }
          int paso = datGen.pasoDeTiempoAbsoluto(ta);
          if (bloques.get(paso - 1).isEmpty()) {
               bloques.get(paso - 1).add(bNue);
          } else {
               boolean encontro = false;
               for (Bloque b : bloques.get(paso - 1)) {
                    if (b.igualObjeto(bNue)) {
                         b.setMultiplicador(bNue.getMultiplicador() + b.getMultiplicador());
                         encontro = true;
                         break;
                    }
               }
               if (!encontro) {
                    bloques.get(paso - 1).add(bNue);
               }
          }
     }
  
    @Override
     public String toString() {
          String texto = "====================================================" + "\n";
          texto += "COMIENZAN DATOS DE EXPANSION " + nombre + "\n";
          texto += "====================================================" + "\n";
          int t = 1;
          for (TiempoAbsoluto ta:est.getDatosGenerales().getTiempoAbsolutoPasos()) {
               texto += "Paso de tiempo: " + ta.toString() + "\n";
               for (int i = 0; i < bloques.get(t - 1).size(); i++) {
                    boolean usaEquiv = false;
                    texto += bloques.get(t - 1).get(i).toStringCorto(usaEquiv);
               }
               t++;
          }
          return texto;
     }          
}
