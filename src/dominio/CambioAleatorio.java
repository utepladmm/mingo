/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.VariableNat;

/**
 *
 * @author ut469262
 */
public class CambioAleatorio implements Serializable {

    private String nombre;
    private String nomVN;
     /**
     * La VariableNat debe tener el nombre igual al del cambio aleatorio
     * con sufijo "_CAL".
     */
    private VariableNat varNat;   
    private ArrayList<Recurso> recursosOrigen;  // no puede haber RecursosBase repetidos
    private ArrayList<Recurso> recursosDestino; // no puede haber RecursosBase repetidos   
    private ArrayList<Integer> mulRecOrigen;     // cantidad de recursosBase de los de recursosOrigen que desaparecen
    private ArrayList<Integer> mulRecDestino;    // cantidad de recursosBase de los de recursosDestino que aparecen

    /**
     * SIEMPRE DEBE HABER UNA LISTA DE RECURSOS DESTINO
     * Si un recurso desaparece desde el punto de vista operativo:
     * - su recurso destino puede tener costos, los costos hundidos a pesar del
     *   cambio aleatorio.
     * - su potencia nominal y oferta de productos es nula
     * - DEBE SER UN RECURSO CON EstadosRec FALL, para que no se incluya en el 
     *   parque operativo en las simulaciones.
     *
     * Las variables de cambio aleatorio sólo toman valores NO y SI.
     * El cambio aleatorio ocurre cuando la variable, entre una etapa y la
     * siguiente pasa de valer NO a valer SI.
     * Si la variable aleatoria asociada a un cambio aleatorio vale SI en t
     * en la sucesión sin crecimiento de t-1 a t, se investiga si existe
     * en t-1 una lista de RecursosBase igual a origen y de ocurrir eso en t
     * dicha lista se convierte en la lista destino
     *
     * ATENCIÓN:
     * 
     * SE BUSCAN COMO RECURSOS ORIGEN EN EL PARQUE LOS QUE TENGAN EL MISMO RECURSO BASE
     * Y EL MISMO ESTADO QUE LOS ESPECIFICADOS EN EL CAMBIO ALEATORIO, SIN IMPORTAR
     * LOS VALORES DE LOS OTROS DATOS.
     * 
     * Cada RecursoBase de recursoDestino da lugar a un Recurso que si tiene edad
     * nace con edad cero.
     */
    public CambioAleatorio() {
        recursosOrigen = new ArrayList<Recurso>();
        recursosDestino = new ArrayList<Recurso>();
        mulRecOrigen = new ArrayList<Integer>();
        mulRecDestino = new ArrayList<Integer>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Recurso> getRecursosDestino() {
        return recursosDestino;
    }

    public void setRecursosDestino(ArrayList<Recurso> recursosDestino) {
        this.recursosDestino = recursosDestino;
    }

    public ArrayList<Recurso> getRecursosOrigen() {
        return recursosOrigen;
    }

    public void setRecursosOrigen(ArrayList<Recurso> recursosOrigen) {
        this.recursosOrigen = recursosOrigen;
    }

    public String getNomVN() {
        return nomVN;
    }

    public void setNomVN(String nomVN) {
        this.nomVN = nomVN;
    }

    public VariableNat getVarNat() {
        return varNat;
    }

    public void setVarNat(VariableNat varNat) {
        this.varNat = varNat;
    }

    public ArrayList<Integer> getMulRecDestino() {
        return mulRecDestino;
    }

    public void setMulRBDestino(ArrayList<Integer> mulRecDestino) {
        this.mulRecDestino = mulRecDestino;
    }

    public ArrayList<Integer> getMulRecOrigen() {
        return mulRecOrigen;
    }

    public void setMulRecOrigen(ArrayList<Integer> mulRecOrigen) {
        this.mulRecOrigen = mulRecOrigen;
    }

    public void addRecOrigen(Recurso rec) {
        recursosOrigen.add(rec);
    }

    public void addRecDestino(Recurso rec) {
        recursosDestino.add(rec);
    }

    public void addMulOrigen(int mul) {
        mulRecOrigen.add(mul);
    }

    public void addMulDestino(int mul) {
        mulRecDestino.add(mul);
    }    
    
    public int mulRecursoOrigen(Recurso rec){
        int ind = recursosOrigen.indexOf(rec);
        return mulRecOrigen.get(ind);
    }
    
    public int mulRecursoDestino(Recurso rec){
        int ind = recursosDestino.indexOf(rec);
        return mulRecDestino.get(ind);
    }    
    
    /**
     * Devuelve true si el cambio aleatorio tiene como uno
     * de sus Recursos origen a un Recurso cuyo RecursoBase es rb
     * @param rb
     * @return 
     */
    public boolean aplicaARB(RecursoBase rb){        
        for(Recurso rec: recursosOrigen){
            if(rec.getRecBase()==rb) return true;
        }
        return false;
    }
    

    @Override
    public String toString() {
        String texto = "";
        int i;
        texto += "CAMBIO ALEATORIO: " + nombre + "\r\n";
        for (i = 0; i < recursosOrigen.size(); i++) {
            texto += "Recursos:" + recursosOrigen.get(i).toString() + "\r\n";
            texto += "Multiplicadores:"  + mulRecOrigen.get(i)+ "\r\n";
        }
        for (i = 0; i < recursosDestino.size(); i++) {
            texto += "Recursos destino: " + recursosDestino.get(i).toString() + "\r\n";
            texto += "Multiplicadores:"  + mulRecDestino.get(i)+ "\r\n";            
        }
        texto += "\r\n";
        return texto;

    }

}
