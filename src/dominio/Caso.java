/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import AlmacenSimulaciones.IdentSimulPaso;
import GrafoEstados.ConjRestGrafoE;
import GrafoEstados.GrafoE;
import GrafoEstados.NodoE;
import GrafoEstados.Objetivo;
import GrafoEstados.RestGrafoE;
import Restricciones3.LectorRestE;
import Restricciones3.RestCota3;
import Restricciones3.RestDeProducto3;
//import dominio.Bloque.TipoBloque;
import TiposEnum.DependenciaDeT;
import TiposEnum.ExistEleg;
import persistenciaMingo.CargaExpaForzada;
import persistenciaMingo.CargaObjetivo;
import persistenciaMingo.CargaRyTBCaso;
import persistenciaMingo.CargaSensibilidad;
import persistenciaMingo.CargaTiposDeDatoCaso;
import persistenciaMingo.XcargaDatos;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author ut469262
 * Un Caso permite resolver el problema del estudio con limitaciones
 * adicionales.
 *
 * Por ejemplo:
 * - se toma como recursos candidatos a intergar el portafolio
 *   óptimo, sólo a un subconjunto de los recursos del estudio
 * - se agregan restricciones significativas de portafolio o de inicio
 *   de construcción.
 *
 * El principio debería ser que las restricciones heurísticas de cantidad
 * de recursos de un caso permitan al menos 1 recurso de cada recursoBase
 * para comparar con la situación si se restringe a cero recursos de ese
 * recursoBase.
 */
public class Caso implements Serializable {

    private Estudio estudio;
    /**
     * programación dinámica
     */
    public static final String PD = "PD";   
    /**
     * algoritmo genético
     */
    public static final String ALGEN = "ALGEN";   
    private String nombre;
    private String directorio;
    private int numeroCaso;
    /**
     * Es el objetivo con el que se evalúan los costos de paso de tiempo y
     * de fin de juego a partir de los resultados de la simulación.
     */
    private Objetivo obj;   
    private String tipoSensib;
    public static final String FRAC = "FRACCION";
    public static final String VABS = "VALOR_ABSOLUTO";
    /**
     * tipo sensib es el tipo de entorno respecto al costo de paso y valor sensib
     * el ancho del entorno.
     */
    private double valorSensib;  
    /**
     * Lista de los percentiles que se quieren sacar en las distribuciones de costos
     * de los resultados
     */
    private ArrayList<Double> percentiles; 
    private Date fechaCreacion;  // hay que ver la clase Date para usarla
    /**
     * Subconjunto de los recursosBase del estudio que se usan en el caso
     */
    private ConjRecB subConjRB;         
    /**
     * Subconjunto de los recursosBase del estudio que se usan en el caso
     */
    private ConjTransB subConjTB;   
    /**
     * RecursosBase elegibles de subConjRecB
     */
    private ConjRecB subConjRBEleg;    
    /**
     * TransformacionesBase elegibles de subConjRecB
     */
    private ConjTransB subConjTBEleg;  
    
     /**
     * Las siguientes son las cotas inferiores y superiores de inicio de construcción,
     * para los RB y TB elegibles, que son todas restricciones significativas.
     * El primer índice es el del orden en el respectivo conjunto subConjRBEleg o subConjTBEleg
     * El segúndo índice es de la etapa, donde la etapa 1 está en el get(0).
     *
     * Para los RecursosBase y TransforBase que son "normales" y que no tienen restricciones
     * significativas de inicio de construcción se cargan cotas 0 inferior y 99 superior,
     * en el método leerCaso.
     */
    private ArrayList<ArrayList<Integer>> cotaInfInicioRBEleg;
    private ArrayList<ArrayList<Integer>> cotaSupInicioRBEleg;
    private ArrayList<ArrayList<Integer>> cotaInfInicioTBEleg;
    private ArrayList<ArrayList<Integer>> cotaSupInicioTBEleg;
     
    /**
     * Almacena los valores de todos los TipoDeDatos fijos del caso
     * alguno de los cuales pueden cambiar respecto a los del estudio
     */
    private ArrayList<ParTipoDatoValor> valoresTDFijosCaso;   
    private Expansion expansionForzadaCaso;
    /**
     * unión de las Expansiones del Estudio y el Caso
     */
    private Expansion expansionForzadaTotal;  
     /**
     * conjRestInicCaso y conjRestParqueCaso son los conjuntos de restricciones
     * significativas que el caso adiciona al estudio
     */
    private ConjRestGrafoE conjRestSigInicioCaso;
    private ConjRestGrafoE conjRestSigParqueCaso;  
    private ConjRestGrafoE conjRestSigInicioResult;
    /**
     * conjRestInicioResult y conjRestParqueResult son los conjuntos de restricciones
     * resultantes, que son la unión de las respectivas restricciones del estudio y
     * adicionales del caso
     */
    private ConjRestGrafoE conjRestSigParqueResult;
    /**
     * El caso generará una sucesión de grafosE al aplicarse los sucesivos
     * conjuntos de restricciones heurísticas de parque que se van almacenando
     * en sucRestHParque.
     * ATENCIÓN: LAS RESTRICCIONES HEURÍSTICAS SON SÓLO DE PARQUE.
     */
    private ArrayList<ConjRestGrafoE> sucRestHParque;
    /**
     * Restricciones DE PARQUE de tipo de PRODUCTO corrientes en el caso dadas las restricciones
     * tanto HEURISTICAS corrientes como SIGNIFICATIVAS. Hay que cargarlas en cada iteración de las restricciones
     * heurísticas usando el método cargaRestsDeParqueCorr.
     *
     * El primer índice corresponde a los productos en el orden en que aparecen en
     * los datos generales del estudio.
     * Si un producto no tiene restricciones aparece una lista vacía en su lugar.
     */
    private ArrayList<ArrayList<RestDeProducto3>> restsDeProdParqueCorr;
    /**
     * Restricciones DE PARQUE de tipo de COTA corrientes en el caso dadas las restricciones
     * tanto HEURISTICAS corrientes como SIGNIFICATIVAS, que se aplican a los RecursosBase elegibles.
     * Hay que cargarlas en cada iteración de las restricciones
     * heurísticas usando el método cargaRestsDeParqueCorr.
     *
     * El primer índice corresponde a los RecursosBase elegibles en el orden en que aparecen en
     * subConjRBEleg y el segundo a la lista de restricciones.
     * Si un RecursoBase elegible no tiene restricciones de cota hay una lista vacía asociada.
     */
    private ArrayList<ArrayList<RestCota3>> restsDeCotaRbElegParqueCorr; 
    /**
     * Sucesión de restricciones de inicio de inversión sólo de tipo de cota por RecursoBase
     * o TranforBase que se emplean en el algoritmo genético.
     * Son restricciones heurísticas para determinar la cantidad de bits
     * del código genético.
     *
     * En el algoritmo genético se tienen en cuenta además las restriccione significativas
     * resultantes del caso, para no dejar nacer o matar los Individuos que no las cumplan.
     *
     */
    private ArrayList<ConjRestGrafoE> sucRestHAlGenInicioCota; 
    /**
     * grafoE es el grafo de estados corriente del caso. Se van generando a partir de
     * sucesivas modificaciones en las restricciones heurísticas de parque
     */
    private GrafoE grafoE;

    
    public Caso(Estudio estudio, String nombre) {
        this.estudio = estudio;
        this.nombre = nombre;
        directorio = estudio.getNombre() + "/" + nombre;
        numeroCaso = estudio.getCasos().size() + 1;
        percentiles = new ArrayList<Double>();
        subConjRB = new ConjRecB();
        subConjTB = new ConjTransB();
        subConjRBEleg = new ConjRecB();
        subConjTBEleg = new ConjTransB();
        sucRestHParque = new ArrayList<ConjRestGrafoE>();
        conjRestSigInicioCaso = new ConjRestGrafoE();
        conjRestSigParqueCaso = new ConjRestGrafoE();
        conjRestSigInicioResult = new ConjRestGrafoE();
        conjRestSigParqueResult = new ConjRestGrafoE();
        valoresTDFijosCaso = new ArrayList<ParTipoDatoValor>();
        for (ParTipoDatoValor pT : estudio.getValoresTDFijosEst()) {
            valoresTDFijosCaso.add(pT.copiaParTDValor());
        }
        restsDeProdParqueCorr = new ArrayList<ArrayList<RestDeProducto3>>();
        restsDeCotaRbElegParqueCorr = new ArrayList<ArrayList<RestCota3>>();
        sucRestHParque = new ArrayList<ConjRestGrafoE>();
        cotaInfInicioRBEleg = new ArrayList<ArrayList<Integer>>();
        cotaSupInicioRBEleg = new ArrayList<ArrayList<Integer>>();
        cotaInfInicioTBEleg = new ArrayList<ArrayList<Integer>>();
        cotaSupInicioTBEleg = new ArrayList<ArrayList<Integer>>();

        sucRestHAlGenInicioCota = new ArrayList<ConjRestGrafoE>();

        expansionForzadaCaso = new Expansion(nombre, estudio,
                estudio.getDatosGenerales().getCantPasos());    
    }

    public ConjRestGrafoE getConjRestSigInicioCaso() {
        return conjRestSigInicioCaso;
    }

    public void setConjRestSigInicConstCaso(ConjRestGrafoE conjRestSigInicConstCaso) {
        this.conjRestSigInicioCaso = conjRestSigInicConstCaso;
    }

    public ConjRestGrafoE getConjRestSigParqueCaso() {
        return conjRestSigParqueCaso;
    }

    public void setConjRestSigParqueCaso(ConjRestGrafoE conjRestSigParqueCaso) {
        this.conjRestSigParqueCaso = conjRestSigParqueCaso;
    }

    public ArrayList<ParTipoDatoValor> getValoresTDFijosCaso() {
        return valoresTDFijosCaso;
    }

    public void setValoresTDFijosCaso(ArrayList<ParTipoDatoValor> valoresTDFijosCaso) {
        this.valoresTDFijosCaso = valoresTDFijosCaso;
    }

    public Estudio getEstudio() {
        return estudio;
    }

    public void setEstudio(Estudio estudio) {
        this.estudio = estudio;
    }

    public String getDirectorio() {
        return directorio;
    }

    public void setDirectorio(String directorio) {
        this.directorio = directorio;
    }

    public int getNumeroCaso() {
        return numeroCaso;
    }

    public void setNumeroCaso(int numeroCaso) {
        this.numeroCaso = numeroCaso;
    }

    public ArrayList<Double> getPercentiles() {
        return percentiles;
    }

    public void setPercentiles(ArrayList<Double> percentiles) {
        this.percentiles = percentiles;
    }



    public Objetivo getObj() {
        return obj;
    }

    public void setObj(Objetivo obj) {
        this.obj = obj;
    }

    public String getTipoSensib() {
        return tipoSensib;
    }

    public void setTipoSensib(String tipoSensib) {
        this.tipoSensib = tipoSensib;
    }

    public double getValorSensib() {
        return valorSensib;
    }

    public void setValorSensib(double valorSensib) {
        this.valorSensib = valorSensib;
    }

    

    public Expansion getExpansionForzadaCaso() {
        return expansionForzadaCaso;
    }

    public void setExpansionForzadaCaso(Expansion expansionForzadaCaso) {
        this.expansionForzadaCaso = expansionForzadaCaso;
    }

    public Expansion getExpansionForzadaTotal() {
        return expansionForzadaTotal;
    }

    public void setExpansionForzadaTotal(Expansion expansionForzadaTotal) {
        this.expansionForzadaTotal = expansionForzadaTotal;
    }
    
    

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public GrafoE getGrafoE() {
        return grafoE;
    }

    public void setGrafoE(GrafoE grafoE) {
        this.grafoE = grafoE;
    }

    public ArrayList<ConjRestGrafoE> getSucRestHParque() {
        return sucRestHParque;
    }

    public void setSucRestHParque(ArrayList<ConjRestGrafoE> sucRestHParque) {
        this.sucRestHParque = sucRestHParque;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ConjRecB getSubConjRB() {
        return subConjRB;
    }

    public void setSubConjRB(ConjRecB subConjRB) {
        this.subConjRB = subConjRB;
    }

    public ConjTransB getSubConjTB() {
        return subConjTB;
    }

    public void setSubConjTB(ConjTransB subConjTB) {
        this.subConjTB = subConjTB;
    }

    public ConjRestGrafoE getConjRestSigInicioResult() {
        return conjRestSigInicioResult;
    }

    public void setConjRestSigInicioResult(ConjRestGrafoE conjRestSigInicioResult) {
        this.conjRestSigInicioResult = conjRestSigInicioResult;
    }

    public ConjRestGrafoE getConjRestSigParqueResult() {
        return conjRestSigParqueResult;
    }

    public void setConjRestSigParqueResult(ConjRestGrafoE conjRestSigParqueResult) {
        this.conjRestSigParqueResult = conjRestSigParqueResult;
    }

    public ConjRecB getSubConjRBEleg() {
        return subConjRBEleg;
    }

    public void setSubConjRBEleg(ConjRecB subConjRBEleg) {
        this.subConjRBEleg = subConjRBEleg;
    }

    public ConjTransB getSubConjTBEleg() {
        return subConjTBEleg;
    }

    public void setSubConjTBEleg(ConjTransB subConjTBElec) {
        this.subConjTBEleg = subConjTBElec;
    }

    public ArrayList<ArrayList<Integer>> getCotaInfInicioRBEleg() {
        return cotaInfInicioRBEleg;
    }

    public void setCotaInfInicioRBEleg(ArrayList<ArrayList<Integer>> cotaInfInicioRBEleg) {
        this.cotaInfInicioRBEleg = cotaInfInicioRBEleg;
    }

    public ArrayList<ArrayList<Integer>> getCotaInfInicioTBEleg() {
        return cotaInfInicioTBEleg;
    }

    public void setCotaInfInicioTBEleg(ArrayList<ArrayList<Integer>> cotaInfInicioTBEleg) {
        this.cotaInfInicioTBEleg = cotaInfInicioTBEleg;
    }

    public ArrayList<ArrayList<Integer>> getCotaSupInicioRBEleg() {
        return cotaSupInicioRBEleg;
    }

    public void setCotaSupInicioRBEleg(ArrayList<ArrayList<Integer>> cotaSupInicioRBEleg) {
        this.cotaSupInicioRBEleg = cotaSupInicioRBEleg;
    }

    public ArrayList<ArrayList<Integer>> getCotaSupInicioInicioTBEleg() {
        return cotaSupInicioTBEleg;
    }

    public void setCotaInicioSupTBEleg(ArrayList<ArrayList<Integer>> cotaSupInicioTBEleg) {
        this.cotaSupInicioTBEleg = cotaSupInicioTBEleg;
    }

    public ArrayList<ArrayList<RestDeProducto3>> getRestsDeProdParqueCorr() {
        return restsDeProdParqueCorr;
    }

    public void setRestsDeProdParqueCorr(ArrayList<ArrayList<RestDeProducto3>> restsDeProdParqueCorr) {
        this.restsDeProdParqueCorr = restsDeProdParqueCorr;
    }

    public ArrayList<ArrayList<Integer>> getCotaSupInicioTBEleg() {
        return cotaSupInicioTBEleg;
    }

    public void setCotaSupInicioTBEleg(ArrayList<ArrayList<Integer>> cotaSupInicioTBEleg) {
        this.cotaSupInicioTBEleg = cotaSupInicioTBEleg;
    }

    public ArrayList<ArrayList<RestCota3>> getRestsDeCotaRbElegParqueCorr() {
        return restsDeCotaRbElegParqueCorr;
    }

    public void setRestsDeCotaRbElegParqueCorr(ArrayList<ArrayList<RestCota3>> restsDeCotaRbElegParqueCorr) {
        this.restsDeCotaRbElegParqueCorr = restsDeCotaRbElegParqueCorr;
    }

    public void setConjRestSigInicioCaso(ConjRestGrafoE conjRestSigInicioCaso) {
        this.conjRestSigInicioCaso = conjRestSigInicioCaso;
    }

    /**
     * Devuelve la cota inferior de un recursoBase elegible en una etapa
     *
     * @param rb recursoBase cuya cota se busca
     *
     * @param e etapa para la que se busca la cota
     */
    public int cotaInfInicioRB(RecursoBase rb, int e) {
        int cota = 0;
        cota = cotaInfInicioRBEleg.get(subConjRB.getRecursosB().indexOf(rb)).get(e - 1);
        return cota;
    }

    /**
     * METODO cotaSupRB
     * Devuelve la cota superior de un recursoBase elegible en una etapa
     *
     * @param rb recursoBase cuya cota se busca
     *
     * @param e etapa para la que se busca la cota
     */
    public int cotaSupInicioRB(RecursoBase rb, int e) {
        int cota = 0;
        cota = cotaSupInicioRBEleg.get(subConjRB.getRecursosB().indexOf(rb)).get(e - 1);
        return cota;
    }

    /**
     * Devuelve la cota inferior de una transformaciónBase elegible en una etapa
     *
     * @param tb es la TransforBase cuya cota se busca
     *
     * @param e etapa para la que se busca la cota
     */
    public int cotaInfInicioTB(TransforBase tb, int e) {
        int cota = 0;
        cota = cotaInfInicioTBEleg.get(subConjTB.getTransforsB().indexOf(tb)).get(e - 1);
        return cota;
    }

    /**
     * Devuelve la cota inferior de un recursoBase elegible en una etapa
     *
     * @param rb recursoBase cuya cota se busca
     *
     * @param e etapa para la que se busca la cota
     */
    public int cotaSupInicioTB(TransforBase tb, int e) {
        int cota = 0;
        cota = cotaSupInicioTBEleg.get(subConjTB.getTransforsB().indexOf(tb)).get(e - 1);
        return cota;
    }

    /**
     * Crea el portafolio de etapa cero
     * @return pCero el parque de etapa cero
     */
    public Parque creaPCero() {
        DatosGeneralesEstudio datGen = estudio.getDatosGenerales();
        int[] cB = new int[estudio.getCantRecNoComunesYTransPosibles()];
        Parque pCero = new Parque(0, cB);
        return pCero;
    }

    /**
     * Crea el portafolio de etapa uno a partir de las expansiones por defecto.
     * @param comunes es true si se quiere incluir los bloques comunes
     *  y false si se quiere incluir los bloques no comunes.
     * @return pUno el parque de etapa uno.
     * @throws persistenciaMingo.XcargaDatos
     */
    public Parque creaPUno(boolean comunes, boolean nocomunes) throws XcargaDatos, XBloqueMultNeg {
        DatosGeneralesEstudio datGen = estudio.getDatosGenerales();
        TiempoAbsoluto taUno = datGen.tiempoAbsolutoDePaso(1);
        ArrayList<Bloque> bloquesExpEstudio = estudio.getExpansionForzadaEstudio().bloquesDeTiempoAbsoluto(taUno);
        ArrayList<Bloque> bloquesExpCaso = expansionForzadaCaso.bloquesDeTiempoAbsoluto(taUno);
        ArrayList<Bloque> bloquesPUno = new ArrayList<Bloque>();
        try {
            for (Bloque b : bloquesExpEstudio) {
                if(b.recBaseEsComun() & comunes | !b.recBaseEsComun() & nocomunes)
                    ParqueCreaSuc.agregaBloque(bloquesPUno, b);
            }
            for (Bloque b : bloquesExpCaso) {
                if(b.recBaseEsComun() & comunes | !b.recBaseEsComun() & nocomunes)                
                    ParqueCreaSuc.agregaBloque(bloquesPUno, b);
            }
        } catch (XBloqueMultNeg ex) {
            throw new XcargaDatos("Al agregar bloques un multiplicador dio negativo");
        }
        Parque pUno = new Parque(estudio, 1, taUno, bloquesPUno);
        return pUno;
    }

    /**
     * Carga la lista de restricciones de producto y de cota (ambas de parque) que
     * son pertinentes para el caso, incluso las que resulten de las restricciones
     * heurísticas corrientes.
     *
     * ATENCIÓN: Debe correrse en cada iteración de restricciones heurísticas.
     *
     * Para las de producto el resultado puede ser una lista vacía
     * para un producto si no hay ninguna restricción
     * de parque de ese producto para el caso.
     *
     */
    public void cargaRestDeParqueCorr() {

        ConjRestGrafoE conjRest = new ConjRestGrafoE();
        ArrayList<ConjRestGrafoE> listaConjsRest = new ArrayList<ConjRestGrafoE>();
        listaConjsRest.add(conjRestSigParqueResult);
        int iter = sucRestHParque.size();
        listaConjsRest.add(sucRestHParque.get(iter - 1));
        conjRest = ConjRestGrafoE.sumaConjuntos(listaConjsRest);
        /**
         * Carga las restricciones de producto
         */
        ArrayList<RestDeProducto3> restsProd = null;
        ArrayList<Producto> productos = estudio.getDatosGenerales().getProductos();

        for (Producto p : productos) {
            /**
             * Los productos se tratan en el orden en que aparecen en los datosGenerales
             * del estudio. Se carga un arrayList vacío si no hay restricciones para
             * un producto.
             */
            ArrayList<RestDeProducto3> auxR3 = new ArrayList<RestDeProducto3>();
            for (RestGrafoE rest : conjRest.getRestricciones()) {
                if (rest instanceof RestDeProducto3) {
                    RestDeProducto3 restP = (RestDeProducto3) rest;
                    if (restP.getProducto() == p) {
                        auxR3.add(restP);
                    }
                }
            }
            restsDeProdParqueCorr.add(auxR3);
        }
        /**
         * Carga las restricciones de cota de Parque, de RecursosBase elegibles para los
         * recursos subConjRBEleg. Si no hay
         */
        for (RecursoBase rbEleg : subConjRBEleg.getRecursosB()) {
            /**
             * Se carga un arrayList vacío si no hay restricciones para
             * un RecursoBase elegible
             */
            ArrayList<RestCota3> auxC3 = new ArrayList<RestCota3>();
            for (RestGrafoE rest : conjRest.getRestricciones()) {
                if (rest instanceof RestCota3) {
                    RestCota3 restC = (RestCota3) rest;
                    if (restC.getTipoBloque().equalsIgnoreCase("REC")) {
                        RecursoBase rbRest = (RecursoBase) restC.getObjeto();
                        if (rbRest == rbEleg) {
                            auxC3.add(restC);
                        }
                    }
                }
            }
            restsDeCotaRbElegParqueCorr.add(auxC3);
        }
    }
    /**
     * Carga el valor de un TipoDeDatos fijo, sobreescribiendo el existente, y si no
     * había valor preexistente lanza un error
     */
    public void cambiaValorTDFijo(TipoDeDatos tipoD, String valor) throws XcargaDatos {
        boolean encontre = false;
        for (ParTipoDatoValor pTDV : valoresTDFijosCaso) {
            if (tipoD == pTDV.getTipoDatos()) {
                encontre = true;
                pTDV.setValor(valor);
                break;
            }
        }
        if (!encontre) {
            throw new XcargaDatos("Se pidió cambiar el valor de un TipoDeDato fijo" +
                    "de nombre " + tipoD.getNombre() + "que no existe en el caso");
        }
    }

    /**
     * Devuelve el valor de un TipoDeDatos fijo, y si no
     * había valor preexistente lanza un error
     */
    public String devuelveValorTDFijo(TipoDeDatos tipoD) throws XcargaDatos {
        String valor = "";
        boolean encontre = false;
        for (ParTipoDatoValor pTDV : valoresTDFijosCaso) {
            if (tipoD == pTDV.getTipoDatos()) {
                encontre = true;
                valor = pTDV.getValor();
                break;
            }
        }
        if (!encontre) {
            throw new XcargaDatos("Se pidió el valor de un TipoDeDato fijo" +
                    "de nombre " + tipoD.getNombre() + " que no existe en el caso");
        }
        return valor;
    }

    /**
     * Devuelve una lista de las simulaciones faltantes en el GrafoE del caso
     * y carga true en el campo simulado
     *
     * @return simFalt es la lista de simulaciones faltantes en grafoE
     */
    public ArrayList<IdentSimulPaso> simulacionesFaltantes(ArrayList<Object> informacion) throws XcargaDatos {
        ArrayList<IdentSimulPaso> simFalt = new ArrayList<IdentSimulPaso>();
        for (int ie = 1; ie <= estudio.getDatosGenerales().getCantEtapas(); ie++) {
            TiempoAbsoluto ta = estudio.tiempoAbsolutoDeEtapa(ie);
            for (NodoE ne : grafoE.nodosEDeEtapa(ie)) {
                if (ne.getSimulado() == false) {
                    Parque par = (Parque) ne.getPort();
                    Parque parOpe = par.calculaParqueOperativo(informacion);
                    parOpe.cambiaPorRBEquiv(informacion);
                    ArrayList<ParTipoDatoValor> valoresTiposDatos = ne.getNodoN().getValoresTiposDatos();
                    simFalt.add(new IdentSimulPaso(parOpe, ta, valoresTiposDatos));
//                    ne.setSimulado(Simulado.EN_PROCESO);
                }
            }
        }
        return simFalt;
    }

//    /**
//     * Devuelve una lista de las simulaciones faltantes en el GrafoE del caso
//     * y carga true en el campo simulado.
//     * IDENTIFICA LAS SIMULACIONES POR SU ClaveStringsimul EN VEZ DE IdentsimulPaso
//     *
//     * @return simFalt es la lista de simulaciones faltantes en grafoE
//     */
//    public ArrayList<ClaveStringSimul> simulacionesFaltantes2() throws XcargaDatos {
//        ArrayList<ClaveStringSimul> simFalt = new ArrayList<ClaveStringSimul>();
//        ArrayList<Object> informacion = new ArrayList<Object>();
//        informacion.add(estudio);
//        informacion.add(this);
//        for (int ie = 1; ie <= estudio.getDatosGenerales().getCantEtapas(); ie++) {
//            for (NodoE ne : grafoE.getNodosE().get(ie)) {
//                if (ne.getSimulado() == Simulado.NO) {
//                    Parque par = (Parque) ne.getPort();
//                    TiempoAbsoluto ta = (TiempoAbsoluto)par.getPerTiempo();
//                    Parque parOpe = par.calculaParqueOperativo();
//                    parOpe.cargaResumenP(informacion);
//                    String resumenP = parOpe.getResumenP();
//                    ArrayList<ParTipoDatoValor> valoresTiposDatos = ne.getNodoN().getValoresTiposDatos();
//                    simFalt.add(new ClaveStringSimul(ta, resumenP, valoresTiposDatos));
//                    ne.setSimulado(Simulado.EN_PROCESO);
//                }
//            }
//        }
//        return simFalt;
//    }


    /**
     * Devuelve las restricciones heurísticas de la iteración it-ésima
     */
    public ConjRestGrafoE restHDeIter(int it) {
        return sucRestHParque.get(it - 1);
    }

    /**
     *  METODO leerCaso
     *  Lee los datos de un caso que están en archivos en el
     *  directorio del caso
     *
     *  Se lee solamente
     * - el conjunto de recursos y transformaciones base del caso (que debe ser un subconjunto
     *   de los del estudio)
     * - la expansión forzada del caso
     * - las restricciones significativas adicionales del caso, de parque y de inicio de construcción
     * - las restricciones heurísticas iniciales del caso
     *
     */
    public void leerCaso(boolean imprimir) throws XcargaDatos, IOException, IOException {

        directorio = estudio.getDirectorio() + "/" + nombre;
        DatosGeneralesEstudio datGen = new DatosGeneralesEstudio();
        datGen = estudio.getDatosGenerales();
        /**
         * Lee el objetivo del caso
         */
        boolean objEst = false;
        String archDatosGen = directorio + "/" + "V5-DatosGeneralesCaso.txt";
        CargaObjetivo.cargarObj(archDatosGen, this, estudio, objEst);
        /**
         * Lee los valores de tipos de datos fijos para el caso.
         * El caso no altera los tipos de datos aleatorios.
         */
        ArrayList<Object> informacion = new ArrayList<Object>();
        informacion.add(estudio);
        informacion.add(this);
        CargaTiposDeDatoCaso.cargarTiposDato(directorio, estudio.getConjTiposDatos(), informacion);


        /**
         * Calcula para todos los NodosN los valores de TipoDeDatos.
         * Este cálculo debe hacerse en cada Caso porque los valores de los
         * TipoDeDatos fijos cambian en cada Caso.
         */
        estudio.getGrafoN().cargaValoresTiposDatos(estudio, this);

        /**
         * Lee los datos de la sensibilidad del caso
         */
        String archSensibilidad = directorio + "/V5-DatosGeneralesCaso.txt";
        CargaSensibilidad.cargarSens(archSensibilidad, this);
        /**
         * Lee conjuntos de recursos y transformaciones base ELEGIBLES del caso
         */
        String dirRyTB = directorio + "/" + "V5-DatosGeneralesCaso.txt";
        CargaRyTBCaso cargadorRyTB = new CargaRyTBCaso() {};
        cargadorRyTB.cargarRyTBCaso(dirRyTB, subConjRB, subConjTB, estudio);

        /**
         * Crea los subconjuntos de elegibles
         */
        for (RecursoBase rb : subConjRB.getRecursosB()) {
            if (rb.getClase().equalsIgnoreCase(ExistEleg.ELEG)) {
                subConjRBEleg.getRecursosB().add(rb);
            }
        }
        for (TransforBase tb : subConjTB.getTransforsB()) {
            if (tb.getClase().equalsIgnoreCase(ExistEleg.ELEG)) {
                subConjTBEleg.getTransforsB().add(tb);
            }
        }
        /**
         * Lee la expansión forzada del caso y crea la expansión forzada total
         */
        String dirExpFor = directorio + "/" + "V7-expansionForzadaCaso.txt";
        int cantPasos = datGen.getCantPasos();
        CargaExpaForzada cargaEFC = new CargaExpaForzada() {};
        cargaEFC.cargarExpDef(dirExpFor, estudio, estudio.getConjRecB(), estudio.getConjTransB(), expansionForzadaCaso);
        expansionForzadaTotal = Expansion.suma2Exps(expansionForzadaCaso, estudio.getExpansionForzadaEstudio());
        
        /**
         * Lee las restricciones significativas adicionales del caso
         * para la programación dinámica
         */
        LectorRestE.cargaConjuntoR(conjRestSigParqueCaso,
                directorio + "/" + "V3-RestSigParque.txt",
                estudio.getConjRecB(), estudio.getConjTransB(), estudio);

        LectorRestE.cargaConjuntoR(conjRestSigInicioCaso,
                directorio + "/" + "V3-RestSigInicio.txt",
                estudio.getConjRecB(), estudio.getConjTransB(), estudio);

        LectorRestE.publicaConjRest(directorio + "/" + "/volcadoRestSigInicio.txt",
                "Rest sig inicio", conjRestSigInicioCaso);

        LectorRestE.publicaConjRest(directorio + "/" + "/volcadoRestSigParque.txt",
                "Rest sig parque", conjRestSigParqueCaso);

        /**
         * Lee las restricciones heurísticas iniciales del caso
         * para la programación dinámica que son todas de parque
         */
        LectorRestE.cargaRestH(sucRestHParque, directorio, PD,
                subConjRB, subConjTB, estudio);

        /**
         * Lee las restricciones heurísticas iniciales del caso
         * para el algoritmo genético que son todas de inicio y de cota
         */

        LectorRestE.cargaRestH(sucRestHAlGenInicioCota, directorio, ALGEN,
                subConjRB, subConjTB, estudio);


        /**
         * Crea las restricciones significativas resultantes para el caso y les carga
         * en el campo informacion
         */
        conjRestSigInicioResult.getRestricciones().addAll(estudio.getConjRestSigInicio().getRestricciones());
        conjRestSigInicioResult.getRestricciones().addAll(conjRestSigInicioCaso.getRestricciones());
        conjRestSigParqueResult.getRestricciones().addAll(estudio.getConjRestSigParque().getRestricciones());
        conjRestSigParqueResult.getRestricciones().addAll(conjRestSigParqueCaso.getRestricciones());

        for (RestGrafoE rest : conjRestSigInicioResult.getRestricciones()) {
            rest.setInformacion(informacion);
        }
        for (RestGrafoE rest : conjRestSigParqueResult.getRestricciones()) {
            rest.setInformacion(informacion);
        }

        /**
         * Carga las restricciones de cota de inicio cotaInf y cotaSup para 
         * recursos base y tranformaciones base elegibles.
         *
         * Verifica que todas las tranformacionesBase, recursosBase con demora
         * aleatoria de construcción y recursos que dependen de la edad o el tiempo
         * y que son elegibles tienen restricciones de inicio de construcción de cota.
         * Si no tienen cota inferior se supone cero.
         * Si no tienen cota superior se produce error.
         *
         * Para los demás recursosBase "normales" carga cotas inferior 0 y superior 99.
         */

        // PROCESA LAS TRANSFORMACIONES BASE
        for (TransforBase tb : subConjTBEleg.getTransforsB()) {
            boolean cargoinf = false;
            boolean cargosup = false;
            for (RestGrafoE ri : conjRestSigInicioResult.getRestricciones()) {
                if (ri instanceof RestCota3) {
                    RestCota3 ric = (RestCota3) ri;
                    if (ric.getTipoBloque().equalsIgnoreCase("TRANS") &&
                            ric.getObjeto().equals(tb)) {
                        
                        // la restricción es de cota de la transformación tb
                        //ArrayList<Integer> cotasPorEtapa = null;  
                        ArrayList<Integer> cotasPorEtapa = new ArrayList();
                        cotasPorEtapa.addAll(ric.getCota());
                        if (ric.getInfSup().equalsIgnoreCase("INF")) {
                            cotaInfInicioTBEleg.add(cotasPorEtapa);
                            cargoinf = true;
                        } else {
                            cotaSupInicioTBEleg.add(cotasPorEtapa);
                            cargosup = true;
                        }
                    }
                }
            }
            // Si el usuario no proporcionó cotas superiores para las transformacionesBase es un error
            if (!cargosup) {
                throw new XcargaDatos("La tranformaciónBase " +
                        tb.getNombre() + " no tiene cotas superiores de inicio de construcción");
            }

            // Si el usuario no proporcionó cotas inferiores para las transformacionesBase se pone cero
            if (!cargoinf) {
                ArrayList<Integer> cotasPorEtapa = new ArrayList<Integer>();
                for (int i = 0; i < datGen.getCantEtapas(); i++) {
                    cotasPorEtapa.add(0);
                }
                cotaInfInicioTBEleg.add(cotasPorEtapa);
            }
        }

        // PROCESA LOS RECURSOS BASE
        for (RecursoBase rb : subConjRBEleg.getRecursosB()) {
            boolean cargoinf = false;
            boolean cargosup = false;
            for (RestGrafoE ri : conjRestSigInicioResult.getRestricciones()) {
                if (ri instanceof RestCota3) {
                    RestCota3 ric = (RestCota3) ri;
                    if (ric.getTipoBloque().equalsIgnoreCase("REC") &&
                            ric.getObjeto().equals(rb)) {
                        // la restricción es de cota del recurso base reb
                        ArrayList<Integer> cotasPorEtapa = new ArrayList<Integer>();
                        cotasPorEtapa.addAll(ric.getCota());
                        if (ric.getInfSup().equalsIgnoreCase("INF")) {
                            cotaInfInicioRBEleg.add(cotasPorEtapa);
                            cargoinf = true;
                        } else {
                            cotaSupInicioRBEleg.add(cotasPorEtapa);
                            cargosup = true;
                        }
                    }
                }
            }
            if (!cargosup) {
                if (rb.getAdmiteCambioAleatorio()) {
                    throw new XcargaDatos("El recurso que admite cambio aleatorio " +
                            rb.getNombre() + " no tiene cotas superiores significativas de inicio de construcción");
                }
                if (!rb.getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.NO)) {
                    throw new XcargaDatos("El recurso que depende del tiempo" +
                            "o la edad " + rb.getNombre() + " no tiene cotas superiores significativas de inicio de construcción");
                }
                if (rb.getTieneDemoraAl()) {
                    throw new XcargaDatos("El recurso con demora aleatoria " +
                            rb.getNombre() + " no tiene cotas superiores significativas de inicio de construcción");
                }
                /**
                 * El recurso no admite cambio aleatorio, no depende de tiempo o edad,
                 * ni tiene demora aleatoria se carga una cota superior 99
                 */
                ArrayList<Integer> cotasPorEtapa = new ArrayList<Integer>();
                for (int i = 0; i < datGen.getCantEtapas(); i++) {
                    cotasPorEtapa.add(99);
                }
                cotaSupInicioRBEleg.add(cotasPorEtapa);
            }
            if (!cargoinf) {
                ArrayList<Integer> cotasPorEtapa = new ArrayList<Integer>();
                for (int i = 0; i < datGen.getCantEtapas(); i++) {
                    cotasPorEtapa.add(0);
                }
                cotaInfInicioRBEleg.add(cotasPorEtapa);
            }
        }

        if (imprimir) {
            String texto;
            texto = this.toString();
            System.out.print(texto);
        }
    }



    /**
     * Graba en el directorio del caso las restricciones heurísticas
     * corrientes, que son las iniciales si no se aplicó aún ninguna
     * iteración o las de la última iteración realizada.
     */
    public void grabaRestHProgDinCorrientes() throws IOException, XcargaDatos{
        // trae el último conjunto de restricciones heuristicas
        int niter = sucRestHParque.size();
        ConjRestGrafoE cResHCorr = restHDeIter(sucRestHParque.size()-1);
        String dirCaso = estudio.getNombre() + "/" + "CASO" + numeroCaso;
        LectorRestE.publicaRestHProgDin(cResHCorr, dirCaso, niter);
    }


    @Override
    public String toString() {
        String texto = "===================================================================" + "\n";
        texto += "COMIENZAN DATOS DEL CASO " + nombre + "\n";
        texto += "==================================================================" + "\n";
        texto += "OBJETIVO DEL CASO" + "\n" + obj.toString() + "\n";
        texto += "RECURSOS BASE DEL CASO" + "\n" + subConjRB.toString() + "\n";
        texto += "TRANFORMACIONES BASE DEL CASO" + "\n" + subConjTB.toString() + "\n";

        texto += "RESTRICCIONES RESULTANTES PARA EL CASO" + "\n";

        for (RestGrafoE r : conjRestSigInicioResult.getRestricciones()) {
            texto += r.toString();
        }
        for (RestGrafoE r : conjRestSigParqueResult.getRestricciones()) {
            texto += r.toString();
        }

        texto += "COTAS DE INICIO DE CONSTRUCCIÓN " + "\n";

        for (RecursoBase rb : subConjRBEleg.getRecursosB()) {
            int i = 0;
            texto += rb.getNombre() + "\n";
            texto += "Cotas inferiores por etapa: ";
            for (int e = 0; e < estudio.getDatosGenerales().getCantEtapas(); e++) {
                texto += cotaInfInicioRBEleg.get(i).get(e) + "\t";
            }
            texto += "\n";
            texto += "Cotas superiores por etapa: ";
            for (int e = 0; e < estudio.getDatosGenerales().getCantEtapas(); e++) {
                texto += cotaSupInicioRBEleg.get(i).get(e) + "\t";
            }
            i++;
            texto += "\n";
        }
        texto += expansionForzadaCaso.toString() + "\n";
        texto += "FIN DEL CASO " + "\n";
        texto += "===================================================================" + "\n";

        return texto;
    }

    public String toStringCorto() {
        String texto = "********************************************" + "\n";
        texto += "COMIENZAN DATOS DEL CASO " + nombre + "\n";
        texto = "********************************************" + "\n";

        texto += "Recursos base del caso" + "\n" + subConjRB.toStringCorto() + "\n";
        texto += "Tranformaciones base del caso" + "\n" + subConjTB.toStringCorto() + "\n";
        return texto;
    }

//    public static void main(String[] args) throws XcargaDatos, IOException {
//        String dirBase = "D:/Java/PruebaJava4";
//        String dirEstudio = dirBase + "/" + "Datos_Estudio";
//        String dirImplementacion = dirBase + "/" + "Datos_Implementacion";
//        String dirCorridas = dirBase + "/" + "corridas";
//        ImplementadorGeneral impG = new ImplementadorEDF();
//        impG.inicializaImplementador(dirImplementacion, dirEstudio, dirCorridas);
//
//        Estudio est = new Estudio("Estudio de prueba", 5, impG);
//        String directorio = "D:/Java/PruebaJava3";
//        boolean imprimir = true;
//        est.leerEstudio(directorio, imprimir);
//
//
//        Caso caso1 = new Caso(est, "CASO1");
//        String dirCaso1 = directorio + "/CASO1";
//        caso1.leerCaso(imprimir);
//
//        Parque pCero = caso1.creaPCero();
//        Parque pUno = caso1.creaPUno();
//
//        DatosGeneralesEstudio datGen = est.getDatosGenerales();
//        int cantEtapas = datGen.getCantEtapas();
//        double tasaDescPaso = datGen.getTasaDescPaso();
//        GrafoN grafoN = est.getGrafoN();
//        ArrayList<int[]> etapasPasos = datGen.getEtapasPasos();
//        ArrayList<Object> informacion = new ArrayList<Object>();
//        informacion.add(est);
//        informacion.add(caso1);
//        ArrayList<PeriodoDeTiempo> tiempoAbsDeEtapas = null;
//
//        for (int ie = 1; ie <= cantEtapas; ie++) {
//            tiempoAbsDeEtapas.add(datGen.tiempoAbsolutoDeEtapa(ie));
//        }
//
//        GrafoE grafoE = new GrafoE(
////                est,
//                cantEtapas, tasaDescPaso,
//                grafoN,
//                etapasPasos,
//                tiempoAbsDeEtapas,
//                pCero, pUno,
//                caso1.getConjRestSigParqueResult(),
//                caso1.getSucRestHParque().get(0),
//                caso1.getConjRestSigInicioResult(), informacion);
//        String texto = grafoE.toString();
//        System.out.print(texto);
//
//    }
}
