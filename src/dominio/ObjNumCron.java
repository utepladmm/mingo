/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import GrafoEstados.*;
import UtilFunciones.FLineal1V;
import UtilFunciones.Funcion1V;
import UtilitariosGenerales.CalculadorDePlazos;
import persistenciaMingo.XcargaDatos;

import java.util.ArrayList;

/**
 *
 * @author ut469262
 * Objetivo genérico crónico.
 *
 * El objetivo es:
 * Esperanza[ f (suma ponderada de costos de numerarios) ]
 * f es una función de modificación (funcModif) de costos para tener en cuenta la aversión al riesgo.
 * En cada crónica los costos de cada numerario se ponderan mediante un vector ponderadores
 *
 * ponderadores son los reales que ponderan cada numerario cuando se determina el
 * costo por crónica.
 *
 * funcModif es la función de modificación del costo que afecta la suma ponderada de
 * costos de numerarios. La función es única pero en cada paso de tiempo se le aplican distintos
 * parámetros.
 *
 *
 */
public class ObjNumCron extends Objetivo {

    private DatosGeneralesEstudio datGen;
    private ArrayList<Double> ponderadores;
    /**
     * Un valor ponderador para cada uno de los numerarios.
     */
    private Funcion1V funcModif;

    /**
     * Es la función de modificación a emplear, para reflejar el grado de aversión al riesgo.
     */
    public DatosGeneralesEstudio getDatGen() {
        return datGen;
    }

    public void setDatGen(DatosGeneralesEstudio datGen) {
        this.datGen = datGen;
    }

    public Funcion1V getFuncModif() {
        return funcModif;
    }

    public void setFuncModif(Funcion1V funcModif) {
        this.funcModif = funcModif;
    }

    public ArrayList<Double> getPonderadores() {
        return ponderadores;
    }

    public void setPonderadores(ArrayList<Double> ponderadores) {
        this.ponderadores = ponderadores;
    }

    /**
     ***************************************************************
     * COMIENZAN METODOS QUE SOBREESCRIBEN LOS DE OBJETIVO
     ***************************************************************
     */



    /**
     * Este método sobre-escribe el de Objetivo
     *
     * @param resSimul es un ResSimul resultado de una simulación
     * @return result es el escalar resultado del objetivo
     * El objetivo es:
     * Esperanza[ f (suma ponderada de costos de numerarios) ]
     * f es una función de modificación (funcModif) de costos para tener en cuenta la aversión al riesgo.
     * En cada crónica los costos de cada numerario se ponderan mediante un vector ponderadores
     *
     *
     */
    @Override
    public double evaluar(Object obResSimul, ArrayList<Object> informacion) throws XcargaDatos {
        ResSimul resSimul = (ResSimul) obResSimul;
        int cantCron = datGen.getCantCronicas();
        int cantNumerarios = datGen.getNumerarios().size();
        double result = 0.0;
        TiempoAbsoluto ta = (TiempoAbsoluto) resSimul.getPer();
        ArrayList<Double> pDepTiempo = paramDepTiempoDePer(ta);
        double[][] matrizResult = resSimul.getResult();
        double[] vectorProb = resSimul.getProbs();
        double valor1Cron;
        for (int icron = 0; icron < cantCron; icron++) {
            valor1Cron = 0.0;
            for (int inum = 0; inum < cantNumerarios; inum++) {
                valor1Cron += matrizResult[inum][icron] * ponderadores.get(inum);
            }
            valor1Cron = funcModif.evaluar(valor1Cron, pDepTiempo);
            result += valor1Cron * vectorProb[icron];
        }
        return result;
    }

    /**
     *
     * Recibe un conjunto de ResSimul, cada uno con una probabilidad
     * y crea los percentiles pedidos de la distribución de probabilidad conjunta.
     * Cada elemento de la distribución es:
     * f (suma ponderada de costos de numerarios)
     * 
     * La probabilidad de cada elemento de la distribución es la probabilidad del
     * ResSimul respectivo por la probabilidad de la realización dentro del ResSimul
     * 
     * f es una función de modificación (funcModif) de costos para tener en cuenta la aversión al riesgo.
     * En cada crónica los costos de cada numerario se ponderan mediante un vector ponderadores
     *
     * @param objsResSimul lista de ResSimul
     * @param probResSimul son la probabilidades respectivas de cada ResSimul de objsResSimul
     * @param vPercentiles son los percentiles de la distribución resultante que se piden
     * @param informacion
     * @return
     */
    @Override
    public ArrayList<Double> percentiles(ArrayList<Object> objsResSimul,
            ArrayList<Double> probResSimul,
            ArrayList<Double> vPercentiles,
            ArrayList<Object> informacion) throws XcargaDatos {

        ArrayList<Double> probabilidades = new ArrayList<Double>();
        ArrayList<Double> dPercentiles = new ArrayList<Double>();
        ArrayList<Double> datos = new ArrayList<Double>();
        int iRS = 0;
        for(Object oRS: objsResSimul){
            ResSimul rS = (ResSimul)oRS;
            double probRS = probResSimul.get(iRS);
            TiempoAbsoluto ta = (TiempoAbsoluto) rS.getPer();
            ArrayList<Double> pDepTiempo = paramDepTiempoDePer(ta);
            double[][] result = rS.getResult();
            double[] probDato = rS.getProbs();
            double dato;
            for(int id=0; id<result[0].length; id++){
                dato = 0.0;
                for(int ipond = 0; ipond<ponderadores.size(); ipond++){
                    dato += result[ipond][id]*ponderadores.get(ipond);
                }
                dato = funcModif.evaluar(dato, pDepTiempo);
                datos.add(dato);
                probabilidades.add(probRS*probDato[id]);
            }
            iRS++;
        }
        dPercentiles = UtilitariosGenerales.DistribucionesProbabilidad.percentiles(
                datos, probabilidades, vPercentiles, false);
        return dPercentiles;

    }

    /**
     ***************************************************************
     * Finalizan métodos que sobre-escriben los de Objetivo
     ***************************************************************
     */
    /**
     * Devuelve los parámetros de escala del PeriodoDeTiempo per
     */
    public ArrayList<Double> paramDepTiempoDePer(PeriodoDeTiempo per) throws XcargaDatos {
        TiempoAbsoluto ta = (TiempoAbsoluto) per;
        CalculadorDePlazos calcP = datGen.getCalculadorDePlazos();
        int avance = calcP.hallaAvanceTiempo(tAbsolutoBaseObjetivo, ta);
        if (avance < 0) {
            throw new XcargaDatos("Se pidió el valor de los parámetros del objetivo " +
                    this.toString() + " en un paso de tiempo = " + ta.toStringCorto() +
                    " anterior al tAbsolutoBaseObjetivo");
        }

        ArrayList<Double> result = new ArrayList<Double>();
        for (int ip = 0; ip < paramDepTiempo.size(); ip++) {
            if (avance >= paramDepTiempo.get(0).size()) {
                throw new XcargaDatos("Se pidió el valor de los parámetros del objetivo " +
                        this.toString() + " en un paso de tiempo = " + ta.toStringCorto() +
                        " posterior al último PasoDeTiempo con datos");

            }
            result.add(paramDepTiempo.get(ip).get(avance));
        }
        return result;
    }

    @Override
    public String toString() {
        String texto = "Objetivo del caso: " + "\n";
        texto += "Ponderadores de numerarios: ";
        for (int j = 0; j < ponderadores.size(); j++) {
            texto += ponderadores.get(j) + "\t";
        }
        texto += "\n";
        texto += "Función de modificación: " + "\n";
        texto += funcModif.toString() + "\n";
        texto += "Parámetros de escala de la función (cada fila un parámetro, cada columna un paso de tiempo): " + "\n";
        for (int i = 0; i < paramDepTiempo.size(); i++) {
            for (int j = 0; j < paramDepTiempo.get(i).size(); j++) {
                texto += paramDepTiempo.get(i).get(j) + "\t";
            }
            texto += "\n";
        }
        return texto;
    }


    public static void main(String[] args) throws XcargaDatos {



        ObjNumCron obj = new ObjNumCron();

        FLineal1V ident = new FLineal1V(Funcion1V.TipoF1V.LINEAL, 0.0, 1.0);

        ArrayList<Double> ponderadores = new ArrayList<Double>();
        ponderadores.add(1.0);
        obj.setPonderadores(ponderadores);
        obj.setFuncModif(ident);
        obj.setParamDepTiempo(null);
        TiempoAbsoluto ta = new TiempoAbsoluto(2011);

        ResSimul rS = new ResSimul();
        double [] prob = new double [5];
        double [][] dato = new double [1][5];
        for(int id = 0; id<=4; id++){
            dato[0][id]=1.0+0.3*id;
            prob[id]=0.2;
        }
        rS.setResult(dato);
        rS.setProbs(prob);


        ArrayList<Double> vPercentiles = new ArrayList<Double>();
        ArrayList<Double> dPercentiles = new ArrayList<Double>();
        ArrayList<Double> probsRS = new ArrayList<Double>();
        ArrayList<Object> aRes = new ArrayList<Object>();
        aRes.add(rS);
        probsRS.add(1.0);
        vPercentiles.add(0.0);
        vPercentiles.add(0.5);
        vPercentiles.add(1.0);
        ArrayList<Object> informacion = new ArrayList<Object>();


        dPercentiles = obj.percentiles(aRes, probsRS, vPercentiles, informacion);

        System.out.println(dPercentiles.get(0));
        System.out.println(dPercentiles.get(1));
        System.out.println(dPercentiles.get(2));
    }



}
