/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import AlmacenSimulaciones.ImplementadorGeneral;
import GrafoEstados.Numerario;
import ImplementaEDF.ImplementadorEDF;
import TiposEnum.DependenciaDeT;
import UtilitariosGenerales.CalculadorDePlazos;
//import dominio.Bloque.EstadosRec;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.NodoN;
import naturaleza3.VariableNat;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class Recurso extends RecOTrans implements Serializable {

     /**
      *  Es un recurso base existente en un parque dado y en un estado
      *  dado.
      *
      */
     private RecursoBase recBase;
     private String estado;    // OPE-operativo, DEM-demorado, CON-en contrucción
     private double valorInvAl;
     /**
      *	Si el recurso base admite aleatoriedad de la inversión el factor multiplicativo
      *   de la inversión de este recurso que resultó, de entre los posibles
      */
     private int valorDemAl;
     /**
      *	Si el recurso base admite demora aleatoria en la construcción
      * valor de la demora de este recurso que resultó, de entre los posibles en pasos de tiempo
      */
     private int indicadorTiempo;
     /**
      *  Si está operativo y tiene edad es el ordinal del paso de
      *  tiempo de vida empezando en 0, como la edad de las personas. Es decir la edad empieza en
      *  0, 1, 2, 3,..
      *  Si está operativo y no tiene edad se conviene en poner 0.
      *
      *  Si está en construcción es un número negativo igual a la cantidad de pasos
      *  faltantes hasta que el recurso completa su construcción. Por ejemplo, si
      *  un recurso demora tres pasos en construirse el indicador vale en pasos sucesivos
      *  -3, -2, -1, y cuando se completa la construcción pasa al paso 0 operativo.
      *
      *  Si está demorado el paso dentro de la demora se codifica igual que en el caso
      *  del plazo de construcción, pero en lugar del período de construcción se toma la
      *  demora adicional.
      */
     private ArrayList<CantProductoSimple> cantsProd;

     
     
     public Recurso(RecursoBase recBase, String estado, int indicadorTiempo) {
          this.recBase = recBase;
          this.estado = estado;
          /**
           * Si el recurso no admite edad y es OPE no se considera el indicadorTiempo
           * y se carga cero.
           */
          if (!recBase.getTieneEdad() & estado.equalsIgnoreCase("OPE")) {
               this.indicadorTiempo = 0;
          } else {
               this.indicadorTiempo = indicadorTiempo;

          }
          valorInvAl = 1.0;
          valorDemAl = 0;
          cantsProd = new ArrayList<CantProductoSimple>();          
     }

     public Recurso() {
          this.recBase = null;
          this.estado = null;
          this.indicadorTiempo = 0;
          valorInvAl = 1.0;
          valorDemAl = 0;
     }

     @Override
     public int getValorDemAl() {
          return valorDemAl;
     }

     public void setValorDemAl(int valorDemAl) {
          this.valorDemAl = valorDemAl;
     }

     @Override
     public double getValorInvAl() {
          return valorInvAl;
     }

     @Override
     public String getClase(){
         return recBase.getClase();
     }     
     
     public void setValorInvAl(Double valorInvAl) {
          this.valorInvAl = valorInvAl;
     }

     public String getEstado() {
          return estado;
     }

     public void setEstado(String estado) {
          this.estado = estado;
     }

     public int getIndicadorTiempo() {
          return indicadorTiempo;
     }

     public void setIndicadorTiempo(int indicadorTiempo) {
          this.indicadorTiempo = indicadorTiempo;
     }

     public RecursoBase getRecBase() {
          return recBase;
     }

     public void setRecBase(RecursoBase recBase) {
          this.recBase = recBase;
     }

    public ArrayList<CantProductoSimple> getCantsProd() {
        return cantsProd;
    }

    public void setCantsProd(ArrayList<CantProductoSimple> cantsProd) {
        this.cantsProd = cantsProd;
    }
     
     

//    public String nombreRecBase(){
//        return recBase.getNombre();
//    }
//     public void setEstadoString(String str) throws XcargaDatos {
//          if (str.equalsIgnoreCase("OPE")) {
//               estado = EstadosRec.OPE;
//          } else if (str.equalsIgnoreCase("CON")) {
//               estado = EstadosRec.CON;
//          } else if (str.equalsIgnoreCase("DEM")) {
//               estado = EstadosRec.DEM;
//          } else {
//               throw new XcargaDatos("Error en estado de recurso: " + str);
//          }
//     }
    

     /**
      *  Halla la potencia nominal de cada módulo del RecursoBase de un Recurso,
      *  cualquiera sea su forma de dependencia del tiempo o de la edad.
      *  Los módulos del recurso son los del recursoBase.
      *
      *  Devuelve cero cuando el tiempo absoluto
      *  ta es anterior a tiempoAbsolutoBase
      *  o bien ta es posterior al ultimoTiempoAbsoluto
      *  del recursoBase asociado al recurso.
      *
      *  Extrapola los valores del último ta con dato hasta ultimoTiempoAbsoluto*
      *
      *  Si el recurso depende de la edad ta es ignorado
      *
      **/
     public double potNom1Mod(DatosGeneralesEstudio datGen,
             TiempoAbsoluto ta) throws XcargaDatos {
          double pot = 0.0;
          if (! recBase.getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.EDAD)) {
               pot = recBase.potNom1ModDeTiempoAbsoluto(datGen, ta);
          } else {
               pot = recBase.potNom1ModDeEdad(indicadorTiempo);
          }
          return pot;
     }

     /**
      *  Halla la cantidad de módulos del RecursoBase del Recurso cualquiera sea su
      *  forma de dependencia del tiempo o de la edad.
      *
      *  Devuelve cero cuando el tiempo absoluto
      *  ta es anterior a tiempoAbsolutoBase
      *  o bien ta es posterior al ultimoTiempoAbsoluto
      *  del recursoBase asociado al recurso
      *
      *  Extrapola los valores del último ta con dato hasta ultimoTiempoAbsoluto
      *
      *  Si el recurso depende de la edad ta es ignorado
      *
      **/
     public int cantMod(DatosGeneralesEstudio datGen,
             TiempoAbsoluto ta) throws XcargaDatos {
          CalculadorDePlazos cP = datGen.getCalculadorDePlazos();
          TiempoAbsoluto ultTA = recBase.getUltimoTiempoAbsoluto();
          TiempoAbsoluto tABase = recBase.getTiempoAbsolutoBase();
          int cant = 0;
          if (cP.hallaAvanceTiempo(tABase, ta) < 0) {
               if ((ultTA != null) && cP.hallaAvanceTiempo(ta, ultTA) < 0) {
                    return 0;
               }
          }
          if (!recBase.getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.EDAD)) {
               cant = recBase.cantModulosDeTiempoAbsoluto(datGen, ta);
          } else {
               cant = recBase.cantModulosDeEdad(indicadorTiempo);
          }
          return cant;
     }

     /**
      *  Halla el costo fijo en cada numerario por cada módulo
      *  del RecursoBase del Recurso cualquiera sea su
      *  forma de dependencia del tiempo o de la edad.
      *
      *
      *  Devuelve cero cuando el tiempo absoluto
      *  ta es anterior a tiempoAbsolutoBase
      *  o bien ta es posterior al ultimoTiempoAbsoluto
      *  del recursoBase asociado al recurso
      *
      *  Extrapola los valores del último ta con dato hasta ultimoTiempoAbsoluto
      *
      *  Si el recurso depende de la edad ta es ignorado
      *
      **/
     public ArrayList<Double> costoFijo1Mod(Estudio est,
             TiempoAbsoluto ta) throws XcargaDatos {


          if (! recBase.getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.EDAD)) {
               return recBase.costoFijo1ModDeTiempoAbsoluto(est, ta);
          } else {
               return recBase.costoFijo1ModDeEdad(est, indicadorTiempo);
          }

     }

     /**
      *  Halla el costo fijo en cada numerario por cada módulo
      *  del RecursoBase del Recurso cualquiera sea su
      *  forma de dependencia del tiempo o de la edad.
      *
      *
      *  Devuelve cero cuando el tiempo absoluto
      *  ta es anterior a tiempoAbsolutoBase
      *  o bien ta es posterior al ultimoTiempoAbsoluto
      *  del recursoBase asociado al recurso
      *
      *  Extrapola los valores del último ta con dato hasta ultimoTiempoAbsoluto
      *
      *  Si el RecursoBase depende de la edad ta es ignorado
      *
      *  Si el RecursoBase tiene costos fijos aleatorios se considera
      *  el valor de la variable aleatoria respectiva en el NodoN nN, de
      *  lo contrario es ignorada.
      *
      **/
     public ArrayList<Double> costoFijo1Mod(Estudio est,
             TiempoAbsoluto ta, NodoN nN) throws XcargaDatos {
         double multCFijo = 1.0;
         ArrayList<Double> cFijos;
         ArrayList<Double> result = new ArrayList<Double>();
         if(recBase.getTieneCFijAleat()){
              VariableNat vN = recBase.getVNCFijAl();
              multCFijo = Double.parseDouble(nN.valorDeUnaVN(vN));
          }

          if (! recBase.getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.EDAD)) {
               cFijos = recBase.costoFijo1ModDeTiempoAbsoluto(est, ta);
          } else {
               cFijos = recBase.costoFijo1ModDeEdad(est, indicadorTiempo);
          }
          for (double d: cFijos){
              result.add(d*multCFijo);
          }
          return result;
     }
     


     
     

     /**
      * Devuelve la cantidad de producto simple del producto pr del recurso
      * no importa cuál sea su forma de dependencia del tiempo. Es el total del
      * Recurso es decir que tiene en cuenta la cantidad de módulos del RecursoBase.
      *
      * Sólo los recursos operativos son capaces de ofertar o demandar
      * cantidades de producto
      * 
      * Para el caso de los RecursoBase que no dependen de la edad, se toma en cuenta las fechas
      * de entrada de modúlos y de variación de las potencias nominales contenidas en los DatoT
      * cantModulosDT y potenciaNominalDT del RecursoBase, si los datos se entraron por fechas.
      */
     public CantProductoSimple cantProdSimp(Producto pr, DatosGeneralesEstudio datGen,
             TiempoAbsoluto ta) throws XcargaDatos {
          CantProductoSimple cP = new CantProductoSimple(pr);

          if (estado.equalsIgnoreCase("OPE")) {
               int cM = cantMod(datGen, ta);
               double pN = potNom1Mod(datGen, ta);
               cP.setCant(recBase.cantBaseUnProd1Mod(pr).getCant());
               /**
                * En los recursos base que dependen del tiempo o de la edad
                * la cantidad de producto crece en proporción al aumento de la
                * potencia nominal de cada módulo y de la cantidad de módulos
                */
               double escalar = cM * pN / recBase.getPotNominalTABase();
               cP.escalarCant(escalar);
               return cP;
          } else {
               cP.setCant(0.0);
               return cP;
          }

     }

     /**
      * Crea una copia del Recurso corriente (this) como un nuevo objeto
      * que puede modificarse sin alterar el original
      * @return copia
      */
     public Recurso copiaRecurso() {
          Recurso copia = new Recurso(recBase, estado, indicadorTiempo);
          copia.setValorInvAl(valorInvAl);
          copia.setValorDemAl(valorDemAl);
          return copia;
     }

     @Override
     public boolean equals(Object obj) {
          if (obj == null) {
               return false;
          }
          if (getClass() != obj.getClass()) {
               return false;
          }
          final Recurso other = (Recurso) obj;
          if (this.recBase != other.recBase && (this.recBase == null || !this.recBase.equals(other.recBase))) {
               return false;
          }
          if (!this.estado.equalsIgnoreCase(other.estado)) {
               return false;
          }
          if (this.valorInvAl != other.valorInvAl) {
               return false;
          }
          if (this.valorDemAl != other.valorDemAl) {
               return false;
          }
          if (this.indicadorTiempo != other.indicadorTiempo) {
               return false;
          }
          return true;
     }

     @Override
     public int hashCode() {
          int hash = 5;
          hash = 23 * hash + (this.recBase != null ? this.recBase.hashCode() : 0);
          hash = 23 * hash + (this.estado != null ? this.estado.hashCode() : 0);
//          hash = 23 * hash + (this.estado != null ? EstadosRec.hashSustituto(this.estado) : 0);
          hash = 23 * hash + (int) (Double.doubleToLongBits(this.valorInvAl) ^ (Double.doubleToLongBits(this.valorInvAl) >>> 32));
          hash = 23 * hash + this.valorDemAl;
          hash = 23 * hash + this.indicadorTiempo;
          return hash;
     }

     @Override
     public String toString() {
          String texto = "COMIENZA DESCRIPCION DE RECURSO ";
          texto += recBase.toString() + "\n";
          texto += "estado: " + estado.toString() + "\n";
          texto += "indicadorTiempo: " + indicadorTiempo + "\n";
          texto += "valor inversión aleatoria " + valorInvAl + "\n";
          texto += "valor demora aleatoria " + valorDemAl + "\n";
          texto += "\n";
          return texto;
     }

     @Override
     public String toStringCorto() {
          String texto = "Recurso:  ";
          texto += recBase.getNombre() + "--";
          texto += "estado: " + estado.toString() + "--";
          texto += "indicadorTiempo: " + indicadorTiempo + "--";
          texto += "valor inversión aleatoria " + valorInvAl + "--";
          texto += "valor demora aleatoria " + valorDemAl + "--";
          return texto;
     }

     @Override
     public String toStringMuyCorto(boolean usaEquiv) {
          String texto = "";
          if(usaEquiv && recBase.tieneEquivEnSimul()){
                texto += recBase.getEquivEnSimul().getNombre();
          }else{
            texto += recBase.getNombre() + "/";
          }
          texto += estado.toString() + "/";
          texto += "t" + indicadorTiempo + "/";
          if(recBase.getTieneInvAleat()) texto += valorInvAl + "/";
          if(recBase.tieneDemoraAl()) texto += valorDemAl + "/";          
          return texto;
     }

     public static void main(String[] args) throws IOException, XcargaDatos {

          String dirBase = "D:/Java/PruebaJava4";
          String dirEstudio = dirBase + "/" + "Datos_Estudio";
          String dirImplementacion = dirBase + "/" + "Datos_Implementacion";
          String dirCorridas = dirBase + "/" + "corridas";
          ImplementadorGeneral impG = new ImplementadorEDF();
          impG.inicializaImplementador(dirImplementacion, dirEstudio, dirCorridas);

          Estudio est = new Estudio("Estudio de prueba", 5, impG);
          String directorio = "D:/Java/PruebaJava2";
          est.leerEstudio(directorio, true);


          RecursoBase rb1 = new RecursoBase(est);
          RecursoBase rb2 = new RecursoBase(est);
          rb1 = est.getConjRecB().getUnRecB("5a");
          rb2 = est.getConjRecB().getUnRecB("6a");

          Recurso ra = new Recurso();
          Recurso rb = new Recurso();
          ra.setRecBase(rb1);
          ra.setEstado("OPE");
          ra.setIndicadorTiempo(1);

          rb.setRecBase(rb2);
          rb.setEstado("OPE");
          rb.setIndicadorTiempo(1);

          System.out.println(ra.toStringCorto());
          System.out.println(rb.toStringCorto());

          TiempoAbsoluto ta = new TiempoAbsoluto(2012);

          double pota = ra.potNom1Mod(est.getDatosGenerales(), ta);
          int canta = ra.cantMod(est.getDatosGenerales(), ta);
          CantProductoSimple cantPS;
          Producto pr = est.getDatosGenerales().devuelveProducto("potenciaFirme");
          cantPS = ra.cantProdSimp(pr, est.getDatosGenerales(), ta);
          String texto = "potencia " + pota + " - cant modulos " + canta + "\n" +
                  cantPS.toString();
          System.out.print(texto);
     }
}
