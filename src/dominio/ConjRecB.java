/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import TiposEnum.ExistEleg;
import persistenciaMingo.XcargaDatos;

//import dominio.RecursoBase.TipoRec;
import java.util.ArrayList;
import java.io.Serializable;
import java.util.Collections;

/**
 *
 * @author ut600232
 */
public class ConjRecB implements Serializable {

    private ArrayList<RecursoBase> recursosB;
    
    private int iterador;

    public ConjRecB() {
        recursosB = new ArrayList<RecursoBase>();
        iterador = 0;
    }

    public void agregar(RecursoBase recB) {
        recursosB.add(recB);
    }

    public void remover(String nomRecB) {
        for (RecursoBase rB : recursosB) {
            if (nomRecB.compareToIgnoreCase(rB.getNombre()) == 0) {
                recursosB.remove(rB);
            }
        }
    }

    public ArrayList<RecursoBase> getRecursosB() {
        return recursosB;
    }

    public void setRecursosB(ArrayList<RecursoBase> recursosB) {
        this.recursosB = recursosB;
    }

    public RecursoBase getUnRecB(String nom) {
        /**
         *  Si el conjunto de recursosBase contiene uno de nombre
         *  nom, devuelve dicho recursoBase, si el conjunto si no lo
         *  contiene incluso porque está vacio devuelve null
         */
        RecursoBase recB = null;
        if (!recursosB.isEmpty()) {
            for (RecursoBase rB : recursosB) {
                if (rB.getNombre().equalsIgnoreCase(nom)) {
                    recB = rB;
                }
            }
        }
        return recB;
    }
     /**
         *  Si el conjunto de recursosBase contiene uno de nombre
         *  nom, o cuyo nombre comienza con el String nom y tiene otros caracteres después
         *  devuelve dicho recursoBase, si el conjunto si no lo
         *  contiene incluso porque está vacio devuelve null
         */
        public RecursoBase getUnRecBTr(String nom) { 
            RecursoBase recB = null;
            if (!recursosB.isEmpty()) {
                for (RecursoBase rB : recursosB) {
                    String nRB = rB.getNombre();
                    if (nRB.equalsIgnoreCase(nom)) {
                        recB = rB;
                        break;
                    }else{
                        int largoNom = nom.length();
                        if(nRB.length()>largoNom){
                            if(nRB.substring(0, largoNom).equalsIgnoreCase(nom)){
                                recB = rB;
                                break;
                            }
                        }                    
                    }
                }
            }
            return recB;
        }
        
        
    public void iniciaIterador(){
        iterador = 0;        
    }
    
    public RecursoBase siguienteRB(){ 
        RecursoBase rb;
        if(iterador>=recursosB.size()){
            rb = null;
        }else{
            rb = recursosB.get(iterador);        
        }
        iterador ++;
        return rb;
    }

    /**
     * Devuelve el equivalente en simulación de un RecursoBase rb
     * y lanza una excepción si rb no tiene equivalente en simulación.
     * @param rb el RecursoBase cuyo equivalente se busca.
     * @return
     */
    public RecursoBase equivEnSimul(RecursoBase rb) throws XcargaDatos{
        if(!rb.tieneEquivEnSimul()) throw new XcargaDatos("Se pidió el equivalente en simulación" +
                "de un RecursoBase que no tiene equivalente en simulación");
        return getUnRecB(rb.getNombreEquivEnSimul());
    }

    /**
     * A partir del conjunto de recursosBase this
     * crea el conjunto de recursosBase contenido en recB
     * que son generadores. Si no hay ningún generador
     * devuelve un conjunto sin generadores.
     */
    public ConjRecB creaConjGeneradores() { 
        ConjRecB conjGen = new ConjRecB();
        for (RecursoBase rB : this.getRecursosB()) {
            if (rB.getTipo().equalsIgnoreCase("GEN")) {
                conjGen.agregar(rB);
            }
        }
        return conjGen;
    }

    /**
     * A partir del conjunto de recursosBase this
     * crea el conjunto de recursosBase contenido en recB
     * que son cargas. Si no hay ningún generador
     * devuelve un conjunto sin generadores.
     */
    public ConjRecB creaConjCargas() {       
        ConjRecB conjGen = new ConjRecB();
        for (RecursoBase rB : this.getRecursosB()) {
            if (rB.getTipo().equalsIgnoreCase("CARG")) {
                conjGen.agregar(rB);
            }
        }
        return conjGen;
    }

    public void ordenaConjunto() {
        Collections.sort(recursosB);
    }

    @Override
    public String toString() {
        String texto = "====================================================" + "\n";
        texto += "COMIENZA CONJUNTO DE RECURSOS BASE " + "\n";
        texto += "====================================================" + "\n";

        int i;
        for (i = 0; i < recursosB.size(); i++) {
            if (recursosB.get(i) instanceof GeneradorTermico) {
                texto += ((GeneradorTermico) recursosB.get(i)).toString() + "\n\n\n";

            } else if (recursosB.get(i) instanceof GeneradorEolico) {
                texto += ((GeneradorEolico) recursosB.get(i)).toString() + "\n\n\n";
            } else if (recursosB.get(i) instanceof GeneradorHidro) {
                texto += ((GeneradorHidro) recursosB.get(i)).toString() + "\n\n\n";
            } else if (recursosB.get(i) instanceof ImportEstac) {
                texto += ((ImportEstac) recursosB.get(i)).toString() + "\n\n\n";
            } else if (recursosB.get(i) instanceof ImportFormul) {
                texto += ((ImportFormul) recursosB.get(i)).toString() + "\n\n\n";
            } else if (recursosB.get(i) instanceof ExportEstac) {
                texto += ((ExportEstac) recursosB.get(i)).toString() + "\n\n\n";
            } else if (recursosB.get(i) instanceof ExportFormul) {
                texto += ((ExportFormul) recursosB.get(i)).toString() + "\n\n\n";
            }
        }
        return texto;
    }

    public String toStringCorto() {
        String texto = "====================================================" + "\n";
        texto += "COMIENZA CONJUNTO DE RECURSOS BASE " + "\n";
        texto += "====================================================" + "\n";
        for (RecursoBase rB : recursosB) {
            texto += rB.getNombre() + "\t";
        }
        return texto;
    }

    public static void main(String[] args) {
        RecursoBase tg = new RecursoBase();
        tg.setClase(ExistEleg.ELEG);
        tg.setNombre("TG120");

        RecursoBase cc = new RecursoBase();
        cc.setClase(ExistEleg.ELEG);
        cc.setNombre("CC360");

        RecursoBase sB = new RecursoBase();
        sB.setClase(ExistEleg.EXIST);
        sB.setNombre("SALAB");

        RecursoBase ia = new RecursoBase();
        ia.setClase(ExistEleg.ELEG);
        ia.setNombre("IARSPFO");

        RecursoBase eo = new RecursoBase();
        eo.setClase(ExistEleg.ELEG);
        eo.setNombre("EOLO250");

        System.out.println("tg cc" + tg.compareTo(cc) + cc.compareTo(tg));
        System.out.println("tg SALAB" + tg.compareTo(sB) + sB.compareTo(tg));
        System.out.println("tg eo" + tg.compareTo(eo) + eo.compareTo(tg));
        System.out.println("sB cc" + sB.compareTo(cc) + cc.compareTo(sB));
        System.out.println("tg SALAB" + tg.compareTo(sB) + sB.compareTo(tg));
        System.out.println("eo CC" + eo.compareTo(cc) + cc.compareTo(eo));

        System.out.println("Strings tg cc" + "TG120".compareToIgnoreCase("CC360") + "CC360".compareToIgnoreCase("TG120"));

    }
}

