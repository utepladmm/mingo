/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import GrafoEstados.ConjDeDecision;
import GrafoEstados.ConjDetPortE;
import GrafoEstados.GrafoE;
import GrafoEstados.NodoE;
import GrafoEstados.Objetivo;
import GrafoEstados.PeriodoDeTiempo;
import GrafoEstados.Portafolio;
import TiposEnum.EstadosRec;
import dominio.RecursoBase;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import naturaleza3.ConjNodosN;
import naturaleza3.EscenarioCN;
import naturaleza3.ListaEscenariosCN;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author D230513
 */
public class RecorredorReportes {

    /**
     * Genera y graba en disco los reportes de trayectorias óptimas
     * @param grafoE
     * @param obj
     * @param listaEsc
     * @param dirResultCaso
     * @throws XcargaDatos
     * @throws SQLException 
     */
    public static void generaReportes(GrafoE grafoE, Objetivo obj, ListaEscenariosCN listaEsc, String dirResultCaso)
            throws XcargaDatos {

        Connection conex = null;
        Statement st = null; 
        try{
            Class.forName("org.sqlite.JDBC");
            conex = DriverManager.getConnection("jdbc:sqlite:" + dirResultCaso + "/DatosAnuales.db");
        }catch(Exception e3){
            throw new XcargaDatos("No se pudo conectar a la DB:"+dirResultCaso + "/DatosAnuales.db");
        }
                       
        ArrayList<Object> informacion = grafoE.getInformacion();
        Estudio est = (Estudio) informacion.get(0);
        String cabezalFila;
        ResultSet rs;
        String archDB = dirResultCaso + "/DatosAnuales.db";
        // Determina cantidad de fuentes en la base de datos
      
        int cantFuentes;    
        try {            
            st = conex.createStatement();       
            rs = st.executeQuery("select count(id_fuente) from Fuentes;");
            rs.next();
            cantFuentes = rs.getInt(1);
            rs.close();
            st.close();
        } catch (Exception e) {
            throw new XcargaDatos("Error en la consulta de la cantidad de fuentes");
        } 
        
        int cantPosibles = est.getCantRecNoComunesYTransPosibles();
        int cantRNC = est.getCantRecNoComunes();
        int cantEtapas = grafoE.getCantEtapas();
        int cantFilDec = est.cantRecBaseEleg() + est.cantTransBaseEleg();
        int cantSubtipos1 = TiposEnum.SubTipo1.lista.length;
        int nfilp = 15 + cantPosibles + cantFilDec;
        int ncol = 1 + cantEtapas;
        int nfile = 6 + cantFuentes + cantSubtipos1;
        int nfilc = 6 + cantFuentes + cantSubtipos1;


       
        /**
         * Carga los todos los datos por paso de tiempo
         */
        int iesc = 1;
        for (EscenarioCN esc : listaEsc.getEscenarios()) {

            String[][] portsOpt = new String[nfilp][ncol];
            String[][] energias = new String[nfile][ncol];
            String[][] costos = new String[nfilc][ncol];   
            // Borra el archivo de resultados si preexiste
            String archTextoRes = dirResultCaso + "/ResultEscenario" + iesc +".txt";
            if(UtilitariosGenerales.DirectoriosYArchivos.existeArchivo(archTextoRes)){
                UtilitariosGenerales.DirectoriosYArchivos.eliminaArchivo(archTextoRes);
            }      
            // Crea la primera columna                        
            int ifil = 0;
            /**
             * Genera primera columna de todos
             */
            // portsOpt
            portsOpt[ifil][0] ="RESULTADOS ESCENARIO = " + iesc;  
            ifil++;                
            portsOpt[ifil][0] ="========================";        
            ifil++;                
            portsOpt[ifil][0] = "PARQUE OPTIMO";
            ifil++;
            for (int ir = 1; ir <= est.getCantRecNoComunes(); ir++) {
                Recurso rec = (Recurso) est.recNoComunYTransPosible(ir - 1);
                cabezalFila = rec.getRecBase().getNombre() + " "
                        + rec.getEstado() + "\t IT="
                        + rec.getIndicadorTiempo() + "\t DA="
                        + rec.getValorDemAl() + "\t IA=" + rec.getValorInvAl();
                portsOpt[ifil][0] = cabezalFila;
                ifil++;
            }
            for (int ir = cantRNC +1; ir <= cantPosibles; ir++) {
                Transformacion tra = (Transformacion) est.recNoComunYTransPosible(ir - 1);
                cabezalFila = tra.getTransBase().getNombre() + " "
                        + tra.getEstado() + "\t IT="
                        + tra.getIndicadorTiempo() + "\t DA="
                        + tra.getValorDemAl() + "\t IA=" + tra.getValorInvAl();
                portsOpt[ifil][0] = cabezalFila;
                ifil++;
            }            
            
            
            portsOpt[ifil][0] = "DECISIÓN OPTIMA";
            ifil++;
            est.getConjRecBEleg().iniciaIterador();
            RecursoBase rb;
            while ((rb = est.getConjRecBEleg().siguienteRB()) != null) {
                portsOpt[ifil][0] = rb.getNombre();
                ifil++;
            }
            TransforBase tb;
            while ((tb = est.getConjTransBEleg().siguienteRB()) != null) {
                portsOpt[ifil][0] = tb.getNombre();
                ifil++;
            }
            portsOpt[ifil][0] = "VALOR DE BELLMAN DEL CD";        
            ifil++;
            portsOpt[ifil][0] = "COSTO ESPERADO GENERALIZADO DEL CD";        
            ifil++;
            portsOpt[ifil][0] = "INVERSION";        
            ifil++;        
            portsOpt[ifil][0] = "COSTOS FIJOS";        
            ifil++;  
            portsOpt[ifil][0] = "NOTA: los valores del Bellman se calculan al inicio del paso de tiempo, el resto de valores en el instante medio";        
            ifil++;              
            /**
             * Inicia la creación la matriz de ENERGIAS
             * Primero se carga la primer columna de nombres 
             * Luego se realiza un loop en los años para cargar las energías 
             */ 
            ifil = 0;       
            energias[ifil][0] = "********** ENERGIAS ( GWh )************";
            costos[ifil][0]="********** COSTOS  ( MUSD )************";   
            ifil++;
            energias[ifil][0] = "PALIER(fuente en DB)";
            costos[ifil][0]="PALIER (fuente en DB)";
            ifil++;
            try {
                st = conex.createStatement();
                for (String subt1 : TiposEnum.SubTipo1.lista) {
                    rs = st.executeQuery("select id_fuente, recursoBase from Fuentes where subtipo1= '" + subt1 + "';");
                    while (rs.next()) {
                        energias[ifil][0] = rs.getString("recursoBase");
                        costos[ifil][0] = rs.getString("recursoBase");
                        ifil++;
                    }
                    energias[ifil][0] = "TOTAL " + subt1;
                    costos[ifil][0] = "TOTAL" + subt1;
                    rs.close();
                    ifil ++;
                }
                rs = st.executeQuery("select id_fuente, recursoBase from Fuentes where tipo= 'FALLA';");
                while (rs.next()) {
                    energias[ifil][0] = rs.getString("recursoBase");
                    costos[ifil][0] = rs.getString("recursoBase");
                    ifil++;
                }
                energias[ifil][0] = "TOTAL FALLA"; 
                costos[ifil][0] = "TOTAL FALLA"; 
                ifil++;
                rs.close();
                rs = st.executeQuery("select id_fuente, recursoBase from Fuentes where tipo= 'DEMANDA';");
                while (rs.next()) {
                    energias[ifil][0] = rs.getString("recursoBase");
                    costos[ifil][0] = rs.getString("recursoBase");
                    ifil++;
                }
                energias[ifil][0] = "TOTAL DEMANDA";
                costos[ifil][0] = "TOTAL COSTOS DE MAQUINAS (SIN SC_gas)";
                ifil++;
                rs.close();
                st.close();
            } catch (Exception e) {
            	e.printStackTrace();
                throw new XcargaDatos("Error en la consulta de la cantidad de fuentes");                
            } finally {
            }            
            ConjDeDecision cDUno = grafoE.getBolsaCD().devuelveCDInicial();
            ConjDetPortE cDPEe = cDUno.getConjsDetP().get(0);
            for (int e = 1; e <= cantEtapas; e++) {
                /**
                 * cDPNe es el conjunto determinante de parque en grafoN, es por
                 * lo tanto un ConjNodosN que tienen el mismo estado de las variables
                 * de la naturaleza observadas y de las que determinan parque (si existen)
                 *
                 * cDPEe es el conjunto determinante de parque en grafoE, conjunto de
                 * nodosE tales que tienen el mismo sucesor en e+1 si en e+1 el estado de
                 * la naturaleza es el mismo
                 *
                 * cDe es el conjunto de decisión en e
                 * porte es el portafolio del cd en e
                 *
                 */
                PeriodoDeTiempo pt = grafoE.getPerTiempoDeEtapas().get(e);

                ConjNodosN cDPNe = esc.getConjsNodosN().get(e);

                ConjDeDecision cDe = cDPEe.getConjDec();
                Portafolio porte = cDe.getPort();
                Parque parque = (Parque) porte;
                int[] listaIdCorrDelCDPE = new int[cDe.getNodos().size()];
                // Se carga los id_corrida de las corridas del cDPE
                int il=0;
                for (NodoE nE : cDPEe.getNodosE()) {
                    String noms = nE.getNombreSimulacion();
                    try{
                        st=conex.createStatement();
                        rs=st.executeQuery("select id_corrida from Corridas where dirCorrida = '"+noms+"';");
                        rs.next();
                        listaIdCorrDelCDPE[il] = rs.getInt(1);
                        rs.close();
                        st.close();
                    }catch(Exception e2 ){
                        throw new XcargaDatos("No encontró id_corrida de: "+noms);
                    }
                    il++;
                }

                /**
                 * Comienza los reportes de un año 
                 */
                // Carga el año
                int anio = est.getDatosGenerales().tiempoAbsolutoDeEtapa(e).getAnio();

                portsOpt[2][e] = String.valueOf(anio);
                // Carga el Parque óptimo  
                ifil = 3;
                for (int ir = 0; ir < cantPosibles; ir++) {
                    portsOpt[ifil][e] = String.valueOf(parque.codigoDeInd(ir));
                    ifil++;
                }
                ifil++;
                // Carga la Decision optima
                if (e < cantEtapas) {
                    Parque dec = (Parque) cDe.decOptima(obj);
                    est.getConjRecBEleg().iniciaIterador();
                    while ((rb = est.getConjRecBEleg().siguienteRB()) != null) {
                        portsOpt[ifil][e] = String.valueOf(dec.cantDeUnRecBaseCE(rb, EstadosRec.CON, informacion));
                        ifil++;
                    }
                    est.getConjTransBEleg().iniciaIterador();
                    while ((tb = est.getConjTransBEleg().siguienteRB()) != null) {
                        portsOpt[ifil][e] = String.valueOf(dec.cantDeUnaTransBaseCE(tb, EstadosRec.CON, informacion));
                        ifil++;
                    }
                }else{
                    est.getConjRecBEleg().iniciaIterador();
                    while ((rb = est.getConjRecBEleg().siguienteRB()) != null) {
                        portsOpt[ifil][e] = " ";
                        ifil++;
                    }
                    /**NO entra en la transformación base ?¿?¿
                    while ((tb = est.getConjTransBEleg().siguienteRB()) != null) {
                        portsOpt[ifil][e] = " ";
                        ifil++;
                    }
                     */
                }
                
                // Carga el Valor de Bellman del CD
                portsOpt[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(cDe.valBellmanDeObj(obj),2);
                ifil++;
                
                if(est.getDatosGenerales().getTipoCargosFijos().equalsIgnoreCase(DatosGeneralesEstudio.ANUALIDADES)){
	                // Carga el costo generalizado del paso
	                portsOpt[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(cDe.costoEspPasoDeObj(obj),2);
	                ifil++;                
	                // Carga las anualidades
	                portsOpt[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(parque.anualidadPond(est, obj),2);        
	                ifil++;         
	                // Carga los costos fijos
	                double cf=0;
	                for (NodoE nE : cDPEe.getNodosE()){
	                    cf+=parque.costoFijoPond(est, obj, nE.getNodoN())*nE.getNodoN().getProbCondDadoCDP();
	                }
	                portsOpt[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(cf,2);           
                }else{
	                // Carga el costo generalizado del paso, NO SE APLICA
	                portsOpt[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(0.0,2);
	                ifil++;                
	                // Carga la inversi�n actualizada
	                double inv=0.0;
	                if(cDe.getNodos().get(0).getOtrosResultadosPaso().size()>1) {
	                	inv = cDe.getNodos().get(0).getOtrosResultadosPaso().get(1); 
	                }
	                portsOpt[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(inv,2);        
	                ifil++;         
	                // Carga los costos fijos
	                double cf=0;
	                for (NodoE nE : cDe.getNodos()){
	                	double cf1=0.0;
	                	if(cDPEe.getNodosE().get(0).getOtrosResultadosPaso().size()>1) cf1 = cDe.getNodos().get(0).getOtrosResultadosPaso().get(2); 
	                    cf+=cf1*nE.getNodoN().getProbCondDadoCDP();
	                }
	                portsOpt[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(cf,2); 
                	
                	
                }
                // Carga las energías en cada año
                ifil = 1;
                energias[ifil][e] = Integer.toString(anio);
                costos[ifil][e] = Integer.toString(anio);
                double ctot=0.0;
                ifil++;
                try {
                    st = conex.createStatement();
                    for (String subt1 : TiposEnum.SubTipo1.lista) {
                        double sumae = 0.0;
                        double sumac = 0.0;
                        rs = st.executeQuery("select id_fuente, recursoBase from Fuentes where subtipo1= '" + subt1 +"';");
                        while (rs.next()) {
                            int idfuente = rs.getInt("id_fuente");
                            double acume = 0.0;
                            double acumc = 0.0;
                            il = 0;
                            for (NodoE nE : cDPEe.getNodosE()) {
                                String cons = "select avg(energiaAn), avg(costoAn) from "
                                        + "EnergiasYCostos natural join Fuentes natural join Localizadores natural join Corridas"
                                        + " where id_corrida = " + listaIdCorrDelCDPE[il]
                                        + " and anio = "+anio+" and id_fuente = "+idfuente+";";
                                Statement st1 = conex.createStatement();
                                ResultSet rs1=st1.executeQuery(cons);
                                rs1.next();
                                double ener1=rs1.getDouble(1);
                                double cos1=rs1.getDouble(2);
                                acume+= ener1*nE.getNodoN().getProbCondDadoCDP();
                                acumc+=cos1*nE.getNodoN().getProbCondDadoCDP();
                                il ++;                     
                            }
                            energias[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(acume,2);
                            costos[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(acumc,2);
                            ifil++;
                            sumae += acume; 
                            sumac += acumc;
                        }
                        energias[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(sumae,2);
                        costos[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(sumac,2);
                        ctot+=sumac;
                        ifil ++;
                        rs.close();
                    }
                    rs = st.executeQuery("select id_fuente, recursoBase from Fuentes where tipo= 'FALLA';");
                    double sumfe=0.0;
                    double sumfc=0.0;
                    while (rs.next()) {
                        int idfalla = rs.getInt("id_fuente");
                        double acumfe=0.0;
                        double acumfc=0.0;
                        il=0;
                        Statement stf = conex.createStatement();
                        for (NodoE nE : cDPEe.getNodosE()){
                            String cons = "select avg(energiaAn), avg(costoAn) from "
                                        + "EnergiasYCostos natural join Fuentes natural join Localizadores natural join Corridas"
                                        + " where id_corrida = " + listaIdCorrDelCDPE[il]
                                        + " and anio = "+anio+" and id_fuente = "+idfalla+";";
                            ResultSet rsf = stf.executeQuery(cons);
                            rsf.next();
                            double efalla= rsf.getDouble(1);
                            double cfalla= rsf.getDouble(2);
                            acumfe+=efalla*nE.getNodoN().getProbCondDadoCDP();
                            acumfc+=cfalla*nE.getNodoN().getProbCondDadoCDP();
                            il++;
                        }
                        stf.close();
                        energias[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(acumfe,2);
                        costos[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(acumfc,2);
                        sumfe+=acumfe;
                        sumfc+=acumfc;
                        ifil++;
                    }
                    energias[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(sumfe,2);
                    costos[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(sumfc,2);
                    ctot+=sumfc;
                    ifil ++;
                    rs.close();
                    rs = st.executeQuery("select id_fuente, recursoBase from Fuentes where tipo like '%DEM%';");
                    double sumd=0.0;
                    while (rs.next()) {
                        int idDem = rs.getInt("id_fuente");
                        double acumd=0.0;
                        il=0;
                        Statement std = conex.createStatement();
                        for (NodoE nE : cDPEe.getNodosE()){
                            String cons = "select avg(energiaAn) from "
                                        + "EnergiasYCostos natural join Fuentes natural join Localizadores natural join Corridas"
                                        + " where id_corrida = " + listaIdCorrDelCDPE[il]
                                        + " and anio = "+anio+" and id_fuente = "+idDem+";";
                            ResultSet rsd = std.executeQuery(cons);
                            rsd.next();
                            double edem= rsd.getDouble(1);
                            acumd+=edem*nE.getNodoN().getProbCondDadoCDP();
                            il++;
                        }
                        std.close();
                        energias[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(acumd,2);
                        sumd+=acumd;
                        ifil++;
                    }
                    //Carga el los totales de la demanda
                    energias[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(sumd,2);
                    costos[ifil][e] = UtilitariosGenerales.Numeros.redondeaDouble(ctot,2);
                    ifil++;
                    rs.close();
                    st.close();
            
                } catch (Exception e1) {
                    throw new XcargaDatos("Error en el cargado de Energías");
                } finally {
                    
                }

                /**
                 * Genera el ConjDetPortE sucesor cDPEemas1 salvo que se esté en la última etapa
                 */
                if (e < grafoE.getCantEtapas()) {
                    ConjNodosN cDPNemas1 = esc.getConjsNodosN().get(e + 1);
                    ConjDetPortE cDPEemas1 = cDPEe.cDPortSucesor(cDPNe, cDPNemas1, obj);
                    cDPEe = cDPEemas1;
                }        
            }
            UtilitariosGenerales.DirectoriosYArchivos.agregaTexto(archTextoRes, portsOpt, ",");
            UtilitariosGenerales.DirectoriosYArchivos.agregaTexto(archTextoRes, energias, ",");                
            UtilitariosGenerales.DirectoriosYArchivos.agregaTexto(archTextoRes, costos, ",");
            iesc++;
        }
        try {
            conex.close();
        } catch (SQLException ex) {
            throw new XcargaDatos("No pudo cerrar la conexión");
        }
        /**
         * Graba los textos de los dos archivos de salida
         */

//        UtilitariosGenerales.DirectoriosYArchivos.grabaTexto(archExpan, textoExpan);
//        UtilitariosGenerales.DirectoriosYArchivos.grabaTexto(archResum, textoResum);
    }
}
