/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

//import AlmacenSimulaciones.BolsaDeSimulaciones2;
import AlmacenSimulaciones.BolsaDeSimulaciones3;
import AlmacenSimulaciones.ImplementadorGeneral;
import GrafoEstados.ConjDeDecision;
import GrafoEstados.Portafolio;
import GrafoEstados.ConjRestGrafoE;
import GrafoEstados.NodoE;
import GrafoEstados.Objetivo;
import GrafoEstados.ResSimul;
import GrafoEstados.RestGrafoE;
import Restricciones3.RestCota3;
import Restricciones3.RestDeProducto3;
import Restricciones3.RestDeProducto3.InfSup;
import TiposEnum.DependenciaDeT;
import TiposEnum.EstadosRec;
import TiposEnum.TipoBloque;
import TiposEnum.TipoRec;
import UtilitariosGenerales.CalculadorDePlazos;
import UtilitariosGenerales.EnumeradorLexicografico;
import UtilitariosGenerales.Numeros;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JOptionPane;
import naturaleza3.ConjNodosN;
import naturaleza3.GrafoN;
import naturaleza3.MatrizTransicion;
import naturaleza3.NodoN;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 * La clase Parque hereda de Portafolio
 * En el ArrayList<Object> informaciÃ³n asociado a los parques hay:
 * - en get(0) el estudio
 * - el get(1) el caso
 ****************************************************************
 *
 * ATENCION EL HASH Y EL EQUALS DE PARQUE NO TIENEN EN CUENTA
 * ETAPA Y PERIODO DE TIEMPO
 *
 * *************************************************************
 */
public class Parque extends Portafolio implements Serializable {

        /**
     * Son los bloques de Recursos que:
     * - Proceden de RecursosBase EXIST es decir existentes o predeterminados del problema.
     * - No son origen de TransforBase.
     * - No son objeto de cambio aleatorio.
     * POR LO TANTO SU EVOLUCIÃ“N ESTÃ� PREDETERMINADA ES DECIR:
     * la evoluciÃ³n de estos bloques es comÃºn a todos los parques del GrafoE
     * y su estado sÃ³lo depende del TiempoAbsoluto o etapa.
     * 
     * El primer get es la etapa. get(0) es la etapa 1.
     */
    private static ArrayList<ArrayList<Bloque>> bloquesComunes;

    
    /**
     * Es un cÃ³digo de enteros donde los valores ordenados indican la cantidad de Recursos
     * y de Transformaciones en ese orden, de las que aparecen en el Estudio en
     * recElegPosibles y transElegPosibles respectivamente.
     * 
     */
    private int[] cBloques;
    
    
    private static int[] cBloquesAux;


    static {
        bloquesComunes = new ArrayList<ArrayList<Bloque>>();
    }



    /**
     * GENERA UN PARQUE A PARTIR DE UN CODIGO DE ENTEROS
     * @param e etapa
     * @param ta tiempoAbsoluto
     * @param cBloques es un cÃ³digo de Parque.
     */
    public Parque(int e, int[] cBloques) {
        super(e);
        this.cBloques = cBloques;
    }

    /**
     * GENERA UN PARQUE A PARTIR DE BLOQUES DE Recursos y Tranformaciones
     * @param e
     * @param ta
     * @param bloquesAL 
     */
    public Parque(Estudio est, int e, TiempoAbsoluto ta, ArrayList<Bloque> bloquesAL) throws XcargaDatos, XBloqueMultNeg {
        super(e);
        int dimArray = est.getCantRecNoComunesYTransPosibles();
        cBloques = new int[dimArray];
        for (Bloque b : bloquesAL) {
            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC)) {
                Recurso rec = b.getRecurso();
                int indice = est.indiceEnPosibles(rec);
                cBloques[indice] = cBloques[indice] + b.getMultiplicador();
                if (cBloques[indice] < 0) {
                    throw new XBloqueMultNeg("Etapa " + e + "Cantidad de recursos negativa de " + rec.toStringMuyCorto(false));
                }
            } else {
                Transformacion trans = b.getTransformacion();
                int indice = est.indiceEnPosibles(trans);
                cBloques[indice] = cBloques[indice] + b.getMultiplicador();
                if (cBloques[indice] < 0) {
                    throw new XBloqueMultNeg("Etapa " + e + "Cantidad de Tranformaciones negativa de " + trans.toStringMuyCorto(false));
                }
            }
        }
    }
    
    /**
     * Crea una copia del Parque this que puede alterarse sin cambiar this.
     */
    public Parque clona(){
        int[] copiaCodigo = new int[cBloques.length];
        System.arraycopy(cBloques, 0, copiaCodigo, 0, cBloques.length);
        Parque pCopia = new Parque(etapa, copiaCodigo);
        return pCopia;
        
    }


//    @Override
//    public String getNombreSimul() {
//        return nombreSimul;
//    }
//
//    @Override
//    public void setNombreSimul(String nombreSimul) {
//        this.nombreSimul = nombreSimul;
//    }


    public int[] getCBloques() {
        return cBloques;
    }

    public void setCBloques(int[] cBloques) {
        this.cBloques = cBloques;
    }

    
    
    
    
    /**
     * Devuelve el TiempoAbsoluto en que vive un Parque
     * 
     * @param est es el estudio en el que vive el Parque.
     */    
    public TiempoAbsoluto tiempoAbsolutoDeParque(Estudio est) throws XcargaDatos{
        return est.tiempoAbsolutoDeEtapa(etapa);        
    }
    

//    /**
//     * Devuelve la cantidad de unidades del RecursoBase rb que hay en el parque
//     * que se encuentran en Recursos con EstadoRec estado.
//     * INCLUYE TANTO LOS RECURSOS COMUNES COMO NO COMUNES !!!!!!!!!!!!!!!!
//     * @param rb es el RecursoBase a contar.
//     * @param estado es el valor de EstadosRec, incluso TODOS, que se busca.
//     * @return
//     */
//    public int cantDeUnRecBase(RecursoBase rb, String estado, ArrayList<Object> informacion) throws XcargaDatos {
//        int result = 0;
//        Estudio est = (Estudio) informacion.get(0);       
//        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
//        ArrayList<Bloque> bloques;
//        // cuenta los RecursoBase de los bloques comunes
//        bloques = bloquesComunesDeTA(ta, est);
//        boolean todos = false;
//        if (estado.equalsIgnoreCase(EstadosRec.TODOS)) {
//            todos = true;
//        }
//        for (Bloque b : bloques) {
//            boolean este = false;
//            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC) && b.getRecurso().getRecBase() == rb) {
//                if (b.estadoDeRecOTrans().equalsIgnoreCase(estado)) {
//                    este = true;
//                }
//                if (todos | este) {
//                    result = result + b.getMultiplicador();
//                }
//            }
//        }
//        // cuenta los RecursoBase del cÃ³digo del Parque en cBloques
//        int indice = rb.getPosicionRecursoInicial();
//        while (indice<est.getCantRecNoComunesYTransPosibles() && 
//                est.getRecNoComunesYTransPosibles().get(indice).esDeRB(rb)) {
//            Recurso rec = (Recurso) est.getRecNoComunesYTransPosibles().get(indice);
//            boolean este = false;
//            if (rec.getEstado().equalsIgnoreCase(estado)) {
//                este = true;
//            }
//            if (todos | este) {
//                result = result + cBloques[indice];
//            }
//            indice++;
//        }
//        return result;
//    }
//    
    
    
    /**     
     * Devuelve la cantidad de unidades del RecursoBase rb que hay en el parque
     * que se encuentran en Recursos con EstadoRec estado.
     * 
     * 
     * INCLUYE TANTO LOS RECURSOS COMUNES COMO NO COMUNES !!!!!!!!!!!!!!!!
     * @param rb es el RecursoBase a contar.
     * @param estado es el valor de EstadosRec, incluso TODOS, que se busca.
     * @return
     */
    public int cantDeUnRecBaseCE(RecursoBase rb, String estado, ArrayList<Object> informacion) throws XcargaDatos {        
        int result = 0;
        Estudio est = (Estudio) informacion.get(0);       
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
        boolean todos = false;
        if (estado.equalsIgnoreCase(EstadosRec.TODOS)) {
            todos = true;
        }        
        // cuenta los RecursoBase de los bloques comunes
        ArrayList<Bloque> bloques;        
        bloques = bloquesComunesDeTA(ta, est);          
        for (Bloque b : bloques) {
            boolean este = false;
            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC) && b.getRecurso().getRecBase() == rb) {
                if (b.estadoDeRecOTrans().equalsIgnoreCase(estado)) {
                    este = true;
                }
                if (todos | este) {
                    result = result + b.getMultiplicador();
                }
            }
        }        
        // cuenta los RecursoBase del cÃ³digo de enteros del Parque
        int indice = rb.getPosicionRecursoInicial();
        while (indice<est.getCantRecNoComunesYTransPosibles() && 
                est.getRecNoComunesYTransPosibles().get(indice).esDeRB(rb)) {
            Recurso rec = (Recurso) est.getRecNoComunesYTransPosibles().get(indice);
            boolean este = false;
            if (rec.getEstado().equalsIgnoreCase(estado)) {
                este = true;
            }
            if (todos | este) {
                result = result + cBloques[indice];
            }
            indice++;
        }
        return result;
    }
        
    /**     
     * Devuelve la cantidad de unidades de la TransforBase tb que hay en el parque
     * que se encuentran en Transformasiones con EstadoRec estado.
     * 
     * 
     * @param tb es la TransforBase a contar.
     * @param estado es el valor de EstadosRec, incluso TODOS, que se busca.
     * @return
     */
    public int cantDeUnaTransBaseCE(TransforBase tb, String estado, ArrayList<Object> informacion) throws XcargaDatos {        
        int result = 0;
        Estudio est = (Estudio) informacion.get(0);       
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
        boolean todos = false;
        if (estado.equalsIgnoreCase(EstadosRec.TODOS)) {
            todos = true;
        }        
        // cuenta las TransforBase del cÃ³digo de enteros del Parque
        int indice = tb.getPosicionTransformacionInicial();
        while (indice<est.getCantRecNoComunesYTransPosibles() && 
                est.getRecNoComunesYTransPosibles().get(indice).esDeTB(tb)) {
            Transformacion trans = (Transformacion) est.getRecNoComunesYTransPosibles().get(indice);
            boolean este = false;
            if (trans.getEstado().equalsIgnoreCase(estado)) {
                este = true;
            }
            if (todos | este) {
                result = result + cBloques[indice];
            }
            indice++;
        }
        return result;
    }

    /**
     * Devuelve un Parque cuyo cÃ³digo cBloques contiene sÃ³lo los 
     * Recursos OPE.
     * @return
     * @throws XcargaDatos 
     */
    public Parque calculaParqueOperativo(ArrayList<Object> informacion) throws XcargaDatos {
        Estudio est = (Estudio) informacion.get(0);
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);

        int[] cBloquesOp = new int[est.getCantRecNoComunesYTransPosibles()];
        int indice = 0;
        ArrayList<RecOTrans> posibles = est.getRecNoComunesYTransPosibles();
        for (RecOTrans rot : posibles) {
            if (rot instanceof Recurso) {
                ((Recurso) rot).getEstado().equalsIgnoreCase(EstadosRec.OPE);
                cBloquesOp[indice] = cBloques[indice];
            } else {
                cBloquesOp[indice] = 0;
            }
            indice ++;
        }
        Parque parqueOp = new Parque(etapa, cBloquesOp);
        return parqueOp;
    }
    
    /**
     * Altera el parque cambiando en el cÃ³digo de enteros cBloques, cada RecursoBase
     * por su equivalente.
     * @param informacion
     * @throws XcargaDatos 
     */
    public void cambiaPorRBEquiv(ArrayList<Object> informacion) throws XcargaDatos {
        Estudio est = (Estudio) informacion.get(0);
        ArrayList<RecOTrans> posibles = est.getRecNoComunesYTransPosibles();
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
        int[] cBloquesEquiv = new int[est.getCantRecNoComunesYTransPosibles()];
        // los Recursos que aparecen o se restan porque se generan a partir de otros equivalentes 
        boolean cBloquesVacio = true;
        for(int i=0; i<cBloques.length; i++){
            if(cBloques[i]!=0)cBloquesVacio = false;
            cBloquesEquiv[i] = 0;
        }
        if(!cBloquesVacio){
                for(int i=0; i<cBloques.length; i++){
                    RecOTrans rot = posibles.get(i);
                    RecursoBase rB = rot.devuelveRB();
                    if(rB!= null  && rB.tieneEquivEnSimul()){ 
                        // rot es un Recurso y tiene equivalente en simulaciÃ³n
                        RecursoBase rBE = rB.getEquivEnSimul();
                        Recurso recE = ((Recurso)rot).copiaRecurso();
                        recE.setRecBase(rBE);
                        int indiceEquiv = est.indiceEnPosibles(recE);
                        cBloquesEquiv[indiceEquiv] = cBloquesEquiv[indiceEquiv] + cBloques[i];
                        cBloquesEquiv[i] = -cBloques[i];
                    }
                }
                for(int i=0; i<cBloques.length; i++){
                    cBloques[i] = cBloquesEquiv[i] + cBloques[i];
                }
        }

    }    


    /**
     * Obtiene el Parque suma de dos Parques dados.
     * El Parque suma vive en la etapa del primer sumando.
     * Los objetos del Parque suma son nuevos, se pueden cambiar sin alterar
     * los Parques sumandos.
     *
     * @param par1 es el primer Parque a sumar
     * @param par2 el el segundo Parque a sumar
     * @return devuelve el Parque suma
     */
    public static Parque sumaParques(Parque par1, Parque par2) throws XcargaDatos, XBloqueMultNeg {
        assert par1.getCBloques().length == par2.getCBloques().length : "Parques de distinta dimension";
        int largo = par1.getCBloques().length;
        int[] cSuma = new int[largo];
        for (int i = 0; i < largo; i++) {
            cSuma[i] = par1.getCBloques()[i] + par2.getCBloques()[i];
            if (cSuma[i] < 0) {
                throw new XBloqueMultNeg("Suma de Recursos o Tranformaciones es negativa");
            }
        }
        Parque suma = new Parque(par1.getEtapa(), cSuma);
        return suma;
    }

    /**
     * Crea un parque copia equal al original pero con
     * nuevos objetos Bloque, Recursos y Tranformacion.
     * SÃ³lo toma los mismos objetos RecursoBase o TransforBase
     * La copia puede ser manipulada sin que el parque original se vea afectado,
     * mientras no se alteren los RecursoBase o TransforBase.
     * SE CONSIDERAN SOLO LOS BLOQUES NO COMUNES.     
     */
    public static Parque creaCopia(Parque parIni) throws XcargaDatos {
        int etapaIni = parIni.getEtapa();
        int[] cBloquesCopia = new int[parIni.getCBloques().length];
        System.arraycopy(parIni.getCBloques(), 0, cBloquesCopia, 0, parIni.getCBloques().length);
        Parque copia = new Parque(etapaIni, cBloquesCopia);
        copia.setEtapa(parIni.getEtapa());
        return copia;
    }

    /**
     * Crea una lista de Bloques correspondiente al cÃ³digo de bloques del Parque
     * sin importar que los cÃ³digos sean nulos o negativos.
     * @param est
     * @return bloques
     */
    public ArrayList<Bloque> bloquesDelParque(Estudio est) throws XcargaDatos {
        ArrayList<RecOTrans> posibles = est.getRecNoComunesYTransPosibles();
        ArrayList<Bloque> bloques = new ArrayList<Bloque>();
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
        int indice = 0;
        Bloque blo = null;
        for (RecOTrans rot : posibles) {
            if (cBloques[indice] != 0) {
                if (rot instanceof Recurso) {
                    Recurso rcopia = ((Recurso) rot).copiaRecurso();
                    blo = new Bloque(rcopia, cBloques[indice], ta);
                } else {
                    Transformacion tcopia = ((Transformacion) rot).copiaTrans();
                    blo = new Bloque(tcopia, cBloques[indice], ta);
                }
                bloques.add(blo);                
            }
            indice++;
        }
        return bloques;
    }
    
    
    /**
     * Devuelve el cÃ³digo entero del Parque en la posiciÃ³n i-Ã©sima 
     */
    public int codigoDeInd(int i){
        return cBloques[i];
    }
    
    /**
     * Carga el valor entero cod en la posiciÃ³n i-Ã©sima del cÃ³digo del Parque
     */
    public void cargaCodigo(int i, int cod){
        cBloques[i] = cod;
    }

    
   /**
     * Devuelve el cÃ³digo entero de codigoAux de la clase Parque en la posiciÃ³n i-Ã©sima 
     */
    public static int codigoDeIndAux(int i){
        return cBloquesAux[i];
    }
    
    /**
     * Carga el valor entero cod en la posiciÃ³n i-Ã©sima del cÃ³digo del Parque
     */
    public static void cargaCodigoAux(int i, int cod) throws XcargaDatos{
        if (cod<0) {
            throw new XcargaDatos("CÃ³digo negativo de un parque");
        }
        cBloquesAux[i] = cod;
    }    

//    /**
//     * Calcula la cantidad de producto de un producto pr que ofrecen los Generadores de un parque,
//     * tomando los Recursos con un EstadoRec dado. No se restan los requerimientos de las Cargas
//     * TODOS quiere decir cualquier EstadoRec.
//     * 
//     * SE INCLUYE EN EL CÃ�LCULO LOS BLOQUES COMUNES Y NO COMUNES  !!!!!!!!!!!!!!
//     *
//     * @param pr es el producto.
//     * @param estado es el EstadoRec (que puede ser TODOS los EstadosRec).
//     */
//    public CantProductoSimple cantProdGenParque(Producto pr, String estado,
//            ArrayList<Object> informacion) throws XcargaDatos {
//        Estudio est = (Estudio) informacion.get(0);
//        DatosGeneralesEstudio datGen = est.getDatosGenerales();
//        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
//        ArrayList<Bloque> bloques = bloquesDelParque(est);
//        boolean todos = false;
//        if (estado.equalsIgnoreCase(EstadosRec.TODOS)) {
//            todos = true;
//        }
//        CantProductoSimple suma = new CantProductoSimple(pr, 0.0);
//        
//        for (Bloque b : bloquesComunesDeTA(ta, est)) {
//            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC) && b.getRecurso().getRecBase().getTipo().equalsIgnoreCase("GEN")) {
//                if (b.estadoDeRecOTrans().equalsIgnoreCase(estado) || todos) {
//                    CantProductoSimple cant1B = b.cantProd(pr, datGen, ta);
//                    suma.sumaCant(cant1B);
//                }
//            }
//        }        
//        
//        for (Bloque b : bloques) {
//            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC) && b.getRecurso().getRecBase().getTipo().equalsIgnoreCase("GEN")) {
//                if (b.estadoDeRecOTrans().equalsIgnoreCase(estado) || todos) {
//                    CantProductoSimple cant1B = b.cantProd(pr, datGen, ta);
//                    suma.sumaCant(cant1B);
//                }
//            }
//        }
//        return suma;
//    }

    
    /**
     * Calcula la cantidad de producto de un producto pr que ofrecen los Generadores de un parque,
     * tomando los Recursos con un EstadoRec dado. No se restan los requerimientos de las Cargas
     * TODOS quiere decir cualquier EstadoRec.
     * SE INCLUYE EN EL CÃ�LCULO LOS BLOQUES COMUNES Y NO COMUNES  !!!!!!!!!!!!!!
     * 
     * 
     * @param pr es el producto.
     * @param estado es el EstadoRec (que puede ser TODOS los EstadosRec).
     */
    public CantProductoSimple cantProdGenParqueCE(Producto pr, String estado,
            ArrayList<Object> informacion) throws XcargaDatos {
        Estudio est = (Estudio) informacion.get(0);
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);

        boolean todos = false;
        if (estado.equalsIgnoreCase(EstadosRec.TODOS)) {
            todos = true;
        }
        CantProductoSimple suma = new CantProductoSimple(pr, 0.0);  
        // Suma el producto de los bloques comunes
        for (Bloque b : bloquesComunesDeTA(ta, est)) {
            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC) && b.getRecurso().getRecBase().getTipo().equalsIgnoreCase("GEN")) {
                if (b.estadoDeRecOTrans().equalsIgnoreCase(estado) || todos) {
                    CantProductoSimple cant1B = b.cantProd(pr, datGen, ta);
                    suma.sumaCant(cant1B);
                }
            }
        }              
        // Sume el producto de los cÃ³digos enteros del Parque
        int indice = 0;
        int mult = 0;
        while (indice<est.getCantRecNoComunes()) {            
            Recurso rec = (Recurso) est.getRecNoComunesYTransPosibles().get(indice);
            if(rec.getRecBase().getTipo().equalsIgnoreCase(TipoRec.GEN) ){
                boolean este = false;
                if (rec.getEstado().equalsIgnoreCase(estado)) {
                    este = true;
                }
                if (todos | este) {
                    mult = cBloques[indice];                
                }
                CantProductoSimple cant1rec = rec.cantProdSimp(pr, datGen, ta).clona();
                cant1rec.escalarCant(mult);
                suma.sumaCant(cant1rec); 
            }
            indice++;
        }        
        return suma;
    }    
    
    
//    /**
//     * Devuelve la cantidad de producto del producto pr requerida por los Recursos
//     * del parque que son cargas y se encuentra en el estado estado
//     * SE CONSIDERAN TODOS LOS BLOQUES COMUNES Y NO COMUNES !!!!!!!!!!!!!!!!
//     * @param pr
//     * @param estado
//     * @param datGen
//     * @return
//     * @throws XcargaDatos
//     */
//    public CantProductoSimple reqProdCargasParque(Producto pr, String estado,
//            ArrayList<Object> informacion) throws XcargaDatos {
//        Estudio est = (Estudio) informacion.get(0);
//        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
//        DatosGeneralesEstudio datGen = est.getDatosGenerales();
//        ArrayList<Bloque> bloques = bloquesDelParque(est);
//        boolean todos = false;
//        if (estado.equalsIgnoreCase(EstadosRec.TODOS)) {
//            todos = true;
//        }
//        CantProductoSimple suma = new CantProductoSimple(pr, 0.0);
//        for (Bloque b : bloques) {
//            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC) && b.getRecurso().getRecBase().getTipo().equalsIgnoreCase("CARG")) {
//                if (b.estadoDeRecOTrans().equalsIgnoreCase(estado) || todos) {
//                    suma.sumaCant(b.cantProd(pr, datGen, ta));
//                }
//            }
//        }
//        for (Bloque b : bloquesComunesDeTA(ta, est)) {
//            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC) && b.getRecurso().getRecBase().getTipo().equalsIgnoreCase("CARG")) {
//                if (b.estadoDeRecOTrans().equalsIgnoreCase(estado) || todos) {
//                    suma.sumaCant(b.cantProd(pr, datGen, ta));
//                }
//            }
//        }
//        return suma;
//    }
    
    
    
    /**
     * Calcula la cantidad de producto de un producto pr que requieren las Cargas de un parque,
     * tomando los Recursos con un EstadoRec dado. 
     * TODOS quiere decir cualquier EstadoRec.
     * SE INCLUYE EN EL CÃ�LCULO LOS BLOQUES COMUNES Y NO COMUNES  !!!!!!!!!!!!!!
     * 
     * SUPONE RECURSOS COMUNES DADOS POR PARQUES CON CÃ“DIGOS ENTEROS Y NO BLOQUES
     * 
     * @param pr es el producto.
     * @param estado es el EstadoRec (que puede ser TODOS los EstadosRec).
     */
    public CantProductoSimple reqProdCargasParqueCE(Producto pr, String estado,
            ArrayList<Object> informacion) throws XcargaDatos {
        Estudio est = (Estudio) informacion.get(0);

        String tipo = TipoRec.CARG;
        CantProductoSimple suma;  
        suma = reqProdCE(pr, estado, tipo, informacion);
        return suma;
    }    

    /**
     * Calcula la cantidad de producto de un producto pr que ofrecen los Generadores de un parque,
     * tomando los Recursos con un EstadoRec dado. No se restan los requerimientos de las Cargas
     * TODOS quiere decir cualquier EstadoRec.
     * SE INCLUYE EN EL CÃ�LCULO LOS BLOQUES COMUNES Y NO COMUNES  !!!!!!!!!!!!!!
     * 
     * SUPONE RECURSOS COMUNES DADOS POR PARQUES CON CÃ“DIGOS ENTEROS Y NO BLOQUES
     * 
     * @param pr es el producto.
     * @param estado es el EstadoRec (que puede ser TODOS los EstadosRec).
     */
    public CantProductoSimple reqProdGenParqueCE(Producto pr, String estado,
            ArrayList<Object> informacion) throws XcargaDatos {
        Estudio est = (Estudio) informacion.get(0);

        String tipo = TipoRec.GEN;
        CantProductoSimple suma;  
        suma = reqProdCE(pr, estado, tipo, informacion);
        return suma;
    }        
    
    
    /**
     * Calcula la cantidad de producto de un producto pr que ofrecen o requieren respectivamente 
     * los Generadores o Cargas de un parque,
     * tomando los Recursos con un EstadoRec dado. 
     * TODOS quiere decir cualquier EstadoRec.
     * SE INCLUYE EN EL CÃ�LCULO LOS BLOQUES COMUNES Y NO COMUNES  !!!!!!!!!!!!!!
     * 
     * SUPONE RECURSOS COMUNES DADOS POR PARQUES CON CÃ“DIGOS ENTEROS Y NO BLOQUES
     * 
     * @param pr es el producto.
     * @param estado es el EstadoRec (que puede ser TODOS los EstadosRec).
     * @param tipo es el TipoRec (CARG o GEN)
     */    
    public CantProductoSimple reqProdCE(Producto pr, String estado, String tipo,
            ArrayList<Object> informacion) throws XcargaDatos {
        Estudio est = (Estudio) informacion.get(0);
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
        boolean todos = false;
        if (estado.equalsIgnoreCase(EstadosRec.TODOS)) {
            todos = true;
        }
        CantProductoSimple suma = new CantProductoSimple(pr, 0.0);  
        // Suma el producto de los bloques comunes
        for (Bloque b : bloquesComunesDeTA(ta, est)) {
            if (b.getTipoBloque().equalsIgnoreCase(TipoBloque.REC) && b.getRecurso().getRecBase().getTipo().equalsIgnoreCase(tipo)) {
                if (b.estadoDeRecOTrans().equalsIgnoreCase(estado) || todos) {
                    CantProductoSimple cant1B = b.cantProd(pr, datGen, ta);
                    suma.sumaCant(cant1B);
                }
            }
        }              
        // Sume el producto de los cÃ³digos enteros del Parque
        int indice = 0;
        int mult = 0;
        while (indice<est.getCantRecNoComunes()) {            
            Recurso rec = (Recurso) est.getRecNoComunesYTransPosibles().get(indice);
            if(rec.getRecBase().getTipo().equalsIgnoreCase(tipo) ){
                boolean este = false;
                if (rec.getEstado().equalsIgnoreCase(estado)) {
                    este = true;
                }
                if (todos | este) {
                    mult = cBloques[indice];                
                }
                CantProductoSimple cant1rec = rec.cantProdSimp(pr, datGen, ta).clona();
                cant1rec.escalarCant(mult);
                suma.sumaCant(cant1rec); 
            }
            indice++;
        }        
        return suma;
    }    
            
    
    
    

    /**
     * Calcula la mÃ¡xima cantidad de producto simple pr que pueden tener
     * los sucesores sin crecimiento al cabo de tAvance pasos de tiempo del
     * parque this, si se estÃ¡ inicialmente en el tiempoAbsoluto ta en el conjunto
     * de informaciÃ³n CI.
     *
     * @param pr producto simple cuya cantidad se calcula
     * @param CI conjunto de decisiÃ³n del grafoN inicial
     * @param tavance avance en pasos de tiempo
     * @return
     */
    public CantProductoSimple prodMaxSucSinCre(Producto pr, ConjNodosN CI,
            int tAvance, ArrayList<Object> informacion) throws XcargaDatos {
        /**
         * ATENCIÃ“N: SON tAvance PASOS DE TIEMPO LO QUE PUEDE NO COINCIDIR CON EL PASO
         * DE TIEMPO DE LA ETAPA SIGUIENTE.
         * Los sucesores sin crecimiento son los que resultan de no agregar inversiones.
         * Puede haber muchos sucesores debido a los cambios aleatorios.
         *
         * SE SUPONE QUE LAS VARIABLES DE LA NATURALEZA QUE DETERMINAN
         * LOS CAMBIOS ALEATORIOS SON SIEMPRE OBSERVABLES
         * Por lo tanto alcanza con considerar un sÃ³lo nodo representativo del CI para
         * ver los posibles sucesores futuros al cabo de tAvance pasos de tiempo.
         * Se toma el primero de la lista de nodos del CI
         *
         */
        NodoN ne = CI.getNodos().get(0);
        Estudio est = (Estudio) informacion.get(0);
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int pasoe = datGen.pasoRepDeEtapa(etapa);
        int pasoAvance = pasoe + tAvance;
        if (pasoAvance > datGen.getCantPasos()) {
            pasoAvance = datGen.getCantPasos();
        }
        /**
         * ATENCIÃ“N: Considera el grafo original por pasos y no el grafo por etapas
         */
        GrafoN grafoNpasos = est.getGrafoNPasos();
        MatrizTransicion matEaEmas1 = new MatrizTransicion(grafoNpasos, pasoe);
        matEaEmas1.calculaMatrizEtapas(pasoe, pasoAvance);
        int[] nulo = new int[est.getCantRecNoComunesYTransPosibles()];
        for(int i=0; i<nulo.length;i++){
            nulo[i]=0;
        }
        Parque decNula = new Parque(etapa, nulo); // decisiÃ³n con ningÃºn recurso o transformaciÃ³n
        CantProductoSimple cantMax = new CantProductoSimple(pr, 0.0);
        ArrayList<NodoN> sucNAvance = new ArrayList<NodoN>();

        for (int isuc = 0; isuc < matEaEmas1.getCantColumnas(); isuc++) {
            if (matEaEmas1.getMatSuc()[0][isuc] == true) {
                sucNAvance.add(grafoNpasos.getNodos().get(pasoAvance).get(isuc));
            }
        }
        for (NodoN nemas1 : sucNAvance) {
            Parque pSuc = (Parque) this.sucesorDeUnPort(decNula, ne, nemas1, informacion);
            CantProductoSimple cantSuc = pSuc.cantProdGenParqueCE(pr, EstadosRec.OPE, informacion);
            if (cantSuc.comparaCant(cantMax) == 1) {
                cantMax = cantSuc;
            }
        }
        return cantMax;
    }

    /**
     * *******************************************************************
     * A CONTINUACIÃ“N APARECEN LOS MÃ‰TODOS QUE SOBREEESCRIBEN LOS DE LA
     * CLASE PORTAFOLIO
     * *******************************************************************
     */
    
    
//    ESTE QUE APARECE ACA ES EL VIEJO SUCESORDEUNPORT ANTES DE PASAR TODO A CODIGO DE ENTEROS.
      /**        
//     * Genera el parque sucesor de uno dado pare que vive en la etapa e, en el
//     * paso representativo de la etapa pre, cuando en el paso pre se toma la
//     * decisiÃ³n de inversiÃ³n dece.
//     * SE CONSIDERAN SOLO LOS BLOQUES NO COMUNES !!!!!!!!!
//     * El parque sucesor vive en la etapa e+1, en el paso representativo
//     * de la etapa e+1, premas1.
//     * El sucesor es funciÃ³n de:
//     * - los nodos de la naturaleza ne (de la etapa e) y nemas1 (de e+1)
//     * - la decisiÃ³n de tomada en e
//     * - informacion es un paquete de cualquier informaciÃ³n relevante.
//     *
//     * El orden en que se procesan los eventos es:
//     *
//     * - Se aplican los cambios aleatorios a los recursos existentes en e, si la
//     * variable aleatoria pasa de NO en pre a SI en premas1 y existen los
//     * recursos origen en el parque, esos recursos se agregan al parque en e+1.
//     *
//     * - Se agregan los recursos y transformaciones de las expansiones forzadas
//     * del estudio y del caso que aparecen en los pasos entre el siguiente a pre
//     * y premas1.
//     *
//     * - Se agregan al parque las transformaciones de la decision dece
//     *
//     * - Las transformaciones demoradas en pre reducen su demora o completan su
//     * construcciÃ³n al pasar a premas1.
//     *
//     * - Las transformaciones no demoradas avanzan su construcciÃ³n o la terminan.
//     *
//     * - De las anteriores, Las tranformaciones cuya demora terminÃ³ o que completan su construcciÃ³n
//     * se aplican a los recursos que hayan quedado de los pasos anteriores.
//     *
//     * - Se agrega al parque en premas1 la decisiÃ³n de inversion de.
//     *
//     * - Se hace envejecer los recursos operativos con edad o dependencia del tiempo,
//     * avanzando del paso pre al paso premas1.
//     *
//     * - Los recursos demorados en pre reducen su demora o
//     * completan su construcciÃ³n al pasar a premas1.
//     *
//     * - Los recursos en construcciÃ³n en pre cuyo plazo de construcciÃ³n se completa
//     * antes de o en premas1, estÃ¡n sujetos a la demora aleatoria segÃºn el valor de
//     * la variable aleatoria en el paso de tiempo del fin de construcciÃ³n normal, que
//     * puede no coincidir con premas1.
//     * ATENCION: Esto se ve en el grafo completo por pasos de tiempo.
//     *
//     * - Finalmente todos los bloques del parque se datan en el paso de la etapa e+1.
//     *
//     * @param de es la decisiÃ³n de inversiÃ³n (clase Portafolio) tomada en la etapa e
//     * @param ne es el NodoN asociado al portafolio en la etapa e
//     * @param nemas1 es el NodoN asociado al que se transita en la etapa e+1
//     *
//     */
//    @Override
//    public Portafolio sucesorDeUnPort(Portafolio de, NodoN ne, NodoN nemas1, ArrayList<Object> informacion)
//            throws XcargaDatos {
//        Estudio est = (Estudio) informacion.get(0);
//        DatosGeneralesEstudio datGen = est.getDatosGenerales();
//        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
//        int emas1 = etapa + 1;
//        int prmas1 = datGen.pasoRepDeEtapa(emas1);
//        // prmas1 es el paso de la siguiente etapa emas1.
//        // tamas1 es el TiempoAbsoluto de la etapa emas1.
//        TiempoAbsoluto tamas1 = datGen.tiempoAbsolutoDePaso(prmas1);
//        int avanceTiempo = datGen.getCalculadorDePlazos().hallaAvanceTiempo(ta, tamas1);
//        /**
//         * Suma la decisiÃ³n de al parque this.
//         */
//        Parque dec = (Parque) de;
//        Parque par1 = Parque.creaCopia(this); // Se parte de una copia el parque inicial, que no se altera
//        Parque par2;
//        try {
//            par2 = Parque.sumaParques(par1, dec);
//        } catch (XBloqueMultNeg ex) {
//            throw new XcargaDatos("Al agregar bloques un multiplicador dio negativo");
//        }
//
//        /**
//         * Pone los bloques del Bloque[] en un ArrayList<Bloque> para procesarlos
//         */
//        ArrayList<Bloque> pIniAL;
//        ArrayList<Bloque> pFinAL;
//        ArrayList<Bloque> par2AL = par2.bloquesDelParque(est);
//        try {
//            /**
//             * Agrega las expansiones forzadas NO COMUNES del estudio y el caso.
//             * Este mÃ©todo debe ser el primero a aplicar de los de ParqueCreaSuc, y avanza la etapa del parque a emas1
//             */
//            boolean comunes = false;
//            boolean nocomunes = true;
//            pIniAL = ParqueCreaSuc.agregaExpsForzadas(par2AL, ta, tamas1, comunes, nocomunes, informacion);
//
//            /**
//             * Aplica cambios aleatorios.
//             */
//            pFinAL = new ArrayList<Bloque>();
//            ConjCambiosAleat conjCA = est.getConjCambAleat();
//            ParqueCreaSuc.aplicaCambiosAleatorios(pIniAL, pFinAL, tamas1, ne, nemas1,
//                    conjCA, informacion);
//            /**
//             * Aplica transformaciones.
//             */
//            ParqueCreaSuc.avanzaTiempoTransformaciones(pIniAL, pFinAL, ne, nemas1, ta, avanceTiempo, informacion);
//            /**
//             * Avanza tiempo de Recursos
//             */
//            ParqueCreaSuc.avanzaTiempoRecursos(pIniAL, pFinAL, ne, nemas1, tamas1, avanceTiempo, informacion);
//
//            /**
//             * Carga el TiempoAbsoluto de tamas1 de la etama emas1 en todos los bloques
//             * resultantes del Parque pFin
//             */
//            for (Bloque b : pFinAL) {
//                b.setTa(tamas1);
//            }
//        } catch (XBloqueMultNeg ex) {
//            throw new XcargaDatos("En sumaParque algÃºn multiplicador resultÃ³ negativo");
//        }
//        // Se agregan los parÃ¡metros de impresiÃ³n a informacion.
//        informacion.add((Boolean) false);
//        informacion.add((Boolean) false);
//        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
//        informacionImp.add(true);
//        informacionImp.add(false);
////        if (!(pIniAL.isEmpty())) {
////            throw new XcargaDatos("Error al procesar"
////                    + "la sucesiÃ³n de un parque, pIni no quedÃ³ vacÃ­o " + "El parque inicial es "
////                    + this.toStringCorto(informacion, informacionImp));
////        }
//        Collections.sort(pFinAL);
//        Parque pFin;
//        try {
//            pFin = new Parque(est, emas1, tamas1, pFinAL);
//        } catch (XBloqueMultNeg ex) {
//            throw new XcargaDatos("Un parque sucesor tiene un multiplicador negativo");
//        }
////        pFin.cargaResumenP(informacion, false);
////        pFin.cargaParqueOperativo(informacion);
//        return pFin;
//    }
//    
    
    
    
    /**
     * Â¡ATENCION! 
     * ESTA ES LA VERSIÃ“N QUE EMPLEA LOS CODIGOS DE ENTEROS
     * 
     * Genera el parque sucesor de uno dado pare que vive en la etapa e, en el
     * paso representativo de la etapa pre, cuando en el paso pre se toma la
     * decisiÃ³n de inversiÃ³n dece.
     * SE CONSIDERAN SOLO LOS BLOQUES NO COMUNES SE REPRESENTAN EN CBLOQUES MEDIANTE CODIGOS ENTEROS
     * El parque sucesor vive en la etapa e+1, en el paso representativo
     * de la etapa e+1, premas1.
     * El sucesor es funciÃ³n de:
     * - los nodos de la naturaleza ne (de la etapa e) y nemas1 (de e+1)
     * - la decisiÃ³n de tomada en e
     * - informacion es un paquete de cualquier informaciÃ³n relevante.
     *
     * El orden en que se procesan los eventos es:
     *
     * - Se aplican los cambios aleatorios a los recursos existentes en e, si la
     * variable aleatoria pasa de NO en pre a SI en premas1 y existen los
     * recursos origen en el parque, esos recursos se agregan al parque en e+1.
     *
     * - Se agregan los recursos y transformaciones de las expansiones forzadas
     * del estudio y del caso que aparecen en los pasos entre el siguiente a pre
     * y premas1.
     *
     * - Se agregan al parque las transformaciones de la decision dece
     *
     * - Las transformaciones demoradas en pre reducen su demora o completan su
     * construcciÃ³n al pasar a premas1.
     *
     * - Las transformaciones no demoradas avanzan su construcciÃ³n o la terminan.
     *
     * - De las anteriores, Las tranformaciones cuya demora terminÃ³ o que completan su construcciÃ³n
     * se aplican a los recursos que hayan quedado de los pasos anteriores.
     *
     * - Se agrega al parque en premas1 la decisiÃ³n de inversion de.
     *
     * - Se hace envejecer los recursos operativos con edad o dependencia del tiempo,
     * avanzando del paso pre al paso premas1.
     *
     * - Los recursos demorados en pre reducen su demora o
     * completan su construcciÃ³n al pasar a premas1.
     *
     * - Los recursos en construcciÃ³n en pre cuyo plazo de construcciÃ³n se completa
     * antes de o en premas1, estÃ¡n sujetos a la demora aleatoria segÃºn el valor de
     * la variable aleatoria en el paso de tiempo del fin de construcciÃ³n normal, que
     * puede no coincidir con premas1.
     * ATENCION: Esto se ve en el grafo completo por pasos de tiempo.
     *
     *
     * @param de es la decisiÃ³n de inversiÃ³n (clase Portafolio) tomada en la etapa e
     * @param ne es el NodoN asociado al portafolio en la etapa e
     * @param nemas1 es el NodoN asociado al que se transita en la etapa e+1
     *
     */
    @Override
    public Portafolio sucesorDeUnPort(Portafolio de, NodoN ne, NodoN nemas1, ArrayList<Object> informacion)
            throws XcargaDatos {
        Estudio est = (Estudio) informacion.get(0);
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
        int emas1 = etapa + 1;
        int prmas1 = datGen.pasoRepDeEtapa(emas1);
        // prmas1 es el paso de la siguiente etapa emas1.
        // tamas1 es el TiempoAbsoluto de la etapa emas1.
        TiempoAbsoluto tamas1 = datGen.tiempoAbsolutoDePaso(prmas1);
        int avanceTiempo = datGen.getCalculadorDePlazos().hallaAvanceTiempo(ta, tamas1);
        /**
         * Suma la decisiÃ³n de al parque this.
         */
        Parque dec = (Parque) de;
        Parque par2;
        try {
            par2 = Parque.sumaParques(this, dec);
        } catch (XBloqueMultNeg ex) {
            throw new XcargaDatos("Al agregar bloques un multiplicador dio negativo");
        }

        try {
            /**
             * Agrega las expansiones forzadas NO COMUNES del estudio y el caso.
             * Este mÃ©todo debe ser el primero a aplicar de los de ParqueCreaSuc, y avanza la etapa del parque a emas1
             */
            ParqueCreaSuc.agregaExpsForzadasCE(par2, ta, tamas1, emas1, informacion);

            /**
             * Aplica cambios aleatorios.
             */
            ConjCambiosAleat conjCA = est.getConjCambAleat();
            ParqueCreaSuc.aplicaCambiosAleatoriosCE(par2, tamas1, ne, nemas1,
                    conjCA, informacion);
            
            
            /**
             * Aplica transformaciones.
             */
            ParqueCreaSuc.avanzaTiempoTransformacionesCE(par2, ne, nemas1, ta, avanceTiempo, informacion);
            
 
            
            /**
             * Avanza tiempo de Recursos
             */
            ParqueCreaSuc.avanzaTiempoRecursosCE(par2, ne, nemas1, tamas1, avanceTiempo, informacion);


        } catch (XBloqueMultNeg ex) {
            throw new XcargaDatos("En sumaParque algÃºn multiplicador resultÃ³ negativo");
        }
        // Se agregan los parÃ¡metros de impresiÃ³n a informacion.
        informacion.add((Boolean) false);
        informacion.add((Boolean) false);
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        informacionImp.add(true);
        informacionImp.add(false);
//        if (!(pIniAL.isEmpty())) {
//            throw new XcargaDatos("Error al procesar"
//                    + "la sucesiÃ³n de un parque, pIni no quedÃ³ vacÃ­o " + "El parque inicial es "
//                    + this.toStringCorto(informacion, informacionImp));
//        }

        return par2;
    }    
    

    /**
     * METODO decisionesDeUnPort
     *
     * Devuelve la lista de decisiones que pueden tomarse en
     * el paso de tiempo representativo de una etapa e, si se estÃ¡ en un conjunto de
     * decisiÃ³n dado, que cumplen las restricciones
     * de inicio de construccion y que no pueden descartarse de antemano
     * por conducir a violar restricciones de parque en e+1.
     * Este descarte es especÃ­fico a cada clase que hereda de Portafolio y
     * no de Portafolio.
     *
     * @param cDe es el conjunto de decisiÃ³n.
     * @param conjRInicio ES NULL Y SE MANTIENE POR COMPATIBILIDAD
     * @param conjRParque ES NULL Y SE MANTIENE POR COMPATIBILIDAD
     * El caso, trasmitido en informacion, tiene toda la informaciÃ³n de restricciones
     * @param informacion.
     *
     * @result listadec es la lista de decisiones.
     *
     */
    @Override
    public ArrayList<Portafolio> decisionesDeUnPort(ConjDeDecision cDe, ConjRestGrafoE conjRInicio,
            ConjRestGrafoE conjRParque, ArrayList<Object> informacion) throws XcargaDatos {
        /**
         * Se parte de las cotas del enumerador determinadas cuando se leyÃ³ el caso
         * para todos los pasos de tiempo y se perfecciona esa cota para la etapa
         * en que vive el parque, considerando las restricciones de producto y
         * de cota que limitan el parque en la etapa siguiente.
         *
         * Si un recurso o transformaciÃ³n estÃ¡n incluÃ­dos en la decisiÃ³n de inversiÃ³n
         * de la etapa e, de paso de tiempo pre, aparecerÃ­an en el paso de tiempo
         * pre + 1, de las siguientes maneras:
         *
         * - Si tienen plazo de construcciÃ³n igual a uno, o plazo mayor pero existe
         * una cota de plazos de construcciÃ³n en maxPerCons en DatosGeneralesEstudio,
         * tanto los recursos como transformaciones aparecen en su primer aÃ±o de construcciÃ³n,
         *
         * - Los recursos aparecen operativos si tienen plazo de construcciÃ³n uno
         * o la cota maxPerCons es uno para la etapa e.
         *
         * - Las transformaciones se completan si tienen plazo de construcciÃ³n uno
         * o la cota maxPerCons es uno para la etapa e.
         *
         * - No se incluyen RecursosBase que dado el plazo de construcciÃ³n tendrÃ­an que
         * entrar en servicio antes de su tiempoAbsolutoBase.
         */
        Estudio estudio = (Estudio) (informacion.get(0));
        DatosGeneralesEstudio datGen = estudio.getDatosGenerales();
        TiempoAbsoluto taUltEtapa = estudio.tiempoAbsolutoDeEtapa(datGen.getCantEtapas());        
        Caso caso = (Caso) (informacion.get(1));
        ConjDemandas conjDem = estudio.getConjDemandas();
        ConjRecB conjRBEleg = caso.getSubConjRBEleg();
        ConjTransB conjTBEleg = caso.getSubConjTBEleg();
        TiempoAbsoluto tae = estudio.tiempoAbsolutoDeEtapa(etapa);
        ConjNodosN CI = cDe.getConjInfo();
        /**
         * Carga las cotas de enumerador lexicogrÃ¡fico
         */
        ArrayList<RecursoBase> listaRBEleg = conjRBEleg.getRecursosB();
        ArrayList<TransforBase> listaTBEleg = conjTBEleg.getTransforsB();
        int dimRBEleg = listaRBEleg.size();
        int dimTBEleg = listaTBEleg.size();
        int[] cotasInf, cotasSup;
        cotasInf = new int[dimRBEleg + dimTBEleg];
        cotasSup = new int[dimRBEleg + dimTBEleg];

        int i = 0;
        for (RecursoBase rb : listaRBEleg) {
            cotasInf[i] = caso.cotaInfInicioRB(rb, etapa);
            cotasSup[i] = caso.cotaSupInicioRB(rb, etapa);
            i++;
        }

        for (TransforBase tb : listaTBEleg) {
            cotasInf[i] = caso.cotaInfInicioTB(tb, etapa);
            cotasSup[i] = caso.cotaSupInicioTB(tb, etapa);
            i++;
        }
        /**
         * El recursoBase i-esimo (empezando en cero)
         * está en el get(i)  de cotasInf y Sup
         * La tranformacionBase j-ésima (empezando en cero) está en el
         * get(dimRBEleg + j) de cotasInf y Sup
         *
         */
        /**
         * Se corrigen las cotas solo de los recursosBase "normales" es decir que no admiten
         * cambio aleatorio, ni tienen demora aleatoria de construcciÃ³n, ni dependen del tiempo.
         * Los recursosBase "no normales" y las transformaciones tienen cotas
         * especificadas por el usuario que no se corrigen.
         */
        ArrayList<ArrayList<RestDeProducto3>> listaRestProdParque = caso.getRestsDeProdParqueCorr();
        int indiceRB = 0;
        boolean huboAcotacionSupProd;
        boolean huboAcotacionSupCota;
        CalculadorDePlazos calcP = datGen.getCalculadorDePlazos();        
        for (RecursoBase rb : conjRBEleg.getRecursosB()) {
            huboAcotacionSupProd = false;
            huboAcotacionSupCota = false;
            int tcons = rb.getPerConstr(tae, estudio);
            // taCons es el TiempoAbsoluto de entrada en servicio sin demora
            TiempoAbsoluto taCons = calcP.hallaTiempoAbsoluto(tae, tcons);
            if(calcP.hallaAvanceTiempo(taCons,taUltEtapa)<0){
                cotasSup[indiceRB] = 0;
                cotasInf[indiceRB] = 0;    
            }else{         
                if (!rb.getAdmiteCambioAleatorio() && !rb.getTieneDemoraAl() && rb.getDependenciaDeT().equalsIgnoreCase(DependenciaDeT.NO)) {
                    // el recurso es "normal"
                    int numFaltanteMin = 9999;
                    int indRest = 0;
                    /**
                     * Se encuentran cotas superiores de enumerador a partir
                     * de restricciones de producto.
                     */
                    for (Producto pr : datGen.getProductos()) {
                        int numFaltanteRb = 0;
                        ArrayList<RestDeProducto3> restsDePr = listaRestProdParque.get(indRest);
                        indRest++;
                        /**
                         * Se consideran las restricciones del Producto pr que acotan superiormente
                         * la cantidad de producto que oferta un parque. Se toma la que restringe mÃ¡s.
                         */
                        CantProductoSimple prodMin = conjDem.prodMinDem(pr, tae, CI, tcons);
                        double proporcionMin = 999999.9;
                        for (RestDeProducto3 rest : restsDePr) {
                            if (rest.getInfSup() == InfSup.SUP) {
                                huboAcotacionSupProd = true;
                                double proporcionRest = rest.proporcionDeTa(taCons);
                                if (proporcionRest < proporcionMin) {
                                    proporcionMin = proporcionRest;
                                }
                            }
                        }
                        prodMin.escalarCant(proporcionMin);
                        CantProductoSimple prodSCCMax = (this).prodMaxSucSinCre(pr, CI, tcons, informacion);
                        prodSCCMax.escalarCant(-1.0);
                        CantProductoSimple faltante = CantProductoSimple.suma2Cants(prodMin, prodSCCMax);

                        double cociente = faltante.getCant() / rb.cantBaseUnProd1Mod(pr).getCant();
                        if (Double.isInfinite(cociente)) {
                            numFaltanteRb = (int) 9999.0;
                        } else {
                            numFaltanteRb = Math.max(0, (int) (faltante.getCant() / rb.cantBaseUnProd1Mod(pr).getCant()) + 1);
                        }

                        if (numFaltanteRb < numFaltanteMin) {
                            numFaltanteMin = numFaltanteRb;
                        }
                    }
                    if (numFaltanteMin < cotasSup[indiceRB]) {
                        cotasSup[indiceRB] = numFaltanteMin;
                    }

                    /**
                     * Se corrige el enumerador con las cotas superiores e inferiores a partir
                     * de restricciones de cota de RecursosBase en el Parque en emas1.
                     * Se toma la cota tcons pasos de tiempo mÃ¡s adelante, siendo tcons
                     * el tiempo de construcciÃ³n.
                     *
                     * Se consideran los recursos operativos del mismo RecursoBase y se evita
                     * que con los que inician su construcciÃ³n se exceda la cota. Como no se
                     * consideran los recursos que ya estÃ¡n en construcciÃ³n el enumerador generarÃ¡ posiblemente
                     * parque no factibles, que serÃ¡n eliminados despuÃ©s.
                     */
                    int pasoe = datGen.pasoRepDeEtapa(etapa); // pasoe es el paso de la etapa.
                    int pasoemas1 = Math.min(pasoe + tcons, datGen.getCantPasos());
                    int indRb = caso.getSubConjRBEleg().getRecursosB().indexOf(rb);

                    ArrayList<RestCota3> restsDeCota = caso.getRestsDeCotaRbElegParqueCorr().get(indRb);
                    int cotaMin = 9999;
                    for (RestCota3 rest : restsDeCota) {
                        if (rest.getInfSup().equalsIgnoreCase("SUP") && rest.cotaDePaso(pasoemas1) < cotaMin) {
                            huboAcotacionSupCota = true;
                            cotaMin = rest.cotaDePaso(pasoemas1);
                        }
                    }
                    int faltanteRbPorCota = Math.max(0, cotaMin - cantDeUnRecBaseCE(rb, EstadosRec.OPE, informacion));
                    if (faltanteRbPorCota < cotasSup[indiceRB]) {
                        cotasSup[indiceRB] = faltanteRbPorCota;
                    }

                    /**
                     * Verifica si hubo alguna mejora de la acotación superior.
                     * Si no la hubo es un error.
                     */
                    if (!huboAcotacionSupCota & !huboAcotacionSupProd) {
                        // no fue posible mejorar la acotaciÃ³n del enumerador
                        throw new XcargaDatos("No fue posible mejorar la acotación superior del"
                                + "enumerador lexicográfico del recursoBase" + rb.getNombre());
                    }
                }
                /**
                 * Se anulan las cotas superior e inferior de los RecursoBase
                 * que por su plazo de construcción normal estarían operativos antes del
                 * tiempoAbsolutoBase del RecursoBase y que tienen cota superior de inicio
                 * de construcción no nula.
                 */
                TiempoAbsoluto tAbsRecBase = rb.getTiempoAbsolutoBase();
                if (datGen.getCalculadorDePlazos().hallaAvanceTiempo(taCons, tAbsRecBase) > 0
                        && cotasSup[indiceRB] > 0) {
                    int selec;
                    selec = JOptionPane.showOptionDialog(null, "Entrada antes de tiempoAbsolutoBase "
                            + rb.getNombre(), "Â¿Quiere seguir?", JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE, null,
                            new Object[]{"Detener la ejecuciÃ³n", "Seguir"}, null);
                    // Devuelve 0, 1 o 2 segÃºn la opciÃ³n
                    if (selec == 0) {
                        throw new XcargaDatos("Se detuvo ejecuciÃ³n porque un RecursoBase entrarÃ­a en servicio "
                                + "antes de su tiempoAbsolutoBase");
                    }
                    cotasSup[indiceRB] = 0;
                    cotasInf[indiceRB] = 0;
                }
            }
            indiceRB++;
        }
        
        /**
         * Se corrige la cota superior de las transformaciones verificando si el Parque
         * tiene los Recursos origen que requiere la transformaciÃ³n.
         * Si el parque no los tiene la cota superior se carga en cero.
         * 
         */
        for(TransforBase tb: listaTBEleg){ 
            int tcons = tb.getPerConstr(tae, estudio);
            // taCons es el TiempoAbsoluto de entrada en servicio sin demora
            TiempoAbsoluto taCons = calcP.hallaTiempoAbsoluto(tae, tcons);
            if(calcP.hallaAvanceTiempo(taCons,taUltEtapa)<0){
                cotasSup[indiceRB] = 0;
                cotasInf[indiceRB] = 0;  
            }else{
                int indTB = 0;
                int minCociente = Integer.MAX_VALUE; 
                /**
                 * MÃ­nimo cociente entre cantidad de RecursoBase de cada tipo en el parque 
                 * y los requeridos por la TransforBase 
                 */   
                
                /**
                 * ATENCIÃ“N: ESTO NO IMPIDE QUE SE INICIEN DISTINTAS TRANSFORMACIONES QUE USAN LOS MISMOS
                 * RECURSOSBASE ORIGEN. CUANDO SE COMPLETEN PUEDE OCURRIR UN ERROR
                 * LA SOLUCIÃ“N ES IMPONER RESTRICCIONES LÃ“GICAS DE INICIO DE CONSTRUCCIÃ“N SOBRE LAS TRANSFORMACIONESBASE
                 */
                for (Recurso rec : tb.getRecursosOrigen()) {
                    // cantEnParque es la cantidad de unidades del Recurso rec en parque con el estado 
                    // requerido por la TransformacionBase tb.
                    // multrb es la cantidad que requiere la TransformacionBase tb.
                    int cantEnParque = this.cantDeUnRecBaseCE(rec.getRecBase(), rec.getEstado(), informacion);
                    int multrb = tb.multDeUnRecOrigen(rec);
                    int cociente = cantEnParque/multrb;                   
                    if (cociente < minCociente) {
                        minCociente = cociente;                  
                    }
                } 
                minCociente = Math.max(0,minCociente-cantDeUnaTransBaseCE(tb, EstadosRec.CON, informacion));
                cotasSup[dimRBEleg + indTB] = minCociente;
                indTB++;                                
            }
        }
                       
        /**
         * Se construye el listado de decisiones de inversiÃ³n
         */
        ArrayList<Object> listaObj = new ArrayList<Object>();
        listaObj.addAll(conjRBEleg.getRecursosB());
        listaObj.addAll(conjTBEleg.getTransforsB());

        ArrayList<Portafolio> listaDec = new ArrayList<Portafolio>();
        EnumeradorLexicografico enumL = new EnumeradorLexicografico(dimRBEleg + dimTBEleg, cotasInf, cotasSup);
        int[] vecMult;
        do {
            vecMult = enumL.devuelveVector();
            /**
             * vecMult contiene cantidades de cada uno de los RecursosBase y TransforBase elegibles
             * del caso, en el orden en que aparecen en conjRBEleg y conjTBEleg (que son propios del caso).
             * Debe crearse el cÃ³digo del Parque cBloques asociado a vecMult.
             * cBloques tiene cantidades de Recursos y Transformaciones elegibles posibles
             * entre las del Estudio, en el orden en que aparecen en recElegPosibles y transElegPosibles
             * (que son propias del Estudio.
             */
            int posicion;
            if (vecMult != null) {
                int[] cBloquesDec = new int[estudio.getCantRecNoComunesYTransPosibles()];
                int indVecMult = 0;
                for (RecursoBase rb : conjRBEleg.getRecursosB()) {
                    posicion = rb.posicionCodigoRecInicioConst(estudio, tae);
                    cBloquesDec[posicion] = vecMult[indVecMult];
                    indVecMult++;
                }
                for (TransforBase tb : conjTBEleg.getTransforsB()) {
                    posicion = tb.posicionCodigoTransInicioConst(estudio, tae);
                    cBloquesDec[posicion] = vecMult[indVecMult];                    
                    indVecMult ++;
                }

                Portafolio dec = new Parque(etapa, cBloquesDec);
//                dec.cargaResumenP(informacion, false);
                listaDec.add(dec);
            }
        } while (vecMult != null);
        /**
         * Se eliminan las decisiones de inversiÃ³n que no cumplen las restricciones
         * de inicio de construcciÃ³n (es decir aquÃ©llas que pueden evaluarse directamente
         * conociendo el parque en e y la decisiÃ³n.
         */
        ArrayList<Portafolio> aEliminar = new ArrayList<Portafolio>();
        for (Portafolio dec : listaDec) {
            Parque sumaPIniMasDec;
            try {
                sumaPIniMasDec = sumaParques(this, (Parque)dec);
            } catch (XBloqueMultNeg ex) {
                throw new XcargaDatos("Al agregar bloques un multiplicador dio negativo");
            }
            NodoN nvacio = new NodoN();
            Portafolio vacio = new Portafolio();
            /**
             *  Se crea sÃ³lo para evitar la ambigÃ¼edad en el llamado a evaluar
             */
            for (RestGrafoE resI : caso.getConjRestSigInicioResult().getRestricciones()) {
                if (!resI.evaluar(informacion, sumaPIniMasDec, vacio, nvacio)) {
                    aEliminar.add(dec);
                    break;
                }
            }
        }
        for (Portafolio dec : aEliminar) {
            listaDec.remove(dec);
        }
        return listaDec;
    }
    
    




    /**
     * Carga el costo del paso si el Portafolio (Parque) es this, el objetivo obj
     * y el estado de la naturaleza nt.
     * Los costos estÃ¡n cargados en la mitad del perÃ­odo
     * CARGA TAMBIEN OTROS RESULTADOS PARA LOS QUE REQUIERE EL NODOE
     * Si el parque es vacÃ­o (bloques == null) lanza excepciÃ³n.
     * @param obj
     * @param nE el NodoE al que pertenece el Parque
     * @param informacion
     * @return
     */
    @Override
    public double calculaCostoPaso(Objetivo obj, Object obBolsaSim, NodoE nE, ArrayList<Object> informacion) throws XcargaDatos {
        double costoPaso = 0.0;
//        IdentSimulPaso idSim = new IdentSimulPaso(parqueOperativo, nt.getValoresTiposDatos());

        Estudio est = (Estudio) informacion.get(0);
        NodoN nN = nE.getNodoN();
        ResSimul resS3 = resSimulCostoPaso(obj, obBolsaSim, nE, informacion);
        nE.setNombreSimulacion(resS3.getNombreSimul());
        // Carga el costo total del paso
        costoPaso = obj.evaluar(resS3, informacion);
        return costoPaso;
    }

    /**
     * Devuelve un ResSimul con los resultados del caso INCLUSO OTROS COSTOS ADICIONALES A LA SIMULACION
     * que para un Parque son los costos fijos y de inversiÃ³n, si el portafolio es this, el objetivo obj
     * y el NodoE es nE.
     * Los costos estÃ¡n cargados en la mitad del perÃ­odo.
     * @param obj
     * @param baseDeDatosResult 
     * @param nE
     * @param informacion
     * @return
     */
    @Override
    public ResSimul resSimulCostoPaso(Objetivo obj, Object obBolsaSim, NodoE nE, ArrayList<Object> informacion)
            throws XcargaDatos {
    	ResSimul resFinal=null;
        ObjNumCron objNC = (ObjNumCron) obj;
        ArrayList<Double> pond = objNC.getPonderadores();
        NodoN nN = nE.getNodoN();
//        nE.getOtrosResultadosPaso().clear();
        Estudio est = (Estudio) informacion.get(0);
        BolsaDeSimulaciones3 bolsaSim = (BolsaDeSimulaciones3) obBolsaSim;
//        cargaNombreSimul(obBolsaSim, nN, informacion);
        // Resultados de operaciÃ³n
        Parque parqueOper = calculaParqueOperativo(informacion);
        parqueOper.cambiaPorRBEquiv(informacion);
        ResSimul resS = bolsaSim.devuelveRes(parqueOper, nN, informacion);
//        nE.getOtrosResultadosPaso().add(resS.valorEsperado(pond));
        if(est.getDatosGenerales().getTipoCargosFijos().equalsIgnoreCase(DatosGeneralesEstudio.TOTALPASODECISION)){
        	// YA NO SE SUMAN ANUALIDADES Y COSTOS FIJOS Y NO SE AGREGAN A otrosResultadosPaso del nodo nE
        	resFinal = resS;
        } else if (est.getDatosGenerales().getTipoCargosFijos().equalsIgnoreCase(DatosGeneralesEstudio.ANUALIDADES)){
        	// Anualidad
            ResSimul resS2 = resS.sumaVecConstante(anualidParqueCE(est));
//            nE.getOtrosResultadosPaso().add(anualidadPond(est, obj));
            // Costo fijo anual
            ResSimul resS3 = resS2.sumaVecConstante(costoFijoParque(est, nN));
//            nE.getOtrosResultadosPaso().add(costoFijoPond(est, obj, nN));
            resFinal=resS3;
        }
        return resFinal;
    }
    
    
    
    /**
     * Carga en el nodoE nE (en otrosResultados) el costo operativo del paso y si se está en tipoCargosFijos = ANUALIDADES
     * carga también la anualidad y el costo fijo.
     * @param obj
     * @param obBolsaSim
     * @param nE
     * @param informacion
     * @throws XcargaDatos
     */
    public void cargaOtrosResultados(Objetivo obj, Object obBolsaSim, NodoE nE, ArrayList<Object> informacion)
            throws XcargaDatos {
        ObjNumCron objNC = (ObjNumCron) obj;
        ArrayList<Double> pond = objNC.getPonderadores();
        NodoN nN = nE.getNodoN();
        nE.getOtrosResultadosPaso().clear();
        Estudio est = (Estudio) informacion.get(0);
        BolsaDeSimulaciones3 bolsaSim = (BolsaDeSimulaciones3) obBolsaSim;
//        cargaNombreSimul(obBolsaSim, nN, informacion);
        // Resultados de operaciÃ³n
        Parque parqueOper = calculaParqueOperativo(informacion);
        parqueOper.cambiaPorRBEquiv(informacion);
        ResSimul resS = bolsaSim.devuelveRes(parqueOper, nN, informacion);
        nE.getOtrosResultadosPaso().add(resS.valorEsperado(pond));
        if (est.getDatosGenerales().getTipoCargosFijos().equalsIgnoreCase(DatosGeneralesEstudio.ANUALIDADES)){
        	// Anualidad            
            nE.getOtrosResultadosPaso().add(anualidadPond(est, obj));
            // Costo fijo anual
            nE.getOtrosResultadosPaso().add(costoFijoPond(est, obj, nN));
        }
        
    }
    
    
    

    @Override
    /**
     * Levanta los resultados detallados de la corrida de nombre dirCorrida y carga
     * 
     */
    public void cargaBaseDeDatos(Object impGen, Object bolsaSimul, String dirResultCaso, String dirCorrida, ArrayList<Object> informacion) throws
            XcargaDatos {
        Estudio est = (Estudio)informacion.get(0);
        
        TiempoAbsoluto ta = est.getDatosGenerales().tiempoAbsolutoDeEtapa(etapa);
        
        try{
            ResSimulDB resDB = ((ImplementadorGeneral)impGen).creaResDB(dirCorrida, ta, est); 
            ((BolsaDeSimulaciones3)bolsaSimul).cargaDatosDB(dirResultCaso, dirCorrida, resDB);
        }
        catch(Exception e){
            throw new XcargaDatos(e.getMessage());
        }
       
        return;
    }        


    /**
     * Carga el costo de fin de juego (valor de Bellman al fin de la Ãºltima etapa)
     * si el portafolio final es this, el objetivo obj
     * y el estado de la naturaleza nt.
     * SÃ³lo puede aplicarse a parques de la etapa final, que por definiciÃ³n
     * corresponde solo al paso final ya que la etapa final tiene un Ãºnico paso, el paso final
     * @param obj
     * @param nt
     * @param informacion
     * @return
     */
    @Override
    public double calculaCostoFinJuego(Objetivo obj, Object obBolsaSim, NodoE nE, ArrayList<Object> informacion) throws XcargaDatos {
        NodoN nt = nE.getNodoN();
        Estudio est = (Estudio) informacion.get(0);
        int k = est.getDatosGenerales().getCantPasosAgregados();
        double costoFin = calculaCostoPaso(obj, obBolsaSim, nE, informacion);
        // se toma el costo de un paso y calcula la suma de la serie
        // que repite dicho costo en los pasos T+1, T+2,.......T+k
        // siendo T el Ãºltimo paso en el que vive este parque, no incluÃ­do en la suma.
        // esa serie se actualiza al instante final del paso T.

        double tasa = est.getDatosGenerales().getTasaDescPaso();
        double factor = 1 / (1 + tasa);
        costoFin = costoFin * Math.sqrt(factor) * (1 - Math.pow(factor, k)) / (1 - factor);
        return costoFin;
    }

    /**
     * Carga en el parque el resumen ordenado, el parque operativo, las
     * CantProductoSimple de Generadores OPE y los requerimientos de CantProductoSimple
     * de Cargas OPE
     * @param informacion
     * @throws persistenciaMingo.XcargaDatos
     */
    @Override
    public void cargaResumenFinal(ArrayList<Object> informacion) throws XcargaDatos {
        Estudio est = (Estudio) informacion.get(0);
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        informacion.add((Boolean) false);
        informacion.add((Boolean) false);
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        boolean soloNoComunes = true;
        boolean prod = false;
        boolean usaEquiv = false;       
        boolean soloNoNulos = true;
        informacionImp.add(soloNoComunes);
        informacionImp.add(prod);
        informacionImp.add(usaEquiv);
        informacionImp.add(soloNoNulos);                
    }

    /**
     * Crea textos adicionales para la impresiÃ³n de los NodoE
     * - Cantidad de productos de las demandas
     * - Para cada producto, proporcion (oferta generadores)/(req. demanda + req. cargas)
     *   se hace para los generadores y cargas OPE.
     */
    @Override
    public String creaTextoAdicNodoE(ArrayList<Object> informacion, NodoN nN) throws XcargaDatos {
        Estudio est = (Estudio) informacion.get(0);
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        ArrayList<Producto> productos = datGen.getProductos();
        ConjDemandas conjD = est.getConjDemandas();
        String texto = "Cant. productos de los generadores del parque";
        for (Producto pr : productos) {
            texto += cantProdGenParqueCE(pr, EstadosRec.OPE, informacion);
        }
        texto += "\r\n";
        texto = "Req. productos de las cargas del parque";
        for (Producto pr : productos) {
            texto += reqProdCargasParqueCE(pr, EstadosRec.OPE, informacion);
        }
        texto += "\r\n";
        ArrayList<CantProductoSimple> cantPD = conjD.cantProdsDemanda(nN, etapa, est);
        texto = "Req. productos de las demandas: " + " ";
        for (CantProductoSimple cPSDem : cantPD) {
            texto += cPSDem.toStringCortoLinea();
        }
        texto += "\r\n";
        texto += "Proporciones   (Ofe Generadores)/(Req Demandas + Req Cargas) : " + " ";
        for (CantProductoSimple cPSDem : cantPD) {
            Producto pr = cPSDem.getProducto();
            CantProductoSimple reqCargas = reqProdCargasParqueCE(pr, TiposEnum.EstadosRec.OPE, informacion);
            CantProductoSimple ofertaGen = cantProdGenParqueCE(pr, TiposEnum.EstadosRec.OPE, informacion);
            CantProductoSimple suma = CantProductoSimple.suma2Cants(cPSDem, reqCargas);
            double prop = CantProductoSimple.proporcionDeDosCant(ofertaGen, suma);
            texto += Numeros.redondeaDouble(prop, 4) + "   ";
        }
        texto += "\r\n";
        return texto;
    }

    @Override
    /**
     * Carea un resumen del Parque que contiene la descripciÃ³n en texto de los 
     * Recursos No Comunes y Transformaciones.
     * Este resumen para el parque operativo, es parte de la clave ClaveStringSimul
     * con la que se almacena en la BolsaDeSimulaciones3 
     */
    public String creaResumenP(ArrayList<Object> informacion, boolean usaEquiv) throws XcargaDatos {
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        boolean soloNoComunes = true;
        boolean prod = false;
        boolean soloNoNulos = true;
        informacionImp.add(soloNoComunes);
        informacionImp.add(prod);
        informacionImp.add(usaEquiv);
        informacionImp.add(soloNoNulos); 
        return toStringCorto(informacion, informacionImp);
    }

    /**
     * Devuelve el ResSimul cuando el portafolio es this y el NodoN es nN
     * @param nN es el NodoN
     */
    @Override
    public ResSimul devuelveResSimul(NodoN nN, Object baseDeDatosResult,
            ArrayList<Object> informacion) throws XcargaDatos {
        BolsaDeSimulaciones3 bolsaSim = (BolsaDeSimulaciones3) baseDeDatosResult;
        ResSimul rS = bolsaSim.devuelveRes(this, nN, informacion);
        return rS;
    }

    /**
     * Devuelve los parques contiguos de uno dado segÃºn la definiciÃ³n
     * de portafolios contiguos de uno dado. Requiere las caracterÃ­sticas
     * particulares de parque, por lo que no puede ser un mÃ©todo de la clase
     * padre Portafolio.
     *
     * @return devuelve un ArrayList<Portafolio> con los parques contiguos
     * de this
     *
     * La definiciÃ³n de portafolios contiguo es:
     * Sean:
     * S es el conjunto de restricciones significativas de parque
     * H es el conjunto de restricciones heurÃ­sticas de parque
     *
     * Hay n recursosBase elegibles operativos en el portafolio (explÃ­citamente se
     * excluyen las transformaciones).
     *
     * Sea T={-1, 0, 1} 
     *     
     *
     * Un parque q es contiguo a otro p si:
     * - q cumple todas las restricciones de S
     * - q = p+d, donde 
     *      si simple = false, d pertenece a T**n (producto cartesiano)
     *      si simple = true, d tiene un solo valor no nulo -1 o 1.
     */
    @Override
    public ArrayList<Portafolio> entregaContiguos(
            ArrayList<Object> informacion, ConjRestGrafoE conjRestSigParque, boolean simple) throws XcargaDatos {
        ArrayList<Portafolio> contiguos = new ArrayList<Portafolio>();
        Estudio est = (Estudio) informacion.get(0);
        Caso caso = (Caso) informacion.get(1);
        ConjRecB conjRBE = caso.getSubConjRBEleg();
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
        int cantDigitos = conjRBE.getRecursosB().size();
        // La cantidad de recursos base elegibles da la cantidad de dÃ­gitos del enumerador

        int[] cotasInferiores;
        cotasInferiores = new int[cantDigitos];
        int[] cotasSuperiores;
        cotasSuperiores = new int[cantDigitos];
        for (int i = 0; i < cantDigitos; i++) {
            cotasInferiores[i] = -1;
            cotasSuperiores[i] = 1;
        }
        EnumeradorLexicografico enumL =
                new EnumeradorLexicografico(cantDigitos, cotasInferiores, cotasSuperiores);

        boolean fin = false;

        int valorSimple = 1;
        int[] d;
        int idig = 0;
        do {

            if (simple){
                if(idig==cantDigitos){
                    d=null;
                }else{
                    d = new int[cantDigitos];
                    for(int id=0;id<cantDigitos;id++){
                        d[id]=0;
                    }
                    d[idig]=valorSimple;
                    if(valorSimple==1){
                        valorSimple = -1;
                    }else{
                        idig++;
                        valorSimple = 1;
                    }
                }
            }else{
                d = enumL.devuelveVector();
            }
            if (d != null) {               
                Parque candidato = null;
                boolean cumpleSig = true;
//                this.cargaResumenP(informacion, false);
                int[] codigoD = new int[est.getCantRecNoComunesYTransPosibles()];
                for(int i=0; i<codigoD.length; i++){
                    codigoD[i]=0;
                }
                int indRB = 0;
                for(RecursoBase rb: conjRBE.getRecursosB()){
                    int indiceCodigo = rb.getPosicionRecursoOPE();
                    codigoD[indiceCodigo] = codigoD[indiceCodigo] + d[indRB];
                    indRB ++;                    
                }
                Parque parqueD = new Parque(etapa, codigoD);
                try {
                    candidato = sumaParques(this, parqueD);
                } catch (XBloqueMultNeg ex) {
                    /**
                     * Al sumar con vector d los RecursoBase dio algÃºn multiplicador negativo
                     * el canditato no es admisible y no es un parque contiguo.
                     */
                    cumpleSig = false;
                }
                Portafolio pvacio = new Portafolio();
                NodoN nvacio = new NodoN();
                if (cumpleSig) {
                    for (RestGrafoE rest : conjRestSigParque.getRestricciones()) {
                        if (!rest.evaluar(informacion, candidato, pvacio, nvacio)) {
                            cumpleSig = false;
                            break;
                        }
                    }
                }
                if (cumpleSig) {
                    contiguos.add(candidato);
                }
            } else {
                fin = true;
            }
        } while (fin == false);

        return contiguos;
    }

// *******************************************************************
// ACA TERMINAN LOS MÃ‰TODOS QUE SOBREESCRIBEN LOS DE PORTAFOLIO
// *******************************************************************
//    /**
//     * Calcula la anualidad de inversiÃ³n para cada numerario del Parque incluyendo sÃ³lo bloques
//     * con Recursos OPE y Transformaciones FALL y TERMIN.
//     * NO SE INCLUYEN LOS BLOQUES COMUNES
//     * @return
//     */
//    public ArrayList<Double> anualidParque(Estudio est) throws XcargaDatos {
//        int cantNum = est.getDatosGenerales().getNumerarios().size();
//        ArrayList<Double> anualidP = new ArrayList<Double>();
//        for (int inum = 0; inum < cantNum; inum++) {
//            anualidP.add(0.0);
//        }
//        ArrayList<Bloque> bloques = bloquesDelParque(est);
// 
//        for (Bloque b : bloques) {
//            // se consideran recursos OPE y transformaciones FALL y TERMIN
//            if (b.estadoDeRecOTrans().equalsIgnoreCase(EstadosRec.OPE) || b.estadoDeRecOTrans().equalsIgnoreCase("FALL") || b.estadoDeRecOTrans().equalsIgnoreCase("TERMIN")) {
//                for (int inum = 0; inum < cantNum; inum++) {
//                    anualidP.set(inum, anualidP.get(inum) + b.anualidInv(est).get(inum));
//                }
//            }
//        }
//        return anualidP;
//    }

    /**
     * Calcula la anualidad de inversiÃ³n para cada numerario del Parque incluyendo sÃ³lo bloques
     * con Recursos OPE y Transformaciones FALL y TERMIN.
     * La anualidad se carga a la mitado del perÃ­odo
     * NO SE INCLUYEN LOS BLOQUES COMUNES
     * 
     *
     * @return
     */    
    public ArrayList<Double> anualidParqueCE(Estudio est) throws XcargaDatos {
        int cantNum = est.getDatosGenerales().getNumerarios().size();
        TiempoAbsoluto ta = tiempoAbsolutoDeParque(est);
        ArrayList<Double> anualidP = new ArrayList<Double>();
        for (int inum = 0; inum < cantNum; inum++) {
            anualidP.add(0.0);
        }
        int indice = 0;
        for (RecOTrans rot: est.getRecNoComunesYTransPosibles()) {
            // se consideran recursos OPE y transformaciones FALL y TERMIN           
            if (       rot.getEstado().equalsIgnoreCase(EstadosRec.OPE) 
                    || rot.getEstado().equalsIgnoreCase(EstadosRec.FALL) 
                    || rot.getEstado().equalsIgnoreCase(EstadosRec.TERMIN)   ) {
                for (int inum = 0; inum < cantNum; inum++) {
                    anualidP.set(inum, anualidP.get(inum) + rot.anualidInvRoT(est, ta).get(inum)*cBloques[indice]);
                }
            }
            indice ++;
        }
        return anualidP;
    }    
    
    
    /**
     * Devuelve para cada numerario el valor actual de inversión en el horizonte
     * de vida operativa de cada recursos o transformación que están en su primer año de construcción
     * (se aplicará a decisiones y no a parques)
     * @param est
     * @return
     * @throws XcargaDatos
     */
    public ArrayList<Double> invPasoDecision(Estudio est) throws XcargaDatos{
        int cantNum = est.getDatosGenerales().getNumerarios().size();
        TiempoAbsoluto ta = tiempoAbsolutoDeParque(est);
        ArrayList<Double> invH = new ArrayList<Double>();
        for (int inum = 0; inum < cantNum; inum++) {
            invH.add(0.0);
        }
        int indice = 0;
        for (RecOTrans rot: est.getRecNoComunesYTransPosibles()) {
            // se consideran recursos CON en el primer año de construcción y transformaciones CON en el primer año de construcción    	
            if (rot.getEstado().equalsIgnoreCase(EstadosRec.CON) && (rot.getIndicadorTiempo()== - rot.devuelvePerConst(est, ta))) {
                for (int inum = 0; inum < cantNum; inum++) {
                	invH.set(inum, invH.get(inum) + rot.invHRoTPasoDecision(est, ta).get(inum)*cBloques[indice]);
                }
            }
            indice ++;
        }
        return invH;
    	
    }
    
    
    /**
     * Devuelve un Ãºnico valor de anualidad de inversiÃ³n del parque
     * ponderando los numerarios.
     * NO ESTÃ�N INCLUIDOS LOS BLOQUES COMUNES
     * @param est
     * @return
     */
    public double anualidadPond(Estudio est, Objetivo obj) throws XcargaDatos {
        double aP = 0.0;
        ObjNumCron oGC = (ObjNumCron) obj;
        ArrayList<Double> pond = oGC.getPonderadores();
        ArrayList<Double> anPorNum = anualidParqueCE(est);
        for (int inum = 0; inum < pond.size(); inum++) {
            aP += pond.get(inum) * anPorNum.get(inum);
        }
        return aP;
    }

//    /**
//     * Calcula el costo fijo anual, adicional a las anualidades de inversiÃ³n
//     * para cada numerario del Parque incluyendo sÃ³lo bloques
//     * con Recursos OPE.
//     * @return
//     */
//    public ArrayList<Double> costoFijoParque(Estudio est) throws XcargaDatos {
//        ArrayList<Double> costosP = new ArrayList<Double>();
//        int cantNum = est.getDatosGenerales().getNumerarios().size();
//        for (int inum = 0; inum < cantNum; inum++) {
//            costosP.add(0.0);
//        }
//        for (Bloque b : bloques) {
//            // se consideran recursos OPE
//            if (b.estadoDeRecOTrans().equalsIgnoreCase(EstadosRec.OPE)) {
//                for (int inum = 0; inum < cantNum; inum++) {
//                    ArrayList<Double> cFijos = b.costosFijos(est);
//                    costosP.set(inum, costosP.get(inum) + cFijos.get(inum));
//                }
//
//            }
//        }
//        return costosP;
//    }
    /**
     * Devuelve un Ãºnico valor de costo fijo anual del parque
     * ponderando los numerarios.
     * SE CONSIDERAN SOLO LOS BLOQUE NO COMUNES.
     * @param est
     * @return
     */
    public double costoFijoPond(Estudio est, Objetivo obj, NodoN nN) throws XcargaDatos {
        double aP = 0.0;
        ObjNumCron oGC = (ObjNumCron) obj;
        ArrayList<Double> pond = oGC.getPonderadores();
        ArrayList<Double> cFPorNum = costoFijoParque(est, nN);
        for (int inum = 0; inum < pond.size(); inum++) {
            aP += pond.get(inum) * cFPorNum.get(inum);
        }
        return aP;
    }

//    /**
//     * Calcula el costo fijo anual, adicional a las anualidades de inversiÃ³n
//     * para cada numerario del Parque incluyendo sÃ³lo bloques
//     * con Recursos OPE.
//     * SE CONSIDERAN SOLO LOS BLOQUE NO COMUNES.
//     * SE USAN BLOQUES    
//     * @return
//     */
//    public ArrayList<Double> costoFijoParque(Estudio est, NodoN nN) throws XcargaDatos {
//        ArrayList<Double> costosP = new ArrayList<Double>();
//        int cantNum = est.getDatosGenerales().getNumerarios().size();
//        for (int inum = 0; inum < cantNum; inum++) {
//            costosP.add(0.0);
//        }
//        ArrayList<Bloque> bloques = bloquesDelParque(est);
//        for (Bloque b : bloques) {
//            // se consideran recursos OPE
//            if (b.estadoDeRecOTrans().equalsIgnoreCase(EstadosRec.OPE)) {
//                for (int inum = 0; inum < cantNum; inum++) {
//                    ArrayList<Double> cFijos = b.costosFijos(est, nN);
//                    costosP.set(inum, costosP.get(inum) + cFijos.get(inum));
//                }
//
//            }
//        }
//        return costosP;
//    }
    
    
    /**
     * Calcula el costo fijo anual, adicional a las anualidades de inversiÃ³n
     * para cada numerario del Parque incluyendo sÃ³lo Recursos OPE.
     * SE CONSIDERAN SOLO LOS RECURSOS NO COMUNES
     * LAS TRANSFORMACIONES NO TIENEN COSTOS FIJOS
     * 
     * NO SE USAN BLOQUES.
     * @return
     */
    public ArrayList<Double> costoFijoParque(Estudio est, NodoN nN) throws XcargaDatos {
        ArrayList<Double> costosP = new ArrayList<Double>();

        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int cantNum = datGen.getNumerarios().size();        
        TiempoAbsoluto ta = datGen.tiempoAbsolutoDeEtapa(etapa);
        for (int inum = 0; inum < cantNum; inum++) {
            costosP.add(0.0);
        }        
        for(int ind=0; ind<est.getCantRecNoComunes();ind++){
            int multiplicador = cBloques[ind];            
            Recurso rec = (Recurso)est.recNoComunYTransPosible(ind);
            if (rec.getEstado().equalsIgnoreCase(EstadosRec.OPE) && multiplicador>0) {
                ArrayList<Double> cFijos1Rec = new ArrayList<Double>();
                cFijos1Rec = rec.costoFijo1Mod(est, ta, nN);
                for (int inum = 0; inum< cantNum; inum++) {
                    /**
                     * Multiplica por la cantidad de mÃ³dulos del RecursoBase y por
                     * el multiplicador del Bloque.
                     */
                    costosP.set(inum, costosP.get(inum) +  cFijos1Rec.get(inum) * rec.cantMod(datGen, ta) * multiplicador);
                    inum++;                
                }
            }            

        }
        return costosP;
    }
        

    /**
     * Calcula el costo fijo actualizado al inicio del período (cFijoH), adicional a invH
     * para los recursos que están en el primer año de su período de construcción
     * (se aplica a decisones y no a parques)
     * SE CONSIDERAN SOLO LOS RECURSOS NO COMUNES
     * LAS TRANSFORMACIONES NO TIENEN COSTOS FIJOS
     * 
     * NO SE USAN BLOQUES.
     * @return
     */
    public ArrayList<Double> cFijoPasoDecision(Estudio est, NodoN nN) throws XcargaDatos {
    	int T = est.getDatosGenerales().getFinEstudio().getAnio(); 
    	int t = tiempoAbsolutoDeParque(est).getAnio();
    	DatosGeneralesEstudio datGen = est.getDatosGenerales();
        double tasaPaso = datGen.getTasaDescPaso();
        double factorDescPaso = 1 / (1 + tasaPaso);
        int vidaUtil = 0;
    	ArrayList<Double> costosP = new ArrayList<Double>();
        int cantNum = datGen.getNumerarios().size();        
        TiempoAbsoluto ta = datGen.tiempoAbsolutoDeEtapa(etapa);
        for (int inum = 0; inum < cantNum; inum++) {
            costosP.add(0.0);
        }  
    
        for(int ind=0; ind<est.getCantRecNoComunes();ind++){
            int multiplicador = cBloques[ind];  
            
            Recurso rec = (Recurso)est.recNoComunYTransPosible(ind);  
            RecursoBase rB = rec.getRecBase();
            int tabase = rB.getTiempoAbsolutoBase().getAnio();
            int desplazamiento = t - tabase; // desplazamiento en años para calcular el escalamiento de inversiones
            desplazamiento = Math.min(desplazamiento, rB.getEscalamientocFijo().size()-1);  
            int desplazamientoFin =  Math.min(T+1 - tabase, rB.getEscalamientocFijo().size()-1);  
            
            if (rec.getEstado().equalsIgnoreCase(EstadosRec.CON) && (rec.getIndicadorTiempo()==-rB.getPerConstr(ta, est)) && multiplicador>0) {
                ArrayList<Double> cFijos1Rec = new ArrayList<Double>();               
                cFijos1Rec = rec.costoFijo1Mod(est, ta, nN);
                vidaUtil = rec.getRecBase().getVidaUtil();
                int perConst = rec.getRecBase().getPerConstr();
                for (int inum = 0; inum< cantNum; inum++) {
                	if(t+perConst<=T){
	                    /**
	                     * Multiplica por la cantidad de mÃ³dulos del RecursoBase y por
	                     * el multiplicador del Bloque.
	                     */
                
                		double auxActVUtil = (1-Math.pow(factorDescPaso, vidaUtil))/(1-factorDescPaso);
                		double cFijIniConst1Num = cFijos1Rec.get(inum) * rec.cantMod(datGen, ta) * multiplicador * 
                				Math.pow(factorDescPaso, perConst+0.5) * auxActVUtil * rB.getEscalamientocFijo().get(desplazamiento); // idem inversión total a costo del año t                		
                		double cFFin = cFijos1Rec.get(inum) * rec.cantMod(datGen, ta) * multiplicador * rB.getEscalamientocFijo().get(desplazamientoFin);
                		double VRCF = cFFin * Math.pow(factorDescPaso, T+ 1.5-t) * (1- Math.pow(factorDescPaso, t+ perConst + vidaUtil-T-1)) / (1- factorDescPaso);
	                    costosP.set(inum, costosP.get(inum) + cFijIniConst1Num - VRCF );	                    
	                    inum++; 
                	}
                }
            }            

        }
        return costosP;
    }
    
    
    @Override
    public double calculaCostoTotalDecision(Objetivo obj, ArrayList<Object> informacion, NodoE nE) throws XcargaDatos{
    	double cTotInv=0.0;
    	double cTotFij=0.0;
    	Estudio est = (Estudio) informacion.get(0);
    	ObjNumCron oGC = (ObjNumCron) obj;
        ArrayList<Double> pond = oGC.getPonderadores();
        NodoN nN = nE.getNodoN();
        ArrayList<Double> cFijo = cFijoPasoDecision(est, nN);
        ArrayList<Double> cInv = invPasoDecision(est);
        for (int inum = 0; inum < pond.size(); inum++) {
            cTotInv += pond.get(inum)*cInv.get(inum);
            cTotFij += pond.get(inum)*cFijo.get(inum); 
        }
       
    	return cTotInv + cTotFij;
    }
    
    
    /**
     * Devuelve inversión y costos fijos actualizados de una decisión, que es un Portafolio
     * @param obj
     * @param informacion
     * @param nE
     * @return
     * @throws XcargaDatos
     */
    @Override
    public ArrayList<Double> totalInvYCFijosActualizados(Objetivo obj, ArrayList<Object> informacion, NodoE nE) throws XcargaDatos{
    	ArrayList<Double> orp = new ArrayList<Double>();
    	double cTotInv=0.0;
    	double cTotFij=0.0;
    	Estudio est = (Estudio) informacion.get(0);
    	ObjNumCron oGC = (ObjNumCron) obj;
        ArrayList<Double> pond = oGC.getPonderadores();
        NodoN nN = nE.getNodoN();
        ArrayList<Double> cFijo = cFijoPasoDecision(est, nN);
        ArrayList<Double> cInv = invPasoDecision(est);
        for (int inum = 0; inum < pond.size(); inum++) {
            cTotInv += pond.get(inum)*cInv.get(inum);
            cTotFij += pond.get(inum)*cFijo.get(inum); 
        }
        orp.add(cTotInv);
        orp.add(cTotFij);
    	return orp;
    	
    }
    
    
    
    
    

//    public void cargaNombreSimul(Object obBolsaSim, NodoN nt, ArrayList<Object> informacion) throws XcargaDatos {
// 
////        IdentSimulPaso idSim = new IdentSimulPaso(parqueOperativo, nt.getValoresTiposDatos());
////        int ie = this.etapa;
////        Estudio est = (Estudio) informacion.get(0);
//        BolsaDeSimulaciones3 bolsaSim = (BolsaDeSimulaciones3) obBolsaSim;
//        ResSimul resSim = bolsaSim.devuelveRes(this, nt, informacion);
//        nombreSimul = resSim.getNombreSimul();
//    }
    
    /**
     * Crea el array int[] de cBloquesAux
     */
    public static void creaCBloquesAux(int dimension){
        cBloquesAux = new int[dimension];    
    }
    
    /**
     * Llena con ceros cBloquesAux
     */
    public static void anulaCBloquesAux(){
        for(int i=0; i<cBloquesAux.length; i++){
            cBloquesAux[i] = 0;
        }
    }

    /**
     * Calcula los bloquesComunes para todas las etapas representativas
     * del Estudio est
     * ATENCION: EN EL FUTURO LOS BLOQUES COMUNES PUEDEN DEPENDER DEL CASO
     * 
     * 
     */
    public static void calculaBloquesComunes(ArrayList<Object> informacion) throws XcargaDatos {
        Estudio est = (Estudio) informacion.get(0);
        Caso caso = (Caso) informacion.get(1);
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        TiempoAbsoluto taUno = datGen.tiempoAbsolutoDePaso(1);
        GrafoN grafoN = est.getGrafoN();
        ArrayList<Bloque> bloquesIni = new ArrayList<Bloque>();
        try {
            /**
             *  Carga los bloques comunes de las expansiones en el TiempoAbsoluto inicial
             */
            ArrayList<Bloque> bloquesExpEstudio = est.getExpansionForzadaEstudio().bloquesDeTiempoAbsoluto(taUno);
            ArrayList<Bloque> bloquesExpCaso = caso.getExpansionForzadaCaso().bloquesDeTiempoAbsoluto(taUno);
            ArrayList<Bloque> bloquesUnTA = new ArrayList<Bloque>();
            for (Bloque b : bloquesExpEstudio) {
                if (b.recBaseEsComun()) {
                    ParqueCreaSuc.agregaBloque(bloquesUnTA, b);
                    bloquesIni.add(b.copiaBloque());
                }
            }
            for (Bloque b : bloquesExpCaso) {
                if (b.recBaseEsComun()) {
                    ParqueCreaSuc.agregaBloque(bloquesUnTA, b);
                }
                bloquesIni.add(b.copiaBloque());
            }
            bloquesComunes.add(bloquesUnTA);
            /**
             * Carga los bloques comunes sucesivos que resultan de la evoluciÃ³n
             * de los de la etapa anterior y el agregado de los bloques comunes de las 
             * expaniones forzadas.
             */
            for (int ie = 1; ie < datGen.getCantEtapas(); ie++) {
                /**
                 * Avanza el tiempo de los recursos que hay en bloquesIni
                 * y carga los recursos cambiados en bloquesFin.
                 * Se eligen nodosN cualesquiera porque la evoluciÃ³n de bloquesComunes es determinÃ­stica.
                 */
                TiempoAbsoluto ta = datGen.tiempoAbsolutoDeEtapa(ie);
                TiempoAbsoluto tamas1 = datGen.tiempoAbsolutoDeEtapa(ie + 1);
                int avanceTiempo = datGen.pasoRepDeEtapa(ie + 1) - datGen.pasoRepDeEtapa(ie);
                NodoN nNe = grafoN.nodoEtapaOrdinal(ie, 1);
                NodoN nNemas1 = grafoN.nodoEtapaOrdinal(ie + 1, 1);
                ArrayList<Bloque> bloquesFin = new ArrayList<Bloque>();
                ParqueCreaSuc.avanzaTiempoRecursos(bloquesIni, bloquesFin, nNe, nNemas1, tamas1, avanceTiempo, informacion);
                /**
                 * Agrega en bloquesFin los bloques comunes de las expansionesForzadas
                 */
                ArrayList<Bloque> bloquesFin2 = ParqueCreaSuc.agregaExpsForzadas(bloquesFin, ta, tamas1, true, false, informacion);
                bloquesUnTA = new ArrayList<Bloque>();
                bloquesIni = new ArrayList<Bloque>();
                for (Bloque b : bloquesFin2) {
                    bloquesUnTA.add(b);
                    bloquesIni.add(b.copiaBloque());
                }
                bloquesComunes.add(bloquesUnTA);
            }
        } catch (XBloqueMultNeg ex) {
            throw new XcargaDatos("Al agregar bloques un multiplicador dio negativo");
        }
    }
    
    
//    /**
//     * Calcula los parquesComunes para todas las etapas representativas
//     * del Estudio est
//     * ATENCION: EN EL FUTURO LOS PARQUES COMUNES PUEDEN DEPENDER DEL CASO
//     * 
//     * OJOJOJOJOJOJOJOJO:
//     * ESTO ES PROVISORIO EN TANTO NO SE CREEN LOS METODOS DE SUCESORES DE PARQUE
//     * QUE PRESCINDAN DE LOS BLOQUES.
//     * 
//     * 
//     */
//    public static void calculaParquesComunes(ArrayList<Object> informacion) throws XcargaDatos {
//        Estudio est = (Estudio) informacion.get(0);
//        int cantCod = est.getCantRecNoComunesYTransPosibles();
//        Caso caso = (Caso) informacion.get(1);
//        DatosGeneralesEstudio datGen = est.getDatosGenerales();
//        TiempoAbsoluto taUno = datGen.tiempoAbsolutoDePaso(1);
//        GrafoN grafoN = est.getGrafoN();
//        ArrayList<Bloque> bloquesIni = new ArrayList<Bloque>();
//        try {
//            /**
//             *  Carga los bloques comunes de las expansiones en el TiempoAbsoluto inicial
//             */
//            ArrayList<Bloque> bloquesExpEstudio = est.getExpansionForzadaEstudio().bloquesDeTiempoAbsoluto(taUno);
//            ArrayList<Bloque> bloquesExpCaso = caso.getExpansionForzadaCaso().bloquesDeTiempoAbsoluto(taUno);
//            ArrayList<Bloque> bloquesUnTA = new ArrayList<Bloque>();
//            for (Bloque b : bloquesExpEstudio) {
//                if (b.recBaseEsComun()) {
//                    ParqueCreaSuc.agregaBloque(bloquesUnTA, b);
//                    bloquesIni.add(b.copiaBloque());
//                }
//            }
//            for (Bloque b : bloquesExpCaso) {
//                if (b.recBaseEsComun()) {
//                    ParqueCreaSuc.agregaBloque(bloquesUnTA, b);
//                }
//                bloquesIni.add(b.copiaBloque());
//            }
//            bloquesComunes.add(bloquesUnTA);
//            /**
//             * Carga los bloques comunes sucesivos que resultan de la evoluciÃ³n
//             * de los de la etapa anterior y el agregado de los bloques comunes de las 
//             * expaniones forzadas.
//             */
//            for (int ie = 1; ie < datGen.getCantEtapas(); ie++) {
//                /**
//                 * Avanza el tiempo de los recursos que hay en bloquesIni
//                 * y carga los recursos cambiados en bloquesFin.
//                 * Se eligen nodosN cualesquiera porque la evoluciÃ³n de bloquesComunes es determinÃ­stica.
//                 */
//                TiempoAbsoluto ta = datGen.tiempoAbsolutoDeEtapa(ie);
//                TiempoAbsoluto tamas1 = datGen.tiempoAbsolutoDeEtapa(ie + 1);
//                int avanceTiempo = datGen.pasoRepDeEtapa(ie + 1) - datGen.pasoRepDeEtapa(ie);
//                NodoN nNe = grafoN.nodoEtapaOrdinal(ie, 1);
//                NodoN nNemas1 = grafoN.nodoEtapaOrdinal(ie + 1, 1);
//                ArrayList<Bloque> bloquesFin = new ArrayList<Bloque>();
//                ParqueCreaSuc.avanzaTiempoRecursos(bloquesIni, bloquesFin, nNe, nNemas1, tamas1, avanceTiempo, informacion);
//                /**
//                 * Agrega en bloquesFin los bloques comunes de las expansionesForzadas
//                 */
//                ArrayList<Bloque> bloquesFin2 = ParqueCreaSuc.agregaExpsForzadas(bloquesFin, ta, tamas1, true, false, informacion);
//                bloquesUnTA = new ArrayList<Bloque>();
//                // AGREGADO                
//                int[] codigoUnTa = new int[cantCod];
//                // AGREGADO                
//                bloquesIni = new ArrayList<Bloque>();
//
//                for (Bloque b : bloquesFin2) {
//                    bloquesUnTA.add(b);
//                    // AGREGADO                    
//                    RecOTrans rot = b.getObjeto();
//                    int indice = est.indiceEnPosibles(rot);
//                    codigoUnTa[indice] = codigoUnTa[indice] + b.getMultiplicador();
//                    // AGREGADO                    
//                    bloquesIni.add(b.copiaBloque());
//                }
//                bloquesComunes.add(bloquesUnTA);
//                // AGREGADO
//                parquesComunes.add(new Parque(ie, codigoUnTa));
//                // AGREGADO
//            }
//        } catch (XBloqueMultNeg ex) {
//            throw new XcargaDatos("Al agregar bloques un multiplicador dio negativo");
//        }
//    }
//    
    
    
    
    
    
    
    
    
    
    
    
    
    

    /**
     * Devuelve los Bloques de bloquesComunes que existen en el TiempoAbsoluto ta
     * @param ta es el TiempoAbsoluto para el que se piden los bloques comunes
     */
    public static ArrayList<Bloque> bloquesComunesDeTA(TiempoAbsoluto ta, Estudio est) throws XcargaDatos {
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int etapa = datGen.etapaDeUnPaso(datGen.pasoDeTiempoAbsoluto(ta));
        return bloquesComunes.get(etapa - 1);
    }
    
//    /**
//     * Devuelve el Parque de parquesComunes que existe en el TiempoAbsoluto ta
//     * @param ta es el TiempoAbsoluto para el que se pide el parque comÃºn 
//     */
//    public static Parque parqueComunDeTA(TiempoAbsoluto ta, Estudio est) throws XcargaDatos {
//        DatosGeneralesEstudio datGen = est.getDatosGenerales();
//        int etapa = datGen.etapaDeUnPaso(datGen.pasoDeTiempoAbsoluto(ta));
//        return parquesComunes.get(etapa - 1);
//    }    
    
    
    
    
    
    

    /***************************************************************
     *
     * ATENCION EL HASH Y EL EQUALS DE PARQUE NO TIENEN EN CUENTA
     * ETAPA Y PERIODO DE TIEMPO Y SE APLICAN SOLO A BLOQUES NO COMUNES
     *
     * *************************************************************
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Parque other = (Parque) obj;
        if (this.cBloques != other.cBloques && (this.cBloques == null || !Arrays.equals(this.cBloques, other.cBloques))) {
            return false;
        }
//        if (this.bloques != other.bloques && (this.bloques == null || !this.bloques.equals(other.bloques))) {
//            return false;
//        }        
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Arrays.hashCode(this.cBloques);
        return hash;
    }

    @Override
    public String toString() {
        String texto = "----------------------------------" + "\n";
        texto += "INICIA DESCRIPCION DE PARQUE" + "\n";
        texto += "----------------------------------" + "\n";
        texto += "BLOQUES DEL PARQUE" + "\t";
        if (cBloques == null) {
            texto += " sin bloques";
        }
        for (int blo : cBloques) {
            texto += blo + ",";
        }
        return texto;
    }


    /**
     * Genera un texto ordenado que describe el Parque.
     * ATENCION: ESTE METODO SIRVE PARA GENERAR LA CLAVE EN LA BOLSA DE SIMULACIONES
     * SI SE CAMBIA EL TEXTO QUE GENERA EL MÃ‰TODO LAS SIMULACIONES EXISTENTES SE PIERDEN
     * En el texto resultante aparecen primero los Recursos comunes y despues los 
     * Recursos no comunes y transformaciones, ordenados de acuerdo al orden de 
     * cBloques definido en recNoComunesYTransPosibles del Estudio.
     * 
     * @param informacionImp, contiene booleans con opciones de impresion
     * informacionImp.get(0) soloNoComunes - si es true no imprime los Recursos de los bloquesComunes 
     * de los Parques.
     * informacionImp.get(1) prod - si es true imprime el aporte de Productos de cada Bloque de Recursos OPE
     * informacionImp.get(2) usaEquiv - si es true usa el nombre de los RecursosBase equivalentes en
     * la simulaciÃ³n para los RecursosBase que los tienen.
     * informacionImp.get(3) soloNoNulos - si es true no imprime los Recursos o Transformaciones con multiplicador nulo.
     * 
     * @return
     * @throws persistenciaMingo.XcargaDatos
     * ---------------------------------------------------------------------------------
     * ATENCION: LO QUE GENERA ESTE METODO ES LA CLAVE EN LA BOLSA DE SIMULACIONES
     * SI SE CAMBIA EL TEXTO QUE GENERA EL MÃ‰TODO LAS SIMULACIONES EXISTENTES SE PIERDEN
     * ---------------------------------------------------------------------------------
     */
    @Override
    public String toStringCorto(ArrayList<Object> informacion, ArrayList<Boolean> informacionImp) throws XcargaDatos {
        Estudio est = (Estudio) informacion.get(0);
        ArrayList<RecOTrans> posibles = est.getRecNoComunesYTransPosibles();
        boolean soloNoComunes = (boolean) informacionImp.get(0);
        boolean prod = (boolean) informacionImp.get(1);
        boolean usaEquiv = (boolean) informacionImp.get(2);
        boolean soloNoNulos = (boolean) informacionImp.get(3);
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(etapa);
        int[] cBloquesEquiv = new int[est.getCantRecNoComunesYTransPosibles()];
        // los Recursos que aparecen porque se generan a partir de otros equivalentes
        int[] cBloquesImp = new int[est.getCantRecNoComunesYTransPosibles()];        
        // el total final
        boolean cBloquesVacio = true;
        for(int i=0; i<cBloques.length; i++){
            if(cBloques[i]!=0)cBloquesVacio = false;
            cBloquesEquiv[i] = 0;
        }
        if(!cBloquesVacio){
            if (usaEquiv) {
                for(int i=0; i<cBloques.length; i++){
                    RecOTrans rot = posibles.get(i);
                    RecursoBase rB = rot.devuelveRB();
                    if(rB!= null  && rB.tieneEquivEnSimul()){ 
                        // rot es un Recurso y tiene equivalente en simulaciÃ³n
                        RecursoBase rBE = rB.getEquivEnSimul();
                        Recurso recE = ((Recurso)rot).copiaRecurso();
                        recE.setRecBase(rBE);
                        int indiceEquiv = est.indiceEnPosibles(recE);
                        cBloquesEquiv[indiceEquiv] = cBloquesEquiv[indiceEquiv] + cBloques[i];
                        cBloquesEquiv[i] = -cBloques[i];
                    }
                }
                for(int i=0; i<cBloques.length; i++){
                    cBloquesImp[i] = cBloquesEquiv[i] + cBloques[i];
                }
            } else {
                System.arraycopy(cBloques, 0, cBloquesImp, 0, cBloques.length);
            }

        }
        String texto = "";        
        if (!soloNoComunes) {
            // se imprimen bloques comunes
            for(Bloque b: bloquesComunesDeTA(ta, est)){
                if(!soloNoNulos || (soloNoNulos & b.getMultiplicador()!=0)) texto += b.toStringCorto(usaEquiv);
            }
        }
        // Se imprimen bloques no comunes
        if (cBloquesVacio) {
//            texto += "Sin bloques"; // OJO SI SE SACA ESTE COMENTARIO NO SON COMPATIBLES LAS VIEJAS BOLSAS DE SIMULACIONES
        }


        int indice = 0;
        for (RecOTrans rot : est.getRecNoComunesYTransPosibles()) {
            if(!soloNoNulos || (soloNoNulos & cBloquesImp[indice]!=0)) { 
                int mult = cBloquesImp[indice];
                texto += rot.toStringMuyCorto(usaEquiv);
                texto += "/m" + mult;            
                texto += " + ";
                if (rot.devuelveRB()!=null  && ((Recurso)rot).getEstado().equalsIgnoreCase(EstadosRec.OPE) && prod) {
                    for (Producto pr : datGen.getProductos()) {
                        Recurso rec = (Recurso)rot;
                        CantProductoSimple cant = rec.cantProdSimp(pr, datGen, ta);
                        cant.escalarCant(mult);
                        texto += cant.toStringCorto();
                    }
                    texto+="\r\n";
                }
            }
            indice ++;
        }

        if (prod) {
            ArrayList<Producto> productos = est.getDatosGenerales().getProductos();
            texto += "\r\nCantidades de producto en los recursos operativos:  ";
            for (Producto pr : productos) {
                texto += cantProdGenParqueCE(pr, EstadosRec.OPE, informacion).toStringCorto() + "\t";
            }
        }
        return texto;
    }

    

    
    

}
