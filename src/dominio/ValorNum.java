/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import GrafoEstados.Numerario;

/**
 *
 * @author ut469262
 */
public class ValorNum {

	/**
	 * Es una pareja numerario, valor real asociado a ese numerario
	 */
	private Numerario num;
	private double valor;

	public ValorNum(Numerario num, double valor) {
		this.num = num;
		this.valor = valor;
	}



	public Numerario getNum() {
		return num;
	}

	public void setNum(Numerario num) {
		this.num = num;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}





}
