/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;
import java.util.ArrayList;

import persistenciaMingo.CargaFuentes;
import persistenciaMingo.XcargaDatos;

import java.io.Serializable;

/**
 *
 * @author ut469262
 */
public class ConjFuentes implements Serializable{
    private ArrayList<Fuente> fuentes;

    public ConjFuentes() {
        fuentes = new ArrayList<Fuente>();
    }

    public void agregar(Fuente fuen){
        fuentes.add(fuen);
    }

    /**
     * Si existe la fuente de nombre nomFuente en el conjunto la elimina
     * y devuelve true. Si no existe una con ese nombre devuelve false.
     *
     */
    public boolean remover(String nomFuente){
        Fuente aEliminar = null;
        boolean elimino = false;
        for(Fuente fuen: fuentes){            
            if( nomFuente.compareToIgnoreCase(fuen.getNombre()) == 0 ){
                aEliminar = fuen;
                elimino = true;
                break;
            }
        }
        fuentes.remove(aEliminar);
        return elimino;
    }


    public Fuente fuenteDeNombre(String nomFuente) throws XcargaDatos{
        Fuente result = null;
        boolean encontro = false;
        for(Fuente fuen: fuentes){            
            if( nomFuente.compareToIgnoreCase(fuen.getNombre()) == 0 ){
                result = fuen;
                encontro = true;
                break;
            }
        }
        if(! encontro) throw new XcargaDatos("No encontro en el conjunto de fuentes la fuente de" +
                "nombre " + nomFuente);
        return result;
    }

    @Override
    public String toString() {
		String texto = "====================================================" + "\n";
		texto += "COMIENZA CONJUNTO DE FUENTES " + "\n";
        texto += "====================================================" + "\n";
        int i;
        for(i = 0; i < fuentes.size(); i++){
            texto += fuentes.get(i).toString() + "\n\n";
        }
        return texto;
    }





    public static void main(String[] args){
        ConjFuentes c1F = new ConjFuentes();
        String dirArchivo = "D:/Java/PruebaJava/V1-Fuentes.txt";
        CargaFuentes cargador = new CargaFuentes() {};
        try{
            cargador.cargarConjFuentes(dirArchivo, c1F);
        }catch(XcargaDatos ex){
            System.out.println("------ERROR-------");
            System.out.println(ex.getDescripcion());
            System.out.println("------------------\n");}

        System.out.println( c1F.toString() + "\n" );
    }




}
