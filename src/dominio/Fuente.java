/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class Fuente implements Serializable{
    
    protected TipoFuente tipo;
    protected String nombre;
    /**
     * si es false no requiere inyector
     * es el tratamiento que se dará inicialmente a todos los datos
     */
    protected boolean requiereInyector;
    /**
     * ESTO NO SE USA POR AHORA
     */
    protected ArrayList<DispAnioSemana> disponibilidad;

    protected double potMax;  // máxima potencia en MW de la fuente, fija o con la que nace.
    protected double energMaxPaso;   // máxima energía anual en GWh de la fuente, fija o con la que nace.
    protected TiempoAbsoluto iniDisp;  // primer tiempo absoluto en que puede usarse.
    protected TiempoAbsoluto finDisp;  // último tiempo absoluto en que puede usarse.
    /**
     * Tiempo absoluto base a partir del cual se mide la dependencia según
     * PASO_DE_TIEMPO de potencia y energía máximas.
     */
    protected TiempoAbsoluto tiempoAbsolutoBase;
    /**
     *  Potencia nominal por paso de tiempo
     *  en caso de que dependa del paso de tiempo.
     *  El get(0) tiene la potencia en el tiempoAbsolutoBase
     *    si el recurso depende del paso de tiempo absoluto
     *  ATENCION !!!!!!!!!
     *  Si la potencia para una edad o paso de tiempo no se explicita
     *  se tiene un error. El programa no hace supuestos al respecto.
     */
    protected ArrayList<Double> potMaxT;
    /**
     * Lo mismo para energMax por paso de tiempo.
     */
    protected ArrayList<Double> energMaxPasoT;
    
    public enum TipoFuente{
        COMBUS;
    }


    public Fuente() {
        tipo = TipoFuente.COMBUS;
        nombre = "";
        requiereInyector = false;
        disponibilidad = new ArrayList<DispAnioSemana>();
        potMax = 0.0;
        energMaxPaso = 0.0;
        potMaxT = new ArrayList<Double>();
        energMaxPasoT = new ArrayList<Double>();
    }

    public ArrayList<DispAnioSemana> getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(ArrayList<DispAnioSemana> disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public TipoFuente getTipo() {
        return tipo;
    }

    public void setTipo(TipoFuente tipo) {
        this.tipo = tipo;
    }

    

    public double getEnergMaxPaso() {
        return energMaxPaso;
    }

    public void setEnergMaxPaso(double energMaxPaso) {
        this.energMaxPaso = energMaxPaso;
    }

    public ArrayList<Double> getEnergMaxPasoT() {
        return energMaxPasoT;
    }

    public void setEnergMaxPasoT(ArrayList<Double> energMaxPasoT) {
        this.energMaxPasoT = energMaxPasoT;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPotMax() {
        return potMax;
    }

    public void setPotMax(double potMax) {
        this.potMax = potMax;
    }

    public ArrayList<Double> getPotMaxT() {
        return potMaxT;
    }

    public void setPotMaxT(ArrayList<Double> potMaxT) {
        this.potMaxT = potMaxT;
    }

    public boolean isRequiereInyector() {
        return requiereInyector;
    }

    public void setRequiereInyector(boolean requiereInyector) {
        this.requiereInyector = requiereInyector;
    }

    public TiempoAbsoluto getTiempoAbsolutoBase() {
        return tiempoAbsolutoBase;
    }

    public void setTiempoAbsolutoBase(TiempoAbsoluto tiempoAbsolutoBase) {
        this.tiempoAbsolutoBase = tiempoAbsolutoBase;
    }

    public TiempoAbsoluto getFinDisp() {
        return finDisp;
    }

    public void setFinDisp(TiempoAbsoluto finDisp) {
        this.finDisp = finDisp;
    }

    public TiempoAbsoluto getIniDisp() {
        return iniDisp;
    }

    public void setIniDisp(TiempoAbsoluto iniDisp) {
        this.iniDisp = iniDisp;
    }


    public static TipoFuente tipoDeFuente(String texto){
        return TipoFuente.COMBUS;
    }



    @Override
    public String toString() {
        String texto = "";
        texto += "FUENTE: " + nombre + "\n";
        texto += "Tipo: " + tipo + "\n";
        texto += "Requiere Inyector: " + requiereInyector;
        for (DispAnioSemana das: disponibilidad){
			texto += das.toString();
		}
		texto +=  "\n";
        texto += "TiempoAbsoluto inicial: " + iniDisp.toStringCorto() + "\n";
        texto += "TiempoAbsoluto final: " + finDisp.toStringCorto() + "\n";
        texto += "Potencia máxima: " + potMax + "\n";
        texto += "Energía máxima anual: " + energMaxPaso + "\n";
        return texto;

    }




}
