/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;
import java.util.ArrayList;

import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class ConjTiposDeDato implements Serializable{ 
     
    /**
     * Contiene las plantillas asociadas a los tipos en el mismo orden.
     */
    private ArrayList<TipoDeDatos> tipos;

    public ConjTiposDeDato() {
        tipos = new ArrayList<TipoDeDatos>();
    }

    public ArrayList<TipoDeDatos> getTipos() {
        return tipos;
    }

    public void setTipos(ArrayList<TipoDeDatos> tipos) {
        this.tipos = tipos;
    }




    /**
     * Devuelve el tipo de datos de nombre nom y si el tipo no existe
     * lanza una excepción.
     * @param nom es el nombre del tipo que se busca
     * @param est es el Estudio
     * @return tipo es el tipo si existe.
     */
    public TipoDeDatos tipoDeNombre(String nom) throws XcargaDatos{
        TipoDeDatos tipo = null;
        boolean encontro = false;
        int itip = 0;
        do{
            if (nom.equalsIgnoreCase(tipos.get(itip).getNombre())){
                encontro = true;
                tipo = tipos.get(itip);
            }
            itip++;
        }while(itip < tipos.size() && !encontro);
        if(!encontro) throw new XcargaDatos("No existe el tipo de datos de nombre " + nom);
        return tipo;
    }


//    /**
//     * Carga las plantillas en texto que se emplean para fabricar archivos de comandos,
//     * asociadas a cada uno de los tipos en su orden respectivo.
//     * @param dirArchivo es el path del archivo de plantillas.
//     */
//    public void cargaPlantillasDeTipos(String dirArchivo) throws XcargaDatos{
//        ArrayList<PlantillaEnTexto> plantAux = CreadorTex.leePlantillas(dirArchivo);
//        for(TipoDeDatos tip: tipos){
//            int iplan = 0;
//            boolean encontro = false;
//            do{
//               if(tip.getNombre().equalsIgnoreCase(plantAux.get(iplan).getNombre()) ){
//                   encontro = true;
//                   ParTipoDatoPlantilla parTP = new ParTipoDatoPlantilla(tip, plantAux.get(iplan) );
//                   plantillasDeTipos.add(parTP);
//               }
//               iplan++;
//            }while(iplan < plantAux.size() && !encontro);
//        }
//    }


    @Override
    public String toString(){

        String texto = "===================================" + "\n";
        texto += "Conjunto de tipos de datos" + "\n";
        texto += "===================================" + "\n";
        for (TipoDeDatos tip: tipos){
            texto += tip.toString() + "\n";
        }

        return texto;
    }

//    public static void main(String[] args) throws XcargaDatos, IOException{
//        String directorio = "D:/Java/PruebaJava3";
//        Estudio est = new Estudio("prueba leer tipo de datos ", 4);
//        boolean imprimir = false;
//        est.leerEstudio(directorio, imprimir);
//        String dirArchivo = directorio + "/" + "tiposDeDatos.txt";
//        ConjTiposDeDato conjTD = new ConjTiposDeDato();
//        CargaTiposDeDatoEst.cargarTiposDato(dirArchivo, conjTD, est);
//        System.out.print(conjTD.toString());
//    }

}
