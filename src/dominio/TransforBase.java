/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

/**
 *
 * @author ut469262
 */
import TiposEnum.ExistEleg;
import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.VariableNat;
import persistenciaMingo.XcargaDatos;

/**
 *
 * @author ut600232
 *
 * Los recursos destino de una transformacion base deben ser recursos elegibles
 * con restricciones de inicio de construcciÃ³n nulas, si la Ãºnica forma
 * de obtenerlos es con la transformaciÃ³n base.
 * Si una transformaciÃ³n se quiere aplicar a un RecursoBase sujeto a un cambio
 * aleatorio el programa da error.
 *
 * ATENCION: EN LA IMPLEMENTACIÃ“N ACTUAL LAS TRANSFORMACIONES NO PUEDEN TENER
 * INVERSIÃ“N ALEATORIA NI DEMORA ALEATORIA DE CONSTRUCCIÃ“N
 * 
 * 
 * SE BUSCAN COMO RECURSOS ORIGEN EN EL PARQUE LOS QUE TENGAN EL MISMO RECURSO BASE
 * Y EL MISMO ESTADO QUE LOS ESPECIFICADOS EN EL CAMBIO ALEATORIO, SIN IMPORTAR
 * LOS VALORES DE LOS OTROS DATOS.

 *
 * LAS INVERSIONES DE LA TRANSFORMACION TERMINADA O FALLIDA QUEDAN EN LOS PARQUES PARA
 * CONSERVAR EL COSTO DE INVERSION. LA INVERSION DE LOS RECURSOS DESTINO DEBE TENER
 * EN CUENTA ESTO PARA NO DUPLICAR INVERSIONES.
 * UN CRITERIO POSIBLE ES QUE LOS RECURSOS DESTINO CONSERVEN LA ANUALIDAD DE LOS
 * RECURSOS ORIGEN.
 *
 * LAS TRANSFORMACIONES TERMINADAS O FALLIDAS NO APORTAN COSTOS VARIABLES
 * QUE DEBEN PROCEDER DE LOS RECURSOS DESTINO.
 *
 *
 */
public class TransforBase implements Serializable, Comparable {
    /**
     * PRUEBA PARA VER SI PUEDO COMITEAR (M.I)
     */

    Estudio est;
    private String nombre;
    private String clase;
    private ArrayList<Recurso> recursosOrigen;
    /**
     *
     */
    private ArrayList<Integer> minVidaRes;
    private ArrayList<Recurso> recursosTransitorios;
    /**
     * Una lista vacÃ­a de recursosTransitorios significa que se pasa
     * directamente de los recursosOrigen a los recursosDestino.
     * Los recursos transitorios no pueden depender del tiempo ni de la edad y deben
     * ser de tipo ELEG o TRANS.
     * 
     * ATENCIÃ“N LOS RECURSOS TRANSITORIOS NO ESTÃ�N IMPLEMENTADOS 
     * 
     */
    private ArrayList<Recurso> recursosDestino;
    /**
     * Los recursosBase de recursosDestino no pueden
     * Una lista vacÃ­a de recursosDestino significa que desaparecen los recursos
     * origen
     */

    private ArrayList<Integer> mulRecOrigen;         // cantidad de Recursos de los que aparecen
    
    private ArrayList<Integer> mulRecDestino;            // cantidad de Recursos de los que aparecen
    
    private ArrayList<Integer> mulRecTransit;            // cantidad de Recursos de los que aparecen
    private ArrayList<String> edadRecursosDestinoStr;
    /**
     * edad recursos destino, puede ser:
     * -el nombre de uno de los recurso origen
     * -maximo (se interpreta la mÃ¡xima v.residual de los rec. origen
     * -Ã­dem minimo
     * -un nÃºmero entero
     */

    private String tipo;
    private int perConstr;
    private boolean tieneDemoraAl;
    private VariableNat vNDemoraAl;  // El nombre de la variable es el de la TransforBase con sufijo "_DEMAL".
    private ArrayList<String> valoresDemAlS;
    private ArrayList<Integer> valoresDemAl;
    /**
     * Primer Ã­ndice: es el Ã­ndice de numerarios, en el orden en que estÃ¡n en DatosGeneralesEstudio.
     *
     * Segundo Ã­ndice: En el get(0) estÃ¡ la inversiÃ³n del primer paso de tiempo de construcciÃ³n que se supone
     * cargada en el instante medio del paso. En el get(size-1) estÃ¡ la inversiÃ³n en el aÃ±o
     * anterior al de entrada en servicio normal.
     *
     * UNA TRANSFORMACIÃ“N UNA VEZ COMPLETADA O CON RESULTADO FALLIDA QUEDA EN EL PARQUE
     * AL SOLO EFECTO DEL CALCULO DE LOS COSTOS DE ANUALIDAD DE INVERSION
     */
    private ArrayList<ArrayList<Double>> inversiones;
    
    
    private ArrayList<Double> escalamientoInv;  // variación en por uno del precio de inversiones overhead 
    // tomando com o base 1 en el tiempo absoluto base

    private ArrayList<Double> escalamientocFijo;  // variación en por uno del precio de costos fijo 
	  // tomando com o base 1 en el tiempo absoluto base
    
//    private ArrayList<ArrayList<Double>> costosFijos;
    private boolean tieneInvAleat;
    private VariableNat vNInvAl;
    private ArrayList<String> valoresInvAlS;
    private ArrayList<Double> valoresInvAl;
    
    private int posicionTransformacionInicial;    
    /**
     * Es la posiciÃ³n en el cÃ³digo de recursos no comunes de Parque, cBloques,
     * del primer recurso asociado a este RecursoBase.
     */    
    

    public TransforBase(Estudio est) {

        this.est = est;
        nombre = "";
        clase = ExistEleg.ELEG;
        recursosOrigen = new ArrayList<Recurso>();
        minVidaRes = new ArrayList<Integer>();
        recursosTransitorios = new ArrayList<Recurso>();
        recursosDestino = new ArrayList<Recurso>();
        mulRecOrigen = new ArrayList<Integer>();        
        mulRecDestino = new ArrayList<Integer>();        
        mulRecTransit = new ArrayList<Integer>();
        edadRecursosDestinoStr = new ArrayList<String>();
        tipo = "";

        perConstr = 0;
        tieneDemoraAl = false;
        valoresDemAl = new ArrayList<Integer>();
        inversiones = new ArrayList<ArrayList<Double>>();
//        costosFijos = new ArrayList<ArrayList<Double>>();
        for (int inum = 0; inum < est.getDatosGenerales().getNumerarios().size(); inum++) {
            ArrayList<Double> auxDI = new ArrayList<Double>();
            inversiones.add(auxDI);
            ArrayList<Double> auxDCF = new ArrayList<Double>();
//            costosFijos.add(auxDCF);
        }
        valoresInvAlS = new ArrayList<String>();        
        valoresInvAl = new ArrayList<Double>();
        valoresDemAlS = new ArrayList<String>();                
        valoresDemAl = new ArrayList<Integer>();
        tieneInvAleat = false;
        valoresInvAl = new ArrayList<Double>();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

//     public void setClase(String clase) {
//          assert (clase.equalsIgnoreCase("EXISTENTE") ||
//                  clase.equalsIgnoreCase("ELEGIBLE")) :
//                  "clase de una transformaciÃ³n no es existente ni elegible, recurso :" +
//                  this.nombre + " - clase leÃ­da:" + clase;
//          if (clase.equalsIgnoreCase("EXISTENTE")) {
//               this.clase = ExistEleg.EXIST;
//          }
//     }
    public void addRecOrigen(Recurso rec) {
        recursosOrigen.add(rec);
    }

    public void setRecursosOrigen(ArrayList<Recurso> recursosOrigen) {
        this.recursosOrigen = recursosOrigen;
    }

    public ArrayList<Integer> getMinVidaRes() {
        return minVidaRes;
    }

    public void setMinVidaRes(ArrayList<Integer> minVidaRes) {
        this.minVidaRes = minVidaRes;
    }

    public ArrayList<Recurso> getRecursosTransitorios() {
        return recursosTransitorios;
    }

    public void addRecTransit(Recurso rec) {
        recursosTransitorios.add(rec);
    }

    public ArrayList<String> getEdadRecursosDestinoStr() {
        return edadRecursosDestinoStr;
    }

    public void setEdadRecursosDestinoStr(ArrayList<String> edadRecursosDestinoStr) {
        this.edadRecursosDestinoStr = edadRecursosDestinoStr;
    }

    public ArrayList<Recurso> getRecursosDestino() {
        return recursosDestino;
    }

    public void setRecursosDestino(ArrayList<Recurso> recursosDestino) {
        this.recursosDestino = recursosDestino;
    }

    public void addRecDestino(Recurso rec) {
        recursosDestino.add(rec);
    }

    public ArrayList<Recurso> getRecursosOrigen() {
        return recursosOrigen;
    }

    public void setRecursosTransitorios(ArrayList<Recurso> recursosTransitorios) {
        this.recursosTransitorios = recursosTransitorios;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getPerConstr() {
        return perConstr;
    }

    public int getPerConstr(TiempoAbsoluto ta, Estudio est) throws XcargaDatos {
        int result = 0;
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        int pasoTa = datGen.pasoDeTiempoAbsoluto(ta);
        result = Math.min(datGen.getMaximoPerConst().get(pasoTa), perConstr);        
        return result;
    }

    public void setPerConstr(int perConstr) {
        this.perConstr = perConstr;
    }

    public boolean getTieneDemoraAl() {
        return tieneDemoraAl;
    }

    public void setTieneDemoraAl(boolean tieneDemoraAl) {
        this.tieneDemoraAl = tieneDemoraAl;
    }

    public void setValoresDemAl(ArrayList<Integer> valoresDemAl) {
        this.valoresDemAl = valoresDemAl;
    }



    public VariableNat getVNInvAl() {
        return vNInvAl;
    }

    public void setVNInvAl(VariableNat vNInvAl) {
        this.vNInvAl = vNInvAl;
    }

//    public ArrayList<ArrayList<Double>> getCostosFijos() {
//        return costosFijos;
//    }
//
//    public void setCostosFijos(ArrayList<ArrayList<Double>> costosFijos) {
//        this.costosFijos = costosFijos;
//    }
    public Estudio getEst() {
        return est;
    }

    public void setEst(Estudio est) {
        this.est = est;
    }

    public ArrayList<ArrayList<Double>> getInversiones() {
        return inversiones;
    }

    public void setInversiones(ArrayList<ArrayList<Double>> inversiones) {
        this.inversiones = inversiones;
    }

    public boolean getTieneInvAleat() {
        return tieneInvAleat;
    }

    public void setTieneInvAleat(boolean tieneInvAleat) {
        this.tieneInvAleat = tieneInvAleat;
    }

    public void setValoresInvAl(ArrayList<Double> valoresInvAl) {
        this.valoresInvAl = valoresInvAl;
    }

    public VariableNat getVNDemoraAl() {
        return vNDemoraAl;
    }

    public void setVNDemoraAl(VariableNat vNDemoraAl) {
        this.vNDemoraAl = vNDemoraAl;
    }

    public boolean tieneDemoraAl() {
        return tieneDemoraAl;
    }

    public boolean tieneInvAleat() {
        return tieneInvAleat;
    }

    public ArrayList<Integer> getValoresDemAl() {
        return valoresDemAl;
    }

    public ArrayList<Double> getValoresInvAl() {
        return valoresInvAl;
    }


    public ArrayList<Integer> getMulRecDestino() {
        return mulRecDestino;
    }

    public void setMulRecDestino(ArrayList<Integer> mulRecDestino) {
        this.mulRecDestino = mulRecDestino;
    }

    public ArrayList<Integer> getMulRecOrigen() {
        return mulRecOrigen;
    }

    public void setMulRecOrigen(ArrayList<Integer> mulRecOrigen) {
        this.mulRecOrigen = mulRecOrigen;
    }

    
    public ArrayList<Integer> getMulRecTransit() {
        return mulRecTransit;
    }

    public void setMulRecTransit(ArrayList<Integer> mulRecTransit) {
        this.mulRecTransit = mulRecTransit;
    }

    public void addMulOrigen(Integer mul) {
        mulRecOrigen.add(mul);
    }    

    public void addMulDestino(Integer mul) {
        mulRecDestino.add(mul);
    }        
    
    public void addMulTransit(Integer mul) {
        mulRecTransit.add(mul);
    }        
    
    public void addMinVidaRes(Integer min){
        minVidaRes.add(min);
    }
    
    public void addEdadRecDestino(String edad){
        edadRecursosDestinoStr.add(edad);
    }
    
    public void setPosicionTransformacionInicial(int posicionTransformacionInicial) {
        this.posicionTransformacionInicial = posicionTransformacionInicial;
    }

    public int getPosicionTransformacionInicial() {
        return posicionTransformacionInicial;
    }
    
    

    public ArrayList<String> getValoresDemAlS() {
        return valoresDemAlS;
    }

    public void setValoresDemAlS(ArrayList<String> valoresDemAlS) {
        this.valoresDemAlS = valoresDemAlS;
    }

    public ArrayList<String> getValoresInvAlS() {
        return valoresInvAlS;
    }

    public void setValoresInvAlS(ArrayList<String> valoresInvAlS) {
        this.valoresInvAlS = valoresInvAlS;
    }
    
    public int multDeUnRecOrigen(Recurso rec){
        int indic = recursosOrigen.indexOf(rec);
        return mulRecOrigen.get(indic);
    }
    
    public int multDeUnRecTransit(Recurso rec){
        int indic = recursosTransitorios.indexOf(rec);
        return mulRecTransit.get(indic);
    }
    
    public int multDeUnRecDestino(Recurso rec){
        int indic = recursosDestino.indexOf(rec);
        return mulRecDestino.get(indic);
    }    
    
    
    
    
    public VariableNat getvNDemoraAl() {
		return vNDemoraAl;
	}

	public void setvNDemoraAl(VariableNat vNDemoraAl) {
		this.vNDemoraAl = vNDemoraAl;
	}

	public ArrayList<Double> getEscalamientoInv() {
		return escalamientoInv;
	}

	public void setEscalamientoInv(ArrayList<Double> escalamientoInv) {
		this.escalamientoInv = escalamientoInv;
	}

	public ArrayList<Double> getEscalamientocFijo() {
		return escalamientocFijo;
	}

	public void setEscalamientocFijo(ArrayList<Double> escalamientocFijo) {
		this.escalamientocFijo = escalamientocFijo;
	}

	public VariableNat getvNInvAl() {
		return vNInvAl;
	}

	public void setvNInvAl(VariableNat vNInvAl) {
		this.vNInvAl = vNInvAl;
	}

	/**
     * Devuelve la posicion en el cÃ³digo cBloques 
     * del Estudio, de una Transformacion que inicia su construcciÃ³n en el TiempoAbsoluto ta.
     * Tiene en cuenta el mÃ¡ximo perÃ­odo de construcciÃ³n del ta propio del Estudio.
     */ 
    public int posicionCodigoTransInicioConst(Estudio est, TiempoAbsoluto ta) throws XcargaDatos{
        assert(clase.equalsIgnoreCase(ExistEleg.ELEG)): "Se pidio posiciÃ³n de inicio construcciÃ³n de Transformacion"                
                + "de la TransforBase no ELEG de nombre: " + nombre;        
        int perTa = est.getDatosGenerales().maxPerConstDeTA(ta);
        int ip = posicionTransformacionInicial;
        /**
         * Si el perÃ­odo mÃ¡ximo del estudio acota el plazo de construcciÃ³n en ta,
         * no se toma el Recurso con plazo mayor del RecursoBase
         */
        if(perTa<perConstr) ip = ip + perConstr - perTa;
        return ip;             
        
    }    
    

    public int compareTo(Object o) {
        RecursoBase orb = (RecursoBase) o;
        if (clase.equalsIgnoreCase(ExistEleg.EXIST) & !orb.getClase().equalsIgnoreCase(ExistEleg.EXIST)) {
            return -1;
        }
        if (!clase.equalsIgnoreCase(ExistEleg.EXIST) & orb.getClase().equalsIgnoreCase(ExistEleg.EXIST)) {
            return 1;
        }
        if (clase.equalsIgnoreCase(ExistEleg.ELEG) & orb.getClase().equalsIgnoreCase(ExistEleg.TRANS)) {
            return -1;
        }
        if (clase.equalsIgnoreCase(ExistEleg.TRANS) & orb.getClase().equalsIgnoreCase(ExistEleg.ELEG)) {
            return 1;
        }
        // la clase de ambos objetos es igual y se comparan los nombres
        return nombre.compareToIgnoreCase(orb.getNombre());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TransforBase other = (TransforBase) obj;
        if ((this.nombre == null) ? (other.nombre != null) : !this.nombre.equals(other.nombre)) {
            return false;
        }
        if (!this.clase.equalsIgnoreCase(other.clase)) {
            return false;
        }
        if (this.recursosOrigen != other.recursosOrigen && (this.recursosOrigen == null || !this.recursosOrigen.equals(other.recursosOrigen))) {
            return false;
        }
        if (this.minVidaRes != other.minVidaRes && (this.minVidaRes == null || !this.minVidaRes.equals(other.minVidaRes))) {
            return false;
        }
        if (this.recursosTransitorios != other.recursosTransitorios && (this.recursosTransitorios == null || !this.recursosTransitorios.equals(other.recursosTransitorios))) {
            return false;
        }
        if (this.recursosDestino != other.recursosDestino && (this.recursosDestino == null || !this.recursosDestino.equals(other.recursosDestino))) {
            return false;
        }
 
        if (this.mulRecOrigen != other.mulRecOrigen && (this.mulRecOrigen == null || !this.mulRecOrigen.equals(other.mulRecOrigen))) {
            return false;
        }

        if (this.mulRecDestino != other.mulRecDestino && (this.mulRecDestino == null || !this.mulRecDestino.equals(other.mulRecDestino))) {
            return false;
        }
        if (this.mulRecTransit != other.mulRecTransit && (this.mulRecTransit == null || !this.mulRecTransit.equals(other.mulRecTransit))) {
            return false;
        }
        if (this.edadRecursosDestinoStr != other.edadRecursosDestinoStr && (this.edadRecursosDestinoStr == null || !this.edadRecursosDestinoStr.equals(other.edadRecursosDestinoStr))) {
            return false;
        }
        if ((this.tipo == null) ? (other.tipo != null) : !this.tipo.equals(other.tipo)) {
            return false;
        }
        if (this.perConstr != other.perConstr) {
            return false;
        }
        if (this.tieneDemoraAl != other.tieneDemoraAl) {
            return false;
        }
        if (this.vNDemoraAl != other.vNDemoraAl && (this.vNDemoraAl == null || !this.vNDemoraAl.equals(other.vNDemoraAl))) {
            return false;
        }
        if (this.valoresDemAl != other.valoresDemAl && (this.valoresDemAl == null || !this.valoresDemAl.equals(other.valoresDemAl))) {
            return false;
        }
        if (this.inversiones != other.inversiones && (this.inversiones == null || !this.inversiones.equals(other.inversiones))) {
            return false;
        }
        if (this.tieneInvAleat != other.tieneInvAleat) {
            return false;
        }
        if (this.valoresInvAl != other.valoresInvAl && (this.valoresInvAl == null || !this.valoresInvAl.equals(other.valoresInvAl))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.nombre != null ? this.nombre.hashCode() : 0);
        hash = 97 * hash + this.clase.hashCode();
//          hash = 97 * hash + ExistEleg.hashSustituto(this.clase);
        hash = 97 * hash + (this.recursosOrigen != null ? this.recursosOrigen.hashCode() : 0);
        hash = 97 * hash + (this.minVidaRes != null ? this.minVidaRes.hashCode() : 0);
        hash = 97 * hash + (this.recursosTransitorios != null ? this.recursosTransitorios.hashCode() : 0);
        hash = 97 * hash + (this.recursosDestino != null ? this.recursosDestino.hashCode() : 0);
        hash = 97 * hash + (this.mulRecOrigen != null ? this.mulRecOrigen.hashCode() : 0);
        hash = 97 * hash + (this.mulRecDestino != null ? this.mulRecDestino.hashCode() : 0);
        hash = 97 * hash + (this.mulRecTransit != null ? this.mulRecTransit.hashCode() : 0);
        hash = 97 * hash + (this.edadRecursosDestinoStr != null ? this.edadRecursosDestinoStr.hashCode() : 0);
        hash = 97 * hash + (this.tipo != null ? this.tipo.hashCode() : 0);
        hash = 97 * hash + this.perConstr;
        hash = 97 * hash + (this.tieneDemoraAl ? 1 : 0);
        hash = 97 * hash + (this.vNDemoraAl != null ? this.vNDemoraAl.hashCode() : 0);
        hash = 97 * hash + (this.valoresDemAl != null ? this.valoresDemAl.hashCode() : 0);
        hash = 97 * hash + (this.inversiones != null ? this.inversiones.hashCode() : 0);
        hash = 97 * hash + (this.tieneInvAleat ? 1 : 0);
        hash = 97 * hash + (this.valoresInvAl != null ? this.valoresInvAl.hashCode() : 0);
//          try {
//               DirectoriosYArchivos.agregaTexto("X:/EDF/2011/PruebasMingo/PruebaJava4/corridas", nombre + "_" + hash + "\r\n");
//          } catch (XcargaDatos ex) {
//               Logger.getLogger(TransforBase.class.getName()).log(Level.SEVERE, null, ex);
//          }
        return hash;
    }

    @Override
    public String toString() {
        String texto = "";
        int i;
        texto += "TRANSFORMACION BASE: " + nombre + "\n";
        for (i = 0; i < recursosOrigen.size(); i++) {
            texto += "Recursos origen: " + recursosOrigen.get(i).toString() + "\n";
        }
        for (i = 0; i < minVidaRes.size(); i++) {
            texto += "MÃ­nima vida residual: " + minVidaRes.get(i).toString() + "\n";
        }
        for (i = 0; i < recursosTransitorios.size(); i++) {
            texto += "Recursos transitorios: " + recursosTransitorios.get(i).toString() + "\n";
        }
        for (i = 0; i < recursosDestino.size(); i++) {
            texto += "Recursos destino: " + recursosDestino.get(i).toString() + "\n";
        }

        texto += "Tipo: " + tipo + "\n";
        texto += "PerÃ­odo de construcciÃ³n: " + perConstr + "\n";
        texto += "Â¿Tiene demora aleatoria? " + tieneDemoraAl + "\n";
        texto += "Valores de la demora aleatoria: ";
        for (i = 0; i < valoresDemAlS.size(); i++) {
            texto += " \t" + valoresDemAlS.get(i);
        }
        texto += "\n";

        for (i = 0; i < inversiones.size(); i++) {
            texto += "InversiÃ³n: " + inversiones.get(i).toString() + "\n";
        }
//        for(i = 0; i < costosFijos.size(); i++){
//            texto += "Costo fijo: " + costosFijos.get(i).toString() + "\n";
//        }
        texto += "Â¿La inversiÃ³n es aleatoria? " + tieneInvAleat + "\n";

        texto += "Valores de la variable aleatoria de inversiÃ³n: ";
        for (i = 0; i < valoresInvAlS.size(); i++) {
            texto += " \t" + valoresInvAlS.get(i);
        }
        texto += "\n";
        return texto;
    }

    public String toStringCorto() {
        String texto = "";
        int i;
        texto += "TRANSFORMACION BASE: " + nombre + "\n";
        for (i = 0; i < recursosOrigen.size(); i++) {
            texto += "Recursos origen: " + recursosOrigen.get(i).getRecBase().getNombre() + "\n";
        }
        for (i = 0; i < minVidaRes.size(); i++) {
            texto += "MÃ­nima vida residual: " + minVidaRes.get(i).toString() + "\n";
        }
        for (i = 0; i < recursosTransitorios.size(); i++) {
            texto += "Recursos transitorios: " + recursosTransitorios.get(i).getRecBase().getNombre() + "\n";
        }
        for (i = 0; i < recursosDestino.size(); i++) {
            texto += "Recursos destino: " + recursosDestino.get(i).getRecBase().getNombre() + "\n";
        }

        texto += "Tipo: " + tipo + "\n";
        texto += "PerÃ­odo de construcciÃ³n: " + perConstr + "\n";
        texto += "Â¿Tiene demora aleatoria? " + tieneDemoraAl + "\n";
        texto += "Valores de la demora aleatoria: ";
        for (i = 0; i < valoresDemAl.size(); i++) {
            texto += " \t" + valoresDemAl.get(i);
        }
        texto += "\n";

        for (i = 0; i < inversiones.size(); i++) {
            texto += "InversiÃ³n: " + inversiones.get(i).toString() + "\n";
        }
//        for(i = 0; i < costosFijos.size(); i++){
//            texto += "Costo fijo: " + costosFijos.get(i).toString() + "\n";
//        }
        texto += "Â¿La inversiÃ³n es aleatoria? " + tieneInvAleat + "\n";

        texto += "Valores de la variable aleatoria de inversiÃ³n: ";
        for (i = 0; i < valoresInvAl.size(); i++) {
            texto += " \t" + valoresInvAl.get(i);
        }
        texto += "\n";
        return texto;

    }
}
