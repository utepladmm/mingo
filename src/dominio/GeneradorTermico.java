/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class GeneradorTermico extends Generador implements Serializable{

//    private int ntran; número de tranches se pasó a RecursoBase y es el número de módulos
    private String mintec; // S o P (semanal o por poste)
    private double potenciaMintec; // potencia mínimo técnico de un poste
    private double dpotan;  // reducción anual de potencia nominal no se usa
    private double rendPotenciaNom; // rendimiento a potencia nominal
    private double rendPotenciaMin; // idem a potencia mínimo técnico
    private double drenan; // derateo anual de rendimiento no se usa ahora
    private double disponibilidad; //disponibilidad por tranche
	private ArrayList<Combustible> combustibles; // lista ordenada de combustibles que puede usar
	private double dPotCS; // derateo de potencia por usar combustible secundario
	private double dRenCS; // derateo de rendimiento por usar combustible secundario
	private double cOYMV; // costo variable de OyM en US$/MWh
	private double cOYMF; // costo fijo de OyM en US$/MWh
	
	// Cantidad de m�dulos en mantenimiento por semana del a�o, tiene 52 valores
	private ArrayList<ArrayList<String>> manten;
	public static final int CANTSEM = 52;


	public GeneradorTermico() {
        combustibles = new ArrayList<Combustible>();
        
        manten = new ArrayList<ArrayList<String>>();
	}

	public double getDisponibilidad() {
		return disponibilidad;
	}

	public void setDisponibilidad(double disponibilidad) {
		this.disponibilidad = disponibilidad;
	}

	public double getDpotan() {
		return dpotan;
	}

	public void setDpotan(double dpotan) {
		this.dpotan = dpotan;
	}

	public double getDrenan() {
		return drenan;
	}

	public void setDrenan(double drenan) {
		this.drenan = drenan;
	}

	public String getMintec() {
		return mintec;
	}

	public void setMintec(String mintec) {
		this.mintec = mintec;
	}

	public double getPotenciaMintec() {
		return potenciaMintec;
	}

	public void setPotenciaMintec(double potenciaMintec) {
		this.potenciaMintec = potenciaMintec;
	}

	public double getRendPotenciaMin() {
		return rendPotenciaMin;
	}

	public void setRendPotenciaMin(double rendPotenciaMin) {
		this.rendPotenciaMin = rendPotenciaMin;
	}

	public double getRendPotenciaNom() {
		return rendPotenciaNom;
	}

	public void setRendPotenciaNom(double rendPotenciaNom) {
		this.rendPotenciaNom = rendPotenciaNom;
	}

	public double getCOYMF() {
		return cOYMF;
	}

	public void setCOYMF(double cOYMF) {
		this.cOYMF = cOYMF;
	}

	public double getCOYMV() {
		return cOYMV;
	}

	public void setCOYMV(double cOYMV) {
		this.cOYMV = cOYMV;
	}

	public ArrayList<Combustible> getCombustibles() {
		return combustibles;
	}

	public void setCombustibles(ArrayList<Combustible> combustibles) {
		this.combustibles = combustibles;
	}



	public double getDPotCS() {
		return dPotCS;
	}

	public void setDPotCS(double dPotCS) {
		this.dPotCS = dPotCS;
	}

	public double getDRenCS() {
		return dRenCS;
	}

	public void setDRenCS(double dRenCS) {
		this.dRenCS = dRenCS;
	}

	
	


	public ArrayList<ArrayList<String>> getManten() {
		return manten;
	}

	public void setManten(ArrayList<ArrayList<String>> manten) {
		this.manten = manten;
	}

	public static int getCantsem() {
		return CANTSEM;
	}

	@Override
	public String toString() {

		String texto ="";
		texto += super.toString() + "\n";
		texto += "potencia mínimo técnico de un poste: " + potenciaMintec+ "\n";
		texto += "reducción anual de potencia nominal: " + dpotan+ "\n";
		texto += "rendimiento a potencia nominal: " + rendPotenciaNom+ "\n";
		texto += "rendimiento a potencia mínimo tecnico: " + rendPotenciaMin+ "\n";
		texto += "derateo anual de rendimiento: " + drenan+ "\n";
		texto += "combustibles: ";
		for (Combustible comb: combustibles){
			texto += comb.getNombre() + "  ";
		}
		texto += "\n";
		texto += "derateo de potencia por comb. secundario: " + dPotCS+ "\n";
		texto += "derateo de rendimiento por usar combustible secundario: " + dRenCS + "\n";
		texto += "costo variable de OyM en US$/MWh: " + cOYMV + "\n";
		texto += "costo fijo de OyM en US$/MWh: " + cOYMF + "\n";
        return texto;
	}

//	public static void main(String[] args){
//		RecursoBase rb = new RecursoBase();
//		GeneradorTermico genter = new GeneradorTermico();
//		genter.disponibilidad = 0.0;
//		genter.dpotan = 0.0;
//		genter.drenan = 0.0;
//		genter.mintec = "S";
//		genter.potenciaMintec = 10.0;
//		genter.rendPotenciaMin = 0.3;
//		genter.rendPotenciaNom = 0.4;
//		genter.drenan = 0.0;
//		ArrayList<String> auxS = new ArrayList<String>();
//		auxS.add("comb1");
//		auxS.add("comb2");
////		genter.setCombustibles(auxS);
//		genter.dPotCS = 0.0;
//		genter.dRenCS = 0.0;
//		genter.cOYMF = 1.0;
//		genter.cOYMV = 2.0;
//		System.out.print(genter.toString());
//		}

}
