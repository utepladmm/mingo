/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package dominio;

import java.io.Serializable;
import java.lang.Integer;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class ImportEstac extends Importacion implements Serializable{
	private String pais;
	private int nest;
	private ArrayList<Integer> semFinEst;
	private ArrayList<Double> preEst;
	private ArrayList<Double> facEst;
	private ArrayList<ArrayList<Double>> potEst;
	/**
     * Son las potencias por poste y estación que corresponden a la potencia potNominal
     * del RecursoBase padre.
     * Estas potencias se incrementan proporcionalmente con las variaciones de la potencia
     * nominal del RecursoBase padre.
	 * El primer índice es el poste (fila) y el segundo la estación (columna)
	 */
	private ArrayList<Double> dispEst;

	public ImportEstac(String pais, int nest, ArrayList<Integer> semFinEst, ArrayList<Double> preEst, ArrayList<ArrayList<Double>> potEst) {
		this.pais = pais;
		this.nest = nest;
		this.semFinEst = semFinEst;
		this.preEst = preEst;
		this.potEst = potEst;
	}

	public ImportEstac() {
        semFinEst = new ArrayList<Integer>();
        preEst = new ArrayList<Double>();
        facEst = new ArrayList<Double>();
        potEst = new ArrayList<ArrayList<Double>>();
	}



	public ArrayList<Double> getDispEst() {
		return dispEst;
	}

	public void setDispEst(ArrayList<Double> dispEst) {
		this.dispEst = dispEst;
	}

	public int getNest() {
		return nest;
	}

	public void setNest(int nest) {
		this.nest = nest;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public ArrayList<ArrayList<Double>> getPotEst() {
		return potEst;
	}

	public void setPotEst(ArrayList<ArrayList<Double>> potEst) {
		this.potEst = potEst;
	}

	public ArrayList<Double> getPreEst() {
		return preEst;
	}

	public void setPreEst(ArrayList<Double> preEst) {
		this.preEst = preEst;
	}

    public ArrayList<Double> getFacEst() {
        return facEst;
    }

    public void setFacEst(ArrayList<Double> facEst) {
        this.facEst = facEst;
    }

	public ArrayList<Integer> getSemFinEst() {
		return semFinEst;
	}

	public void setSemFinEst(ArrayList<Integer> semFinEst) {
		this.semFinEst = semFinEst;
	}




	@Override
	public String toString() {
		String texto ="";
		texto += super.toString() + "\n";
		texto += "pais: " + pais + "\n";
		texto += "cantidad de estaciones del año: " + nest + "\n";
		texto += "semana de finalización de cada estación" + "\n";
		for (int i=0; i<semFinEst.size(); i++){
			texto += semFinEst.get(i).toString() + "  ";
		}
		texto += "\n";
		texto += "precio por estación - constante" + "\n";
		for (int i=0; i<preEst.size(); i++){
			texto += preEst.get(i).toString() + "  ";
		}
		texto += "precio por estación - factor por el precio spot del país" + "\n";
		for (int i=0; i<facEst.size(); i++){
			texto += facEst.get(i).toString() + "  ";
		}
		texto += "\n";
		texto += "potencia por estación (columna) y poste (fila)" + "\n";
		for (int i=0; i<potEst.size(); i++){
			for (int j=0; j<potEst.get(i).size(); j++){
				texto += potEst.get(i).get(j).toString() + "  ";
			}
			texto += "\n";
		}

        return texto;
	}



}



