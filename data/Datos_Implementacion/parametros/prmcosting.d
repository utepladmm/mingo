 $PRMCTING
   TOPSPOT=8000.0
   CANGASES=3
   GASES='GNL','GNLPTI','GNLMOT'
   NESTIM=1
   LIMEST=0.0
   CONST=0.0
   CCV=0.0
   CCV2=0.0
   CENH=0.0
   CENH2=0.0
   CCVENH=0.0
   PRCONST=0
   PRCCV=0.0
   PRCCV2=0.0
   PRLIM=0.0
   PRLIMCONST=0.0
   PRLIMCCV=0.23
 $END

Descripci�n de los nuevos par�metros de prmcosting.txt

   TOPSPOT=8000.0   // Tope del spot
   CANGASES=3      // Cantidad de gases que se incluyen en la correcci�n
   GASES='GNL','GNLPTI','GNLMOT'   // Nombre de los gases en combus que se incluyen

  // VALORES CON TANQUE DE 240
 // los primeros con los coeficientes de la correcci�n cr�nica
   NESTIM=3          // Cantidad de regresiones estimadas
   LIMEST=0.65,0.9   // L�mites del COEFVAR para las estimaciones
   CONST=0.6380985, -0.4795998, 49.1935449	// Constante de la correcci�n cr�nica
   CCV=1.165267197, 3.173828402, -45.59892323		// factor del coeficiente de variaci�n de costo de gas ca�o
   CCV2=0.0, 0.0, 0.0	// �dem del cuadrado del coef de variaci�n
   CENH=-0.000417943, -0.000808625, -0.001521718	// factor de la energ�a hidr�ulica de la cr�nica
   CENH2=4.67766E-08, 9.74575E-08, 1.79731E-0.7	// �dem del cuadrado de la energ�a hidr�ulica
   CCVENH=0.0, 0.0, 0.0		// �dem del producto de coef de variaci�n y energ�a hidr�ulica
// ahora los coeficientes de la correcci�n sobre el promedio de los costos en las cr�nicas
   PRCONST=0.046382137	// Constante de la correcci�n al costo promedio
   PRCCV=0.84922473	// factor del coeficiente de variaci�n de costo de gas ca�o
   PRCCV2=0.0		// �dem del cuadrado del coef de variaci�n
   PRLIM=0.35		// l�mite sup. de coef de variaci�n por debajo del cual se aplica extrapolaci�n
   PRLIMCONST=0.0	// constante en la extrapolaci�n
   PRLIMCCV=1.0		// factor proporcional del coef de variaci�n en la extrapolaci�n.



   Las f�rmulas a aplicar se dedujeron y se describen en:
G:\PLA\Pla_datos\Archivos\x\2015\Planificaci�n del abastecimiento\Estudios\GEN\Sobrecostos de GAS
Hay una planilla que explica la f�rmula: V9-Delta costos y energ�as_sinara_150226.xlsx

