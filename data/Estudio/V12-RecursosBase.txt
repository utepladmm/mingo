////////////////////////////////////////////////////////////
//
//
// Este es un archivo de ejemplo de recursos base
//
// Todos los datos literales le�dos por el modelo se almacenan en min�scula. Se recomienda no obstante usar camelCase
//
// Se recomienda como buena pr�ctica que los campos ilustrativos del tipo de dato que aparecen al principio de cada l�nea
// sean en may�scula separando palabras con sub gui�n
// 
// 
////////////////////////////////////////////////////////////



&LISTA_RECURSOS_BASE      // solo se consideran en el estudio los recursosBase de esta lista                      
CTR	GPTI GMO80 GAPR_C GTG120 SCC_180 SCC180 SCC500 GTG170 SOLdeci SOL200 EOLOdeci  EOLO100  BmNoDesp BmDesp UPM1 MDP UPM2 IBRIV IBMELO IARGO  EXCED  TERRA	BAYGORRIA PALMAR SALTO
$END

&FIN


///////////////////////////////////
// 
//          TERMICOS
//
///////////////////////////////////



&RECURSO_BASE
NOMBRE			QUINTA
CLASE			EXIST
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		cicloVapor
TIENE_EDAD		no	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2009
ULTIMO_TIEMPO_ABSOLUTO	2020
CANT_MODULOS	 	1		
POTENCIA_NOMINAL 	77.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	53.9	
PRODUCTO		potenciaMaxima 	53.9	
INVERSION 		MUSD	0.0	     
COSTO_FIJO		MUSD	0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			QUINTA		
MINTEC			S			
POTENCIA_MINTEC		20.0		
DPOTAN		0.00		
REND_POTENCIA_NOM	0.297
REND_POTENCIA_MIN	0.297
DRENAN			0.00  
DISPONIBILIDAD		0.7
COMBUSTIBLES		FUEL_OIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			13.5	
COYMF			0.0	
&FIN




&RECURSO_BASE
NOMBRE			SEXTA
CLASE			EXIST		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		cicloVapor
TIENE_EDAD		no	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2009
ULTIMO_TIEMPO_ABSOLUTO	2020
CANT_MODULOS	 	1		
POTENCIA_NOMINAL 	115.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	80.5	
PRODUCTO		potenciaMaxima 	80.5	
INVERSION 		MUSD		0.0     
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			SEXTA		
MINTEC			S			
POTENCIA_MINTEC		30.0		
DPOTAN		0.00		
REND_POTENCIA_NOM	0.305
REND_POTENCIA_MIN	0.305
DRENAN			0.00  
DISPONIBILIDAD		0.7
COMBUSTIBLES		FUEL_OIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			12.1	
COYMF			0.0	
&FIN




&RECURSO_BASE
NOMBRE			SALAB
CLASE			EXIST		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		cicloVapor
TIENE_EDAD		no	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2009
ULTIMO_TIEMPO_ABSOLUTO	2015
CANT_MODULOS	 	1		
POTENCIA_NOMINAL 	50.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	22.5	
PRODUCTO		potenciaMaxima 	22.5
INVERSION 		MUSD		0.0     
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			SALAB		
MINTEC			S			
POTENCIA_MINTEC		20.0		
DPOTAN		0.00		
REND_POTENCIA_NOM	0.246
REND_POTENCIA_MIN	0.246
DRENAN			0.00  
DISPONIBILIDAD		0.45
COMBUSTIBLES		FUEL_OIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			10.7	
COYMF			0.0	
&FIN



&RECURSO_BASE
NOMBRE			CTR
CLASE			EXIST		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		turbinaAGas
TIENE_EDAD		no	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2009
ULTIMO_TIEMPO_ABSOLUTO	2024
CANT_MODULOS	 	2		
POTENCIA_NOMINAL 	100.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	70.0	
PRODUCTO		potenciaMaxima 	70.0	
INVERSION 		MUSD		0.0     
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			CTR		
MINTEC			P			
POTENCIA_MINTEC		20.0		
DPOTAN		0.00		
REND_POTENCIA_NOM	0.296
REND_POTENCIA_MIN	0.296
DRENAN			0.00  
DISPONIBILIDAD		0.7
COMBUSTIBLES		GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			4.2	
COYMF			0.0	
&FIN




&RECURSO_BASE
NOMBRE			GPTI
CLASE			EXIST		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		turbinaAGas
TIENE_EDAD		no	
DEPENDE_TIEMPO		no    
ULTIMO_TIEMPO_ABSOLUTO	2040   		
TIEMPO_ABSOLUTO_BASE	2009
CANT_MODULOS	 	6		
POTENCIA_NOMINAL 	50.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	37.5
PRODUCTO		potenciaMaxima 	37.5
INVERSION 		MUSD		0.0     
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			GPTI		
MINTEC			P			
POTENCIA_MINTEC		5.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.375
REND_POTENCIA_MIN	0.375
DRENAN			0.00  
DISPONIBILIDAD		0.75
COMBUSTIBLES		GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			8.7	
COYMF			0.0	
&FIN




&RECURSO_BASE
NOMBRE			GMO80
CLASE			EXIST		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		motor
TIENE_EDAD		no	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2009
ULTIMO_TIEMPO_ABSOLUTO	2030
CANT_MODULOS	 	8		
POTENCIA_NOMINAL 	10.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	7.5	
PRODUCTO		potenciaMaxima 	7.5	
INVERSION 		MUSD		0.0     
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			GMO80		
MINTEC			P			
POTENCIA_MINTEC		0.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.393
REND_POTENCIA_MIN	0.393
DRENAN			0.00  
DISPONIBILIDAD		0.75
COMBUSTIBLES		F_OIL_C		GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			12.2	
COYMF			0.0	
&FIN



&RECURSO_BASE
NOMBRE			SCC500
CLASE			EXIST		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		CC
TIENE_EDAD		no	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2019
ULTIMO_TIEMPO_ABSOLUTO	2045
CANT_MODULOS	 	2		
POTENCIA_NOMINAL 	266.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	239.4	
PRODUCTO		potenciaMaxima 	239.4	
INVERSION 		MUSD		0.0     
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			SCC500		
MINTEC			S			
POTENCIA_MINTEC		40.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.525
REND_POTENCIA_MIN	0.525
DRENAN			0.0 
DISPONIBILIDAD		0.9
COMBUSTIBLES		GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			5.0	
COYMF			0.0	
&FIN



&RECURSO_BASE
NOMBRE			GTG170
CLASE			EXIST		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		turbinaAGas
TIENE_EDAD		no	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2018  
ULTIMO_TIEMPO_ABSOLUTO	2018
CANT_MODULOS	 	2		
POTENCIA_NOMINAL 	177.3					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	159.6	
PRODUCTO		potenciaMaxima 	159.6	
INVERSION 		MUSD		0.0     
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			GTG170		
MINTEC			P			
POTENCIA_MINTEC		20.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.35
REND_POTENCIA_MIN	0.35
DRENAN			0.0 
DISPONIBILIDAD		0.9
COMBUSTIBLES		GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			3.0	
COYMF			0.0	
&FIN



&RECURSO_BASE
NOMBRE			GLIC
CLASE			EXIST		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		turbinaAGas
TIENE_EDAD		no	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2013
ULTIMO_TIEMPO_ABSOLUTO	2040
CANT_MODULOS	 	3		
POTENCIA_NOMINAL 	50.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	45.0	
PRODUCTO		potenciaMaxima 	45.0	
INVERSION 		MUSD		0.0     
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			GLIC	
MINTEC			P			
POTENCIA_MINTEC		0.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.354
REND_POTENCIA_MIN	0.354
DRENAN			0.0 
DISPONIBILIDAD		0.90
COMBUSTIBLES		GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			10.0	
COYMF			0.0	
&FIN


&RECURSO_BASE
NOMBRE			GAPR_B
CLASE			EXIST		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		turbinaAGas
TIENE_EDAD		no	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2013  
ULTIMO_TIEMPO_ABSOLUTO	2016 
CANT_MODULOS	 	6		
POTENCIA_NOMINAL 	24.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	21.6	
PRODUCTO		potenciaMaxima 	21.6	
INVERSION 		MUSD		0.0     
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			GAPR_B	
MINTEC			P			
POTENCIA_MINTEC		0.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.345
REND_POTENCIA_MIN	0.345
DRENAN			0.0 
DISPONIBILIDAD		0.9
COMBUSTIBLES		GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			10.0	
COYMF			0.0	
&FIN

&RECURSO_BASE
NOMBRE			GAPR_C
CLASE			EXIST		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		turbinaAGas
TIENE_EDAD		no	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2013  
ULTIMO_TIEMPO_ABSOLUTO	2050 
CANT_MODULOS	 	2		
POTENCIA_NOMINAL 	24.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	21.6	
PRODUCTO		potenciaMaxima 	21.6	
INVERSION 		MUSD		0.0     
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			GAPR_C	
MINTEC			P			
POTENCIA_MINTEC		0.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.345
REND_POTENCIA_MIN	0.345
DRENAN			0.0 
DISPONIBILIDAD		0.9
COMBUSTIBLES		GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			10.0	
COYMF			0.0	
&FIN






///////////// COMIENZAN RECURSOS BASE T�RMICOS ELEGIBLES


&RECURSO_BASE
NOMBRE			SCC180
CLASE			ELEG		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		turbinaAGas
TIENE_EDAD		no
VIDA_UTIL		20	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2016
ULTIMO_TIEMPO_ABSOLUTO	2049
CANT_MODULOS	 	1		
POTENCIA_NOMINAL 	180.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	162.0
PRODUCTO		potenciaMaxima 	162.0	
INVERSION 		MUSD		216.0     //  180 MW x 1200 USD/kW => 216 MUSD de inversi�n
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			SCC180		
MINTEC			S			
POTENCIA_MINTEC		30.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.525
REND_POTENCIA_MIN	0.525
DRENAN			0.0 
DISPONIBILIDAD		0.9
COMBUSTIBLES		GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			5.0	
COYMF			0.0	
&FIN


// Es un ciclo combinado id�ntico a SCC180, le cambiamos el nombre para distinguir que proviene de una tranformaci�n base (sino se generan mal los genter)
&RECURSO_BASE
NOMBRE			SCC_180
CLASE			ELEG		
EQUIV_EN_SIMUL_A SCC180
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		turbinaAGas
TIENE_EDAD		no
VIDA_UTIL		20	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2015
ULTIMO_TIEMPO_ABSOLUTO	2049
CANT_MODULOS	 	1		
POTENCIA_NOMINAL 	180.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	162.0
PRODUCTO		potenciaMaxima 	162.0	
INVERSION 		MUSD		216  
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			SCC_180		
MINTEC			S			
POTENCIA_MINTEC		30.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.525
REND_POTENCIA_MIN	0.525
DRENAN			0.0 
DISPONIBILIDAD		0.9
COMBUSTIBLES		GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			5.0	
COYMF			0.0	
&FIN


&RECURSO_BASE
NOMBRE			GTG100
CLASE			ELEG		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		turbinaAGas
TIENE_EDAD		no	
VIDA_UTIL		20	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2015
ULTIMO_TIEMPO_ABSOLUTO	2040
CANT_MODULOS	 	1		
POTENCIA_NOMINAL 	100.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	90.0	
PRODUCTO		potenciaMaxima 	90.0	
INVERSION 		MUSD		45.0     
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			GTG100		
MINTEC			P			
POTENCIA_MINTEC		20.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.35
REND_POTENCIA_MIN	0.35
DRENAN			0.0 
DISPONIBILIDAD		0.9
COMBUSTIBLES		GNL	GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			3.0	
COYMF			0.0	
&FIN


&RECURSO_BASE
NOMBRE			GTG120
CLASE			ELEG		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		turbinaAGas
TIENE_EDAD		no	
VIDA_UTIL		20	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2014
ULTIMO_TIEMPO_ABSOLUTO	2040
CANT_MODULOS	 	1		
POTENCIA_NOMINAL 	120.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	108.0	
PRODUCTO		potenciaMaxima 	108.0	
INVERSION 		MUSD		54.0 54 20 10     //    120 MW x 450 USD/kW => 54 MUSD
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			GTG120		
MINTEC			P			
POTENCIA_MINTEC		25.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.35
REND_POTENCIA_MIN	0.35
DRENAN			0.0 
DISPONIBILIDAD		0.9
COMBUSTIBLES		GASOIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			3.0	
COYMF			0.0	
&FIN





&RECURSO_BASE
NOMBRE			SCAR300
CLASE			ELEG		
PUNTO			unico
TIPO			generador
SUBTIPO1		termico
SUBTIPO2		turbinaAGas
TIENE_EDAD		no
VIDA_UTIL		20	
DEPENDE_TIEMPO		no       		
TIEMPO_ABSOLUTO_BASE	2016
ULTIMO_TIEMPO_ABSOLUTO	2035
CANT_MODULOS	 	1		
POTENCIA_NOMINAL 	300.0					
PERIODO_CONST		1               
PRODUCTO		potenciaFirme  	255.0
PRODUCTO		potenciaMaxima 	255.0	
INVERSION 		MUSD		200.0     200.0   200.0
COSTO_FIJO		MUSD		0.0		    
INVERSION_ALEAT 	no 	
&FIN
&DATOS_ESPECIFICOS
NOMBRE			SCAR300		
MINTEC			S			
POTENCIA_MINTEC		200.0		
DPOTAN			0.00		
REND_POTENCIA_NOM	0.38
REND_POTENCIA_MIN	0.38
DRENAN			0.0 
DISPONIBILIDAD		0.85
COMBUSTIBLES		CARBON    FUEL_OIL
DPOTCS			0.0	
DRENCS			0.0	
COYMV			10.0	
COYMF			0.0	
&FIN





///////////////////////////////////
// 
//          HIDRAULICOS
//	En la implementaci�n EDF s�lo se consideran estos datos
//	en los balances de productos
//
///////////////////////////////////




&RECURSO_BASE
NOMBRE		TERRA
//POTENCIA_NOMINAL 152.0
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	hidraulico
SUBTIPO2	RioNegro
TIENE_EDAD	no
DEPENDE_TIEMPO	no
CANT_MODULOS	1
POTENCIA_NOMINAL 152.0         			     	
VIDA_UTIL       10			// como no tiene edad no se usa
PRODUCTO	potenciaFirme  		14.0  // Potencia Firme - Informe Garantia de suministro (ADME) Mes de menor valor total hidro 
PRODUCTO	potenciaMaxima 		136.8 // 90% de la potencia nominal
COSTO_FIJO	MUSD	0.0		
&FIN
&DATOS_ESPECIFICOS
NOMBRE		TERRA
//DATOS		Bonete			// nombre del directorio de datos asociado a la central
&FIN




&RECURSO_BASE
NOMBRE		BAYGORRIA
//POTENCIA_NOMINAL 108.0
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	hidraulico
SUBTIPO2	RioNegro
TIENE_EDAD	no
DEPENDE_TIEMPO	no
CANT_MODULOS	1
POTENCIA_NOMINAL 108.0         			     	
VIDA_UTIL       10			// como no tiene edad no se usa
PRODUCTO	potenciaFirme  		13.0   // Potencia Firme - Informe Garantia de suministro (ADME) Mes de menor valor total hidro 
PRODUCTO	potenciaMaxima 		97.2   // 90% de la potencia nominal
COSTO_FIJO	MUSD	0.0	
&FIN
&DATOS_ESPECIFICOS
NOMBRE		BAYGORRIA
//DATOS		Baygorria
&FIN



&RECURSO_BASE
NOMBRE		PALMAR
//POTENCIA_NOMINAL 333.0
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	hidraulico
SUBTIPO2	RioNegro
TIENE_EDAD	no
DEPENDE_TIEMPO	no 
CANT_MODULOS	1
POTENCIA_NOMINAL 333.0          			     	
VIDA_UTIL       10			// como no tiene edad no se usa
PRODUCTO	potenciaFirme  		43.0   // Potencia Firme - Informe Garantia de suministro (ADME) Mes de menor valor total hidro 
PRODUCTO	potenciaMaxima 		299.7  // 90% de la potencia nominal 
COSTO_FIJO	MUSD	0.0
&FIN
&DATOS_ESPECIFICOS
NOMBRE		PALMAR
//DATOS		Palmar
&FIN


&RECURSO_BASE
NOMBRE		SALTO
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	hidraulico
SUBTIPO2	R�oUruguay
TIENE_EDAD	no
DEPENDE_TIEMPO	no 
CANT_MODULOS	1
POTENCIA_NOMINAL 945.0   		// parte uruguaya       			     	
VIDA_UTIL       10			// como no tiene edad no se usa
PRODUCTO	potenciaFirme  		121.0    // Potencia Firme - Informe Garantia de suministro (ADME) Mes de menor valor total hidro 
PRODUCTO	potenciaMaxima 		850.5    // 90% de la potencia nominal
COSTO_FIJO	MUSD	0.0	
&FIN
&DATOS_ESPECIFICOS
NOMBRE		SALTO
//DATOS		SALTO
&FIN





///////////////////////////////////
// 
//          EOLICOS
//
///////////////////////////////////

// Se toma un costo fijo para precio del e�lico de 85 US$/MWh y factor 0.40, lo que equivale a 23.827 MUSD/a�o cada 100 MW



&RECURSO_BASE
NOMBRE		EOLOdeci
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	eolico
SUBTIPO2	unitario_escalado
TIENE_EDAD	no
DEPENDE_TIEMPO	pasoDeTiempo 
CANT_MODULOS	1
TIEMPO_ABSOLUTO_BASE		2016
POTENCIA_NOMINAL 		  963.0	1333.0	1461.0  1471.0 // En 2015, 2016 y 2017 se toma el promedio del potencia en el a�o
VIDA_UTIL       30			
PRODUCTO	potenciaFirme  	365.94      // FACTOR E�LICO 38% 
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		0.0	// 35.74	
ALEAT_OPERATIVA  sortecron=1
&FIN
&DATOS_ESPECIFICOS
NOMBRE		EOLOdeci
DATOS_UNITARIOS	eolicaUni	
&FIN




&RECURSO_BASE
NOMBRE		EOLO100		// El e�lico con entrada que se deciden en 2012 y 2013
CLASE		ELEG
PUNTO		unico
TIPO		generador
SUBTIPO1	eolico
SUBTIPO2	unitario_escalado
TIENE_EDAD	no
DEPENDE_TIEMPO	no 
CANT_MODULOS	1
TIEMPO_ABSOLUTO_BASE		2015
POTENCIA_NOMINAL 		100.0
PERIODO_CONST	1    
VIDA_UTIL       30			
PRODUCTO	potenciaFirme  	38.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD			18.97 //  23.30 CORRESPONDE A UN PRECIO DE 70 USD Y UN FACTOR DE 0.38 por 100MW // 20.97 CORRESPONDE A 63 USD // 18.97 corresponde a 57 USD/MWh
ALEAT_OPERATIVA  sortecron=1
&FIN
&DATOS_ESPECIFICOS
NOMBRE		EOLO100
DATOS_UNITARIOS	eolicaUni	
&FIN


&RECURSO_BASE
NOMBRE		EOLO200		// El e�lico con entrada que se deciden en 2012 y 2013
CLASE		ELEG
PUNTO		unico
TIPO		generador
SUBTIPO1	eolico
SUBTIPO2	unitario_escalado
TIENE_EDAD	no
DEPENDE_TIEMPO	no 
CANT_MODULOS	1
TIEMPO_ABSOLUTO_BASE		2015
POTENCIA_NOMINAL 		200.0
PERIODO_CONST	1    
VIDA_UTIL       30			
PRODUCTO	potenciaFirme  	74.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		45.376	//  CORRESPONDE A UN PRECIO DE 70 USD Y UN FACTOR DE 0.37 por 200 MW
ALEAT_OPERATIVA  sortecron=1
&FIN

&DATOS_ESPECIFICOS
NOMBRE		EOLO200
DATOS_UNITARIOS	eolicaUni	
&FIN

////////////////////////////
//    SOLAR
///////////////////////////

&RECURSO_BASE
NOMBRE		SOLdeci		// 
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	eolico
SUBTIPO2	unitario_escalado
TIENE_EDAD	no
DEPENDE_TIEMPO	pasoDeTiempo 
CANT_MODULOS	1
TIEMPO_ABSOLUTO_BASE	2016
POTENCIA_NOMINAL 		 90   90	239
PERIODO_CONST	1    
VIDA_UTIL       30			
PRODUCTO	potenciaFirme  	19.44
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD			0.0 // (factor 0.216) 
ALEAT_OPERATIVA  sortecron=1
&FIN
&DATOS_ESPECIFICOS
NOMBRE		SOLdeci
DATOS_UNITARIOS	solarUni	
&FIN




&RECURSO_BASE
NOMBRE		SOL250		//   Se considera potencia nominal (potencia del inverso)
CLASE		ELEG
PUNTO		unico
TIPO		generador
SUBTIPO1	eolico
SUBTIPO2	unitario_escalado
TIENE_EDAD	no
DEPENDE_TIEMPO	no 
CANT_MODULOS	1
TIEMPO_ABSOLUTO_BASE		2015
POTENCIA_NOMINAL 		250.0
PERIODO_CONST	1    
VIDA_UTIL       30			
PRODUCTO	potenciaFirme  	54.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD			32.16 // (factor 0.216) 54 USD/MWh => 25.54 MUSD,   61 USD/MWh => 28.85 MUSD, 68 USD/MWh => 32.16
ALEAT_OPERATIVA  sortecron=1
&FIN
&DATOS_ESPECIFICOS
NOMBRE		SOL250
DATOS_UNITARIOS	solarUni	
&FIN


&RECURSO_BASE
NOMBRE		SOL200		//   Se considera potencia nominal (potencia del inverso)
CLASE		ELEG
PUNTO		unico
TIPO		generador
SUBTIPO1	eolico
SUBTIPO2	unitario_escalado
TIENE_EDAD	no
DEPENDE_TIEMPO	no 
CANT_MODULOS	1
TIEMPO_ABSOLUTO_BASE		2015
POTENCIA_NOMINAL 		200.0
PERIODO_CONST	1    
VIDA_UTIL       30			
PRODUCTO	potenciaFirme  	43.2
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD			23.08 10 5 // (factor 0.216)    61 USD/MWh => 23.08 MUSD, 68 USD/MWh => 25.73
ALEAT_OPERATIVA  sortecron=1
&FIN
&DATOS_ESPECIFICOS
NOMBRE		SOL200
DATOS_UNITARIOS	solarUni	
&FIN


&RECURSO_BASE
NOMBRE		SOL125		// 
CLASE		ELEG
PUNTO		unico
TIPO		generador
SUBTIPO1	eolico
SUBTIPO2	unitario_escalado
TIENE_EDAD	no
DEPENDE_TIEMPO	no 
CANT_MODULOS	1
TIEMPO_ABSOLUTO_BASE		2015
POTENCIA_NOMINAL 		125.0
PERIODO_CONST	1    
VIDA_UTIL       30			
PRODUCTO	potenciaFirme  	27.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		    16.08 // (factor 0.216)  , 68 USD/MWh => 16.08 , 54 USD/MWh => 12.77 MUSD,   61 USD/MWh => 14.43 MUSD
ALEAT_OPERATIVA  sortecron=1
&FIN
&DATOS_ESPECIFICOS
NOMBRE		SOL125
DATOS_UNITARIOS	solarUni	
&FIN



///////////////////////////////////
// 
//          BIOMASA
//
///////////////////////////////////



&RECURSO_BASE
NOMBRE		BmNoDesp
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	biomasa
SUBTIPO2	estac	
TIENE_EDAD	no
TIEMPO_ABSOLUTO_BASE		2016
DEPENDE_TIEMPO	si          			     	
CANT_MODULOS	1
POTENCIA_NOMINAL 		 30.3 30.3 30.3 36.1 
VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	30.3
PRODUCTO	potenciaMaxima 	30.3
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=1
&FIN
&DATOS_ESPECIFICOS
NOMBRE		BmNoDesp
PAIS		BRA			
NEST	1				
SEMFINEST	52	
PREEST		0.01	
FACEST		0.0	
POTEST		30.3	
POTEST      30.3
POTEST      30.3	
POTEST      30.3
POTEST		30.3	
POTEST      30.3
POTEST      30.3	
POTEST      30.3	
DISPEST		1.0	
&FIN



&RECURSO_BASE
NOMBRE		BmDesp
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	biomasa
SUBTIPO2	estac		
TIENE_EDAD	no
TIEMPO_ABSOLUTO_BASE		2016
DEPENDE_TIEMPO	si          			     	
CANT_MODULOS	1
POTENCIA_NOMINAL 		  0.7  0.7  0.7  0.7  
//VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	0.62
PRODUCTO	potenciaMaxima 	0.62
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=1
&FIN
&DATOS_ESPECIFICOS
NOMBRE		BmDesp
PAIS		BRA			
NEST	1				
SEMFINEST	52	
PREEST		70.0	
FACEST		0.0	
POTEST		0.7	
POTEST      0.7	
POTEST      0.7	
POTEST      0.7	
POTEST		0.7	
POTEST      0.7	
POTEST      0.7	
POTEST      0.7	
DISPEST		0.88	
&FIN


&RECURSO_BASE
NOMBRE		UPM1 
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	biomasa
SUBTIPO2	estac		// tipo_entrada estac se presentan los datos por estaci�n y poste
TIEMPO_ABSOLUTO_BASE	2016
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	40.0 
//VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	13.6
PRODUCTO	potenciaMaxima 	13.6
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=0
&FIN
&DATOS_ESPECIFICOS
NOMBRE		UPM1
PAIS		ARG			// pa�s de origen
NEST	3				// cantidad de estaciones
SEMFINEST	40	42	52	// semana final de cada una de las estaciones
PREEST		0.01	0.01	0.01	// precio por estaci�n
FACEST		0.0	    0.0	    0.0	       // factor sobre el precio spot
POTEST		40.0	0.0 	40.0	// potencia por estaci�n (columna) y poste horario (fila) LA CANTIDAD DE POSTES ES UN DATO GENERAL
POTEST      40.0	0.0 	40.0
POTEST      40.0	0.0 	40.0
POTEST      40.0	0.0 	40.0
POTEST      40.0	0.0 	40.0
POTEST      40.0	0.0 	40.0
POTEST      40.0	0.0 	40.0
POTEST      40.0	0.0 	40.0
DISPEST		0.34	0.0 	0.34 	// disponibilidad
&FIN


&RECURSO_BASE
NOMBRE		MDP
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	biomasa
SUBTIPO2	estac		// tipo_entrada estac se presentan los datos por estaci�n y poste
TIEMPO_ABSOLUTO_BASE	2016
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	100.0 
//VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	65.1
PRODUCTO	potenciaMaxima 	65.1
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=0
&FIN
&DATOS_ESPECIFICOS
NOMBRE		MDP
PAIS		ARG			// pa�s de origen
NEST	3				// cantidad de estaciones
SEMFINEST	13	15	52	// semana final de cada una de las estaciones
PREEST		0.01	0.01	0.01	// precio por estaci�n
FACEST		0.0	    0.0	    0.0	       // factor sobre el precio spot
POTEST		100.0	0.0 	100.0	// potencia por estaci�n (columna) y poste horario (fila) LA CANTIDAD DE POSTES ES UN DATO GENERAL
POTEST      100.0	0.0 	100.0
POTEST      100.0	0.0 	100.0
POTEST      100.0	0.0 	100.0
POTEST      100.0	0.0 	100.0
POTEST      100.0	0.0 	100.0
POTEST      100.0	0.0 	100.0
POTEST      100.0	0.0 	100.0
DISPEST		0.651	0.0 	0.651 	// disponibilidad
&FIN

&RECURSO_BASE
NOMBRE		UPM2
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	biomasa
SUBTIPO2	estac		// tipo_entrada estac se presentan los datos por estaci�n y poste
TIEMPO_ABSOLUTO_BASE	2024
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	150.0 
//VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	135.0
PRODUCTO	potenciaMaxima 	135.0
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=0
&FIN
&DATOS_ESPECIFICOS
NOMBRE		UPM2
PAIS		ARG			// pa�s de origen
NEST	3				// cantidad de estaciones
SEMFINEST	42	44	52	// semana final de cada una de las estaciones
PREEST		0.01	0.01	0.01	// precio por estaci�n
FACEST		0.0	    0.0	    0.0	       // factor sobre el precio spot
POTEST		150.0	0.0 	150.0	// potencia por estaci�n (columna) y poste horario (fila) LA CANTIDAD DE POSTES ES UN DATO GENERAL
POTEST      150.0	0.0 	150.0
POTEST      150.0	0.0 	150.0
POTEST      150.0	0.0 	150.0
POTEST      150.0	0.0 	150.0
POTEST      150.0	0.0 	150.0
POTEST      150.0	0.0 	150.0
POTEST      150.0	0.0 	150.0
DISPEST		0.90 	0.0 	0.90 	// disponibilidad
&FIN




///////////////////////////////////
// 
//          IMPORTACIONES
//
///////////////////////////////////


&RECURSO_BASE
NOMBRE		IARSPFO // EN PRINCIPIO NO SE ESTA USANDO POR LO CUAL NO FUE REVISADO
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	importacion
SUBTIPO2	estac		// tipo_entrada estac se presentan los datos por estaci�n y poste
TIEMPO_ABSOLUTO_BASE	2011
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	150.0 
VIDA_UTIL       10		
PRODUCTO	potenciaFirme  		0.0
PRODUCTO	potenciaMaxima 		0.0
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=0
&FIN
&DATOS_ESPECIFICOS
NOMBRE		IARSPFO
PAIS		ARG			// pa�s de origen
NEST	3				// cantidad de estaciones
SEMFINEST	19	34	52	// semana final de cada una de las estaciones
PREEST		120.0	120.0	120.0	// precio por estaci�n
FACEST		0.0	0.0	0.0	// factor sobre el precio spot
POTEST		150.0	150.0	150.0	// potencia por estaci�n (columna) y poste horario (fila) LA CANTIDAD DE POSTES ES UN DATO GENERAL
POTEST         	150.0	150.0	150.0
POTEST         	150.0	150.0	150.0
POTEST         	150.0	150.0	150.0
POTEST         	150.0	150.0	150.0
POTEST         	150.0	150.0	150.0
POTEST         	150.0	150.0	150.0
POTEST         	150.0	150.0	150.0
DISPEST		0.5	0.2	0.5 	// disponibilidad
&FIN



&RECURSO_BASE
NOMBRE		IARGO110 // NO SE USA SE SUSTITUYE POR PALIER FORMUL IARGO PARA EVITAR CAMBIAR PRECIO CADA VEZ QUE CAMBIA VALOR DE PETROLEO
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	importacion
SUBTIPO2	estac		// tipo_entrada estac se presentan los datos por estaci�n y poste
TIEMPO_ABSOLUTO_BASE	2011
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	200.0 
//VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	0.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=0
&FIN
&DATOS_ESPECIFICOS
NOMBRE		IARGO110
PAIS		ARG			// pa�s de origen
NEST	3				// cantidad de estaciones
SEMFINEST	18	40	52	// semana final de cada una de las estaciones
PREEST		340.0	340.0	340.0	// precio por estaci�n
FACEST		0.0	0.0	0.0	// factor sobre el precio spot
POTEST		200.0	140.0	200.0	// potencia por estaci�n (columna) y poste horario (fila) LA CANTIDAD DE POSTES ES UN DATO GENERAL
POTEST      200.0	140.0	200.0
POTEST      200.0	140.0	200.0
POTEST      200.0	140.0	200.0
POTEST      200.0	140.0	200.0
POTEST      200.0	140.0	200.0
POTEST      200.0	140.0	200.0
POTEST      200.0	140.0	200.0
DISPEST		0.65	0.5 	0.65 	// disponibilidad
&FIN

&RECURSO_BASE
NOMBRE	IARGO 
CLASE 	EXIST
PUNTO 	unico
TIPO	generador
SUBTIPO1	importacion
SUBTIPO2	formul		
// para establecer la dependencia del precio de importacion con el bbl se usa palier formul con archivo cmg que contiene precio de compra o precio de compra por
// valor fijo para incluir diferencia estacional, esa multiplicaci�n  luego se revierte considerando como factor del tramo el inverso
TIEMPO_ABSOLUTO_BASE	2015
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	200.0  
//VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	0.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=0
&FIN
&DATOS_ESPECIFICOS
NOMBRE		IARGO
PAIS		ARG
NTRAM		2
LIMTRA		1000.0   // valor que hoy esta por encima de los correspondientes a cada uno los valores de petr�leo considerados
PRETRAM		0	0		// el precio correspondiente est� en el archivo cosmar.arg. 2 estaciones, en una de ellas este precio est� multiplicado por 10
FACTRAM		1.0     0.1 // para considerar estaciones revierte la multiplicaci�n del precio, divide entre 10
POTRAM		200.0	0.0	
POTRAM		200.0	0.0
POTRAM		200.0	0.0
POTRAM		200.0	0.0
POTRAM		200.0	0.0	
POTRAM		200.0	0.0
POTRAM		200.0	0.0
POTRAM		200.0	0.0
DISPTRAM	0.65 0.5
&FIN

&RECURSO_BASE
NOMBRE		IBRIV
CLASE		EXIST
PUNTO		unico
TIPO		generador
SUBTIPO1	importacion
SUBTIPO2	estac		// tipo_entrada estac se presentan los datos por estaci�n y poste
TIEMPO_ABSOLUTO_BASE	2016
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	45.0 
//VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	0.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		0.0
ALEAT_OPERATIVA  sortecron=0	
&FIN
&DATOS_ESPECIFICOS
NOMBRE		IBRIV
PAIS		BRA			// pa�s de origen
NEST	3				// cantidad de estaciones
SEMFINEST	18	40	52	// semana final de cada una de las estaciones
PREEST		362.0	362.0	362.0	// precio por estaci�n
FACEST		0.0	0.0	0.0	// factor sobre el precio spot
POTEST		0.0	0.0	0.0	// potencia por estaci�n (columna) y poste horario (fila) LA CANTIDAD DE POSTES ES UN DATO GENERAL
POTEST      0.0	0.0	0.0
POTEST      0.0	0.0	0.0
POTEST      0.0	0.0	0.0
POTEST      0.0	0.0	0.0
POTEST      0.0	0.0	0.0
POTEST      0.0	0.0	0.0
POTEST      45.0 45.0 45.0
DISPEST		1.0	0.0	1.0 	// disponibilidad
&FIN



&RECURSO_BASE
NOMBRE	IBMELO
CLASE 	EXIST
PUNTO 	unico
TIPO	generador
SUBTIPO1	importacion
SUBTIPO2	formul		
TIEMPO_ABSOLUTO_BASE	2016
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	200.0 300.0 300.0 
//VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	0.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=0
&FIN
&DATOS_ESPECIFICOS
NOMBRE		IBMELO
PAIS		BRA
NTRAM		2
LIMTRA		136.0
PRETRAM		361.0	9999.9		
FACTRAM		0.0
POTRAM		200.0	200.0	
POTRAM		200.0	200.0
POTRAM		200.0	200.0
POTRAM		200.0	200.0
POTRAM		200.0	200.0	
POTRAM		200.0	200.0
POTRAM		200.0	200.0
POTRAM		200.0	200.0
DISPTRAM	0.7 0.7
&FIN



&RECURSO_BASE
NOMBRE	IB500SEE // NO SE USA - SIN CHEQUEAR
CLASE 	EXIST
PUNTO 	unico
TIPO	generador
SUBTIPO1	importacion
SUBTIPO2	formul		
TIEMPO_ABSOLUTO_BASE	2011
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	200.0 
VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	0.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=0
&FIN
&DATOS_ESPECIFICOS
NOMBRE		IB500SEE
PAIS		BRA			
NTRAM		1
LIMTRA		9999.0
PRETRAM		180.0		
FACTRAM		0.0
POTRAM		200.0	200.0	200.0	200.0
DISPTRAM	0.5
&FIN


///////////////////////////////////
// 
//          EXPORTACIONES
//
///////////////////////////////////


&RECURSO_BASE
NOMBRE	EBMELO
CLASE 	EXIST
PUNTO 	unico
TIPO	carga
SUBTIPO1	exportacion
SUBTIPO2	formul		
TIEMPO_ABSOLUTO_BASE	2015
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	300.0 
VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	0.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=0
&FIN
&DATOS_ESPECIFICOS
NOMBRE		EBMELO
PAIS		BRA			
NTRAM		2
LIMTRA		80.0
PRETRAM		0.0 21.0		
FACTRAM		0.0 0.0
POTRAM		300.0	300.0	
POTRAM		300.0	300.0	
POTRAM		300.0	300.0
POTRAM		300.0	300.0
POTRAM		300.0	300.0	
POTRAM		300.0	300.0	
POTRAM		300.0	300.0
POTRAM		300.0	300.0
DISPTRAM	0.0 1.0
&FIN


&RECURSO_BASE
NOMBRE	EBRIV
CLASE 	EXIST
PUNTO 	unico
TIPO	carga
SUBTIPO1	exportacion
SUBTIPO2	formul		
TIEMPO_ABSOLUTO_BASE	2015
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	70.0 
VIDA_UTIL       10		
PRODUCTO	potenciaFirme  	0.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		0.0	
ALEAT_OPERATIVA  sortecron=0
&FIN
&DATOS_ESPECIFICOS
NOMBRE		EBRIV
PAIS		BRA			
NTRAM		2
LIMTRA		80.0
PRETRAM		0.0 20.0		
FACTRAM		0.0 0.0
POTRAM		70.0	70.0	
POTRAM		70.0	70.0	
POTRAM		70.0	70.0
POTRAM		70.0	70.0
POTRAM		70.0	70.0	
POTRAM		70.0	70.0	
POTRAM		70.0	70.0
POTRAM		70.0	70.0
DISPTRAM	0.0 1.0
&FIN


&RECURSO_BASE
NOMBRE	EARSP15
CLASE 	EXIST
PUNTO 	unico
TIPO	carga
SUBTIPO1	exportacion
SUBTIPO2	estac		// tipo_entrada estac se presentan los datos por estaci�n y poste
TIEMPO_ABSOLUTO_BASE	2015
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	200.0 
VIDA_UTIL       100		
PRODUCTO	potenciaFirme  	0.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		0.0	
&FIN
&DATOS_ESPECIFICOS
NOMBRE		EARSP15
PAIS		ARG			// pa�s de origen
NEST		1			// cantidad de estaciones
SEMFINEST	52
PREEST		15.0	
FACEST		0.0	
POTEST		200.0
POTEST      200.0
POTEST      200.0
POTEST      200.0
POTEST		200.0
POTEST      200.0
POTEST      200.0
POTEST      200.0
DISPEST		1.0	
&FIN



&RECURSO_BASE
NOMBRE	EXCED
CLASE 	EXIST
PUNTO 	unico
TIPO	carga
SUBTIPO1	exportacion
SUBTIPO2	estac		// tipo_entrada estac se presentan los datos por estaci�n y poste
TIEMPO_ABSOLUTO_BASE	2015
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	5000.0 
//VIDA_UTIL       100		
PRODUCTO	potenciaFirme  	0.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		0.0	
&FIN
&DATOS_ESPECIFICOS
NOMBRE		EXCED
PAIS		ARG			// pa�s de origen
NEST		1			// cantidad de estaciones
SEMFINEST	52
PREEST		0.03	
FACEST		0.0	
POTEST		5000.0
POTEST      5000.0
POTEST      5000.0
POTEST      5000.0
POTEST		5000.0
POTEST      5000.0
POTEST      5000.0
POTEST      5000.0
DISPEST		1.0
&FIN


&RECURSO_BASE
NOMBRE	SUMID
CLASE 	EXIST
PUNTO 	unico
TIPO	carga
SUBTIPO1	exportacion
SUBTIPO2	estac		// tipo_entrada estac se presentan los datos por estaci�n y poste
TIEMPO_ABSOLUTO_BASE	2015
TIENE_EDAD	no
DEPENDE_TIEMPO	no           			     	
CANT_MODULOS	1
POTENCIA_NOMINAL	10000.0 
//VIDA_UTIL       100		
PRODUCTO	potenciaFirme  	0.0
PRODUCTO	potenciaMaxima 	0.0
COSTO_FIJO	MUSD		0.0	
&FIN
&DATOS_ESPECIFICOS
NOMBRE		SUMID
PAIS		ARG			// pa�s de origen
NEST		1			// cantidad de estaciones
SEMFINEST	52
PREEST		0.025	
FACEST		0.0	
POTEST		10000.0
POTEST      10000.0
POTEST      10000.0
POTEST      10000.0
POTEST		10000.0
POTEST      10000.0
POTEST      10000.0
POTEST      10000.0
DISPEST		1.0
&FIN





///////////////////////////////////
// 
//          EJEMPLO
//
///////////////////////////////////


//&RECURSO_BASE
//NOMBRE		TG120
//CLASE		ELEG		
//PUNTO		unico
//TIPO		generador
//SUBTIPO1	termico
//SUBTIPO2	turbinaAGas
//TIENE_EDAD	no		// el tipo de recurso  puede tener o no edad. si no tiene edad, no puede tener vida �til ni puede depender de la edad
//DEPENDE_TIEMPO	no           	// el valor de dependeTiempo puede ser edad, pasoDeTiempo o no, seg�n las caracter�sticas del recursoBase
 			     	// dependan de la edad, del paso de tiempo o sean invariantes en el tiempo.
				// un recurso que depende de la edad debe tener edad
//TIEMPO_ABSOLUTO_BASE	2009	// Si el recurso depende del paso de tiempo, se debe especificar
				// un tiempo absoluto base a partir del cual se cargan los datos
				// En cualquier caso un recursoBase no aporta productos antes de su tiempo absoluto base

				



//CANT_MODULOS	 1				// CANTIDAD DE M�DULOS	
//POTENCIA_NOMINAL 120.0				// el primer n�mero es el valor al nacer si el recursoBase tiene edad o el valor
						// en el NOMBRE_PASO_BASE si depende del tiempo, o simplemente el valor constante si
						// el recursoBase no var�a con la edad ni el tiempo.
						// Los siguientes si existen son los valores
						// para edades o pasos de tiempo subsiguientes.
						// Si faltan valores se mantiene el �ltimo le�do



//PERIODO_CONST	1               // per�odo de construcci�n en a�os


//PRODUCTO	potenciaFirme  		100.0	
//INVERSION 	MUSD		20.0	20.0        //  inversiones en MUSD en los a�os de construcci�n, solo los recursos ELEGs tienen inversi�n

//COSTO_FIJO	MUSD		2.0	2.0	2.0	2.0	     // costos fijos en MUSD por a�o seg�n edad

//INVERSION_ALEAT no 		// la inversi�n es aleatoria seg�n el valor de una variable de la naturaleza, solo para recursos ELEGs
				// el nombre de la variable de la naturaleza en el grafo es el del recurso base con sufijo _in	  
				// el valor num�rico de esta variable determina un factor multiplicativo de la inversi�n
// ALEAT_OPERATIVA  string que describe el origen de la aleatoriedad
//                  para el modelo de operaci�n. EN EDF sortecron=0 o =1 seg�n los sorteos en murvagua
				
//&FIN
//&DATOS_ESPECIFICOS
//NOMBRE	TG120		
//MINTEC	P			// puede ser S semanal o P por poste
//POTENCIA_MINTEC	20.0		// potencia de m�nimo t�cnico por tranche ATENCI�N
//DPOTAN		0.00		// reducci�n de potencia nominal
//REND_POTENCIA_NOM	0.3450
//REND_POTENCIA_MIN	0.2854
//DRENAN			0.00    // derateo anual del rendimiento
//DISPONIBILIDAD		0.85
//COMBUSTIBLES		FUEL_OIL	// Lista de combustibles en el orden de prioridad de su uso si existe ese orden apriori	
//DPOTCS			0.0	// derateo de potencia por usar combustible secundario
//DRENCS			0.0	// derateo de rendimiento por usar combustible secundario
//COYMV			5.0	// costo variable de OyM en US$/MWh
//COYMF			0.0	// costo fijo de OyM en US$/MWh
//&FIN

