// DATOS PARA UN PROCESO DEMANDA CON A O BASE
NOMBRE_PROCESO  Dem_TT
ESTIMACION  Prueba_setiembre22
CANT_VARIABLES  1
SUMA_VARIABLES SI   // SI - El proceso tiene una única V.A. que es la suma de las variables definidas, NO - Cada variable genera una V.A diferente  
NOMBRE_VAR_SUMA  DEM_TT   // Se ignora si SUMA_VARIABLES SI vale NO
NOMBRES_VARIABLES DEMANDA-TT
NOMBRE_PASO HORA
ANIOS_BASE 2020  // A OS BASE SEPARADOS POR ESPACIO EN ORDEN CRONOLOGICO
ANIO_BASE_ELEGIDO 2020
ANIO_INICIAL_HORIZONTE 2023
ANIO_FINAL_HORIZONTE 2048
ENERGIAS_GWH  // PARA CADA UNO DE LOS A OS BASE Y DEL HORIZONTE EN ORDEN CRONOLOGICO
2020 749.72
2023 749.72
2024 751.78
2025 749.72
2026 749.72
2027 749.72
2028 751.78
2029 749.72
2030 749.72
2031 749.72
2032 751.78
2033 749.72
2034 749.72
2035 749.72
2036 751.78
2037 749.72
2038 749.72
2039 749.72
2040 751.78
2041 749.72
2042 749.72
2043 749.72
2044 751.78
2045 749.72
2046 749.72
2047 749.72
2048 751.78
AJUSTE_ENERGIAS_ANUALES SI    // SI = CORRIGE LAS POTENCIAS PARA QUE CIERRE LA SUMA DE POTENCIAS DE TODAS LAS VARIABLES CON LA ENERG A ANUAL PREVISTA
// TIPOS DE D AS DE LOS 7 DIAS DE LA SEMANA DO, LU, MA, MI, JU, VI, SA
TIPOS_DIA_SEMANA 1 2 3 4 5 6 7
// FERIADOS QUE NO CAMBIAN DE FECHA EN TODOS LOS A OS
FERIADOS_COMUNES CANTIDAD 5   // mes / d a
1 1
5 1
7 18
8 25
12 25
// DIAS ESPECIALES DE LOS A OS BASE Y DEL HORIZONTE DE PREDICCI N
2010 2 15 lunes-carnaval
2010 2 16 martes-carnaval
2010 3 29 lunes-turismo
2010 3 30 martes-turismo
2010 4 1 miercoles-turismo
2010 4 2 jueves-turismo
2010 4 3 viernes-turismo
2011 3 7 lunes-carnaval
2011 3 8 martes-carnaval
2011 4 18 lunes-turismo
2011 4 19 martes-turismo
2011 4 20 miercoles-turismo
2011 4 21 jueves-turismo
2011 4 22 viernes-turismo
2012 2 20 lunes-carnaval
2012 2 21 martes-carnaval
2012 4 2 lunes-turismo
2012 4 3 martes-turismo
2012 4 4 miercoles-turismo
2012 4 5 jueves-turismo
2012 4 6 viernes-turismo
2013 2 11 lunes-carnaval
2013 2 12 martes-carnaval
2013 3 25 lunes-turismo
2013 3 26 martes-turismo
2013 3 27 miercoles-turismo
2013 3 28 jueves-turismo
2013 3 29 viernes-turismo
2014 3 3 lunes-carnaval
2014 3 4 martes-carnaval
2014 4 14 lunes-turismo
2014 4 15 martes-turismo
2014 4 16 miercoles-turismo
2014 4 17 jueves-turismo
2014 4 18 viernes-turismo
2015 2 16 lunes-carnaval
2015 2 17 martes-carnaval
2015 3 30 lunes-turismo
2015 3 31 martes-turismo
2015 4 1 miercoles-turismo
2015 4 2 jueves-turismo
2015 4 3 viernes-turismo
2017 2 27 lunes-carnaval
2017 2 28 martes-carnaval
2017 4 10 lunes-turismo
2017 4 11 martes-turismo
2017 4 12 miercoles-turismo
2017 4 13 jueves-turismo
2017 4 14 viernes-turismo
2018 2 12 lunes-carnaval
2018 2 13 martes-carnaval
2018 3 26 lunes-turismo
2018 3 27 martes-turismo
2018 3 28 miercoles-turismo
2018 3 29 jueves-turismo
2018 3 30 viernes-turismo
2019 3 4 lunes-carnaval
2019 3 5 martes-carnaval
2019 4 15 lunes-turismo
2019 4 16 martes-turismo
2019 4 17 miercoles-turismo
2019 4 18 jueves-turismo
2019 4 19 viernes-turismo
2020 2 24 lunes-carnaval
2020 2 25 martes-carnaval
2020 4 6 lunes-turismo
2020 4 7 martes-turismo
2020 4 8 miercoles-turismo
2020 4 9 jueves-turismo
2020 4 10 viernes-turismo
2021 2 15 lunes-carnaval
2021 2 16 martes-carnaval
2021 3 29 lunes-turismo
2021 3 30 martes-turismo
2021 3 31 miercoles-turismo
2021 4 1 jueves-turismo
2021 4 2 viernes-turismo
2022 2 28 lunes-carnaval
2022 3 1 martes-carnaval
2022 4 11 lunes-turismo
2022 4 12 martes-turismo
2022 4 13 miercoles-turismo
2022 4 14 jueves-turismo
2022 4 15 viernes-turismo
2023 2 20 lunes-carnaval
2023 2 21 martes-carnaval
2023 4 3 lunes-turismo
2023 4 4 martes-turismo
2023 4 5 miercoles-turismo
2023 4 6 jueves-turismo
2023 4 7 viernes-turismo
2024 2 12 lunes-carnaval
2024 2 13 martes-carnaval
2024 3 25 lunes-turismo
2024 3 26 martes-turismo
2024 3 27 miercoles-turismo
2024 3 28 jueves-turismo
2024 3 29 viernes-turismo
2025 3 3 lunes-carnaval
2025 3 4 martes-carnaval
2025 4 14 lunes-turismo
2025 4 15 martes-turismo
2025 4 16 miercoles-turismo
2025 4 17 jueves-turismo
2025 4 18 viernes-turismo
2026 2 16 lunes-carnaval
2026 2 17 martes-carnaval
2026 3 30 lunes-turismo
2026 3 31 martes-turismo
2026 4 1 miercoles-turismo
2026 4 2 jueves-turismo
2026 4 3 viernes-turismo
2027 2 8 lunes-carnaval
2027 2 9 martes-carnaval
2027 3 22 lunes-turismo
2027 3 23 martes-turismo
2027 3 24 miercoles-turismo
2027 3 25 jueves-turismo
2027 3 26 viernes-turismo
2028 2 28 lunes-carnaval
2028 2 29 martes-carnaval
2028 4 10 lunes-turismo
2028 4 11 martes-turismo
2028 4 12 miercoles-turismo
2028 4 13 jueves-turismo
2028 4 14 viernes-turismo
2029 2 12 lunes-carnaval
2029 2 13 martes-carnaval
2029 3 26 lunes-turismo
2029 3 27 martes-turismo
2029 3 28 miercoles-turismo
2029 3 29 jueves-turismo
2029 3 30 viernes-turismo
2030 3 4 lunes-carnaval
2030 3 5 martes-carnaval
2030 4 15 lunes-turismo
2030 4 16 martes-turismo
2030 4 17 miercoles-turismo
2030 4 18 jueves-turismo
2030 4 19 viernes-turismo
2031 2 24 lunes-carnaval
2031 2 25 martes-carnaval
2031 4 7 lunes-turismo
2031 4 8 martes-turismo
2031 4 9 miercoles-turismo
2031 4 10 jueves-turismo
2031 4 11 viernes-turismo
2032 2 9 lunes-carnaval
2032 2 10 martes-carnaval
2032 3 22 lunes-turismo
2032 3 23 martes-turismo
2032 3 24 miercoles-turismo
2032 3 25 jueves-turismo
2032 3 26 viernes-turismo
2033 2 28 lunes-carnaval
2033 3 1 martes-carnaval
2033 4 11 lunes-turismo
2033 4 12 martes-turismo
2033 4 13 miercoles-turismo
2033 4 14 jueves-turismo
2033 4 15 viernes-turismo
2034 2 20 lunes-carnaval
2034 2 21 martes-carnaval
2034 4 3 lunes-turismo
2034 4 4 martes-turismo
2034 4 5 miercoles-turismo
2034 4 6 jueves-turismo
2034 4 7 viernes-turismo
2035 2 5 lunes-carnaval
2035 2 6 martes-carnaval
2035 3 19 lunes-turismo
2035 3 20 martes-turismo
2035 3 21 miercoles-turismo
2035 3 22 jueves-turismo
2035 3 23 viernes-turismo
2036 2 25 lunes-carnaval
2036 2 26 martes-carnaval
2036 4 7 lunes-turismo
2036 4 8 martes-turismo
2036 4 9 miercoles-turismo
2036 4 10 jueves-turismo
2036 4 11 viernes-turismo
2037 2 16 lunes-carnaval
2037 2 17 martes-carnaval
2037 3 30 lunes-turismo
2037 3 31 martes-turismo
2037 4 1 miercoles-turismo
2037 4 2 jueves-turismo
2037 4 3 viernes-turismo
2038 3 8 lunes-carnaval
2038 3 9 martes-carnaval
2038 4 19 lunes-turismo
2038 4 20 martes-turismo
2038 4 21 miercoles-turismo
2038 4 22 jueves-turismo
2038 4 23 viernes-turismo
2039 2 21 lunes-carnaval
2039 2 22 martes-carnaval
2039 4 4 lunes-turismo
2039 4 5 martes-turismo
2039 4 6 miercoles-turismo
2039 4 7 jueves-turismo
2039 4 8 viernes-turismo
2040 2 13 lunes-carnaval
2040 2 14 martes-carnaval
2040 3 26 lunes-turismo
2040 3 27 martes-turismo
2040 3 28 miercoles-turismo
2040 3 29 jueves-turismo
2040 3 30 viernes-turismo
2041 2 13 lunes-carnaval
2041 2 14 martes-carnaval
2041 3 26 lunes-turismo
2041 3 27 martes-turismo
2041 3 28 miercoles-turismo
2041 3 29 jueves-turismo
2041 3 30 viernes-turismo
2042 2 13 lunes-carnaval
2042 2 14 martes-carnaval
2042 3 26 lunes-turismo
2042 3 27 martes-turismo
2042 3 28 miercoles-turismo
2042 3 29 jueves-turismo
2042 3 30 viernes-turismo
2043 2 13 lunes-carnaval	// A PARTIR DE ACA SE REPITEN LAS FECHAS 
2043 2 14 martes-carnaval
2043 3 26 lunes-turismo
2043 3 27 martes-turismo
2043 3 28 miercoles-turismo
2043 3 29 jueves-turismo
2043 3 30 viernes-turismo
2044 2 13 lunes-carnaval
2044 2 14 martes-carnaval
2044 3 26 lunes-turismo
2044 3 27 martes-turismo
2044 3 28 miercoles-turismo
2044 3 29 jueves-turismo
2044 3 30 viernes-turismo
2045 2 13 lunes-carnaval
2045 2 14 martes-carnaval
2045 3 26 lunes-turismo
2045 3 27 martes-turismo
2045 3 28 miercoles-turismo
2045 3 29 jueves-turismo
2045 3 30 viernes-turismo
2046 2 13 lunes-carnaval
2046 2 14 martes-carnaval
2046 3 26 lunes-turismo
2046 3 27 martes-turismo
2046 3 28 miercoles-turismo
2046 3 29 jueves-turismo
2046 3 30 viernes-turismo
2047 2 13 lunes-carnaval
2047 2 14 martes-carnaval
2047 3 26 lunes-turismo
2047 3 27 martes-turismo
2047 3 28 miercoles-turismo
2047 3 29 jueves-turismo
2047 3 30 viernes-turismo
2048 2 13 lunes-carnaval
2048 2 14 martes-carnaval
2048 3 26 lunes-turismo
2048 3 27 martes-turismo
2048 3 28 miercoles-turismo
2048 3 29 jueves-turismo
2048 3 30 viernes-turismo
