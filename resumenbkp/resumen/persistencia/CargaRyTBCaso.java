/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import dominio.ConjRecB;
import dominio.ConjTransB;
import dominio.Estudio;
import dominio.RecursoBase;
import dominio.TransforBase;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public abstract class CargaRyTBCaso {

    public void cargarRyTBCaso(String dirArchivo, ConjRecB conjRBCaso,
            ConjTransB conjTBCaso, Estudio est) throws XcargaDatos {

        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        LeerDatosArchivo cargador = new LeerDatosArchivo() {
        };
        datAr = cargador.getDatos(dirArchivo);

        CargaRecBase cargadorRB = new CargaRecBase() {
        };
        CargaTransBase cargadorTB = new CargaTransBase() {
        };

        ArrayList<String> listaRBCaso = new ArrayList<String>();
        ArrayList<String> listaTBCaso = new ArrayList<String>();

        listaRBCaso = cargadorRB.cargarListRec(datAr);
        listaTBCaso = cargadorTB.cargarListTrans(datAr);

        for (String st : listaRBCaso) {
            RecursoBase rB = new RecursoBase(est);
            rB = est.getConjRecB().getUnRecB(st);
            if (rB == null) {
                throw new XcargaDatos("El recurso " + st
                        + " que se quiere incluir en el caso no existe en el estudio");
            } else {
                if (!(rB.getClase().equalsIgnoreCase(TiposEnum.ExistEleg.ELEG))){
                    throw new XcargaDatos("El RecursoBase "+ st + "no es ELEG");
                }else{
                    conjRBCaso.agregar(rB);
                }
            }
        }

        for (String st : listaTBCaso) {
            TransforBase tB = new TransforBase(est);
            tB = est.getConjTransB().getUnaTransB(st);
            if (tB == null) {
                throw new XcargaDatos("La tranformacionBase " + st
                        + " que se quiere incluir en el caso no existe en el estudio");
            } else {
               if (!(tB.getClase().equalsIgnoreCase(TiposEnum.ExistEleg.ELEG))){
                    throw new XcargaDatos("La TransforBase "+ st + "no es ELEG");
                }else{                               
                conjTBCaso.agregar(tB);
               }
            }
        }



    }
}
