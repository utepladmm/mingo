package persistencia;

import java.util.ArrayList;

public class CargaModeloOP {
	
	/*
	 * Devuelve la constante que identifica al modelo de operaci�n seg�n lo que lea en el 
	 * archivo dirModeloOperacion
	 */
	public static String devuelveModeloOp(String dirModeloOperacion){
		ArrayList<ArrayList<String>> datos = LeerDatosArchivo.getDatos(dirModeloOperacion);
		String modeloOp = null;
		if(datos.get(0).get(0).equalsIgnoreCase(UtilitariosGenerales.ConstantesMingo.EDF)){
			modeloOp = UtilitariosGenerales.ConstantesMingo.EDF;
		}else if(datos.get(0).get(0).equalsIgnoreCase(UtilitariosGenerales.ConstantesMingo.MOP)){
			modeloOp = UtilitariosGenerales.ConstantesMingo.MOP;
		}else{
			System.out.println("ERROR EN LA LECTURA DEL ARCHIVO MODELO_OPERACION.txt");
			System.exit(1);
		}
		return modeloOp;
	}

}
