/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistencia;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public abstract class DeleteGetDatosArchivo {

        public ArrayList<ArrayList<String>> getDatos(String dirArchivo){
        BufferedReader entrada = null;
        ArrayList<ArrayList<String>> resultado =
                                            new ArrayList<ArrayList<String>>();
        ArrayList<String> lineaResul;
        String[] datosLinea;
        String linea;
        String sep = "[\\s]+"; // Separador de datos.
        String coment = "//"; // Indicador de comentario.
        int posCom; // Posición del inicio del comentario.
        int j;

        try{
            File archivo = new File(dirArchivo);
            entrada = new BufferedReader(new FileReader(archivo));
            while( (linea = entrada.readLine()) != null){
                // Eliminación de los comentarios de las líneas.
                if( (posCom = linea.indexOf(coment)) > -1 ){
                    linea = (posCom == 0)? "" : linea.substring(0, posCom);
                }
                // Si la línea no es una línea de blancos, entonces se agregan
                // los datos y se agrega una nueva "línea" a resultado.
                if( !linea.matches("[\\s]*" ) ){
                    datosLinea = linea.split(sep);
                    lineaResul = new ArrayList<String>();
                    for(j = 0; j < datosLinea.length; j++){
                        /* Si el dato NO es un string de longitud cero "",
                         * lo cual podría ocurrir si
                         * la línea comienza por ej. con tabulador, se agrega
                         * el dato. */
                        if( !datosLinea[j].equalsIgnoreCase("") ){
                            lineaResul.add(datosLinea[j]);
                        }
                    }
                    resultado.add(lineaResul);
                }
            }
            entrada.close();
        }catch(FileNotFoundException ex){
            System.out.println("------ERROR-------");
            System.out.println("No se encontró el archivo: " + dirArchivo);
            System.out.println("------------------\n");
        }catch(IOException ex){
            System.out.println("------ERROR-------");
            System.out.println("Al leer el archivo: " + dirArchivo +
                    ", se produjo la excepción: " + ex.toString() );
            System.out.println("------------------\n");
        }
        
        
        return resultado;        
    }

}

