/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistencia;

import dominio.FallaPorcentual;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class CargaFallaPorcentual {

    public void cargar(ArrayList<ArrayList<String>> datAr, String nombDem, FallaPorcentual falla) throws XcargaDatos{
	//  A partir del texto en datAr carga los datos de la  GeneradorTermico genTer que ya existe
	//  y tiene los datos de RecursoBase de ese generador térmico

        // Marca de inicio de los datos específicos de un recurso base.
        String ini = "&DATOS_FALLA";
        // Marca de fin de los datos específicos de un recurso base.
        String fin = "&FIN";

        String dato;
        int i, j;
        ArrayList<Integer> auxI;
        ArrayList<Double> auxD;
		ArrayList<ArrayList<Double>> auxDD;
        boolean encontrado = false;
        boolean noSeReconoceEtiqueta = false;

        // Búsqueda del nombre de la demanda cuya falla se está leyendo.
        i = 0;
        while( !encontrado && (i + 1 <  datAr.size()) ){
            if( ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0){
                if( datAr.get(i+1).size() > 1 && 0 ==
                   nombDem.compareToIgnoreCase(datAr.get(i + 1).get(1)) ) {
                encontrado = true;
                }
            }
            i++;
        }
        if( !encontrado ){
            throw new XcargaDatos("No se encontraron los datos específicos de la demanda: " +
                    nombDem);
        }

        while( i < datAr.size() &&
                fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0 ){
            //Carga la información correspondiente a los datos del recurso base.
            try{


				if( 0==dato.compareToIgnoreCase("nombre")){
                }else if( 0 == dato.compareToIgnoreCase("cant_escalones") ){
                    falla.setCantEscalones(Integer.parseInt(datAr.get(i).get(1)));

// habría que verificar cantidad de pasos de tiempo en las variables siguientes
				}else if( 0 == dato.compareToIgnoreCase("ancho_escalones") ){
					auxDD = new ArrayList<ArrayList<Double>>();
                    for (int il=1; il<=falla.getCantEscalones(); il++){
						i++;
						auxD = new ArrayList<Double>();
						for (j = 0; j < datAr.get(i).size(); j++ ){
							auxD.add(Double.parseDouble(datAr.get(i).get(j)));
						}
						auxDD.add(auxD);
					}
					falla.setAnchoEscalones(auxDD);
				}else if( 0 == dato.compareToIgnoreCase("costo_escalones") ){
					auxDD = new ArrayList<ArrayList<Double>>();
                    for (int il=1; il<=falla.getCantEscalones(); il++){
						i++;
						auxD = new ArrayList<Double>();
						for (j = 0; j < datAr.get(i).size(); j++ ){
							auxD.add(Double.parseDouble(datAr.get(i).get(j)));
						}
						auxDD.add(auxD);
					}
					falla.setCostoEscalones(auxDD);
                }else{
                    noSeReconoceEtiqueta = true;
                }
            }catch(Exception ex){
				System.out.println( ex.toString() );
                throw new XcargaDatos("Falla: " + nombDem +
                        ". No se pudo cargar el dato: " + dato +
                        ". Se generó la excepción: " + ex.toString());
            } // Fin del catch.
            if( noSeReconoceEtiqueta ){
                throw new XcargaDatos("Falla de demanda: " + nombDem +
                        ". No se reconoce la etiqueta: " + dato);
            }
            i++;
        } // Fin de while.

    }



}
