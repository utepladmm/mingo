/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import dominio.ConjRecB;
import dominio.ConjTransB;
import dominio.DatosGeneralesEstudio;
import GrafoEstados.Numerario;
import dominio.Producto;

import dominio.TiempoAbsoluto;
import java.util.ArrayList;


/**
 *
 * @author ut469262
 */
public abstract class CargaDatosGenerales {

     public void cargarDatosGen(String dirArchivo, DatosGeneralesEstudio datosGen) throws XcargaDatos {
          /**
           *
           */
          ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
          // Obtención de todos los datos del archivo.
          datAr = LeerDatosArchivo.getDatos(dirArchivo);
          String ini = "&INICIO_DATOS_GENERALES";
          String fin = "&FIN";
          String dato;
          int i, j;
          ArrayList<Integer> auxI;
          ArrayList<Double> auxD;
          ArrayList<String> auxS;
          int[] arrI = null;
          boolean encontrado = false;
          boolean noSeReconoceEtiqueta = false;
          i = 0;
          while (!encontrado && (i + 1 < datAr.size())) {
               if (ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0) {
                    encontrado = true;
               }
               i++;
          }
          if (!encontrado) {
               throw new XcargaDatos("No se encontraron los datos generales en archivo " +
                       dirArchivo);
          }
          while (i < datAr.size() &&
                  fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0) {
               //Carga la información correspondiente a los datos del recurso base.
               try {
                    if (0 == dato.compareToIgnoreCase("nombre_estudio")) {
                         datosGen.setNombreEstudio(datAr.get(i).get(1));
                    } else if (0 == dato.compareToIgnoreCase("pasos_por_anio")) {
                         datosGen.setPasosPorAnio(Integer.parseInt(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("tasa_desc_paso")) {
                         datosGen.setTasaDescPaso(Double.parseDouble(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("inicio_estudio")) {
                         datosGen.setInicioEstudio(new TiempoAbsoluto(datAr.get(i).get(1)));
                    } else if (0== dato.compareToIgnoreCase("TIPO_CARGO_FIJOS")){
                    	 datosGen.setTipoCargosFijos(datAr.get(i).get(1));
                    } else if (0 == dato.compareToIgnoreCase("cant_pasos_estudio")) {
                         datosGen.setCantPasos(Integer.parseInt(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("cant_pasos_agregados")) {
                         datosGen.setCantPasosAgregados(Integer.parseInt(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("etapas_pasos")) {
                         i++;
                         // se carga en get(0) los datos de la etapa 0 con el paso 0
                         arrI = new int[3];
                         arrI[0] = 0;
                         arrI[1] = 0;
                         arrI[2] = 0;
                         boolean finEtapas = false;
                         datosGen.getEtapasPasos().add(arrI);
                         for (j = 1; j < datAr.get(i).size(); j++) {
                              arrI = new int[3];
                              arrI[0] = Integer.parseInt(datAr.get(i).get(j));
                              arrI[1] = Integer.parseInt(datAr.get(i + 1).get(j));
                              arrI[2] = Integer.parseInt(datAr.get(i + 2).get(j));
                              if(arrI[0]>arrI[1] || arrI[0]>arrI[2] || arrI[1]>arrI[2])
                                  throw new XcargaDatos("Error en la definición de etapa " + j);
                              // [0] paso inicial de la etapa
                              // [1] paso final
                              // [2] paso representativo
                              if(arrI[2]>datosGen.getCantPasos()){
                                  // no hay ninguna etapa que termine en la cantidad de pasos del estudio
                                  throw new XcargaDatos("En la cantidad de pasos del estudio no entra " +
                                          "una cantidad de etapas enteras");
                              }else{
                                  // se debe seguir agregando etapas
                                  datosGen.getEtapasPasos().add(arrI);
                                  if(arrI[2]==datosGen.getCantPasos()){
                                      // se llegó a la última etapa dentro de la cantidad de pasos a estudiar
                                      finEtapas = true;
                                      if (arrI[0] != arrI[1]) {
                                           throw new XcargaDatos("La última etapa de un estudio tiene más de un paso");
                                      }
                                      datosGen.setCantEtapas(j);
                                  }
                              }
                              if (j > 1 & ( arrI[0] != (datosGen.getEtapasPasos().get(j-1)[1]+1)  )) {
                                   throw new XcargaDatos("Error en la definición de etapas. La etapa " + j + "no empieza" +
                                           "en el paso siguiente al paso final de la etapa anterior");
                              }
                              if(finEtapas) break;
                         }


                         i = i + 2;
                         datosGen.setCantEtapas(datosGen.getEtapasPasos().size() - 1);
                    } else if (0 == dato.compareToIgnoreCase("cant_subpasos")) {
                         datosGen.setCantSubpasos(Integer.parseInt(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("cant_postes")) {
                         datosGen.setCantPostes(Integer.parseInt(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("cant_cronicas")) {
                         datosGen.setCantCronicas(Integer.parseInt(datAr.get(i).get(1)));
                    } else if (0 == dato.compareToIgnoreCase("combinatoria_contiguos_simple")) {
                         if(datAr.get(i).get(1).equalsIgnoreCase("SI")){
                             datosGen.setCombContiguosSimple(true);
                         }else{
                             datosGen.setCombContiguosSimple(false);
                         }
                    } else if (0 == dato.compareToIgnoreCase("producto")) {
                         String nom = datAr.get(i).get(1);
                         if (datosGen.devuelveProducto(nom) != null) {
                              throw new XcargaDatos("Hay dos productos de igual nombre " +
                                      nom);
                         } else {
                              if (datAr.get(i).get(2).equalsIgnoreCase("simple")) {
                                   Producto ps = new Producto(nom);
                                   ps.setTipo(datAr.get(i).get(2));
                                   String st = datAr.get(i).get(3);
                                   if(st.equalsIgnoreCase(Producto.MIN)) {                                                                              
                                       ps.setTipo(Producto.MIN);
                                   }else if(st.equalsIgnoreCase(Producto.PROM)){
                                       ps.setTipo(Producto.PROM);                                       
                                   }else{
                                       throw new XcargaDatos("error en la definición del producto" + nom);
                                   }                                   
                                   ps.setDiaIni(Integer.parseInt(datAr.get(i).get(4)));
                                   ps.setDiaFin(Integer.parseInt(datAr.get(i).get(5)));                                   
                                   datosGen.agregarProducto(ps);                                   
                              }
                         }
                    } else if (0 == dato.compareToIgnoreCase("numerario")) {
                         String nom = datAr.get(i).get(1);
                         if (datosGen.devuelveNumerario(nom) != null) {
                              throw new XcargaDatos("Hay dos numerarios de igual nombre " +
                                      nom);
                         } else {
                              Numerario nu = new Numerario(nom);
                              datosGen.agregarNumerario(nu);
                         }
                    } else if (0 == dato.compareToIgnoreCase("tasas_numerarios")) {
                         for (j = 1; j < datAr.get(i).size(); j++) {
                              datosGen.getNumerarios().get(j - 1).setTasaDescuento((Double.parseDouble(datAr.get(i).get(j))));
                         }
                    } else if (0 == dato.compareToIgnoreCase("descripcion_numerarios")) {
                         auxS = new ArrayList<String>();
                         if (datAr.get(i).size() != datosGen.getNumerarios().size()) {
                              throw new XcargaDatos("No coinciden los nombres y descripciones de numerarios ");
                         } else {
                              for (j = 1; j < datAr.get(i).size(); j++) {
                                   datosGen.getNumerarios().get(j).setDescripcion(datAr.get(i).get(j));
                              }
                         }
                    } else if (0 == dato.compareToIgnoreCase("maximo_per_const")) {
                         for (j = 1; j < datAr.get(i).size(); j++) {
                              datosGen.getMaximoPerConst().add(Integer.parseInt(datAr.get(i).get(j)));
                         }
                    } else {
                         noSeReconoceEtiqueta = true;
                    }
               } catch (Exception ex) {
                    System.out.println(ex.toString());
                    //throw new XcargaDatos("Error en la lectura de datos generales");
               } // Fin del catch.
               if (noSeReconoceEtiqueta) {
                    throw new XcargaDatos("No se reconoce la etiqueta en archivo de datos generales: " + dato);
               }
               i++;
          } // Fin de while.
          datosGen.cargaTiemposAbsolutosPasos();
          datosGen.cargaFinEstudio();
     }

     public static void main(String[] args) {
          ConjRecB c1RB = new ConjRecB();
          ConjTransB c1TB = new ConjTransB();
          DatosGeneralesEstudio dGE = new DatosGeneralesEstudio();
          String texto = "";

          CargaRecBase cargadorRB = new CargaRecBase() {
          };
          CargaTransBase cargadorTB = new CargaTransBase() {
          };
          CargaDatosGenerales cargadorDGE = new CargaDatosGenerales() {
          };
          CargaExpaForzada cargadorCED = new CargaExpaForzada() {
          };
//		CargaDatosTermico carTer = new CargaDatosTermico() {};
          try {

               String dirArchivo = "D:/Java/PruebaJava2/V4-DatosGenerales.txt";
               cargadorDGE.cargarDatosGen(dirArchivo, dGE);
               texto = dGE.toString();
               System.out.println(texto + "\n");

          } catch (XcargaDatos ex) {
               System.out.println("------ERROR-------");
               System.out.println(ex.getDescripcion());
               System.out.println("------------------\n");
          }
     }
}

