package persistencia;

import java.util.ArrayList;

import dominio.Acumulador;
import dominio.ListaReales;

public class CargaDatosAcum {
	
    public void cargar(ArrayList<ArrayList<String>> datAr, Acumulador acum) throws XcargaDatos{


        // Marca de inicio de los datos específicos de un recurso base.
        String ini = "&DATOS_ESPECIFICOS";
        // Marca de fin de los datos específicos de un recurso base.
        String fin = "&FIN";

        String dato;
        int i, j;
        ArrayList<Integer> auxI;
        ArrayList<Double> auxD;
        ListaReales auxLR;
        boolean encontrado = false;
        boolean noSeReconoceEtiqueta = false;

        // Búsqueda del nombre del recurso base a cargar.
        i = 0;
        while( !encontrado && (i + 1 <  datAr.size()) ){
            if( ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0){
                if( datAr.get(i+1).size() > 1 && 0 ==
                   acum.getNombre().compareToIgnoreCase(datAr.get(i + 1).get(1)) ) {
                encontrado = true;
                }
            }
            i++;
        }
        if( !encontrado ){
            throw new XcargaDatos("No se encontraron los datos específicos del recurso: " +
                    acum.getNombre());
        }

        while( i < datAr.size() &&
                fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0 ){
            //Carga la información correspondiente a los datos del recurso base.
            try{


				if( 0==dato.compareToIgnoreCase("nombre")){
				}else if( 0 == dato.compareToIgnoreCase("datos_unitarios") ){
					acum.setRef((datAr.get(i).get(1)));
				}else{
                    noSeReconoceEtiqueta = true;
                }
            }catch(Exception ex){
				System.out.println( ex.toString() );
                throw new XcargaDatos("Acumulador: " + acum.getNombre() +
                        ". No se pudo cargar el dato: " + dato +
                        ". Se gener� la excepci�n: " + ex.toString());
            } // Fin del catch.
            if( noSeReconoceEtiqueta ){
                throw new XcargaDatos("Recurso: " + acum.getNombre() +
                        ". No se reconoce la etiqueta: " + dato);
            }
            i++;
        } // Fin de while.

    }

}
