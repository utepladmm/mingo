/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import dominio.Caso;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class CargaSensibilidad {

    /**
     * Carga los datos de sensibilidad para mostrar portafolios próximos al óptimo
     * @param dirArchivo archivo del que se leen los datos
     * @param caso para el que se leen los datos
     * @throws XcargaDatos
     */
    public static void cargarSens(String dirArchivo, Caso caso) throws XcargaDatos {
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        LeerDatosArchivo cargador = new LeerDatosArchivo() {
        };
        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        // Marca de inicio de los datos de un Objetivo.
        String ini = "&SENSIBILIDAD";

        String dato;
        int i, j;
        boolean noSeReconoceEtiqueta = false;
        boolean encontrado = false;

        i = 0;
        // llega a la linea inicial
        do {
            dato = datAr.get(i).get(0);
            if (dato.equalsIgnoreCase(ini)) {
                encontrado = true;
            }
            i++;
        } while (!encontrado & i < datAr.size());

        if (!encontrado) {
            throw new XcargaDatos("No se encontró sensibilidad del caso " + caso.getNombre());
        }


        try {
            dato = datAr.get(i).get(0);
            if (0 == dato.compareToIgnoreCase("TIPO"))  {
                if(datAr.get(i).get(1).equalsIgnoreCase("FRACCION")){
                    caso.setTipoSensib(Caso.FRAC);
                }else if(datAr.get(i).get(1).equalsIgnoreCase("VALOR_ABSOLUTO")){
                    caso.setTipoSensib(Caso.VABS);
                }else{
                    throw new XcargaDatos("Error en lectura de datos sensibilidad: TIPO");
                }
            }
            i++;
            dato = datAr.get(i).get(0);
            if (0 == dato.compareToIgnoreCase("VALOR"))  {
                    caso.setValorSensib(Double.parseDouble(datAr.get(i).get(1)));
                }else{
                    throw new XcargaDatos("Error en lectura de datos sensibilidad: VALOR");
                }
            i++;
            dato = datAr.get(i).get(0);
            if (0 == dato.compareToIgnoreCase("PERCENTILES"))  {
                for (j=1; j<datAr.get(i).size();j++){
                    caso.getPercentiles().add(Double.parseDouble(datAr.get(i).get(j)));
                }
            }
        }
        catch (Exception ex) {
            throw new XcargaDatos("No se pudo cargar el objetivo, excepción: " + ex.toString());
        } // Fin del catch.
    }
}
