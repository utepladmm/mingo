/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistencia;

import GrafoEstados.ConjRestGrafoE;
import Restricciones3.RestDeProducto3;
import Restricciones3.RestDeProducto3.InfSup;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import dominio.Producto;
import dominio.TiempoAbsoluto;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class CargaRestDeProducto3 {

	    public void cargarRestDeProducto(String dirArchivo,
		Estudio estudio,
		ConjRestGrafoE conRP) throws XcargaDatos {
		// Carga la totalidad de las restricciones 
		// de producto
        System.out.print("Se leen restricciones de producto en   " + dirArchivo +"\n");
        DatosGeneralesEstudio datGen = estudio.getDatosGenerales();
        // Datos de todo el archivo.
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();

        // Obtención de todos los datos del archivo.
        LeerDatosArchivo cargador = new LeerDatosArchivo() {};
        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        // Carga de la lista de recursos a cargar.
        // Carga de los recursos.
        for(Producto pr: datGen.getProductos()){
			cargarRestUnProd(datAr, pr, conRP, estudio);
        }
    }

    /**
     * Carga los datos de las restricciones de producto de un Producto.
     */
    private void cargarRestUnProd( ArrayList<ArrayList<String>> datAr,
            Producto pr, ConjRestGrafoE conRP,
			Estudio estudio) throws XcargaDatos{

        // Marca de inicio de los datos de un producto.

		String ini;
		ini = "&PRODUCTO";
		String nomb;
		nomb = pr.getNombre();
		RestDeProducto3 resP;

		
        // Marca de fin de los datos de una restricción de producto.
        String fin = "&FIN";

        String dato;
        int i, j;
        ArrayList<Integer> auxI;
        ArrayList<Double> auxD;
		ArrayList<String> auxS;

        boolean encontrado = false;
        boolean encontro1 = false;
        boolean noSeReconoceEtiqueta = false;

        i = 0;
        encontrado = false;
        encontro1 = false;
        while( i+1 < datAr.size() ){
          
            // Búsqueda del nombre del producto

            while( !encontro1 && (i + 1 <  datAr.size()) ){
                if( ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0){
                    if( datAr.get(i+1).size() > 1 && 0 ==
                       nomb.compareToIgnoreCase(datAr.get(i + 1).get(1)) ) {
                        encontrado = true;
                        encontro1 = true;
                    }
                }
                i++;
            }
            if(encontro1){
                // encontró una restricción
                resP = new RestDeProducto3(pr, estudio, true);
                encontro1 = false;
                while( i < datAr.size() &&
                        fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0 ){
                    //Carga la información correspondiente a los datos de la restricción.
                    try{
                        if (0 == dato.compareToIgnoreCase("nombre_rest")) {
                             String nombRest = "";
                             for(j=1; j<datAr.get(i).size(); j++){
                                  nombRest += datAr.get(i).get(j);
                                  if (j<datAr.get(i).size()-1) nombRest = nombRest + " ";
                             }
                             resP.setNombre(nombRest);
                        }else if( 0 == dato.compareToIgnoreCase("tiempo_absoluto") ){
                           TiempoAbsoluto ta = new TiempoAbsoluto(datAr.get(i).get(1));
                           resP.setTiempoAbsolutoBase(ta);
                        }else if( 0 == dato.compareToIgnoreCase("cota") ){
                           if(datAr.get(i).get(1).equalsIgnoreCase("inferior")){
                               resP.setInfSup(InfSup.INF);
                           }else{
                               resP.setInfSup(InfSup.SUP);
                           }
                        }else if( 0 == dato.compareToIgnoreCase("proporcion") ){
                           auxD = new ArrayList<Double>();
                           for(j = 1; j < datAr.get(i).size(); j++){
                               auxD.add(Double.parseDouble((datAr.get(i).get(j))));
                           }
                           resP.setProporcion(auxD);
                        }else if(0 == dato.compareToIgnoreCase("es_heuristica") ){
                           String s = datAr.get(i).get(1);
                           if(s.equalsIgnoreCase("SI")){
                               resP.setEsHeuristica(true);
                           }else if(s.equalsIgnoreCase("NO")){
                               resP.setEsHeuristica(false);
                           }else{
                               throw new XcargaDatos("La restricción no especifica SI o NO en Heurística");
                           }

                        }

                    }catch(Exception ex){
                        System.out.println( ex.toString() );
                        throw new XcargaDatos("Recurso o tranformación: " + nomb +
                                ". No se pudo cargar el dato: " + dato +
                                ". Se generó la excepción: " + ex.toString());
                    } // Fin del catch.
                    if( noSeReconoceEtiqueta ){
                        throw new XcargaDatos("Recurso o transformacion: " + nomb +
                                ". No se reconoce la etiqueta: " + dato);
                    }
                    i++;
                } // Fin de while
                conRP.getRestricciones().add(resP);
            } // Fin de if.

        }  // Fin de while buscando restricciones
        if(!encontrado) System.out.println("No se encontraron restricciones de producto del Producto " + pr.getNombre());
	}
}
