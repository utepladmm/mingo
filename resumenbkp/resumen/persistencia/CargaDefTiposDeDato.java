/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistencia;

import dominio.ConjTiposDeDato;
import dominio.TipoDeDatos;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class CargaDefTiposDeDato {

    public static void cargarTiposDato(String dirArchivo,
        ConjTiposDeDato conjTD) throws XcargaDatos{
        
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        // Obtención de todos los datos del archivo.
        LeerDatosArchivo cargador = new LeerDatosArchivo() {};
        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        int i;


        i = 0;
        while( i < datAr.size() ){
            String dato = datAr.get(i).get(0);
            TipoDeDatos tipo = new TipoDeDatos();
            try{
                if( 0 == dato.compareToIgnoreCase("nombre") ){
                    tipo.setNombre(datAr.get(i).get(1));
                    String auxS = "";
                    // junta para la dirección los substrings separados por blancos
                    for(int j=2; j<datAr.get(i).size(); j++){
                        auxS += datAr.get(i).get(j);
                    }
                    tipo.setDireccion(auxS);
                }else{
                    throw new XcargaDatos("Error en lectura de definición de TipoDeDatos. No aparece la etiqueta nombre");
                }
            }catch(Exception ex){
                    throw new XcargaDatos(ex.toString());
            }
            conjTD.getTipos().add(tipo);
            i++;
        } // Fin de while.
    }
}
