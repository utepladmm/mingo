/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistencia;

import dominio.Combustible;
import dominio.ConjFuentes;
import dominio.Fuente;
import dominio.DispAnioSemana;
import dominio.Fuente.TipoFuente;
import dominio.TiempoAbsoluto;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class CargaFuentes {

        public void cargarConjFuentes(String dirArchivo, ConjFuentes conjF) throws XcargaDatos{

        // Datos de todo el archivo.
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        // Datos del recurso base a cargar.
        Fuente fuente;
        // Lista de los recursos base a cargar.
        ArrayList<String> listaFuentes = new ArrayList<String>();

        // Obtención de todos los datos del archivo.
        LeerDatosArchivo cargador = new LeerDatosArchivo() {};
        datAr = cargador.getDatos(dirArchivo);
        // Carga de la lista de recursos a cargar.
        listaFuentes = cargarListFuentes(datAr);
        // Carga de los recursos.
        for(String nombF: listaFuentes){
            cargarFuente(datAr, conjF, nombF);
        }
    }


    private ArrayList<String> cargarListFuentes(ArrayList<ArrayList<String>> datAr)
            throws XcargaDatos{
        ArrayList<String> resultado = new ArrayList<String>();
        String iniLista = "&LISTA_FUENTES"; // Marca de lista de
                                                     // fuentes a cargar.
        String finLista = "&FIN"; // Marca de fin de la lista de fuentes
                                 // base a cargar.
        boolean encontrado;
        int i, j;

        // Búsqueda de la lista de recursos a cargar.
        encontrado = false;
        i = 0;
        while( !encontrado && (i <  datAr.size()) ){
            if( iniLista.compareToIgnoreCase(datAr.get(i).get(0)) == 0 ) {
                encontrado = true;
            }
            i++;
        }
        // Si se encontró la marca de inicio de la lista y
        // no sigue la marca de fin de la lista, se carga la lista de fuentes.
        if( encontrado && (i <  datAr.size()) &&
                finLista.compareToIgnoreCase(datAr.get(i).get(0)) != 0 ){
            for(j = 0; j < datAr.get(i).size(); j++){
                resultado.add(datAr.get(i).get(j));
            }
        }else{
            throw new XcargaDatos("No se encontró la lista de fuentes.");
        }

        return resultado;
    }


    private void cargarFuente(ArrayList<ArrayList<String>> datAr,
            ConjFuentes conjF, String nombFuen) throws XcargaDatos{
        // Marca de inicio de los datos de un recurso base.
        String ini = "&FUENTE";
        // Marca de fin de los datos de un recurso base.
        String fin = "&FIN";
        String dato;

        Fuente fuen = new Fuente();
        int i, j;
        ArrayList<Integer> auxI;
        ArrayList<Double> auxD;

        boolean encontrado = false;
        boolean noSeReconoceEtiqueta = false;

        // Búsqueda del nombre del recurso a cargar.
        i = 0;
        while( !encontrado && (i + 1 <  datAr.size()) ){
            if( ini.compareToIgnoreCase(datAr.get(i).get(0)) == 0){
                if( datAr.get(i+1).size() > 1 && 0 ==
                   nombFuen.compareToIgnoreCase(datAr.get(i + 1).get(1)) ) {
                encontrado = true;
                }
            }
            i++;
        }
        if( !encontrado ){
            throw new XcargaDatos("No se encontraron los datos de la fuente: " +
                    nombFuen);
        }

        while( i < datAr.size() &&
                fin.compareToIgnoreCase(dato = datAr.get(i).get(0)) != 0 ){


            try{
                if( 0 == dato.compareToIgnoreCase("nombre") ){
                    fuen.setNombre(datAr.get(i).get(1));
				}else if(0 == dato.compareToIgnoreCase("tipo")){
					fuen.setTipo(Fuente.tipoDeFuente(datAr.get(i).get(1)));
                }else if(0 == dato.compareToIgnoreCase("requiere_inyector")){
					fuen.setRequiereInyector( (datAr.get(i).get(1)).equalsIgnoreCase("SI")?
                                                true: false);
                /**
                 * Alternativamente su puede leer un DispAnioSemana o un TiempoAbsoluto
                 */
                }else if( 0 == dato.compareToIgnoreCase("aninidisp") ){
					DispAnioSemana das = new DispAnioSemana();
					das.setAninidisp(Integer.parseInt(datAr.get(i).get(1)));
					i++;
					if(0 == "anfindisp".compareToIgnoreCase(datAr.get(i).get(0))){
						das.setAnfindisp(Integer.parseInt(datAr.get(i).get(1)));
					}else{
						throw new XcargaDatos("error en anfindisp, fuente: " + fuen.getNombre());
					}
					i++;
					if(0 == "seminidisp".compareToIgnoreCase(datAr.get(i).get(0))){
						auxI = new ArrayList<Integer>();
						for (j=1; j<datAr.get(i).size(); j++ ){
							auxI.add(Integer.parseInt(datAr.get(i).get(j)));
						}
						das.setSeminidisp(auxI);
					}else{
						throw new XcargaDatos("error en seminidisp, fuente: " + fuen.getNombre());
					}
					i++;
					if(0 == "semfindisp".compareToIgnoreCase(datAr.get(i).get(0))){
						auxI = new ArrayList<Integer>();
						for (j=1; j<datAr.get(i).size(); j++ ){
							auxI.add(Integer.parseInt(datAr.get(i).get(j)));
						}
						das.setSemfindisp(auxI);
					}else{
						throw new XcargaDatos("error en seminidisp, fuente: " + fuen.getNombre());
					}
				fuen.getDisponibilidad().add(das);
                }else if(0 == dato.compareToIgnoreCase("inidisp")){
                        fuen.setIniDisp(new TiempoAbsoluto(datAr.get(i).get(1)));
                }else if(0 == dato.compareToIgnoreCase("findisp")){
                        fuen.setFinDisp(new TiempoAbsoluto(datAr.get(i).get(1)));
                }else if(0 == dato.compareToIgnoreCase("tiempo_absoluto_base")){
                        fuen.setTiempoAbsolutoBase(new TiempoAbsoluto(datAr.get(i).get(1)));
                }else if( 0 == dato.compareToIgnoreCase("potencia_maxima") ){
                    fuen.setPotMax(Double.parseDouble(datAr.get(i).get(1) ));
                }else if( 0 == dato.compareToIgnoreCase("energia_maxima_anual") ){
                    fuen.setEnergMaxPaso(Double.parseDouble(datAr.get(i).get(1) ));
                }else{
                    noSeReconoceEtiqueta = true;
                }
            }catch(Exception ex){
                throw new XcargaDatos("Fuente: " + fuen +
                        ". No se pudo cargar el dato: " + dato +
                        ". Se generó la excepción: " + ex.toString());
            } // Fin del catch.
            if( noSeReconoceEtiqueta ){
                throw new XcargaDatos("Fuente: " + fuen +
                        ". No se reconoce la etiqueta: " + dato);
            }
            i++;
        } // Fin de while.

		if (fuen.getTipo()==TipoFuente.COMBUS){
            Combustible comb = new Combustible();
            comb.cargaFuente(fuen);
            conjF.agregar(comb);
        }
    }
}
