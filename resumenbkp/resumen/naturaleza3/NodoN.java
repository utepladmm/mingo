/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaleza3;

import dominio.Caso;
import dominio.Estudio;
import dominio.ParTipoDatoValor;
import dominio.TipoDeDatos;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.util.ArrayList;
import persistencia.XcargaDatos;

/**
 *
 * @author Usuario
 */
public class NodoN implements java.io.Serializable {

     private GrafoN grafoN;  // el grafo el que pertenece el nodo
     private int tiempo;		// paso de tiempo en el que existe el nodo
     // OJO ES PASO DE TIEMPO Y NO ETAPA
     private int ordinal;	// ordinal dentro de los nodos del mismo paso de tiempo
     private ConjNodosN conjInfo;
     /**
      * conjunto de información al que pertenece el nodo
      **/
     private ConjNodosN conjDetParque;
     /**
      * conjunto que determina parque al que pertenece el nodo
      **/
     private double probabilidad;
     private double probCondDadoCI;
     /**
      * probabilidad condicional del nodo dado el CI al que pertenece
      */
     private double probCondDadoCDP;
     /**
      * probabilidad condicional del nodo dado el CDP al que pertenece
      */
     private ArrayList<String> valoresVN;
     private ArrayList<ValorVN> valoresVN2;
     /**
      * ojo habría que usar esto y no el orden en el grafo como valoresVN
      * pero por ahora se usa valoresVN;
      */

     private ArrayList<ParTipoDatoValor> valoresTiposDatos;
     /**
      * Cuando se aplica un GrafoN a un Estudio, es la lista ordenada de valores del
      * NodoN datos los TipoDeDatos del Estudio.
      * Algunos de los tipos de datos tienen asociada una variable aleatoria
      * y en valoresTiposDatos se coloca su valor simbólico en este nodo this.
      * Los otros tipos de datos tienen valores fijos que también se cargan en
      * valoresTiposDatos de este nodo.
      */
     private ArrayList<NodoN> sucesores;
     private ArrayList<Double> probSucesores; // probabilidad de cada sucesor; deben sumar 1

     public NodoN(int tiempo, Double probabilidad, ArrayList<String> valoresVN, ArrayList<NodoN> sucesores, ArrayList<Double> probSucesores) {
          this.tiempo = tiempo;
          this.probabilidad = probabilidad;
          this.valoresVN = valoresVN;
          this.sucesores = sucesores;
          this.probSucesores = probSucesores;
     }

     public NodoN(int tiempo, int ordinal, ArrayList<String> valoresVN) {
          this.tiempo = tiempo;
          this.ordinal = ordinal;
          this.probabilidad = 0.0;
          this.valoresVN = valoresVN;
          conjInfo = null;
          conjDetParque = null;
          this.sucesores = new ArrayList<NodoN>();
          this.probSucesores = new ArrayList<Double>();
     }

     public NodoN(int tiempo, int ordinal) {
          this.tiempo = tiempo;
          this.ordinal = ordinal;
          this.probabilidad = 0.0;
          this.valoresVN = new ArrayList<String>();
          conjInfo = null;
          conjDetParque = null;
          this.sucesores = new ArrayList<NodoN>();
          this.probSucesores = new ArrayList<Double>();
     }

     public NodoN() {
          tiempo = 0;
          ordinal = 0;
          probabilidad = 0.0;
          valoresVN = new ArrayList<String>();
          sucesores = new ArrayList<NodoN>();
          probSucesores = new ArrayList<Double>();


     }

     public GrafoN getGrafoN() {
          return grafoN;
     }

     public void setGrafoN(GrafoN grafoN) {
          this.grafoN = grafoN;
     }

     public int getOrdinal() {
          return ordinal;
     }

     public void setOrdinal(int ordinal) {
          this.ordinal = ordinal;
     }

     public ConjNodosN getConjInfo() {
          return conjInfo;
     }

     public void setConjInfo(ConjNodosN conjInfo) {
          this.conjInfo = conjInfo;
     }

     public ConjNodosN getConjDetParque() {
          return conjDetParque;
     }

     public void setConjDetParque(ConjNodosN conjDetParque) {
          this.conjDetParque = conjDetParque;
     }

     public Double getProbCondDadoCDP() {
          return probCondDadoCDP;
     }

     public void setProbCondDadoCDP(Double probCondDadoCDP) {
          this.probCondDadoCDP = probCondDadoCDP;
     }

     public ArrayList<NodoN> getSucesores() {
          return sucesores;
     }

     public void setSucesores(ArrayList<NodoN> sucesores) {
          this.sucesores = sucesores;
     }

     public ArrayList<Double> getProbSucesores() {
          return probSucesores;
     }

     public void setProbSucesores(ArrayList<Double> probSucesores) {
          this.probSucesores = probSucesores;
     }

     public int getTiempo() {
          return tiempo;
     }

     public void setTiempo(int tiempo) {
          this.tiempo = tiempo;
     }

     public Double getProbabilidad() {
          return probabilidad;
     }

     public void setProbabilidad(Double probabilidad) {
          this.probabilidad = probabilidad;
     }

     public Double getProbCondDadoCI() {
          return probCondDadoCI;
     }

     public void setProbCondDadoCI(Double probCondDadoCI) {
          this.probCondDadoCI = probCondDadoCI;
     }

     public ArrayList<String> getValoresVN() {
          return valoresVN;
     }

     public void setValoresVN(ArrayList<String> valoresVN) {
          this.valoresVN = valoresVN;
     }

     public ArrayList<ValorVN> getValoresVN2() {
          return valoresVN2;
     }

     public void setValoresVN2(ArrayList<ValorVN> valoresVN2) {
          this.valoresVN2 = valoresVN2;
     }

     public ArrayList<ParTipoDatoValor> getValoresTiposDatos() {
          return valoresTiposDatos;
     }

     public void setValoresTiposDatos(ArrayList<ParTipoDatoValor> valoresTiposDatos) {
          this.valoresTiposDatos = valoresTiposDatos;
     }

     public NodoN copiaParcial() {
          // crea un nuevo nodo copia del corriente, sin incluir sucesores
          NodoN n = new NodoN(tiempo, ordinal);
          int i = 0;
          for (String valVN : valoresVN) {
               n.getValoresVN().add(valVN);
          }
          n.setGrafoN(grafoN);
          n.setConjInfo(conjInfo);
          n.setConjDetParque(conjDetParque);
          n.setProbabilidad(probabilidad);
          n.setProbCondDadoCI(probCondDadoCI);
          n.setProbCondDadoCDP(probCondDadoCDP);
          return n;
     }

     /**
      * METODO getValoresObservadas
      *
      * Crea una lista de Strings con los valores de las variables observadas
      * en el nodo
      * Queda a responsabilidad del usuario pasar el grafoN correcto al que
      * pertenece el nodo
      *
      * @param grafoN el grafo de la naturaleza al que pertenece el nodo
      *
      * @return valores son los valores de las variables
      */
     public ArrayList<String> getValoresObservadas(GrafoN grafoN) {

          ArrayList<String> valores = new ArrayList<String>();
          int iv = 0;
          for (VariableNat vN : grafoN.getVarGrafo()) {
               if (vN.getObservadaEnOptim()) {
                    valores.add(valoresVN.get(iv));
               }
               iv++;
          }
          return valores;

     }

     /**
      *  Crea una lista de ValorVN con de las variables observadas
      *  en el nodo
      */
     public ArrayList<ValorVN> getValoresObservadas2() {
          ArrayList<ValorVN> auxVVN = new ArrayList<ValorVN>();
          for (ValorVN vVN : valoresVN2) {
               if (vVN.getVarN().getObservadaEnOptim()) {
                    auxVVN.add(vVN);
               }
          }
          return auxVVN;
     }

     /**
      * Devuelve el valor de una VN dada
      */
     public String valorDeUnaVN(VariableNat varN) throws XcargaDatos {

          String valor = "";
          int indiceVN = grafoN.getVarGrafo().indexOf(varN);
          if (indiceVN < 0) {
               throw new XcargaDatos("No existe la variable de la naturaleza" +
                       varN.toString());
          }
          valor = valoresVN.get(indiceVN);
          return valor;
     }

     /**
      * Devuelve el valor de una VN dada
      */
     public String valorDeUnaVN2(VariableNat varN) throws XcargaDatos {
          /**
           * Devuelve el valor de una VN dada
           */
          String valor = "";
          boolean encontro = false;
          for (ValorVN vVN : valoresVN2) {
               if (vVN.getVarN() == varN) {
                    encontro = true;
                    valor = vVN.getValorVN();
                    break;
               }
          }
          if (!encontro) {
               throw new XcargaDatos("No encontró la variable" +
                       varN.toString() + " en el nodo etapa = " + tiempo + "ordinal" +
                       ordinal);
          }
          return valor;
     }

     /**
      * Devuelve el valor de la variable de nombre nombreVarN
      */
     public String valorDeUnaVN(String nombreVarN) throws XcargaDatos {

          String valor = null;
          int indiceVN = -1;
          for (int iv = 0; iv < grafoN.getVarGrafo().size(); iv++) {
               if (grafoN.getVarGrafo().get(iv).getNombre().equalsIgnoreCase(nombreVarN)) {
                    valor = valoresVN.get(iv);
                    indiceVN = iv;
                    break;
               }
          }
          if (indiceVN < 0) {
               throw new XcargaDatos("No existe la variable de la naturaleza" +
                       nombreVarN);
          }
          return valor;
     }

     /**
      * Devuelve el valor de la variable de nombre nombreVarN
      */
     public String valorDeUnaVN2(String nombreVarN) throws XcargaDatos {

          String valor = "";
          boolean encontro = false;
          for (ValorVN vVN : valoresVN2) {
               if (vVN.getVarN().getNombre().equalsIgnoreCase(nombreVarN)) {
                    encontro = true;
                    valor = vVN.getValorVN();
                    break;
               }
          }
          if (!encontro) {
               throw new XcargaDatos("No encontró la variable " +
                       "de nombre " + nombreVarN + " en el nodo etapa = " + tiempo + "ordinal " + ordinal);
          }
          return valor;
     }

     /**
      * Devuelve la probabilidad de uno de los nodos sucesores
      * Si suc no es nodo sucesor de this da error
      */
     public double probDeUnSucesor(NodoN suc) {

          double result = 0.0;
          int indice = sucesores.indexOf(suc);
          assert (indice >= 0) : "ERROR: El nodo " + this.toStringCorto() +
                  "no tiene como sucesor al nodo " + suc.toStringCorto();
          result = probSucesores.get(indice);
          return result;
     }

     /**
      * Cuando se aplica un GrafoN a un Estudio y Caso dado , el método permite cargar
      * los valoresTiposDatos del NodoN this asociados el estudio, tanto los fijos como
      * los aleatorios, y en el orden adecuado.
      */
     public void cargaValoresTiposDatos(Estudio est, Caso caso) throws XcargaDatos {
          ArrayList<TipoDeDatos> tiposDEst = est.getConjTiposDatos().getTipos();
          valoresTiposDatos = new ArrayList<ParTipoDatoValor>();
          for (TipoDeDatos tD : tiposDEst) {
               VariableNat vN = tD.getVNDelDato();
               ParTipoDatoValor parTDV = new ParTipoDatoValor();
               parTDV.setTipoDatos(tD);
               if (tD.esAleat()) {
                    parTDV.setValor(valorDeUnaVN(vN));
               } else {
                    parTDV.setValor(caso.devuelveValorTDFijo(tD));
               }
               valoresTiposDatos.add(parTDV);
          }
     }

     @Override
     public String toString() {

          String texto = "COMIENZA nodoN: " + "\r\n";
          texto += "Tiempo: " + tiempo + " - ";
          texto += "Ordinal del nodo: " + ordinal + "\r\n";
          texto += "Probabilidad del nodo: " + probabilidad + "\r\n";
          texto += "Probabilidad condicional dado el CI: " + probCondDadoCI + "\r\n";
          texto += "Probabilidad condicional dado el CDP: " + probCondDadoCDP + "\r\n";
          texto += "Valores de variables de la naturaleza:  ";
          for (String s : valoresVN) {
               texto += s + "  ";
          }
          texto += "\r\n";
          for (int i = 0; i < sucesores.size(); i++) {
               texto += "sucesor-" + i + "(t=" + sucesores.get(i).getTiempo() + ", ord=" +
                       sucesores.get(i).getOrdinal() + ")" + " - probabilidad de pasaje= " + probSucesores.get(i) + "\r\n";
          }
          return texto;
     }

     public String toStringCorto() {
          // no imprime los sucesores del nodo
          String texto = "COMIENZA nodoN: " + "\r\n";
          texto += "Tiempo: " + tiempo + " - ";
          texto += "Ordinal del nodo: " + ordinal + "\r\n";
          texto += "Probabilidad del nodo: " + probabilidad + "\r\n";
          texto += "Probabilidad condicional dado el CI: " + probCondDadoCI + "\r\n";
          texto += "Probabilidad condicional dado el CDP: " + probCondDadoCDP + "\r\n";
          texto += "Valores de variables de la naturaleza:  ";
          for (String s : valoresVN) {
               texto += s + "  ";
          }
          texto += "\r\n";
          texto += "Ordinales de sucesores en etapa siguiente :";
          texto += "\r\n";
          int is = 0;
          for (is = 0; is<sucesores.size(); is++) {
               texto += "( Ordinal: " + sucesores.get(is).getOrdinal() + " - Probabilidad de transición: " + probSucesores.get(is) +")";
               texto += "\r\n";
          }          
          return texto;
     }

     public static void main(String[] args) throws ClassNotFoundException {

          NodoN s1 = new NodoN(2, 1);
          NodoN s2 = new NodoN(2, 2);
          ArrayList<NodoN> suc = new ArrayList<NodoN>();
          suc.add(s1);
          suc.add(s2);
          NodoN n1 = new NodoN(1, 2);
          System.out.print(n1.toString());
          try {

               FileOutputStream fos = new FileOutputStream("t.tmp");
               ObjectOutputStream oos = new ObjectOutputStream(fos);

               oos.writeObject(n1);
               oos.close();

          } catch (ObjectStreamException e1) {
               System.out.println(e1.toString());
          } catch (FileNotFoundException e2) {
               System.out.println(e2.toString());
          } catch (IOException e3) {
               System.out.println(e3.toString());
          }
          NodoN n2 = new NodoN();

          try {

               FileInputStream fis = new FileInputStream("t.tmp");
               ObjectInputStream ois = new ObjectInputStream(fis);

               n2 = (NodoN) ois.readObject();
               ois.close();

          } catch (ObjectStreamException e1) {
               System.out.println(e1.toString());
          } catch (FileNotFoundException e2) {
               System.out.println(e2.toString());
          } catch (IOException e3) {
               System.out.println(e3.toString());
          }

          System.out.print(n2.toString());



     }
}
