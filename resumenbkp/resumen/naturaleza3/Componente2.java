/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package naturaleza3;

import java.io.Serializable;
import java.util.ArrayList;


/**
 *
 * @author ut600232
 */
public class Componente2 implements Serializable{

    private String nombre;
	private TipoComp tipo;	// independiente o dependiente
	private ArrayList<String> variables;

	/**
	 * observada:  Si es observada en la optimización o no la componente
	 * (todas las variables VN de ella)
	 **/
	private boolean observada;
	/**
     * True si la componente (todas sus VN) son observadas al decidir
     *
	 * Si la componente es no observada y tiene una variable que cambia
	 * parque, (debe ser una componente independiente y estar sóla la variable
	 * en la componente)
	 */
	private boolean cambiaParque;
	
	/* Para cada variable en variables se carga la lista de valores
	 * que puede tomar. */	
	private ArrayList<ArrayList<String>> valoresDe;

	/* Nombres de las variables de cuyos valores dependen
	 las variables de esta componente. */
	private ArrayList<String> dependenDe;

	/* Tabla de la función tal como aparece en el archivo. */
	private ArrayList<ArrayList<String>> funcion;

	/* Conjunto de nodos de la componente. */
	private ArrayList<NodoN> nodos;

	/* Conjunto de arcos de la componente. */
	private ArrayList<Arco> arcos;


    public Componente2() {
		variables = new ArrayList<String>();
		valoresDe = new ArrayList<ArrayList<String>> ();
		dependenDe = new ArrayList<String> ();
		nodos = new ArrayList<NodoN>();
		arcos = new ArrayList<Arco>();
		dependenDe = new ArrayList<String>();
		funcion = new ArrayList<ArrayList<String>>();
        nombre = "";
		cambiaParque = false;
		observada = false;
    }
    
    public Componente2(String nombre) {
		variables = new ArrayList<String>();
		valoresDe = new ArrayList<ArrayList<String>> ();
		dependenDe = new ArrayList<String> ();
		nodos = new ArrayList<NodoN>();
        arcos = new ArrayList<Arco>();
		dependenDe = new ArrayList<String>();
		funcion = new ArrayList<ArrayList<String>>();
        this.nombre = nombre;
		cambiaParque = false;
		observada = false;
    }

	public enum TipoComp{
		Indep,	// Independiente
		Dep;	// Dependiente
	}

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

	public void setTipo(TipoComp tipo) {
		this.tipo = tipo;
	}

	public void setVariables(ArrayList<String> variables) {
		this.variables = variables;
	}

	public void addVariable(String var) {
		variables.add(var);
	}

	public void setObservada(boolean observada) {
		this.observada = observada;
	}



	public void setCambiaParque(boolean cambiaParque) {
		this.cambiaParque = cambiaParque;
	}



	public void setValoresDe(ArrayList<ArrayList<String>> valoresDe) {
		this.valoresDe = valoresDe;
	}

	public void addValoresDe(ArrayList<String> valoresVarAl) {
		valoresDe.add(valoresVarAl);
	}

	public void setDependenDe(ArrayList<String> dependenDe) {
		this.dependenDe = dependenDe;
	}

	public void addDependenDe(String variable){
		dependenDe.add(variable);
	}

	public void setFuncion(ArrayList<ArrayList<String>> funcion) {
		this.funcion = funcion;
	}

	public void addFilaFuncion(ArrayList<String> filaFuncion){
		funcion.add(filaFuncion);
	}

	public void setNodos(ArrayList<NodoN> nodos) {
		this.nodos = nodos;
	}

	public void addNodo(NodoN nodo) {
		nodos.add(nodo);
	}

	public void setArcos(ArrayList<Arco> arcos){
		this.arcos = arcos;
	}

    public String getNombre(){
        return nombre;
    }

	public TipoComp getTipo() {
		return tipo;
	}

	public ArrayList<String> getVariables() {
		return variables;
	}

	public boolean getObservada() {
		return observada;
	}

	public boolean cambiaParque() {
		return cambiaParque;
	}


	public ArrayList<ArrayList<String>> getValoresDe() {
		return valoresDe;
	}

	public ArrayList<String> getDependenDe() {
		return dependenDe;
	}

	public ArrayList<ArrayList<String>> getFuncion() {
		return funcion;
	}

	public ArrayList<NodoN> getNodos() {
		return nodos;
	}

	public NodoN getNodoPasoTOrd(int idT, int idOrd){
		NodoN nod = null;
		for(NodoN n: nodos){
			if( n.getTiempo() == idT && n.getOrdinal() == idOrd){
				nod = n;
			}
		}
		return nod;
	}

    public ArrayList<Arco> getArcos(int t){
		ArrayList<Arco> arcosT = new ArrayList<Arco>();
		for(Arco a: arcos){
			if( a.getNodoIni().getTiempo() == t ){
				arcosT.add(a);
			}
		}
        return arcosT;
    }

    public ArrayList<NodoN> getNodosT(int pasoT){
        ArrayList<NodoN> nodosT = new ArrayList<NodoN>();
		for(NodoN n: nodos){
			if( n.getTiempo() == pasoT ){
				nodosT.add(n);
			}
		}
        return nodosT;
    }
    
    public ArrayList<NodoN> getPredecesoresNodo(NodoN nodo){
        ArrayList<NodoN> nodosPred = new ArrayList<NodoN>();
		for(Arco a: arcos){
			if( a.getNodoFin() == nodo ){
				nodosPred.add( a.getNodoFin() );
			}
		}
        return nodosPred;
    }

    public ArrayList<NodoN> getSucesoresNodo(NodoN nodo){
        ArrayList<NodoN> nodosSuc = new ArrayList<NodoN>();
		for(Arco a: arcos){
			if( a.getNodoIni() == nodo ){
				nodosSuc.add( a.getNodoFin() );
			}
        }
        return nodosSuc;
    }

	public void addArco(Arco a){
		arcos.add(a);
	}

	public void addArcos(ArrayList<Arco> ars){
		for(Arco a: ars){
			arcos.add(a);
		}
	}

	public ArrayList<Arco> getArcos(){
		return arcos;
	}

//    @Override
//    public Componente clone(){
//		ArrayList<String> NuevaVariables = new ArrayList<String>();
//		ArrayList<String> NuevaObservada = new ArrayList<String>();
//		ArrayList<ArrayList<String>> NuevaValoresDe =
//				new ArrayList<ArrayList<String>>();
//		ArrayList<String> NuevaDependenDe = new ArrayList<String>();
//		ArrayList<ArrayList<String>> NuevaFuncion =
//				new ArrayList<ArrayList<String>>();
//		ArrayList<NodoN> NuevaNodos = new ArrayList<NodoN>();
//		ArrayList<Arco> NuevaArcos = new ArrayList<Arco> ();
//		Componente nuevaC = new Componente();
//
//		for(String var: variables){
//			NuevaVariables.add(var);
//		}
//		for(String obs: observada){
//			NuevaObservada.add(obs);
//		}
//		for(ArrayList<String> vals: valoresDe){
//			ArrayList<String> aux = new ArrayList<String>();
//			for(String str: vals){
//				aux.add(str);
//			}
//			NuevaValoresDe.add(aux);
//		}
//		for(String var: dependenDe){
//			NuevaDependenDe.add(var);
//		}
//		for(ArrayList<String> vals: funcion){
//			ArrayList<String> aux = new ArrayList<String>();
//			for(String str: vals){
//				aux.add(str);
//			}
//			NuevaFuncion.add(aux);
//		}
//		for(NodoN n: nodos){
//			NuevaNodos.add(n.clone());
//		}
//		for(Arco a: arcos){
//			NuevaArcos.add(a.clone());
//		}
//
//		nuevaC.setNombre(nombre);
//		nuevaC.setTipo(tipo);
//		nuevaC.setVariables(variables);
//		nuevaC.setObservada(observada);
//		nuevaC.setValoresDe(valoresDe);
//		nuevaC.setDependenDe(dependenDe);
//		nuevaC.setFuncion(funcion);
//		nuevaC.setArcos(arcos);
//		nuevaC.setNodos(nodos);
//        return nuevaC;
//    }
//
//    @Override
//    public String toString(){
//        int k, j;
//        String texto = "COMPONENTE: ";
//        texto += nombre + " * * * * * * * * * * * * * * * * * * * * *\n";
//		texto += "Tipo: " + tipo + "\n";
//		texto += "Variables:\n";
//		for(String var: variables){
//			texto += "\t" + var + "\n";
//		}
//		texto += "¿Las variables son observadas?\n";
//		for(String obs: observada){
//			texto += "\t" + obs + "\n";
//		}
//		for(k = 0; k < variables.size(); k++){
//			texto += "Valores de [" + variables.get(k) + "]:\n";
//			for(j = 0; j < valoresDe.get(k).size(); j++){
//				texto += "\t" + valoresDe.get(k).get(j) + "\n";
//			}
//		}
//		texto += "Dependen de:\n";
//		for(String var: dependenDe){
//			texto += "\t" + var + "\n";
//		}
//		texto += "Tabla Función:\n";
//		texto += "\t[Nom. var. indep.]\t[Valor var. indep.]\t[Valor var. dep.]\t[Paso tiempo]\n";
//		for(k = 0; k < funcion.size(); k++){
//			for(j = 0; j < funcion.get(k).size(); j++){
//				texto += "\t" + funcion.get(k).get(j);
//			}
//			texto += "\n";
//		}
//		texto += "Nodos:\n";
//		for(NodoN n: nodos){
//			texto+= n.toString() + "\n";
//		}
//		texto += "Arcos:\n";
//		for(Arco a: arcos){
//			texto+= a.toString() + "\n";
//		}
//        texto +="*********************************************************";
//        return texto;
//    }
    
}
