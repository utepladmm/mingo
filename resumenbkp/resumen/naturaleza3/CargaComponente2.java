/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaleza3;

import persistencia.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import naturaleza3.Componente2.TipoComp;

/**
 *
 * @author ut600232
 */
public abstract class CargaComponente2 {

    public ArrayList<Componente2> cargarComponentes(String dirArchivo)
            throws FileNotFoundException, IOException, XcargaDatos {
//System.out.println("----------INICIO-----------\n");
        // Datos de todo el archivo.
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();
        /* Conjunto de componentes a devolver. */
        ArrayList<Componente2> componentes;
        // Datos de la componente a cargar.
        Componente2 comp;
        // Lista de los recursos base a cargar.
        ArrayList<String> listaComponentes = new ArrayList<String>();

        // Obtención de todos los datos del archivo.
//System.out.println("----------obtención de datos del archivo-----------\n");
//        datAr = getDatos(dirArchivo);
        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        // Carga de la lista de componentes a cargar.
//System.out.println("----------Carga de la lista de componentes a cargar-----------\n");
        listaComponentes = cargarListComps(datAr);
        // Carga de las componentes.
//System.out.println("----------Carga de las componentes.-----------\n");
        componentes = new ArrayList<Componente2>();
        for (String nombComp : listaComponentes) {
//System.out.println("----------Carga de la componente: " + nombComp + " -----------\n");
            comp = new Componente2();
            cargarComp(comp, datAr, nombComp);
            // Agregación de la componente al conjunto de componentes.
            componentes.add(comp);
        }
        return componentes;
    }

    public ArrayList<String> cargarListComps(
            ArrayList<ArrayList<String>> datAr) throws XcargaDatos {

        ArrayList<String> resultado = new ArrayList<String>();
        String iniListaComps = "&LISTA_COMPONENTES"; // Marca de lista de
        // componentes base a cargar.
        String listaFin = "&FIN"; // Marca de fin de la lista de recuros
        // base a cargar.
        boolean encontrado;
        int i, j;

        // Búsqueda de la lista de componentes a cargar.
        encontrado = false;
        i = 0;
        while (!encontrado && (i < datAr.size())) {
            if (iniListaComps.equalsIgnoreCase(datAr.get(i).get(0))) {
                encontrado = true;
            }
            i++;
        }
        // Si se encontró la marca de inicio de la lista y
        // no sigue la marca de fin de la lista, se carga la lista de
        // componentes.
        if (encontrado && (i < datAr.size()) &&
                listaFin.compareToIgnoreCase(datAr.get(i).get(0)) != 0) {
            for (j = 0; j < datAr.get(i).size(); j++) {
                resultado.add(datAr.get(i).get(j));
            }
        } else {
            throw new XcargaDatos("No se encontró la lista de componentes.");
        }

        return resultado;
    }

//    public static ArrayList<String> cargarNodoInicial(
//            String dirArchivo, GrafoN grafoN) throws XcargaDatos{
//
//        ArrayList<ArrayList<String>> datAr = LeerDatosArchivo.getDatos(dirArchivo);
//        ArrayList<String> resultado = new ArrayList<String>();
//        String textoIni = "&NODO_INICIAL"; // Marca de lista de
//                                                 // componentes base a cargar.
//        String textoFin = "&FIN"; // Marca de fin de la lista de recuros
//                                 // base a cargar.
//        boolean encontrado;
//        int i, j;
//
//        // Búsqueda de la definición del nodo inicial
//        encontrado = false;
//        i = 0;
//        while( !encontrado && (i <  datAr.size()) ){
//            if( textoIni.equalsIgnoreCase(datAr.get(i).get(0)) ) {
//                encontrado = true;
//            }
//            i++;
//        }
//        if(!encontrado) throw new XcargaDatos("No se encontraron datos del nodo inicial.");
//        // Si se encontró la marca de inicio del nodo se busca la lista de variables y sus valores
//        // con excepción de la variable nada que tiene todo GrafoN
//
//        String nombre = "";
//        String valor = "";
//        String vleido ="";  // valor de la variable leído
//        String nleido="";  // nombre de la variable leído
//        for(VariableNat vn: grafoN.getVarGrafo()){
//            encontrado = false;
//            nombre = vn.getNombre();
//            if(! nombre.equalsIgnoreCase("nada")){
//                j = 0;
//                do{
//                    nleido = datAr.get(i+j).get(1);
//                    vleido = datAr.get(i+j).get(3);
//                    if(nleido.equalsIgnoreCase(nombre)){
//                        if(vn.getValores().contains(vleido)){
//                            resultado.add(vleido);
//                            encontrado = true;
//                        }else{
//                            throw new XcargaDatos("Se especificó un valor inexistente " + vleido +
//                                    "para la VariableNat" + nombre );
//                        }
//                    }
//                    j++;
//                }while(!encontrado & !(datAr.get(i+j).get(0).equalsIgnoreCase(textoFin)) );
//            if (encontrado == false) throw new XcargaDatos("La VariableNat " + nombre + " no tiene valor ");
//            }
//
//        }
//        return resultado;
//    }
    private void cargarComp(Componente2 comp,
            ArrayList<ArrayList<String>> datAr, String nombComp)
            throws XcargaDatos {

        // Marca de inicio de los datos de una componente.
        String ini = "&COMPONENTE";
        // Marca de fin de los datos de un recurso base.
        String fin = "&FIN";
        String dato;
        String varLeida;
        int i, j;
        ArrayList<String> auxS;
        ArrayList<Boolean> auxB;  // sirve para verificar la lectura de valores de la variables
        int valoresLeidos = 0;
        boolean encontrado = false;
        boolean leyoObs = false;
        boolean noSeReconoceEtiqueta = false;
        boolean leyoObservada = false;
        ArrayList<ArrayList<String>> auxSD = null;

        // Búsqueda del nombre de la componente a cargar.
        i = 0;
//System.out.println("- Inicio del while para localizar el comienzo de la componente  " + nombComp + "\n");
        while (!encontrado && (i < datAr.size())) {
            if (ini.equalsIgnoreCase(datAr.get(i).get(0)) &&
                    nombComp.equalsIgnoreCase(datAr.get(i).get(1))) {
                encontrado = true;
                comp.setNombre(datAr.get(i).get(1));
            }
            i++;
        }
        if (!encontrado) {
            throw new XcargaDatos("No se encontraron los datos de " +
                    "la componente: " + nombComp);
        }
        /* En la variable "dato" se va guardando el primer dato de cada
         * línea que se va leyendo. */
//System.out.println("- Inicio del while para la carga de los datos " + nombComp + "\n");
        while (i < datAr.size() &&
                !fin.equalsIgnoreCase(dato = datAr.get(i).get(0))) {
            //Carga la información correspondiente a los datos de la componente.
            try {
                if (dato.equalsIgnoreCase("tipo")) {
                    if (datAr.get(i).get(1).equalsIgnoreCase("independiente")) {
                        comp.setTipo(TipoComp.Indep);
                    } else if (datAr.get(i).get(1).equalsIgnoreCase("dependiente")) {
                        comp.setTipo(TipoComp.Dep);
                    } else {
                        throw new XcargaDatos("La componente " + comp.getNombre() +
                                "tiene un dato de tipo errado");
                    }

                } else if (dato.equalsIgnoreCase("variables")) {
                    auxS = new ArrayList<String>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxS.add(datAr.get(i).get(j));
                    }
                    comp.setVariables(auxS);
                } else if (dato.equalsIgnoreCase("observada")) {
                    leyoObs = true;
                    if (datAr.get(i).get(1).equalsIgnoreCase("si")) {
                        comp.setObservada(true);
                    } else if (datAr.get(i).get(1).equalsIgnoreCase("no")) {
                        comp.setObservada(false);
                    } else {
                        throw new XcargaDatos("La componente " +
                                comp.getNombre() + " no tiene dato de observabilidad");

                    }
                } else if (dato.equalsIgnoreCase("determina_parque")) {
                    if (datAr.get(i).get(1).equalsIgnoreCase("si")) {
                        comp.setCambiaParque(true);
                    } else {
                        comp.setCambiaParque(false);
                    }
                } else if (dato.equalsIgnoreCase("valores_de")) {
                    valoresLeidos++;
                    varLeida = comp.getVariables().get(valoresLeidos - 1);
                    if (!varLeida.equalsIgnoreCase(datAr.get(i).get(1))) {
                        throw new XcargaDatos("Error en valores de variable de la naturaleza " +
                                varLeida);
                    }

                    auxS = new ArrayList<String>();
                    for (j = 3; j < datAr.get(i).size(); j++) {
                        auxS.add(datAr.get(i).get(j));
                    }
                    comp.addValoresDe(auxS);
                } else if (dato.equalsIgnoreCase("dependen_de")) {
                    auxS = new ArrayList<String>();
                    for (j = 1; j < datAr.get(i).size(); j++) {
                        auxS.add(datAr.get(i).get(j));
                    }
                    comp.setDependenDe(auxS);
                } else if (dato.equalsIgnoreCase("inicio_nodos")) {
                    /* Carga de los nodos. */
                    i = cargarNodos(comp, datAr, i);
                } else if (dato.equalsIgnoreCase("inicio_arcos")) {
                    /* Carga de los arcos. */
                    i = cargarArcos(comp, datAr, i);
                } else if (dato.equalsIgnoreCase("inicio_funcion")) {
//System.out.println("->> Llamada a cargarFunción-----------\n");
					/* Carga de la función. */
                    i = cargarFuncion(comp, datAr, i);
                } else {
                    noSeReconoceEtiqueta = true;
                }
            } catch (Exception ex) {
                throw new XcargaDatos("Componente: " + nombComp +
                        ". No se pudo cargar el dato: " + dato +
                        ". Se generó la excepción: " + ex.toString());
            } // Fin del catch.
            if (noSeReconoceEtiqueta) {
                throw new XcargaDatos("Componente: " + nombComp +
                        ". No se reconoce la etiqueta: " + dato);
            }
            i++;
        } // Fin de while(i < ...) .
        if (comp.cambiaParque() & comp.getTipo() == TipoComp.Dep) {
            throw new XcargaDatos("Componente: " + nombComp + " cambia" +
                    "parque y no es independiente");
        }
        if (leyoObs == false) {
            throw new XcargaDatos("Componente: " + nombComp + " no se " +
                    "cargo la observabilidad");
        }
    }

    private int cargarNodos(Componente2 comp,
            ArrayList<ArrayList<String>> datAr, int i) throws XcargaDatos {
        /* Carga los nodos y devuelve el número de línea siguiente a la última
        línea que indica el final de los datos de los nodos. */
        NodoN n;
        int ant, sig, k, m;
        k = i + 1;
        int tant = -1;
        int oant = 0;
        while (k < datAr.size() &&
                !"fin_nodos".equalsIgnoreCase(datAr.get(k).get(0))) {
            /* Si hay que repetir el último paso de tiempo. */
            if ("repeticion".equalsIgnoreCase(datAr.get(k).get(0))) {
                /* Paso de tiempo del nodo anterior a la repetición. */
                ant = Integer.parseInt(datAr.get(k - 1).get(0));
                /* Paso de tiempo del nodo posterior a la repetición. */
                sig = Integer.parseInt(datAr.get(k + 1).get(0));
                /* Copia de cada nodo del paso de tiempo anterior */
                if (ant >= sig) {
                    throw new XcargaDatos("Componente: " + comp.getNombre() +
                            "Repetición equivocada en nodos en tiempo: " + ant);
                }

                for (m = ant + 1; m < sig; m++) {
                    for (NodoN nod : comp.getNodosT(ant)) {
                        n = new NodoN();
                        n.setTiempo(m);
                        n.setOrdinal(nod.getOrdinal());
                        for (String valorVN : nod.getValoresVN()) {
                            n.getValoresVN().add(valorVN);
                        }
                        comp.addNodo(n);
                    }
                }
                k = k + 1;
            } /* Fin del if "repeticion". */
            /* Si no es un caso de repetición. */
            n = new NodoN();
            int t = Integer.parseInt(datAr.get(k).get(0));
            int o = Integer.parseInt(datAr.get(k).get(1));
            n.setTiempo(t);
            n.setOrdinal(o);

            if ((t != tant) && (o != 1)) {
                throw new XcargaDatos("Componente: " + comp.getNombre() + "\n" +
                        "Secuencia equivocada en nodo: tiempo " + t + "ordinal " + o);
            }
            if ((t == tant) && (o != oant + 1)) {
                throw new XcargaDatos("Componente: " + comp.getNombre() + "\n" +
                        "Secuencia equivocada en nodo: tiempo " + t + " - ordinal " + o);
            }
            tant = t;
            oant = o;
            for (m = 0; m < comp.getVariables().size(); m++) {
                n.getValoresVN().add(datAr.get(k).get(2 + m));
            }
            comp.addNodo(n);
            k = k + 1;
        } /* Fin del while. */
        return k;
    }

    private int cargarArcos(Componente2 comp,
            ArrayList<ArrayList<String>> datAr, int i) {
        /* Carga los arcos y devuelve el número de línea siguiente a la última
        línea que indica el final de los datos de los arcos. */
        Arco a;
        int ant, sig, k, m;
        k = i + 1;
        while (k < datAr.size() &&
                !"fin_arcos".equalsIgnoreCase(datAr.get(k).get(0))) {
            /* Si hay que repetir el último paso de tiempo. */
            if ("repeticion".equalsIgnoreCase(datAr.get(k).get(0))) {
                /* Paso de tiempo del nodo inicial del último arco
                anterior a la repetición. */
                ant = Integer.parseInt(datAr.get(k - 1).get(0));
                /* Paso de tiempo del primer nodo inicial posterior
                a la repetición. */
                sig = Integer.parseInt(datAr.get(k + 1).get(0));
                /* Copia de cada arco del paso de tiempo anterior */
                for (m = ant + 1; m < sig; m++) {
                    for (Arco arc : comp.getArcos(ant)) {
                        a = new Arco();
                        a.setNodoIni(comp.getNodoPasoTOrd(
                                m, arc.getNodoIni().getOrdinal()));
                        a.setNodoFin(comp.getNodoPasoTOrd(
                                m + 1, arc.getNodoFin().getOrdinal()));
                        a.setProbArco(arc.getProbArco());
                        comp.addArco(a);
                    }
                }
                k = k + 1;
            } /* Fin del if "repeticion". */
            /* Si no es un caso de repetición. */
            a = new Arco();
            a.setNodoIni(comp.getNodoPasoTOrd(
                    Integer.parseInt(datAr.get(k).get(0)),
                    Integer.parseInt(datAr.get(k).get(1))));
            a.setNodoFin(comp.getNodoPasoTOrd(
                    Integer.parseInt(datAr.get(k).get(2)),
                    Integer.parseInt(datAr.get(k).get(3))));
            a.setProbArco(
                    Double.parseDouble(datAr.get(k).get(4)));
            comp.addArco(a);
            k = k + 1;
        } /* Fin del while. */
        return k;
    }

    private int cargarFuncion(Componente2 comp,
            ArrayList<ArrayList<String>> datAr, int i) {
        /* Carga la función y devuelve el número de la línea siguiente a la
        línea que indica el final de la función. */
        ArrayList<String> auxFilaFuncion;
        int j, k;
        k = i + 1;
//System.out.println("----------Inicio de cargarFunción-----------\n");
        while (k < datAr.size() &&
                !"fin_funcion".equalsIgnoreCase(datAr.get(k).get(0))) {
            auxFilaFuncion = new ArrayList<String>();
            for (j = 0; j < datAr.get(k).size(); j++) {
                auxFilaFuncion.add(datAr.get(k).get(j));
//System.out.println();
//System.out.println("=====================================================");
//System.out.println(datAr.get(k).get(j));
//System.out.println("=====================================================");
            }
            comp.addFilaFuncion(auxFilaFuncion);
            k = k + 1;
        } /* Fin del while. */
        return k;
    }

    private ArrayList<ArrayList<String>> getDatos(String dirArchivo)
            throws FileNotFoundException, IOException {
        BufferedReader entrada;
        ArrayList<ArrayList<String>> resultado =
                new ArrayList<ArrayList<String>>();
        ArrayList<String> lineaResul;
        String[] datosLinea;
        String linea;
        String sep = "[\\s]+"; // Separador de datos.
        String coment = "//"; // Indicador de comentario.
        int posCom; // Posición del inicio del comentario.
        int j;
        File archivo = new File(dirArchivo);
        entrada = new BufferedReader(new FileReader(archivo));

        while ((linea = entrada.readLine()) != null) {
            // Eliminación de los comentarios de las líneas.
            if ((posCom = linea.indexOf(coment)) > -1) {
                linea = (posCom == 0) ? "" : linea.substring(0, posCom);
            }
            // Si la línea no es una línea de blancos, entonces se agregan
            // los datos y se agrega una nueva "línea" a resultado.
            if (!linea.matches("[\\s]*")) {
                datosLinea = linea.split(sep);
                lineaResul = new ArrayList<String>();
                for (j = 0; j < datosLinea.length; j++) {
                    /* Si el dato NO es un string de longitud cero "",
                     * lo cual podría ocurrir si
                     * la línea comienza por ej. con tabulador, se agrega
                     * el dato. */
                    if (!datosLinea[j].equalsIgnoreCase("")) {
                        lineaResul.add(datosLinea[j]);
                    }
                }
                resultado.add(lineaResul);
            }
        }
        entrada.close();
        return resultado;
    }
}
