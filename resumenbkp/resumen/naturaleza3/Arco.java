/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package naturaleza3;

import java.io.Serializable;

/**
 *
 * @author Usuario
 */
public class Arco implements Serializable{
    private NodoN nodoIni;
    private NodoN nodoFin;
    private Double probArco;

    public Arco(NodoN nodoIni, NodoN nodoFin, Double probArco) {
        this.nodoIni = nodoIni;
        this.nodoFin = nodoFin;
        this.probArco = probArco;
    }

	public Arco() {
	}


    public NodoN getNodoFin() {
        return nodoFin;
    }

    public void setNodoFin(NodoN nodoFin) {
        this.nodoFin = nodoFin;
    }

    public NodoN getNodoIni() {
        return nodoIni;
    }

    public void setNodoIni(NodoN nodoIni) {
        this.nodoIni = nodoIni;
    }

    public Double getProbArco() {
        return probArco;
    }

    public void setProbArco(Double probArco) {
        this.probArco = probArco;
    }

    @Override
    public String toString() {
        String texto = "";
        texto += nodoIni.toString() + "\n";
        texto += nodoFin.toString() + "\n";
        texto += probArco.toString(probArco) + "\n";
        return texto;
    }



}
