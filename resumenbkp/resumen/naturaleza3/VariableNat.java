/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaleza3;

import java.util.ArrayList;

/**
 *
 * Las variables de la naturaleza representan factores no controlables
 * por el decisor, que son aleatorios.
 * 
 * Algunas VariablesNat tienen nombres predefinidos que deben emplearse
 * al definirlas en el GrafoN:
 * 
 * - La VariableNat asociada a una demanda aleatoria debe tener nombre igual
 * al nombre de la demanda, con sufijo "_DEM".
 * 
 * - La VariableNat asociada a un cambio aleatorio debe tener nombre igual
 * al nombre del cambio, con sufijo "_CAL".
 * 
 * - La VariableNat asociada a una demora aleatoria en la construcción de un
 * RecursoBase o TransforBase debe tener nombre igual al nombre del RB o TB, 
 * con sufijo "_DAC".
 *
 * - La VariableNat asociada a un monto aleatorio de inversión en la construcción de un
 * RecursoBase o TransforBase debe tener nombre igual al nombre del RB o TB,
 * con sufijo "_INV".

 *
 *
 *
 *
 *
 */
public class VariableNat implements java.io.Serializable {

     private String nombre;
     private ArrayList<String> valores;
//    private boolean observable;
     private boolean observadaEnOptim;
     /**
      * observadaEnOptim = true significa que al tomar las decisiones de inversión
      * se toma en cuenta el valor de la variable
      * Las variables con observadaEnOptim = false
      * es decir las variables no observadas, deben ser independientes
      * de las variables de componentes observadas.
      *
      * Al considerar que una variable de la naturaleza es no observada en la
      * optimización, se comete un error respecto a si es observada, excepto si
      * el valor de la variable en t es independiente del valor en t-1 !!!!!!!!!!
      */
     private boolean detParque;

     /**
      * detParque = true significa que la variable interviene en el método
      * sucesorDeUnPort que determina los sucesores de un portafolio (parque)
      * Una variable que determina parque puede ser observada o puede ser no
      * observada y pertenecer a una componente independiente
      */
     public VariableNat() {
     }

     public VariableNat(String nombre, ArrayList<String> valores) {
          this.nombre = nombre;
          this.valores = valores;
     }

     public String getNombre() {
          return nombre;
     }

     public void setNombre(String nombre) {
          this.nombre = nombre;
     }

     public ArrayList<String> getValores() {
          return valores;
     }

     public void setValores(ArrayList<String> valores) {
          this.valores = valores;
     }

     public boolean getObservadaEnOptim() {
          return observadaEnOptim;
     }

     public void setObservadaEnOptim(boolean observadaEnOptim) {
          this.observadaEnOptim = observadaEnOptim;
     }

     public boolean detParque() {
          return detParque;
     }

     public void setDetParque(boolean detParque) {
          this.detParque = detParque;
     }

     @Override
     public boolean equals(Object obj) {
          if (obj == null) {
               return false;
          }
          if (getClass() != obj.getClass()) {
               return false;
          }
          final VariableNat other = (VariableNat) obj;
          if ((this.nombre == null) ? (other.nombre != null) : !this.nombre.equals(other.nombre)) {
               return false;
          }
          if (this.valores != other.valores && (this.valores == null || !this.valores.equals(other.valores))) {
               return false;
          }
          if (this.observadaEnOptim != other.observadaEnOptim) {
               return false;
          }
          if (this.detParque != other.detParque) {
               return false;
          }
          return true;
     }

     @Override
     public int hashCode() {
          int hash = 3;
          hash = 79 * hash + (this.nombre != null ? this.nombre.hashCode() : 0);
          hash = 79 * hash + (this.valores != null ? this.valores.hashCode() : 0);
          hash = 79 * hash + (this.observadaEnOptim ? 1 : 0);
          hash = 79 * hash + (this.detParque ? 1 : 0);
          return hash;
     }

//    public boolean isObservable() {
//        return observable;
//    }
//
//    public void setObservable(boolean observable) {
//        this.observable = observable;
//    }
     @Override
     public String toString() {
          String texto = "Variable de la naturaleza: " + "\r\n";
          texto += "Nombre: " + nombre + "\r\n";
          texto += "Valores posibles: " + valores.toString() + "\r\n";
//		texto += "Observable =" + observable + "\r\n";
          texto += "Observada en la optimización = " + observadaEnOptim;
          texto += "\r\n";
          return texto;
     }
}
