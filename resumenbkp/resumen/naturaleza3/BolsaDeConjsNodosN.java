/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaleza3;

import UtilitariosGenerales.OperMatrices;
import dominio.DatosGeneralesEstudio;
import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.ConjNodosN.TConjNodoN;
import persistencia.CargaDatosGenerales;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 * Es la estructura que almacena los conjuntos de NodoN empleados en el grafo
 *
 */
public class BolsaDeConjsNodosN implements Serializable {

    private GrafoN grafoN;  // el grafo asociado
    private TConjNodoN tipo;
    private ArrayList<VariableNat> variablesObservadas;
    private ArrayList<VariableNat> variablesDetP;
    private BolsaDeConjsNodosN bolsaCIAsoc;
    /**
     * Si la bolsa this es de conjuntos que determinan parque
     * bolsaCIAsoc es la bolsa de conjuntos de información asociada
     */
    private ArrayList<ArrayList<ConjNodosN>> conjuntosNodosN;
    /** el primer índice es etapa. El get(0) es la etapa 0.
     *  el segundo ordinal del CI dentro de la etapa
     */
    private ArrayList<boolean[][]> matricesSuc;   // matriz de sucesores
    private ArrayList<double[][]> matricesProb;  // matriz de probabilidades de transición

    /**
     * Lista de matrices de transición entre ConjNodosN de una etapa y la siguiente.
     * en el get(0) de la lista está la matriz entre la etapa 0 y la 1 y así
     * sucesivamente.
     */
    public BolsaDeConjsNodosN(GrafoN grafoN) {
        this.grafoN = grafoN;
        variablesObservadas = new ArrayList<VariableNat>();
        variablesDetP = new ArrayList<VariableNat>();
        conjuntosNodosN = new ArrayList<ArrayList<ConjNodosN>>();
        for (int ie = 0; ie <= grafoN.getCantEtapas(); ie++) {
            ArrayList<ConjNodosN> aux = new ArrayList<ConjNodosN>();
            conjuntosNodosN.add(aux);
        }
        matricesSuc = new ArrayList<boolean[][]>() ;
        matricesProb = new ArrayList<double[][]>();
    }

    public BolsaDeConjsNodosN(int cantEtapas) {
        variablesObservadas = new ArrayList<VariableNat>();
        variablesDetP = new ArrayList<VariableNat>();
        conjuntosNodosN = new ArrayList<ArrayList<ConjNodosN>>();
        for (int ie = 0; ie <= cantEtapas; ie++) {
            ArrayList<ConjNodosN> aux = new ArrayList<ConjNodosN>();
            conjuntosNodosN.add(aux);
        }
        matricesSuc = new ArrayList<boolean[][]>() ;
        matricesProb = new ArrayList<double[][]>();
    }

    public ArrayList<ArrayList<ConjNodosN>> getConjuntosNodosN() {
        return conjuntosNodosN;
    }

    public void setConjuntosNodosN(ArrayList<ArrayList<ConjNodosN>> conjuntosNodosN) {
        this.conjuntosNodosN = conjuntosNodosN;
    }

    public BolsaDeConjsNodosN getBolsaCIAsoc() {
        return bolsaCIAsoc;
    }

    public void setBolsaCIAsoc(BolsaDeConjsNodosN bolsaCIAsoc) {
        this.bolsaCIAsoc = bolsaCIAsoc;
    }

    public GrafoN getGrafoN() {
        return grafoN;
    }

    public void setGrafoN(GrafoN grafoN) {
        this.grafoN = grafoN;
    }

    public ArrayList<VariableNat> getVariablesObservadas() {
        return variablesObservadas;
    }

    public void setVariablesObservadas(ArrayList<VariableNat> variablesObservadas) {
        this.variablesObservadas = variablesObservadas;
    }

    public TConjNodoN getTipo() {
        return tipo;
    }

    public void setTipo(TConjNodoN tipo) {
        this.tipo = tipo;
    }

    public ArrayList<double[][]> getMatricesProb() {
        return matricesProb;
    }

    public void setMatricesProb(ArrayList<double[][]> matricesProb) {
        this.matricesProb = matricesProb;
    }

    public ArrayList<boolean[][]> getMatricesSuc() {
        return matricesSuc;
    }

    public void setMatricesSuc(ArrayList<boolean[][]> matricesSuc) {
        this.matricesSuc = matricesSuc;
    }

    

    public ArrayList<VariableNat> getVariablesDetP() {
        return variablesDetP;
    }

    public void setVariablesDetP(ArrayList<VariableNat> variablesDetP) {
        this.variablesDetP = variablesDetP;
    }

//	public void crearBolsaConjsInfoM1 (){
//		/** ATENCIÓN ESTO SÓLO SIRVE SI EL GN ES MARKOVIANO DE ORDEN 1
//		 * ES DECIR SI NO HAY DOS NODOS DEL GN DISTINTOS, CONTEMPORÁNEOS Y
//		 * CON LOS MISMOS VALORES DE VARIABLES OBSERVADAS
//		 *
//		 * La BolsaDeConjsInfo se carga en el grafoN al que pertenece
//		 *
//		 *
//		 **/
//		BolsaDeConjsNodosN bolsaCI = new BolsaDeConjsNodosN(grafoN);
//		String texto;
//		int ordinalCI;
//		// ie es el indice de etapa en el grafo de la naturaleza
//		// ordinalCI es el ordinal del CI en la etapa
//		for (int ie=1; ie <= grafoN.getCantEtapas(); ie++ ){
//			ordinalCI = 1;
//			for (NodoN n: grafoN.getNodos().get(ie)){
//				boolean encontroCI = false;
//				int iCI = 0;
//				if (! bolsaCI.getConjuntosNodosN().get(ie).isEmpty()){
//					do {
//						// busca entre los CI preexistentes
//						if (bolsaCI.getConjuntosNodosN().get(ie).get(iCI).nodoPerteneceAlCI(n)){
//							bolsaCI.getConjuntosNodosN().get(ie).get(iCI).getNodos().add(n);
//							n.setConjInfo(bolsaCI.getConjuntosNodosN().get(ie).get(iCI));
//							encontroCI = true;}
//						iCI++;
//					} while (iCI<bolsaCI.getConjuntosNodosN().get(ie).size() &&
//							 encontroCI==false);
//				}
//				if (! encontroCI){
//					// el nodo n requiere crear un nuevo CI
//					ConjNodosN cIN = new ConjNodosN(grafoN, ie, n.getValoresObservadas(grafoN));
//					cIN.setEtapa(ie);
//					cIN.setOrdinal(ordinalCI);
//					cIN.setTiempo(grafoN.getPasoDeEtapa().get(ie));
//					cIN.getNodos().add(n);
//					cIN.setValoresObservadas(n.getValoresObservadas(grafoN));
//					n.setConjInfo(cIN);
//					bolsaCI.getConjuntosInfo().get(ie).add(cIN);
////					System.out.println("crea CI etapa " + ie );
//					texto = cIN.toString();
////					System.out.print(texto);
//				}
//			}
//		}
//		grafoN.setBolsaCI(bolsaCI);
//	}
//
//
//	public void crearBolsaConjsInfoPerfecta (){
//		/** ATENCIÓN ESTO SÓLO SIRVE SI EL GN ES DE INFORMACIÓN
//		 * PERFECTA ES DECIR CADA NODO DEL GN ESTA SOLO EN SU CI
//		 *
//		 * ATENCION HAY QUE CHEQUEAR Y NO ESTÁ HECHO, QUE TODAS LAS
//		 * VARIABLES SEAN OBSERVADAS
//		 *
//		 * La BolsaDeConjsInfo se carga en el grafoN al que pertenece
//		 *
//		 *
//		 *
//		*/
//
//		BolsaDeConjsNodosN bolsaCI = new BolsaDeConjsNodosN(grafoN);
//		String texto;
//		int ordinalCI;
//		// ie es el indice de etapa en el grafo de la naturaleza
//		// ordinalCI es el ordinal del CI en la etapa
//		for (int ie=1; ie <= grafoN.getCantEtapas(); ie++ ){
//			ordinalCI = 1;
//			for (NodoN n: grafoN.getNodos().get(ie)){
//				ConjNodosN cIN = new ConjNodosN(grafoN, ie, n.getValoresObservadas(grafoN));
//				cIN.setEtapa(ie);
//				cIN.setOrdinal(ordinalCI);
//				ordinalCI++;
//				cIN.setTiempo(grafoN.getPasoDeEtapa().get(ie));
//				cIN.getNodos().add(n);
//				cIN.setValoresObservadas(n.getValoresObservadas(grafoN));
//				n.setConjInfo(cIN);
//				bolsaCI.getConjuntosInfo().get(ie).add(cIN);
////				System.out.println("crea CI etapa " + ie );
//				texto = cIN.toString();
////				System.out.print(texto);
//			}
//		}
//		grafoN.setBolsaCI(bolsaCI);
//	}
    public void calcProbCondIgnoraTotal() {
        /**
         * SOLO EN EL CASO EN QUE DE LAS VARIABLES NO OBSERVADAS EN LA
         * OPTIMIZACIÓN NO SE TIENE NINGUN DATO DESDE T=1 EN ADELANTE
         *
         * LO MEJOR ES QUE LOS VALORES DE LAS VARIABLES NO OBSERVADAS
         * EN CADA t SEAN INDEPENDIENTES DE LOS DE PERÍODOS ANTERIORES
         * Calcula en cada nodo de grafoN la probabilidad condicional
         * de estar en ese nodo, dado que se está en el conjunto de información.
         */
        for (int e = 1; e <= grafoN.getCantEtapas(); e++) {
            for (ConjNodosN ci : grafoN.getBolsaCI().getConjuntosNodosN().get(e)) {
                double sumaProb = 0.0;
                for (NodoN n : ci.getNodos()) {
                    sumaProb += n.getProbabilidad();
                }
                for (NodoN n : ci.getNodos()) {
                    n.setProbCondDadoCI(n.getProbabilidad() / sumaProb);
                }
            }
        }

    }

    public void calcProbCond2(ConjNodosN.TConjNodoN tipoC) {
        /**
         * Calcula la probabilidad condicional de cada nodoN de un ConjInfo
         * como el cociente entre la probabilidad del nodoN y la suma de
         * probabilidades de todos los nodos del ConjInfo
         *
         */
        for (int e = 1; e < conjuntosNodosN.size(); e++) {
            for (ConjNodosN ci : conjuntosNodosN.get(e)) {
                double sumaProb = 0.0;
                for (NodoN n : ci.getNodos()) {
                    sumaProb += n.getProbabilidad();
                }
                if(sumaProb==0){
                    System.out.println("ATENCIÓN: CI o CDP etapa "+ e +", ordinal "+ci.getOrdinal()+"tiene probabilidad nula");
                }
                for (NodoN n : ci.getNodos()) {
                    if (tipoC == TConjNodoN.Info) {
                        if(sumaProb!=0){
                            n.setProbCondDadoCI(n.getProbabilidad() / sumaProb);
                        }else{
                            n.setProbCondDadoCI(0.0);
                        }
                    } else {
                        if(sumaProb!=0){                        
                            n.setProbCondDadoCDP(n.getProbabilidad() / sumaProb);
                        }else{
                            n.setProbCondDadoCDP(0.0);                            
                        }
                    }
                }
            }
        }

    }

    public void cargaNodoEnCI(int t, ConjNodosN cI, NodoN n) throws XcargaDatos {
        /**
         * Carga en el ConjInfo cI, del paso t, el NodoN n
         * si en t no existe cI da un error
         */
        if (!conjuntosNodosN.get(t).contains(cI)) {
            throw new XcargaDatos("El CI " +
                    cI.toString() + " no existe en paso " + t);
        }
        cI.getNodos().add(n);
    }

    /**
     *  Crea la lista de escenarios que resulta de la BolsaDeConjsNodosN
     *  (this).
     */
    public ListaEscenariosCN devuelveListaEsc() {
        ListaEscenariosCN listaEsc = new ListaEscenariosCN(this, "escenario");
        GrafoN grafo = grafoN;
        int cantEtapas = grafo.getCantEtapas();

        for (int e = 0; e <= cantEtapas; e++) {
            ArrayList<EscenarioCN> nuevosEsc = new ArrayList<EscenarioCN>();
            int cantEsc = 0;
            if (e == 0) {
                for (ConjNodosN ci : conjuntosNodosN.get(e)) {
                    EscenarioCN esc = new EscenarioCN(this);
                    esc.setCantEtapas(0);
                    esc.getConjsNodosN().add(ci);
                    nuevosEsc.add(esc);
                    cantEsc++;
                }
            } else {
                for (int ies = 0; ies < listaEsc.getEscenarios().size(); ies++) {
                    EscenarioCN escBase = listaEsc.getEscenarios().get(ies);
                    ArrayList<ConjNodosN> auxSuc = new ArrayList<ConjNodosN>();
                    auxSuc.addAll(escBase.getConjsNodosN().get(e - 1).getSucesores());
                    ArrayList<Double> auxD = new ArrayList<Double>();
                    auxD.addAll(escBase.getConjsNodosN().get(e - 1).getProbSucesores());
                    nuevosEsc.addAll(escBase.generarHijos(this, auxSuc, auxD));
                    cantEsc += auxSuc.size();
                }
            }
            String texto = "Etapa = " + e + "cantidad escenarios= " + cantEsc + "\n";
            System.out.print(texto);
            listaEsc.setEscenarios(nuevosEsc);
        }
        for (int ies = 0; ies < listaEsc.getEscenarios().size(); ies++) {
            listaEsc.getEscenarios().get(ies).setNumesc(ies + 1);
        }
        return listaEsc;
    }

    public void calculaMatTrans() {
        for (int ie = 0; ie < grafoN.getCantEtapas(); ie++) {
            int cantFilas = conjuntosNodosN.get(ie).size();
            int cantColumnas = conjuntosNodosN.get(ie + 1).size();
            boolean[][] matSuc = new boolean[cantFilas][cantColumnas];
            double[][] matProb = new double[cantFilas][cantColumnas];
            int i = 0;
            for (ConjNodosN n : conjuntosNodosN.get(ie)) {
                int j = 0;
                for (ConjNodosN m : conjuntosNodosN.get(ie + 1)) {
                    // si el j-esimo nodo de etapa + 1 no es sucesor del i-esimo de etapa
                    // la probabilidad se asigna igual a 0
                    matSuc[i][j] = false;
                    matProb[i][j] = 0.0;
                    int isuc = 0;
                    for (ConjNodosN suc : n.getSucesores()) {
                        if (suc == m) {
                            matSuc[i][j] = true;
                            matProb[i][j] = conjuntosNodosN.get(ie).get(i).getProbSucesores().get(isuc);
                        }
                        isuc++;
                    }
                    j++;
                }
                i++;
            }
            matricesSuc.add(matSuc);
            matricesProb.add(matProb);
        }
    }


    public double[][] matrizProbCompuesta(int etapaini, int etapafin) throws XcargaDatos{
        if(etapafin<=etapaini) throw new XcargaDatos("Se pidio en BolsaDeConjsNodosN" +
                "una transición de la etapa " + etapaini + " a la etapa " + etapafin +
                " que no es posterior" );

        double[][] prod = matricesProb.get(etapaini);
        for (int ie=etapaini; ie<etapafin-1; ie++){
            double[][] prodNue = OperMatrices.productoMatRealAB(prod,
                    matricesProb.get(ie+1));
            prod = OperMatrices.copiaMatReal(prodNue);
        }
        return prod;
    }


    public boolean[][] matrizBoolCompuesta(int etapaini, int etapafin) throws XcargaDatos{
        if(etapafin<=etapaini) throw new XcargaDatos("Se pidio en BolsaDeConjsNodosN" +
                "una transición de la etapa " + etapaini + " a la etapa " + etapafin +
                " que no es posterior" );

        boolean[][] prod = matricesSuc.get(etapaini);
        for (int ie=etapaini; ie<etapafin-1; ie++){
            boolean[][] prodNue = OperMatrices.productoMatBoolAB(prod,
                    matricesSuc.get(ie+1));
            prod = OperMatrices.copiaMatBool(prodNue);
        }
        return prod;
    }

    /**
     * Devuelve la probabilidad de transición entre el ConjNodosN ci de etapa e
     * y el ConjNodosN cisuc que se supone sucesor y de la etapa siguiente a ci, esuc = e+1.
     * Devuelve excepción si los ConjNodosN no están las etapas especificadas o el
     * cisuc no es sucesor de ci.
     */
    public double probDeTransCI(ConjNodosN ci, int e, ConjNodosN cisuc, int esuc) throws XcargaDatos{
        double prob = 0.0;
        if(esuc != e+1) throw new XcargaDatos("Las etapas no son consecutivas");
        if(!conjuntosNodosN.get(e).contains(ci)) throw new
                XcargaDatos("El ConjNodosN " + ci.toStringCorto() + "no pertenece a etapa " + e);
        if(!conjuntosNodosN.get(esuc).contains(cisuc)) throw new
                XcargaDatos("El ConjNodosN " + cisuc.toStringCorto() + "no pertenece a etapa " + esuc);
        int indci = conjuntosNodosN.get(e).indexOf(ci);
        int indcisuc = conjuntosNodosN.get(e).indexOf(cisuc);
        if(! matricesSuc.get(e)[indci][indcisuc]) throw new
                XcargaDatos("El ConjNodosN " + cisuc.toStringCorto() + " no es sucesor de" +
                ci.toStringCorto());
        prob = matricesProb.get(e)[indci][indcisuc];
        return prob;
    }


    @Override
    public String toString() {

        String texto = "==============================================" + "\t\n";
        texto += "COMIENZA BOLSA DE CONJUNTOS DE INFORMACIÓN" + "\t\n";
        texto += "==============================================" + "\t\n";

        texto += "GRAFO QUE ORIGINA LOS CI: " + grafoN.getNombre() + "\t\n";
        texto += "VARIABLES OBSERVADAS " + "\t\n";
        for (VariableNat vn : variablesObservadas) {
            texto += vn.toString() + "\t\n";
        }
        if (tipo == TConjNodoN.DetParque) {
            texto += "VARIABLES QUE DETERMINAN PARQUE " + "\t\n";
            for (VariableNat vn : variablesDetP) {
                texto += vn.toString() + "\t\n";
            }
        }
        for (int i = 0; i < conjuntosNodosN.size(); i++) {
            for (int j = 0; j < conjuntosNodosN.get(i).size(); j++) {
                texto += conjuntosNodosN.get(i).get(j).toString() + "\t\n";
            }
        }
        return texto;
    }

    static void main(String[] args) throws XcargaDatos {

        DatosGeneralesEstudio datGen1 = new DatosGeneralesEstudio();
        CargaDatosGenerales cargadorDG = new CargaDatosGenerales() {
        };
        String dirDatGen = "D:/Java/PruebaJava/V2-DatosGenerales.txt";
        try {
            cargadorDG.cargarDatosGen(dirDatGen, datGen1);
        } catch (XcargaDatos ex) {
            System.out.println("------ERROR-------");
            System.out.println(ex.getDescripcion());
            System.out.println("------------------\n");

        }

        GrafoN grafoNaturaleza = new GrafoN();
        CargaGrafoN2 cargaNat = new CargaGrafoN2() {
        };

        String dirArchivo = "D:/Java/PruebaJava2/V9-GrafoNaturaleza.txt";
        int cantPasos = datGen1.getCantPasos();
        grafoNaturaleza = cargaNat.cargarGrafoN(dirArchivo, cantPasos, false);
        String texto = grafoNaturaleza.toString();
        System.out.print(texto);


        texto = grafoNaturaleza.getBolsaCI().toString();
        System.out.print(texto);

    }
}
