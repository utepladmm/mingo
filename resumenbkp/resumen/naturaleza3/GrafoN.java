/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaleza3;

import dominio.Caso;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import persistencia.XcargaDatos;

/**
 *
 * @author Usuario
 */
public class GrafoN implements java.io.Serializable {

    private int cantEtapas; // cantidad de etapas adicionales al paso cero
    private ArrayList<Integer> pasoDeEtapa;
    /** indica el paso de tiempo asociado cada etapa
     *  incluso la etapa cero cuyo paso es el cero que se carga en el constructor
     */
    boolean pasosCompletos; // es true si cada paso tiene una etapa
    private String nombre;
    private ArrayList<VariableNat> varGrafo;
    /**
     * El grafo tiene siempre una primera VariableNat de nombre nada que existe
     * para que haya siempre grafo aunque no se hayan especificado componentes
     */
    private ArrayList<ArrayList<NodoN>> nodos;
    /**
     *  nodos.get(0) tiene un solo nodo, que vive en t=0
     *  SE LEEN LAS PROBABILIDADES DE UNOS ARCOS FICTICIOS ENTRE T=0 Y T=1
     *  Puede haber más de un nodoN en t=1.
     */
    private NodoN nodoInicial;
    /**
     * Es el nodo, que vive en el paso 1, en que se encuentra la naturaleza inicialmente
     */
    private BolsaDeConjsNodosN bolsaCI;
    /**
     * Estructura para los conjuntos de información del grafo.
     * Conjunto de información es el conjunto de los nodos del grafo
     * de la naturaleza completo, asociados a un único estado (nodo)
     * del grafo de las variables observadas.
     */
    private BolsaDeConjsNodosN bolsaCDP;

    /**
     * Estructura para los conjuntos determinantes de parque
     * Conjunto determinante de parque es cada conjunto de nodos del grafo
     * de la naturaleza completo, asociados a un único estado (nodo)
     * del grafo de las variables observadas y variables que determinan el parque
     * Si las variables que determinan el parque son observadas
     * bolsaCDT y bolsaCI son iguales.
     *
     * Las variables que determinan parque son las de cambio aleatorio, inversión aleatoria
     * y demora aleatoria.
     *
     */
    public GrafoN(int cantEtapas, String nombre, boolean pasosCompletos) {
        this.cantEtapas = cantEtapas;
        this.nombre = nombre;
        this.pasosCompletos = pasosCompletos;
        pasoDeEtapa = new ArrayList<Integer>();
        pasoDeEtapa.add(0);	// el paso de la primera etapa es el cero
        if (pasosCompletos) {
            for (int it = 1; it <= cantEtapas; it++) {
                pasoDeEtapa.add(it);
            }
        }
        varGrafo = new ArrayList<VariableNat>();
        nodos = new ArrayList<ArrayList<NodoN>>();
        bolsaCI = new BolsaDeConjsNodosN(this);
        bolsaCDP = new BolsaDeConjsNodosN(this);
    }

    public GrafoN() {
        varGrafo = new ArrayList<VariableNat>();
        nodos = new ArrayList<ArrayList<NodoN>>();
        pasoDeEtapa = new ArrayList<Integer>();
        pasoDeEtapa.add(0);	// el paso de la primera etapa es el cero
        bolsaCI = new BolsaDeConjsNodosN(this);
        bolsaCDP = new BolsaDeConjsNodosN(this);
    }

    public int getCantEtapas() {
        return cantEtapas;
    }

    public void setCantEtapas(int cantEtapas) {
        this.cantEtapas = cantEtapas;
    }

    public ArrayList<Integer> getPasoDeEtapa() {
        return pasoDeEtapa;
    }

    public void setPasoDeEtapa(ArrayList<Integer> pasoDeEtapa) {
        this.pasoDeEtapa = pasoDeEtapa;
    }

    public boolean isPasosCompletos() {
        return pasosCompletos;
    }

    public void setPasosCompletos(boolean pasosCompletos) {
        this.pasosCompletos = pasosCompletos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<VariableNat> getVarGrafo() {
        return varGrafo;
    }

    public void setVarGrafo(ArrayList<VariableNat> varGrafo) {
        this.varGrafo = varGrafo;
    }

    public ArrayList<ArrayList<NodoN>> getNodos() {
        return nodos;
    }

    public void setNodos(ArrayList<ArrayList<NodoN>> nodos) {
        this.nodos = nodos;
    }

    public NodoN getNodoInicial() {
        return nodoInicial;
    }

    public void setNodoInicial(NodoN nodoInicial) {
        this.nodoInicial = nodoInicial;
    }

    public BolsaDeConjsNodosN getBolsaCI() {
        return bolsaCI;
    }

    public void setBolsaCI(BolsaDeConjsNodosN bolsaCI) {
        this.bolsaCI = bolsaCI;
    }

    public BolsaDeConjsNodosN getBolsaCDP() {
        return bolsaCDP;
    }

    public void setBolsaCDP(BolsaDeConjsNodosN bolsaCDP) {
        this.bolsaCDP = bolsaCDP;
    }

    /**
     *
     * @param dirArchivo es el archivo (incluso path) desde el que se lee el GrafoN
     * @param cantPasos es la cantidad de pasos del GrafoN
     * @param impDet si es true hace que se genere una impresión detallada.
     * @return grafoN es el grafo creado y leído.
     * @throws XcargaDatos
     */
    public static GrafoN creaYLeeGrafoN(String dirArchivo, int cantPasos,
            boolean impDet) throws XcargaDatos {
        GrafoN grafoN;
        CargaGrafoN2 cargadorGN = new CargaGrafoN2() {
        };
        grafoN = cargadorGN.cargarGrafoN(dirArchivo, cantPasos, impDet);
        return grafoN;
    }

    public GrafoN pasaAGrafoEtapas(DatosGeneralesEstudio datosGenerales) throws XcargaDatos {
        GrafoN grafoEtapas;
        grafoEtapas = PasaAGrafoEtapas.pasarAGrafoEtapas(this, datosGenerales);
        return grafoEtapas;
    }

    public void agregaNodosEtapa(ArrayList<NodoN> nodopaso) {
        // agrega un paso de tiempo de nodos
        ArrayList<NodoN> aux = new ArrayList<NodoN>();
        for (NodoN n : nodopaso) {
            aux.add(n);
        }
        nodos.add(aux);
    }

    /**
     * Devuelve la variable de la naturaleza de nombre nombre.
     *
     * @param nombre es el nombre de la variable que se busca.
     * @return variable es la variable de la naturaleza asociada.
     * @throws persistencia.XcargaDatos en caso de que no exista ninguna
     * variable de la naturaleza del nombre buscado.
     */
    public VariableNat devuelveVN(String nombre) throws XcargaDatos {

        VariableNat variable = null;
        boolean encontro = false;

        for (VariableNat vn : this.varGrafo) {
            if (vn.getNombre().equalsIgnoreCase(nombre)) {
                variable = vn;
                encontro = true;
            }
        }
        if (!encontro) {
            throw new XcargaDatos("La variable de la naturaleza " + nombre + " no existe");
        }
        return variable;
    }

    /**
     * Devuelve los valores (Strings) de las variables de la naturaleza
     * pedidas, en el nodo de paso de etapa ie y ordinal ord
     */
    public ArrayList<String> devuelveValoresVN(ArrayList<VariableNat> variables, int ie, int ord)
            throws XcargaDatos {

        ArrayList<String> auxS = new ArrayList<String>();
        for (VariableNat vn : variables) {
            auxS.add(nodos.get(ie).get(ord).getValoresVN().get(varGrafo.indexOf(vn)));
        }
        return auxS;
    }

    public boolean variableVale(NodoN nod, VariableNat vn, String val) {
        /** Devuelve true si en el nodo nod (que tiene que ser del grafo)
         *  la variable de la naturaleza vn toma el valor val
         */
        boolean vale = false;
        if (nod.getValoresVN().get(varGrafo.indexOf(vn)).equalsIgnoreCase(val)) {
            vale = true;
        }
        return vale;
    }

    public String valorVariableNat(NodoN nod, VariableNat vn) {
        /**valorVariableNat
         * Devuelve el valor que toma la variable de la naturaleza vn
         * en el nodo nod
         */
        String valor = nod.getValoresVN().get(varGrafo.indexOf(vn));
        return valor;
    }

    /**
     * Devuelve el nodo que en el paso paso tiene VariablesNat con valores valores
     * o null si no hay tal nodo
     * @param paso es el paso de tiempo
     * @valores es la lista de los valores de las VariableNat en el orden en que sparecen en el grafo,
     * excluyendo el de la variable nada, que todo GrafoN tiene por defecto
     */
    public NodoN nodoDeValores(int paso, ArrayList<String> valores) {
        NodoN nodoN = null;
        ArrayList<NodoN> nodosPaso = nodos.get(paso);
        for (NodoN n : nodosPaso) {
            boolean igual = true;
            ArrayList<String> valoresN = n.getValoresVN();
            for (int i = 0; i < valores.size(); i++) {
                // se empieza de i=1 porque la variable nada es irrelevante
                if (!(valoresN.get(i + 1).equalsIgnoreCase(valores.get(i)))) {
                    // los valores de una VariableNat no coinciden
                    igual = false;
                    break;
                }
            }
            if (igual == true) {
                nodoN = n;
                break;
            }
        }
        return nodoN;
    }

    public int pasoDeUnaEtapa(int etapa) {
        int paso;
        paso = this.getPasoDeEtapa().get(etapa);
        return paso;
    }

    public GrafoN componeGrafo(GrafoN grafoAdic, int num) throws XcargaDatos {
        /**
         * Compone el grafo this con el nuevo GrafoN grafoAdic, en la
         * hipótesis de que son independientes.
         *
         * El grafo que llama este procedimiento debe
         * tener cargada la cantidad de etapas para que funcione
         * num es un rotulo numérico sin otra finalidad
         *
         * Si el grafo this que llama al procedimiento tiene cargados en
         * los nodos el conjunto de información al que pertenecen, los nodos
         * del grafo agregado que se generan a partir de él heredan dicho CI
         */
        GrafoN grafoAgregado = new GrafoN(cantEtapas, "grafo agregado con " + num + " componentes", true);

        if(grafoAdic.cantEtapas < cantEtapas)throw new XcargaDatos("La componente "
                + grafoAdic.getNombre() + "tiene menos etapas que " + cantEtapas);
        
        if (cantEtapas != grafoAdic.cantEtapas) {
            int sigueComponiendo = JOptionPane.showConfirmDialog(null,
                    "Las componentes tienen distinto largo: " + cantEtapas +"y"+ grafoAdic.cantEtapas);
            if (! (sigueComponiendo == JOptionPane.OK_OPTION) ) {
                throw new XcargaDatos("Se quieren componer grafos " + nombre + " y " +
                    grafoAdic.nombre + " con distintos etapas de tiempo");
            }
        }



        // Crea lista de variables
        ArrayList<VariableNat> vnat = new ArrayList<VariableNat>();
        vnat.addAll(varGrafo);
        vnat.addAll(grafoAdic.getVarGrafo());
        grafoAgregado.setVarGrafo(vnat);
        /* Al hacer los productos cartesianos de nodos, el ìndice en el grafo compAdic es el
         * que varía más rapidamente
         * */
        int ordinal;
        double probSi;
        double probSj;
        NodoN nAgr;  // nodo del grafo agregado de ambos
        NodoN nAdic; // nodo del grafo a adicionar
        ArrayList<NodoN> nodost;
        for (int t = 0; t <= cantEtapas; t++) {
            ordinal = 0;
            nodost = new ArrayList<NodoN>();
            for (int i = 0; i < nodos.get(t).size(); i++) {
                for (int j = 0; j < grafoAdic.getNodos().get(t).size(); j++) {
                    ordinal++;
                    nAgr = new NodoN();
                    nAgr.setTiempo(t);
                    nAgr.setOrdinal(ordinal);
                    NodoN n = nodos.get(t).get(i);
                    /**
                     * El nodo de grafoAgregado hereda el conjunto
                     * de información de su padre del grafo this,
                     * si el padre tiene CI, y análogamente con el
                     * conjunto determinante de parque
                     */
                    if (!(n.getConjInfo() == null)) {
                        nAgr.setConjInfo(n.getConjInfo());
                    }
                    if (!(n.getConjDetParque() == null)) {
                        nAgr.setConjDetParque(n.getConjDetParque());
                    }
                    ArrayList<String> valVN = new ArrayList<String>();
                    valVN.addAll(nodos.get(t).get(i).getValoresVN());
                    valVN.addAll(grafoAdic.getNodos().get(t).get(j).getValoresVN());
                    nAgr.setValoresVN(valVN);
                    nodost.add(nAgr);
                    /**
                     *  En nodost se van cargando los nodos del grafo agregado
                     *  datados en t
                     */
                }
            }
            grafoAgregado.getNodos().add(nodost);
        }
        // Crea los vectores de sucesores de los nodos
        ArrayList<NodoN> suc;
        ArrayList<Double> aprob;
        for (int t = 0; t < cantEtapas; t++) {
            // recorro los nodos del grafo agregado en el orden en que se generaron por el producto cartesiano
            ordinal = 0;
            for (int i = 0; i < nodos.get(t).size(); i++) {
                for (int j = 0; j < grafoAdic.getNodos().get(t).size(); j++) {
                    ordinal++;
                    // empieza el tratamiento de un nodo del grafo agregado para generar sus sucesores
                    nAgr = grafoAgregado.getNodos().get(t).get(ordinal - 1);
                    NodoN n = nodos.get(t).get(i);
                    nAdic = grafoAdic.getNodos().get(t).get(j);
                    suc = new ArrayList<NodoN>();  // en suc se cargarán los sucesores de n
                    aprob = new ArrayList<Double>();
                    for (NodoN s : n.getSucesores()) {
                        for (NodoN sAdic : nAdic.getSucesores()) {
                            int inds = n.getSucesores().indexOf(s); // el lugar que ocupa s entre los sucesores de n (empezando en 0)
                            double probs = n.getProbSucesores().get(inds);
                            int indsAdic = nAdic.getSucesores().indexOf(sAdic);    // el lugar que ocupa sAdic entre los sucesores de nAdic (empezando en 0)
                            double probsAdic = nAdic.getProbSucesores().get(indsAdic);
                            int ordSucAgr = (s.getOrdinal() - 1) * grafoAdic.getNodos().get(t + 1).size() +
                                    sAdic.getOrdinal();
                            /* ordSucAgr es el ordinal en el grafo agregado del sucesor hallado para nAgr
                             * ese sucesor vive en t+1
                             * */
                            suc.add(grafoAgregado.getNodos().get(t + 1).get(ordSucAgr - 1));
                            aprob.add(probs * probsAdic);
                        }
                    }
                    nAgr.setSucesores(suc);
                    nAgr.setProbSucesores(aprob);
                    // termina el tratamiento de un nodo del grafo agregado para generar sus sucesores
                }
            }

        }
        return grafoAgregado;
    }

    public void cargaNodos(ArrayList<NodoN> listaNodos) {
        /**
         *
         *  Para ejecutar la carga de nodos debe existir un grafo
         *  recién creado y con su ArrayList<NodoN> nodos vacío
         *
         *  listaNodos debe estar ordenada por paso de tiempo y dentro
         *  del paso de tiempo por ordinal, comenzando del paso de tiempo
         *  cero
         */
        int t = -1;
        for (NodoN n : listaNodos) {
            if (n.getTiempo() != t) {
                t = n.getTiempo();
                ArrayList<NodoN> aux = new ArrayList<NodoN>();
                nodos.add(aux);
            }
            nodos.get(t).add(n);
        }
        cantEtapas = t;
    }

    /**
     * METODO cargaVarGrafo
     * Carga en el grafo los objetos variables de la naturaleza
     * con sus nombres, valores y observabilidad, a partir de
     * los strings respectivos
     *
     * @param variables es la lista de nombres de variables a crear
     *
     * @param valoresDe tiene ordenados primero por variable y luego por valor
     *
     * @param valObs tiene la observabilidad de la variable
     * ATENCION: lo observable son las componentes de grafo, todas las variablesN
     * de una componente son o no observables según la componente lo sea
     */
    public void cargaVarGrafo(ArrayList<String> variables,
            ArrayList<ArrayList<String>> valoresDe, ArrayList<Boolean> valObs) {

        VariableNat varN = null;

        for (int i = 0; i < variables.size(); i++) {

            ArrayList<String> auxS = new ArrayList<String>();
            varN = new VariableNat();

            varN.setNombre(variables.get(i));
            for (int j = 0; j < valoresDe.get(i).size(); j++) {
                auxS.add(valoresDe.get(i).get(j));
            }
            varN.setValores(auxS);
            varN.setObservadaEnOptim(valObs.get(i));

            ArrayList<Boolean> auxB = new ArrayList<Boolean>();
            varGrafo.add(varN);
        }
    }

    /**
     * METODO cargaArcos
     *
     * Carga los arcos del grago a partir de una lista de arcos
     * Para ejecutar la carga de arcos deben preexistir todos los nodos
     * del grafo, de lo contrario hay error
     * El procedimiento carga los sucesores de los nodos y sus probabilidades
     * y calcula las probabilidades absolutas de los nodos
     *
     * RECORDAR QUE ES NECESARIO AGREGAR LOS ARCOS ENTRE T=0 Y T=1 CUANDO EN T=1
     * ES POSIBLE MÁS DE UN ESTADO DE LAS VARIABLES
     *
     * @param listaArcos es la lista de arcos a agregar
     */
    public void cargaArcos(ArrayList<Arco> listaArcos) {

        for (Arco ar : listaArcos) {
            ar.getNodoIni().getSucesores().add(ar.getNodoFin());
            ar.getNodoIni().getProbSucesores().add(ar.getProbArco());
        }
    }

    /**
     * ESTE METODO SE MANTIENE SOLO POR COMPATIBILIDAD CON RUTINAS VIEJAS.
     * Calculan las probabilidades absolutas de los nodos
     * suponiendo que están calculadas las probabilidades de
     * los sucesores de cada nodo
     *
     */
    public void cargaProb() throws XcargaDatos {
        Double prob;
        Double epsilon = 0.0000000001;
        nodos.get(0).get(0).setProbabilidad(1.0);
        for (int t = 1; t <= cantEtapas; t++) {
            for (NodoN n : nodos.get(t)) {
                n.setProbabilidad(0.0);
            }
        }
        for (int t = 0; t < cantEtapas; t++) {
            double sumaprob = 0;
            for (NodoN n : nodos.get(t)) {
                int is = 0;     // is recorre los sucesores s del nodo n
                for (NodoN s : n.getSucesores()) {
                    s.setProbabilidad(
                            s.getProbabilidad() +
                            n.getProbabilidad() * n.getProbSucesores().get(is));
                    is++;
                }
                sumaprob += n.getProbabilidad();
            }
            if (Math.abs(sumaprob - 1) > epsilon) {
                throw new XcargaDatos("Las probabilidades de nodos no suman 1 " +
                        " en etapa " + t);
            }
        }
    }

    /**
     * Calcula las probabilidades absolutas de los nodos
     * suponiendo que están calculadas las probabilidades de
     * los sucesores de cada nodo y partiendo del nodo inicial de grafo
     *
     */
    public void cargaProb2() throws XcargaDatos {
        Double prob;
        Double epsilon = 0.0000000001;

        for (int t = 1; t <= cantEtapas; t++) {
            for (NodoN n : nodos.get(t)) {
                n.setProbabilidad(0.0);
            }
        }

        nodoInicial.setProbabilidad(1.0);

        for (int t = 1; t < cantEtapas; t++) {
            double sumaprob = 0;
            for (NodoN n : nodos.get(t)) {
                int is = 0;     // is recorre los sucesores s del nodo n
                for (NodoN s : n.getSucesores()) {
                    s.setProbabilidad(
                            s.getProbabilidad() +
                            n.getProbabilidad() * n.getProbSucesores().get(is));
                    is++;
                }
                sumaprob += n.getProbabilidad();
            }
            if (Math.abs(sumaprob - 1) > epsilon) {
                throw new XcargaDatos("Las probabilidades de nodos no suman 1 " +
                        " en etapa " + t);
            }
        }
    }

    /**
     * Para que el GrafoN se emplee en un Caso de un Estudio, carga los valores de TipoDeDatos
     * para los nodos del GrafoN.
     * @param est el Estudio al que se asocia el GrafoN
     * @param caso el Caso dentro del Estudio al que
     */
    public void cargaValoresTiposDatos(Estudio est, Caso caso) throws XcargaDatos {
        // El nodo de etapa cero no tiene valores porque no se simulará en etapa cero.
        for (int e = 1; e < nodos.size(); e++) {
            for (NodoN nN : nodos.get(e)) {
                nN.cargaValoresTiposDatos(est, caso);
            }
        }
    }

//	public double[][] matrizTransicion(int t1) throws XcargaDatos {
//	// ESTO CREO QUE FINALMENTE NO SE TERMINO USANDO
//		/**
//	 * Construye la matriz de probabilidades de transición de la etapa t1
//	 * a la siguiente de un grafo tomando las probabilidades del grafo de la naturaleza.
//	 * Las filas de la matriz (primer índice) corresponden a ordinal del nodo
//	 * inicial en t1
//	 * Las columnas de la matriz (segundo índice) corresponden a ordinal del nodo
//	 * final en t1+1
//	 */
//		if (t1==cantEtapas){
//			throw new XcargaDatos("error en matriz de transición");
//		}
//		double [][] mat = new double [nodos.get(t1).size()][nodos.get(t1+1).size()];
//
//		for (NodoN n: nodos.get(t1)){
//			int i = 0;
//			for (NodoN m: nodos.get(t1+1)){
//				int j = 0;
//				mat[i][j]= nodos.get(t1).get(i).getProbSucesores().get(j);
//				j++;
//			}
//			i++;
//		}
//		return mat;
//	}
    /**
     * Devuelve los NodoN de la etapa (o paso) etapa.
     * @param etapa
     * @return
     */
    public ArrayList<NodoN> nodosEtapa(int etapa) {
        ArrayList<NodoN> nodosR = new ArrayList<NodoN>();
        nodosR.addAll(nodos.get(etapa));
        return nodosR;
    }

    /**
     * Devuelve el nodo de la etapa etapa y ordinal ordinal
     * @param etapa
     * @param ordinal
     * @return
     */
    public NodoN nodoEtapaOrdinal(int etapa, int ordinal) throws XcargaDatos {

        ArrayList<NodoN> nodEtap = nodosEtapa(etapa);
        for (int inod = 0; inod < nodEtap.size(); inod++) {
            NodoN n = nodEtap.get(inod);
            if (n.getOrdinal() == ordinal) {
                return n;
            }
        }
        throw new XcargaDatos("no se encontro el NodoN de etapa " + etapa + " y ordinal " +
                ordinal);
    }

    @Override
    public String toString() {
        String texto = "";
        texto += "=======================================================" + "\r\n";
        texto += "COMIENZA GRAFO DE LA NATURALEZA: " + nombre + "\n";
        texto += "=======================================================" + "\r\n";
        texto += "Variables de la naturaleza del grafo: " + "\n";
        for (VariableNat v : varGrafo) {
            texto += v.toString() + "\n";
        }

        for (int t = 0; t < nodos.size(); t++) {
            texto += "COMIENZAN NODOS DE ETAPA = " + t + " - paso " + pasoDeEtapa.get(t) + "\n";
            for (int i = 0; i < nodos.get(t).size(); i++) {
                texto += "Grafo: " + nombre + "\n";
                texto += nodos.get(t).get(i).toString();
            }
            texto += "\n";
        }
        texto += "Nodo Inicial " + nodoInicial.toString();
        texto += "FIN DEL GRAFO DE LA NATURALEZA" + "\n";
        texto += "=======================================================" + "\n";
        return texto;
    }

    public String toStringCorto() {
        String texto = "";
        texto += "=======================================================" + "\r\n";
        texto += "COMIENZA GRAFO DE LA NATURALEZA: " + nombre + "\r\n";
        texto += "=======================================================" + "\r\n";
        texto += "Variables de la naturaleza del grafo: " + "\r\n";
        for (VariableNat v : varGrafo) {
            texto += v.toString() + "\r\n";
        }

        for (int t = 0; t < nodos.size(); t++) {
            texto += "\r\n";
            texto += "COMIENZAN NODOS DE ETAPA = " + t + " - paso " + pasoDeEtapa.get(t) + "\r\n";
            for (int i = 0; i < nodos.get(t).size(); i++) {
                texto += "Grafo: " + nombre + "\r\n";
                texto += nodos.get(t).get(i).toStringCorto();
            }
            texto += "\r\n";
        }
        texto += "NODO INICIAL DEL GRAFO " + nodoInicial.getOrdinal();
        texto += "FIN DEL GRAFO DE LA NATURALEZA" + "\r\n";
        texto += "=======================================================" + "\r\n";
        return texto;
    }

    public static void main(String[] args) throws ClassNotFoundException, XcargaDatos {
        ArrayList<String> auxS;
        // CREA GRAFO comp1

        VariableNat combus = new VariableNat();
        auxS = new ArrayList<String>();
        ArrayList<NodoN> alist = new ArrayList<NodoN>();
        auxS.add("gas bajo");
        auxS.add("gas alto");
        combus.setValores(auxS);
        GrafoN comp1 = new GrafoN(3, "comp1", true);
        comp1.getVarGrafo().add(combus);
        ArrayList<Arco> arcos = new ArrayList<Arco>();
        auxS = new ArrayList<String>();
        NodoN n01 = new NodoN(0, 1);
        auxS.add("gas bajo");
        NodoN n11 = new NodoN(1, 1, auxS);
        NodoN n21 = new NodoN(2, 1, auxS);
        NodoN n31 = new NodoN(3, 1, auxS);
        auxS = new ArrayList<String>();
        auxS.add("gas alto");
        NodoN n12 = new NodoN(1, 2, auxS);
        NodoN n22 = new NodoN(2, 2, auxS);
        NodoN n32 = new NodoN(3, 2, auxS);
        alist = new ArrayList<NodoN>();
        alist.add(n01);
        comp1.agregaNodosEtapa(alist);
        alist = new ArrayList<NodoN>();
        alist.add(n11);
        alist.add(n12);
        comp1.agregaNodosEtapa(alist);
        alist = new ArrayList<NodoN>();
        alist.add(n21);
        alist.add(n22);
        comp1.agregaNodosEtapa(alist);
        alist = new ArrayList<NodoN>();
        alist.add(n31);
        alist.add(n32);
        comp1.agregaNodosEtapa(alist);
        Arco a0111 = new Arco(n01, n11, 0.5);
        Arco a0112 = new Arco(n01, n12, 0.5);
        Arco a1121 = new Arco(n11, n21, 0.5);
        Arco a1122 = new Arco(n11, n22, 0.5);
        Arco a1221 = new Arco(n12, n21, 0.3);
        Arco a1222 = new Arco(n12, n22, 0.7);
        Arco a2131 = new Arco(n21, n31, 0.5);
        Arco a2132 = new Arco(n21, n32, 0.5);
        Arco a2231 = new Arco(n22, n31, 0.3);
        Arco a2232 = new Arco(n22, n32, 0.7);
        arcos.add(a0111);
        arcos.add(a0112);
        arcos.add(a1121);
        arcos.add(a1122);
        arcos.add(a1221);
        arcos.add(a1222);
        arcos.add(a2131);
        arcos.add(a2132);
        arcos.add(a2231);
        arcos.add(a2232);
        comp1.cargaArcos(arcos);
        comp1.cargaProb();

        System.out.print(comp1.toString());

        // CREA GRAFO comp2
        combus.setNombre("Demanda");
        VariableNat demanda = new VariableNat();
        demanda.setNombre("demanda");
        auxS = new ArrayList<String>();
        alist = new ArrayList<NodoN>();
        auxS.add("dem baja");
        auxS.add("dem alta");
        demanda.setValores(auxS);
        GrafoN comp2 = new GrafoN(3, "comp2", true);

        comp2.getVarGrafo().add(demanda);
        arcos = new ArrayList<Arco>();
        auxS = new ArrayList<String>();
        auxS.add("dem baja");
        NodoN m01 = new NodoN();
        NodoN m11 = new NodoN(1, 1, auxS);
        NodoN m21 = new NodoN(2, 1, auxS);
        NodoN m31 = new NodoN(3, 1, auxS);
        auxS = new ArrayList<String>();
        auxS.add("dem alta");
        NodoN m12 = new NodoN(1, 2, auxS);
        NodoN m22 = new NodoN(2, 2, auxS);
        NodoN m32 = new NodoN(3, 2, auxS);
        alist = new ArrayList<NodoN>();
        alist.add(m01);
        comp2.agregaNodosEtapa(alist);
        alist = new ArrayList<NodoN>();
        alist.add(m11);
        alist.add(m12);
        comp2.agregaNodosEtapa(alist);
        alist = new ArrayList<NodoN>();
        alist.add(m21);
        alist.add(m22);
        comp2.agregaNodosEtapa(alist);
        alist = new ArrayList<NodoN>();
        alist.add(m31);
        alist.add(m32);
        comp2.agregaNodosEtapa(alist);
        a0111 = new Arco(m01, m11, 0.5);
        a0112 = new Arco(m01, m12, 0.5);
        a1121 = new Arco(m11, m21, 0.5);
        a1122 = new Arco(m11, m22, 0.5);
        a1221 = new Arco(m12, m21, 0.3);
        a1222 = new Arco(m12, m22, 0.7);
        a2131 = new Arco(m21, m31, 0.5);
        a2132 = new Arco(m21, m32, 0.5);
        a2231 = new Arco(m22, m31, 0.3);
        a2232 = new Arco(m22, m32, 0.7);
        arcos.add(a0111);
        arcos.add(a0112);
        arcos.add(a1121);
        arcos.add(a1122);
        arcos.add(a1221);
        arcos.add(a1222);
        arcos.add(a2131);
        arcos.add(a2132);
        arcos.add(a2231);
        arcos.add(a2232);
        comp2.cargaArcos(arcos);
        comp2.cargaProb();

        System.out.print(comp2.toString());

        GrafoN grafoFinal = new GrafoN();
        grafoFinal = comp1.componeGrafo(comp2, 2);

        System.out.print(grafoFinal.toString());

        try {

            FileOutputStream fos = new FileOutputStream("t.tmp");
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(grafoFinal);
            oos.close();

        } catch (ObjectStreamException e1) {
            System.out.println(e1.toString());
        } catch (FileNotFoundException e2) {
            System.out.println(e2.toString());
        } catch (IOException e3) {
            System.out.println(e3.toString());
        }


        System.out.print("AHORA VIENE EL OBJETO RECUPERADO");

        GrafoN grafoFinal2 = new GrafoN();
        try {

            FileInputStream fis = new FileInputStream("t.tmp");
            ObjectInputStream ois = new ObjectInputStream(fis);

            grafoFinal2 = (GrafoN) ois.readObject();
            ois.close();

        } catch (ObjectStreamException e1) {
            System.out.println(e1.toString());
        } catch (FileNotFoundException e2) {
            System.out.println(e2.toString());
        } catch (IOException e3) {
            System.out.println(e3.toString());
        }

        System.out.print(grafoFinal2.toString());
    }
}
