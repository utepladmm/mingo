/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaleza3;

import java.io.Serializable;
import java.util.ArrayList;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class ConjNodosN implements Serializable {

     /**
      *
      * Conjunto de NodoN (CNN) del grafo que tiene las siguientes propiedades:
      *
      * Conjunto de Información (CI): CNN que agrupa todos los NodoN entre los que no
      * se puede distinguir porque tienen los mismos estados de las variables observadas
      *
      * Conjunto que determina parque (CDP):
      * Conjunto determinante de parque es cada conjunto de nodos del grafo
      * de la naturaleza completo, asociados a un único estado (nodo)
      * del grafo de las variables observadas y variables que determinan el parque
      * Si las variables que determinan el parque son observadas
      * bolsaCDT y bolsaCI son iguales.
      *
      * Las variables que determinan parque son las de cambio aleatorio, inversión aleatoria
      * y demora aleatoria.*
      *
      *
      */
     private GrafoN grafoN;  // grafo al que pertenece
     private TConjNodoN tipoConj; // si es un conjunto de información
     private int etapa;   // etapa del grafo asociado a la que pertenece el CNN
     private int tiempo;	 // paso de tiempo al que pertenece el CNN
     private int ordinal;
     /**
      * ordinal entre los CNN de la etapa empezando en 1
      * se asignan los ordinales según aparecen los nuevos nodos por su ordinal
      * de nodo
      */
     private ArrayList<String> valoresObservadas;
     /**
      * valores de las variables observadas en el orden en que
      * aparecen en el grafo
      * Las variables observadas se cargan en el grafo antes que las no observadas
      * Las variables NO observadas sólo pueden aparecer en componentes en
      * las que no hay variables observadas.
      * Las variables no observadas y las observadas deben ser indepencientes}
      * entre sí.
      */
     private ArrayList<String> valoresDetP;
     /**
      * valores de las variables que determinan el parque y no son
      * observadas en el orden en que aparecen en el grafo
      */
     private ConjNodosN cIAsociado;
     /**
      * Si el conjunto de nodos es un conjunto que determina parque, el conjunto
      * de información asociado.
      */
     private ArrayList<ConjNodosN> cDPsAsociados;
     /**
      * Si el conjunto de nodos es un conjunto de información, la lista
      * de conjuntos que determinan parque asociados
      */
     private ArrayList<NodoN> nodos;
     /**
      * contiene los nodos del grafoN que pertenecen al CI
      */
     private ArrayList<ConjNodosN> sucesores;
     private ArrayList<Double> probSucesores;

     /**
      * Conjuntos de información sucesores y sus probabilidades respectivas
      * en el orden en que aparecen
      */
     public enum TConjNodoN {

          /**
           * Tipo de conjunto de nodosN
           */
          Info, // conjuntos de información
          DetParque; // conjuntos que determinan parque
     }

     public ConjNodosN(GrafoN grafoN, int etapa) {
          this.grafoN = grafoN;
          this.etapa = etapa;
          tiempo = grafoN.getPasoDeEtapa().get(etapa);
//		this.ordinal = ordinal;
          valoresObservadas = new ArrayList<String>();
          valoresDetP = new ArrayList<String>();
          nodos = new ArrayList<NodoN>();
          sucesores = new ArrayList<ConjNodosN>();
          probSucesores = new ArrayList<Double>();
          cDPsAsociados = new ArrayList<ConjNodosN>();
     }

     public ConjNodosN(GrafoN grafoN, int etapa, ArrayList<String> valoresObservadas) {
          this.grafoN = grafoN;
          this.etapa = etapa;
          tiempo = grafoN.getPasoDeEtapa().get(etapa);
//		this.ordinal = ordinal;
          this.valoresObservadas = valoresObservadas;
          valoresDetP = new ArrayList<String>();
          nodos = new ArrayList<NodoN>();
          sucesores = new ArrayList<ConjNodosN>();
          probSucesores = new ArrayList<Double>();
          cDPsAsociados = new ArrayList<ConjNodosN>();
     }

     public GrafoN getGrafoN() {
          return grafoN;
     }

     public void setGrafoN(GrafoN grafoN) {
          this.grafoN = grafoN;
     }

     public int getEtapa() {
          return etapa;
     }

     public void setEtapa(int etapa) {
          this.etapa = etapa;
     }

     public int getTiempo() {
          return tiempo;
     }

     public void setTiempo(int tiempo) {
          this.tiempo = tiempo;
     }

     public ArrayList<NodoN> getNodos() {
          return nodos;
     }

     public void setNodos(ArrayList<NodoN> nodos) {
          this.nodos = nodos;
     }

     public int getOrdinal() {
          return ordinal;
     }

     public void setOrdinal(int ordinal) {
          this.ordinal = ordinal;
     }

     public ArrayList<String> getValoresObservadas() {
          return valoresObservadas;
     }

     public TConjNodoN getTipoConj() {
          return tipoConj;
     }

     public void setTipoConj(TConjNodoN tipoConj) {
          this.tipoConj = tipoConj;
     }

     public void setValoresObservadas(ArrayList<String> valoresObservadas) {
          this.valoresObservadas = valoresObservadas;
     }

     public ConjNodosN getCIAsociado() {
          return cIAsociado;
     }

     public void setCIAsociado(ConjNodosN cIAsociado) {
          this.cIAsociado = cIAsociado;
     }

     public ArrayList<ConjNodosN> getCDPsAsociados() {
          return cDPsAsociados;
     }

     public void setCDPsAsociados(ArrayList<ConjNodosN> cDPsAsociados) {
          this.cDPsAsociados = cDPsAsociados;
     }

     public ArrayList<String> getValoresDetP() {
          return valoresDetP;
     }

     public void setValoresDetP(ArrayList<String> valoresDetP) {
          this.valoresDetP = valoresDetP;
     }

     public ArrayList<Double> getProbSucesores() {
          return probSucesores;
     }

     public void setProbSucesores(ArrayList<Double> probSucesores) {
          this.probSucesores = probSucesores;
     }

     public ArrayList<ConjNodosN> getSucesores() {
          return sucesores;
     }

     public void setSucesores(ArrayList<ConjNodosN> sucesores) {
          this.sucesores = sucesores;
     }

     public boolean nodoPerteneceAlCI(NodoN n) {
          // devuelve true o false según el nodo pertenezca o no al
          // conjunto de información
          // si el CI está vacío devuelve false
          // verifica que el nodo sea de la etapa correcta
          assert (n.getTiempo() == tiempo) : "ASSERTION ERRADA: " +
                  "no coincide el tiempo de un nodo y un CI" + "tiepo del nodo" + n.getTiempo();
          boolean pertenece = false;
          if (!nodos.isEmpty()) {
               int iv = 0;
               pertenece = true;
               do {
                    if (!valoresObservadas.get(iv).equalsIgnoreCase(n.getValoresObservadas(grafoN).get(iv))) {
                         pertenece = false;
                    }
                    iv++;
               } while (iv < valoresObservadas.size() && pertenece == true);

               return pertenece;
          } else {
               return pertenece;
          }
     }

     public NodoN nodoNDeOrdinal(int ordinal) throws XcargaDatos{
         for(NodoN n: nodos){
             if (n.getOrdinal()==ordinal){
                 return n;
             }
         }
         throw new XcargaDatos("se pidió un ordinal que no existe en un ConjNodosN");
     }

     @Override
     public String toString() {
          String texto = "\n";
          if (tipoConj == TConjNodoN.Info) {
               texto += "COMIENZA CONJUNTO DE INFORMACION" + "\n";
               texto += "Etapa del conjunto: " + etapa + "\n";
          } else {
               texto += "COMIENZA CONJUNTO DETERMINANTE DE PARQUE" + "\n";
               texto += "Etapa del conjunto: " + etapa + "\n";
               texto += "Conjunto de información asociado ordinal " + cIAsociado.getOrdinal();
          }
          texto += "\n";
          texto += "Ordinal del Conjunto en su etapa: " + ordinal + "\n";
          texto += "Valores de las variables de la naturaleza observadas :";
          for (String s : valoresObservadas) {
               texto += s + "\t";
          }
          texto += "\n";
          texto += "Valores de las VN que determinan parque :";
          for (String s : valoresDetP) {
               texto += s + "\t";
          }
          texto += "\n";

          texto += "Enumeración de nodos del CI" + "\n";
          for (NodoN n : nodos) {
               texto += "NodoN - Etapa: " + etapa + " - Ordinal NodoN: " + n.getOrdinal() + "\n";
          }
          texto += "\n";
          texto += "Conjuntos sucesores:";
          texto += "\n";
          int is = 0;
          for (ConjNodosN suc : sucesores) {
               texto += "Etapa " + (etapa + 1) + " - Ordinal del Conjunto sucesor - " + suc.getOrdinal() + " - prob. sucesor = " +
                       probSucesores.get(is);
               texto += "\n";
               is++;
          }
          return texto;
     }

     public String toStringCorto() {
          String texto = "\n";
          if (tipoConj == TConjNodoN.Info) {
               texto += "COMIENZA CONJUNTO DE INFORMACION" + "\n";
               texto += "Etapa del conjunto: " + etapa + "\n";
          } else {
               texto += "COMIENZA CONJUNTO DETERMINANTE DE PARQUE" + "\n";
               texto += "Etapa del conjunto: " + etapa + "\n";
               texto += "Conjunto de información asociado ordinal " + cIAsociado.getOrdinal();
          }
          texto += "\n";
          texto += "Ordinal del Conjunto en su etapa: " + ordinal + "\n";
          texto += "Valores de las variables de la naturaleza observadas :";
          for (String s : valoresObservadas) {
               texto += s + "\t";
          }
          texto += "\n";
          texto += "Valores de las VN que determinan parque :";
          for (String s : valoresDetP) {
               texto += s + "\t";
          }
          texto += "\n";

          return texto;
     }
}
