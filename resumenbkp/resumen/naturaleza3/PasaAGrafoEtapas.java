/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaleza3;

import UtilitariosGenerales.OperMatrices;
import dominio.DatosGeneralesEstudio;
import java.util.ArrayList;
import persistencia.CargaDatosGenerales;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class PasaAGrafoEtapas {
     // toma un grafo de la naturaleza de pasos completos
     // y a partir de él crea otro reducido con las etapas definidas en
     // los datos generales de un estudio

     public static GrafoN pasarAGrafoEtapas(GrafoN g, DatosGeneralesEstudio datosGen) throws XcargaDatos {
          if (g.isPasosCompletos() == false) {
               throw new XcargaDatos("el grafo inicial" +
                       " que se quiere pasar a etapas no es de pasos completos");
          }
         /**
           * Construye un nuevo grafo de estados de la naturaleza en los pasos representativos
           * definidos para el estudio, a partir del grafo corriente de pasos completos,
           * es decir con una etapa por paso
           */
          //agregado 17 oct 2011
          int cantPasos = datosGen.getCantPasos();
          int viejasEtapas = datosGen.getCantEtapas();
          int nuevasEtapas = 0;
          for(int ie = 1; ie<= viejasEtapas; ie++){
              if(datosGen.pasoFinDeEtapa(ie)==cantPasos){
                  nuevasEtapas = ie;
                  break;
              }
          }
          // fin agregado
//          int nuevasEtapas = datosGen.getEtapasPasos().size() - 1;
          // nuevasEtapas es la cantidad de etapas adicionales al paso 0 del grafo por etapas que se crea
          GrafoN gEtapas = new GrafoN(nuevasEtapas, "Grafo por bloques de pasos de tiempo", false);
          ArrayList<Integer> auxInt = new ArrayList<Integer>();
          auxInt.add(0);
          for(int i=1; i<=nuevasEtapas; i++){
              auxInt.add(datosGen.pasoRepDeEtapa(i));
          }
          gEtapas.setPasoDeEtapa(auxInt);
          gEtapas.setVarGrafo(g.getVarGrafo());


          ArrayList<NodoN> auxAN = new ArrayList<NodoN>();

          for (int it = 0; it <= nuevasEtapas; it++) {
               // pasoRIt = paso de tiempo representativo de la etapa it-ésima del estudio
               int pasoRIt = datosGen.getEtapasPasos().get(it)[2];

               /**
                * Copia los nodos relevantes sin los sucesores
                * los nodos copiados conservan el orden dentro de cada etapa
                * y el ordinal.
                */
               auxAN = new ArrayList<NodoN>();
               for (NodoN n : g.getNodos().get(pasoRIt)) {
                    NodoN nuevoNodo;
                    nuevoNodo = n.copiaParcial();
                    auxAN.add(nuevoNodo);
                    // Identifica el nodoInicial del grafo gEtapas
                    if (it == 1 && n == g.getNodoInicial()) {
                         gEtapas.setNodoInicial(nuevoNodo);
                    }
               }
               gEtapas.getNodos().add(auxAN);

               /**
                * Carga la bolsaCI
                * Emplea la información de ordinal de los NodoN que pertenecen a cada
                * conjunto de información
                */
               ArrayList<ConjNodosN> auxACI = new ArrayList<ConjNodosN>();
               for(ConjNodosN cInf: g.getBolsaCI().getConjuntosNodosN().get(pasoRIt)){
                   ConjNodosN cInfNue = new ConjNodosN(gEtapas, nuevasEtapas);
                   for(NodoN n: cInf.getNodos()){
                        NodoN nNue = gEtapas.nodoEtapaOrdinal(it, n.getOrdinal());
                        cInfNue.getNodos().add(nNue);
                        nNue.setConjInfo(cInfNue);
                   }
                   cInfNue.setOrdinal(cInf.getOrdinal());
                   auxACI.add(cInfNue);
               }
               gEtapas.getBolsaCI().getConjuntosNodosN().get(it).addAll(auxACI);

               // Carga la bolsaCDP
               auxACI = new ArrayList<ConjNodosN>();
               for(ConjNodosN cDP: g.getBolsaCDP().getConjuntosNodosN().get(pasoRIt)){
                   ConjNodosN cDPNue = new ConjNodosN(gEtapas, nuevasEtapas);
                   for(NodoN n: cDP.getNodos()){
                        NodoN nNue = gEtapas.nodoEtapaOrdinal(it, n.getOrdinal());
                        cDPNue.getNodos().add(nNue);
                        nNue.setConjDetParque(cDPNue);
                   }
                   // carga el CI asociado.
                   int ordinalCIAsoc = cDP.getOrdinal();
                   ConjNodosN cIAsoc = 
                           gEtapas.getBolsaCI().getConjuntosNodosN().get(pasoRIt).get(ordinalCIAsoc-1);
                   cDPNue.setCIAsociado(cIAsoc);
                   auxACI.add(cDPNue);
              }
              gEtapas.getBolsaCDP().getConjuntosNodosN().get(it).addAll(auxACI);

          }
          // carga de los sucesores de los nodos del grafo gEtapas
          int pasoRIni = 0;
          int pasoRFin = 0;
          for (int it = 0; it < nuevasEtapas; it++) {
               pasoRIni = datosGen.getEtapasPasos().get(it)[2];
               int filMat = g.getNodos().get(pasoRIni).size();
               pasoRFin = datosGen.getEtapasPasos().get(it + 1)[2];
               int colMat = g.getNodos().get(pasoRFin).size();
               MatrizTransicion matriz = new MatrizTransicion(g, pasoRIni,
                       pasoRFin, filMat, colMat);
               matriz.calculaMatrizEtapas(pasoRIni, pasoRFin);
               for (int ifil = 1; ifil <= filMat; ifil++) {
                    for (int icol = 1; icol <= colMat; icol++) {
                         if (matriz.getMatSuc()[ifil - 1][icol - 1]) {
                              gEtapas.getNodos().get(it).get(ifil - 1).getSucesores().add(gEtapas.getNodos().get(it + 1).get(icol - 1));
                              gEtapas.getNodos().get(it).get(ifil - 1).getProbSucesores().add(matriz.getMatProb()[ifil - 1][icol - 1]);
                         }
                    }
               }
          }

          // carga probabilidades de sucesores de conjuntos de información y determinantes de parque


          g.getBolsaCDP().calculaMatTrans();
          for (int it = 0; it < nuevasEtapas; it++) {
              pasoRIni = datosGen.getEtapasPasos().get(it)[2];
              pasoRFin = datosGen.getEtapasPasos().get(it + 1)[2];
              double [][] matR = g.getBolsaCDP().matrizProbCompuesta(pasoRIni,pasoRFin );
              boolean [][] matB = g.getBolsaCDP().matrizBoolCompuesta(pasoRIni, pasoRFin);
              int nfil = OperMatrices.cantFilasMatReal(matR);
              int ncol = OperMatrices.cantColMatReal(matR);
              BolsaDeConjsNodosN bolsaCDP = gEtapas.getBolsaCDP();
              bolsaCDP.getMatricesProb().add(matR);
              bolsaCDP.getMatricesSuc().add(matB);
              for (int ifil = 1; ifil<= nfil; ifil ++){
                  for( int icol = 1; icol <= ncol; icol ++){
                      ConjNodosN cini = bolsaCDP.getConjuntosNodosN().get(it).get(ifil-1);
                      ConjNodosN csuc = bolsaCDP.getConjuntosNodosN().get(it+1).get(icol-1);
                      if(matB[ifil-1][icol-1] == true){
                          cini.getSucesores().add(csuc);
                          cini.getProbSucesores().add(matR[ifil-1][icol-1]);
                      }
                  }
              }
          }



          gEtapas.cargaProb2();
          return gEtapas;
     }

     public static void main(String[] args) throws XcargaDatos {

          DatosGeneralesEstudio datGen1 = new DatosGeneralesEstudio();
          CargaDatosGenerales cargadorDG = new CargaDatosGenerales() {
          };
          String dirDatGen = "D:/Java/PruebaJava2/V4-DatosGenerales.txt";
          try {
               cargadorDG.cargarDatosGen(dirDatGen, datGen1);
          } catch (XcargaDatos ex) {
               System.out.println("------ERROR-------");
               System.out.println(ex.getDescripcion());
               System.out.println("------------------\n");
          }

          GrafoN grafoNat = new GrafoN();
          CargaGrafoN2 cargaNat = new CargaGrafoN2() {
          };

          String dirArchivo = "D:/Java/PruebaJava2/V11-GrafoNaturaleza.txt";
          int cantPasos = datGen1.getCantPasos();
          grafoNat = cargaNat.cargarGrafoN(dirArchivo, cantPasos, true);
          String texto = grafoNat.toString();
          System.out.print(texto);
//		MatrizTransicion matPrueba1 = new MatrizTransicion(grafoNaturaleza, 3);
//		matPrueba1.calculaMatrizEtapa(3);
//		String texto = matPrueba1.toString();
//		System.out.print(texto);
//		int filas3 = grafoNaturaleza.getNodos().get(3).size();
//		int cols5 = grafoNaturaleza.getNodos().get(5).size();
//		MatrizTransicion matPrueba2 = new MatrizTransicion(grafoNaturaleza, 3,5
//									,filas3,cols5);
//		matPrueba2.calculaMatrizEtapas(3, 5);
//		texto = matPrueba2.toString();
//		System.out.print(texto);
          PasaAGrafoEtapas pasaEtapas = new PasaAGrafoEtapas() {
          };
          GrafoN grafoNatEtapas;
          grafoNatEtapas = PasaAGrafoEtapas.pasarAGrafoEtapas(grafoNat, datGen1);
          texto = grafoNatEtapas.toString();
          System.out.print(texto);



     }
}




