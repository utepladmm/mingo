/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package TiposEnum;

/**
 *
 * @author ut469262
 */
public class DependenciaDeT {

    /**
    *  NO el recurso base no varía con la edad ni el paso de tiempo
    *  EDAD el recurso base varía con su propia edad
    *  PASO_DE_TIEMPO el recurso base varía según el paso de tiempo en que vive
    */
    public static final String NO = "NO";
    public static final String EDAD = "EDAD";
    public static final String PASO_DE_TIEMPO = "PASO_DE_TIEMPO";
}
