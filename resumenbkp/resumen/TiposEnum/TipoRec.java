/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package TiposEnum;

import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class TipoRec {
     public static final String GEN = "GEN"; // Generador: cualquier RecursoBase que inyecta energía en el sistema          
     public static final String CARG = "CARG"; // Carga: cualquier RecursoBase que extrae energía del sistema
     
     public static String valueOf(String s) throws XcargaDatos{
         if(s.equalsIgnoreCase(GEN)) return "GEN";
         if(s.equalsIgnoreCase(CARG)) return "CARG";
         throw new XcargaDatos("Error en la lectura de un tipo de RecursoBase");
     }



}
