/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package TiposEnum;

import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 */
public abstract class TipoBloque {

     public static final String REC = "REC";
     public static final String TRANS = "TRANS";
     
     public static String valueOf(String s) throws XcargaDatos{
         if(s.equalsIgnoreCase(TRANS)) return "TRANS";
         if(s.equalsIgnoreCase(REC)) return "REC";
         throw new XcargaDatos("Error en la lectura de un tipo de Bloque");
     }    
}
