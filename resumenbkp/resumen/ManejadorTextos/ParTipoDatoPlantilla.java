/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ManejadorTextos;

import dominio.TipoDeDatos;
import java.io.Serializable;

/**
 *
 * @author ut469262
 */
    public class ParTipoDatoPlantilla implements Serializable{
        private TipoDeDatos tipoD;
        private PlantillaEnTexto plantilla;

        public ParTipoDatoPlantilla(TipoDeDatos tipoD, PlantillaEnTexto plantilla) {
            this.tipoD = tipoD;
            this.plantilla = plantilla;
        }

        public PlantillaEnTexto getPlantilla() {
            return plantilla;
        }

        public void setPlantilla(PlantillaEnTexto plantilla) {
            this.plantilla = plantilla;
        }

        public TipoDeDatos getTipoD() {
            return tipoD;
        }

        public void setTipoD(TipoDeDatos tipoD) {
            this.tipoD = tipoD;
        }


    }