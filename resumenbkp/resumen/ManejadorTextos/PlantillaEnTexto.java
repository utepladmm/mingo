/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ManejadorTextos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 * Es un conjunto de posiciones int[2] en un texto asociadas a un nombre, por ejemplo
 * el nombre de un tipo de datos. Los indicadores de fila y de columna empiezan en cero.
 */
public class PlantillaEnTexto implements Serializable{
    private String nombre;
    /**
     *  Es el nombre de un TipoDeDatos u otro concepto al que se asocia la plantilla,
     *  por ejemplo combustibles, demanda, etc.
     */
    private ArrayList<int[]> posiciones;
    /**
     * Cada elemento de posiciones es una pareja de enteros contenida en un array int[2],
     * que tiene una posición en el texto de comandos. El primer entero es la fila empezando
     * en cero y el segundo la columna empezando en cero.
     * 
     */

    public PlantillaEnTexto(){
        posiciones = new ArrayList<int[]>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<int[]> getPosiciones() {
        return posiciones;
    }

    public void setPosiciones(ArrayList<int[]> posiciones) {
        this.posiciones = posiciones;
    }


    

}
