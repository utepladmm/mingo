/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ManejadorTextos;

import UtilitariosGenerales.DirectoriosYArchivos;
import UtilitariosGenerales.UtilArrayList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import persistencia.LeerDatosArchivo;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 * La clase proporciona métodos para sustituir valores en un texto base.
 * - textoBase es el texto con marcas de posición que son sitios donde se colocará un texto.
 * - los textos a sustituir en textoBase se marcan con la cadena <?>VALOR<?> .
 * - plantillas es el conjunto de PlantillaEnTexto, cada una de las cuales tiene un nombre
 *   y un conjunto de posiciones donde se sustituirá texto; el nombre sirve para determinar
 *   el texto que se insertará.
 */
public class CreadorTex {



    /**
     * Crea un texto donde las posiciones de cada PlantillaEnTexto
     * Las posiciones, filas y columnas en el texto base se empiezan a contar desde 1.
     * están ocupadas por el String asociado al nombre de la plantilla.
     * Debe haberse leído previamente el texto base con el método leeTextoBase
     *
     * @param textoBase es el texto en el que realizar las sustituciones.
     * @param valores es la lista con los valores que se insertarán
     * @param plantillas es la lista de plantillas asociadas a los valores
     * uno a uno por su orden (ATENCION CON EL ORDEN).
     * @return lineas es el texto, con un String por línea de comando y saltos de línea.
     */
    public static String creaTexto(ArrayList<ArrayList<String>> textoBase,
            ArrayList<String> valores, ArrayList<PlantillaEnTexto> plantillas) throws XcargaDatos{
        if (valores.size() != plantillas.size())
            throw new XcargaDatos("La cantidad de valores es distinta que la de plantillas");
        String result = "";
        ArrayList<ArrayList<String>> texto = UtilArrayList.copiaALString2D(textoBase);
        int inp = 0;
        for(PlantillaEnTexto plan: plantillas){
                for(int[] par: plan.getPosiciones()){
                    int indFil = par[0] - 1;
                    int indCol = par[1] - 1;
                    if( texto.get(indFil).get(indCol).equalsIgnoreCase("<?>VALOR<?>") )
                        throw new XcargaDatos("Se está sustituyendo un string que no es <?>VALOR<?>");
                    texto.get(indFil).set(indCol, valores.get(inp));
                }
                inp++;
        }
        int i, j;
        for(i=0; i<texto.size(); i++){
            String auxS = "";
            for(j=0; j<texto.get(i).size(); j++){
                auxS += texto.get(i).get(j);
            }
            result += "\r\n" + auxS;
        }
        return result;
    }
    
    
    
    

    
    

    /**
     * Lee el textoBase de un archivo que se le especifica y devuelve sus
     * líneas separadas en Strings y los Strings de una línea
     * divididos según un separador sep.
     *
     * @param dirArchivo es el nombre del archivo incluso path.
     * @param sep es el string que separa los campos de las líneas.
     * @result resultado es un ArrayList<ArrayList<String>> con el texto base.
     */
    public static ArrayList<ArrayList<String>> leeTextoBase(String dirArchivo, String sep) throws XcargaDatos{
        BufferedReader entrada = null;
        ArrayList<ArrayList<String>> resultado =
                                            new ArrayList<ArrayList<String>>();
        ArrayList<String> lineaResul = null;
        String[] arrayLinea;
        String linea;
        int j;
        try{
            File archivo = new File(dirArchivo);
            entrada = new BufferedReader(new FileReader(archivo));
            while( (linea = entrada.readLine()) != null){
                arrayLinea = linea.split(sep);
                lineaResul = new ArrayList<String>();
                for(j=0; j<arrayLinea.length; j++){
                    lineaResul.add(arrayLinea[j]);
                }
                resultado.add(lineaResul);
            }
            return resultado;
        }catch(FileNotFoundException ex){
            System.out.println("------ERROR-------");
            System.out.println("No se encontró el archivo: " + dirArchivo);
            System.out.println("------------------\n");
            throw new XcargaDatos("Error al leer textoBase de comandos");
        }catch(IOException ex){
            System.out.println("------ERROR-------");
            System.out.println("Al leer el archivo: " + dirArchivo +
                    ", se produjo la excepción: " + ex.toString() );
            System.out.println("------------------\n");
            throw new XcargaDatos("Error al leer textoBase de comandos");
        }
    }

    /**
     * Lee un conjunto de plantillas, cada una corresponde a una línea
     * de la forma:
     * nombre (f1,c1) (f2,c2) ...., donde fi,ci son la fila y la columna asociadas
     * al nombre de plantilla nombre.
     * Filas y columnas se empiezan a numerar en 1.
     * @param dirArchivo es el archivo donde están las plantillas
     * @throws persistencia.XcargaDatos
     */
    public static ArrayList<PlantillaEnTexto> leePlantillas(String dirArchivo) throws XcargaDatos{
        ArrayList<ArrayList<String>> datAr = new ArrayList<ArrayList<String>>();

        datAr = LeerDatosArchivo.getDatos(dirArchivo);
        int i, j;
        ArrayList<PlantillaEnTexto> plantillas = new ArrayList<PlantillaEnTexto>();
        String expReg = "[(),]";
        String[] parString;
        String par;
        int[] parEnteros;
        for(i=0; i<datAr.size(); i++){
            PlantillaEnTexto plant = new PlantillaEnTexto();
            String nombrePlant = datAr.get(i).get(0);
            plant.setNombre(nombrePlant);
            for (j=1; j<datAr.get(i).size(); j++ ){
                par = datAr.get(i).get(j);
                parString = par.split(expReg); 
                parEnteros = new int[2];
                parEnteros[0] = Integer.parseInt(parString[1]);
                parEnteros[1] = Integer.parseInt(parString[2]);
                plant.getPosiciones().add(parEnteros);
            }
            plantillas.add(plant);
        }
        return plantillas;
    }


    public static void main(String[] args) throws XcargaDatos, IOException{

        String dirArchivo = "D:/Java/PruebaJava3";

//        ConjTiposDeDato conjTD = new ConjTiposDeDato();
//        TipoDeDatos combus = new TipoDeDatos();
//        combus.setNombre("combus");
//        TipoDeDatos demanda = new TipoDeDatos();
//        demanda.setNombre("demanda");
//        conjTD.getTipos().add(demanda);
//        conjTD.getTipos().add(combus);
        String sep = "\\<\\?\\>"; // Separador de datos es "<?>"
        ArrayList<ArrayList<String>> textoBase = leeTextoBase(dirArchivo + "/batCorrida.txt", sep);
        ArrayList<PlantillaEnTexto> plantillas = leePlantillas(dirArchivo + "/plantillas.txt");
        ArrayList<String> valores = new ArrayList<String>();
        valores.add("combAlto");
        valores.add("22feb2011");
        ArrayList<String> nombresPlantillas = new ArrayList<String>();
        nombresPlantillas.add("combus");
        nombresPlantillas.add("demanda");

        String texto = creaTexto(textoBase, valores, plantillas);
        DirectoriosYArchivos.grabaTexto(dirArchivo + "/comando.bat" , texto);
    }
}



