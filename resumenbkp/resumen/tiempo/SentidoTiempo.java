package tiempo;

import java.io.Serializable;

/**
 * Para las evoluciones indica si el tiempo va para adelante o para atr�s
 * @author ut469262
 *
 */
public class SentidoTiempo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int sentido; // 1 si avanza y -1 si retrocede

	
	
	
	public SentidoTiempo(int sentido) {
		this.sentido = sentido;
	}

	public int getSentido() {
		return sentido;
	}

	public void setSentido(int sentido) {
		this.sentido = sentido;
	}
		

}

