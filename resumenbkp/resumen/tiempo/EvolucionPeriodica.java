package tiempo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;



public class EvolucionPeriodica<T> extends Evolucion<T> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private EvolucionPorInstantes<T> determinante;
	private int periodo;	
	private int cantPeriodos;
	private EvolucionPorInstantes<T> definicionPeriodo;
	
	public EvolucionPeriodica(GregorianCalendar tiempoInicial, EvolucionPorInstantes<T> definicionPeriodo, int periodo, int cantPeriodos, SentidoTiempo sentido) {
		super(sentido);
		this.setPeriodo(periodo);
		this.setCantPeriodos(cantPeriodos);
		this.definicionPeriodo = definicionPeriodo;
		
		Hashtable<Integer, T> paraDeterminante = (Hashtable<Integer, T>)definicionPeriodo.getValorizador().clone();
		ArrayList<Integer> instantesOrdenados = definicionPeriodo.getInstantesOrdenados();
		
		GregorianCalendar ref = (GregorianCalendar)tiempoInicial.clone();
		int instanteNuevo;
		for (int i = 1; i < cantPeriodos; i++) {
			ref.add(periodo, 1);
			//System.out.println(ref.getTime());
			instanteNuevo = restarFechas(ref, tiempoInicial);
			paraDeterminante.put(instanteNuevo, definicionPeriodo.getValorInicial());
			for (Integer inst: instantesOrdenados) {
				paraDeterminante.put(instanteNuevo+inst, definicionPeriodo.getValorizador().get(inst));
			}
			
		}
		setDeterminante(new EvolucionPorInstantes<T>(paraDeterminante,definicionPeriodo.getValorInicial(), sentido));
	}

	public EvolucionPorInstantes<T> getDefinicionPeriodo() {
		return definicionPeriodo;
	}

	public void setDefinicionPeriodo(EvolucionPorInstantes<T> definicionPeriodo) {
		this.definicionPeriodo = definicionPeriodo;
	}

	@Override
	public T getValor(int instante) {
		
		return determinante.getValor(instante);
	}
	@Override
	public T getValor(){
		return determinante.getValor();
	}

	@Override
	public void inicializarParaSimulacion() {
		// TODO Auto-generated method stub
		
	}

	private int restarFechas(GregorianCalendar fFin, GregorianCalendar fIni){
		return (int) ((fFin.getTimeInMillis()-fIni.getTimeInMillis())/1000);
	}

	public EvolucionPorInstantes<T> getDeterminante() {
		return determinante;
	}

	public void setDeterminante(EvolucionPorInstantes<T> determinante) {
		this.determinante = determinante;
	}
	public static void main(String[] args) {

		Hashtable<Integer, Double> vals = new Hashtable<Integer, Double>();
		vals.put(10, 23.2);
		vals.put(10000000, 2.2);
		vals.put(20000000, 11.2);

		
		SentidoTiempo st = new SentidoTiempo(1);
		Evolucion<Double> ev = new EvolucionPorInstantes<Double>(vals, 100.0, st);		
		EvolucionPeriodica<Double> ev2 = new EvolucionPeriodica<Double>(new GregorianCalendar(2000, Calendar.JANUARY, 1), (EvolucionPorInstantes<Double>) ev, Calendar.YEAR, 10, st);
		
		System.out.println("Instante=1 : " + ev2.getValor(1));
		for (Integer inst: ev2.getDeterminante().getInstantesOrdenados()) {
			System.out.println("Instante=" + inst + " : " + ev2.getValor(inst));
		}
		System.out.println("----------------------------------------------------------" );
		System.out.println("----------------------------------------------------------" );
		System.out.println();
		System.out.println(Calendar.DAY_OF_YEAR);
		

	}

	public int getPeriodo() {
		return periodo;
	}

	public void setPeriodo(int periodo) {
		this.periodo = periodo;
	}

	public int getCantPeriodos() {
		return cantPeriodos;
	}

	public void setCantPeriodos(int cantPeriodos) {
		this.cantPeriodos = cantPeriodos;
	}

}
