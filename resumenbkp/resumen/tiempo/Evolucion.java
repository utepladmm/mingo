package tiempo;

import java.io.Serializable;

/**
 * Clase evolución abstracta
 * @author ut602614
 *
 */

public abstract class Evolucion<T> implements Serializable{
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public SentidoTiempo getSentido() {
		return sentido;
	}

	public void setSentido(SentidoTiempo sentido) {
		this.sentido = sentido;
	}

	protected T valor; //es el valor corriente
	protected SentidoTiempo sentido; 
		
	
	public Evolucion(SentidoTiempo sentido) {
		super();
		this.sentido = sentido;
	}

	public abstract T getValor(int instante);
	
	public abstract void inicializarParaSimulacion();

	public T getValor() {
		return valor;
	}

	public void setValor(T valor) {
		this.valor = valor;
	}

	

	
}
