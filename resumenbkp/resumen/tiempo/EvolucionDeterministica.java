package tiempo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

import utilitarios.Constantes;
import datatypes.Pair;

/**
 * Evoluci�n determin�stica OJO!!! PROBABLEMENTE SE TIRE ESTA CLASE Y PARTE DE SU L�GICA SE HAGA EN LA PRESENTACI�N, REPRESENTANDO 
 * EN UNA EVOLUCI�N POR INSTANTES LO NECESARIO
 * @author ut602614
 * 
 *
 */

public class EvolucionDeterministica extends Evolucion<Double>{
	private ArrayList<Pair<Integer, Double>> determinante; //contiene los valores Fijos
	private ArrayList<Pair<Integer, Double>> periodicaAnual; //contiene los cambios periodicos anualmente
	private Hashtable<Integer, Double> valorizador;	// es donde se almacena el instante en que cambia y el nuevo valor
	private ArrayList<Integer> instantesOrdenados; // son los instantes ordenados para recorrer y extraer luego del valorizador
	
	public EvolucionDeterministica(
			ArrayList<Pair<Integer, Double>> determinante,
			ArrayList<Pair<Integer, Double>> periodicaAnual, SentidoTiempo st) {
		super(st);
		this.determinante = determinante;
		this.periodicaAnual = periodicaAnual;
		this.valorizador = new Hashtable<Integer,Double>();
		this.instantesOrdenados = new ArrayList<Integer>();
		generarValorizadoreInstantes();		
	}

	
	private void generarValorizadoreInstantes() {
		for(Pair<Integer, Double> p: determinante) {
			this.valorizador.put(p.first, p.second);
			this.instantesOrdenados.add(p.first);
		}
		
		for(Pair<Integer, Double> p: periodicaAnual) {
			Double valor = this.valorizador.get(p.first);
			if (this.valorizador.containsKey(p.first)) {
				valor+=p.second;				
			} else {
				valor=p.second;
			}
			this.valorizador.put(p.first, valor);
			this.instantesOrdenados.add(p.first);
		}
		
		Collections.sort(this.instantesOrdenados);
	}

	
	@Override
	public void inicializarParaSimulacion() {
		// DELIBERADAMENTE VAC�O
		
	}
	
	//debe estar definido el instante inicial como instante 0
	public Double dameValor(Integer instante){
		int i = 0;
		while (this.instantesOrdenados.get(i) > instante) {
			i++;
		}
		return this.valorizador.get(i-1);
	}
	
//	public Double dameValor(PasoTiempo paso){
//		Double valorInicial = dameValor(paso.getInstanteInicial());
//		Double valorFinal= dameValor(paso.getInstanteInicial());
//		if (Math.abs(valorInicial - valorFinal) < Constantes.EPSILONCOEF) {
//			return valorFinal;
//		}
//		//TODO: AC� VA LA INTERPOLACI�N
//		return null;
//	}
	
	public ArrayList<Pair<Integer, Double>> getDeterminante() {
		return determinante;
	}

	public void setDeterminante(ArrayList<Pair<Integer, Double>> determinante) {
		this.determinante = determinante;
	}

	public ArrayList<Pair<Integer, Double>> getPeriodicaAnual() {
		return periodicaAnual;
	}

	public void setPeriodicaAnual(ArrayList<Pair<Integer, Double>> periodicaAnual) {
		this.periodicaAnual = periodicaAnual;
	}


	public ArrayList<Integer> getInstantesOrdenados() {
		return instantesOrdenados;
	}


	public void setInstantesOrdenados(ArrayList<Integer> instantesOrdenados) {
		this.instantesOrdenados = instantesOrdenados;
	}


	@Override
	public Double getValor(int instante) {
		// TODO Auto-generated method stub
		return null;
	}




}
