package tiempo;

/**
 * Clase que representa un paso de tiempo en la l�nea de tiempo
 * @author ut602614
 *
 */
public class PasoTiempo {

	private int instanteInicial;
	private int instanteFinal;
	private int duracionPaso;
	private int periodoPaso; /**Este per�odo puede ser SEGUNDO, DIA, HORA, SEMANA, toma valores de las constantes DE CALENDAR*/
	private LineaTiempo linea;
	private BloqueTiempo bloque;
	
	
	public void setDuracionPaso(int duracionPaso) {
		this.duracionPaso = duracionPaso;
	}

	public BloqueTiempo getBloque() {
		return bloque;
	}

	public void setBloque(BloqueTiempo bloque) {
		this.bloque = bloque;
	}

	public PasoTiempo(int instanteInicial, int longitudPaso, int perPaso, LineaTiempo linea, BloqueTiempo bloque) {
		this.instanteInicial = instanteInicial;
		this.setInstanteFinal(instanteInicial + longitudPaso);
		this.duracionPaso = longitudPaso;
		this.setPeriodoPaso(perPaso);
		this.linea = linea;
		this.bloque = bloque;
	}

	public int getInstanteInicial() {
		return instanteInicial;
	}

	public void setInstanteInicial(int instanteInicial) {
		this.instanteInicial = instanteInicial;
	}

	public int getDuracionPaso() {
		return duracionPaso;
	}

	public void setDuraciondPaso(int longitudPaso) {
		this.duracionPaso = longitudPaso;
	}

	public int getInstanteFinal() {
		return instanteFinal;
	}

	public void setInstanteFinal(int instanteFinal) {
		this.instanteFinal = instanteFinal;
	}

	public LineaTiempo getLinea() {
		return linea;
	}

	public void setLinea(LineaTiempo linea) {
		this.linea = linea;
	}
	
	public int getIntervaloMuestreo() {
		return this.bloque.getIntervaloMuestreo();
	}

	public int getPeriodoPaso() {
		return periodoPaso;
	}

	public void setPeriodoPaso(int periodoPaso) {
		this.periodoPaso = periodoPaso;
	}
}
