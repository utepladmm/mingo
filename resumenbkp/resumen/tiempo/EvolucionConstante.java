package tiempo;

import java.io.Serializable;

/**
 * Clase que representa una evoluci�n constante en el tiempo (siempre el mismo valor)
 * @author ut602614
 *
 * @param <T> Tipo de dato del valor de la evoluci�n
 */

public class EvolucionConstante<T> extends Evolucion<T> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public EvolucionConstante(T val, SentidoTiempo st) {
		super(st);
		this.setValor(val);

	}
	
	
	@Override
	public T getValor(int instante) {
		// TODO Auto-generated method stub
		return valor;
	}
	
	
	@Override
	public void inicializarParaSimulacion() {
		// DELIBERADAMENTE VAC�O
		
	}



}

