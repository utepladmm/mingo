package tiempo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import datatypes.DatosPostizacion;
import datatypesSalida.DatosPaso;
import datatypesTiempo.DatosLineaTiempo;

/**
 * Clase que representa la l�nea de tiempo asociada a la simulaci�n o a la optimizaci�n
 * @author ut602614
 *
 */

public class LineaTiempo {
	private SentidoTiempo sentidoTiempo;
	private ArrayList<PasoTiempo> linea;
	private ArrayList<BloqueTiempo> bloques;
	
	private GregorianCalendar inicioTiempo;
	private GregorianCalendar finTiempo;
	
	/*
	 * Toma el valor entero representado por las constantes Calendar.MONTH, Calendar.YEAR, etc.
	 * Representa el per�odo para el cu�l se desea tener una vista
	 */
	private int periodoDeIntegracion; 
	private boolean usarPeriodoIntegracion;
	
	private int inicial; /*tiempo inicial de la linea de tiempo expresado en segundos respecto al inicio de tiempo*/
	private int numPaso;
	private int bloqueActual;
	private int cantidadPasos;
	private int maxDurPaso;
	
	/**
	 * Instantes de inicio de cada uno de los a�os en los que la 
	 * corrida se extiende. Ej. si la corrida empieza el marzo de 2020 y
	 * termina en el instante final de 2022, la corrida se extiende por 3 a�os
	 */
	
	private int [] instInicioAnio;
	
	
	/**
	 * Instantes de inicio de cada uno de los a�os en los que la corrida se extienda
	 * Clave a�o ( ejemplo 2020)
	 * Valor instante de inicio del a�o 2020 
	 */
	private Hashtable<Integer, Integer> instInicioAnioHT;
	
	/**
	 * Duraci�n de cada a�o en segundos
	 */
	private Hashtable<Integer, Integer> durAnio;
	
	private int anioInic;   // primer a�o de la corrida
	private int anioFin;   // �ltimo a�o de la corrida
	
	/**
	 * Constructor de la LineaTiempo cuando la postizaci�n es interna
	 * @param datos
	 * @param tiempoInicialCorrida
	 */
	public LineaTiempo(DatosLineaTiempo datos, GregorianCalendar tiempoInicialCorrida, GregorianCalendar tiempoFinalCorrida) {
		super();
		this.sentidoTiempo = datos.getSentido();
		this.linea = new ArrayList<PasoTiempo>();
		this.bloques = new ArrayList<BloqueTiempo>();
		this.cantidadPasos = 0;
		this.inicial =datos.getInstanteInicial();
		
		DateFormat df = new SimpleDateFormat("dd MM yyyy"); // Se parsea un string con este formato a GregorianCalendar
		Date date = null;
		try {
			date = df.parse(datos.getTiempoInicial());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.inicioTiempo = new GregorianCalendar();
		this.inicioTiempo.setTime(date);
		
		int instanteInicialCorrida = dameInstante(tiempoInicialCorrida);
		ArrayList<Integer> pasosPorBloque = datos.getPasosPorBloque();
		ArrayList<Integer> periodoPorBloque= datos.getPeriodoPasoPorBloque();
		int durpaso;
		this.numPaso = 0;
		this.setBloqueActual(0);
		this.periodoDeIntegracion = datos.getPeriodoIntegracion();
		this.usarPeriodoIntegracion = datos.isUsarPeriodoIntegracion();
		
		maxDurPaso = 0;
		if (!usarPeriodoIntegracion) {
			for (int cbloques = 0; cbloques < datos.getCantBloques(); cbloques++) {
				durpaso = datos.getDuracionPasoPorBloque().get(cbloques);
				if (durpaso > maxDurPaso) maxDurPaso = durpaso;
				if(instanteInicialCorrida > dameInstante(tiempoFinalCorrida)) break;  // Emiliano			
				this.bloques.add(new BloqueTiempo(cbloques, this.linea.size(), pasosPorBloque.get(cbloques),durpaso, datos.getIntMuestreoPorBloque().get(cbloques), datos.getDurPostesPorBloque().get(cbloques),datos.getCronologicos().get(cbloques)));		
				for (int j = 0; j < pasosPorBloque.get(cbloques); j++) {				
					this.linea.add(new PasoTiempo(instanteInicialCorrida, durpaso, periodoPorBloque.get(cbloques),this,this.bloques.get(cbloques)));
					this.cantidadPasos++;
					instanteInicialCorrida += durpaso;
					if(instanteInicialCorrida > dameInstante(tiempoFinalCorrida)) break; // Emiliano
				}
					
			}
		} else {
			GregorianCalendar finIntegracion= (GregorianCalendar)inicioTiempo.clone();
			finIntegracion.add(periodoDeIntegracion, 1);
			GregorianCalendar referencia = (GregorianCalendar)inicioTiempo.clone();
			
			for (int cbloques = 0; cbloques < datos.getCantBloques(); cbloques++) { 
				durpaso = datos.getDuracionPasoPorBloque().get(cbloques);	
				if(dameInstante(referencia) > dameInstante(tiempoFinalCorrida)) break; //Emiliano
				referencia.add(Calendar.SECOND, durpaso);
				if (referencia.compareTo(finIntegracion) > 0) {
					referencia.add(Calendar.SECOND, -durpaso);
					this.bloques.add(new BloqueTiempo(cbloques, this.linea.size(), pasosPorBloque.get(cbloques)-1,durpaso, datos.getIntMuestreoPorBloque().get(cbloques), datos.getDurPostesPorBloque().get(cbloques),datos.getCronologicos().get(cbloques)));	
					this.bloques.add(new BloqueTiempo(cbloques, this.linea.size() + pasosPorBloque.get(cbloques), 1,durpaso + restarFechas(finIntegracion, referencia), datos.getIntMuestreoPorBloque().get(cbloques), datos.getDurPostesPorBloque().get(cbloques),datos.getCronologicos().get(cbloques)));
					
				}
				
				for (int j = 0; j < pasosPorBloque.get(cbloques)-1; j++) {					
					this.linea.add(new PasoTiempo(instanteInicialCorrida, durpaso, periodoPorBloque.get(cbloques),this,this.bloques.get(bloqueActual)));
					this.cantidadPasos++;
					instanteInicialCorrida += durpaso;
				}					
				
				referencia.add(Calendar.SECOND, durpaso);
				if (referencia.compareTo(finIntegracion) > 0) {					
					referencia.add(Calendar.SECOND, -durpaso);				  
					durpaso = durpaso + restarFechas(finIntegracion, referencia);
					this.linea.add(new PasoTiempo(instanteInicialCorrida, durpaso, periodoPorBloque.get(cbloques),this,this.bloques.get(bloqueActual)));
					this.cantidadPasos++;
					instanteInicialCorrida += durpaso;
				}
				referencia = dameTiempo(instanteInicialCorrida);				
			}			
		}
		
		this.finTiempo = dameTiempo(this.linea.get(linea.size()-1).getInstanteFinal());	
		int cantAnios = this.finTiempo.get(Calendar.YEAR) - this.inicioTiempo.get(Calendar.YEAR) + 1;
		instInicioAnio = new int[cantAnios+1];
		instInicioAnioHT = new Hashtable<Integer, Integer>();
		durAnio =  new Hashtable<Integer, Integer>();
		GregorianCalendar ref = new GregorianCalendar(this.inicioTiempo.get(Calendar.YEAR), Calendar.JANUARY, 1);
		int j = 0;
		do  {
			instInicioAnio[j] = dameInstante(ref);
			instInicioAnioHT.put(ref.get(Calendar.YEAR), dameInstante(ref));
			ref.add(Calendar.YEAR, 1);
			j++;
		} while (ref.before(finTiempo));	

		instInicioAnio[j] = dameInstante(ref);
		this.anioInic = this.inicioTiempo.get(Calendar.YEAR);
		this.anioFin = this.finTiempo.get(Calendar.YEAR);
		instInicioAnioHT.put(ref.get(Calendar.YEAR), dameInstante(ref));
		for(int ian=anioInic; ian<=anioFin; ian++){
			if(instInicioAnioHT.get(ian+1)!=null){
				durAnio.put(ian, instInicioAnioHT.get(ian+1)-instInicioAnioHT.get(ian));
			}
		}
	}

	
	
	private DatosPaso determinarDurPosCantPosInterPos(DatosPostizacion post, int paso, int intervalo) {
		DatosPaso retorno = new DatosPaso();
		ArrayList<Integer> numpos = post.getColnumpos().get(paso);
		
		int cantPos = 0;
		
		for (Integer i: numpos) {
			if (i > cantPos) {
				cantPos=i;
			}			
		}	
		int [] interPorPoste = new int [cantPos];
		for (Integer i: numpos) {
			interPorPoste[i-1]+=1;	
		}
		
		int [] durPos = new int [cantPos];
		for (int i = 0; i < cantPos; i++) {
			durPos[i] = interPorPoste[i]*intervalo;
		}
		retorno.setCantPostes(cantPos);
		retorno.setCantIntervMuestreo(numpos.size());
		retorno.setDurPostes(durPos);
		retorno.setNumPaso(paso);
		retorno.setDurpaso(numpos.size()*intervalo);
		return retorno;
	}
	
	
	/**
	 * Constructor de la LineaTiempo cuando la postizaci�n es externa
	 *
	 */
	public LineaTiempo(DatosLineaTiempo datos, DatosPostizacion postizacion, GregorianCalendar tiempoInicialCorrida, GregorianCalendar tiempoFinalCorrida) {
		super();
		this.sentidoTiempo = datos.getSentido();
		this.linea = new ArrayList<PasoTiempo>();
		this.bloques = new ArrayList<BloqueTiempo>();
		this.cantidadPasos = 0;
		this.inicial =datos.getInstanteInicial();
		
		DateFormat df = new SimpleDateFormat("dd MM yyyy"); // Se parsea un string con este formato a GregorianCalendar
		Date date = null;
		try {
			date = df.parse(datos.getTiempoInicial());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.inicioTiempo = new GregorianCalendar();
		this.inicioTiempo.setTime(date);
		
		int instanteInicialCorrida = dameInstante(tiempoInicialCorrida);
		int instanteFinalCorrida = dameInstante(tiempoFinalCorrida);
	
//		ArrayList<Integer> pasosPorBloque = datos.getPasosPorBloque();
//		ArrayList<Integer> periodoPorBloque= datos.getPeriodoPasoPorBloque();
//		int durpaso;
		this.numPaso = 0;
		this.setBloqueActual(0);
		this.periodoDeIntegracion = datos.getPeriodoIntegracion();
		this.usarPeriodoIntegracion = datos.isUsarPeriodoIntegracion();
	
		boolean masPasos = true;
		
		maxDurPaso = 0;
		if (!usarPeriodoIntegracion) {
			/**
			 * ac� hay que construir bloques lineas pasos maxdurpaso a partir de la postizacion
			 */
			int instanteInicialPaso = instanteInicialCorrida;
			ArrayList<ArrayList<Integer>> colnumpos = postizacion.getColnumpos();
			this.cantidadPasos = colnumpos.size();
			int intervalo = datos.getIntMuestreoPorBloque().get(0);
			boolean nuevoBloque = true;
			BloqueTiempo bloque = null;
			int cbloques = 0;
			int i = 0;
			while (i<cantidadPasos && masPasos) {
				DatosPaso dp = determinarDurPosCantPosInterPos(postizacion, i, intervalo);
				if (i!=0) {
					nuevoBloque = !(dp.getDurpaso()==bloques.get(bloques.size()-1).getDuracionPaso() && dp.getCantPostes() == bloques.get(bloques.size()-1).getCantPostes());
				}
				if (nuevoBloque) {
					bloque = new BloqueTiempo(cbloques, i, 1, dp.getDurpaso(), intervalo, dp.getDurPostes(), datos.getCronologicos().get(0));
					cbloques++;
					bloques.add(bloque);
				} else {
					bloque = bloques.get(bloques.size()-1);
					bloque.setCantidadPasos(bloque.getCantidadPasos()+1);
				}
				PasoTiempo p = new PasoTiempo(instanteInicialPaso,dp.getDurpaso(),0,this, bloque);
				this.linea.add(p);
				instanteInicialPaso += dp.getDurpaso();
				i++;
				masPasos = instanteInicialPaso<instanteFinalCorrida;
				if (!masPasos) this.cantidadPasos = i;
			}

		} 
		this.finTiempo = dameTiempo(this.linea.get(linea.size()-1).getInstanteFinal());
		GregorianCalendar tiempoAnterior = dameTiempo(this.linea.get(linea.size()-2).getInstanteFinal());
		int ultanio = (tiempoAnterior.get(Calendar.YEAR)!=this.finTiempo.get(Calendar.YEAR))?this.finTiempo.get(Calendar.YEAR):tiempoAnterior.get(Calendar.YEAR);
		int cantAnios = ultanio- this.inicioTiempo.get(Calendar.YEAR) + 1;
		instInicioAnio = new int[cantAnios+1];
		instInicioAnioHT = new Hashtable<Integer, Integer>();
		GregorianCalendar ref = new GregorianCalendar(this.inicioTiempo.get(Calendar.YEAR), Calendar.JANUARY, 1);
		int j = 0;
		for (j = 0; j < cantAnios+1; j++)  {
			instInicioAnio[j] = dameInstante(ref);	
			instInicioAnioHT.put(ref.get(Calendar.YEAR), dameInstante(ref));
			ref.add(Calendar.YEAR, 1);				 
		} 	
		this.anioInic = this.inicioTiempo.get(Calendar.YEAR);
		this.anioFin = this.finTiempo.get(Calendar.YEAR);
		instInicioAnioHT.put(ref.get(Calendar.YEAR), dameInstante(ref));
		durAnio =  new Hashtable<Integer, Integer>();		
		for(int ian=anioInic; ian<=anioFin; ian++){
			if(instInicioAnioHT.get(ian+1)!=null){
				durAnio.put(ian, instInicioAnioHT.get(ian+1)-instInicioAnioHT.get(ian));
			}
		}
	}
	
	
	
	/*
	 * Toma el valor entero representado por las constantes Calendar.MONTH, Calendar.DAY, etc.
	 * y devuelve su duraci�n en segundos
	 */
//	private int duracionPeriodoSegundos(int periodoInt) {
//		if (periodoInt == Calendar.YEAR) return
//		return 0;
//	}


	
	public ArrayList<PasoTiempo> getLinea() {
		return linea;
	}

	public void setLinea(ArrayList<PasoTiempo> linea) {
		this.linea = linea;
	}


	public Integer getInicial() {
		return inicial;
	}

	public void setInicial(Integer inicial) {
		this.inicial = inicial;
	}

	public PasoTiempo pasoActual() {
		return linea.get(numPaso);		
	}
	
	public GregorianCalendar getInicioTiempo() {
		return inicioTiempo;
	}

	public void setInicioTiempo(GregorianCalendar inicioTiempo) {
		this.inicioTiempo = inicioTiempo;
	}

	public int getNumPaso() {
		return numPaso;
	}

	public void setNumPaso(int numPaso) {
		this.numPaso = numPaso;
	}

	
	public PasoTiempo devuelvePasoActual() {
		if (numPaso<linea.size())	return linea.get(numPaso);
		return null;
	}
	
	public void retrocederPaso() {
		numPaso--;
		bloqueActual = linea.get(numPaso).getBloque().getOrdinalBloque();
		
	}
	
	public void avanzarPaso() {
		numPaso++;	
		if(numPaso<linea.size()) bloqueActual = linea.get(numPaso).getBloque().getOrdinalBloque();		
	}
	
	/**
	 * Devuelve el tiempo transcurrido entre dos fechas en segundos
	 * @param fFin
	 * @param fIni
	 * @return
	 */
	public int restarFechas(GregorianCalendar fFin, GregorianCalendar fIni){
		return (int) ((fFin.getTimeInMillis()-fIni.getTimeInMillis())/1000);
	}

	/**
	 * Devuelve el paso al que pertenece el instante (abierto al final del paso)
	 */
	
	public PasoTiempo damePaso(GregorianCalendar instanteTiempo) {
		int segundosDesdeInicio = restarFechas(instanteTiempo, this.getTiempoInicial());
		int primerPasoPosible = segundosDesdeInicio/maxDurPaso;
		int  i = primerPasoPosible;
		while (i < this.linea.size()) {
			if (segundosDesdeInicio < this.linea.get(i).getInstanteFinal()) {
				return this.linea.get(i);				
			}
			i++;
		}
		return null; //Est� fuera de la l�nea de tiempo
		
	}
	
	/**
	 * Devuelve el paso de ordinal ordinalPaso de la l�nea de tiempo
	 * @param ordinalPaso
	 * @return
	 */
	public PasoTiempo damePasoDeOrdinal(int ordinalPaso){
		return linea.get(ordinalPaso);
	}
	
	/**
	 * Ajusta la linea de tiempo a per�odo de integraci�n "estirando" el �ltimo paso
	 */
	public void ajustarAPeriodoIntegracion() {
		
	}
	
	/**
	 * Devuelve un GregorianCalendar a partir de una fecha en String
	 * con el formato "dd MM yyyy HH:mm:ss"
	 * @param fecha
	 * @return
	 */
	public static GregorianCalendar devuelveGregorianDeFechaString(String fecha){
		DateFormat df = new SimpleDateFormat("dd MM yyyy HH:mm:ss"); // Se parsea un string con este formato a GregorianCalendar
		Date date = null;
		try {
			date = df.parse(fecha);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		GregorianCalendar result = new GregorianCalendar();
		result.setTime(date);
		return result;
	}
	
	public ArrayList<BloqueTiempo> getBloques() {
		return bloques;
	}


	public void setBloques(ArrayList<BloqueTiempo> bloques) {
		this.bloques = bloques;
	}


	public int getBloqueActual() {
		return bloqueActual;
	}


	public void setBloqueActual(int bloqueActual) {
		this.bloqueActual = bloqueActual;
	}


	public GregorianCalendar getTiempoInicial() {
		return inicioTiempo;
	}


	public void setTiempoInicial(GregorianCalendar tiempoInicial) {
		this.inicioTiempo = tiempoInicial;
	}

	public int getPeriodoDeIntegracion() {
		return periodoDeIntegracion;
	}

	public void setPeriodoDeIntegracion(int periodoDeIntegracion) {
		this.periodoDeIntegracion = periodoDeIntegracion;
	}


	public int getCantidadPasos() {
		return cantidadPasos;
	}

	public void setCantidadPasos(int cantidadPasos) {
		this.cantidadPasos = cantidadPasos;
	}

	public int getMaxDurPaso() {
		return maxDurPaso;
	}

	public void setMaxDurPaso(int maxDurPaso) {
		this.maxDurPaso = maxDurPaso;
	}

	public void setInicial(int inicial) {
		this.inicial = inicial;
	}

	public GregorianCalendar dameTiempo(int instante) {
		int segundosTranscurridos = instante - this.inicial;
		GregorianCalendar resultado = (GregorianCalendar) this.inicioTiempo.clone();
		resultado.setTime(this.inicioTiempo.getTime());
		resultado.add(Calendar.SECOND, segundosTranscurridos);
		return resultado;
		
	}
	
	public String fechaYHoraDeInstante(int instante){
		StringBuilder sb = new StringBuilder();
		GregorianCalendar gc = dameTiempo(instante);
		sb.append(gc.get(Calendar.YEAR)+ "/");
		sb.append((gc.get(Calendar.MONTH)+1)+ "/");
		sb.append(gc.get(Calendar.DAY_OF_MONTH) + "/");
		sb.append(gc.get(Calendar.HOUR) + ":");
		sb.append(gc.get(Calendar.MINUTE)+ ":");
		sb.append(gc.get(Calendar.SECOND));
		return sb.toString();
	}
	
	public String fechaDeInstante(int instante){
		StringBuilder sb = new StringBuilder();
		GregorianCalendar gc = dameTiempo(instante);
		sb.append(gc.get(Calendar.YEAR)+ "/");
		sb.append((gc.get(Calendar.MONTH)+1)+ "/");
		sb.append(gc.get(Calendar.DAY_OF_MONTH));
		return sb.toString();
	}
	
	public PasoTiempo damePaso(int instante) {
		return damePaso(dameTiempo(instante));
	}
	
	
	
	public int dameInstante(GregorianCalendar tiempo) {		
		return restarFechas(tiempo, this.inicioTiempo);
		
	}

	public boolean isUsarPeriodoIntegracion() {
		return usarPeriodoIntegracion;
	}

	public void setUsarPeriodoIntegracion(boolean usarPeriodoIntegracion) {
		this.usarPeriodoIntegracion = usarPeriodoIntegracion;
	}

	
	/**
	 * Vuelve al inicio la l�nea de tiempo 
	 */
	public void reiniciar() {
		this.numPaso = 0;
		this.bloqueActual = 0;		
	}

	public Integer getCantidadPostes() {
		return bloques.get(bloqueActual).getCantPostes();
	}

	public Integer getDuracionPaso() {
		return pasoActual().getDuracionPaso();
	}

	public int[] getDuracionPostes() {		
		return bloques.get(bloqueActual).getDuracionPostes();
	}

	public int[] getInstInicioAnio() {
		
		return instInicioAnio;
	}
	
	
	

	public Hashtable<Integer, Integer> getInstInicioAnioHT() {
		return instInicioAnioHT;
	}



	public void setInstInicioAnioHT(Hashtable<Integer, Integer> instInicioAnioHT) {
		this.instInicioAnioHT = instInicioAnioHT;
	}


	

	public Hashtable<Integer, Integer> getDurAnio() {
		return durAnio;
	}



	public void setDurAnio(Hashtable<Integer, Integer> durAnio) {
		this.durAnio = durAnio;
	}



	public void setInstInicioAnio(int [] instInicioAnio) {
		this.instInicioAnio = instInicioAnio;
	}

	public int getAnioInic() {
		return anioInic;
	}

	public void setAnioInic(int anioInic) {
		this.anioInic = anioInic;
	}
	
	

	public int getAnioFin() {
		return anioFin;
	}



	public void setAnioFin(int anioFin) {
		this.anioFin = anioFin;
	}



	public boolean primerPasoBloque() {		
		return bloques.get(bloqueActual).getPrimerPaso()==numPaso;
	}

	public boolean esPasoFinal() {
		return numPaso==cantidadPasos-1;
	}

	public void llevarAlFinal() {
		this.numPaso = this.cantidadPasos-1;
		this.bloqueActual = this.bloques.size()-1;	
		
	}
	
	
	public SentidoTiempo getSentidoTiempo() {
		return sentidoTiempo;
	}

	public void setSentidoTiempo(SentidoTiempo sentidoTiempo) {
		this.sentidoTiempo = sentidoTiempo;
	}

	public GregorianCalendar getFinTiempo() {
		return finTiempo;
	}

	public void setFinTiempo(GregorianCalendar finTiempo) {
		this.finTiempo = finTiempo;
	}

	public int getInstInicPasoCorriente() {
		return this.getLinea().get(this.getNumPaso()).getInstanteInicial();
	}

	
	public String toString(){		
		StringBuilder sb = new StringBuilder();
		sb.append("LINEA DE TIEMPO\n");
		sb.append("Instante inicio de corrida" + inicial + "\n\n");
		sb.append("Instantes iniciales de pasos\n");
		int numpaso = 0;
		for(PasoTiempo p: linea){
			sb.append("Paso " + numpaso + " - Inst.Inicio " + p.getInstanteInicial() + " - Inst.Final " + p.getInstanteFinal() + "\n" );
			numpaso++;
		}
		sb.append("\nInstantes de inicio de anios\n");
		for(int ia=0; ia<instInicioAnio.length; ia++){
			sb.append(instInicioAnio[ia] + "\n");	
		}
		return sb.toString();
	}

	public void setSentidoTiempo(int i) {
		this.getSentidoTiempo().setSentido(i);
		
	}


}
