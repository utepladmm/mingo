package tiempo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

/**
 * Subclase de una evoluci�n que utiliza en su representaci�n una lista valor,
 * instante Debe estar definido el valor para el instante 0 El valorizador esta
 * defenido de forma que en los instantes de cambio vale el nuevo valor
 * (continua por derecha)
 * 
 * @author ut602614
 *
 * @param <T>
 */
public class EvolucionPorInstantes<T> extends Evolucion<T> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int sentidoLlamadaAnterior; // 1 si est� avanzando y -1 si est�
										// retrocediendo en el
	// tiempo

	private Integer siguienteInstanteInicial;
	private int siguienteIndiceInicial;
	private int instanteInvocacionAnterior;

	private Integer siguienteInstante;
	private int siguienteIndice;
	private T valorInicial;

	private Hashtable<Integer, T> valorizador; // es donde se almacena el
												// instante en que cambia y el
												// nuevo valor DEBE ESTAR
												// DEFINIDO EL INSTANTE
												// INICIAL!!!
	private ArrayList<Integer> instantesOrdenados; // son los instantes
													// ordenados para recorrer y
													// extraer luego del
													// valorizador

	private boolean nomascambios;

	public EvolucionPorInstantes(Hashtable<Integer, T> vals, T valorInicial, SentidoTiempo st) {
		super(st);
		this.valorizador = vals;
		this.instantesOrdenados = new ArrayList<Integer>();
		generarValorizadoresInstantes();
		sentidoLlamadaAnterior = st.getSentido();
		setNomascambios(false);

		reinicializarSegunSentido();

		this.siguienteInstante = instantesOrdenados.get(siguienteIndice);
		this.valorInicial = valorInicial;
		this.valor = vals.get(siguienteInstante);
		this.siguienteInstanteInicial = this.siguienteInstante;
		this.siguienteIndiceInicial = this.siguienteIndice;

	}

	private void reinicializarSegunSentido() {
		if (sentido.getSentido() == 1) {
			siguienteIndice = 0;
			instanteInvocacionAnterior = -1;
			siguienteInstante = instantesOrdenados.get(siguienteIndice);
		} else {
			siguienteIndice = valorizador.size() - 1;
			instanteInvocacionAnterior = Integer.MAX_VALUE-1;
			siguienteInstante = instantesOrdenados.get(siguienteIndice);
		}

	}

	public void inicializarParaSimulacion() {
		this.siguienteInstante = this.siguienteInstanteInicial;
		this.siguienteIndice = this.siguienteIndiceInicial;
	}

	private void generarValorizadoresInstantes() {
		Set<Integer> claves = valorizador.keySet();
		Iterator<Integer> it = claves.iterator();
		while (it.hasNext()) {
			this.instantesOrdenados.add(it.next());
		}

		Collections.sort(this.instantesOrdenados);

	}

	// public T dameValor(int instante){
	// int i = 0;
	//
	// for (i = 0; i<instantesOrdenados.size();i++) {
	// if (instantesOrdenados.get(i)>instante) break;
	// }
	// return this.valorizador.get(instantesOrdenados.get(i-1));
	// }

	public ArrayList<Integer> getInstantesOrdenados() {
		return instantesOrdenados;
	}

	public void setInstantesOrdenados(ArrayList<Integer> instantesOrdenados) {
		this.instantesOrdenados = instantesOrdenados;
	}

	@Override
	public T getValor(int instante) {
		if (instante == instanteInvocacionAnterior)
			return valor;
	
		if (sentido.getSentido() != sentidoLlamadaAnterior
				|| sentido.getSentido() * (instante - instanteInvocacionAnterior) < 0) {
			reinicializarSegunSentido();
		} else {
			instanteInvocacionAnterior = instante;
		}
		
		sentidoLlamadaAnterior = this.sentido.getSentido();
		

		if (instante < instantesOrdenados.get(0)) {
			valor = valorInicial;
			return valor;
		}
		if (instante >= instantesOrdenados.get(instantesOrdenados.size() - 1)) {
			valor = valorizador.get(instantesOrdenados.get(instantesOrdenados.size() - 1));
			return valor;
		}
		if (this.sentido.getSentido() == 1) {

			while (instante >= siguienteInstante) {
				siguienteIndice += 1;
				siguienteInstante = instantesOrdenados.get(siguienteIndice);

			}
			valor = valorizador.get(instantesOrdenados.get(siguienteIndice - 1));

		} else {

			while (instante < siguienteInstante) {
				siguienteIndice -= 1;
				siguienteInstante = instantesOrdenados.get(siguienteIndice);

			}
			valor = valorizador.get(siguienteInstante);

		}

		return valor;
	}
	
	public Integer getSiguienteInstante() {
		return siguienteInstante;
	}

	public void setSiguienteInstante(Integer siguienteInstante) {
		this.siguienteInstante = siguienteInstante;
	}

	public boolean isNomascambios() {
		return nomascambios;
	}

	public void setNomascambios(boolean nomascambios) {
		this.nomascambios = nomascambios;
	}

	public static void main(String[] args) {

		Hashtable<Integer, Double> vals = new Hashtable<Integer, Double>();
		vals.put(3, 23.2);
		vals.put(5, 2.2);
		vals.put(9, 11.2);

		System.out.println("SENTIDO = 1" );
		SentidoTiempo st = new SentidoTiempo(1);
		Evolucion<Double> ev = new EvolucionPorInstantes<Double>(vals, 100.0, st);
		for (int i = 0; i <= 10; i++) {
			System.out.println("Instante=" + i + " : " + ev.getValor(i));
		}
		System.out.println("----------------------------------------------------------" );
		System.out.println("----------------------------------------------------------" );
		System.out.println();
		
		System.out.println("SENTIDO = -1" );
		st = new SentidoTiempo(-1);
		ev.setSentido(st);

		for (int i = 10; i >= 0; i--) {
			System.out.println("Instante=" + i + " : " + ev.getValor(i));
		}
		System.out.println("----------------------------------------------------------" );
		System.out.println("----------------------------------------------------------" );
		System.out.println();
		
		System.out.println("SENTIDO = 1" );		
		st = new SentidoTiempo(1);
		ev.setSentido(st);
		
		for (int i = 10; i >= 0; i--) {
			System.out.println("Instante=" + i + " : " + ev.getValor(i));
		}

		System.out.println("----------------------------------------------------------" );
		System.out.println("----------------------------------------------------------" );
		System.out.println();
		System.out.println("SENTIDO = -1" );		
		st = new SentidoTiempo(-1);
		ev.setSentido(st);
		
		for (int i = 0; i < 10; i++) {
			System.out.println("Instante=" + i + " : " + ev.getValor(i));
		}


	}

	public int getSentidoLlamadaAnterior() {
		return sentidoLlamadaAnterior;
	}

	public void setSentidoLlamadaAnterior(int sentidoLlamadaAnterior) {
		this.sentidoLlamadaAnterior = sentidoLlamadaAnterior;
	}

	public int getInstanteInvocacionAnterior() {
		return instanteInvocacionAnterior;
	}

	public void setInstanteInvocacionAnterior(int instanteInvocacionAnterior) {
		this.instanteInvocacionAnterior = instanteInvocacionAnterior;
	}

	public T getValorInicial() {
		return valorInicial;
	}

	public void setValorInicial(T valorInicial) {
		this.valorInicial = valorInicial;
	}

	public Hashtable<Integer, T> getValorizador() {
		return valorizador;
	}

	public void setValorizador(Hashtable<Integer, T> valorizador) {
		this.valorizador = valorizador;
	}
}
