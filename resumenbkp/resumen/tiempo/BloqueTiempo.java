package tiempo;

import java.util.ArrayList;

import utilitarios.Constantes;

/**
 * Clase que representa un bloque de tiempo, todo bloque tiene un conjunto de pasos de igual duraci�n
 * @author ut602614
 *
 */
public class BloqueTiempo {
	private int primerPaso;
	private int cantidadPasos;
	private int duracionPaso;
	private int ordinalBloque; // ordinal del bloque empezando en 0 a partir del comienzo de la l�nea de tiempo
	
	/**
	 * ATENCI�N: EL PASO DE TIEMPO DE TODOS LOS PROCESOS ESTOC�STICOS MUESTREADOS DEBE
	 * COINCIDIR CON EL INTERVALO DE MUESTREO
	 */
	private int intervaloMuestreo; //en segundos, la duraci�n del paso y de los postes debe ser m�ltiplo de este valor
	private int instantesDesplazados;
	private int [] duracionPostes; //en segundos, la suma de todas la duraciones de los postes debe ser la duaracion del paso
	private int cantPostes;
	private boolean cronologico; 
	
	public BloqueTiempo(int ordinalBloque, int primerPaso, int pasosPorBloque, int durpaso, int intervaloMuestreo, ArrayList<Integer> durPostes, Boolean cronologico) {
		this.ordinalBloque = ordinalBloque;
		this.primerPaso = primerPaso;
		this.cantidadPasos = pasosPorBloque;
		this.duracionPaso = durpaso;
		this.intervaloMuestreo = intervaloMuestreo; 
		this.cronologico = cronologico;
		this.cantPostes = durPostes.size();
		duracionPostes = new int[cantPostes];
		for (int i = 0; i < cantPostes; i++) {
			duracionPostes[i] = durPostes.get(i);
		}
		setInstantesDesplazados((int)(Constantes.DESLIZAMIENTOMUESTREO*intervaloMuestreo));
	}
	
	public BloqueTiempo(int ordinalBloque, int primerPaso, int pasosPorBloque, int durpaso, int intervaloMuestreo, int[] durPostes, boolean cronologico) {
		this.ordinalBloque = ordinalBloque;		
		this.primerPaso = primerPaso;
		this.cantidadPasos = pasosPorBloque;
		this.duracionPaso = durpaso;
		this.intervaloMuestreo = intervaloMuestreo; 
		this.cronologico = cronologico;
		this.cantPostes = durPostes.length;
		duracionPostes = durPostes;
		
		setInstantesDesplazados((int)(Constantes.DESLIZAMIENTOMUESTREO*intervaloMuestreo));
	}
	
	
	
	public int getPrimerPaso() {
		return primerPaso;
	}
	public void setPrimerPaso(int primerPaso) {
		this.primerPaso = primerPaso;
	}
	public int getCantidadPasos() {
		return cantidadPasos;
	}
	public void setCantidadPasos(int cantidadPasos) {
		this.cantidadPasos = cantidadPasos;
	}
	public int getDuracionPaso() {
		return duracionPaso;
	}
	public void setDuracionPaso(int duracionPaso) {
		this.duracionPaso = duracionPaso;
	}
	
	
	
	public int getOrdinalBloque() {
		return ordinalBloque;
	}

	public void setOrdinalBloque(int ordinalBloque) {
		this.ordinalBloque = ordinalBloque;
	}

	public int getIntervaloMuestreo() {
		return intervaloMuestreo;
	}
	public void setIntervaloMuestreo(int intervaloMuestreo) {
		this.intervaloMuestreo = intervaloMuestreo;
	}
	public int [] getDuracionPostes() {
		return duracionPostes;
	}
	public void setDuracionPostes(int [] duracionPostes) {
		this.duracionPostes = duracionPostes;
	}
	public int getCantPostes() {
		return cantPostes;
	}
	public void setCantPostes(int cantPostes) {
		this.cantPostes = cantPostes;
	}
	public boolean isCronologico() {
		return cronologico;
	}
	public void setCronologico(boolean cronologico) {
		this.cronologico = cronologico;
	}


	public int getInstantesDesplazados() {
		return instantesDesplazados;
	}


	public void setInstantesDesplazados(int instantesDesplazados) {
		this.instantesDesplazados = instantesDesplazados;
	}
	

}
