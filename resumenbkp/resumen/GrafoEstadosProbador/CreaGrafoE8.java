/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstadosProbador;

import AlmacenSimulaciones.BolsaDeSimulaciones3;
import AlmacenSimulaciones.IdentSimulPaso;
import AlmacenSimulaciones.ImplementadorGeneral;
import GrafoEstados.AplicadorProgramacionDinamica;
import GrafoEstados.AplicadorProgramacionDinamica.TipoNoObs;
import GrafoEstados.ConjRestGrafoE;
import GrafoEstados.GrafoE;
import GrafoEstados.Objetivo;
import GrafoEstados.PeriodoDeTiempo;
import GrafoEstados.RecorredorPorEscenarios;
import ImplementaEDF.ImplementadorEDF;
import ImplementaMOP.ImplementadorMOP;
import dominio.Caso;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import UtilitariosGenerales.DirectoriosYArchivos;
import UtilitariosGenerales.ManejaObjetosEnDisco;
import dominio.Parque;
import dominio.RecorredorReportes;
import dominio.TiempoAbsoluto;
import dominio.XBloqueMultNeg;
import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import naturaleza3.BolsaDeConjsNodosN;
import naturaleza3.ListaEscenariosCN;
import persistencia.CargaModeloOP;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class CreaGrafoE8 {

//    public static void main(String[] args) throws XcargaDatos, IOException, FileNotFoundException, ClassNotFoundException, XBloqueMultNeg {
    public static void main(String[] args){    	
    	try{
	        String stringArchEstExist = "S:/UTE/MINGO";        
	//        String stringArchEstExist = "D:/_Migro/mibarburu/Mingo/PruebaMingo9CuatroTecCE";
	
	        JFileChooser constEstDeTexto = new JFileChooser();
	        constEstDeTexto.setSelectedFile(new File("S:/UTE/MINGO"));
	//        constEstDeTexto.setSelectedFile(new File("D:/_Migro/mibarburu/Mingo/PruebaMingo9CuatroTecCE"));        
	
	        constEstDeTexto.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	        constEstDeTexto.setDialogTitle("ELIJA DIRECTORIO DEL ESTUDIO CON DOBLE CLICK- SI CANCELA USA "+ stringArchEstExist);
	        constEstDeTexto.setPreferredSize(new Dimension(800, 400));
	        int result = constEstDeTexto.showOpenDialog(null);
	
	        if(result == JFileChooser.APPROVE_OPTION){
	            File archEstExist = constEstDeTexto.getSelectedFile();
	            stringArchEstExist = archEstExist.getPath();
	        }
	
	        String dirBase = stringArchEstExist;
	
	        // Lee el estudio
	        String dirEstudios = dirBase;
	        String dirImplementacion = "";
	        ImplementadorGeneral impG = null;
	        

	        // Lee el nombre del modelo de operaci�n a emplear
	        String dirModeloOperacion = dirBase + "/MODELO_OPERACION.txt";
	        // Si no hay archivo de configuraci�n se usa el EDF.
	        if(!UtilitariosGenerales.DirectoriosYArchivos.existeArchivo(dirModeloOperacion)){
	        	UtilitariosGenerales.ConstantesMingo.IMPLEMENTADOR = UtilitariosGenerales.ConstantesMingo.EDF;
	        }else{		        
	        	UtilitariosGenerales.ConstantesMingo.IMPLEMENTADOR = CargaModeloOP.devuelveModeloOp(dirModeloOperacion);	        	
	        }
	        
	        if(UtilitariosGenerales.ConstantesMingo.IMPLEMENTADOR.equalsIgnoreCase(UtilitariosGenerales.ConstantesMingo.EDF)){
	        	dirImplementacion = dirBase + "/" + "Datos_Implementacion";
	        	impG = new ImplementadorEDF();
	        }else if(UtilitariosGenerales.ConstantesMingo.IMPLEMENTADOR.equalsIgnoreCase(UtilitariosGenerales.ConstantesMingo.MOP)){
	        	dirImplementacion = dirBase + "/" + "Datos_ImplementacionMOP";
	        	impG = new ImplementadorMOP();
	        }
	        String dirCorridas = dirBase + "/" + "corridas";
	        String dirResultados = dirBase + "/Resultados";
	        String dirBolsaSim = dirBase + "/Resultados";
	        String archOrdinalPaquete = dirCorridas + "/" + "OrdinalPaquete.txt";
	        if(! UtilitariosGenerales.DirectoriosYArchivos.existeArchivo(archOrdinalPaquete)){
	            UtilitariosGenerales.DirectoriosYArchivos.copy2(dirImplementacion + "/OrdinalPaquete.txt",
	                    archOrdinalPaquete);
	        }
	        
	        impG.inicializaImplementador(dirImplementacion, dirEstudios, dirCorridas);
	        int cantPasos = 20;
	        Estudio est = new Estudio("Estudio", cantPasos, impG);
	        String texto;
	        est.leerEstudio(dirEstudios, true);
	        DatosGeneralesEstudio datgen = est.getDatosGenerales();
	        int cantEtapas = datgen.getCantEtapas();
	        ArrayList<TiempoAbsoluto> tiemposAbsolutosEtapas = new ArrayList<TiempoAbsoluto>();
	        for (int ita = 1; ita <= datgen.getCantEtapas(); ita++) {
	            tiemposAbsolutosEtapas.add(datgen.tiempoAbsolutoDeEtapa(ita));
	        }
	
	        int cantEtapasEstudio = est.getDatosGenerales().getCantEtapas();
	        
	        
	        // Lee el nombre del caso a analizar
	        
	        String nombreCaso = "CASO1";
	        JFileChooser elijeCaso = new JFileChooser();
	        elijeCaso.setSelectedFile(new File(stringArchEstExist+"/"+"Estudio"));
	        elijeCaso.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	        elijeCaso.setDialogTitle("ELIJA CASO CON DOBLE CLICK-SI CANCELA USA "+ stringArchEstExist+"/Estudio/"+nombreCaso);
	        elijeCaso.setPreferredSize(new Dimension(800, 400));
	        result = elijeCaso.showOpenDialog(null);
	
	        if(result == JFileChooser.APPROVE_OPTION){
	            File archCaso = elijeCaso.getSelectedFile();
	            String stringCaso = archCaso.getPath();
	            stringCaso = DirectoriosYArchivos.barraAscendente(stringCaso);
	            String[] auxS = stringCaso.split("/");
	            nombreCaso = auxS[auxS.length-1];
	        }        
	        
	        
	        
	        Caso caso1 = new Caso(est, nombreCaso);
	        boolean imprimir = true;
	        caso1.leerCaso(imprimir);
	        est.getCasos().add(caso1);
	
	        texto = est.getGrafoN().toStringCorto();
	        texto += est.getGrafoN().getBolsaCDP().toString();
	        String dirArchGN = dirBase + "/Estudio/ResultadoGrafoN.txt";
	        DirectoriosYArchivos.grabaTexto(dirArchGN, texto);
	
	        /**
	         * Crea los parques de paso cero y paso uno
	         * El pUno SOLO INCLUYE BLOQUES NO COMUNES
	         * LOS BLOQUES COMUNES SON VARIABLES STATIC DE LA CLASE PARQUE
	         * 
	         */ 
	        ArrayList<Object> informacion = new ArrayList<Object>();        
	        informacion.add(est);
	        informacion.add(caso1);
	        // agrega los par�metros de impresi�n del grafo
	        boolean imprimeExist = false;
	        boolean imprimeProd = true;
	        informacion.add((Boolean) imprimeExist);
	        informacion.add((Boolean) imprimeProd);
	        
	        Parque pCero = caso1.creaPCero();
	        boolean comunes = false;
	        boolean nocomunes = true;
	        Parque.calculaBloquesComunes(informacion);
	        
	        Parque pUno = caso1.creaPUno(comunes, nocomunes);
	
	        caso1.cargaRestDeParqueCorr();
	        double tasaDesc = datgen.getTasaDescPaso();
	
	        /**
	         * Crea el grafoE del caso
	         */
	        
	        ArrayList<int[]> etapasPasos = datgen.getEtapasPasos();
	        // carga los TiemposAbsolutos de cada etapa en el grafo incluso la etapa 0.
	        PeriodoDeTiempo per = new PeriodoDeTiempo();
	        ArrayList<PeriodoDeTiempo> tiempoAbsolutoDeEtapas = new ArrayList<PeriodoDeTiempo>();
	        tiempoAbsolutoDeEtapas.add(per);
	        for (int ie = 1; ie <= cantEtapas; ie++) {
	            tiempoAbsolutoDeEtapas.add(datgen.tiempoAbsolutoDeEtapa(ie));
	        }
	
	        GrafoE gCaso = new GrafoE(cantEtapasEstudio, tasaDesc,
	                est.getGrafoN(),
	                etapasPasos,
	                tiempoAbsolutoDeEtapas,
	                pCero, pUno,
	                caso1.getConjRestSigParqueResult(), caso1.getSucRestHParque().get(0),
	                caso1.getConjRestSigInicioResult(),
	                informacion);
	        
	        caso1.setGrafoE(gCaso);
	        String dirCaso = dirBase + "/Estudio/" + nombreCaso;
	        String dirResultCaso = dirCaso + "/Result_Caso";
	        /**
	         * Si no existe el directorio de resultados del caso lo crea
	         */
	        UtilitariosGenerales.DirectoriosYArchivos.creaDirectorio(dirCaso, "Result_Caso");
	
	        /**
	         * Si no se quiere imprimir el grafo asignar null
	         * en el segundo argumento de construyeGrafoE()
	         */
	        int construyeGrafo = JOptionPane.showConfirmDialog(null, "�Quiere construir el grafo de estados?");
	        if (construyeGrafo == JOptionPane.OK_OPTION) {
	            int imprimeGrafo = JOptionPane.showConfirmDialog(null, "�Quiere escribir el grafo en disco?");
	            if (imprimeGrafo == JOptionPane.OK_OPTION) {
	                gCaso.construyeGrafoE(informacion, dirResultados);
	            }else{
	                gCaso.construyeGrafoE(informacion, null);
	            }
	            String dirArchGE = dirCaso + "/ResultadoGrafoE.txt";
	            if (imprimeGrafo == JOptionPane.OK_OPTION) {
	                gCaso.escribeGrafoE(informacion, null, dirArchGE);
	            }                     
	        }        
	
	
	        String archEstudioSerializado = "estudioObj";
	        caso1.setGrafoE(null);
	        // OJOJOJOJOJOJO SE ROMPE LA SERIALIZACI�N SI SE AGREGA EL GRAFOE AL ESTUDIO
	        UtilitariosGenerales.ManejaObjetosEnDisco.guardarEnDisco(dirEstudios, archEstudioSerializado, est);
	        caso1.setGrafoE(gCaso);
	
	
	
	        BolsaDeSimulaciones3 bolsaSim = new BolsaDeSimulaciones3(tiemposAbsolutosEtapas,impG);
	
	        // Levanta la BolsaDeSimulaciones si se desea
	        int levantaBolsaSim = JOptionPane.showConfirmDialog(null, "�Quiere levantar la BolsaDeSimulaciones?");
	        if (levantaBolsaSim == JOptionPane.OK_OPTION) {
	            bolsaSim = (BolsaDeSimulaciones3) ManejaObjetosEnDisco.traerDeDisco(dirBolsaSim, "BolsaSim");
	            bolsaSim.ampliarBolsa(est);
	        }
	        // Genera los directorios de datos y comandos de las corridas necesarias
	        ArrayList<IdentSimulPaso> listaCorridas = caso1.simulacionesFaltantes(informacion);
	
	
	        // Lee el ordinal del �ltimo paquete corrido en resultados
	
	
	        ArrayList<ArrayList<String>> dat = persistencia.LeerDatosArchivo.getDatos(archOrdinalPaquete);
	        int ordinalPaquete = Integer.parseInt(dat.get(1).get(1));
	
	        // Lanza las simulaciones
	        int lanzar = JOptionPane.showConfirmDialog(null, "�Quiere lanzar corridas?");
	        if (lanzar == JOptionPane.OK_OPTION) {
	            int nuevoPaq = JOptionPane.showConfirmDialog(null, "�Quiere crear nuevo paquete de corridas? " +
	                    "De lo contrario borra las anteriores");
	            if(nuevoPaq == JOptionPane.OK_OPTION){
	                ordinalPaquete ++;
	                texto = "ORDINAL DEL ULTIMO PAQUETE CORRIDO" + "\r\n";
	                texto += "ORDINAL_PAQUETE " +  ordinalPaquete;
	                UtilitariosGenerales.DirectoriosYArchivos.grabaTexto(archOrdinalPaquete, texto);
	            }
	            bolsaSim.lanzaPaqueteCorridas(listaCorridas, ordinalPaquete, dirCorridas, informacion);
	
	        }
	
	        // Levanta las simulaciones
	        int levantar = JOptionPane.showConfirmDialog(null, "�Quiere levantar corridas?");
	        if (levantar == JOptionPane.OK_OPTION) {
	            bolsaSim.levantaPaqueteCorridas(est, dirCorridas, ordinalPaquete, 0, informacion);
	            // Guarda la bolsa de simulaciones con las simulaciones agregadas
	            ManejaObjetosEnDisco.guardarEnDisco(dirBolsaSim, "BolsaSim", bolsaSim);
	        }
	
	
	
	        // Ejecuta la programaci�n din�mica del caso
	        AplicadorProgramacionDinamica APD = new AplicadorProgramacionDinamica(gCaso, bolsaSim);
	        String dirArchPD = dirResultCaso + "/ResultadoPD.txt";
	
	        Objetivo objetivo = caso1.getObj();
	        APD.aplicaPD(objetivo, bolsaSim, dirArchPD, TipoNoObs.Indep, true);
	
	
	        // Graba resultados GrafoE
	        int grabar = JOptionPane.showConfirmDialog(null, "�Quiere grabar GrafoE?");
	        if (grabar == JOptionPane.OK_OPTION) {
	            /**
	             * Escribe ResultadoGrafoE2 que tiene tambi�n el resultado
	             * de la programaci�n din�mica
	             */
	            String dirArchGE2 = dirResultCaso +"/ResultadoGrafoE2.txt";
	            gCaso.escribeGrafoE(informacion, objetivo, dirArchGE2);
	
	        }
	
	 
	
	        // Recorre trayectorias �ptimas y determina si hay restricciones heur�sticas activas
	     
	        int iterHeuristica = caso1.getSucRestHParque().size();
	        ConjRestGrafoE conjRHCorr = caso1.restHDeIter(iterHeuristica);
	        ConjRestGrafoE conjRSigParque = caso1.getConjRestSigParqueResult();
	
	        BolsaDeConjsNodosN bolsaCDP = est.getGrafoN().getBolsaCDP();
	        ListaEscenariosCN listaEsc = new ListaEscenariosCN(bolsaCDP, "Lista escenarios");
	
	        listaEsc = bolsaCDP.devuelveListaEsc();
	
	//        String archExpansion = dirResultCaso + "/DecisionesOptimas.txt";
	        RecorredorPorEscenarios.recorreEsc3(gCaso, bolsaSim, caso1.getObj(),
	                conjRHCorr, conjRSigParque, listaEsc, dirResultCaso);
	        RecorredorReportes.generaReportes(gCaso, caso1.getObj(), listaEsc, dirResultCaso);
	
	        String descRestH = gCaso.generaDescViolacionesRestricHeu(informacion);
	        String archRestH = dirResultCaso + "/RestriccionesHeuristicas.txt";
	        UtilitariosGenerales.DirectoriosYArchivos.grabaTexto(archRestH, descRestH);
	        JOptionPane.showMessageDialog(null, "FIN DE EJECUCI�N\n Verificar los resultados en:\n "+dirResultCaso);
	        System.out.println("FIN DE EJECUCI�N\n Verificar los resultados en:\n "+dirResultCaso);
	        
	    }
    	catch(Exception e){
			System.out.print(e.toString()+"\n");
			e.printStackTrace();
    	}
    }
}
