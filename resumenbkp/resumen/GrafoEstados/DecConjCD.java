/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import java.io.Serializable;
import java.util.ArrayList;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class DecConjCD implements Serializable {

    /**
     * 
     * Describe los sucesores de un CD con una decisión dada
     *
     */
    private Portafolio decision; // la decisión
    private ArrayList<ConjDeDecision> cdSucs; // los posibles CD sucesores
    private ArrayList<Double> probTrans;

    /**
     * las probabilidades respectivas de los cd de conjsDeDEc,
     * que son las de transición de las
     * variables de la naturaleza observadas.
     */
    public Portafolio getDecision() {
        return decision;
    }

    public void setDecision(Portafolio decision) {
        this.decision = decision;
    }

    public ArrayList<ConjDeDecision> getCdSucs() {
        return cdSucs;
    }

    public void setCdSucs(ArrayList<ConjDeDecision> cdSucs) {
        this.cdSucs = cdSucs;
    }

    public ArrayList<Double> getProbTrans() {
        return probTrans;
    }

    public void setProbTrans(ArrayList<Double> probTrans) {
        this.probTrans = probTrans;
    }

    @Override
    public String toString() {
        String texto = "Par nodo-decision";
        texto += decision.toString() + "\r\n";
        texto += "CDs: ";
        int ind = 0;
        for (ConjDeDecision cd : cdSucs) {
            texto += cd.toString() + "prob=" + probTrans.get(ind) + "\r\n";
            ind++;
        }
        return texto;
    }

    public String toStringCorto(ArrayList<Object> informacion) throws XcargaDatos {
        String texto = "Par nodo-decision" + "\r\n";
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        boolean soloEleg = true;
        boolean prod = false;
        boolean usaEquiv = false;
        boolean soloNoNulos = true;
        informacionImp.add(soloEleg);
        informacionImp.add(prod);
        informacionImp.add(usaEquiv);
        informacionImp.add(soloNoNulos);
        texto += decision.toStringCorto(informacion, informacionImp) + "\n";
        texto += "Nodos: ";
        int ind = 0;
        for (ConjDeDecision cd : cdSucs) {
            texto += cd.getOrdinalCD() + "prob=" + probTrans.get(ind) + "\r\n";
            ind++;
        }
        return texto;
    }
//     public String toStringCortoLinea(ArrayList<Object> informacion) throws XcargaDatos {
//          ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
//          informacionImp.add(true);
//          informacionImp.add(false);
//          informacionImp.add(false);
//          String texto = decision.toStringCortoLinea(informacion, informacionImp) + ",";
//          int ind =0;
//          for (ConjDeDecision cd : cdSucs) {
//               texto += "OrdinalCD " + cd.getOrdinalCD() + " prob=" + probTrans.get(ind);
//               ind ++;
//          }
//          return texto;
//     }
}
