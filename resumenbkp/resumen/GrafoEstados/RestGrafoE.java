/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.NodoN;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class RestGrafoE implements Serializable {

     protected String nombre;
     protected boolean esHeuristica;
     protected String pasoEtapa;
     /**
      * Si es PASO la restricción se define para todos los pasos del estudio
      * Si es ETAPA la restricción se define sólo para los pasos representativos de las etapas
      * y los índices de los parámetros de la restricción se refieren a los índices de etapas.
      */
     public static final String PASO = "PASO";
     public static final String ETAPA = "ETAPA";
     
     protected ArrayList<Object> informacion;
     protected PeriodoDeTiempo tiempoAbsolutoBase;
     /**
      * TiempoAbsoluto a partir del cual se presentan los parámetros que dependen del tiempo.
      * En los arrays de las clases herederas el get(0) corresponderá a ese tiempoAbsoluto
      */
     protected ArrayList<IncumplimientoRHeu> violaciones;

     /**
      * Conjunto de CDs que no verifican la restricción, entre los recorridos por
      * el recorredor de escenarios.
      */
     public RestGrafoE() {
          violaciones = new ArrayList<IncumplimientoRHeu>();
     }

     public RestGrafoE(boolean esHeuristica, String pasoEtapa, ArrayList<Object> informacion, PeriodoDeTiempo tiempoAbsolutoBase) {
          this.esHeuristica = esHeuristica;
          this.pasoEtapa = pasoEtapa;
          this.informacion = informacion;
          this.tiempoAbsolutoBase = tiempoAbsolutoBase;
          violaciones = new ArrayList<IncumplimientoRHeu>();
     }

     public String getNombre() {
          return nombre;
     }

     public void setNombre(String nombre) {
          this.nombre = nombre;
     }

     public boolean isEsHeuristica() {
          return esHeuristica;
     }

     public void setEsHeuristica(boolean esHeuristica) {
          this.esHeuristica = esHeuristica;
     }

     public String getPasoEtapa() {
          return pasoEtapa;
     }

     public void setPasoEtapa(String pasoEtapa) {
          this.pasoEtapa = pasoEtapa;
     }

     public PeriodoDeTiempo getTiempoAbsolutoBase() {
          return tiempoAbsolutoBase;
     }

     public void setTiempoAbsolutoBase(PeriodoDeTiempo tiempoAbsolutoBase) {
          this.tiempoAbsolutoBase = tiempoAbsolutoBase;
     }

     public ArrayList<IncumplimientoRHeu> getViolaciones() {
          return violaciones;
     }

     public void setViolaciones(ArrayList<IncumplimientoRHeu> violaciones) {
          this.violaciones = violaciones;
     }

     public Object getInformacion() {
          return informacion;
     }

     public void setInformacion(ArrayList<Object> informacion) {
          this.informacion = informacion;
     }

     /**
      * Crea un texto con el resumen de las violaciones de esta restricción
      * donde cada línea tiene // para indicar comentario
      */
     public String resumenViolaciones(ArrayList<Object> informacion) throws XcargaDatos {
          String result = "// RESTRICCIÓN: " + nombre + " - Listado de violaciones " + "\r\n";
          if(violaciones.isEmpty()) result += "NO HAY VIOLACIONES A LA RESTRICCIÓN"  + "\r\n";
          for (IncumplimientoRHeu tcdp : violaciones) {
               result += tcdp.toStringCorto(informacion);
          }
          result += "\r\n";
          return result;
     }

//     public enum PasoEtapa {
//
//          PASO, // La restricción está expresada en los pasos de tiempo del estudio
//          ETAPA;  // La restricción está expresada en las etapas del estudio
//     }

//**********************************************************************
// INICIO DE METODOS A SOBREESCRIBIR POR LAS RESTRICCIONES
// QUE HEREDEN DE ESTA CLASE
//**********************************************************************
//     /**
//      * METODO evaluar con un sólo portafolio
//      *
//      * Evalúa si la restricción es cumplida por el Portafolio p
//      * cuando el nodo de la naturaleza es n
//      *
//      * El portafolio vive en un período de tiempo y el nodoN n también
//      *
//      * Devuelve true si la restricción se cumple
//      * Devuelve false si las restricción no se cumple
//      *
//      * @param informacion
//      *
//      * @param p es el portafolio que se quiere evaluar.
//      *
//      * @param n es el NodoN, que en la implementación actual es irrelevante.
//      *
//      * @return result true si se cumple la restricción y false si no se cumple.
//      *
//      */
//     public boolean evaluar(ArrayList<Object> informacion,
//             Portafolio p, NodoN n) throws XcargaDatos {
//          boolean result = true;
//          return result;
//     }



     /**
      * Método evaluar con dos portafolios
      *
      * El primer portafolios p es el de un NodoE del que se parte
      *
      * El segundo portafolios d es el de una Decision que se puede aplicar
      *
      * Evalúa si la restricción es cumplida por el Portafolio p (y la decisión d)
      * cuando el nodo de la naturaleza es n
      *
      * El portafolio vive en un período de tiempo y el nodoN n también
      *
      * Devuelve true si la restricción se cumple
      * Devuelve false si las restricción no se cumple
      *
      * @param informacion
      *
      * @param p es el portafolio que se quiere evaluar.
      *
      * @param n es el NodoN, que en la implementación actual es irrelevante.
      *
      * @return result true si se cumple la restricción y false si no se cumple.*
      *
      * Si estos métodos no están implementados en las clases hijas,
      * al invocarlos dan true, es decir es como si las restricciones no actuaran.
      *
      */
     public boolean evaluar(ArrayList<Object> informacion,
             Portafolio p, Portafolio d, NodoN n) throws XcargaDatos {
          boolean result = true;
          return result;
     }





     public RestGrafoE pasaAEtapas() {
          RestGrafoE result = new RestGrafoE();
          return result;
     }
//**********************************************************************
// FIN DE METODOS A SOBREESCRIBIR POR LAS RESTRICCIONES QUE HEREDEN DE ESTA CLASE
//**********************************************************************
}


