/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author ut469262
 */
public class ConjRestGrafoE implements Serializable {

    private Object informacion;
    private GrafoE grafoE;
    private ArrayList<RestGrafoE> restricciones;

    public ConjRestGrafoE() {
        restricciones = new ArrayList<RestGrafoE>();
    }

    public GrafoE getGrafoE() {
        return grafoE;
    }

    public void setGrafoE(GrafoE grafoE) {
        this.grafoE = grafoE;
    }

    public Object getInformacion() {
        return informacion;
    }

    public void setInformacion(Object informacion) {
        this.informacion = informacion;
    }

    public ArrayList<RestGrafoE> getRestricciones() {
        return restricciones;
    }

    public void setRestricciones(ArrayList<RestGrafoE> restricciones) {
        this.restricciones = restricciones;
    }

    public GrafoE getGrafo() {
        return grafoE;
    }

    public void setGrafo(GrafoE grafoE) {
        this.grafoE = grafoE;
    }

//    /**
//     * OJOJOJOJOJOJOJOJOJO
//     * @param pt
//     * @param informacion
//     * @param p
//     * @param n
//     * @return
//     */
//    public boolean esInterior(PeriodoDeTiempo pt, Object informacion,
//            Portafolio p, NodoN n){
//        /**
//         * Evalúa si el portafolio p es interior a
//         */
//        boolean result;
//        return result = false;
//    }
    /**
     * Crea un conjuntos de restricciones con todas las restricciones
     * de una lista de conjuntos de restricciones
     * @param conjsRest es la lista de conjuntos.
     * @return suma es el conjunto suma de restricciones.
     */
    public static ConjRestGrafoE sumaConjuntos(ArrayList<ConjRestGrafoE> conjsRest) {
        ConjRestGrafoE suma = new ConjRestGrafoE();
        for (ConjRestGrafoE cR : conjsRest) {
            suma.getRestricciones().addAll(cR.getRestricciones());
        }
        return suma;
    }

    @Override
    public String toString() {
        String texto = "// CONJUNTO DE RESTRICCIONES";
        texto += "\r\n";
        for (RestGrafoE r : restricciones) {
            texto += r.toString();
            texto += "\r\n";
        }
        return texto;
    }
}
