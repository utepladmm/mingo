/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import AlgoritmoGenetico.UtilitariosProgGen;
import AlmacenSimulaciones.BolsaDeSimulaciones3;
import dominio.Estudio;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import naturaleza3.ConjNodosN;
import naturaleza3.EscenarioCN;
import naturaleza3.ListaEscenariosCN;
import naturaleza3.NodoN;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class RecorredorPorEscenarios implements Serializable {

    /**
     *
     * Recorre en grafoE las trayectorias óptimas (en los CDP) para cada uno
     * de los escenarios de la lista de escenarios, en la optimización
     * del grafoE con el objetivo obj.
     *
     * Carga en las restricciones del conjunto de restricciones heurísticas de
     * parque del GrafoE grafoE, conjRHP, la lista de conjuntos CDP pertenecientes
     * a las trayectorias óptimas en las que las restricciones no se cumplen con holgura
     *
     * Crea un reporte de expansión óptima para cada escenario y lo guarda en archExpan.
     * Reporta los costos y portafolios de CDs asociados a cada CDP que tienen costo anual esperado
     * cercanos al óptimo, en un porcentaje o en una magnitud del objetivo fija.
     *
     * Carga en las restricciones heurísticas las violaciones registradas
     * (restricciones que no se cumplen con holgura)
     *
     * @param grafoE es el grafo a recorrer
     * 
     * @param bolsaDeSimulResult es el objeto con resultados de simulación, en particular
     * la BolsaDeSimulaciones
     *
     * @param obj es el objetivo cuyas decisiones óptimas se recorren
     *
     * @param conjRHP es el conjunto de restricciones heurísticas de parque
     * (cuyas violaciones hay que reportar)
     *
     * @param conjRSP es el conjunto de restricciones significativas de parque, que
     * son necesarias para determinar los parques contiguos a uno dado para saber si es interior.
     *
     * @param listaEsc es una ListaEscenariosCN con los escenarios de ConjNodosN a recorrer
     *
     * @param archExpan es el nombre del archivo donde se grabarán las expansiones
     * óptimas, una para cada escenario. En otro archivo con sufijo res antes de la
     * extensión se graba un resumen.
     *
     */
    public static void recorreEsc3(GrafoE grafoE, Object bolsaDeSimulResult, Objetivo obj,
            ConjRestGrafoE conjRHP, ConjRestGrafoE conjRSP,
            ListaEscenariosCN listaEsc, String dirResultCaso) throws XcargaDatos {

        String archExpan = dirResultCaso + "/DecisionesOptimas.txt";

        String archResum = archExpan.replace(".", "Res.");
        String textoExpan = "EXPANSIONES OPTIMAS PARA CADA ESCENARIO DE VARIABLES DE LA NATURALEZA "
                + "OBSERVADAS Y QUE DETERMINAN PARQUE" + "\r\n";
        String textoResum = textoExpan;
        ArrayList<Object> informacion = grafoE.getInformacion();
        Estudio est = (Estudio) informacion.get(0);
        boolean simple = est.getComContiguosSimple();
        // Elimina las bases de datos de resultados si existen
        String archDB = dirResultCaso + "/DatosAnuales.db";
        boolean yaHayDB = UtilitariosGenerales.DirectoriosYArchivos.existeArchivo(archDB);
        if (yaHayDB) {
            UtilitariosGenerales.DirectoriosYArchivos.eliminaArchivo(archDB);
        }

        // Crea nuevas bases de datos de resultados (las tablas vacías) 
        BolsaDeSimulaciones3 bds3 = (BolsaDeSimulaciones3) bolsaDeSimulResult;
        try {
            bds3.creaDB(dirResultCaso);
            // OJO CAPAZ QUE HAY QUE CREAR UN SOLO OBJETO 
            // Connection conex= DriverManager.getConnection("jdbc:sqlite:"+dirResultCaso+"/DatosAnuales.db");       
        } catch (Exception e) {
            throw new XcargaDatos("no se pudo conectar a la base de datos"+e.toString());
        }

        int iesc = 1;
        for (EscenarioCN esc : listaEsc.getEscenarios()) {
            System.out.println("Inicia recorrido escenario = " + iesc);
            String textoCabezal = "\r\n" + "--------------------------" + "\r\n";
            textoCabezal += "\r\n" + "ESCENARIO NUMERO : " + iesc + "\r\n";
            textoCabezal += "\r\n" + "--------------------------" + "\r\n";
            textoExpan += textoCabezal;
            textoResum += textoCabezal;
            ConjDeDecision cDUno = grafoE.getBolsaCD().devuelveCDInicial();
            ConjDetPortE cDPEe = cDUno.getConjsDetP().get(0);
            for (int e = 1; e <= grafoE.getCantEtapas(); e++) {
                System.out.println("Inicia etapa = " + e);                
                /**
                 * cDPNe es el conjunto determinante de parque en grafoN, es por
                 * lo tanto un ConjNodosN que tienen el mismo estado de las variables
                 * de la naturaleza observadas y de las que determinan parque (si existen)
                 *
                 * cDPEe es el conjunto determinante de parque en grafoE, conjunto de
                 * nodosE tales que tienen el mismo sucesor en e+1 si en e+1 el estado de
                 * la naturaleza es el mismo
                 *
                 * cDe es el conjunto de decisión en e
                 * porte es el portafolio del cd en e
                 *
                 */
                PeriodoDeTiempo pt = grafoE.getPerTiempoDeEtapas().get(e);

                ConjNodosN cDPNe = esc.getConjsNodosN().get(e);

                ConjDeDecision cDe = cDPEe.getConjDec();
                Portafolio porte = cDe.getPort();
                /**
                 *  Procesa las restricciones heurísticas
                 */
                boolean cumpleRest = true;

                Portafolio pvacio = new Portafolio();
                NodoN nN = cDe.getConjInfo().getNodos().get(0); 
                for (RestGrafoE rh : conjRHP.getRestricciones()) {
                    boolean resultado = rh.evaluar(grafoE.getInformacion(), porte, pvacio, nN);
                    /**
                     * En la implementación actual las restricciones heurísticas
                     * no dependen del estado de la naturaleza, por lo tanto el NodoN
                     * se llena con null.
                     */
                    if (resultado == false) {
                        // el portafolio porte no cumple la restricción es un error de programa
//                        cumpleRest = false;
//                        TernaCDPeriodoEsc tcd = new TernaCDPeriodoEsc(cDe, pt, esc);
//                        rh.getViolaciones().add(tcd);
                        throw new XcargaDatos("Error: un portafolio de un cd óptimo no cumple "
                                + "restricciones heurísticas - período " + pt.toStringCorto() + "escenario " + esc);
//                        break;
                    }
                }
                if (cumpleRest) {
                    // el portafolio cumple todas las restricciones pero puede no ser interior
                    ArrayList<Portafolio> contiguos = porte.entregaContiguos(grafoE.getInformacion(), conjRSP, simple);
                    ArrayList<ParRestPort> causanNoInt =
                            porte.esPortafolioInterior2(nN, informacion, conjRHP, conjRSP, contiguos);
                    /**
                     *  Si porte es interior causanNoInt es vacío
                     *  Si porte no es interior causanNoInt tiene la lista de restricciones heurísticas
                     *  que son violadas por los portafolios contiguos a porte
                     */
                    for (ParRestPort prp : causanNoInt) {
                        RestGrafoE rh = prp.getRest();
                        Portafolio porC = prp.getPort();
//                        porC.cargaResumenP(informacion, false);
                        IncumplimientoRHeu tcd = new IncumplimientoRHeu(cDe, pt, esc, porC);
                        if (!(rh.getViolaciones().contains(tcd))) {
                            rh.getViolaciones().add(tcd);
                        }
                    }
                }
                /**
                 * Genera la descripción del CD en expansión óptima del escenario
                 */
                String reporteCD = cDe.generaReporteCDOptimo(grafoE, obj, est,
                        bolsaDeSimulResult, dirResultCaso);
                textoResum += reporteCD;
                /**
                 * Agrega al texto expandido la descripción de cada NOdoE
                 */
                textoExpan += reporteCD;
                textoExpan += "---------------------------------------" + "\r\n";
                textoExpan += "Nodos del cDPE" + "\r\n";
                for (NodoE nE : cDPEe.getNodosE()) {
                    textoExpan += nE.resultPDDelNodo2(obj, informacion);
                    textoExpan += "\r\n";
                }


                /**
                 * Genera el ConjDetPortE sucesor cDPEemas1 salvo que se est� en la �ltima etapa
                 */
                if (e < grafoE.getCantEtapas()) {
                    ConjNodosN cDPNemas1 = esc.getConjsNodosN().get(e + 1);
                    ConjDetPortE cDPEemas1 = cDPEe.cDPortSucesor(cDPNe, cDPNemas1, obj);
                    cDPEe = cDPEemas1;
                }
            }
            iesc++;
        }
        /**
         * Graba los textos de los dos archivos de salida
         */
        UtilitariosGenerales.DirectoriosYArchivos.grabaTexto(archExpan, textoExpan);
        UtilitariosGenerales.DirectoriosYArchivos.grabaTexto(archResum, textoResum);
    }

//    /**
//     * Genera y graba en disco los reportes de trayectorias óptimas
//     * @param grafoE
//     * @param obj
//     * @param listaEsc
//     * @param dirResultCaso
//     * @throws XcargaDatos
//     * @throws SQLException 
//     */
//    public static void generaReportes(GrafoE grafoE, Objetivo obj, ListaEscenariosCN listaEsc, String dirResultCaso)
//            throws XcargaDatos, SQLException {
//
//
//        ArrayList<Object> informacion = grafoE.getInformacion();
//        Estudio est = (Estudio) informacion.get(0);
//        boolean simple = est.getComContiguosSimple();
//        String archDB = dirResultCaso + "/DatosAnuales.db";
//        // Determina cantidad de fuentes en la base de datos
//        Statement st = null;
//        Connection conex=null;
//        int cantFuentes;
//        try{
//            Class.forName("org.sqlite.JDBC");
//            conex= DriverManager.getConnection("jdbc:sqlite:"+dirResultCaso+"/DatosAnuales.db"); 
//            st=conex.createStatement();
//            ResultSet rs= st.executeQuery("select count(id_fuente) from Fuentes;");
//            rs.next();
//            cantFuentes= rs.getInt(1);                    
//        }
//        catch(Exception e){
//            throw new XcargaDatos("Error en la consulta de la cantidad de fuentes");
//        }finally{
//            conex.close();
//            st.close();
//        }                
//        int cantEtapas = grafoE.getCantEtapas();
//        int nfilp = 1 + est.getCantRecNoComunesYTransPosibles() + est.cantRecBaseEleg() + est.cantTransBaseEleg();
//        int ncol = 1 + cantEtapas;
//        int nfile= 10+ cantFuentes;
//        int nfilc= 10+ cantFuentes;
//        String[][] portsOpt = new String[nfilp][ncol];       
//        String[][] energias = new String[nfile][ncol];
//        String[][] costos = new String[nfilc][ncol];
//
//        int iesc = 1;
//        for (EscenarioCN esc : listaEsc.getEscenarios()) {
//
//            ConjDeDecision cDUno = grafoE.getBolsaCD().devuelveCDInicial();
//            ConjDetPortE cDPEe = cDUno.getConjsDetP().get(0);
//            for (int e = 1; e <= grafoE.getCantEtapas(); e++) {
//                /**
//                 * cDPNe es el conjunto determinante de parque en grafoN, es por
//                 * lo tanto un ConjNodosN que tienen el mismo estado de las variables
//                 * de la naturaleza observadas y de las que determinan parque (si existen)
//                 *
//                 * cDPEe es el conjunto determinante de parque en grafoE, conjunto de
//                 * nodosE tales que tienen el mismo sucesor en e+1 si en e+1 el estado de
//                 * la naturaleza es el mismo
//                 *
//                 * cDe es el conjunto de decisión en e
//                 * porte es el portafolio del cd en e
//                 *
//                 */
//                PeriodoDeTiempo pt = grafoE.getPerTiempoDeEtapas().get(e);
//
//                ConjNodosN cDPNe = esc.getConjsNodosN().get(e);
//
//                ConjDeDecision cDe = cDPEe.getConjDec();
//                Portafolio porte = cDe.getPort();
//
//                /**
//                 * Comienza reportes de un año 
//                 */
//
//                // Carga el Parque óptimo y la Decision óptima
//                
//                
//                
//                
//                for (NodoE nE : cDPEe.getNodosE()) {
//
//                }               
//                
//                
//                
//
//                /**
//                 * Genera el ConjDetPortE sucesor cDPEemas1 salvo que se esté en la última etapa
//                 */
//                if (e < grafoE.getCantEtapas()) {
//                    ConjNodosN cDPNemas1 = esc.getConjsNodosN().get(e + 1);
//                    ConjDetPortE cDPEemas1 = cDPEe.cDPortSucesor(cDPNe, cDPNemas1, obj);
//                    cDPEe = cDPEemas1;
//                }
//            }
//            iesc++;
//        }
//        /**
//         * Graba los textos de los dos archivos de salida
//         */
//        UtilitariosGenerales.DirectoriosYArchivos.grabaTexto(archExpan, textoExpan);
//        UtilitariosGenerales.DirectoriosYArchivos.grabaTexto(archResum, textoResum);
//    } 
    
    
}
