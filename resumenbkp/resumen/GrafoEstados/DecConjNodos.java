/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import java.io.Serializable;
import java.util.ArrayList;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class DecConjNodos implements Serializable {

    /**
     * Pareja (lista de nodosE, decisión)
     * En cada NodoE hay estas parejas para indicar
     * - los predecesores: el nodoE del que se sale y la decisión que lleva
     *   a N1
     * - los sucesores: la decisión d y la lista de nodosE a los que se llega
     *    partiendo de N1 con la decisión d, cuando cambia el NodoN de destino  
     *
     *
     *
     */
    private ArrayList<NodoE> nodosE;
    private Portafolio decision;

    public DecConjNodos(ArrayList<NodoE> nodosE, Portafolio decision) {
        this.nodosE = nodosE;
        this.decision = decision;
    }

    public DecConjNodos(Portafolio decision) {
        nodosE = new ArrayList<NodoE>();
        this.decision = decision;
    }

    public Portafolio getDecision() {
        return decision;
    }

    public void setDecision(Portafolio decision) {
        this.decision = decision;
    }

    public ArrayList<NodoE> getNodosE() {
        return nodosE;
    }

    public void setNodosE(ArrayList<NodoE> nodosE) {
        this.nodosE = nodosE;
    }

    @Override
    public String toString() {
        String texto = "Par nodo-decision";
        texto += decision.toString() + "\n";
        texto += "Nodos: ";
        for (NodoE ne : nodosE) {
            texto += ne.toString() + "\n";
        }
        return texto;
    }

    public String toStringCorto(ArrayList<Object> informacion) throws XcargaDatos {
        String texto = "Par nodo-decision" + "\n";
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();

        boolean soloNoComunes = true;
        boolean prod = false;
        boolean usaEquiv = false;
        boolean soloNoNulos = true;
        informacionImp.add(soloNoComunes);
        informacionImp.add(prod);
        informacionImp.add(usaEquiv);
        informacionImp.add(soloNoNulos);        
        texto += decision.toStringCorto(informacion, informacionImp) + "\n";
        texto += "Nodos: ";
        for (NodoE ne : nodosE) {
            texto += ne.getOrdinalNE() + "\t";
        }
        return texto;
    }
}
