/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GrafoEstados;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class AtributosPortafolio implements Serializable{
	private ArrayList<ConjDeDecision> cDsUnPortafolio;
	/**
	 * cDUnPortafolio es el conjunto de los conjuntos de decisión asociados
	 * a un portafolio en una etapa dada.
	 */


	public AtributosPortafolio() {
		cDsUnPortafolio = new ArrayList<ConjDeDecision>();
	}

	public ArrayList<ConjDeDecision> getCDsUnPortafolio() {
		return cDsUnPortafolio;
	}

	public void setCDsUnPortafolio(ArrayList<ConjDeDecision> cDsUnPortafolio) {
		this.cDsUnPortafolio = cDsUnPortafolio;
	}

	

	public void agregaUnCD(ConjDeDecision cd){
		cDsUnPortafolio.add(cd);
	}

	public boolean eliminaUnCD(ConjDeDecision cd){
		/**
		 * Elimina el CD cd de los atributos de portafolio y
		 * devuelve false si no existía el CD
		 */
		boolean result = cDsUnPortafolio.remove(cd);
		return result;
	}
	
	
	
}
