/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import UtilitariosGenerales.DirectoriosYArchivos;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import naturaleza3.ConjNodosN;
import naturaleza3.GrafoN;
import naturaleza3.NodoN;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 * Clase GrafoE
 * Es la estructura de nodos sobre la que se hace la programaciÃ³n dinÃ¡mica.
 *
 * En la programaciÃ³n dinÃ¡mica sÃ³lo se calculan valores de Bellman al inicio de
 * los pasos de tiempo representativos de cada etapa. Por esto un GrafoE es un
 * grafo cuyos nodos viven en las etapas.
 *
 * En las transiciones de la programaciÃ³n dinÃ¡mica, para calcular el VB al comienzo
 * de la etapa e, se suman los costos de pasos entre el paso representativo de la etapa e
 * inclusive y el paso anterior al representativo de la etapa e+1 inclusive, a
 * menos que e sea la Ãºltima etapa y allÃ­ se incluye solo el paso
 * representativo (que es el Ãºnico de esa etapa).
 *
 */
public class GrafoE implements Serializable {

//    private Estudio est;
    private int cantEtapas; // cantidad de etapas posteriores al nodo 0
    /**
     * La ultima etapa debe tener un Ãºnico paso de tiempo
     */
     /** En cada elemento de etapasPasos estÃ¡n los datos
     *  de una etapa de pasos de tiempo, empezando de la etapa 0 que
     *  consta sÃ³lo del paso 0
     *  Cada etapa tiene;
     *  paso inicial de la etapa en [0]
     *  paso final de la etapa en [1]
     *  paso representativo de la etapa en [2]
     *  El elemento get(0) tiene todos ceros en el array[]
     *
     *  LA ULTIMA ETAPA DEBE TENER UN ÃšNICO PASO DE TIEMPO
     */
    private ArrayList<int[]> etapasPasos;
   /**
     * PeriodoDeTiempo de cada etapa del grafo. El get(0) corresponde a la etapa 0
     */
    private ArrayList<PeriodoDeTiempo> perTiempoDeEtapas;
    
    private double tasaDescPaso;
    private double factorDescPaso;
    private boolean imprime = true;
     /**
     * informaciÃ³n es un objeto que cuando se instancie GrafoE contendrÃ¡
     * toda la informaciÃ³n especÃ­fica del problema que se usarÃ¡ en los mÃ©todos
     * sobreescritos de las clases que hereden de
     * - Portafolio
     * - RestGrafoE
     * En este paquete el contenido de informaciÃ³n no es relevante
     */
    private ArrayList<Object> informacion;
   /**
     * Conjunto de restricciones significativas de parque
     */
    private ConjRestGrafoE conjRestSParque;
      /**
     * restricciones heurÃ­sticas de parque corrientes
     * las restricciones heurÃ­sticas se van relajando en las sucesivas
     * iteraciones
     */
    private ConjRestGrafoE conjRestHParque;
   /**
     * conjunto de restricciones totales de parque, uniÃ³n de los conjuntos
     * anteriores
     */
    private ConjRestGrafoE conjRestParque;
   /**
     * conjunto de restricciones de inicio de construcciÃ³n, que son las
     * que pueden evaluarse sin usar datos del parque en las etapas siguientes
     *
     */
    private ConjRestGrafoE conjRestInicio;
    /**
     * En nodosE el get(0) corresponde a la etapa 0
     * Puede haber mÃ¡s de un nodoE en la etapa 1, si hay mÃ¡s de un NodoN en
     * la etapa 1.
     */
    private ArrayList<HashMap<ClaveNodoE,NodoE>> nodosE;
    /**
     * Preserva el orden natural de generaciÃ³n de los nodos, mediante sus claves.
     */
    private ArrayList<ArrayList<ClaveNodoE>> clavesOrdenadas;
 
    
    
    private ArrayList<Objetivo> objetivos;
    private GrafoN grafoN;
    private BolsaCD bolsaCD;


    /**
     * Es la clave en el HashMap nodosE
     */
    private class ClaveNodoE{
        private Portafolio port;
        private NodoN nodoN;

        public ClaveNodoE(Portafolio port, NodoN nodoN) {
            this.port = port;
            this.nodoN = nodoN;
        }
                
        public NodoN getNodoN() {
            return nodoN;
        }

        public void setNodoN(NodoN nodoN) {
            this.nodoN = nodoN;
        }

        public Portafolio getPort() {
            return port;
        }

        public void setPort(Portafolio port) {
            this.port = port;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ClaveNodoE other = (ClaveNodoE) obj;
            if (this.port != other.port && (this.port == null || !this.port.equals(other.port))) {
                return false;
            }
            if (this.nodoN != other.nodoN && (this.nodoN == null || !this.nodoN.equals(other.nodoN))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 97 * hash + (this.port != null ? this.port.hashCode() : 0);
            hash = 97 * hash + (this.nodoN != null ? this.nodoN.hashCode() : 0);
            return hash;
        }
        
        
        
    }
    
    
    public GrafoE(int cantEtapas, double tasaDescPaso,
            GrafoN grafoN,
            ArrayList<int[]> etapasPasos,
            ArrayList<PeriodoDeTiempo> perTiempoDeEtapas,
            Portafolio pCero, Portafolio pIni,
            ConjRestGrafoE conjRestSParque, ConjRestGrafoE conjRestHParque,
            ConjRestGrafoE conjRestInicio,
            ArrayList<Object> informacion) {

//        this.est = est;
        this.cantEtapas = cantEtapas;
        this.grafoN = grafoN;
        this.conjRestSParque = conjRestSParque;
        this.conjRestHParque = conjRestHParque;
        this.conjRestInicio = conjRestInicio;
        this.tasaDescPaso = tasaDescPaso;
        this.informacion = informacion;
        this.etapasPasos = etapasPasos;
        this.perTiempoDeEtapas = perTiempoDeEtapas;
        conjRestParque = new ConjRestGrafoE();
        conjRestParque.getRestricciones().addAll(conjRestSParque.getRestricciones());
        conjRestParque.getRestricciones().addAll(conjRestHParque.getRestricciones());
        factorDescPaso = 1 / (1 + tasaDescPaso);
        bolsaCD = new BolsaCD(this);

        nodosE = new ArrayList<HashMap<ClaveNodoE,NodoE>>();        
        clavesOrdenadas = new ArrayList<ArrayList<ClaveNodoE>>();
        for (int i = 0; i <= cantEtapas; i++) {
            HashMap<ClaveNodoE,NodoE> auxH = new HashMap<ClaveNodoE,NodoE>();
            ArrayList<ClaveNodoE> auxC = new ArrayList<ClaveNodoE>();            
            nodosE.add(auxH);
            clavesOrdenadas.add(auxC);
        }
        // se construyen los nodos de etapas cero y uno y las demÃ¡s estructuras
        // para esos pasos
        creaNodosIniGE(pCero, pIni);
    }

    /**
     * grafoAnt es un grafo de estados preexistente.
     *
     * conjRestHParque es el nuevo conjunto de restricciones heurÃ­sticas
     * de parque, que debe ser menos restrictivo que el anterior.
     *
     * El nuevo grafo tendrÃ¡ en cada nodo preexistente un conjunto de decisiones
     * que incluye a las decisiones del nodo en el grafo preexistente.
     *
     * Aparecen nuevos cDs porque los parques factibles son mÃ¡s que antes
     * ya que relajan las restricciones heurÃ­sticas de parqeu.
     *
     * ATENCION: EL GRAFO PREEXISTENTE NO SE CONSERVA
     *
     * Los viejos conjuntos de decisiÃ³n no tienen nuevos nodosE, porque
     * corresponden a parejas (activos, NodoN).
     */
    public GrafoE(GrafoE grafoEAnt, ConjRestGrafoE conjRestHParque) {
//        OJO CON ESO DE QUE LOS VIEJOS CONJS DE DECISION NO TIENEN NUEVOS NODOSE

        this.informacion = grafoEAnt.getInformacion();
        this.cantEtapas = grafoEAnt.getCantEtapas();
        this.tasaDescPaso = grafoEAnt.getTasaDescPaso();
        this.nodosE = grafoEAnt.getNodosE();
        this.objetivos = grafoEAnt.getObjetivos();
        this.grafoN = grafoEAnt.getGrafoN();
        this.bolsaCD = grafoEAnt.getBolsaCD();
        this.conjRestInicio = grafoEAnt.conjRestInicio;
    }

    public int getCantEtapas() {
        return cantEtapas;
    }

    public void setCantEtapas(int cantEtapas) {
        this.cantEtapas = cantEtapas;
    }

    public ArrayList<PeriodoDeTiempo> getPerTiempoDeEtapas() {
        return perTiempoDeEtapas;
    }

    public void setPerTiempoDeEtapas(ArrayList<PeriodoDeTiempo> perTiempoDeEtapas) {
        this.perTiempoDeEtapas = perTiempoDeEtapas;
    }

    public PeriodoDeTiempo perTiempoDeUnaEtapa(int e) {
        return perTiempoDeEtapas.get(e);
    }

    public ConjRestGrafoE getConjRestInicio() {
        return conjRestInicio;
    }

    public void setConjRestInicio(ConjRestGrafoE conjRestInicio) {
        this.conjRestInicio = conjRestInicio;
    }

//    public Estudio getEst() {
//        return est;
//    }
//
//    public void setEst(Estudio est) {
//        this.est = est;
//    }

    public ConjRestGrafoE getConjRestParque() {
        return conjRestParque;
    }

    public void setConjRestParque(ConjRestGrafoE conjRestParque) {
        this.conjRestParque = conjRestParque;
    }

    public ConjRestGrafoE getConjRestSParque() {
        return conjRestSParque;
    }

    public void setConjRestSParque(ConjRestGrafoE conjRestSParque) {
        this.conjRestSParque = conjRestSParque;
    }

    public ArrayList<int[]> getEtapasPasos() {
        return etapasPasos;
    }

    public void setEtapasPasos(ArrayList<int[]> etapasPasos) {
        this.etapasPasos = etapasPasos;
    }

    public ConjRestGrafoE getConjRestHParque() {
        return conjRestHParque;
    }

    public void setConjRestHParque(ConjRestGrafoE conjRestHParque) {
        this.conjRestHParque = conjRestHParque;
    }

    public double getTasaDescPaso() {
        return tasaDescPaso;
    }

    public void setTasaDescPaso(double tasaDescPaso) {
        this.tasaDescPaso = tasaDescPaso;
    }

    public double getFactorDescPaso() {
        return factorDescPaso;
    }

    public void setFactorDescPaso(double factorDescPaso) {
        this.factorDescPaso = factorDescPaso;
    }

    public GrafoN getGrafoN() {
        return grafoN;
    }

    public void setGrafoN(GrafoN grafoN) {
        this.grafoN = grafoN;
    }

    public ArrayList<HashMap<ClaveNodoE,NodoE>> getNodosE() {
        return nodosE;
    }

    public void setNodosE(ArrayList<HashMap<ClaveNodoE,NodoE>> nodosE) {
        this.nodosE = nodosE;
    }

    public ArrayList<Object> getInformacion() {
        return informacion;
    }

    public void setInformacion(ArrayList<Object> informacion) {
        this.informacion = informacion;
    }

    public ArrayList<Objetivo> getObjetivos() {
        return objetivos;
    }

    public void setObjetivos(ArrayList<Objetivo> objetivos) {
        this.objetivos = objetivos;
    }

    public BolsaCD getBolsaCD() {
        return bolsaCD;
    }

    public void setBolsaCD(BolsaCD bolsaCD) {
        this.bolsaCD = bolsaCD;
    }


    public boolean imprime() {
        return imprime;
    }

    
    

    /***************************************************************
     *
     * COMIENZA UN CONJUNTO DE METODOS EMPLEADOS EN LA CONSTRUCCIÃ“N
     * DEL GRAFO
     *
     * *************************************************************
     */
    
    
    /**
     *
     * Es la iteraciÃ³n mÃ¡s general en las etapas, que avanza en las etapas
     * para construir el grafoE
     * @param dirImprimir es el directorio donde grabar el resultado de la construcciÃ³n del GrafoE
     * si es null no se imprime nada.
     * 
     * 
     */
    public void construyeGrafoE(ArrayList<Object> informacion, String dirImprimir) throws XcargaDatos, IOException {
        imprime = true;
        if(dirImprimir == null) imprime = false;
        String archDir = dirImprimir + "/ConstruyeGrafoE.txt";
        try{
            UtilitariosGenerales.DirectoriosYArchivos.eliminaArchivo(archDir);
        }catch(Exception e){
            System.out.println(e.toString());
        }
        for (int e = 1; e < cantEtapas; e++) {
            System.out.println("Comienza generaciÃ³n de sucesores de etapa " + e);
            creaSucesoresEtapa(e);
            if(imprime){
                String texto = "DESCRIPCIÃ“N DE NODOS CREADOS EN ETAPA " + e + "\r\n";
                UtilitariosGenerales.DirectoriosYArchivos.agregaTexto(archDir, texto);
                for (ClaveNodoE cl: clavesOrdenadas.get(e)) {
                    texto = (nodosE.get(e).get(cl)).toStringCorto(informacion, null) + "\r\n";
                    UtilitariosGenerales.DirectoriosYArchivos.agregaTexto(archDir, texto);
                }                
            }
//            eliminaCDSinSuc(e);
        }
        cargaConjDetPort();
        cargaResumenFinal(informacion);
    }

//    public void imprimeNodosEtapa(int e){
//        System.out.print("**********************************" + "\n");
//        System.out.print("EMPIEZA LA ETAPA " + e + "DEL GRAFO DE ESTADOS" + "\n");
//        System.out.print("**********************************" + "\n");
//        String texto = "\n";
//        for(NodoE nE: nodosE.get(e)){
//            texto += nE.toStringCorto() + "\n";
//        }
//        System.out.print(texto);
//    }
    /**
     * METODO creaNodosIniGE
     *
     * SE EJECUTA AUTOMÃ�TICAMENTE CON EL CONSTRUCTOR
     * Se carga el nodo de etapa cero del grafoE con el nodo de
     * etapa cero del grafo de la naturaleza
     * Se cargan los nodos de etapa uno del grafoE con el portafolio
     * inicial pIni y con cada estado de la naturaleza posible en 1
     * Se cargan los conjuntos de decisiÃ³n de la etapa 1
     *
     */
    public void creaNodosIniGE(Portafolio pCero, Portafolio pIni) {

        NodoE nodoCero = new NodoE(this, 0, pCero, grafoN.nodosEtapa(0).get(0));
        agregaUnNodoEnEtapa(0, pCero, grafoN.nodosEtapa(0).get(0), nodoCero);
        /**
         * Se crea un nodo inicial en el paso 1 con el nodoInicial del GrafoN
         */
        NodoE nIni = new NodoE(this, 1, pIni, grafoN.getNodoInicial());

        Portafolio d = new Portafolio();
        recibeNodoE(nIni, nodoCero, d, 1);
        /**
         * El pasaje del nodo de etapa cero a etapa 1 es irrelevante
         * y se toma decisiÃ³n d que es nula ya que la programaciÃ³n dinÃ¡mica se termina en
         * los nodos de etapa 1
         */
    }
    

    /**
     * METODO agregaUnNodoEnEtapa
     * Numera el ordinal del nodo, incrementa el contador de nodos
     * y agrega el nodo a nodosE, la estructura del grafo que los conserva.
     * Carga la clave para mantener el orden natural.
     */
    public void agregaUnNodoEnEtapa(int e, Portafolio port, NodoN nN, NodoE nE) {

        nE.setOrdinalNE(NodoE.getContadorNodos());
        NodoE.setContadorNodos(NodoE.getContadorNodos() + 1);
        System.out.println("CreÃ³ NodoE ordinal: " + nE.getOrdinalNE() + " - etapa " + e);
        ClaveNodoE claveNE = new ClaveNodoE(port, nN);
        nodosE.get(e).put(claveNE, nE);
        
        clavesOrdenadas.get(e).add(claveNE);
    }

    /**
     * METODO eliminaVinculosUnNodo
     * Elimina el nodoE nE que vive en la etapa e de nodosE
     * Elimina el nodo como sucesor de sus eventuales predecesores
     * Elimina el nodo como predecesor de sus eventuales sucesores
     * Elimina la clave de la lista ordenada de claves.
     */
    public void eliminaVinculosUnNodo(int e, NodoE nE) throws XcargaDatos {

        if (!(e == nE.getEtapa())) {
            throw new XcargaDatos("el nodo " + nE.toString() +
                    " que se quiere eliminar es incoherente con etapa " + e);
        }

        // Elimina el nodo como sucesor de predecesores
        for (DecConjNodos dcn : nE.getPredecesores()) {
            NodoE npred = dcn.getNodosE().get(0);
            Portafolio d = dcn.getDecision();
            npred.eliminaVinculoSucConUnaDec(d);
        }
        // Elimina el nodo como predecesor de sucesores
        for (DecConjNodos dcn : nE.getSucesores()) {
            for (NodoE nsuc : dcn.getNodosE()) {
                Portafolio d = dcn.getDecision();
                nsuc.eliminaUnPred(d);
            }
        }
        // Elimina el nodo de la estructura
        ClaveNodoE claveNE = new ClaveNodoE(nE.getPort(), nE.getNodoN());        
        nodosE.get(e).remove(claveNE);
        // Elimina la clave
        clavesOrdenadas.get(e).remove(claveNE);
    }

    /**
     * METODO creaSucesoresEtapa
     *
     * Crea los sucesores de la etapa e, que viven en la etapa e+1
     * Pueden preexistir sucesores en la etapa e+1 si hay un grafoE
     * preexistente.
     * Pueden preexistir decisiones en los cDs de la etapa e.
     * El valor de e debe ser mayor o igual que 1.
     * Los sucesores se agregan a nodosE por un mÃ©todo de la clase
     * ConjuntoPortafolios.
     *
     */
    public void creaSucesoresEtapa(int e) throws XcargaDatos {

        assert (e > 0) : "Se pide una creaciÃ³n de sucesores de etapa cero";
        ArrayList<Boolean> infoImp = new ArrayList<Boolean>();
        infoImp.add(true);
        infoImp.add(false);
        infoImp.add(false);
        infoImp.add(true);                
        for (ConjDeDecision cd: bolsaCD.cDsUnaEtapa(e)) {            
            ArrayList<Portafolio> nuevasDecisiones = new ArrayList<Portafolio>();
            Portafolio port = cd.getPort();
            //System.out.println("Inicia sucesores de cd ordinal:"+cd.getOrdinalCD());
            ArrayList<Portafolio> decDeUnPort = port.decisionesDeUnPort(cd, conjRestInicio,
                    conjRestParque, informacion);
            for (Portafolio d : decDeUnPort) {
                /**
                 * Se procesan solamente las decisiones nuevas, que no
                 * preexisten entre las decisiones del CD.
                 * El mÃ©todo de portafolio decisionesDeUnPort puede generar
                 * nuevas decisiones respecto a las preexistentes porque se han
                 * relajado las restricciones, lo que hace que cambie el objeto
                 * informaciÃ³n.
                 *
                 * ATENCIÃ“N-1: EN LA IMPLEMENTACIÃ“N ACTUAL, COMO LAS RESTRICCIONES
                 * DE INICIO DE CONSTRUCCIÃ“N SON TODAS SIGNIFICATIVAS, NO SE
                 * RELAJAN DE UNA ITERACIÃ“N A LA SIGUIENTE.
                 *
                 * ATENCION-2: PUEDEN SER DECISIONES QUE YA SE CONSIDERARON EN UNA
                 * ITERACIÃ“N ANTERIOR PERO FUERON DESCARTADAS PORQUE LOS SUCESORES
                 * NO CUMPLÃ�AN LAS RESTRICCIONES DE PARQUE.
                 */
                boolean agregar = true;
                if (!cd.contieneDecision(d)) {
                    //System.out.println("-Procesa decisiÃ³n"+d.toStringCorto(informacion, infoImp));
                    // La decisiÃ³n d es nueva
                    for (NodoN n : cd.getConjInfo().getNodos()) {
                        for (NodoN nn : n.getSucesores()) {
                            Portafolio portsig = port.sucesorDeUnPort(d, n, nn, informacion);
                            // Verifica si el nodo siguiente es factible
                            if (!portsig.esNodoEFactible(nn, informacion,
                                    conjRestParque)) {
                                agregar = false;
                                break;
                            }
                        }
                        if (agregar == false) {
                            break;
                        }
                    }
                }
                if (agregar == true) {
                    nuevasDecisiones.add(d);
                }
            }
            /**
             * En nuevasDesiciones estÃ¡n todas las decisiones nuevas
             * Con cada decisiÃ³n factible del conjunto de decisiÃ³n genera los
             * nodosE sucesores
             */
            if (nuevasDecisiones.isEmpty() && cd.tieneDecisiones() == false) {
                /**
                 * El CD no tenÃ­a decisiones y no aparecen nuevas decisiones
                 * El CD se agrega a una bolsa de cds a eliminar
                 */
                bolsaCD.agregaCDSinSuc(cd, e);
                // el CD se marca como sin sucesores
                cd.setSinSuc(true);
                
            } else {
                // Hay decisiones nuevas y nodos que crear
                for (Portafolio d : nuevasDecisiones) {
            
                    //System.out.println("-Procesa decisiÃ³n"+d.toStringCorto(informacion, infoImp));
                    cd.agregaDecision(d, informacion);
                    for (NodoE nE : cd.getNodos()) {
                        NodoN n = nE.getNodoN();
                        for (NodoN nn : n.getSucesores()) {
                            Portafolio portSuc;
                            portSuc = port.sucesorDeUnPort(d, n, nn, informacion);
                            assert (portSuc.esNodoEFactible(nn, informacion, conjRestParque)) : "Error:" +
                                    "el nodo debÃ­a ser factible y no lo es";
//                                   portSuc.cargaResumenP();
                            NodoE nuevoNE = new NodoE(this, e + 1, portSuc, nn);
                            recibeNodoE(nuevoNE, nE, d, e + 1);
                        }
                    }
                }
            }
        }
    }
    
    
	public void recibeNodoE(NodoE nodoE, NodoE nEAnt, Portafolio d, int t){
		/**
		 * Recibe un NodoE nodoE creado reciÃ©n, de la etapa t, para t>0
		 * El nodo es factible lo que se debiÃ³ verificar en el mÃ©todo que
		 * invoca a Ã©ste.
                 * 
		 * Crea los vÃ­nculos con el nodo antecesor nEant
		 * Si el portafolio y el nodo ya existen sÃ³lo crea los vÃ­nculos
		 *   
		 * ObsÃ©rvese que un cD sÃ³lo tiene un Ãºnico portafolio asociado
		 * Si el nodoE es nuevo lo agrega en nodosE del GrafoE
		 * ATENCION: EN ESTE MÃ‰TODO ES QUE SE CREAN LOS CD
		 *
		 */
		Portafolio port = nodoE.getPort();
		NodoN nodoN = nodoE.getNodoN();
		ConjNodosN cI = nodoN.getConjInfo();

		assert (t>0) : "Se llamÃ³ a recibe nodo para recibir el nodo de etapa cero";  
                NodoE nodoEExist = devuelveNodoE(t, port, nodoN);
                boolean existeNodoE = nodoEExist!=null;                                
                if(existeNodoE){
                    // Ya existe un NodoE igual a nodoE en el grafo, se crean los vÃ­nculos con nEAnt.
                    nEAnt.crearVinculoConSucesor(d,devuelveNodoE(t, port, nodoN));                    
                }else{
                    /**
                     * No existe un NodoE igual a nodoE en el grafo y debe agregarse
                     * y posiblemente crearse un nuevo CD.
                     */
                    ConjDeDecision cDExist = bolsaCD.devuelveCD(t, port, cI);
                    boolean existeCD = (cDExist!=null); 
                    if(existeCD){
                       /**
                        * El cDExist ya existe con otros NodoE anteriores, hay que agregar nodoE al cDExist
                        */
                        cDExist.agregaNodoE(nodoE);
                        nodoE.setCD(cDExist);
                        nEAnt.crearVinculoConSucesor(d, nodoE); 
			agregaUnNodoEnEtapa(t, port, nodoN, nodoE);                        
                    }else{
                        /**
                         * El CD no existe y debe crearse con el nuevo NodoE
                         */
                        ConjDeDecision cDNue = new ConjDeDecision(t, port);
			cDNue.setConjInfo(cI);
			cDNue.agregaNodoE(nodoE);
                        cDNue.setPort(port);
			bolsaCD.recibeUnCD(t,cDNue);
                        nodoE.setCD(cDNue);
			agregaUnNodoEnEtapa(t, port, nodoN, nodoE);
			nEAnt.crearVinculoConSucesor(d, nodoE);       
                    }                    
                }
            }
    
    
    

    /**
     * Elimina los CD sin sucesores de la etapa e y anteriores y todos los
     * nodos que como consecuencia se quedan sin predecesores
     *
     */
    public void eliminaCDSinSuc(int e) throws XcargaDatos {

        boolean avanzar;
        HashSet<NodoE> nodosAActUnaEtapa;
        int smin = 0;
        ArrayList<HashSet<NodoE>> nodosAActualizar = new ArrayList<HashSet<NodoE>>();
        for (int s = 0; s <= e; s++) {
            HashSet<NodoE> auxhs = new HashSet<NodoE>();
            nodosAActualizar.add(auxhs);
        }
        /**
         * Recorrido hacia atrÃ¡s: elimina CD sin sucesores y sus nodos
         * Almacena en nodosAActualizar los nodos que
         * han perdido predecesores como resultado de eliminar un CD y sus decisiones
         */
        int s;
        avanzar = false;
        for (s = e; s >= 1; s--) {
            nodosAActUnaEtapa = new HashSet<NodoE>();
            while (!bolsaCD.estaVaciaCDSinSucEtapa(s)) {
                if (s == 1) {
                    throw new XcargaDatos("No hay sucesores de algÃºn " +
                            "CD de etapa 1");
                }
                ConjDeDecision cd = bolsaCD.devuelveUnCDSinSucEtapa(s);

                boolean primerNodoEDelCD = true;
                for (NodoE nodoE : cd.getNodos()) {
                    /**
                     * Recorre lista de nodosE del cd que se elimina y viven en etapa s
                     */
                    for (DecConjNodos dCN1 : nodoE.getPredecesores()) {
                        /**
                         *  Recorre la lista de decisiones que llevan al nodoE
                         */
                        Portafolio d = dCN1.getDecision();
                        NodoE nEsmenos1 = dCN1.getNodosE().get(0);
                        // Hay un Ãºnico nodo nSmenos1 predecesor del nodoE con d
                        ConjDeDecision cdsmenos1 = nEsmenos1.getCD();

                        Set<NodoE> auxUnN = new HashSet<NodoE>();
                        try{
                            auxUnN = cdsmenos1.eliminaUnaDec(d);
                        }catch(XcargaDatos xcd){
                            if(primerNodoEDelCD) throw new XcargaDatos(xcd.toString());
                        }
                        primerNodoEDelCD = false;
                        auxUnN.remove(nodoE);
                        /**
                         * auxUnN tiene los nodos de etapa s que pierden
                         * un precesor al eliminar d, exceptuando el propio nodoE
                         */
                        if (!auxUnN.isEmpty()) {
                            avanzar = true;
                        }
                        for (NodoE nact : auxUnN) {
                            nact.eliminaUnPred(d);
                        }
                        nodosAActUnaEtapa.addAll(auxUnN);
                        if (!cdsmenos1.tieneDecisiones()) {
                            if (s == 2) {
                                throw new XcargaDatos("No hay sucesores de algÃºn " +
                                        "CD de etapa 1");
                            } else {
                                bolsaCD.agregaCDSinSuc(cdsmenos1, s - 1);
                            }
                        }
                    }

                }
                 for (NodoE nodoE : cd.getNodos()) {
                    eliminaVinculosUnNodo(s, nodoE);
                 }
                /**
                 * Elimina el CD sin sucesores
                 */
                bolsaCD.eliminaUnCDSinSucEtapa(s, cd);
                nodosAActUnaEtapa.removeAll(cd.getNodos());
                cd.getNodos().clear();
                bolsaCD.eliminaUnCDDelGrafo(s, cd);
                smin = s;
            }
            /**
             * smin tiene el valor de la Ãºltima etapa en la que se eliminaron
             * CDs sin sucesores
             *
             * El get(s) de nodosAActualizar se carga con los nodos a
             * actualizar de la etapa s
             **/
            nodosAActualizar.get(s).addAll(nodosAActUnaEtapa);
        }
        /**
         * Empieza a avanzar
         * Recorre hacia adelante revisando nodosAActualizar
         * y eliminando nodos y CDs que han quedado sin predecesores
         * Al eliminar un nodo o CD que ha quedado sin predecesores
         * no pueden aparecer CDs predecesores que queden sin sucesores
         */
        s = smin;
        while (s <= e & avanzar) {
            for (NodoE ne : nodosAActualizar.get(s)) {
                if (!ne.tienePredecesores()) {
                    /** ne no tiene predecesores
                     *  si ne vive en s menor que e
                     *  entonces sus sucesores son nuevos nodos a actualizar en s+1
                     *  que deben agregarse a nodosAActualizar
                     **/
                    if (s < e) {
                        for (DecConjNodos dcn : ne.getSucesores()) {
                            nodosAActualizar.get(s + 1).addAll(dcn.getNodosE());
                        }
                    }
                    // Elimina el nodo ne
                    eliminaVinculosUnNodo(s, ne);
                    ConjDeDecision cd = ne.getCD();
                    cd.getNodos().remove(ne);
                    // Si el CD del nodo eliminado queda vacÃ­o se elimina tambiÃ©n
                    if (cd.getNodos().isEmpty()) {
                        bolsaCD.eliminaUnCDDelGrafo(s, cd);
                    }
                } else {
                    /**
                     * el nodo ne a actualizar tiene predecesores, se da por
                     * verificado y se marca para ser eliminado de
                     * nodosAActualizar
                     */
                    nodosAActualizar.get(s).remove(ne);
                }
            }
            s++;
        }
    }

    /**
     * Una vez que el grafoE estÃ¡ construÃ­do crea los ConjDetPort
     * para ser usados al recorrer el grafoE y los carga en los ConjDeDecision.
     * Debe correrse al final de la construcciÃ³n de cada GrafoE.
     */
    public void cargaConjDetPort() throws XcargaDatos {
        for (int e = 1; e <= cantEtapas; e++) {
            for (ConjDeDecision cd : bolsaCD.cDsUnaEtapa(e)) {
                // cI es el conjunto de informaciÃ³n asociado a cd
                System.out.println("etapa " + e + " - pasa por cd " + cd.getOrdinalCD());
                for (NodoE nE : cd.getNodos()) {
                    boolean encontro = false;
                    NodoN nN = nE.getNodoN();
                    ConjNodosN cdPN = nN.getConjDetParque();
                    for (ConjDetPortE cdPE : cd.getConjsDetP()) {
                        if (cdPN == cdPE.getConjDPN()) {
                            // el conjunto determinante de parque en GrafoN
                            // del nodoE coincide con el de un cdPE que ya existe
                            nE.setCDPE(cdPE);
                            cdPE.getNodosE().add(nE);
                            encontro = true;
                        }
                        if (encontro) {
                            break;
                        }
                    }
                    if (!encontro) {
                        ConjDetPortE cdPE = new ConjDetPortE();
                        cdPE.setConjDPN(cdPN);
                        cdPE.setConjDec(cd);
                        nE.setCDPE(cdPE);
                        cdPE.getNodosE().add(nE);
                        cd.getConjsDetP().add(cdPE);

                    }
                }
            }
            System.out.println("TerminÃ³ de cargar ConjDetPort etapa " + e);
        }
    }






    /**
     * Calcula los costos de paso de todos los nodosE
     * y los costos de fin de juego de los nodos finales
     * a partir de los componentes de cada numerario
     * para el objetivo obj.
     * Los costos de paso estÃ¡n cargados en la mitad del perÃ­odo
     *
     */
    public void cargaCostosPaso(Objetivo obj, Object baseDeDatosResult) throws XcargaDatos {
        boolean etapafinal = false;
        for (int e = 1; e <= cantEtapas; e++) {
            if (e == cantEtapas) {
                etapafinal = true;
            }
            for (NodoE ne : nodosE.get(e).values()) {
                ne.cargaCostoPaso(obj, baseDeDatosResult, informacion);
                ne.setSimulado(true);
                if (etapafinal) {
                    ne.cargaCostoFinJuego(obj, baseDeDatosResult, informacion);
                }
            }
        }
    }

    
    
    
    /**
     * Carga en todos los nodos del grafoE los otrosResultadosPaso
     *
     */
    public void cargaOtrosResultados(Objetivo obj, Object baseDeDatosResult) throws XcargaDatos {
        for (int e = 1; e <= cantEtapas; e++) {
            for (NodoE ne : nodosE.get(e).values()) {
                ne.cargaOtrosResultados(obj, baseDeDatosResult, informacion);
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    /***************************************************************
     *
     * COMIENZA UN CONJUNTO DE METODOS EMPLEADOS PARA
     * RECORRER EL GRAFO
     *
     * *************************************************************
     */
    public NodoE nodoECero() {
        /**
         * Devuelve el nodo de etapa cero del grafo this
         */
        NodoE nInic = nodosE.get(0).get(0);
        return nInic;
    }

    /**
     * Halla el conjunto de decisiÃ³n cDecDestino al que se llega en
     * el grafo, si:
     *
     * - se parte del conjunto de decisiÃ³n cDecOrigen en t
     * - se emplea la decisiÃ³n Ã³ptima del cDecOrigen
     *
     * y los conjuntos que determinan parque en el grafoN son:
     *
     * conjDetPt en t
     * conjDetPtmas1 en t+1
     *
     * Se elige:
     * el primer nodoE de cDecOrigen,
     * el primer nodoN de conjDetPt,
     * el primer nodoN de conjDetPtmas1,
     * porque para encontrar el Conjunto de decisiÃ³n sucesor,
     * es indistinto cuÃ¡l de los nodosN y nodoE se elijan
     */
    public ConjDeDecision conjDecSucesorOpt(Objetivo obj, ConjDeDecision cDecOrigen,
            ConjNodosN conjDetPt, ConjNodosN conjDetPtmas1) throws XcargaDatos {

        ConjDeDecision cDecDestino = new ConjDeDecision();

        NodoE nE = cDecOrigen.getNodos().get(0);
        NodoN nNt = conjDetPt.getNodos().get(0);
        NodoN nNtmas1 = conjDetPtmas1.getNodos().get(0);
        Portafolio dOpt = cDecOrigen.decOptima(obj);
        NodoE nEtmas1 = nE.sucesorDecNat(dOpt, nNtmas1);
        cDecDestino = nEtmas1.getCD();
        return cDecDestino;
    }

    /**
     ***************************************************************
     ***************************************************************
     */
    /**
     * Genera la descripciÃ³n de las restricciones heurÃ­sticas
     * con la descripciÃ³n de violaciones
     */
    public String generaDescViolacionesRestricHeu(ArrayList<Object> informacion) throws XcargaDatos {
        String texto = "RESTRICCIONES HEURÃ�STICAS DEL GRAFO " + "\r\n";
        for (RestGrafoE rh : conjRestHParque.getRestricciones()) {
            texto += rh.resumenViolaciones(informacion);
        }
        return texto;
    }

    /**
     * Carga en todos los nodos del GrafoE ya construÃ­do, el resumen del portafolio
     * ordenado y el parqueOperativo
     *
     * @param informacion
     * @throws persistencia.XcargaDatos
     */
    public void cargaResumenFinal(ArrayList<Object> informacion) throws XcargaDatos {
        for (int ie = 1; ie <= cantEtapas; ie++) {
            for (NodoE nE : nodosE.get(ie).values()) {
                nE.getPort().cargaResumenFinal(informacion);
            }
        }
    }
    
    /**
     * Devuelve los NodoE de la etapa e, en el orden natural de su creaciÃ³n.
     * LOS NODOS NO PUEDEN SER ALTERADOS
     * @param e etapa
     */
    public ArrayList<NodoE> nodosEDeEtapa(int e){
        ArrayList<NodoE> nodosUnaEtapa = new ArrayList<NodoE>();
        for(ClaveNodoE cl: clavesOrdenadas.get(e)){
            nodosUnaEtapa.add(nodosE.get(e).get(cl));            
        }
        return nodosUnaEtapa;
    }
    

    /**
     * Devuelve el NodoE de portafolio port y conjunto de informaciÃ³n cI de la etapa e
     * si este existe, y de lo contrario devuelve null.
     * @param e etapa
     * @param port 
     * @param cI conjunto de informaciÃ³n.
     */
    public NodoE devuelveNodoE(int e, Portafolio port, NodoN nN){
        HashMap<ClaveNodoE, NodoE> mapa;
        mapa = nodosE.get(e);
        ClaveNodoE claveNE = new ClaveNodoE(port, nN);
        return mapa.get(claveNE);        
    }
    


    public void escribeGrafoE(ArrayList<Object> informacion, Objetivo obj, String archivo) throws XcargaDatos {
        String texto = "\r\n" + "******************************************" +
                "\r\n" + "COMIENZA GRAFO DE ESTADOS" +
                "\r\n" + "******************************************" + "\r\n";
        if(DirectoriosYArchivos.existeArchivo(archivo)) 
            UtilitariosGenerales.DirectoriosYArchivos.eliminaArchivo(archivo);
        UtilitariosGenerales.DirectoriosYArchivos.agregaTexto(archivo, texto);
        for (int e = 1; e <= cantEtapas; e++) {
            texto = "-----------------------------------------------------" + "\r\n" +
                     "COMIENZA ETAPA " + e + "\r\n" +
                     "-----------------------------------------------------" + "\r\n";
            UtilitariosGenerales.DirectoriosYArchivos.agregaTexto(archivo, texto);          
            for (ClaveNodoE cl: clavesOrdenadas.get(e)) {
                texto = (nodosE.get(e).get(cl)).toStringCorto(informacion, obj) + "\r\n";
                texto += "----------------------------------" + "\r\n";
                UtilitariosGenerales.DirectoriosYArchivos.agregaTexto(archivo, texto);
            }
        }
        texto += "********************************************************" + "\r\n";
        texto += "FIN DE GRAFO DE ESTADOS" + "\r\n";
        texto += "********************************************************" + "\r\n";
    }

}


