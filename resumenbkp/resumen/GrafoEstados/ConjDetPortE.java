/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GrafoEstados;

import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.ConjNodosN;
import naturaleza3.NodoN;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class ConjDetPortE implements Serializable{
    /**
     * Conjunto determinante de parque de nodosE (CDPE)
     * Conserva el conjunto de nodosE que tienen el mismo portafolio y cuyos
     * nodosN asociados, además de pertenecer al mismo conjunto de información
     * en el grafoN, tienen los mismos estados (nodos) de las variablesN que determinan
     * parque: cambio aleatorio, inversión aleatoria, demora aleatoria.
     *
     * Los nodos de un ConjDetPortE pertenecen al mismo conjunto de decisión.
     * Si no hay componentes en GrafoN que determinen parque y que no sean observadas
     * los ConjDetPorE coinciden con los ConjDeDecision.
     *
     * Si dos nodosE pertenecen al mismo ConjDetPortE en t, y tanto en t como en t+1 se da cierto
     * estado de la naturaleza, ambos nodosE tendrán el mismo nodoE sucesor, en particular
     * el mismo portafolio sucesor.
     */
    private ConjDeDecision conjDec; // conjunto de decisión al que pertenece este conjDetPortE
    private Portafolio port;  // portafolio asociado (ya está en el Conjunto de Decisión ConjDec)
    private ArrayList<NodoE> nodosE; // nodosE que pertenecen a this
    private ConjNodosN conjDPN; // conjunto determinante de parque en GrafoN asociado a this
    private int ordinal;
    private static int indiceOrdinal = 0;

    public ConjDetPortE (){
        nodosE = new ArrayList<NodoE>();
        ordinal = indiceOrdinal + 1;
        indiceOrdinal ++;

    }


    public ConjDeDecision getConjDec() {
        return conjDec;
    }

    public void setConjDec(ConjDeDecision conjDec) {
        this.conjDec = conjDec;
    }

    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }



    public ArrayList<NodoE> getNodosE() {
        return nodosE;
    }

    public void setNodosE(ArrayList<NodoE> nodosE) {
        this.nodosE = nodosE;
    }

    public Portafolio getPort() {
        return port;
    }

    public void setPort(Portafolio port) {
        this.port = port;
    }

    public ConjNodosN getConjDPN() {
        return conjDPN;
    }

    public void setConjDPN(ConjNodosN conjDPN) {
        this.conjDPN = conjDPN;
    }

    

    /**
     * Devuelve el ConjDetPortE al que pertenecen todos los nodosE sucesores
     * de los nodosE del ConjDetPortE this, en la optimización con el objetivo obj,
     * cuando en t la naturaleza está en cdpNt y en t+1 está en cdpNtmas1.
     *
     * Todos los nodosE de this tienen el mismo portafolio
     * Todos los nodosE sucesores de los nodosE de this tienen el mismo portafolio
     */
    public ConjDetPortE cDPortSucesor(ConjNodosN cdpNt,
            ConjNodosN cdpNtmas1, Objetivo obj) throws XcargaDatos{

        ConjDetPortE cdpEtmas1 = null;
        Portafolio dOp = conjDec.decOptima(obj);
        /**
         * Basta con elegir un nodoE cualquiera del ConjDetPortE this, un
         * nodoN de cdpNtmas1 y encontrar el nodoE
         * sucesor nodoEtmas1.
         * El ConjDetPortE sucesor será el que contenga a nodoEtmas1.
         */
        NodoE nodoEt = this.getNodosE().get(0);
        NodoN nodoNtmas1 = cdpNtmas1.getNodos().get(0);
        NodoE nodoEtmas1 = nodoEt.sucesorDecNat(dOp, nodoNtmas1);
        cdpEtmas1 = nodoEtmas1.getCDPE();
        return cdpEtmas1;
    }

    /**
     * Devuelve el ConjDetPortE al que pertenecen todos los nodosE sucesores
     * de los nodosE del ConjDetPortE this, cuando la decisión de inversión es dOp,
     * cuando en t la naturaleza está en cdpNt y en t+1 está en cdpNtmas1.
     *
     * Todos los nodosE de this tienen el mismo portafolio
     * Todos los nodosE sucesores de los nodosE de this tienen el mismo portafolio
     */
    public ConjDetPortE cDPortSucesor(ConjNodosN cdpNt,
            ConjNodosN cdpNtmas1, Portafolio dOp) throws XcargaDatos{

        ConjDetPortE cdpEtmas1 = null;
        /**
         * Basta con elegir un nodoE cualquiera del ConjDetPortE this, un
         * nodoN de cdpNtmas1 y encontrar el nodoE
         * sucesor nodoEtmas1.
         * El ConjDetPortE sucesor será el que contenga a nodoEtmas1.
         */
        NodoE nodoEt = this.getNodosE().get(0);
        NodoN nodoNtmas1 = cdpNtmas1.getNodos().get(0);
        NodoE nodoEtmas1 = nodoEt.sucesorDecNat(dOp, nodoNtmas1);
        cdpEtmas1 = nodoEtmas1.getCDPE();
        return cdpEtmas1;
    }


    @Override
    public String toString() {
        String texto = "COMIENZA CDPE - conjunto de nodosE que determinan parque " + "\n";
        texto += "Ordinal del ConjDetPortE = "  + ordinal;
        texto += "Conjunto de decisión al que pertenece - ordinal " + conjDec.getOrdinalCD()+ "\n";
        texto += "NodosE del CDPE - Lista de ordinales de NodosE: ";
        for(NodoE nE: nodosE){
            texto += nE.getOrdinalNE() + "\t";
        }
    return texto;
    }







}
