/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GrafoEstados;

/**
 *
 * @author ut469262
 *
 * Agrupa una RestGrafoE y un Portafolio
 *
 * Por ejemplo una RestGrafoE heurística que ocasiona que un portafolio p no
 * sea interior y el Portafolio contiguo a p que no cumple la restricción
 *
 */
public class ParRestPort {
    RestGrafoE rest;
    Portafolio port;

    public ParRestPort(RestGrafoE rest, Portafolio port) {
        this.rest = rest;
        this.port = port;
    }

    public Portafolio getPort() {
        return port;
    }

    public void setPort(Portafolio port) {
        this.port = port;
    }

    public RestGrafoE getRest() {
        return rest;
    }

    public void setRest(RestGrafoE rest) {
        this.rest = rest;
    }
    
}
