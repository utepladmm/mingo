/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import UtilitariosGenerales.CalculadorDePlazos;
import dominio.Estudio;
import dominio.TiempoAbsoluto;
import java.io.Serializable;
import java.util.ArrayList;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 * Define el aporte que una simulación en un período de tiempo
 * hace al costo del paso respectivo.
 */
public class Objetivo implements Serializable {

    protected String nombre;
//     protected Estudio est;
    protected TiempoAbsoluto tAbsolutoBaseObjetivo;
    /**
     * El TiempoAbsoluto a partir del cual se cuentan los pasos de tiempo
     * de paramDepTiempo.
     */
    protected ArrayList<ArrayList<Double>> paramDepTiempo;

    /**
     * Son los que sirven para evaluar el objetivo y dependen
     * del PeriodoDeTiempo. Por ejemplo, una función que emplea el objetivo tiene un parámetro
     * y ese parámetro cambia según el PeriodoDeTiempo.
     * El primer get indica el índice de parámetro y el segundo el índice de PeriodoDeTiempo
     * comenzando con el PeriodoDeTiempo tAbsolutoBaseObjetivo.
     * Debe existir un valor por PeriodoDeTiempo para cada parámetro de escala.
     * Según la clase de la función fundModif, la cantidad de parámetros será distinta.
     */
    public Objetivo() {
        tAbsolutoBaseObjetivo = new TiempoAbsoluto();
        paramDepTiempo = new ArrayList<ArrayList<Double>>();
    }

    public ArrayList<ArrayList<Double>> getParamDepTiempo() {
        return paramDepTiempo;
    }

    public void setParamDepTiempo(ArrayList<ArrayList<Double>> paramDepTiempo) {
        this.paramDepTiempo = paramDepTiempo;
    }

//     public Estudio getEst() {
//          return est;
//     }
//
//     public void setEst(Estudio est) {
//          this.est = est;
//     }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TiempoAbsoluto gettAbsolutoBaseObjetivo() {
        return tAbsolutoBaseObjetivo;
    }

    public void settAbsolutoBaseObjetivo(TiempoAbsoluto tAbsolutoBaseObjetivo) {
        this.tAbsolutoBaseObjetivo = tAbsolutoBaseObjetivo;
    }

    /**
     * ********************************************************
     * EMPIEZAN METODOS A SER SOBRE ESCRITOS
     * ********************************************************
     */
//     /**
//      * Devuelve los parámetros de escala del PeriodoDeTiempo per
//      */
//     public ArrayList<Double> paramDepTiempoDePer(PeriodoDeTiempo per) throws XcargaDatos {
//          TiempoAbsoluto ta = (TiempoAbsoluto) per;
//          CalculadorDePlazos calcP = est.getDatosGenerales().getCalculadorDePlazos();
//          int avance = calcP.hallaAvanceTiempo(tAbsolutoBaseObjetivo, ta);
//          if (avance < 0) {
//               throw new XcargaDatos("Se pidió el valor de los parámetros del objetivo " +
//                       this.toString() + " en un paso de tiempo = " + ta.toStringCorto() +
//                       " anterior al tAbsolutoBaseObjetivo");
//          }
//
//          ArrayList<Double> result = new ArrayList<Double>();
//          for (int ip = 0; ip < paramDepTiempo.size(); ip++) {
//               if(avance>=paramDepTiempo.get(0).size()){
//                       throw new XcargaDatos("Se pidió el valor de los parámetros del objetivo " +
//                       this.toString() + " en un paso de tiempo = " + ta.toStringCorto() +
//                       " posterior al último PasoDeTiempo con datos");
//
//               }
//               result.add(paramDepTiempo.get(ip).get(avance));
//          }
//          return result;
//     }
    /**
     * Evalúa el objetivo dando un real
     * @return
     */
    public double evaluar(Object resultado, ArrayList<Object> informacion) throws XcargaDatos {
        return 0.0;
    }

    public ArrayList<Double> percentiles(ArrayList<Object> objsResSimul,
            ArrayList<Double> probabilidades,
            ArrayList<Double> vPercentiles,
            ArrayList<Object> informacion) throws XcargaDatos {
        ArrayList<Double> result = null;
        return result;
    }

    /**
     * ********************************************************
     * TERMINAN METODOS A SER SOBRE ESCRITOS
     * ********************************************************
     */
    /**
     * Son los tipos posibles de objetivo.
     */
    public enum TiposObj {

        GEN_CRON;   //
    }
}
