/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package GrafoEstados;

import java.io.Serializable;
import java.util.Calendar;

/**
 *
 * @author ut469262
 */
public class PeriodoDeTiempo implements Serializable{
    /**
     * Clase más general que define períodos de tiempo
     */
    protected Calendar inicio;
    protected Calendar fin;

    public PeriodoDeTiempo(Calendar inicio, Calendar fin) {
        this.inicio = inicio;
        this.fin = fin;
    }

//    public PeriodoDeTiempo(TiempoAbsoluto ta, DatosGeneralesEstudio datGen){
//        // ATENCION ESTÁN MAL LAS FECHAS
//        inicio = Calendar.getInstance();
//        fin = Calendar.getInstance();
//        inicio = FCalendar.fechaIniDeTa(ta, datGen);
//        fin = FCalendar.fechaIniDeTa(ta, datGen);
//
//        kk está todo mal los constructores de tiempo absoluto y PeriodoDeTiempo
//    }

    public PeriodoDeTiempo() {
    }


    public Calendar getFin() {
        return fin;
    }

    public void setFin(Calendar fin) {
        this.fin = fin;
    }

    public Calendar getInicio() {
        return inicio;
    }

    public void setInicio(Calendar inicio) {
        this.inicio = inicio;
    }

    /**
     * Crea una copia que puede modificarse sin alterar el original
     */
    public PeriodoDeTiempo copiaPerDeTiempo(){
        PeriodoDeTiempo copia = new PeriodoDeTiempo((Calendar)inicio.clone(),
                                                    (Calendar)fin.clone());
        return copia;
    }


    public String toStringCorto() {
        return "";
    }




}
