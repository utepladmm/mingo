                                                                                                                                    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GrafoEstados;

import dominio.ConjDemandas;
import dominio.Estudio;
import java.io.Serializable;
import java.util.ArrayList;
import naturaleza3.NodoN;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 */
public class NodoE implements Serializable, Comparable {

    /**
     * Grafo al que pertenece el nodo
     */
    private GrafoE grafoE; 
    /**
     * Los nodos se corresponden con los pasos representativos de las
     * etapas
     */
    private int etapa;
    /**
     * ordinal del nodo en el grafo
     */
    private int ordinalNE; 
     /**
     * simulado = false indica que no se dispone aÃºn de una simulaciÃ³n del
     * un programa, necesaria para calcular el costoPaso
     *          = true indica que ya existe la simulaciÃ³n
     * Este miembro lo usarÃ¡ la clase Caso en la implementaciÃ³n particular
     */
    private boolean simulado;
   
    private ConjDeDecision cD;
    private ConjDetPortE cDPE;
    /**
     * Conjunto de decisiÃ³n y conjunto determinante de parque a los que pertenece el nodoE this.
     */
    private static int contadorNodos = 0;
    private Portafolio port;
    /**
     * El texto resumen del Portafolio
     */
    private String resumenP;   
    private NodoN nodoN;
    /**
     * El DecConjNodos tiene una decisiÃ³n y el Ãºnico nodo predecesor con esa
     * decisiÃ³n.
     * ATENCIÃ“N: dos decisiones distintas de dos CD distintos pueden ser
     * iguales en su contenido
     */
    private ArrayList<DecConjNodos> predecesores;
    /**
     * El DecConjNodos tiene una decisiÃ³n y puede tener mÃ¡s de un nodo sucesor
     * con esa decisiÃ³n, segÃºn los nodoE de la etapa siguiente.
     */
    private ArrayList<DecConjNodos> sucesores;
    
    /**
     * Costo del paso representativo
     * QUE SE SUPONE CARGADO EN EL INSTANTE MEDIO DEL PASO DE TIEMPO
     */
    private double costoPaso;
    
//	/**
//	 * Costos actualizados al inicio del paso representativo
//	 * de los costos de paso desde el paso representativo de la etapa (e)
//	 * al paso anterior al representativo de la etapa siguiente (e+1),
//	 * para cada objetivo
//	 */
//	private ArrayList<ObjValor> costoEtapa;
    
    /**
     * Valor de Bellman al inicio del paso representativo de la etapa en el nodo,
     * para cada objetivo ya calculado, cuando se toma la decisiÃ³n Ã³ptima en la etapa.
     */
    private double valorVBIniPaso;
    /**
     * Valor de fin de juego en el nodoE
     * ATENCION: sÃ³lo aplicable para los nodos de la Ãºltima etapa
     */
    private double valorFinJuego;
    
    private Portafolio decOptima;
    /**
     * Es un String que sirve para identificar de dÃ³nde se obtuvieron los datos
     * de simulaciÃ³n que permiten obtener el costo del nodo.
     * En la implementaciÃ³n inicial es un nombre de directorio de corrida.
     */
    private String nombreSimulacion;
    
    private final static ArrayList<String> nombreOtrosResultadosPaso;
    private ArrayList<Double> otrosResultadosPaso;



//    public enum Simulado {
//
//        NO, // No se dispone de la simulaciÃ³n para el nodo ni se ha iniciado su proceso
//        SI, // Se disponde la simulaciÃ³n para el nodo
//        EN_PROCESO;     // La simulaciÃ³n estÃ¡ en proceso
//    }

    static{
        nombreOtrosResultadosPaso = new ArrayList<String>();
        nombreOtrosResultadosPaso.add("Costo medio simulacion");
        nombreOtrosResultadosPaso.add("Inversión");
        nombreOtrosResultadosPaso.add("Costos fijos");
    }

    public NodoE(GrafoE grafoE, int etapa, Portafolio port, NodoN nodoN) {
        this.grafoE = grafoE;
        this.etapa = etapa;
        this.port = port;
        this.nodoN = nodoN;
        simulado = false;
        predecesores = new ArrayList<DecConjNodos>();
        sucesores = new ArrayList<DecConjNodos>();
        otrosResultadosPaso = new ArrayList<Double>() ;

    }

    public NodoE(GrafoE grafoE, int etapa) {
        this.grafoE = grafoE;
        this.etapa = etapa;
        simulado = false;
        predecesores = new ArrayList<DecConjNodos>();
        sucesores = new ArrayList<DecConjNodos>();
        otrosResultadosPaso = new ArrayList<Double>() ;
    }


    public GrafoE getGrafo() {
        return grafoE;
    }

    public void setGrafo(GrafoE grafoE) {
        this.grafoE = grafoE;
    }

    public GrafoE getGrafoE() {
        return grafoE;
    }

    public void setGrafoE(GrafoE grafoE) {
        this.grafoE = grafoE;
    }

    public ConjDetPortE getCDPE() {
        return cDPE;
    }

    public void setCDPE(ConjDetPortE cDPE) {
        this.cDPE = cDPE;
    }

    public double getCostoPaso() {
        return costoPaso;
    }

    public void setCostoPaso(double costoPaso) {
        this.costoPaso = costoPaso;
    }

    public Portafolio getDecOptima() {
        return decOptima;
    }

    public void setDecOptima(Portafolio decOptima) {
        this.decOptima = decOptima;
    }

    public double getValorFinJuego() {
        return valorFinJuego;
    }

    public void setValorFinJuego(double valorFinJuego) {
        this.valorFinJuego = valorFinJuego;
    }

    public boolean getSimulado() {
        return simulado;
    }

    public void setSimulado(boolean simulado) {
        this.simulado = simulado;
    }

    public static int getContadorNodos() {
        return contadorNodos;
    }

    public static void setContadorNodos(int contadorNodos) {
        NodoE.contadorNodos = contadorNodos;
    }

    public int getOrdinalNE() {
        return ordinalNE;
    }

    public void setOrdinalNE(int ordinalNE) {
        this.ordinalNE = ordinalNE;
    }

    public ArrayList<DecConjNodos> getSucesores() {
        return sucesores;
    }

    public void setSucesores(ArrayList<DecConjNodos> sucesores) {
        this.sucesores = sucesores;
    }

    public ConjDeDecision getCD() {
        return cD;
    }

    public void setCD(ConjDeDecision cD) {
        this.cD = cD;
    }

//	public ArrayList<ValorNum> getComponentesPaso() {
//		return componentesPaso;
//	}
//
//	public void setComponentesPaso(ArrayList<ValorNum> componentesPaso) {
//		this.componentesPaso = componentesPaso;
//	}
    public int getEtapa() {
        return etapa;
    }

    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }

    public NodoN getNodoN() {
        return nodoN;
    }

    public void setNodoN(NodoN nodoN) {
        this.nodoN = nodoN;
    }

    public Portafolio getPort() {
        return port;
    }

    public void setPort(Portafolio port) {
        this.port = port;
    }

    public ArrayList<DecConjNodos> getPredecesores() {
        return predecesores;
    }

    public void setPredecesores(ArrayList<DecConjNodos> predecesores) {
        this.predecesores = predecesores;
    }

    public double getValorVBFinJuego() {
        return valorFinJuego;
    }

    public void setValorVBFinJuego(double valorVBFinJuego) {
        this.valorFinJuego = valorVBFinJuego;
    }

    public double getValorVBIniPaso() {
        return valorVBIniPaso;
    }

    public void setValorVBIniPaso(double valorVBIniPaso) {
        this.valorVBIniPaso = valorVBIniPaso;
    }

    public String getResumenP() {
        return resumenP;
    }

    public void setResumenP(String resumenP) {
        this.resumenP = resumenP;
    }


    public ConjDetPortE getcDPE() {
        return cDPE;
    }

    public void setcDPE(ConjDetPortE cDPE) {
        this.cDPE = cDPE;
    }

    public String getNombreSimulacion() {
        return nombreSimulacion;
    }

    public void setNombreSimulacion(String nombreSimulacion) {
        this.nombreSimulacion = nombreSimulacion;
    }

    public ArrayList<Double> getOtrosResultadosPaso() {
        return otrosResultadosPaso;
    }

    public void setOtrosResultadosPaso(ArrayList<Double> otrosResultadosPaso) {
        this.otrosResultadosPaso = otrosResultadosPaso;
    }

    public static ArrayList<String> getNombreOtrosResultadosPaso() {
        return nombreOtrosResultadosPaso;
    }

//    public static void setNombreOtrosResultadosPaso(ArrayList<String> nombreOtrosResultadosPaso) {
//        NodoE.nombreOtrosResultadosPaso = nombreOtrosResultadosPaso;
//    }


    /**
     * Calcula y carga costo del paso para un objetivo
     */
    public void cargaCostoPaso(Objetivo obj, Object baseDeDatosResult, ArrayList<Object> informacion) throws XcargaDatos {
        double costo = port.calculaCostoPaso(obj, baseDeDatosResult, this, informacion);
//        nombreSimulacion = port.getNombreSimul();        
        costoPaso = costo;
    }
    
    
    /**
     * Carga otrosResultadosPaso del NodoE
     */
    public void cargaOtrosResultados(Objetivo obj, Object baseDeDatosResult, ArrayList<Object> informacion) throws XcargaDatos {
        port.cargaOtrosResultados(obj, baseDeDatosResult, this, informacion);    
    }

    /**
     * Calcula y carga el costo de fin de juego para un objetivo
     * SÃ³lo aplicable a los nodos de la etapa final cantEtapas.OJOJOJO O DEL PASO FINAL?
     */
    public void cargaCostoFinJuego(Objetivo obj, Object baseDeDatosResult, ArrayList<Object> informacion) throws XcargaDatos {
        double costo = port.calculaCostoFinJuego(obj, baseDeDatosResult, this, informacion);
        valorFinJuego = costo;
    }

    
    /**
     * Agrega a otrosResultadosPaso los valores actualizados de inversión y costos fijos
     * cuando tipoCargosFijos = TOTALPASODECISION
     */
    public void agregaInvYCFijosActualizados(ArrayList<Double> al){
    	this.getOtrosResultadosPaso().addAll(al);
    }
    

    /**
     * Devuelve el ResSimul del nodo a partir de las simulaciones almacenadas
     * en el Object baseDeDatosResult
     *
     * @param obj es el objetivo para el que se estÃ¡ calculando resultados
     * @param baseDeDatosResult es el objeto donde se tienen almacenados los resultados
     * de las simulaciones
     * @param informacion
     * @return
     */
    public ResSimul devuelveResSimulDelNodo(Objetivo obj, Object baseDeDatosResult,
            ArrayList<Object> informacion) throws XcargaDatos{

        ResSimul rS = port.devuelveResSimul(nodoN, baseDeDatosResult, informacion);
        return rS;
    }

//    /**
//     * *********************************************************************
//     * MÃ©todos que borran, calculan y devuelven el costo de un paso para
//     * un objetivo en particular a partir de los componentes en cada numerario
//     */
//    public void borraUnObjCostoPaso(Objetivo obj) {
//        /**
//         * elimina el ObjValor del objetivo obj en el costo del paso
//         * del nodoE this entre los costos de paso ya calculados, si ya existe
//         */
//        for (ObjValor ov : costoPaso) {
//            if (ov.getObj() == obj) {
//                costoPaso.remove(ov);
//            }
//        }
//    }

//	public void calcCostoPasoObj(Objetivo obj){
//		/**
//		 * Calcula el costo escalar de un objetivo en el paso a partir
//		 * de las componentes de todos los numerarios en el paso
//		 */
//		 borraUnObjCostoPaso(obj);
//		 double costo = obj.valorObj(componentesPaso);
//		 ObjValor ov = new ObjValor(obj, costo);
//		 costoPaso.add(ov);
//	}
//    /**
//     * Devuelve el costo escalar de un objetivo en el paso
//     */
//    public double costoPasoObj(Objetivo obj) {
//        double result = 0.0;
//        boolean encontro = false;
//        for (ObjValor ov : costoPaso) {
//            if (ov.getObj() == obj) {
//                result = ov.getValor();
//                encontro = true;
//                break;
//            }
//        }
//        return result;
//    }

    /**
     * *********************************************************************
     * MÃ©todos que calculan y devuelven el valor de fin de juego para un objetivo
     * en particular a partir de los componentes en cada numerario
     */
//	public void calcValorFinJuego(Objetivo obj){
//		/**
//		 * Calcula el costo escalar de fin de juego
//		 * para el objetivo obj
//		 * ATENCIÃ“N: SÃ“LO APLICABLE A LOS NODOS DE LA ÃšLTIMA ETAPA
//		 */
//		 borraUnObjVBIni(obj);
//		 double costo = obj.valorObj(componentesFinJuego);
//		 ObjValor ov = new ObjValor(obj, costo);
//		 valorFinJuego.add(ov);
//	}
    /**
     * Devuelve el escalar valor de fin de juego
     * para el objetivo obj.
     * Es el valor de Bellman al fin de la Ãºltima etapa.
     * ATENCIÃ“N: SÃ“LO APLICABLE A LOS NODOS DE LA ÃšLTIMA ETAPA
     */
//    public double valorFinJuegoObj(Objetivo obj) {
//        double result = 0.0;
//        boolean encontro = false;
//        for (ObjValor ov : valorFinJuego) {
//            if (ov.getObj() == obj) {
//                result = ov.getValor();
//                encontro = true;
//                break;
//            }
//        }
//        return result;
//    }

//     /**
//      * Devuelve el escalar costo del paso para
//      * para el objetivo obj. Es el costo que aporta este paso
//      */
//     public double costoPaso(Objetivo obj) {
//          double result = 0.0;
//          boolean encontro = false;
//          for (ObjValor ov : costoPaso) {
//               if (ov.getObj() == obj) {
//                    result = ov.getValor();
//                    encontro = true;
//                    break;
//               }
//          }
//          return result;
//
//
//
//
//     }
//    /**
//     * *********************************************************************
//     * MÃ©todos que borran, cargan y devuelven el Valor de Bellman al inicio
//     * del nodo para un objetivo en particular
//     */
//    public void borraUnObjVBIni(Objetivo obj) {
//        /**
//         * elimina el ObjValor del objetivo obj en el Valor de Bellman
//         * al inicio del nodo en el nodoE this
//         * entre los valores de Bellman ya calculados, si ya existe
//         */
//        for (ObjValor ov : valorVBIniPaso) {
//            if (ov.getObj() == obj) {
//                valorVBIniPaso.remove(ov);
//            }
//        }
//    }

//    public void cargaUnObjVBIni(Objetivo obj, double valor) {
//        /**
//         * Borra el valor viejo y carga un valor nuevo como
//         * Valor de Bellman de un objetivo
//         */
//        borraUnObjVBIni(obj);
//        ObjValor ov = new ObjValor(obj, valor);
//        valorVBIniPaso.add(ov);
//    }

//    public double valorVBIniPasoObj(Objetivo obj) {
//        /**
//         * Devuelve el escalar valor de Bellman para el objetivo obj
//         * al comienzo del paso de tiempo del nodo, que es el nodo
//         * representativo de una etapa
//         */
//        double result = 0.0;
//        boolean encontro = false;
//        for (ObjValor ov : valorVBIniPaso) {
//            if (ov.getObj() == obj) {
//                result = ov.getValor();
//                encontro = true;
//                break;
//            }
//        }
//        return result;
//    }

// *************************************************************
// METODOS PARA RECORRER EL GRAFO
// *************************************************************
    public ArrayList<NodoE> sucesoresConUnaDec(Portafolio d) throws XcargaDatos {
        /**
         * Devuelve los NodosE que suceden al corriente (this)
         * con la decisiÃ³n d
         */
        ArrayList<NodoE> result = new ArrayList<NodoE>();
        result = null;
        for (DecConjNodos dcn : sucesores) {
            if (dcn.getDecision() == d) {
                result = dcn.getNodosE();
                break;
            }

        }
        if (result == null) {
            throw new XcargaDatos("Se buscÃ³ en grafoE" +
                    "los sucesores de una decisiÃ³n que no existia para suceder a un nodoE");
        }
        return result;
    }

    public NodoE sucesorDecNat(Portafolio dt, NodoN ntmas1) throws XcargaDatos {
        /**
         * Devuelve el NodoE que sucede al corriente (this)
         * que vive en t, con la decisiÃ³n dt tomada en t , si en t+1
         * el estado de la naturaleza estÃ¡ dado por el nodo ntmas1
         */
        NodoE nodoSuc = null;
        ArrayList<NodoE> result = new ArrayList<NodoE>();
        for (DecConjNodos dcn : sucesores) {
            if (dcn.getDecision() == dt) {
                result = dcn.getNodosE();
                for (NodoE nEtmas1 : result) {
                    if (nEtmas1.getNodoN() == ntmas1) {
                        return nEtmas1;  // debe salir siempre por acÃ¡
                    }
                }
            }
        }
        if (nodoSuc == null) {
            throw new XcargaDatos("Se buscÃ³ el sucesor de un" +
                    "nodoE en un grafoE, con una decisiÃ³n que el nodo no tiene");
        }
        return nodoSuc;  // no deberÃ­a salir nunca por acÃ¡
    }

//    public Portafolio decOptimaParaObj(Objetivo obj) {
//        /**
//         * Devuelve la decisiÃ³n que es Ã³ptima en el nodo
//         * para el objetivo obj
//         */
//        Portafolio result = null;
//        for (ObjDecision od : decOptimas) {
//            if (od.getObj() == obj) {
//                result = od.getDec();
//                break;
//            }
//        }
//        return result;
//    }

//    public boolean cargaDecOptima(Objetivo obj, Portafolio d) {
//        /**
//         * Carga el par DecisiÃ³n Ã³ptima - Objetivo
//         * en el nodo
//         * Si preexiste una decisiÃ³n Ã³ptima para ese objetivo la borra
//         * carga la nueva decisiÃ³n y devuelve true
//         */
//        boolean estaba = false;
//        for (ObjDecision od : decOptimas) {
//            if (od.getObj() == obj) {
//                estaba = true;
//                od.setDec(d);
//            }
//        }
//        if (!estaba) {
//            ObjDecision od = new ObjDecision(obj, d);
//        }
//        return estaba;
//    }

// ****************************************************************
//  METODOS PARA VINCULAR EL NODO CON PREDECESORES Y SUCESORES
//  CUANDO SE ESTÃ� CREANDO EL GRAFOE
// ****************************************************************
    public void agregarPredecesor(DecConjNodos decConjNodos) {
        /**
         * agrega un predecesor al nodo this
         */
        predecesores.add(decConjNodos);
    }

    public void crearVinculoConSucesor(Portafolio d, NodoE nEsuc) {
        /**
         * 	nEsuc es el nodo sucesor que se desea vincular doblemente con el nodo
         *  corriente (this)
         *  Se agregan los DecConjNodos del nodo this y el sucesor nEsuc
         *  en sucesores y precedesores respectivamente.
         **/
        boolean encontro = false;
        Portafolio dec;
        // carga el sucesor en this
        for (DecConjNodos dcn : sucesores) {
            if (dcn.getDecision() == d) {
                dcn.getNodosE().add(nEsuc);
                encontro = true;
                break;
            }
        }
        if (!encontro) {
            DecConjNodos dcn = new DecConjNodos(d);
            dcn.getNodosE().add(nEsuc);
            sucesores.add(dcn);
        }
        // carga el predecesor en nEsuc
        DecConjNodos dAnt = new DecConjNodos(d);
        dAnt.getNodosE().add(this);
        nEsuc.agregarPredecesor(dAnt);
    }

    public void eliminaVinculoSucConUnaDec(Portafolio d) {
        DecConjNodos aElim = null;
        for (DecConjNodos dcn : sucesores) {
            if (dcn.getDecision() == d) {
                aElim = dcn;
                break;
            }
        }
        sucesores.remove(aElim);
    }

    public boolean tienePredecesores() {
        boolean result = true;
        if (predecesores.isEmpty()) {
            result = false;
        }
        return result;
    }

    public void eliminaUnPred(Portafolio d) {
        /**
         * Elimina en el nodo el predecesor asociado a la
         * decisiÃ³n d (datada en la etapa anterior)
         */
        DecConjNodos aElim = null;
        for (DecConjNodos dcn : predecesores) {
            if (dcn.getDecision() == d) {
                aElim = dcn;
                break;
            }
        }
        predecesores.remove(aElim);
    }

// ****************************************************************
    public boolean esFactible(int iterRest) throws XcargaDatos {
        /**
         * Devuelve true si el nodoE es factible para el conjunto de las restricciones
         * incluso las heurÃ­sticas, cuanto se estÃ¡ en la iteraciÃ³n iterRest de
         * evoluciÃ³n de las restricciones heurÃ­sticas
         */
        ConjRestGrafoE cRP = grafoE.getConjRestParque();
        boolean esF = port.esNodoEFactible(nodoN, grafoE.getInformacion(), cRP);
        return esF;
    }


    /**
     * Devuelve un texto con los resultados de la programaciÃ³n dinÃ¡mica
     */
    public String resultPDDelNodo2(Objetivo obj, ArrayList<Object> informacion) throws XcargaDatos {
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        boolean soloNoComunes = true;
        boolean prod = false;
        boolean usaEquiv = false;
        boolean soloNoNulos = true;
        informacionImp.add(soloNoComunes);
        informacionImp.add(prod);
        informacionImp.add(usaEquiv);
        informacionImp.add(soloNoNulos);        
        String texto = "";
        texto += "NodoE - ordinal " + ordinalNE + "\r\n" 
                + "Ordinal cd= " + cD.getOrdinalCD() + "\r\n" 
                + "Ordinal cdPE " + cDPE.getOrdinal() + "\r\n"  
                + "Portafolio= " + port.creaResumenP(informacion, usaEquiv) + "\r\n" 
                + "Probabilidad del NodoE en el cDPEe " +
                nodoN.getProbCondDadoCDP() + "\r\n";
        if (obj != null && cD.decOptima(obj) != null) {
            texto += "DecisiÃ³n Ã³ptima del NodoE " 
                    + cD.decOptima(obj).toStringCorto(informacion, informacionImp)+ "\r\n";
        }
        if (obj != null) {
            texto += "Valor de Bellman instante inicial = "
                + valorVBIniPaso  + "\r\n"
                + "Costo del paso = " + costoPaso + "\r\n"
                + "Componentes del costo: ";
            for(int or = 0; or<otrosResultadosPaso.size(); or++){
                texto += nombreOtrosResultadosPaso.get(or)+ " " + otrosResultadosPaso.get(or) + " - ";
            }
            texto += "\r\n";
            texto += "Nombre de la simulacion =" + nombreSimulacion + "\r\n";
        }
        texto += "\r\n";
        return texto;
    }

    /**
     * Devuelve un texto con los resultados de la programaciÃ³n dinÃ¡mica
     * en una sola linea adecuada para levantar desde planilla electrÃ³nica,
     * como texto separado por comas.
     */
//    public String resultPDDelNodo2Linea(Objetivo obj, ArrayList<Object> informacion) throws XcargaDatos {
//        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
//        boolean soloEleg = true;
//        boolean prod = false;
//        informacionImp.add(soloEleg);
//        informacionImp.add(prod);
//        String texto = "OrdinalNODOE ";
//        texto += ordinalNE + "," 
//                + cD.getOrdinalCD() + "," 
//                + cDPE.getOrdinal() + ","  
//                + port.getResumenP() + "," 
//                + nodoN.getProbCondDadoCDP() + ",";
//        if (obj != null && cD.decOptima(obj) != null) {
//            texto += cD.decOptima(obj).toStringCortoLinea(informacion, informacionImp)+ ",";
//        }
//        if (obj != null) {
//            texto +=  valorVBIniPasoObj(obj)  + ","
//                + costoPasoObj(obj) + ",";
//                
//            for(int or = 0; or<otrosResultadosPaso.size(); or++){
//                texto += otrosResultadosPaso.get(or) + ",";
//            }            
//            texto += port.devuelveNombreSimul();
//        }
//        return texto;
//    }

    
   
    public int compareTo(Object o) {
    
        NodoE otroNodoE = (NodoE)o;
        if(ordinalNE<otroNodoE.getOrdinalNE()) return -1;
        if(ordinalNE==otroNodoE.getOrdinalNE()) return 0;                
        return 1;        
    }

// ****************************************************************
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NodoE other = (NodoE) obj;
        if (this.etapa != other.etapa) {
            return false;
        }
        if (this.port != other.port && (this.port == null || !this.port.equals(other.port))) {
            return false;
        }
//		if (this.nodoN != other.nodoN && (this.nodoN == null || !this.nodoN.equals(other.nodoN))) {
        /**
         *		El nodoN tiene que ser idÃ©ntico, el mismo. Se evita el riesgo de que
         *		en el grafo de la naturaleza se sobreescriba el equals
         */
        if (this.nodoN != other.nodoN) {
            return false;
        }
        return true;
    }


    public String toStringCorto(ArrayList<Object> informacion, Objetivo obj) throws XcargaDatos {
        String texto = "\r\n" + "COMIENZA DESCRIPCION DE NODOE" + "\r\n";
        texto += "Etapa " + etapa + "\t" + "Ordinal " + ordinalNE + "\r\n";
        texto += "Cd al que pertenece - ordinal " + cD.getOrdinalCD() + "\r\n";
        if(cDPE != null) texto += "CdPE al que pertenece - ordinal " + cDPE.getOrdinal() + "\r\n";
        Estudio est = (Estudio)informacion.get(0);
        ConjDemandas conjD = est.getConjDemandas();
        if (obj != null) {
            if(cD.isSinSuc()){
                texto += "el nodo pertenece a un cd sin sucesores";
            }else{
                texto += resultPDDelNodo2(obj, informacion);
            }
        }
        texto += "\r\n";
        texto += port.creaTextoAdicNodoE(informacion, nodoN);
        texto += "\r\n";
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        boolean soloNoComunes = true;
        boolean prod = true;
        boolean usaEquiv = false;
        boolean soloNoNulos = true;
        informacionImp.add(soloNoComunes);
        informacionImp.add(prod);
        informacionImp.add(usaEquiv); // se usa el RecursoBase y no su equivalente en simulacion
        informacionImp.add(soloNoNulos);        
        texto += "PORTAFOLIO: \r\n"+ port.toStringCorto(informacion, informacionImp) + "\r\n";
        texto += "NodoN ordinal en la etapa: " + nodoN.getOrdinal() + "\r\n";
        texto += "Lista de ordinales de NodosE predecesores" + "\r\n";
        for (int i = 0; i < predecesores.size(); i++) {
            texto += predecesores.get(i).getNodosE().get(0).getOrdinalNE() + "\t";
        }
        prod = false;
        informacionImp.set(1, prod);
        texto += "\r\n";
        texto += "Lista de NodosE sucesores" + "\r\n";
        if(cD.isSinSuc()){
            texto += "Nodo pertenece a cD sin sucesores";
        }else{
            for (int i = 0; i < sucesores.size(); i++) {
                texto += "Decision :" + sucesores.get(i).getDecision().toStringCorto(informacion, informacionImp) + " -- Ordinales de nodos sucesores: ";
                for (int j = 0; j < sucesores.get(i).getNodosE().size(); j++) {
                    texto += sucesores.get(i).getNodosE().get(j).getOrdinalNE() + "\t";
                }
                texto += "\r\n";
            }
        }
        return texto;
    }

//    public String toStringCortoLinea(ArrayList<Object> informacion, Objetivo obj, boolean cabezal)
//            throws XcargaDatos {
//        String texto ="";
//        if(cabezal){
//            texto += "ETAPA ,";
//            texto += "ORDINAL NODOE ,";
//            texto += "ORDINAL CD ,";
//            texto += "ORDINAL CDPE ,";
//            texto += "ORDINAL NODON ,";
//            texto += "PROB NODON EN CI ,";
//            texto += "PORTAFOLIO ,";
//            texto += "SIMULACION ,";
//            texto += "V.BELLMAN INI ,";
//            texto += "COSTO PASO ,";
//            for(String s: nombreOtrosResultadosPaso){
//                texto += s + " ,";              
//            }
//            texto += "SUCESORES ";
//        }else{
//            texto += etapa + " ,";
//            texto += ordinalNE + " ,";
//            texto += cD.getOrdinalCD() + " ,";
//            if(cDPE != null) {
//                texto += cDPE.getOrdinal() + " ,";
//            }else{
//                texto += " ,";
//            }
//            texto += nodoN.getOrdinal() + " ,";
//            texto += nodoN.getProbCondDadoCI() + " ,";
//            texto += port.getResumenP() + " ,";
//            texto += nombreSimulacion + " ,";
//            if(obj != null) {
//                texto += valorVBIniPasoObj(obj)+ " ,";
//                texto += costoPasoObj(obj)+ " ,";
//                for(Double d: otrosResultadosPaso){
//                    texto += d + " ,";                    
//                }
//            }else{
//                texto += " ,";
//                texto += " ,";
//                for(String s: nombreOtrosResultadosPaso){
//                    texto += " ,";
//                }
//            }
//            for (int i = 0; i < sucesores.size(); i++) {
//                texto += sucesores.get(i).getDecision().getResumenP() + " Sucesores: ";
//                for (int j = 0; j < sucesores.get(i).getNodosE().size(); j++) {
//                    texto += sucesores.get(i).getNodosE().get(j).getOrdinalNE() + " ";
//                }
//                texto += ",";
//            }
//
//        }
//    return texto;
//    }
}
