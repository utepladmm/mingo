/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package AlmacenSimulaciones;

import GrafoEstados.PeriodoDeTiempo;
import GrafoEstados.ResSimul;
import dominio.ResSimulDB;
import ManejadorTextos.CreadorTex;
import ManejadorTextos.ParTipoDatoPlantilla;
import ManejadorTextos.PlantillaEnTexto;
import UtilitariosGenerales.DirectoriosYArchivos;
import UtilitariosGenerales.ParString;
import dominio.ConjTiposDeDato;
import dominio.Estudio;
import dominio.TipoDeDatos;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import persistencia.CargaDefTiposDeDato;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 *
 * La clase almacena los datos para la implementación del modelo de operación (MOP) asociado 
 * y tiene los métodos que permiten generar las corridas y los resultados del MOP
 * que deben ser sobreescritos
 *
 *
 */
public class ImplementadorGeneral implements Serializable{

    protected ArrayList<ArrayList<String>> textoBaseComando;
    protected ArrayList<ParTipoDatoPlantilla> plantillas;
    protected ConjTiposDeDato conjTD;
    /**
     * Es el directorio donde se encuentran los datos propios del implementador
     */
    protected String dirDatosImplementador;
    /**
     * Es el directorio donde se almacenarán los datos y resultados de simulaciones
     * que ejecute el implementador
     */
    protected String dirCorridas;
    /**
     * Es el directorio raíz debajo del cual se crean los estudios
     */
    protected String dirEstudios;
    

    public ImplementadorGeneral(){
        textoBaseComando = new ArrayList<ArrayList<String>>();
        plantillas = new ArrayList<ParTipoDatoPlantilla>();
        conjTD = new ConjTiposDeDato();
    }

    public ConjTiposDeDato getConjTD() {
        return conjTD;
    }

    public void setConjTD(ConjTiposDeDato conjTD) {
        this.conjTD = conjTD;
    }

    public ArrayList<ParTipoDatoPlantilla> getPlantillas() {
        return plantillas;
    }

    public void setPlantillas(ArrayList<ParTipoDatoPlantilla> plantillas) {
        this.plantillas = plantillas;
    }

    public ArrayList<ArrayList<String>> getTextoBaseComando() {
        return textoBaseComando;
    }

    public void setTextoBaseComando(ArrayList<ArrayList<String>> textoBaseComando) {
        this.textoBaseComando = textoBaseComando;
    }

    public String getDirEstudios() {
        return dirEstudios;
    }

    public void setDirEstudios(String dirEstudio) {
        this.dirEstudios = dirEstudio;
    }

    public String getDirDatosImplementador() {
        return dirDatosImplementador;
    }

    public void setDirDatosImplementador(String dirDatosImplementador) {
        this.dirDatosImplementador = dirDatosImplementador;
    }

    public String getDirCorridas() {
        return dirCorridas;
    }

    public void setDirCorridas(String dirCorridas) {
        this.dirCorridas = dirCorridas;
    }

/**
 **************************************************
 *
 * COMIENZAN LOS MÉTODOS QUE DEBEN SOBREESCRIBIRSE
 * IMPLEMENTADOR GENERAL
 *
 **************************************************
 */

    /**
     * El método genera los resultados de una corrida correspondientes a un
     * PeriodoDeTiempo
     *
     * ESTE METODO DEBE SER SOBREESCRITO PARA LAS CLASES QUE HEREDAN DE ESTA
     * PARA CADA IMPLEMENTACION CONCRETA PARA UN MODELO DE OPERACION
     *
     * @param dirCorrida es el path del directorio donde están los resultados
     * de la corrida
     * @param per es el PeriodoDeTiempo para el que se creará el resultado.
     * @param informacion es la información empaquetada que puedan precisar
     * los métodos de las clases que heredan de esta.
     * @return es el ResSimulPaso del paso respectivo.
     */
    public ResSimul creaRes(String dirCorrida, PeriodoDeTiempo per, Estudio est) throws XcargaDatos{
        ResSimul res = new ResSimul();
        return res;
    }
    
    public ResSimulDB creaResDB(String dirCorrida, PeriodoDeTiempo per, Estudio est) throws XcargaDatos{ 
        ResSimulDB rsDB = new ResSimulDB(1,1);
        return rsDB;
    }   

    /**
     * Realiza las inicializaciones que requiera cada implementación particular
     * Los métodos que sobreescriban a este le dan contenido.
     */
    public void inicializaParticular(){

    }



    /**
     * El método crea los datos y archivo de comandos de una corrida corespondientes
     * a un PeríodoDeTiempo.
     *
     * ESTE METODO DEBE SER SOBREESCRITO PARA LAS CLASES QUE HEREDAN DE ESTA
     * PARA CADA IMPLEMENTACION CONCRETA PARA UN MODELO DE OPERACION
     *
     * @param dirDatosPadre es el directorio donde se encuentran los datos
     * @param nombreCorr es el nombre de la corrida que aparecerá en el archivo de comando .bat
     * @param dirCorrida es el path del directorio donde se generan
     * los datos y el archivo de comandos de la corrida
     * @param per es el PeriodoDeTiempo que se simulará.
     * @param informacion es la información empaquetada que puedan precisar
     * los métodos de las clases que heredan de esta.
     * 
     * @return en la implementaci�n MOP devuelve ruta de entrada y salida de la corrida
     * en la implmentaci�n EDF es irrelevante
     */
     public ParString creaDatosYComandos(String nombreCorr, IdentSimulPaso idCorr,
             String dirCorrida, ArrayList<Object> informacion) throws XcargaDatos{
    	 return new ParString("","");

     }

/**
 **************************************************
 * FINALIZAN LOS MÉTODOS QUE DEBEN SER SOBREESCRITOS
 * IMPLEMENTADOR GENERAL
 **************************************************
 */

     
     
     /**
      * Inicializa el implementador leyendo los TipoDeDatos, el archivo de texto base
      * de comandos .bat, y las plantillas para sustituir en el texto base.
      *
      * @param dir_Datos_Implementación es el directorio donde se leen los archivos
      * de datos para el implementador
      * @param dir_Datos_Estudio es el directorio donde se leen los archivos de datos del Estudio.
      * @param dir_Corridas es el directorio donde se ejecutan las corridas. Para usar el
      * corredor de Maurizio debe ser un directorio compartido.
      */
     public void inicializaImplementador(String dir_Datos_Implementacion,
             String dir_Estudios, String dir_Corridas) throws XcargaDatos{

        dirDatosImplementador = dir_Datos_Implementacion;
        dirCorridas = dir_Corridas;
        // Verifica si existe el directorio dirCorridas
        if(!DirectoriosYArchivos.existeArchivo(dirCorridas)){
            JOptionPane.showMessageDialog(null, "ATENCION: El directorio de corridas no existe, debe crearlo" +
                    " y luego reiniciar la ejecución cerrando este diálodo");

        }


        dirEstudios = dir_Estudios;
        /**
         * ------------------------------------------------------
         * Lee los tipos de dato de la implementación
         * ------------------------------------------------------
         */
        String dirTiposDeDatos = dir_Datos_Implementacion + "/" + "defTiposDeDatos.txt";
        CargaDefTiposDeDato.cargarTiposDato(dirTiposDeDatos, conjTD);
        /**
         * ------------------------------------------------------
         * Lee las plantillas y el archivo de comandos de la implementación
         * ------------------------------------------------------
         */
        String sep = "\\<\\?\\>"; // Separador de datos es "<?>"
        String archTB = dir_Datos_Implementacion + "/" + "batcorrida.txt";
        String archPlant = dir_Datos_Implementacion + "/" + "plantillas.txt";
        textoBaseComando = CreadorTex.leeTextoBase(archTB, sep);
        plantillas = leePlantillasDeTipos(archPlant);
        /**
         * ----------------------------------------------------
         * Elimina si existe el archivo resumen de simulaciones
         * ----------------------------------------------------
         */
        String archResumenCorr = dir_Corridas + "/" +  "resumenCorridas.txt";        
        try{
            DirectoriosYArchivos.eliminaArchivo(archResumenCorr);

        }catch (IllegalArgumentException e){
            // no se hace nada porque es posible que el archivo no exista cuando se empieza el estudio.
        }
     }


    /**
     * Carga las plantillas en texto que se emplean para fabricar archivos de comandos,
     * asociadas a cada uno de los tipos en su orden respectivo.
     * @param dirArchivo es el path del archivo de plantillas.
     */
    public ArrayList<ParTipoDatoPlantilla> leePlantillasDeTipos(String dirArchivo) throws XcargaDatos{
        ArrayList<PlantillaEnTexto> plantAux = CreadorTex.leePlantillas(dirArchivo);
        ArrayList<ParTipoDatoPlantilla> listaPTDP = new ArrayList<ParTipoDatoPlantilla>();
        ArrayList<TipoDeDatos> tipos = conjTD.getTipos();
        for(TipoDeDatos tip: tipos){
            int iplan = 0;
            boolean encontro = false;
            do{
               if(tip.getNombre().equalsIgnoreCase(plantAux.get(iplan).getNombre()) ){
                   encontro = true;
                   ParTipoDatoPlantilla parTP = new ParTipoDatoPlantilla(tip, plantAux.get(iplan) );
                   listaPTDP.add(parTP);
               }
               iplan++;
            }while(iplan < plantAux.size() && !encontro);
        }
        return listaPTDP;
    }
}
