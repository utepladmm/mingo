/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package AlmacenSimulaciones;

import ManejadorTextos.ParTipoDatoPlantilla;
import dominio.ParTipoDatoValor;
import ManejadorTextos.CreadorTex;
import ManejadorTextos.PlantillaEnTexto;
import UtilitariosGenerales.UtilArrayList;
import dominio.TipoDeDatos;
import java.util.ArrayList;
import persistencia.XcargaDatos;

/**
 *
 * @author ut469262
 * Crea comandos .bat para las corridas 
 */
public abstract class CreadorComandos {


//    public enum TiposDatosEDF{
//        DEMANDA,
//        COMBUS,
//        IMPOEXPO,
//        HIDROYFALLA,
//        PARAMS;
//    }






    
    /**
     * Crea un archivo de comandos para una corrida con el textoBase del creador
     * de comandos this y sustituyendo en cada TipoDeDatos los valores de valoresSimbolicos
     * 
     * @param textoBase es el texto con el comando con valores a ser sustituídos, separados por
     * el texto "<?>".
     * @param valoresSimbolicos son los valores simbolicos asociados a los tipos de datos,
     * uno por cada TipoDeDatos.
     * @param plantillas son las plantillas asociadas a los tipos de datos,
     * uno por cada TipoDeDatos.
     * @param nombreCorr es el nombre que sustituirá a "CORRIDA" en el texto base.
     * @return comandos es el texto de comandos con los saltos de línea necesarios.
     * @throws persistencia.XcargaDatos
     */
    public static String creaTextoDeUnComando(ArrayList<ArrayList<String>> textoBase,
            ArrayList<ParTipoDatoValor> valoresSimbolicos,
            ArrayList<ParTipoDatoPlantilla> plantillas,
            String nombreCorr) throws XcargaDatos{
        if(valoresSimbolicos.size()!= plantillas.size()) throw new XcargaDatos("" +
                "Se pidió crear un comando con una cantidad errada de valores simbólicos" +
                valoresSimbolicos.toString());
        String comando = "";
        ArrayList<ArrayList<String>> copiaTextoBase = UtilArrayList.copiaALString2D(textoBase);
        // Primero ordena emparejando por tipo asociado, valores y plantillas.
        ArrayList<String> valArg = new ArrayList<String>();
        ArrayList<PlantillaEnTexto> planArg = new ArrayList<PlantillaEnTexto>();
        for(ParTipoDatoPlantilla tDPlan: plantillas ){
            TipoDeDatos tD = tDPlan.getTipoD();
            boolean encontro = false;
            int ivs = 0;
            do{
                TipoDeDatos tV = valoresSimbolicos.get(ivs).getTipoDatos();
                if(tV.equals(tD)){
                    encontro = true;
                    valArg.add(tD.getDireccion()+ "/" + valoresSimbolicos.get(ivs).getValor());
                    planArg.add(tDPlan.getPlantilla());
                }
                ivs++;
            }while(ivs<valoresSimbolicos.size() && !encontro);
            if(!encontro) throw new XcargaDatos("No se encontro valor simbólico para el TipoDeDatos " +
                    tDPlan.getTipoD().getNombre());
        }
          // Carga el indicador de corrida donde encuentre en textoBaseComando el String "CORRIDA".
          for (int i = 0; i < copiaTextoBase.size(); i++) {
               for (int j = 0; j < copiaTextoBase.get(i).size(); j++) {
                    if (copiaTextoBase.get(i).get(j).equalsIgnoreCase("CORRIDA")) {
                         copiaTextoBase.get(i).set(j, nombreCorr);
                    }
               }
          }

        // Llama al método que crea el texto
        comando = CreadorTex.creaTexto(copiaTextoBase, valArg, planArg);
        return comando;
    }

//    public static void main(String[] args) throws XcargaDatos, IOException{
//        String dirArchivo = "D:/Java/PruebaJava3";
//        String texto = "";
//        String sep = "\\<\\?\\>"; // Separador de datos es "<?>"
//        ArrayList<ArrayList<String>> textoBase = CreadorTex.leeTextoBase(dirArchivo + "/batCorrida.txt", sep);
//        ArrayList<ParTipoDatoPlantilla> listaPTDP = new ArrayList<ParTipoDatoPlantilla>();
//        Estudio est = new Estudio("Prueba", 4);
//
//        est.leerEstudio(dirArchivo, false);
//        ConjTiposDeDato conjTD = est.getConjTiposDatos();
//        TipoDeDatos tipoCombus = est.getConjTiposDatos().tipoDeNombre("combustibles");
//        ParTipoDatoValor parTDVcom = new ParTipoDatoValor(tipoCombus, "comAlto");
//        TipoDeDatos tipoDem = est.getConjTiposDatos().tipoDeNombre("demanda");
//        ParTipoDatoValor parTDVdem = new ParTipoDatoValor(tipoDem, "demBaja");
//
//        ArrayList<ParTipoDatoValor> valoresSimbolicos = new ArrayList<ParTipoDatoValor>();
//        valoresSimbolicos.add(parTDVcom);
//        valoresSimbolicos.add(parTDVdem);
//        String dirPlantillas = dirArchivo + "/plantillas.txt";
//        ArrayList<PlantillaEnTexto> listaPlantillas = CreadorTex.leePlantillas(dirPlantillas);
//        for(PlantillaEnTexto plan: listaPlantillas){
//            TipoDeDatos tD = conjTD.tipoDeNombre(plan.getNombre());
//            ParTipoDatoPlantilla pTDP = new ParTipoDatoPlantilla(tD, plan);
//            listaPTDP.add(pTDP);
//        }
//        String comando = creaTextoDeUnComando(textoBase, valoresSimbolicos,
//                listaPTDP);
//        DirectoriosYArchivos.grabaTexto(dirArchivo + "/comando.bat" , comando);
//    }
}
