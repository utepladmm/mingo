/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package AlmacenSimulaciones;

import GrafoEstados.PeriodoDeTiempo;
import GrafoEstados.ResSimul;
import dominio.ResSimulDB;
import UtilitariosGenerales.DirectoriosYArchivos;
import UtilitariosGenerales.Wait;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import dominio.ParTipoDatoValor;
import dominio.Parque;
import dominio.TiempoAbsoluto;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;
import naturaleza3.NodoN;
import persistencia.XcargaDatos;
import java.sql.*;

/**
 *
 * @author ut469262
 * La clase almacena los resultados de las simulaciones en un HashMap
 */
public class BolsaDeSimulaciones3 implements Serializable {

    private ImplementadorGeneral impGen;
    private ArrayList<String> estudiosAsociados;
    /**
     * Almacena los directorios de los estudios que emplean esta bolsa
     */
    private ArrayList<TiempoAbsoluto> tiemposSimulados;
    private ArrayList<HashMap<ClaveStringSimul, ResSimul>> resultados;

    /**
     * Hay un HashMap para cada tiempoAbsoluto de tiemposSimulados.
     * El get(0) corresponde al primer TiempoAbsoluto de tiemposSimulados, y así
     * sucesivamente.
     */
    public BolsaDeSimulaciones3(ArrayList<TiempoAbsoluto> tiemposSimulados, ImplementadorGeneral impGen) {
        this.impGen = impGen;
        this.tiemposSimulados = tiemposSimulados;
        resultados = new ArrayList<HashMap<ClaveStringSimul, ResSimul>>();
        int cantEtapas = tiemposSimulados.size();
        for (int ie = 1; ie <= cantEtapas; ie++) {
            HashMap<ClaveStringSimul, ResSimul> hSM = new HashMap<ClaveStringSimul, ResSimul>();
            resultados.add(hSM);
        }
    }

    public ArrayList<String> getEstudiosAsociados() {
        return estudiosAsociados;
    }

    public void setEstudiosAsociados(ArrayList<String> estudiosAsociados) {
        this.estudiosAsociados = estudiosAsociados;
    }

    public ArrayList<HashMap<ClaveStringSimul, ResSimul>> getResultados() {
        return resultados;
    }

    public void setResultados(ArrayList<HashMap<ClaveStringSimul, ResSimul>> resultados) {
        this.resultados = resultados;
    }

//    public HashMap<ClaveStringSimul, ResSimul> resultadosDeEtapa(int etapa) {
//        HashMap<ClaveStringSimul, ResSimul> result = resultados.get(etapa - 1);
//        return result;
//    }
    /**
     * A partir de un tiempo absoluto devuelve el HashMap correspondiente 
     * a los resultados de ese tiempo absoluto
     * @param ta
     * @return 
     */
    public HashMap<ClaveStringSimul, ResSimul> resultadosDeTA(TiempoAbsoluto ta) throws XcargaDatos {
        int it = tiemposSimulados.indexOf(ta);
        if (it == -1) {
            throw new XcargaDatos("No hay datos en la bolsa de simulaciones para el tiempo absoluto: " + ta.toStringCorto());
        }
        return resultados.get(it);
    }

    public ImplementadorGeneral getImpGen() {
        return impGen;
    }

    public void setImpGen(ImplementadorGeneral impGen) {
        this.impGen = impGen;
    }

    /**
     * Devuelve el resultado de una simulación ya realizada que esta
     * almacenada en la bolsa dado un Parque y un NodoN.
     * @param parque es el parque (atención que el parque como Portafolio tiene su TiempoAbsoluto y etapa que
     * este método es capaz de reconocer).
     * @param nt es el NodoN del GrafoN
     * @return resSimP es el resultado de la simulación
     */
    public ResSimul devuelveRes(Parque parque, NodoN nt, ArrayList<Object> informacion) throws XcargaDatos {
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        Estudio est = (Estudio) informacion.get(0);
        TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(parque.getEtapa());
        informacionImp.add(true);
        informacionImp.add(false);
        int etapa = parque.getEtapa();
        if (etapa == 0) {
            throw new XcargaDatos("Se pidió un resultado de etapa cero");
        }
        ResSimul resSimP = null;
        Parque parqueOperativo = parque.calculaParqueOperativo(informacion);
        parqueOperativo.cambiaPorRBEquiv(informacion);
        boolean usaEquiv = true;

        String resumenP = parqueOperativo.creaResumenP(informacion, usaEquiv);
        ArrayList<ParTipoDatoValor> valoresTiposDatos = nt.getValoresTiposDatos();
        ClaveStringSimul ident = new ClaveStringSimul(ta, resumenP, valoresTiposDatos);
        HashMap hashM = resultadosDeTA(ta);
        resSimP = (ResSimul) hashM.get(ident);
        if (resSimP == null) {
            throw new XcargaDatos("Se pidió un resultado de un identificador que no existe "
                    + "identificador es: " + ident.toStringCorto(informacion, informacionImp));
        }
        return resSimP;
    }

    /**
     * Crea un paquete de corridas para que sean ejecutadas por un Corredor
     * Cada corrida tiene un subdirectorio.
     * @param listaCorridas es la lista decorridas que se requieren. Se ejecutan sólo
     * las que no están ya almacenadas en la BolsaDeSimulaciones.
     * @param ordinalPaquete es el ordinal del paquete de corridas en el Estudio.
     * @param direc es el directorio donde se generan las corridas
     * debajo de ese directorio se genera un subdirectorio para cada paquete
     * con el nombre p_nn siendo nn el número ordinalPaquete.
     * @param informacion
     */
    public void lanzaPaqueteCorridas(ArrayList<IdentSimulPaso> listaCorridas, int ordinalPaquete,
            String direc,
            ArrayList<Object> informacion) throws XcargaDatos, FileNotFoundException, IOException {
        Estudio est = (Estudio) informacion.get(0);
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        String dirDatosImplementador = impGen.getDirDatosImplementador();

        /**
         * Crea el subdirectorio donde generará las corridas del paquete
         */
        String nombrePaquete = "P_" + ordinalPaquete;
        String dirPaquete = direc + "/" + nombrePaquete;
        if (!UtilitariosGenerales.DirectoriosYArchivos.existeArchivo(dirPaquete)) {
            UtilitariosGenerales.DirectoriosYArchivos.creaDirectorio(direc, nombrePaquete);
        }

        /**
         * Inicializa el reporte de corridas lanzadas que se escribe en el directorio
         * del paquete
         */
        String archCorrLanzadas = dirPaquete + "/resumenCorridasLanzadas.txt";
        if (UtilitariosGenerales.DirectoriosYArchivos.existeArchivo(archCorrLanzadas)) {
            UtilitariosGenerales.DirectoriosYArchivos.eliminaArchivo(archCorrLanzadas);
        }

        /**
         * Elimina de la lista a ejecutar las corridas que ya están ejecutadas
         * y almacenadas en la BolsaDeSimulaciones
         */
        int paso;
        int etapa;
        ArrayList<IdentSimulPaso> aEliminar = new ArrayList<IdentSimulPaso>();
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        boolean soloExist = true;
        boolean impProd = false;
        boolean usaEquiv = true;
        boolean soloNoNulos = true;
        informacionImp.add(soloExist);
        informacionImp.add(impProd);
        informacionImp.add(usaEquiv);
        informacionImp.add(soloNoNulos);
        /**
         * En las simulaciones se usan los recursos equivalentes en simulación y son ellos
         * los que forman la clave
         */
        for (IdentSimulPaso icorr : listaCorridas) {
            Parque par = icorr.getParque();
            String resumenP = par.creaResumenP(informacion, usaEquiv);
            TiempoAbsoluto ta = icorr.getTa();
            ArrayList<ParTipoDatoValor> vTDClave = icorr.getValoresTiposDatos();
            ClaveStringSimul clave = new ClaveStringSimul(ta, resumenP, vTDClave);
            paso = datGen.pasoDeTiempoAbsoluto(ta);
            etapa = datGen.etapaDeUnPaso(paso);
            if (resultadosDeTA(ta).containsKey(clave)) {
                aEliminar.add(icorr);
            }
        }
        listaCorridas.removeAll(aEliminar);

        /**
         * Guarda en disco en el directorio corridas (atención, no en el 
         * subdirectorio del paquete) un objeto PaqueteCorridas
         * con la lista de corridas que se mandará a ejecutar
         */
        String archListaObj = "PaqueteCorriente";
        ArrayList<Object> listaObjetos = new ArrayList<Object>();
        Integer intOrdPaq = (Integer) ordinalPaquete;
        listaObjetos.add(intOrdPaq);
        for (IdentSimulPaso corr : listaCorridas) {
            Parque par = corr.getParque();
            String resumenP = par.creaResumenP(informacion, usaEquiv);
            TiempoAbsoluto ta = est.tiempoAbsolutoDeEtapa(par.getEtapa());
            ClaveStringSimul csimul = new ClaveStringSimul(ta, resumenP, corr.getValoresTiposDatos());
            listaObjetos.add(csimul);
        }
        UtilitariosGenerales.ManejaObjetosEnDisco.guardaListaObjEnDisco(direc, archListaObj, listaObjetos);


        /**
         * Copia en el directorio direc el archivo COMENZAR.BAT del corredor
         */
        String archOrigen = dirDatosImplementador + "/COMENZAR.BAT";
        String archDestino = dirPaquete + "/" + "COMENZAR.BAT";
        try {
            UtilitariosGenerales.DirectoriosYArchivos.copy2(archOrigen, archDestino);
        } catch (Exception e) {
            throw new XcargaDatos(e.toString());
        }

        /**
         * Elimina el archivo FIN.txt del directorio de corridas
         */
        String archivoFin = dirPaquete + "/FIN.TXT";
        try {
            DirectoriosYArchivos.eliminaArchivo(archivoFin);
        } catch (Exception X) {
            System.out.println("NO ENCONTRO EL ARCHIVO FIN.TXT");
        }
        /**
         * Crea las corridas que deben ejecutarse en el directorio de corridas
         * y agrega un texto identificador de la corrida en resumenCorridas.txt
         */
        int ordinalCorr = 1;

        // crea un paquete de corridas y el archivo de resumen de corridas lanzadas.
        String texto = "";
        DirectoriosYArchivos.agregaTexto(archCorrLanzadas, texto);
        for (IdentSimulPaso idCorr : listaCorridas) {
            creaUnaCorrida(ordinalPaquete, ordinalCorr, idCorr, dirPaquete, informacion);
            texto = "Paquete = " + ordinalPaquete + " - Corrida = " + ordinalCorr
                    + "\r\n" + idCorr.toStringCorto(informacion, informacionImp)
                    + "\r\n" + "hashcode de idCorr = " + idCorr.hashCode() + "\r\n";
            DirectoriosYArchivos.agregaTexto(archCorrLanzadas, texto);
            ordinalCorr++;
        }
        JOptionPane.showMessageDialog(null, "ATENCION: DEBE INICIAR LA EJECUCIÓN DEL CORREDOR\n "
                + " - LUEGO QUE TERMINEN TODAS LAS CORRIDAS\n PRESIONE ACEPTAR");
    }
    

    /**
     * Levanta los resultados de un paquete de corridas verificando si terminaron
     * bien y almacenando el resultado de cada corrida en la BolsaDeSimulaciones this.
     * Cada corrida tiene un subdirectorio.
     * @param listaCorridas es la lista de corridas que se requieren.
     * @param ordinalPaquete es el ordinal del paquete de corridas en el Estudio.
     * @param direc es el directorio donde se generan las corridas
     * @param esperaMinutMax es la máxima espera en minutos admisible, a la espera de
     * que las corridas estén terminadas.
     * @param informacion
     */
    public void levantaPaqueteCorridas(Estudio est,
            String direc, int ordinalPaquete, int esperaMinutMax,
            ArrayList<Object> informacion) throws XcargaDatos, FileNotFoundException, IOException, ClassNotFoundException {
        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        boolean termino = false;
        String nombrePaquete = "P_" + ordinalPaquete;
        String dirPaquete = direc + "/" + nombrePaquete;
        int ordinalCorr = 1;
        int paso;
        int etapa;
        ArrayList<Boolean> informacionImp = new ArrayList<Boolean>();
        Boolean soloExist = true;
        Boolean impProd = false;
        informacionImp.add(soloExist);
        informacionImp.add(impProd);
        String archivoBolsa = dirPaquete + "/corridasLevantadasABolsa.txt";
        if (UtilitariosGenerales.DirectoriosYArchivos.existeArchivo(archivoBolsa)) {
            UtilitariosGenerales.DirectoriosYArchivos.eliminaArchivo(archivoBolsa);
        }
        String archivoFin = dirPaquete + "/FIN.TXT";
        long cantSec = 1;
        int cantEsperas = 0;
        double tiempoEnMinut = 0.0;
        do {
            Wait.manySec(cantSec);
            cantEsperas++;
            tiempoEnMinut = (double) (cantEsperas * 5 / 60);
            termino = DirectoriosYArchivos.existeArchivo(archivoFin);
        } while (!termino && tiempoEnMinut < esperaMinutMax);
        if (!termino) {
            int quiereSeguirIgual = JOptionPane.showConfirmDialog(null,
                    "NO SE ENCONTRÓ FIN.TXT. ¿QUIERE SEGUIR?");
            if (quiereSeguirIgual == JOptionPane.NO_OPTION) {
                throw new XcargaDatos("Se cancela por no haber terminado las corridas");
            }
        }

        // Se terminó de ejecutar las corridas
        boolean corrioMal = false;
        String archivoMal = dirPaquete + "/CORRIOMAL.TXT";
        corrioMal = DirectoriosYArchivos.existeArchivo(archivoMal);
        if (corrioMal) {
            JOptionPane.showConfirmDialog(null, "ALGUNA CORRIDA CORRIO MAL");
        } else {
            /**
             * Levanta el objeto PaqueteCorridas con la listaCorridas
             * del directorio corridas, (atención, no del directorio del paquete)
             */
            ArrayList<Object> listaObjetos;
            ArrayList<ClaveStringSimul> listaCorridas = new ArrayList<ClaveStringSimul>();

            String archListaObj = "PaqueteCorriente";
            listaObjetos =
                    UtilitariosGenerales.ManejaObjetosEnDisco.traerListaObjDeDisco(direc, archListaObj);
            int ordinalPaqueteLeido = (Integer) listaObjetos.get(0);
            if (!(ordinalPaqueteLeido == ordinalPaquete)) {
                throw new XcargaDatos("El ordinal de paquete leído"
                        + "no coincide");
            }
            for (int icorr = 1; icorr < listaObjetos.size(); icorr++) {
                ClaveStringSimul csimul = (ClaveStringSimul) listaObjetos.get(icorr);
                listaCorridas.add(csimul);
            }


            ordinalCorr = 1;
            /**
             * Carga en la BolsaDeSimulaciones el resultado de la corrida
             * Los directorios de cada corrida están en dirPaquete
             */
            for (ClaveStringSimul idCorr : listaCorridas) {

                ResSimul result = levantaUnaCorrida3(est, ordinalPaquete, ordinalCorr,
                        idCorr, dirPaquete);
                TiempoAbsoluto ta = (TiempoAbsoluto) result.getPer();
                paso = datGen.pasoDeTiempoAbsoluto(ta);
                etapa = datGen.etapaDeUnPaso(paso);
                System.out.println("Empieza a levantar: ordinal corrida =" + ordinalCorr
                        + " nombre de simulación " + idCorr.getNombreSimul());
                HashMap<ClaveStringSimul, ResSimul> res1etapa = resultadosDeTA(ta);
                ArrayList<ParTipoDatoValor> vTDClave = idCorr.getValoresTiposDatos();

                String resumenP = idCorr.getResumenP();

                ClaveStringSimul clave = new ClaveStringSimul(ta, resumenP, vTDClave);
                res1etapa.put(clave, result);
                String texto1Corr = "Paquete = " + ordinalPaquete + " - Corrida = " + ordinalCorr
                        + "\r\n" + idCorr.toStringCorto(informacion, informacionImp)
                        + "\r\n" + "hashcode de idCorr = " + idCorr.hashCode() + "\r\n";
                UtilitariosGenerales.DirectoriosYArchivos.agregaTexto(archivoBolsa, texto1Corr);
                ordinalCorr++;

            }
        }
    }

    /**
     * Debajo del directorio direc crea un subdirectorio
     * para una corrida, y en el genera los archivos de datos y de comandos
     * para hacer la corrida basada en identS
     *
     * @param ordinalCorr es el ordinal de la corrida dentro del paquete
     * @param ordinalPaquete es el ordinal del paquete de corridas dentro del estudio
     * @param identS es el identificador de la corrida que tiene la información
     * mínima para hacerla.
     * @param direc es el path del directorio que se supone preexistente en el que genera el
     * subdirectorio de la corrida
     * @param informacion
     */
    public void creaUnaCorrida(int ordinalPaquete, int ordinalCorr, IdentSimulPaso identS,
            String dirPaquete, ArrayList<Object> informacion) throws XcargaDatos, FileNotFoundException {

        String nombreCorr = "c_" + String.valueOf(ordinalPaquete) + "_" + String.valueOf(ordinalCorr);
        DirectoriosYArchivos.creaDirectorio(dirPaquete, nombreCorr);
        String dirCorr = dirPaquete + "/" + nombreCorr;
        impGen.creaDatosYComandos(nombreCorr, identS, dirCorr, informacion);
//        /**
//         * COMIENZA CODIGO CHANCHO DE PRUEBAS
//         */
//        String dirCorr = "D:/_Migro/mibarburu/UTE/PruebaTiemposMingo";
//        impGen.creaDatosYComandos(nombreCorr, identS, dirCorr, informacion);
//        System.out.println("crea un serializado");
    }

    /**
     *
     * @param ordinal ordinal de la corrida dentro del paquete.
     * @param identS identificador de la corrida a levantar.
     * @param dirPaquete es el directorio donde está todo el paquete de corridas.
     * @param informacion.
     * @throws persistencia.XcargaDatos
     */
    public ResSimul levantaUnaCorrida3(Estudio est,
            int ordinalPaquete, int ordinalCorr, ClaveStringSimul identS,
            String dirPaquete) throws XcargaDatos {

        String nombreCorr = "c_" + String.valueOf(ordinalPaquete) + "_" + String.valueOf(ordinalCorr);
        PeriodoDeTiempo per = identS.getTa();
        identS.setNombreSimul(nombreCorr);
        /**
         * nombreCorr se modifica pero no está en el hash ni el equals de IdentSimulPaso
         */
        String dirCorr = dirPaquete + "/" + nombreCorr;
        ResSimul resSim = impGen.creaRes(dirCorr, per, est);
        resSim.setNombreSimul(dirCorr);
        return resSim;
    }

    /**
     * Método que crea el esqueleto de la base de datos
     * @param dirResultCaso
     * @throws ClassNotFoundException
     * @throws XcargaDatos
     * @throws SQLException 
     */
    public void creaDB(String dirResultCaso) throws ClassNotFoundException, XcargaDatos, SQLException {

        Class.forName("org.sqlite.JDBC");  // carga el driver            
        Connection conex = null;
        Statement st = null;

        try {
            conex = DriverManager.getConnection("jdbc:sqlite:" + dirResultCaso + "/DatosAnuales.db");
            conex.setAutoCommit(false);
            st = conex.createStatement();

            String crea = "create table EnergiasYCostos(id_locAn INTEGER, id_fuente INTEGER, energiaAn REAL, costoAn REAL,"
                    + " UNIQUE (id_locAn,id_fuente), FOREIGN KEY (id_locAn) REFERENCES Localizadores (id_locAn) ON UPDATE CASCADE ON DELETE CASCADE,"
                    + "FOREIGN KEY (id_fuente) REFERENCES Fuentes (id_fuente) ON UPDATE CASCADE ON DELETE RESTRICT);";

            if (!(st.executeUpdate(crea) > 0)) {
                // throw new ExcAltaTabla("No se realizo el alta de la Tabla"); 
            }
            crea = "create table Localizadores(id_locAn INTEGER PRIMARY KEY AUTOINCREMENT, anio INTEGER, cronica INTEGER, id_corrida INTEGER,"
                    + "UNIQUE (anio,cronica,id_corrida), FOREIGN KEY (id_corrida) REFERENCES Corridas (id_corrida) ON UPDATE CASCADE ON DELETE CASCADE);";

            if (!(st.executeUpdate(crea) > 0)) {
                // lanzar una excepción que no se pudo realizar el alta de la tabla
            }
            crea = "create table Corridas(id_corrida INTEGER PRIMARY KEY AUTOINCREMENT, dirCorrida TEXT, UNIQUE(dirCorrida));";

            if (!(st.executeUpdate(crea) > 0)) {
                // lanzar una excepción que no se pudo realizar el alta de la tabla
            }
            crea = "create table Fuentes(id_fuente INTEGER PRIMARY KEY AUTOINCREMENT, recursoBase TEXT, tipo TEXT, subtipo1 TEXT, subtipo2 TEXT, UNIQUE (recursoBase));";
            if (!(st.executeUpdate(crea) > 0)) {
                // lanzar una excepción que no se pudo realizar el alta de la tabla
                // throw new ExcepcionAltaTabla(); 
            }
            conex.commit();
        } catch (Exception e) {
            throw new XcargaDatos("No se pudieron crear las tablas de la DB");

        } finally {
            conex.setAutoCommit(true);
            st.close();
            conex.close();
        }

    }

    /**
     * Método que carga los datos de ResSimulDB en la base de datos
     * @param dirResultCaso
     * @param dirCorrida
     * @param resDB
     * @throws XcargaDatos
     * @throws SQLException 
     */
    public void cargaDatosDB(String dirResultCaso, String dirCorrida, ResSimulDB resDB) throws XcargaDatos, SQLException {

        Connection conex = null;
        Statement st = null;
        try {
            //Cargo el driver de la librería y creo una conexión (conex) 
            Class.forName("org.sqlite.JDBC");
            conex = DriverManager.getConnection("jdbc:sqlite:" + dirResultCaso + "/DatosAnuales.db");
            conex.setAutoCommit(false);
            st = conex.createStatement();
            int idCorrida;
            // Primero pregunto si la corrida ya está en DB, si no la cargo
            String sent = "insert or ignore into Corridas(dirCorrida) values ('" + dirCorrida + "');";
            //st.executeUpdate(sent);            
            if ((st.executeUpdate(sent) == 1)) {
                System.out.println("Cargó nueva corrida en DB: " + dirCorrida);
                ResultSet rs = st.executeQuery("select id_corrida from Corridas where dirCorrida ='" + dirCorrida +"';");
                rs.next();
                idCorrida = rs.getInt("id_corrida");
                PreparedStatement pst;
                //Comienzo Ciclo en las fuentes
                for (int ifu = 0; ifu < resDB.getNombFuente().length; ifu++) {
                    // antes de cargar pregunto si la fuente existe si no la cargo.                    
                    rs = st.executeQuery("select id_fuente from Fuentes where recursoBase ='" + resDB.getNombFuente()[ifu] + "';");
                    if (!rs.next()) {
                        //st.executeUpdate("insert into Fuentes(recursoBase, tipo, subtipo1, subtipo2) values ('"+resDB.getNombFuente()[ifu]+"','"+resDB.getTipo()[ifu]+"','"+resDB.getSubtipo1()[ifu]+"','"+resDB.getSubtipo2()[ifu]+"');");
                        pst = conex.prepareStatement("insert into Fuentes(recursoBase, tipo, subtipo1, subtipo2) values( ?,?,?,?);");
                        pst.setString(1, resDB.getNombFuente()[ifu]);
                        pst.setString(2, resDB.getTipo()[ifu]);
                        pst.setString(3, resDB.getSubtipo1()[ifu]);
                        pst.setString(4, resDB.getSubtipo2()[ifu]);
                        pst.execute();
                    }
                    rs = st.executeQuery("select id_fuente from Fuentes where recursoBase ='" + resDB.getNombFuente()[ifu] + "';");
                    rs.next();
                    int idFuente = rs.getInt("id_fuente");
                    //Sentencia preparada para cargar energías y costos
                    pst = conex.prepareStatement("insert into EnergiasYCostos(id_fuente,id_locAn,energiaAn,costoAn) values( ?,?,?,?);");

                    for (int ic = 0; ic < resDB.cantDeCronicas(); ic++) {
                        //En la primer entrada cargo los localizadores 
                        if (ifu == 0) {
                            st.executeUpdate("insert into Localizadores(anio,cronica,id_corrida)"
                                    + "values (" + resDB.getAnio() + "," + resDB.getCronicas()[ic] + "," + idCorrida + ");");
                        }
                        //consulto el id_loc y lo cargo.
                        rs = st.executeQuery("select id_locAn from Localizadores where anio=" + resDB.getAnio() + " and cronica=" + resDB.getCronicas()[ic] + " and id_corrida=" + idCorrida + ";");
                        rs.next();
                        int idLocAn = rs.getInt("id_locAn");
                        /**
                         * Cargo las energías y los costos en la DB a partir de las matrices de resDB
                         */
                        pst.setInt(1, idFuente);
                        pst.setInt(2, idLocAn);
                        pst.setDouble(3, resDB.getMatEner()[ic][ifu]);
                        pst.setDouble(4, resDB.getMatCost()[ic][ifu]);
                        pst.execute();
                        // st.executeUpdate("insert into EnergiasYCostos(id_fuente,id_locAn,energiaAn,costoAn)"
                        //  + "values("+ idFuente +","+ idLocAn +","+resDB.getMatEner()[ic][ifu]+","+resDB.getMatCost()[ic][ifu]+");");                   
                    }
                }
             }
            conex.commit();
        } catch (Exception e) {
            conex.rollback();
            System.out.print(e.getMessage());
            throw new XcargaDatos(e.getMessage());
        } finally {
            conex.setAutoCommit(true);
            conex.close();
            st.close();
        }
    }
    
    /**
     * Si es necesario amplía la bolsa this para que pueda emplearse en el Estudio est.
     * Amplía los TiempoAbsoluto de la BolsaDeSimulaciones para usarla en el
     * Estudio est con TiemposAbsolutos no contenidos inicialmente y agrega los HashMap
     * 
     */
    public void ampliarBolsa(Estudio est) {
 
        // Recorre los TiempoAbsoluto de est y agrega los que falten a la BolsaDeSimulaciones
        ArrayList<TiempoAbsoluto> tiempos = est.getDatosGenerales().getTiempoAbsolutoPasos();
        for(TiempoAbsoluto ta: tiempos){
            if(!tiemposSimulados.contains(ta)){
                tiemposSimulados.add(ta);
                HashMap<ClaveStringSimul, ResSimul> hs = new HashMap<ClaveStringSimul, ResSimul>();
                resultados.add(hs);
            }          
        }        
    }
}
