/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package AlmacenSimulaciones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ut469262
 */
public class PaqueteCorridas implements Serializable{
     private int ordinalPaquete;
     private ArrayList<IdentSimulPaso> listaCorridas;

     public PaqueteCorridas(int ordinalPaquete, ArrayList<IdentSimulPaso> listaCorridas) {
          this.ordinalPaquete = ordinalPaquete;
          this.listaCorridas = listaCorridas;
     }
     

     public ArrayList<IdentSimulPaso> getListaCorridas() {
          return listaCorridas;
     }

     public void setListaCorridas(ArrayList<IdentSimulPaso> listaCorridas) {
          this.listaCorridas = listaCorridas;
     }

     public int getOrdinalPaquete() {
          return ordinalPaquete;
     }

     public void setOrdinalPaquete(int ordinalPaquete) {
          this.ordinalPaquete = ordinalPaquete;
     }

}
