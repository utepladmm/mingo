package AlmacenSimulaciones;

import java.util.ArrayList;

import UtilitariosGenerales.ParString;

public class DatosParalelizacion {
	
	private String rutaEjecutable;
	private ArrayList<ParString> corridas;
		
	public String getRutaEjecutable() {
		return rutaEjecutable;
	}
	public void setRutaEjecutable(String rutaEjecutable) {
		this.rutaEjecutable = rutaEjecutable;
	}
	public ArrayList<ParString> getCorridas() {
		return corridas;
	}
	public void setCorridas(ArrayList<ParString> corridas) {
		this.corridas = corridas;
	}
	
	

}
