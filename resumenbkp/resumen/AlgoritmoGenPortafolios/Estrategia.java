/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package AlgoritmoGenPortafolios;

import naturaleza3.BolsaDeConjsNodosN;
import GrafoEstados.Objetivo;
import GrafoEstados.Portafolio;
import dominio.Caso;
import dominio.ConjRecB;
import dominio.ConjTransB;
import dominio.DatosGeneralesEstudio;
import dominio.Estudio;
import java.util.ArrayList;
import java.util.HashMap;
import naturaleza3.ConjNodosN;
import naturaleza3.EscenarioCN;
import naturaleza3.GrafoN;
import naturaleza3.ListaEscenariosCN;
import naturaleza3.NodoN;
import persistencia.XcargaDatos;
/**
 *
 * @author ut469262
 */
public class Estrategia {

    private static BolsaDeConjsNodosN bolsaCDP;
    /**
     * Bolsa de conjuntos determinantes de parque del grafoN asociado al problema.
     */
    private static Estudio estudio;
    private static Caso caso;
    private static ConjRecB conjRBEleg;
    private static ConjTransB conjTBeleg;



    private HashMap<ConjNodosN, ArrayList<Integer>> cotasInicio;
    /**
     * Para cada ConjNodosN (es decir para cada conjunto determinante de parque
     * de un grafoN) se especifican las cotas de inicio de inversión como enteros
     * en el mismo orden en que aparecen en conjRBEleg y conjTBeleg, en ese orden.
     */


    private HashMap<ConjNodosN, Portafolio> decisiones;
    /**
     * Para cada ConjNodosN se especifica una decisión de inversión.
     */

    public Estrategia (){
        decisiones = new HashMap<ConjNodosN, Portafolio>();
    }

    public static BolsaDeConjsNodosN getBolsaCDP() {
        return bolsaCDP;
    }

    public static void setBolsaCDP(BolsaDeConjsNodosN bolsaCDP) {
        Estrategia.bolsaCDP = bolsaCDP;
    }

    public HashMap<ConjNodosN, Portafolio> getDecisiones() {
        return decisiones;
    }

    public void setDecisiones(HashMap<ConjNodosN, Portafolio> decisiones) {
        this.decisiones = decisiones;
    }

    


    public Portafolio portDeUnCDP(ConjNodosN cdp) throws XcargaDatos{
        Portafolio port = decisiones.get(cdp);
        if (port==null) throw new XcargaDatos("Se pidió el portafolio de un ConjNodosN que no existe" +
                "en la bolsaCDP de las estrategias");
        return port;

    }

    /**
     * Genera las trayectorias de portafolios para cada uno
     * de los escenarios de la lista de escenarios, dada la estrategia est
     *
     *
     * Crea un reporte de expansión de Portafolios para cada escenario y lo guarda en archExpan.
     *
     * Carga en las restricciones heurísticas las violaciones registradas
     * (restricciones que no se cumplen con holgura)
     *
     * Evalúa el objetivo para la estrategia est
     *
     * Atención !!!!!! porque el problema está definido en etapas y no en pasos
     * hay que tener en cuenta las etapas representativas.
     *
     * @param grafoN es el grafo de la naturaleza
     *
     * @param baseDeDatosResult es el objeto donde se almacenan resultados de simulación.
     *
     * @param est es la estrategia cuyas decisiones se considera. Si est==null
     * el recorredor toma las decisiones óptimas para el objetivo obj.
     *
     * @param cotas es el conjunto de restricciones heurísticas de inicio
     * de construcción que pueden ser violadas por las trayectorias óptimas
     * en el get(0) está la etapa 1.
     *
     * @param listaEsc es una ListaEscenariosCN con los escenarios de ConjNodosN a recorrer
     *
     * @param archExpan es el nombre del directorio donde se grabarán las expansiones
     * óptimas, una para cada escenario.
     */
    public static double valorEstrategia(GrafoN grafoN, Estudio est,
            Object baseDeDatosResult,
            Estrategia estr, Portafolio pUno,
            ListaEscenariosCN listaEsc, Objetivo obj,
            String archExpan, ArrayList<Object> informacion) throws XcargaDatos {

        DatosGeneralesEstudio datGen = est.getDatosGenerales();
        double valorObjetivo = 0.0;
        double factorDesc = datGen.getFactorDescPaso();
        datGen.getEtapasPasos();


        /**
         * Se actualiza el objetivo al inicio de la etapa 1.
         */


        HashMap<ConjNodosN, Portafolio> decisiones = estr.getDecisiones();
        int cantEtapas = grafoN.getCantEtapas();
        if(cantEtapas != listaEsc.getCantEtapas()) throw new XcargaDatos("La cantidad de etapas del" +
                "GrafoN y de los escenarios no coinciden");
        Portafolio porte;
        Portafolio portemas1 = null;
        int iesc = 1;
        for (EscenarioCN esc : listaEsc.getEscenarios()) {
            porte = pUno;
            double valorObjEsc = 0.0;
            double probEsc = esc.getProb();
            double factorDescAcum = Math.pow(factorDesc, 0.5);
            for (int e = 1; e <= cantEtapas; e++) {
                /**
                 * cDPNe es el conjunto determinante de parque en grafoN, es por
                 * lo tanto un ConjNodosN que tienen el mismo estado de las variables
                 * de la naturaleza observadas y de las que determinan parque (si existen)
                 */
                ConjNodosN cDPNe = esc.conjNodosNdeEtapa(1);

                for(NodoN nN: cDPNe.getNodos()){
                    /**
                     * El valor del objetivo toma en cuenta todos los NodoN del
                     * conjunto determinante de parque en el grafoN cDPNe, con su probabilidad
                     * condicional.
                     */
//                    valorObjEsc += porte.calculaCostoPaso(obj, baseDeDatosResult, nN, informacion)
//                                    *nN.getProbCondDadoCDP()*factorDescAcum;
// sustituir por el que usa nodoE y no nN como argumento.                    
                }
                // decision es la decisión de inicio de construcción que establece la estrategia.
                Portafolio decision = decisiones.get(cDPNe);
                /**
                 * Genera el Portafolio sucesor portemas1 salvo que se esté en la última etapa
                 */
                if(e<cantEtapas){
                    ConjNodosN cDPNemas1 = esc.conjNodosNdeEtapa(e+1);
                    portemas1 = porte.sucesorDeUnPort(decision, cDPNe, cDPNemas1, informacion);
                }
                factorDescAcum = factorDescAcum*factorDesc;
                porte = portemas1;

            }
            valorObjetivo += valorObjEsc*probEsc;
            iesc++;
        }
        return valorObjetivo;
    }





}
