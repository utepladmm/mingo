/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package AlgoritmoGenetico;

import java.util.ArrayList;

/**
 *
 * @author Usuario
 */
public class UtilitariosProgGen {

    /**
     * Devuelve la cantidad de bits mínima que permite guardar
     * el entero numero
     * @param numero
     * @return
     */
    public static int cantBitsDeEntero(int numero){
        double log2Numero = Math.log(numero)/Math.log(2.0);
        int cant = (int) Math.ceil(log2Numero);
        return cant;
    }

    public static int enteroDeCodBin(ArrayList<Integer> codBin){
        int entero = 0;
        for(Integer bit: codBin){
            entero = entero*2 + bit;
        }
        return entero;
    }
}
