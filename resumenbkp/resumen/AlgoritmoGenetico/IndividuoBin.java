package AlgoritmoGenetico;


import java.util.ArrayList;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario
 */
public class IndividuoBin {
    private ArrayList<Integer> codigo;

    public IndividuoBin() {
        codigo = new ArrayList<Integer>();
    }

    public ArrayList<Integer> getCodigo() {
        return codigo;
    }

    public void setCodigo(ArrayList<Integer> codigo) {
        this.codigo = codigo;
    }

/**
 * Crea un individuo con un código cruzado de los dos individuos ind1 e ind2
 * @param ind1
 * @param ind2
 * @return
 */
    public static IndividuoBin cruzaSimple(IndividuoBin ind1, IndividuoBin ind2){
        int cant = ind1.getCodigo().size();
        double padre = Math.random();
        boolean padre1 = (padre<0.5);
        int nMaxPadre = ind1.eligeUnBit();
        /**
         * nMaxPadre es la cantidad de bits iniciales del padre que tendrá el hijo
         */
        ArrayList<Integer> codHijo = new ArrayList<Integer>();
        ArrayList<Integer> codPad = new ArrayList<Integer>();
        ArrayList<Integer> codMad = new ArrayList<Integer>();
        if(padre1){
            codPad.addAll(ind1.getCodigo());
            codMad.addAll(ind2.getCodigo());
        }else{
            codPad.addAll(ind2.getCodigo());
            codMad.addAll(ind1.getCodigo());
        }
        for(int ibin = 0; ibin <= nMaxPadre; ibin ++){
            codHijo.add(codPad.get(ibin));
        }
        for(int ibin = nMaxPadre+1; ibin < cant; ibin ++){
            codHijo.add(codPad.get(ibin));
        }
        IndividuoBin hijo = new IndividuoBin();
        hijo.setCodigo(codHijo);
        return hijo;
    }

    /**
     * Crea un nuevo individuo con un bit elegido aleatoriamente,
     * cambiado de estado sin afectar el individuo original.
     * @return
     */
    public IndividuoBin mutacion(){
        int ordBit = eligeUnBit();
        IndividuoBin copia = copiaIndividuo();
        if(copia.getCodigo().get(ordBit) == 0){
            copia.getCodigo().set(ordBit,1);
        }else{
            copia.getCodigo().set(ordBit,0);
        }
        return copia;
    }


    /**
     * Hace nacer un individuo binario aleatorio con nbits bits
     */
    public static IndividuoBin naceIndBinNbits(int nbits){
        IndividuoBin nuevo = new IndividuoBin();
        for(int ib=1; ib<= nbits; ib++){
            double real = Math.random();
            if(real<=0.5){
                nuevo.getCodigo().add(0);
            }else{
                nuevo.getCodigo().add(1);
            }
        }
        return nuevo;
    }


    /**
     * Devuelve el ordinal de uno de los bits del individuo
     * @return ordinalBit
     */
    public int eligeUnBit(){
        int cant = codigo.size();
        double corte = Math.random();
        int ordinalBit = (int) Math.floor(cant*corte);
        if(ordinalBit==cant)ordinalBit = cant-1;
        return ordinalBit;
    }

    /**
     * Crea una copia del individuo this sin alterarlo.
     * @return
     */
    public IndividuoBin copiaIndividuo(){
        IndividuoBin copia = new IndividuoBin();
        for(int ic = 0; ic<codigo.size(); ic++){
            copia.getCodigo().add(codigo.get(ic));
        }
        return copia;
    }

    @Override
    public String toString(){
        String texto ="IndividuoBin: ";
        for(int ent: codigo){
            texto += ent + " ";
        }
        return texto;
    }



}

