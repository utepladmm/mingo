/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package AlgoritmoGenetico;

import java.util.ArrayList;

/**
 *
 * @author Usuario
 */
public class IndividuoEntero extends Individuo implements Comparable{
    private ArrayList<Integer> codigoEnteros;
//    private IndividuoBin individuoBin;
    private static ArrayList<Integer> cotasEnteras;
    /**
     * El get(i) tiene el mayor valor que puede tomar el i-esimo entero
     */
    private static ArrayList<Integer> cantsBits;
    /**
     * El get(i) tiene la cantidad de bits asociada al i-esimo entero en el
     * IndividuoBin asociado. Dependen de los valores de cotaEntera
     */
    private static int bitsTotales; // la suma de todas las cantBits
    private Object objeto;
    /**
     * Este objeto será el objeto de la clase concreta que se está evaluando
     * Por ejemplo Portafolio, Estrategia, etc..
     */
    private double calificacion;
    /**
     * Un valor de calificación para cada uno de los numerarios relevantes
     */

    public IndividuoEntero(){
        codigoEnteros = new ArrayList<Integer>();
    }

    /**
     * Devuelve el código entero de ordinal ord
     * Los ordinales empiezan en cero.
     */
    public int getCodigoEnteroOrdinal(int ord){
        return codigoEnteros.get(ord);
    }

    public IndividuoBin getIndividuoBin() {
        return individuoBin;
    }

    public void setIndividuoBin(IndividuoBin individuoBin) {
        this.individuoBin = individuoBin;
    }

    public double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }



    /**
     * Agrega un entero al código y devuelve el ordinal en que quedó
     * Lanza excepción si el entero excede la cota del ordinal.
     * @return
     */
    public int agregaCodigoEnt(int entero) throws Exception{
        int ordinalNue = codigoEnteros.size(); // ordinalNue es el ordinal en que quedó el entero
        if(entero>cotasEnteras.get(ordinalNue)) throw new Exception();
        codigoEnteros.add(entero);
        return ordinalNue;
    }


    public static ArrayList<Integer> getCotasEnteras() {
        return cotasEnteras;
    }

    public static void setCotasEnteras(ArrayList<Integer> cotasEnteras) {
        IndividuoEntero.cotasEnteras = cotasEnteras;
        cantsBits = new ArrayList<Integer>();
        bitsTotales = 0;
        for(int ent: IndividuoEntero.cotasEnteras){
            bitsTotales += UtilitariosProgGen.cantBitsDeEntero(ent);
            cantsBits.add(UtilitariosProgGen.cantBitsDeEntero(ent));
        }
    }

    /**
     * Carga el IndividuoBin asociado a (this)
     */
    public void cargaIndividuoBin() throws Exception{
        if(codigoEnteros.size() != cotasEnteras.size())throw new Exception("La cantidad" +
                "de enteros es errada y no permite pasar a binario");
        individuoBin = indBinDeEntero(this);
    }


    /**
     * Devuelve el IndividuoBin asociado a un IndividuoEntero
     * @param indivE
     * @return
     */
    public static IndividuoBin indBinDeEntero(IndividuoEntero indivE){
        IndividuoBin indivB = new IndividuoBin();
        int cantEnteros = cotasEnteras.size();
        for(int iE=0; iE< cantEnteros; iE++ ){
            String sBin = Integer.toBinaryString(indivE.getCodigoEnteroOrdinal(iE));
            /**
             *  se deben agregar al principio bits 0 hasta completar la cantidad de bits
             *  asociada al entero i-ésimo.
             */
            int aAgregar = cantsBits.get(iE) - sBin.length();
            for(int iBit=1; iBit<= aAgregar; iBit++){
                indivB.getCodigo().add(0);
            }
            for(int iBit=aAgregar+1; iBit<= cantsBits.get(iE); iBit++){
                int puntero = iBit-aAgregar-1;
                String unocero = sBin.substring(puntero, puntero+1);
                if(unocero.equalsIgnoreCase("0")){
                    indivB.getCodigo().add(0);
                }else{
                    indivB.getCodigo().add(1);
                }
            }
        }
        return indivB;
    }




    /**
     * Devuelve el IndividuoEntero asociado a un IndividuoBin
     * @return
     */
    public static IndividuoEntero indEntDeBinario(IndividuoBin indBin) throws Exception{
        IndividuoEntero indEnt = new IndividuoEntero();
        indEnt.setIndividuoBin(indBin);
        if(indBin.getCodigo().size() != bitsTotales) throw new Exception("Error en bitsTotales");
        int puntero = 0;
        for(int ie=0; ie<cantsBits.size(); ie++){
            ArrayList<Integer> aux = new ArrayList<Integer>();
            for(int ib=0; ib<cantsBits.get(ie); ib++){
                aux.add(indBin.getCodigo().get(puntero));
                puntero ++;
            }
            int entero = UtilitariosProgGen.enteroDeCodBin(aux);
            indEnt.agregaCodigoEnt(entero);
        }
        return indEnt;
    }


    /**
     * Crea un individuo por cruza de dos IndividuoEntero sin afectar los padres.
     * @param ind1
     * @param ind2
     * @return
     * @throws java.lang.Exception
     */
    public static IndividuoEntero cruzaSimple(IndividuoEntero ind1, IndividuoEntero ind2) throws Exception{
        IndividuoEntero hijo = new IndividuoEntero();
        if(ind1.getIndividuoBin()==null) throw new Exception("Individuo1 sin binario");
        if(ind2.getIndividuoBin()==null) throw new Exception("Individuo2 sin binario");
        IndividuoBin bin1 = ind1.getIndividuoBin();
        IndividuoBin bin2 = ind2.getIndividuoBin();
        IndividuoBin hijoBin = IndividuoBin.cruzaSimple(bin1, bin2);
        hijo = indEntDeBinario(hijoBin);
        hijo.setIndividuoBin(hijoBin);
        return hijo;
    }


    /**
     * Crea un nuevo individuo por mutación de this, sin alterar this.
     * @return
     */
    public IndividuoEntero mutacionDeIndEnt() throws Exception{
        IndividuoBin binCopia = (this).getIndividuoBin().copiaIndividuo();
        IndividuoBin binMut = binCopia.mutacion();
        IndividuoEntero entMut = IndividuoEntero.indEntDeBinario(binMut);
        return entMut;
    }

//=============================================
//  COMIENZAN MÉTODOS A SER SOBRE ESCRITOS
//=============================================

    /**
     * Determina y carga en el IndividuoEnt su calificación.
     * @param informacion
     */
    public void evaluaIndividuoEnt(ArrayList<Object> informacion){

    }

    public void creaObjeto(ArrayList<Object> informacion){
        
    }

    public Individuo creaCopia(Individuo ind){
        Individuo copia = new IndividuoEntero();
        return copia;
    }



//=============================================
//  FIN MÉTODOS A SER SOBRE ESCRITOS
//=============================================

    @Override
    public String toString(){
        String texto = "";
        for (Integer ent: codigoEnteros){
            texto += ent + " - ";
        }
        return texto;
    }

    public static String toStringClaseIndEnt(){

        String texto = "Cotas Enteras : ";
        for (Integer ent: cotasEnteras){
            texto += ent + " - ";
        }
        texto += "\r\n" + "Cantidad de Bits de los enteros ";
        for (Integer ent: cantsBits){
            texto += ent + " - ";
        }
        return texto;


    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IndividuoEntero other = (IndividuoEntero) obj;
        if (this.individuoBin != other.individuoBin && (this.individuoBin == null || !this.individuoBin.equals(other.individuoBin))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.individuoBin != null ? this.individuoBin.hashCode() : 0);
        return hash;
    }

    public int compareTo(Object o) {
        IndividuoEntero otro = (IndividuoEntero)o;
        if(calificacion<otro.getCalificacion())return -1;
        if(calificacion==otro.getCalificacion())return 0;
        return 1;
    }





}
