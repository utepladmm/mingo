/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package AlgoritmoGenetico;

import java.util.ArrayList;

/**
 *
 * @author Usuario
 * 
 */
public class Individuo implements Comparable{

    protected IndividuoBin individuoBin;
    private static int bitsTotales; // la cantidad de bits del individuoBin asociado
    protected ArrayList<Object> codigoDelIndividuo;
    /**
     * Es el código que describe el Individuo de manea intuitiva
     * por ejemplo un código entero que describe una estrategia discreta
     * de inversiones.
     */
    private Object objeto;
    /**
     * Este objeto será el objeto de la clase concreta que se está evaluando
     * Por ejemplo Portafolio, Estrategia, etc..
     */
    private double calificacion;
    /**
     * Un valor de calificación para cada uno de los numerarios relevantes
     */


    public IndividuoBin getIndividuoBin() {
        return individuoBin;
    }

    public void setIndividuoBin(IndividuoBin individuoBin) {
        this.individuoBin = individuoBin;
    }

    public double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }

    public int getBitsTotales() {
        return bitsTotales;
    }

    public void setBitsTotales(int bitsTotales) {
        Individuo.bitsTotales = bitsTotales;
    }

    public ArrayList<Object> getCodigoDelIndividuo() {
        return codigoDelIndividuo;
    }

    public void setCodigoDelIndividuo(ArrayList<Object> codigoDelIndividuo) {
        this.codigoDelIndividuo = codigoDelIndividuo;
    }

    public Object getObjeto() {
        return objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }


        /**
     * Crea un Individuo por cruza de dos Individuo sin afectar los padres.
     * @param ind1
     * @param ind2
     * @return
     * @throws java.lang.Exception
     */
    public static Individuo cruzaSimple(Individuo ind1, Individuo ind2) throws Exception{
        if(ind1.getIndividuoBin()==null) throw new Exception("Individuo1 sin binario");
        if(ind2.getIndividuoBin()==null) throw new Exception("Individuo2 sin binario");
        IndividuoBin bin1 = ind1.getIndividuoBin();
        IndividuoBin bin2 = ind2.getIndividuoBin();
        IndividuoBin hijoBin = IndividuoBin.cruzaSimple(bin1, bin2);
        Individuo hijo = individuoDeIndBin(hijoBin);
        hijo.setIndividuoBin(hijoBin);
        return hijo;
    }
    


//=============================================
//  COMIENZAN MÉTODOS A SER SOBRE ESCRITOS
//=============================================


    /**
     * Carga el IndividuoBin asociado a (this)
     */
    public void cargaIndividuoBin() throws Exception{

    }

    /**
     * Crea un Individuo a partir de un IndividuoBinario
     */
    public static Individuo individuoDeIndBin(IndividuoBin indBin){
        Individuo ind = new Individuo();
        return ind;
    }


    /**
     *  Crea un individuo nuevo copiado del original que puede modificarse
     *  sin alterar el original
     */
    public Individuo creaCopia(){
        Individuo copia = new Individuo();
        return copia;
    }


    /**
     * Devuelve el IndividuoBin asociado a un Individuo
     * @param indivE
     * @return
     */
    public static IndividuoBin indBinDeIndividuo(Individuo indiv){
        IndividuoBin indivB = new IndividuoBin();
        return indivB;
    }






    /**
     * Devuelve el Individuo asociado a un IndividuoBin
     * @return
     */
    public static Individuo individuoDeBinario(IndividuoBin indBin) throws Exception{
        Individuo ind = new Individuo();
        return ind;
    }




    /**
     * Crea un nuevo individuo por mutación de this, sin alterar this.
     * @return
     */
    public Individuo mutacionDeIndividuo() throws Exception{
        Individuo indMut = new Individuo();
        return indMut;

//        kk acá habria que implementar más y solo
//        que quede para sobreescribir en los hijos de Individuo
//        los metodos que pasan de binario a individuo y objeto y al revés
    }


    /**
     * Crea un nuevo individuo por mutación de this, sin alterar this.
     * @return
     */
    public Individuo mutacionSobreIndividuo() throws Exception{
        Individuo indMut = new Individuo();
        return indMut;
    }



    /**
     * Determina y carga en el Individuo su calificación.
     * @param informacion
     */
    public void evaluaIndividuo(ArrayList<Object> informacion){

    }

    /**
     * Crea un objeto del mundo del problema que es la imagen del Individuo
     * @param informacion
     */
    public void creaObjeto(ArrayList<Object> informacion){

    }



//=============================================
//  FIN MÉTODOS A SER SOBRE ESCRITOS
//=============================================



    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Individuo other = (Individuo) obj;
        if (this.individuoBin != other.individuoBin && (this.individuoBin == null || !this.individuoBin.equals(other.individuoBin))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.individuoBin != null ? this.individuoBin.hashCode() : 0);
        return hash;
    }

    public int compareTo(Object o) {
        Individuo otro = (Individuo)o;
        if(calificacion<otro.getCalificacion())return -1;
        if(calificacion==otro.getCalificacion())return 0;
        return 1;
    }





}
