/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package AlgoritmoGenetico;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Usuario
 */
public class PoblacionIndividuos {
    protected int cantIndiv;  // cantidad de individuos en cada generación
    protected int cantPadres; // cantidad par de individos que generarán hijos en cada generación
    protected int cantHijosPorPareja; // cantidad de hijos que tiene cada pareja
    protected int cantResurrecciones; // cantidad de individuos extintos que vuelven a la vida
    /**
     * Para que la población sea constante debe ser:
     *
     * cantResurrecciones = cantIndiv - (cantPadres/2)*cantHijosPorPareja
     * cantPadres*2 <= cantIndiv
     */
    ArrayList<Individuo> poblacion;
    ArrayList<Individuo> extintos;

    public void iniciaPoblacion(int cantidadIndiv, Individuo indivPrototipo){
        int cantBits = indivPrototipo.getBitsTotales();
        for(int i=1; i<= cantIndiv; i++){
            /**
             * Para hacer nacer los individuos iniciales de la clase del indivPrototipo
             * se empieza por crear copias que luego se "individualizan"
             */
            Individuo copia = indivPrototipo.creaCopia();
            IndividuoBin iBin = IndividuoBin.naceIndBinNbits(cantBits);
            copia.setIndividuoBin(iBin);
            poblacion.add(copia);
        }
    }

    /**
     * Selecciona cantPadres individuos de la población pudiendo repetir padres
     * @return
     */
    public ArrayList<Individuo> seleccionaPadres(){
        Collections.sort(poblacion);
        ArrayList<Individuo> padres = new ArrayList<Individuo>();
        /**
         * Se atribuye a cada individuo una probabilidad proporcional
         * a la diferencia de su calificación con la mímina de la población
         */
        double minCalif = poblacion.get(0).getCalificacion();
        ArrayList<Double> topeSuperior = new ArrayList<Double>();
        /**
         * topeSuperior.get(i) será el máximo valor de la variable aleatoria
         * uniforme (0,1) que selecciona el elemento i-esimo de la población
         * ordenada.
         */
        double sumaCalif = 0.0;
        for(Individuo ind: poblacion){
            sumaCalif += ind.getCalificacion() - minCalif;
            topeSuperior.add(sumaCalif);
        }
        for(int j=0; j<topeSuperior.size(); j++){
            topeSuperior.set(j, topeSuperior.get(j)/sumaCalif);
        }
        for (int i=1; i<=cantPadres; i++){
            double selec = Math.random();
            for(int j=0; j<topeSuperior.size(); j++){
                if(topeSuperior.get(j)>selec ||  j==topeSuperior.size()-1){
                    padres.add(poblacion.get(j));
                    break;
                }
            }
        }
        return padres;
    }


    /**
     * Elije un individuo al azar de un ArrayList de Indivuduo
     * población y lo devuelve, (no devuelve una
     * copia sino el propio individuo).
     * @return
     */
    public Individuo elijeUnIndivAlAzar(ArrayList<Individuo> lista){
        double unif = Math.random();
        int indSel = (int) Math.floor(unif*lista.size());
        return lista.get(indSel);
    }




    /**
     * Crea parejas de padres y genera cantHijosPorPareja de cada pareja.
     * La lista padres no se altera.
     * Si hay un número impar de padres uno al azar pasa idéntico a la población de hijos.
     */
    public ArrayList<Individuo> creaHijos(ArrayList<Individuo> padres) throws Exception{
        ArrayList<Individuo> hijos = new ArrayList<Individuo>();
        int cantP  = padres.size();
        int restoDiv2 = (int) (cantP - Math.floor(cantP / 2) * 2);
        if(restoDiv2!= 0){
            Individuo padre0 = elijeUnIndivAlAzar(padres);
            Individuo hijo0 = padre0.creaCopia();
            hijos.add(hijo0);
        }
        while (!poblacion.isEmpty()){
            Individuo padre1 = elijeUnIndivAlAzar(padres);
            Individuo padre2 = elijeUnIndivAlAzar(padres);
            Individuo hijo = Individuo.cruzaSimple(padre1, padre2);
            hijos.add(hijo);
            padres.remove(padre1);
            padres.remove(padre2);
        }
        return hijos;
    }

    /**
     *  Elije al azar n individuos de la población e introduce en
     *  cada uno de ellos una mutación diferente, dejándolos modificados.
     *
     */
    public void mutaNIndividuos(int n) throws Exception{
        if(n>poblacion.size())throw new Exception("Se pidió mutación de más" +
                "individuos que la población existente");
        for(int imut = 1; imut <= n; imut++){
            Individuo unInd = elijeUnIndivAlAzar(poblacion);
            unInd.mutacionDeIndividuo();
        }
    }
}
